﻿ALTER TABLE [oso].[stg_AW_Cases_Update] DROP CONSTRAINT [DF_stg_AW_Cases_Update_UploadedOn]
GO

ALTER TABLE [oso].[stg_AW_Cases_Update] DROP CONSTRAINT [DF__stg_AW_Cases_Update__Imported]
GO

/****** Object:  Table [oso].[stg_AW_Cases_Update]    Script Date: 08/06/2021 12:21:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[oso].[stg_AW_Cases_Update]') AND type in (N'U'))
DROP TABLE [oso].[stg_AW_Cases_Update]
GO

/****** Object:  Table [oso].[stg_AW_Cases_Update]    Script Date: 08/06/2021 12:21:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_AW_Cases_Update](
	[CaseId] [int] NOT NULL,
	[CasePrefix] [varchar](1) NOT NULL,
	[TitleLiable1] [varchar](50) NULL,
	[FirstnameLiable1] [varchar](50) NULL,
	[LastnameLiable1] [varchar](50) NULL,
	[TitleLiable2] [varchar](50) NULL,
	[FirstnameLiable2] [varchar](50) NULL,
	[LastnameLiable2] [varchar](50) NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[UploadedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_AW_Cases_Update] ADD  CONSTRAINT [DF__stg_AW_Cases_Update__Imported]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [oso].[stg_AW_Cases_Update] ADD  CONSTRAINT [DF_stg_AW_Cases_Update_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO


/****** Object:  StoredProcedure [oso].[usp_AW_Cases_Update_DT]    Script Date: 08/06/2021 16:15:51 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_AW_Cases_Update_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_AW_Cases_Update_DT]    Script Date: 08/06/2021 16:15:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_AW_Cases_Update_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE 
			@CaseId int, 
			@CasePrefix varchar(1),
			@StatusId int,
			@TitleLiable1 varchar(50),
			@FirstnameLiable1 varchar(50),	
			@LastnameLiable1 varchar(50),
			@TitleLiable2 varchar(50),
			@FirstnameLiable2 varchar(50),
			@LastnameLiable2  varchar(50)

            ,@StageId        INT

			,@Succeed bit
			,@ErrorId INT
			,@CStatusId int
			,@UserId int = 2

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorAW')>=-1
		BEGIN
			DEALLOCATE cursorAW
		END

        DECLARE cursorAW CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
		SELECT 
			CaseId, 
			CasePrefix,
			CASE CasePrefix 
				WHEN 'H' THEN 6 
				WHEN 'W' THEN 8		 
				WHEN 'C' THEN 16		
			END StatusId,
			TitleLiable1, 
			FirstnameLiable1, 
			LastnameLiable1, 
			TitleLiable2,
			FirstnameLiable2, 
			LastnameLiable2 	
		FROM 
			[oso].[stg_AW_Cases_Update]
		WHERE 
			Imported = 0 
			AND ErrorId IS NULL

	OPEN cursorAW
	FETCH NEXT
	FROM
		cursorAW
	INTO
		@CaseId, 
		@CasePrefix,
		@StatusId,
		@TitleLiable1,
		@FirstnameLiable1,	
		@LastnameLiable1,
		@TitleLiable2,
		@FirstnameLiable2,
		@LastnameLiable2
	WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION

					IF(@CasePrefix in ('C','H','W'))
					BEGIN
						UPDATE dbo.Cases
						SET 
							previousStatusId = statusId,
							previousStatusDate = statusDate,	
							statusId = @StatusId, 
							statusDate = GETDATE()					
						FROM Cases
						WHERE Id = @caseId
					END

					IF(@CasePrefix = 'U')
					BEGIN
						UPDATE dbo.Cases
						SET 				
							debtName = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1))
						FROM dbo.Cases
						WHERE Id = @caseId 

						-- Update Lead Defaulter Name
						UPDATE dbo.Defaulters
						SET	
							name = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1)),
							firstName = @FirstnameLiable1,
							lastName = @LastnameLiable1
						from 
							dbo.Defaulters 
							join dbo.DefaulterCases dc on dbo.Defaulters.Id = dc.defaulterId		
						where 
						dc.caseId = @caseId
						AND isLeadCustomer = 1

						--Update Second Defaulter Name
						DECLARE @DefaulterId int;
						
						SELECT TOP 1 @DefaulterId = dc.defaulterId  
						FROM DefaulterCases dc
						INNER JOIN Defaulters d on d.Id = dc.defaulterId
						WHERE dc.caseId = @CaseId AND isLeadCustomer = 0
						ORDER BY dc.defaulterId ASC

						IF (@FirstnameLiable2 IS NOT NULL OR @LastnameLiable2 IS NOT NULL)
						BEGIN 
							IF (@DefaulterId IS NOT NULL )
							BEGIN
								--Update 2nd defaulter
								UPDATE dbo.Defaulters
								SET	
									name = (select Concat(@TitleLiable2,' ', @FirstnameLiable2, ' ', @LastnameLiable2)),
									firstName = @FirstnameLiable2,
									lastName = @LastnameLiable2
								from 
									dbo.Defaulters 		
								where 
								Id = @DefaulterId						
							END
							--ELSE
							--BEGIN
							----Create 2nd defaulter
							--END

						END
					END		


					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[stg_AW_Cases_Update]
					SET
					Imported   = 1
					, ImportedOn = GetDate()
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND CaseId = @CaseId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_AW_Cases_Update]
					SET    ErrorId = @ErrorId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND CaseId = @CaseId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorAW
            INTO
				@caseId, 
				@CasePrefix,
				@StatusId,
				@TitleLiable1,
				@FirstnameLiable1,	
				@LastnameLiable1,
				@TitleLiable2,
				@FirstnameLiable2,
				@LastnameLiable2	
        END
        CLOSE cursorAW
        DEALLOCATE cursorAW

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_GetOnlyAnglianWPCases]    Script Date: 14/06/2021 10:46:54 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetOnlyAnglianWPCases]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetOnlyAnglianWPCases]    Script Date: 14/06/2021 10:46:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetOnlyAnglianWPCases]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT
			c.Id CCaseId, c.Id CaseId
		FROM Cases c
			INNER JOIN Batches ba ON c.batchId = ba.Id
			INNER JOIN ClientCaseType cct ON ba.clientCaseTypeId = cct.Id
			INNER JOIN Brands.Brands bb ON cct.brandId = bb.Id
		WHERE 
			cct.clientId = (select Id from Clients where name = 'Anglian Water Pre-Writ')
    END
END

GO