
/****** Object:  Schema [oso]    Script Date: 19/04/2021 15:01:21 ******/
CREATE SCHEMA [oso]
GO
/****** Object:  UserDefinedFunction [oso].[fn_RemoveMultipleSpaces]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [oso].[fn_RemoveMultipleSpaces] (
	@inputString nvarchar(max) 
)
RETURNS nvarchar(max)
AS
BEGIN
	WHILE CHARINDEX(SPACE(2), @inputString) > 0
	BEGIN
		SET @inputString = REPLACE(@inputString, SPACE(2), SPACE(1))
	END

	RETURN @inputString
END
GO
/****** Object:  UserDefinedFunction [oso].[fnSplitStringbasedonDelimiter]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [oso].[fnSplitStringbasedonDelimiter] 
( 
    @string NVARCHAR(MAX), 
    @delimiter char(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END
GO
/****** Object:  Table [oso].[OSColCaseTypeXRef]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColCaseTypeXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CaseType] [nvarchar](100) NOT NULL,
	[CaseTypeId] [int] NOT NULL,
 CONSTRAINT [PK_OSColCaseTypeXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColClentsXRef]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColClentsXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Connid] [int] NOT NULL,
	[CClientId] [int] NOT NULL,
	[ClientName] [nvarchar](250) NULL,
 CONSTRAINT [PK_OSColClentsXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[SQLErrors]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[SQLErrors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ErrorNumber] [int] NULL,
	[Severity] [int] NULL,
	[State] [int] NULL,
	[ErrorProcedure] [nvarchar](128) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[ExecutedOn] [datetime] NULL,
 CONSTRAINT [PK_SQLErrors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_AdditionalCaseData]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_AdditionalCaseData](
	[CaseId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](2000) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_BU_DefaulterEmails]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_BU_DefaulterEmails](
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar](7) NULL,
	[cDefaulterId] [int] NOT NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_BU_DefaulterPhones]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_BU_DefaulterPhones](
	[TypeID] [int] NOT NULL,
	[TelephoneNumber] [varchar](50) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar](7) NULL,
	[cDefaulterId] [int] NOT NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_AdditionalCaseData]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData](
	[ClientCaseReference] [varchar](100) NULL,
	[ClientId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](max) NOT NULL,
	[ConcatenatedNameValuePairs] [nvarchar](max) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_Notes]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_Notes](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_Notes_CaseNumber]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber](
	[CaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_Clients]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_Clients](
	[connid] [int] NULL,
	[Id] [int] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Abbreviation] [nvarchar](80) NOT NULL,
	[ParentClientId] [int] NULL,
	[firstLine] [nvarchar](80) NULL,
	[PostCode] [varchar](12) NOT NULL,
	[Address] [nvarchar](500) NULL,
	[CountryId] [nvarchar](100) NULL,
	[RegionId] [int] NULL,
	[Brand] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CaseTypeId] [int] NOT NULL,
	[AbbreviationName] [nvarchar](80) NOT NULL,
	[BatchRefPrefix] [varchar](50) NULL,
	[ContactName] [nvarchar](256) NULL,
	[ContactAddress] [nvarchar](500) NULL,
	[OfficeId] [int] NOT NULL,
	[PaymentDistributionId] [int] NOT NULL,
	[PriorityCharges] [varchar](50) NOT NULL,
	[LifeExpectancy] [int] NOT NULL,
	[FeeCap] [decimal](18, 4) NOT NULL,
	[minFees] [decimal](18, 4) NOT NULL,
	[MaximumArrangementLength] [int] NULL,
	[ClientPaid] [bit] NOT NULL,
	[OfficerPaid] [bit] NOT NULL,
	[permitArrangement] [bit] NOT NULL,
	[NotifyAddressChange] [bit] NOT NULL,
	[Reinstate] [bit] NOT NULL,
	[AssignMatches] [bit] NOT NULL,
	[showNotes] [bit] NOT NULL,
	[showHistory] [bit] NOT NULL,
	[IsDirectHoldWithLifeExtPastExpiryAllowed] [bit] NOT NULL,
	[xrefEmail] [varchar](512) NULL,
	[PaymentRunFrequencyId] [int] NULL,
	[InvoiceRunFrequencyId] [int] NULL,
	[commissionInvoiceFrequencyId] [int] NULL,
	[isSuccessfulCompletionFee] [bit] NOT NULL,
	[successfulCompletionFee] [decimal](18, 4) NOT NULL,
	[completionFeeType] [bit] NOT NULL,
	[feeInvoiceFrequencyId] [int] NULL,
	[standardReturn] [bit] NOT NULL,
	[paymentMethod] [int] NULL,
	[chargingScheme] [varchar](50) NOT NULL,
	[paymentScheme] [varchar](50) NOT NULL,
	[enforceAtNewAddress] [bit] NOT NULL,
	[defaultHoldTimePeriod] [int] NOT NULL,
	[addressChangeEmail] [varchar](512) NULL,
	[addressChangeStage] [varchar](150) NULL,
	[newAddressReturnCode] [int] NULL,
	[autoReturnExpiredCases] [bit] NOT NULL,
	[generateBrokenLetter] [bit] NOT NULL,
	[useMessageExchange] [bit] NOT NULL,
	[costCap] [decimal](18, 4) NOT NULL,
	[includeUnattendedReturns] [bit] NOT NULL,
	[assignmentSchemeCharge] [int] NULL,
	[expiredCasesAssignable] [bit] NOT NULL,
	[useLocksmithCard] [bit] NOT NULL,
	[EnableAutomaticLinking] [bit] NOT NULL,
	[linkedCasesMoneyDistribution] [int] NOT NULL,
	[isSecondReferral] [bit] NOT NULL,
	[PermitGoodsRemoved] [bit] NOT NULL,
	[EnableManualLinking] [bit] NOT NULL,
	[PermitVRM] [bit] NOT NULL,
	[IsClientInvoiceRunPermitted] [bit] NOT NULL,
	[IsNegativeRemittancePermitted] [bit] NOT NULL,
	[ContactCentrePhoneNumber] [varchar](50) NULL,
	[AutomatedLinePhoneNumber] [varchar](50) NULL,
	[TraceCasesViaWorkflow] [bit] NOT NULL,
	[IsTecAuthorizationApplicable] [bit] NOT NULL,
	[TecDefaultHoldPeriod] [int] NOT NULL,
	[MinimumDaysRemainingForCoa] [int] NOT NULL,
	[TecMinimumDaysRemaining] [int] NOT NULL,
	[IsDecisioningViaWorkflowUsed] [bit] NOT NULL,
	[AllowReverseFeesOnPrimaryAddressChanged] [bit] NOT NULL,
	[IsClientInformationExchangeScheme] [bit] NOT NULL,
	[ClientLifeExpectancy] [int] NULL,
	[DaysToExtendCaseDueToTrace] [int] NULL,
	[DaysToExtendCaseDueToArrangement] [int] NULL,
	[IsWelfareReferralPermitted] [bit] NOT NULL,
	[IsFinancialDifficultyReferralPermitted] [bit] NOT NULL,
	[IsReturnCasePrematurelyPermitted] [bit] NOT NULL,
	[CaseAgeForTBTPEnquiry] [int] NULL,
	[PermitDvlaEnquiries] [bit] NOT NULL,
	[IsAutomaticDvlaRecheckEnabled] [bit] NOT NULL,
	[DaysToAutomaticallyRecheckDvla] [int] NULL,
	[IsComplianceFeeAutoReversed] [bit] NOT NULL,
	[LetterActionTemplateRequiredForCoaInDataCleanseId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInComplianceId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInEnforcementId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInCaseMonitoringId] [int] NULL,
	[CategoryRequiredForCoaInDataCleanseId] [int] NULL,
	[CategoryRequiredForCoaInComplianceId] [int] NULL,
	[CategoryRequiredForCoaInEnforcementId] [int] NULL,
	[CategoryRequiredForCoaInCaseMonitoringId] [int] NULL,
	[IsCoaWithActiveArrangementAllowed] [bit] NOT NULL,
	[ReturnCap] [int] NULL,
	[CurrentReturnCap] [int] NULL,
	[IsLifeExpectancyForCoaEnabled] [bit] NOT NULL,
	[LifeExpectancyForCoa] [int] NOT NULL,
	[ExpiredCaseReturnCode] [int] NULL,
	[WorkflowName] [varchar](255) NULL,
	[OneOffArrangementTolerance] [int] NULL,
	[DailyArrangementTolerance] [int] NULL,
	[TwoDaysArrangementTolerance] [int] NULL,
	[WeeklyArrangementTolerance] [int] NULL,
	[FortnightlyArrangementTolerance] [int] NULL,
	[ThreeWeeksArrangementTolerance] [int] NULL,
	[FourWeeksArrangementTolerance] [int] NULL,
	[MonthlyArrangementTolerance] [int] NULL,
	[ThreeMonthsArrangementTolerance] [int] NULL,
	[PDAPaymentMethodId] [varchar](50) NOT NULL,
	[Payee] [varchar](150) NULL,
	[IsDirectPaymentValid] [bit] NOT NULL,
	[IsDirectPaymentValidForAutomaticallyLinkedCases] [bit] NOT NULL,
	[IsChargeConsidered] [bit] NOT NULL,
	[Interest] [bit] NOT NULL,
	[InvoicingCurrencyId] [int] NOT NULL,
	[RemittanceCurrencyId] [int] NOT NULL,
	[IsPreviousAddressToBePrimaryAllowed] [bit] NOT NULL,
	[TriggerCOA] [bit] NOT NULL,
	[ShouldOfficerRemainAssigned] [bit] NOT NULL,
	[ShouldOfficerRemainAssignedForEB] [bit] NOT NULL,
	[PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun] [bit] NOT NULL,
	[AllowWorkflowToBlockProcessingIfLinkedToGANTCase] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[AreaId] [int] NULL,
	[CourtId] [int] NULL,
	[IsReturnForAPartPaidCasePermitted] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OnestepTesting_CN]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OnestepTesting_CN](
	[CaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_AdditionalCaseData] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterEmails] ADD  DEFAULT (getdate()) FOR [UploadedOn]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterEmails] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  DEFAULT (getdate()) FOR [UploadedOn]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_Notes] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_Onestep_Clients] ADD  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_OnestepTesting_CN] ADD  DEFAULT ((0)) FOR [Imported]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
--CREATE PROCEDURE [oso].[usp_CheckOSClientName]
--@Name nvarchar (250),
--@CaseType nvarchar (250),
--@Brand int

--AS
--BEGIN
--	SET NOCOUNT ON;
--    BEGIN

--		DECLARE @Id INT;		
--		DECLARE @OSClientName varchar(500)
--		Set @OSClientName = @Name + ' ' + @CaseType
		
--		Select @Id = c.Id	  
--	   from dbo.Clients c 
--		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
--		where 
--		c.[name] = @OSClientName
--		AND cct.[BrandId] = @Brand

--		Select 
--			CASE
--				WHEN @Id is null THEN 1
--				ELSE 0
--			END AS 	Valid

--    END
--END
--GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName_UAT]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetAreasXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_GetAreasXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		--SELECT DISTINCT	
		--	[a].[Id] [AreaId],
		--	[a].[Name] [Area]		
		--FROM
		--	[dbo].[Regions] [r]
		--	INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
		--	INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		SELECT 
			[Id] [AreaId], 
			[Name] [Area] 
		FROM 
			[Regions] 
		WHERE 
			[ParentId] IN (select [Id] from [Regions] where [ParentId] IS NULL)
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetCourtsXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetCourtsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT DISTINCT	
			[c].[Id] [CourtId],
			[c].[Name] [Court]	
		FROM
			[dbo].[Regions] [r]
			INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
			INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		--select Id CourtId, Name Court from Regions where ParentId in (select Id from Regions where ParentId in (select Id from Regions where ParentId is null))
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetRegionsXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetRegionsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

	SELECT 
		[r].[Id] [RegionId],	
		[r].[Name] [Region]
		
	FROM
		[dbo].[Regions] [r]
	WHERE
		[r].[ParentId] IS NULL

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_Clients_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_HMCTS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						,CourtId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						,@RegionId 
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  ,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							--SELECT @PriorityChargeId = sc.id
							--FROM 
							--	clientcasetype cct
							--	INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
							--	INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
							--	INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							--WHERE 
							--	cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							--INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							--VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS__BU_DefaulterPhones_DC_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS__BU_DefaulterPhones_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[Staging_OS_BU_DefaulterPhones]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AdditionalCaseData_Staging_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_AdditionalCaseData_Staging_DT] 
AS 
  BEGIN 
	  SET NOCOUNT ON;
      DECLARE @CaseId VARCHAR(10); 
      DECLARE @Pairs NVARCHAR(MAX); 
      DECLARE @MultipleNameValuePairs TABLE 
        ( 
           caseId     VARCHAR(10), 
           namevaluepairs NVARCHAR(MAX) 
        ) 

      INSERT INTO @MultipleNameValuePairs 
                  (caseId, 
                   namevaluepairs) 
      SELECT CaseId, 
             NameValuePairs 
      FROM   oso.Staging_OS_AdditionalCaseData 
      WHERE  ErrorId IS NULL AND Imported = 0

      DECLARE namevaluepairs CURSOR fast_forward FOR 
        SELECT caseId, 
               namevaluepairs 
        FROM   @MultipleNameValuePairs 

      OPEN namevaluepairs 

      FETCH next FROM namevaluepairs INTO @CaseId, @Pairs 

      WHILE @@FETCH_STATUS = 0 
        BEGIN 
            BEGIN try 
                BEGIN TRANSACTION 

				IF (@Pairs LIKE '%;from%')
				BEGIN
					SET @Pairs = REVERSE(SUBSTRING(REVERSE(@Pairs), CHARINDEX(';', REVERSE(@Pairs)), LEN(@Pairs)))
				END

				
                IF( @CaseId IS NOT NULL ) 
                  BEGIN
                    DECLARE @Name NVARCHAR(MAX) 
                    DECLARE @Value NVARCHAR(MAX) 
							
					--If name value pairs have more than one set of name values, consider them for case note insertion, otherwise additional case data
					IF((SELECT Count(value) FROM dbo.Fnsplit((@Pairs), ':')) > 2) 
						BEGIN 
							DECLARE @CaseNoteExists INT
							Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @Pairs
							IF (@CaseNoteExists = 0)
							BEGIN
								INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
								SELECT @CaseId, 
										NULL, 
										2, 
										Rtrim(Ltrim(@Pairs)), 
										Getdate(), 
										1, 
										NULL, 
										NULL 
							END
						END 
						ELSE 
						BEGIN 
								
							IF OBJECT_ID('tempdb..#NameValue') IS NOT NULL
								DROP TABLE #NameValue

							CREATE TABLE #NameValue 
								( 
									id   INT IDENTITY(1, 1) NOT NULL, 
									word NVARCHAR(MAX) 
								) 

							INSERT INTO #NameValue (word) 
							SELECT value 
							FROM   dbo.Fnsplit(@Pairs, ':') 
							
							SET @Name = (SELECT Rtrim(Ltrim(word)) 
							FROM   #NameValue 
							WHERE  id = 1 )

							SET @Value = (SELECT Rtrim(Ltrim(word)) 
							FROM   #NameValue 
							WHERE  id = 2 )

							SET @Value = REPLACE(@Value,';','')
							DECLARE @Counter INT = (SELECT Count(*) 
								FROM   dbo.AdditionalCaseData 
								WHERE  NAME = @Name 
										AND caseid = @CaseId) 
						
							IF( @Counter > 0 ) 
								BEGIN 
									UPDATE dbo.AdditionalCaseData 
									SET    value = Rtrim(Ltrim(@Value)), LoadedOn = GETDATE()
									WHERE  caseid = @CaseId 
											AND NAME = @Name 
								END 
						ELSE 
						BEGIN
							INSERT INTO dbo.AdditionalCaseData 
											(caseid, 
											NAME, 
											value,
											LoadedOn) 
								VALUES      (@CaseId, 
											@Name, 
											Rtrim(Ltrim(@Value)),
											GETDATE()) 
							END 

						END
						
                      UPDATE [oso].[Staging_OS_AdditionalCaseData] 
                      SET    imported = 1, 
                             importedon = Getdate() 
                      WHERE  CaseId = @CaseId 
                             AND NameValuePairs LIKE '%'+ @Pairs +'%'
                             AND (Imported IS NULL OR Imported = 0)
				END
                COMMIT 
            END try 

            BEGIN catch 
                ROLLBACK TRANSACTION 

                INSERT INTO oso.[sqlerrors] 
                VALUES      (Error_number(), 
                             Error_severity(), 
                             Error_state(), 
                             Error_procedure(), 
                             Error_line(), 
                             Error_message(), 
                             Getdate() ) 

                DECLARE @ErrorId INT = Scope_identity(); 

                UPDATE [oso].[Staging_OS_AdditionalCaseData] 
                SET    errorid = @ErrorId 
                WHERE  CaseId = @CaseId 
                       AND namevaluepairs = @Pairs 
                       AND imported = 0 

                SELECT 0 AS Succeed 
            END catch 

            FETCH next FROM namevaluepairs INTO @CaseId, @Pairs 
        END 

      CLOSE namevaluepairs 

      DEALLOCATE namevaluepairs 
  END 
GO

/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterEmails_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterEmails_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@EmailAddress varchar(250) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDE')>=-1
		BEGIN
			DEALLOCATE cursorDE
		END

        DECLARE cursorDE CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			EmailAddress
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterEmails]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDE
        FETCH NEXT
        FROM
              cursorDE
        INTO
			@EmailAddress
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterEmails] 
					(
						[DefaulterId], 
						[Email], 
						[DateLoaded], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@EmailAddress,
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDE
            INTO
				@EmailAddress
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDE
        DEALLOCATE cursorDE



	SELECT @Succeed

END
END


/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 18/01/2021 10:25:48 ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterPhones_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@TypeID int ,
			@TelephoneNumber varchar(50) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDP')>=-1
		BEGIN
			DEALLOCATE cursorDP
		END

        DECLARE cursorDP CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			TypeID
			,TelephoneNumber
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterPhones]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDP
        FETCH NEXT
        FROM
              cursorDP
        INTO
			@TypeID
			,@TelephoneNumber 
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterPhones] 
					(
						[defaulterId], 
						[phone], 
						[TypeId],		 
						[LoadedOn], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@TelephoneNumber,
						@TypeID,		
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDP
            INTO
				@TypeID
				,@TelephoneNumber 
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDP
        DEALLOCATE cursorDP



	SELECT @Succeed

END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]  
AS  
  
BEGIN  
SET NOCOUNT ON;  
IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL  
    DROP TABLE #CaseNotes  
  
CREATE TABLE #CaseNotes  
(  
    [Id] INT IDENTITY(1,1)  NOT NULL,   
    [CaseId] [INT] NOT NULL,  
 [CUserId] [int] NULL,  
 [COfficerId] [int] NULL,  
 [Text] [nvarchar](max) NOT NULL,  
 [Occurred] [datetime] NOT NULL,  
 [Imported] [bit] NULL,  
 [ImportedOn] [datetime] NULL  
)  
  
INSERT into #CaseNotes([CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])  
SELECT [CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]  
      FROM oso.Staging_OS_NewCase_Notes_CaseNumber WHERE Imported = 0 AND ErrorId IS NULL  
       
  DECLARE @CaseNoteID INT;    
  DECLARE @CaseId INT;  
  DECLARE @CaseNumber varchar(10);  
  DECLARE @StringVal NVARCHAR(MAX);  
   
  DECLARE CaseNotes CURSOR FAST_FORWARD FOR  
  SELECT Id, CaseId ,Text  
  FROM   #CaseNotes    
   
  OPEN CaseNotes  
  FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @CaseId , @StringVal  
   
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
  BEGIN TRY  
  BEGIN TRANSACTION  
  --SELECT @CaseId = c.Id   FROM  Cases c   
  --WHERE C.caseNumber = @CaseNumber  
  --Select @CaseId as caseiD  
  IF(@CaseId IS NOT NULL)  
  BEGIN   
   DECLARE @TempNotes TABLE (  
     Content NVARCHAR(2000),   
     Id int  
    )  
     
   INSERT INTO @TempNotes  
   EXEC [oso].[usp_SplitString] @StringVal, 2000, 0;  
  
   ----Remove spaces.  
   Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content)   
  
   INSERT INTO dbo.[CaseNotes]   
     ( [caseId]  
      , [userId]  
      , [officerId]  
      , [text]  
      , [occured]  
      , [visible]  
      , [groupId]  
      , [TypeId]  
     )  
     SELECT  
      @CaseId,  
      2, -- System admin  
      NULL,       
      s.Content,  
      GETDATE(),  
      1,  
      NULL,  
      NULL  
     FROM  
      @TempNotes S  
       
   DELETE FROM @TempNotes  
   UPDATE [oso].[Staging_OS_NewCase_Notes_CaseNumber]  
     SET Imported = 1, ImportedOn = GetDate()  
     WHERE CaseId = @CaseId and Imported = 0  
  
  END  
  COMMIT  
  END TRY  
  BEGIN CATCH  
  ROLLBACK TRANSACTION  
    INSERT INTO oso.[SQLErrors] VALUES  
       (Error_number()  
         , Error_severity()  
         , Error_state()  
         , Error_procedure()  
         , Error_line()  
         , Error_message()  
         , Getdate()  
       )  
  
     DECLARE @ErrorId INT = SCOPE_IDENTITY();  
  
     UPDATE  
     [oso].[Staging_OS_NewCase_Notes_CaseNumber]  
     SET    ErrorId = @ErrorId  
     WHERE CaseId = @CaseId and Imported = 0  
  
    SELECT 0 as Succeed  
  END CATCH   
    
  
  FETCH NEXT FROM CaseNotes INTO  @CaseNoteID, @CaseId , @StringVal    
  END  
  CLOSE CaseNotes  
  DEALLOCATE CaseNotes   
END 
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]
AS

BEGIN

IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL
    DROP TABLE #CaseNotes

CREATE TABLE #CaseNotes
(
    [Id] INT IDENTITY(1,1)  NOT NULL, 
    [ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
)

INSERT into #CaseNotes([ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])
SELECT [ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]
					 FROM oso.Staging_OS_NewCase_Notes WHERE Imported = 0 AND ErrorId IS NULL

		DECLARE @CaseNoteID INT;		
		DECLARE @CaseId INT;
		DECLARE @ClientCaseReference varchar(200);
		DECLARE @ClientId INT;
		DECLARE @StringVal NVARCHAR(MAX);
 
		DECLARE CaseNotes CURSOR FAST_FORWARD FOR
		SELECT Id, ClientCaseReference , ClientId ,Text
		FROM   #CaseNotes		
 
		OPEN CaseNotes
		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal
 
		WHILE @@FETCH_STATUS = 0
		BEGIN
  BEGIN TRY
  BEGIN TRANSACTION
		SET @CaseId = 0
		SELECT @CaseId = c.Id   FROM  Cases c 
		INNER JOIN  Batches b  ON c.batchid = b.id
        INNER JOIN  ClientCaseType CT ON  b.clientCaseTypeId = CT.Id
        WHERE CT.clientId = @ClientId 
		AND C.clientCaseReference = @ClientCaseReference
		
		IF(@CaseId IS NOT NULL AND @CaseId <>0)
		BEGIN 
			DECLARE @TempNotes TABLE (
					Content NVARCHAR(2000), 
					Id int
				)

			INSERT INTO @TempNotes
			EXEC [oso].[usp_SplitString] @StringVal, 1950;

			--Remove spaces.
			Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content) 

			INSERT INTO dbo.[CaseNotes] 
					( [caseId]
						, [userId]
						, [officerId]
						, [text]
						, [occured]
						, [visible]
						, [groupId]
						, [TypeId]
					)
					SELECT
						@CaseId,
						2, -- System admin
						NULL,					
						S.Content,
						GETDATE(),
						1,
						NULL,
						NULL
					FROM
						@TempNotes S

			UPDATE [oso].[Staging_OS_NewCase_Notes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0
			
			DELETE FROM @TempNotes
		END
  COMMIT
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )

					DECLARE @ErrorId INT = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_NewCase_Notes]
					SET    ErrorId = @ErrorId
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0

				SELECT 0 as Succeed
  END CATCH	
  

		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal		
		END
		CLOSE CaseNotes
		DEALLOCATE CaseNotes	
END 
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_GetClientsXMapping_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Cases_GetClientsXMapping_UAT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			c.[ConnId]
			,c.[CClientId] as ClientId
			,c.[ClientName]
			,CASE
			WHEN ct.name ='Council Tax Liability Order' THEN 'CT'
			WHEN ct.name ='National Non Domestic Rates' THEN 'ND'
			WHEN ct.name ='RTA/TMA (RoadTraffic/Traffic Management)' THEN 'PC'
			WHEN ct.name ='Child Maintenance Group' THEN 'CS'
		END AS OffenceCode

		FROM [oso].[OSColClentsXRef] c 
		INNER JOIN dbo.ClientCaseType cct ON c.CClientId = cct.clientId
		INNER JOIN dbo.CaseType ct ON cct.caseTypeId = ct.Id
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DC_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM oso.[stg_Onestep_Clients]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DC_DT_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DC_DT_UAT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM oso.[stg_Onestep_Clients]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					--@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,
					@IsReturnForAPartPaidCasePermitted		bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						--,RegionId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,IsReturnForAPartPaidCasePermitted

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						--,@RegionId
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,@IsReturnForAPartPaidCasePermitted
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  --,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									--,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
									,PermitReturnForPartPaidCase
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
										,@IsReturnForAPartPaidCasePermitted
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							--		Comment this section when PriorityCharge is null or empty

							SELECT @PriorityChargeId = sc.id
							FROM 
								clientcasetype cct
								INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
								INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
								INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							WHERE 
								cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							--		Comment the section above when PriorityCharge is null or empty

							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									--,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									,@IsReturnForAPartPaidCasePermitted
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_GetClientId]
@ClientName nvarchar (250),
@CaseType nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @OSClientName nvarchar(500)
		Set @OSClientName = @ClientName + ' (' + @CaseType + ')'
		
		Select top 1 c.Id	  
	   from Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @OSClientName
		AND cct.[abbreviationName] = @CaseType
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rituraj Choudhury
-- Create date: 11-08-2020
-- Description:	Extract CaseNumber, CaseId and ClientCaseReference using ClientName
-- =============================================
Create PROCEDURE [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_UAT]
@ClientName nvarchar (max)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CaseId, c.ClientCaseReference, c.caseNumber as CaseNumber	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseNumberXMapping_using_CaseId_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rituraj Choudhury
-- Create date: 11-08-2020
-- Description:	Extract CaseNumber, CaseId and ClientCaseReference using CaseId
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseNumberXMapping_using_CaseId_UAT]
@CaseIdList nvarchar (max)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CaseId, c.ClientCaseReference, c.caseNumber as CaseNumber	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
		    c.Id in (Select * from dbo.udf_ConvertStringListToTable(@CaseIdList))
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMappingByClientId]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCasesXMappingByClientId]
@ClientId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, c.ClientCaseReference, c.ClientDefaulterReference, c.CaseNumber as cCaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cl.Id = @ClientId
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference,'YYYYYYYYYYYYYY' AS ClientDefaulterReference,'ZZZZZZZ' AS cCaseNumber
		
		ORDER BY c.Id DESC

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseTypeXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseTypeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	CaseType
	,CaseTypeId
  FROM [oso].[OSColCaseTypeXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientParentXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientParentXMapping_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping_UAT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientsXMapping]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[ConnId]
	,[CClientId]
	,[ClientName]
  FROM [oso].[OSColClentsXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientsXMapping_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientsXMapping_UAT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[ConnId]
	,[CClientId]
	,[ClientName]
  FROM [oso].[OSColClentsXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterIdByCDR]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDefaulterIdByCDR] @DefaulterName nvarchar(150), @CCaseId int
AS
BEGIN
	select Top 1 [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	where [d].[name] = @DefaulterName and c.id = @CCaseId
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedEmailAddress]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedEmailAddress] @cDefaulterId int, @EmailAddress varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], de.Email as cEmailAddress
	from [Defaulters] [d]
	Inner Join DefaulterEmails de on d.Id = de.defaulterId
	where [d].[Id] = @cDefaulterId
	AND de.Email = @EmailAddress
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedPhoneNo]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedPhoneNo] @cDefaulterId int, @TelephoneNumber varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], dp.phone as cTelephoneNumber
	from [Defaulters] [d]
	Inner Join DefaulterPhones dp on d.Id = dp.defaulterId
	where [d].[Id] = @cDefaulterId
	AND dp.phone = @TelephoneNumber
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetIdByClientNameTest]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetIdByClientNameTest] @ClientName nvarchar(150)
AS
BEGIN
	select Top 1 [d].[Id] [test]
	from [clients] [d]
	where [d].[name] = @ClientName and d.isactive = 1
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT] 
AS 
  BEGIN 
	SET NOCOUNT ON;
    CREATE TABLE #nvpairs 
                 ( 
                              [Id] int IDENTITY(1, 1) NOT NULL, 
                              [ClientCaseReference] [varchar](200) NOT NULL, 
                              [ClientId] [int] NOT NULL, 
                              [nvpString] [nvarchar](max) NOT NULL, 
                              [concatenatednvpString] nvarchar(max) NULL 
                 ) 
    INSERT INTO #nvpairs 
    SELECT clientcasereference, 
           clientid, 
           namevaluepairs             AS nvpstring, 
           concatenatednamevaluepairs AS concatenatednvpstring 
    FROM   oso.staging_os_newcase_additionalcasedata 
    WHERE  imported = 0 
    AND    errorid IS NULL 


    DECLARE @NVPID                 INT; 
    DECLARE @CaseId                INT; 
    DECLARE @CCRef                 VARCHAR(200); 
    DECLARE @ClId                  INT; 
    DECLARE @StringVal             NVARCHAR(max); 
    DECLARE @concatenatednvpString NVARCHAR(max); 
    DECLARE nvp CURSOR fast_forward FOR 
    SELECT id, 
           clientcasereference, 
           clientid, 
           nvpstring, 
           concatenatednvpstring 
    FROM   #nvpairs 
    OPEN nvp 
    FETCH next 
    FROM  nvp 
    INTO  @NVPID, 
          @CCRef, 
          @ClId, 
          @StringVal, 
          @concatenatednvpString 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		SET @CaseId = 0
        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClId 
        AND        c.clientcasereference = @CCRef 
		
		--debug
		--Select @CaseId, @ClId, @CCRef

        IF( @CaseId IS NOT NULL AND @CaseId <>0 ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                                             ( 
												id            INT IDENTITY(1, 1) NOT NULL,
												namevaluepair NVARCHAR(max),
												namevalueid   INT
                                             ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                                  namevaluepair, 
                                  namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @StringVal, 
            251, 
            0; 

			DELETE FROM #SingleNameValuePair where namevaluepair = ''
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
              
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
              
			  SET @Value = REPLACE(@Value,';','')
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 

			  IF Object_id('tempdb..#NameValue') IS NOT NULL 
			  DROP TABLE #namevalue

            END 
            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @CCRef 
          AND    clientid = @ClId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @CCRef 
        AND    clientid = @ClId 
        --AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @StringVal 
        SELECT 0 AS succeed 
      END catch 
      FETCH next 
      FROM  nvp 
      INTO  @NVPID, 
            @CCRef, 
            @ClId, 
            @StringVal, 
            @concatenatednvpString 
    END 
    CLOSE nvp 
    DEALLOCATE nvp 
  END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_V2]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_V2] 
AS 
  BEGIN 
	SET NOCOUNT ON;
    --CREATE TABLE #nvpairs 
    --             ( 
    --                          [Id] int IDENTITY(1, 1) NOT NULL, 
    --                          [ClientCaseReference] [varchar](200) NOT NULL, 
    --                          [ClientId] [int] NOT NULL, 
    --                          [nvpString] [nvarchar](max) NOT NULL, 
    --                          [concatenatednvpString] nvarchar(max) NULL 
    --             ) 
    --INSERT INTO #nvpairs 
    --SELECT clientcasereference, 
    --       clientid, 
    --       namevaluepairs             AS nvpstring, 
    --       concatenatednamevaluepairs AS concatenatednvpstring 
    --FROM   oso.staging_os_newcase_additionalcasedata 
    --WHERE  imported = 0 
    --AND    errorid IS NULL 


    --DECLARE @NVPID                 INT; 
    --DECLARE @CaseId                INT; 
    --DECLARE @CCRef                 VARCHAR(200); 
    --DECLARE @ClId                  INT; 
    --DECLARE @StringVal             NVARCHAR(max); 
    --DECLARE @concatenatednvpString NVARCHAR(max); 
    --DECLARE nvp CURSOR fast_forward FOR 
    --SELECT id, 
    --       clientcasereference, 
    --       clientid, 
    --       nvpstring, 
    --       concatenatednvpstring 
    --FROM   #nvpairs 
    --OPEN nvp 
    --FETCH next 
    --FROM  nvp 
    --INTO  @NVPID, 
    --      @CCRef, 
    --      @ClId, 
    --      @StringVal, 
    --      @concatenatednvpString 
    --WHILE @@FETCH_STATUS = 0 

	WHILE Exists(Select Top 1 * From oso.staging_os_newcase_additionalcasedata 
							WHERE  imported = 0 
							AND    errorid IS NULL )
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		
		DECLARE @ClientCaseReference [varchar](200);
		DECLARE @ClientId [int];
		DECLARE @nvpString [nvarchar](max);
		DECLARE @concatenatednvpString nvarchar(max);
		DECLARE @CaseId INT; 

		SELECT @ClientCaseReference = ClientCaseReference, @ClientId = ClientId, @nvpString = NameValuePairs, @concatenatednvpString = ConcatenatedNameValuePairs
		FROM oso.Staging_OS_NewCase_AdditionalCaseData
		WHERE  imported = 0 AND    errorid IS NULL 
		--debug
		Select @ClientId , @ClientCaseReference
        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClientId 
        AND        c.clientcasereference = @ClientCaseReference 
		
		--debug
		Select @CaseId
        IF( @CaseId IS NOT NULL ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                                             ( 
												id            INT IDENTITY(1, 1) NOT NULL,
												namevaluepair NVARCHAR(max),
												namevalueid   INT
                                             ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                                  namevaluepair, 
                                  namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @nvpString, 
            251, 
            0; 
          
		  --debug
		  Select * from #SingleNameValuePair

          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
		   --debug
		   Select @SingleNameValue, @Id
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
				
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
               
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 
            END 

			IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
		  IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
          DROP TABLE #SINGLENAMEVALUEPAIR 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
		  

          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @ClientCaseReference 
          AND    clientid = @ClientId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @ClientCaseReference 
        AND    clientid = @ClientId 
       -- AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @ 
        SELECT 0 AS succeed 
      END catch 
    END 
  END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_Vijay123]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_Vijay123] 
AS 
  BEGIN 
	SET NOCOUNT ON;
    CREATE TABLE #nvpairs 
                 ( 
                              [Id] int IDENTITY(1, 1) NOT NULL, 
                              [ClientCaseReference] [varchar](200) NOT NULL, 
                              [ClientId] [int] NOT NULL, 
                              [nvpString] [nvarchar](max) NOT NULL, 
                              [concatenatednvpString] nvarchar(max) NULL 
                 ) 
    INSERT INTO #nvpairs 
    SELECT clientcasereference, 
           clientid, 
           namevaluepairs             AS nvpstring, 
           concatenatednamevaluepairs AS concatenatednvpstring 
    FROM   oso.staging_os_newcase_additionalcasedata 
    WHERE  imported = 0 
    AND    errorid IS NULL  
	--AND ClientCaseReference ='S6671116'


	CREATE TABLE #InsertRows
	(
	    [Id] int IDENTITY(1, 1) NOT NULL, 
		CONTENT Nvarchar(max),
		isCaseNote bit ,
		caserefeencedefaulter nvarchar(2000),
		caseId int,
		ClientId int
	)


    DECLARE @NVPID                 INT; 
    DECLARE @CaseId                INT; 
    DECLARE @CCRef                 VARCHAR(200); 
    DECLARE @ClId                  INT; 
    DECLARE @StringVal             NVARCHAR(max); 
    DECLARE @concatenatednvpString NVARCHAR(max); 
    --DECLARE nvp CURSOR STATIC FOR
   
   
    --SELECT id, 
    --       clientcasereference, 
    --       clientid, 
    --       nvpstring, 
    --       concatenatednvpstring 
    --FROM   #nvpairs 
    --OPEN nvp 
    --FETCH next 
    --FROM  nvp 
    --INTO  @NVPID, 
    --      @CCRef, 
    --      @ClId, 
    --      @StringVal, 
    --      @concatenatednvpString 
    --WHILE @@FETCH_STATUS = 0 


	DECLARE @Stringcount INT = 1 
          WHILE ( @Stringcount <  (SELECT  COUNT(id) FROM   #nvpairs ))     
           


    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		
        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = (select ClientId from  #nvpairs where  id = @Stringcount)
		        AND   c.clientcasereference = (select clientCaseReference from  #nvpairs where  id = @Stringcount) 
		
        IF( @CaseId IS NOT NULL) 
        BEGIN 
          DECLARE @SINGLENAMEVALUEPAIR TABLE 
                                             ( 
												id            INT IDENTITY(1, 1) NOT NULL,
												namevaluepair NVARCHAR(max),
												namevalueid   INT
                                             ) 

          INSERT INTO @SingleNameValuePair 
                      ( 
                                  namevaluepair, 
                                  namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @StringVal, 
            251, 
            0; 
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   @SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     @SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     @SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   @SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				--DECLARE @CaseNoteExists INT
				--Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				--IF (@CaseNoteExists = 0)
				--BEGIN

				INSERT INTO #InsertRows Select @SingleNameValue, 1 , @CCRef, @CaseId, @ClId
              
               
					-- END
            END 
            ELSE 
            BEGIN 
              IF Object_id('tempdb..#NameValue') IS NOT NULL 
              DROP TABLE #namevalue 
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 
			 

			 INSERT INTO #InsertRows Select @SingleNameValue , 0 ,  @CCRef, @CaseId, @ClId  from #namevalue 
             
            END 
            DELETE 
            FROM   @SingleNameValuePair 
            WHERE  id = @Id 
           -- AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 

          --For concatenatednvpString 
         
         
          ----------------------- 
          --UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          --SET    imported = 1, 
          --       importedon = Getdate() 
          --WHERE  clientcasereference = @CCRef 
          --AND    clientid = @ClId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          --AND    ( 
          --              imported IS NULL 
          --       OR     imported = 0 ) 
        END 
        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @CCRef 
        AND    clientid = @ClId 
        AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @StringVal 
        SELECT 0 AS succeed 
      END catch 

	 -- WAITFOR TIME '0:00:05';
      SET @Stringcount  = @Stringcount  + 1
     
   END

	select * from #InsertRows  

  END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping_CCR_UAT]    Script Date: 19/04/2021 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping_CCR_UAT]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, 
	  CASE 
		WHEN CHARINDEX(';', c.ClientCaseReference) > 0 
			THEN
				LEFT(c.ClientCaseReference, CHARINDEX('_', c.ClientCaseReference)-1) 
			ELSE
				c.ClientCaseReference 
	  END As ClientCaseReference,
	  cl.Id as ClientId, c.issueDate as XIssueDate, 
	  c.addressLine1 as XWarrantAddressLine1, c.addressLine2 as XWarrantAddressLine2,
	  c.addressLine3 as XWarrantAddressLine3, c.addressLine4 as XWarrantAddressLine4, 
	  c.addressLine5 as XWarrantAddressLine5, c.postCode as XWarrantPostcode 
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId, NULL as XIssueDate,
		NULL as XWarrantAddressLine1, NULL as XWarrantAddressLine2,
	  NULL as XWarrantAddressLine3, NULL as XWarrantAddressLine4, 
	  NULL as XWarrantAddressLine5, NULL as XWarrantPostcode 
    END
END

--Select * from cases where caseNumber = 'A731808'
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping_UAT]    Script Date: 19/04/2021 15:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping_UAT]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId, c.issueDate as XIssueDate, 
	  c.addressLine1 as XWarrantAddressLine1, c.addressLine2 as XWarrantAddressLine2,
	  c.addressLine3 as XWarrantAddressLine3, c.addressLine4 as XWarrantAddressLine4, 
	  c.addressLine5 as XWarrantAddressLine5, c.postCode as XWarrantPostcode 
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId, NULL as XIssueDate,
		NULL as XWarrantAddressLine1, NULL as XWarrantAddressLine2,
	  NULL as XWarrantAddressLine3, NULL as XWarrantAddressLine4, 
	  NULL as XWarrantAddressLine5, NULL as XWarrantPostcode 
    END
END

--Select * from cases where caseNumber = 'A731808'
GO
/****** Object:  StoredProcedure [oso].[usp_PopulateValidPostcodeOrDefault]    Script Date: 19/04/2021 15:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_PopulateValidPostcodeOrDefault] @DebtAddressPostcode VARCHAR(50),@AddPostCodeLiable1 VARCHAR(50),@AddPostCodeLiable2 VARCHAR(50)
,@AddPostCodeLiable3 VARCHAR(50),@AddPostCodeLiable4 VARCHAR(50),@AddPostCodeLiable5 VARCHAR(50)

AS

BEGIN
	DECLARE @DefaultPostCode VARCHAR(50) = 'G1 1UG'

	DECLARE @normalizedDebtPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
	DECLARE @normalizedDef1Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
	DECLARE @normalizedDef2Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable2))
	DECLARE @normalizedDef3Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable3))
	DECLARE @normalizedDef4Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable4))
	DECLARE @normalizedDef5Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable5))

	DECLARE @isValidDebtPostcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDebtPostcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef1Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef1Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef2Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef2Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef3Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef3Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef4Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef4Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef5Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef5Postcode, ' '),'AnyCompleteness','AnyType' )


	SELECT
    Case When @isValidDebtPostcode = 0 THEN CASE WHEN (@normalizedDebtPostcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDebtPostcode END AS DebtAddressPostcode,
    Case When @isValidDef1Postcode = 0 THEN CASE WHEN (@normalizedDef1Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef1Postcode END AS AddPostCodeLiable1,
    Case When @isValidDef2Postcode = 0 THEN CASE WHEN (@normalizedDef2Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef2Postcode END AS AddPostCodeLiable2,
    Case When @isValidDef3Postcode = 0 THEN CASE WHEN (@normalizedDef3Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef3Postcode END AS AddPostCodeLiable3,
    Case When @isValidDef4Postcode = 0 THEN CASE WHEN (@normalizedDef4Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef4Postcode END AS AddPostCodeLiable4,
    Case When @isValidDef5Postcode = 0 THEN CASE WHEN (@normalizedDef5Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef5Postcode END AS AddPostCodeLiable5

END
GO
/****** Object:  StoredProcedure [oso].[usp_PopulateValidPostcodeOrDefault_UAT_DeleteME]    Script Date: 19/04/2021 15:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_PopulateValidPostcodeOrDefault_UAT_DeleteME] @DebtAddressPostcode VARCHAR(50),@AddPostCodeLiable1 VARCHAR(50),@AddPostCodeLiable2 VARCHAR(50)
,@AddPostCodeLiable3 VARCHAR(50),@AddPostCodeLiable4 VARCHAR(50),@AddPostCodeLiable5 VARCHAR(50)

AS

BEGIN
	DECLARE @DefaultPostCode VARCHAR(50) = 'G1 1UG'

	DECLARE @normalizedDebtPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
	DECLARE @normalizedDef1Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
	DECLARE @normalizedDef2Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable2))
	DECLARE @normalizedDef3Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable3))
	DECLARE @normalizedDef4Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable4))
	DECLARE @normalizedDef5Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable5))

	DECLARE @isValidDebtPostcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDebtPostcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef1Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef1Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef2Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef2Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef3Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef3Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef4Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef4Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef5Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef5Postcode, ' '),'AnyCompleteness','AnyType' )


	SELECT
    Case When @isValidDebtPostcode = 0 THEN CASE WHEN (@normalizedDebtPostcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDebtPostcode END AS DebtAddressPostcode,
    Case When @isValidDef1Postcode = 0 THEN CASE WHEN (@normalizedDef1Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef1Postcode END AS AddPostCodeLiable1,
    Case When @isValidDef2Postcode = 0 THEN CASE WHEN (@normalizedDef2Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef2Postcode END AS AddPostCodeLiable2,
    Case When @isValidDef3Postcode = 0 THEN CASE WHEN (@normalizedDef3Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef3Postcode END AS AddPostCodeLiable3,
    Case When @isValidDef4Postcode = 0 THEN CASE WHEN (@normalizedDef4Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef4Postcode END AS AddPostCodeLiable4,
    Case When @isValidDef5Postcode = 0 THEN CASE WHEN (@normalizedDef5Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef5Postcode END AS AddPostCodeLiable5

END
GO
/****** Object:  StoredProcedure [oso].[usp_SplitString]    Script Date: 19/04/2021 15:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  VJ
-- Create date: 20-07-2020
-- Description: Split the string to a size and insert the results to CaseNotes table.
-- =============================================

CREATE PROCEDURE  [oso].[usp_SplitString]
(
     @Str NVARCHAR(Max),
	 @Split INT,
	 @RemoveFirstRow bit = 1
)

AS

BEGIN 
DECLARE @End INT
DECLARE @rowNo INT = 1

declare @RowSplitTable table
(
  Id INT IDENTITY(1,1)   NOT NULL, 
  Content Nvarchar(max)
)

declare @FinalRowTable table
(
  Id INT  NOT NULL, 
  Content Nvarchar(max)
)

WHILE (LEN(@Str) > 0)
BEGIN
    IF (LEN(@Str) > @Split)
    BEGIN
        SET @End = LEN(LEFT(@Str, @Split)) - CHARINDEX(' ', REVERSE(LEFT(@Str, @Split)))
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(LEFT(LEFT(@Str, @Split), @End + 1))))
        SET @Str = SUBSTRING(@Str, @End + 2, LEN(@Str))
    END
    ELSE
    BEGIN
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(@Str)))
        SET @Str = ''
    END
END


IF @RemoveFirstRow = 1

BEGIN

INSERT INTO @FinalRowTable
SELECT id, Content FROM (
        SELECT ROW_NUMBER() OVER(ORDER BY id) AS RoNum
              , id , Content
        FROM @RowSplitTable 
) AS tbl 
WHERE @rowNo < RoNum
ORDER BY tbl.Id 

END 
ELSE 
BEGIN 

INSERT INTO @FinalRowTable SELECT *  FROM @RowSplitTable

END


SELECT s.Content,s.Id
FROM @FinalRowTable s

END
GO
