
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientIdByName]    Script Date: 19/04/2021 15:15:43 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetClientIdByName]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetClientIdByName]    Script Date: 19/04/2021 15:15:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 19-04-2021
-- Description:	Extract client id by name
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientIdByName]
@ClientName nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		
		Select top 1 c.Id as ClientId, c.name As ClientName	  
		From Clients c 
		Where c.[name] = @ClientName
		UNION
		SELECT 99999999 AS ClientId, 'XXXXXXXXXXXXXXXX' AS ClientName

    END
END
GO

GRANT SELECT ON SCHEMA::de TO FUTUser
GRANT SELECT,INSERT,DELETE,UPDATE,EXECUTE ON SCHEMA::oso TO FUTUser
GRANT SELECT,INSERT ON SCHEMA::Brands TO FUTUser
