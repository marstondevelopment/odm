﻿
DROP INDEX IF EXISTS [idx_stg_Onestep_Cases_clientcasereference] ON  [oso].[stg_Onestep_Cases]
GO

DROP INDEX IF EXISTS idx_stg_Onestep_Cases_clientid ON  [oso].[stg_Onestep_Cases]
GO

DROP INDEX IF EXISTS idx_stg_Onestep_Cases_OneStepCaseNumber ON  [oso].[stg_Onestep_Cases]
GO

ALTER TABLE [oso].[stg_Onestep_Cases] DROP CONSTRAINT [DF_stg_Onestep_Cases_BatchNoGenerated]
GO

ALTER TABLE [oso].[stg_Onestep_Cases] DROP CONSTRAINT [DF_stg_Onestep_Cases_Imported]
GO

/****** Object:  Table [oso].[stg_Onestep_Cases]    Script Date: 01/03/2021 10:16:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[oso].[stg_Onestep_Cases]') AND type in (N'U'))
DROP TABLE [oso].[stg_Onestep_Cases]
GO

/****** Object:  Table [oso].[stg_Onestep_Cases]    Script Date: 01/03/2021 10:16:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[VehicleVRM] [varchar](7) NULL,
	[VehicleMake] [varchar](50) NULL,
	[VehicleModel] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[Currency] [int] NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[TitleLiable1] [varchar](50) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[MiddlenameLiable2] [nvarchar](50) NULL,
	[FullnameLiable2] [nvarchar](50) NULL,
	[TitleLiable2] [varchar](50) NULL,
	[CompanyNameLiable2] [nvarchar](50) NULL,
	[FirstnameLiable2] [nvarchar](50) NULL,
	[DOBLiable2] [datetime] NULL,
	[LastnameLiable2] [nvarchar](50) NULL,
	[NINOLiable2] [varchar](13) NULL,
	[MinorLiable2] [bit] NULL,
	[Add1Liable2] [nvarchar](80) NULL,
	[Add2Liable2] [nvarchar](320) NULL,
	[Add3Liable2] [nvarchar](100) NULL,
	[Add4Liable2] [nvarchar](100) NULL,
	[Add5Liable2] [nvarchar](100) NULL,
	[AddCountryLiable2] [int] NULL,
	[AddPostCodeLiable2] [varchar](12) NULL,
	[Phone1Liable2] [varchar](50) NULL,
	[Phone2Liable2] [varchar](50) NULL,
	[Phone3Liable2] [varchar](50) NULL,
	[Phone4Liable2] [varchar](50) NULL,
	[Phone5Liable2] [varchar](50) NULL,
	[Email1Liable2] [varchar](250) NULL,
	[Email2Liable2] [varchar](250) NULL,
	[Email3Liable2] [varchar](250) NULL,
	[MiddlenameLiable3] [nvarchar](50) NULL,
	[FullnameLiable3] [nvarchar](150) NULL,
	[TitleLiable3] [varchar](50) NULL,
	[CompanyNameLiable3] [nvarchar](50) NULL,
	[FirstnameLiable3] [nvarchar](50) NULL,
	[DOBLiable3] [datetime] NULL,
	[LastnameLiable3] [nvarchar](50) NULL,
	[NINOLiable3] [varchar](13) NULL,
	[MinorLiable3] [bit] NULL,
	[Add1Liable3] [nvarchar](80) NULL,
	[Add2Liable3] [nvarchar](320) NULL,
	[Add3Liable3] [nvarchar](100) NULL,
	[Add4Liable3] [nvarchar](100) NULL,
	[Add5Liable3] [nvarchar](100) NULL,
	[AddCountryLiable3] [int] NULL,
	[AddPostCodeLiable3] [varchar](12) NULL,
	[Phone1Liable3] [varchar](50) NULL,
	[Phone2Liable3] [varchar](50) NULL,
	[Phone3Liable3] [varchar](50) NULL,
	[Phone4Liable3] [varchar](50) NULL,
	[Phone5Liable3] [varchar](50) NULL,
	[Email1Liable3] [varchar](250) NULL,
	[Email2Liable3] [varchar](250) NULL,
	[Email3Liable3] [varchar](250) NULL,
	[MiddlenameLiable4] [nvarchar](50) NULL,
	[FullnameLiable4] [nvarchar](150) NULL,
	[TitleLiable4] [varchar](50) NULL,
	[CompanyNameLiable4] [nvarchar](50) NULL,
	[FirstnameLiable4] [nvarchar](50) NULL,
	[DOBLiable4] [datetime] NULL,
	[LastnameLiable4] [nvarchar](50) NULL,
	[NINOLiable4] [varchar](13) NULL,
	[MinorLiable4] [bit] NULL,
	[Add1Liable4] [nvarchar](80) NULL,
	[Add2Liable4] [nvarchar](320) NULL,
	[Add3Liable4] [nvarchar](100) NULL,
	[Add4Liable4] [nvarchar](100) NULL,
	[Add5Liable4] [nvarchar](100) NULL,
	[AddCountryLiable4] [int] NULL,
	[AddPostCodeLiable4] [varchar](12) NULL,
	[Phone1Liable4] [varchar](50) NULL,
	[Phone2Liable4] [varchar](50) NULL,
	[Phone3Liable4] [varchar](50) NULL,
	[Phone4Liable4] [varchar](50) NULL,
	[Phone5Liable4] [varchar](50) NULL,
	[Email1Liable4] [varchar](250) NULL,
	[Email2Liable4] [varchar](250) NULL,
	[Email3Liable4] [varchar](250) NULL,
	[MiddlenameLiable5] [nvarchar](50) NULL,
	[FullnameLiable5] [nvarchar](150) NULL,
	[TitleLiable5] [varchar](50) NULL,
	[CompanyNameLiable5] [nvarchar](50) NULL,
	[FirstnameLiable5] [nvarchar](50) NULL,
	[DOBLiable5] [datetime] NULL,
	[LastnameLiable5] [nvarchar](50) NULL,
	[NINOLiable5] [varchar](13) NULL,
	[MinorLiable5] [bit] NULL,
	[Add1Liable5] [nvarchar](80) NULL,
	[Add2Liable5] [nvarchar](320) NULL,
	[Add3Liable5] [nvarchar](100) NULL,
	[Add4Liable5] [nvarchar](100) NULL,
	[Add5Liable5] [nvarchar](100) NULL,
	[AddCountryLiable5] [int] NULL,
	[AddPostCodeLiable5] [varchar](12) NULL,
	[Phone1Liable5] [varchar](50) NULL,
	[Phone2Liable5] [varchar](50) NULL,
	[Phone3Liable5] [varchar](50) NULL,
	[Phone4Liable5] [varchar](50) NULL,
	[Phone5Liable5] [varchar](50) NULL,
	[Email1Liable5] [varchar](250) NULL,
	[Email2Liable5] [varchar](250) NULL,
	[Email3Liable5] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[OneStepCaseNumber] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[ConnId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[DefaultersNames] [nvarchar](1000) NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,
	[EmployerName] [nvarchar](150) NULL,
	[EmployerAddressLine1] [nvarchar](80) NULL,
	[EmployerAddressLine2] [nvarchar](320) NULL,
	[EmployerAddressLine3] [nvarchar](100) NULL,
	[EmployerAddressLine4] [nvarchar](100) NULL,
	[EmployerPostcode] [varchar](12) NULL,
	[EmployerEmailAddress] [varchar](50) NULL,
	[AddCountryLiable1] [int] NULL,
	[EmployerFaxNumber] [varchar](50) NULL,
	[EmployerTelephone] [varchar](50) NULL,
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[Welfare] [bit] NULL,
	[DateOfBirth] [datetime] NULL,
	[NINO] [nvarchar](13) NULL,
	[OffenceValue] [decimal](18, 4) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[BatchNoGenerated] [bit] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_Onestep_Cases] ADD  CONSTRAINT [DF_stg_Onestep_Cases_Imported]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [oso].[stg_Onestep_Cases] ADD  CONSTRAINT [DF_stg_Onestep_Cases_BatchNoGenerated]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO

CREATE INDEX idx_stg_Onestep_Cases_clientcasereference
ON [oso].[stg_Onestep_Cases](ClientCaseReference);
GO
CREATE INDEX idx_stg_Onestep_Cases_clientid
ON [oso].[stg_Onestep_Cases](ClientId);
GO
CREATE INDEX idx_stg_Onestep_Cases_OneStepCaseNumber
ON [oso].[stg_Onestep_Cases](OneStepCaseNumber);
GO


/****** Object:  StoredProcedure [oso].[usp_ODM_Phase3_Cases_DT]    Script Date: 23/07/2020 16:21:48 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_ODM_Phase3_Cases_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_Phase3_Cases_DT]    Script Date: 23/07/2020 16:21:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_ODM_Phase3_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_OS_AddBatches;
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@VehicleVRM varchar(7) ,
			@VehicleMake varchar(50) ,
			@VehicleModel varchar(50) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@TitleLiable2 varchar(50) ,
			@FirstnameLiable2 nvarchar(50) ,
			@MiddlenameLiable2 nvarchar(50) ,
			@LastnameLiable2 nvarchar(50) ,
			@FullnameLiable2 nvarchar(50) ,
			@CompanyNameLiable2 nvarchar(50) ,
			@DOBLiable2 datetime ,
			@NINOLiable2 varchar(13) ,
			@MinorLiable2 bit ,
			@Add1Liable2 nvarchar(80) ,
			@Add2Liable2 nvarchar(320) ,
			@Add3Liable2 nvarchar(100) ,
			@Add4Liable2 nvarchar(100) ,
			@Add5Liable2 nvarchar(100) ,
			@AddCountryLiable2 int ,
			@AddPostCodeLiable2 varchar(12) ,
			@Phone1Liable2 varchar(50) ,
			@Phone2Liable2 varchar(50) ,
			@Phone3Liable2 varchar(50) ,
			@Phone4Liable2 varchar(50) ,
			@Phone5Liable2 varchar(50) ,
			@Email1Liable2 varchar(250) ,
			@Email2Liable2 varchar(250) ,
			@Email3Liable2 varchar(250) ,
			@TitleLiable3 varchar(50) ,
			@FirstnameLiable3 nvarchar(50) ,
			@MiddlenameLiable3 nvarchar(50) ,
			@LastnameLiable3 nvarchar(50) ,
			@FullnameLiable3 nvarchar(150) ,
			@CompanyNameLiable3 nvarchar(50) ,
			@DOBLiable3 datetime ,
			@NINOLiable3 varchar(13) ,
			@MinorLiable3 bit ,
			@Add1Liable3 nvarchar(80) ,
			@Add2Liable3 nvarchar(320) ,
			@Add3Liable3 nvarchar(100) ,
			@Add4Liable3 nvarchar(100) ,
			@Add5Liable3 nvarchar(100) ,
			@AddCountryLiable3 int ,
			@AddPostCodeLiable3 varchar(12) ,
			@Phone1Liable3 varchar(50) ,
			@Phone2Liable3 varchar(50) ,
			@Phone3Liable3 varchar(50) ,
			@Phone4Liable3 varchar(50) ,
			@Phone5Liable3 varchar(50) ,
			@Email1Liable3 varchar(250) ,
			@Email2Liable3 varchar(250) ,
			@Email3Liable3 varchar(250) ,
			@TitleLiable4 varchar(50) ,
			@FirstnameLiable4 nvarchar(50) ,
			@MiddlenameLiable4 nvarchar(50) ,
			@LastnameLiable4 nvarchar(50) ,
			@FullnameLiable4 nvarchar(150) ,
			@CompanyNameLiable4 nvarchar(50) ,
			@DOBLiable4 datetime ,
			@NINOLiable4 varchar(13) ,
			@MinorLiable4 bit ,
			@Add1Liable4 nvarchar(80) ,
			@Add2Liable4 nvarchar(320) ,
			@Add3Liable4 nvarchar(100) ,
			@Add4Liable4 nvarchar(100) ,
			@Add5Liable4 nvarchar(100) ,
			@AddCountryLiable4 int ,
			@AddPostCodeLiable4 varchar(12) ,
			@Phone1Liable4 varchar(50) ,
			@Phone2Liable4 varchar(50) ,
			@Phone3Liable4 varchar(50) ,
			@Phone4Liable4 varchar(50) ,
			@Phone5Liable4 varchar(50) ,
			@Email1Liable4 varchar(250) ,
			@Email2Liable4 varchar(250) ,
			@Email3Liable4 varchar(250) ,
			@TitleLiable5 varchar(50) ,
			@FirstnameLiable5 nvarchar(50) ,
			@MiddlenameLiable5 nvarchar(50) ,
			@LastnameLiable5 nvarchar(50) ,
			@FullnameLiable5 nvarchar(150) ,
			@CompanyNameLiable5 nvarchar(50) ,
			@DOBLiable5 datetime ,
			@NINOLiable5 varchar(13) ,
			@MinorLiable5 bit ,
			@Add1Liable5 nvarchar(80) ,
			@Add2Liable5 nvarchar(320) ,
			@Add3Liable5 nvarchar(100) ,
			@Add4Liable5 nvarchar(100) ,
			@Add5Liable5 nvarchar(100) ,
			@AddCountryLiable5 int ,
			@AddPostCodeLiable5 varchar(12) ,
			@Phone1Liable5 varchar(50) ,
			@Phone2Liable5 varchar(50) ,
			@Phone3Liable5 varchar(50) ,
			@Phone4Liable5 varchar(50) ,
			@Phone5Liable5 varchar(50) ,
			@Email1Liable5 varchar(250) ,
			@Email2Liable5 varchar(250) ,
			@Email3Liable5 varchar(250) ,
			@BatchDate datetime ,
			@OneStepCaseNumber int ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(80) ,
			@DefaultersNames nvarchar(1000) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod varchar(20) ,
			@EmployerName nvarchar(150) ,
			@EmployerAddressLine1 nvarchar(80) ,
			@EmployerAddressLine2 nvarchar(320) ,
			@EmployerAddressLine3 nvarchar(100) ,
			@EmployerAddressLine4 nvarchar(100) ,
			@EmployerPostcode varchar(12) ,
			@EmployerEmailAddress varchar(50) ,
			@AddCountryLiable1 int ,
			@EmployerFaxNumber varchar(50) ,
			@EmployerTelephone varchar(50) ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,	
			@Welfare bit,
			@DateOfBirth datetime,
			@NINO nvarchar(13),
			@OffenceValue decimal(18,4),
			--@OffenceNumber nvarchar(1000) ,	



             @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @PaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int





		SET @succeed = 1

        SELECT
               @PaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Paid'

        SELECT
               @ReturnedStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Returned'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			   [ClientCaseReference]
			  ,[ClientDefaulterReference]
			  ,[IssueDate]
			  ,[OffenceCode]
			  ,[OffenceDescription]
			  ,[OffenceLocation]
			  ,[OffenceCourt]
			  ,[DebtAddress1]
			  ,[DebtAddress2]
			  ,[DebtAddress3]
			  ,[DebtAddress4]
			  ,[DebtAddress5]
			  ,[DebtAddressCountry]
			  ,[DebtAddressPostcode]
			  ,[VehicleVRM]
			  ,[VehicleMake]
			  ,[VehicleModel]
			  ,[StartDate]
			  ,[EndDate]
			  ,[TECDate]
			  ,[FineAmount]
			  ,[Currency]
			  ,[MiddlenameLiable1]
			  ,[FullnameLiable1]
			  ,[TitleLiable1]
			  ,[CompanyNameLiable1]
			  ,[FirstnameLiable1]
			  ,[DOBLiable1]
			  ,[LastnameLiable1]
			  ,[NINOLiable1]
			  ,[MinorLiable1]
			  ,[Add1Liable1]
			  ,[Add2Liable1]
			  ,[Add3Liable1]
			  ,[Add4Liable1]
			  ,[Add5Liable1]
			  ,[AddPostCodeLiable1]
			  ,[IsBusinessAddress]
			  ,[Phone1Liable1]
			  ,[Phone2Liable1]
			  ,[Phone3Liable1]
			  ,[Phone4Liable1]
			  ,[Phone5Liable1]
			  ,[Email1Liable1]
			  ,[Email2Liable1]
			  ,[Email3Liable1]
			  ,[MiddlenameLiable2]
			  ,[FullnameLiable2]
			  ,[TitleLiable2]
			  ,[CompanyNameLiable2]
			  ,[FirstnameLiable2]
			  ,[DOBLiable2]
			  ,[LastnameLiable2]
			  ,[NINOLiable2]
			  ,[MinorLiable2]
			  ,[Add1Liable2]
			  ,[Add2Liable2]
			  ,[Add3Liable2]
			  ,[Add4Liable2]
			  ,[Add5Liable2]
			  ,[AddCountryLiable2]
			  ,[AddPostCodeLiable2]
			  ,[Phone1Liable2]
			  ,[Phone2Liable2]
			  ,[Phone3Liable2]
			  ,[Phone4Liable2]
			  ,[Phone5Liable2]
			  ,[Email1Liable2]
			  ,[Email2Liable2]
			  ,[Email3Liable2]
			  ,[TitleLiable3]
			  ,[FirstnameLiable3]
			  ,[MiddlenameLiable3]
			  ,[LastnameLiable3]
			  ,[FullnameLiable3]
			  ,[CompanyNameLiable3]
			  ,[DOBLiable3]
			  ,[NINOLiable3]
			  ,[MinorLiable3]
			  ,[Add1Liable3]
			  ,[Add2Liable3]
			  ,[Add3Liable3]
			  ,[Add4Liable3]
			  ,[Add5Liable3]
			  ,[AddCountryLiable3]
			  ,[AddPostCodeLiable3]
			  ,[Phone1Liable3]
			  ,[Phone2Liable3]
			  ,[Phone3Liable3]
			  ,[Phone4Liable3]
			  ,[Phone5Liable3]
			  ,[Email1Liable3]
			  ,[Email2Liable3]
			  ,[Email3Liable3]
			  ,[TitleLiable4]
			  ,[FirstnameLiable4]
			  ,[MiddlenameLiable4]
			  ,[LastnameLiable4]
			  ,[FullnameLiable4]
			  ,[CompanyNameLiable4]
			  ,[DOBLiable4]
			  ,[NINOLiable4]
			  ,[MinorLiable4]
			  ,[Add1Liable4]
			  ,[Add2Liable4]
			  ,[Add3Liable4]
			  ,[Add4Liable4]
			  ,[Add5Liable4]
			  ,[AddCountryLiable4]
			  ,[AddPostCodeLiable4]
			  ,[Phone1Liable4]
			  ,[Phone2Liable4]
			  ,[Phone3Liable4]
			  ,[Phone4Liable4]
			  ,[Phone5Liable4]
			  ,[Email1Liable4]
			  ,[Email2Liable4]
			  ,[Email3Liable4]
			  ,[TitleLiable5]
			  ,[FirstnameLiable5]
			  ,[MiddlenameLiable5]
			  ,[LastnameLiable5]
			  ,[FullnameLiable5]
			  ,[CompanyNameLiable5]
			  ,[DOBLiable5]
			  ,[NINOLiable5]
			  ,[MinorLiable5]
			  ,[Add1Liable5]
			  ,[Add2Liable5]
			  ,[Add3Liable5]
			  ,[Add4Liable5]
			  ,[Add5Liable5]
			  ,[AddCountryLiable5]
			  ,[AddPostCodeLiable5]
			  ,[Phone1Liable5]
			  ,[Phone2Liable5]
			  ,[Phone3Liable5]
			  ,[Phone4Liable5]
			  ,[Phone5Liable5]
			  ,[Email1Liable5]
			  ,[Email2Liable5]
			  ,[Email3Liable5]
			  ,[BatchDate]
			  ,[OneStepCaseNumber]
			  ,[ClientId]
			  ,[ConnId]
			  ,[ClientName]
			  ,[DefaultersNames]
			  ,[PhaseDate]
			  ,[StageDate]
			  ,[IsAssignable]
			  ,[OffenceDate]
			  ,[CreatedOn]
			  ,[BillNumber]
			  ,[ClientPeriod]
			  ,[EmployerName]
			  ,[EmployerAddressLine1]
			  ,[EmployerAddressLine2]
			  ,[EmployerAddressLine3]
			  ,[EmployerAddressLine4]
			  ,[EmployerPostcode]
			  ,[EmployerEmailAddress]
			  ,[AddCountryLiable1]
			  ,[EmployerFaxNumber]
			  ,[EmployerTelephone]
			  ,[RollNumber]
			  ,[Occupation]
			  ,[BenefitIndicator]
			  ,[CStatus]
			  ,[CReturnCode]
			  ,[OffenceNotes]
			  ,[Welfare]
		      ,[DateOfBirth]
			  ,[NINO]
			  ,[OffenceValue]
			  --,[OffenceNumber]
		FROM
                 oso.[stg_Onestep_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				 -- ,@OffenceNumber
        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						--SET @CurrentBatchId = NULL
						----Select @CurrentBatchId = Id from Batches where batchDate = @BatchDate and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId)
      --                  Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						
						-- Check and update BatchId
						SET @CurrentBatchId = NULL
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						IF (@CurrentBatchId IS NULL)
						BEGIN
							DECLARE @CurrentDate DateTime = GetDate();
							INSERT into batches
								(
								[referenceNumber],
								[batchDate],
								[closedDate],
								[createdBy],
								[manual],
								[deleted],
								[crossReferenceSent],
								[crossReferenceSentDate],
								[clientCaseTypeId],
								[loadedOn],
								[loadedBy],
								[receivedOn],
								[summaryDate]
								)

							SELECT 
								CONCAT(cct.batchrefprefix,CONVERT(varchar(8),@BatchDate,112))[referenceNumber]
								,CAST(CONVERT(date,@BatchDate) AS DateTime) [batchDate]
								,@CurrentDate [closedDate]
								,2 [createdBy]
								,1 [manual]
								,0 [deleted]
								,1 [crossReferenceSent]
								,@CurrentDate [crossReferenceSentDate]
								,cct.id [clientCaseTypeId]
								,@CurrentDate [loadedOn]
								,2 [loadedBy]
								,@CurrentDate [receivedOn]
								,NULL [summaryDate]
							FROM
								clientcasetype cct
							WHERE
								cct.clientid = @ClientId
						
							SET @CurrentBatchId = SCOPE_IDENTITY();

							-- Set the BatchIDGenerated flag to true in staging
							UPDATE
							[oso].[stg_Onestep_Cases]
							SET    BatchNoGenerated = 1
							WHERE
							Imported = 0 
							AND ErrorId is NULL
							AND ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						END

						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT @StageId = (s.id) from clients cli
                        INNER JOIN stages s on s.clientid = cli.id
                        where cli.id = @ClientId
                                and s.Name = 'Return Case'

						IF (@StageId IS NULL)	-- Client is inactive, so look for stageid from Empty Workflow
						BEGIN
						  SET @StageId = (select s.id from clients cli
												INNER JOIN stages s on s.clientid = cli.id
												INNER JOIN StageTemplates st on s.stagetemplateid = st.id
												INNER JOIN PhaseTemplates pt on st.phasetemplateid = pt.id
												INNER JOIN WorkflowTemplates wft on pt.workflowtemplateid = wft.id
												where cli.id = @ClientId
														and (
														(wft.name <> 'Empty Workflow' and s.onload = 1)
														or
														(wft.name = 'Empty Workflow' and s.name = 'Return Case')
														)
										)
						END

						
						SET @CStatusId = @PaidStatusId; -- change this to PaidStatusId
						SET @ReturnRunId = NULL
						IF (@CStatus = 'Returned')
						BEGIN
							SET @CStatusId = @ReturnedStatusId
							SET @ReturnRunId = 1244
						END

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL

						IF (@CaseTypeId = 6)
						BEGIN
							SET @VTECDate = @TECDate
							SET @ANPR = 1
						END

						
						IF (@CaseTypeId = 24)
						BEGIN
							SET @VTECDate = NULL
							SET @ANPR = 0
						END
						
						SET @cnid = (  
									SELECT MIN(cn.Id)  
									FROM CaseNumbers AS cn WITH (  
										XLOCK  
										,HOLDLOCK  
										)  
									WHERE cn.Reserved = 0  
									--AND cn.id > = 13999996
									AND cn.alphaNumericCaseNumber > 'X000000' --Remove this line for LIVE ACTIVE cases.
									)  

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
									--, @FullnameLiable1 --[debtName]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,'') + ISNULL(@DefaultersNames,'')))--[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @ClientPeriod--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						INSERT INTO dbo.OldCaseNumbers (CaseId, OldCaseNumber)
						VALUES (@CaseId, convert(varchar(20), @OneStepCaseNumber)
);
						
					-- 3) Insert Case Offences
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						VALUES
						(
							@CaseId,
							@OffenceDate,
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId,
							@OffenceValue
						);

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DateOfBirth,
						0,
						0,
						NULL,
						0,
						@NINO,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Vehicle
						IF (@CaseTypeId = 6 AND @VehicleVRM IS NOT NULL)
							BEGIN
								INSERT INTO dbo.Vehicles (
									[vrm]
									,[vrmOwner]
									,[defaulterId]
									,[responseStatusId]
									,[responseDate]
									,[responseLoadedDate]
									,[keeperId]
									,[responseStringId]
									,[eventDate]
									,[licenceExpiry]
									,[exportDate]
									,[scrapDate]
									,[theftDate]
									,[recoveryDate]
									,[make]
									,[model]
									,[colour]
									,[taxClass]
									,[seatingCapacity]
									,[previousKeepers]
									,[Motability]
									,[Check]
									,[LastRequestedOn]
									,[isWarrantVrm]
									,[officerId]
									,[VrmReceivedOn]
								)
								VALUES
								(
									@VehicleVRM -- vrm
									,0 -- vrmOwner
									,@LeadDefaulterId
									,NULL -- responseStatusId
									,NULL -- responseDate
									,NULL -- responseLoadedDate
									,NULL -- keeperId
									,NULL -- responseStringId
									,NULL -- eventDate
									,NULL -- licenceExpiry
									,NULL -- exportDate
									,NULL -- scrapDate
									,NULL -- theftDate
									,NULL -- recoveryDate
									,NULL -- make
									,NULL -- model
									,NULL -- colour
									,NULL -- taxCl--s
									,NULL -- seatingCapacity
									,NULL -- previousKeepers
									,0 -- Motability
									,0 -- [Check]
									,NULL -- L--tRequestedOn
									,1 -- isWarrantVrm
									,NULL -- officerId
									,@CreatedOn -- VrmReceivedOn
									);

								SET @VehicleId = SCOPE_IDENTITY();

								INSERT INTO dbo.CaseVehicles
								(
									caseId,
									vehicleId
								)
								VALUES
								(
									@CaseId,
									@VehicleId
								)

							END

					END

					-- Defaulter2
					IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

					END

					-- Defaulter3
					IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			
					-- Defaulter4
					IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END

					-- Defaulter5
					IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			

					-- 5) Insert Contact Addresses


					-- 6) Insert Defaulter Details
					-- 7) Insert Defaulter Cases
					-- 8 ) Insert Vehicles
							-- Note: LeadDefaulterId is required


					-- 9) Insert Case Note
					-- Not required for now


						UPDATE
						[oso].[stg_Onestep_Cases]
						SET    Imported   = 1
						, ImportedOn = GetDate()
						WHERE
						Imported = 0 
						AND OneStepCaseNumber = @OneStepCaseNumber
						AND ClientId = @ClientId
						AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_Onestep_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					OneStepCaseNumber = @OneStepCaseNumber
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				  --,@OffenceNumber

        END
        CLOSE cursorT
        DEALLOCATE cursorT

		
		-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
		BEGIN TRANSACTION	
			UPDATE ca 
			Set TidReviewedId = 1
			from dbo.ContactAddresses ca
			join dbo.defaultercases dc on ca.defaulterid = dc.defaulterid
			join dbo.cases c on dc.caseid = c.id
			INNER join dbo.OldCaseNumbers oc on c.Id = oc.CaseId
			INNER join oso.stg_Onestep_Cases osc on convert(varchar(20), osc.OneStepCaseNumber) = oc.OldCaseNumber
			where 
			osc.Imported = 1 AND
			c.addressline1 = ca.addressline1 AND
			c.postcode = ca.postcode	
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DT]    Script Date: 01/03/2021 13:13:14 ******/
DROP PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DT]    Script Date: 01/03/2021 13:13:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
		INSERT [dbo].[ContactAddresses] 
		(
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		)
		SELECT 
			[addressline1], 
			[addressline1], 
			[addressline2], 
			[addressline2], 
			[addressline3], 
			[addressline4], 
			[addressline5], 
			[Postcode], 
			[cDefaulterId], 
			1, 
			2, 
			231, 
			0 
		FROM 
			[oso].[stg_OneStep_ContactAddresses]
		WHERE 
			(Imported = 0	OR Imported IS NULL)
		EXCEPT
		SELECT 
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		FROM 
			[dbo].[ContactAddresses]

		UPDATE [oso].[stg_OneStep_ContactAddresses]
		SET Imported = 1, ImportedOn = GetDate()
		WHERE (Imported = 0	OR Imported IS NULL)

		COMMIT TRANSACTION	
		SELECT
			1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterPhones_DT]    Script Date: 01/03/2021 14:52:24 ******/
DROP PROCEDURE [oso].[usp_OS_DefaulterPhones_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterPhones_DT]    Script Date: 01/03/2021 14:52:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterPhones] 
			(
				[defaulterId], 
				[phone], 
				[TypeId],		 
				[LoadedOn], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[TelephoneNumber],
				[TypeID],		
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterPhones]
			WHERE 
				(Imported = 0 OR Imported IS NULL)
			EXCEPT
			SELECT 
				[defaulterId], 
				[phone], 
				[TypeId], 		
				[LoadedOn], 
				[SourceId]		
			FROM 
				[dbo].[DefaulterPhones]

			UPDATE [oso].[stg_OneStep_DefaulterPhones]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL)

			COMMIT TRANSACTION	
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterEmails_DT]    Script Date: 01/03/2021 14:59:17 ******/
DROP PROCEDURE [oso].[usp_OS_DefaulterEmails_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterEmails_DT]    Script Date: 01/03/2021 14:59:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterEmails] 
			(
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[EmailAddress],				
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterEmails]
			WHERE 
				(Imported = 0 OR Imported IS NULL	)
			EXCEPT
			SELECT 
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			FROM 
				[dbo].[DefaulterEmails]

			UPDATE [oso].[stg_OneStep_DefaulterEmails]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL	)

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION 
				END

				INSERT INTO oso.[SQLErrors] 
				VALUES (Error_number(), 
						Error_severity(), 
						Error_state(), 
						Error_procedure(), 
						Error_line(), 
						Error_message(), 
						Getdate()) 

				SELECT 0 as Succeed
		END CATCH
	END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Visits_CrossRef_DT]    Script Date: 01/03/2021 15:07:31 ******/
DROP PROCEDURE [oso].[usp_OS_Visits_CrossRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Visits_CrossRef_DT]    Script Date: 01/03/2021 15:07:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			INSERT [dbo].[Visits] 
			(
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			)
			SELECT 
				[import].[CCaseId],
				[import].[COfficerId], 
				[import].[VisitDate],
				[import].[DoorColour],
				[import].[BuildingType],
				[import].[PersonContacted],	
				[import].[ProofOfIdentity],
				[import].[Notes],
				0,
				53.68456,
				-2.2769,
				0.00,
				[import].[VisitDate],
				[import].[VisitDate]
			FROM 		
				[oso].[stg_OneStep_Visits] [import]	
			WHERE 
				Imported = 0 OR Imported IS NULL
			EXCEPT
			SELECT 
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			FROM 
				[dbo].[Visits]

			UPDATE [oso].[stg_OneStep_Visits]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0 OR Imported IS NULL


			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DT]    Script Date: 01/03/2021 15:15:19 ******/
DROP PROCEDURE [oso].[usp_OS_History_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DT]    Script Date: 01/03/2021 15:15:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
				
				INSERT INTO dbo.[CaseHistory] 
				( 
					[caseId],
					[userId],
					[officerId],
					[Comment],
					[occured],
					[typeId],
					[CaseNoteId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Comment],
					[Occurred],	
					case [HistoryTypeName]
						when 'Broken' then 28
						when 'Clear arrange' then 29
						when 'Clear plan' then 29
						when 'Payment status' then 32
						when 'Status changed' then 42
						when 'Success' then 42
						when 'DVLA info' then 49
						when 'Removed agent' then 53
						when 'Allocated' then 54
						when 'Arrangement' then 56
						when 'Payment plan' then 56
						when 'Cancelled' then 57
						when 'Documents' then 59
						when 'Address' then 68
						when 'Linked' then 71
						when 'Unlinked' then 72
						when 'Fee added' then 74
						when 'Fee status' then 74
						when 'Debt Changed' then 75
						when 'Stage' then 97
						when 'DVLA Enquiry' then 102
						when 'Return Details' then 110
						when 'Off hold' then 111
						when 'Hold' then 112
						when 'Phone' then 123
						when 'COG Visit' then 126
						when 'No COG visit' then 126
						when 'Contact details' then 137
						when 'PayOnline' then 140
						when 'Moved' then 145
						when 'Figures changed' then 150
						when 'Phone (inbound)' then 181
						when 'Letter' then 302
						when 'SMS to debtor' then 608
						else 0
					end,					
					[CaseNoteId]
                FROM
                    oso.[Staging_OS_History]
				WHERE 
					Imported = 0 OR Imported IS NULL
					
				UPDATE [oso].[Staging_OS_History]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0 OR Imported IS NULL	

				COMMIT TRANSACTION
                SELECT
                    1 as Succeed
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION
                END
                INSERT INTO oso.[SQLErrors] VALUES
                        (Error_number()
                            , Error_severity()
                            , Error_state()
                            , Error_procedure()
                            , Error_line()
                            , Error_message()
                            , Getdate()
                        )
                SELECT
                    0 as Succeed
            END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 01/04/2021 09:38:35 ******/
DROP PROCEDURE [oso].[usp_OS_TaggedNotes_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 01/04/2021 09:38:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
					---- Insert case notes
					--INSERT INTO dbo.[CaseNotes] 
					--( [caseId]
					--	, [userId]
					--	, [officerId]
					--	, [text]
					--	, [occured]
					--	, [visible]
					--	, [groupId]
					--	, [TypeId]
					--)
					--SELECT
					--	[CCaseId],
					--	[CUserId],
					--	NULL,
					--	[CaseNote],
					--	[DateAdded],
					--	1,
					--	NULL,
					--	NULL
					--FROM
					--	[oso].[stg_Onestep_PhsTwo_TaggedNotes] 
					--WHERE 
					--	Imported = 0 AND CaseNote IS NOT NULL AND CaseNote <>''

					--UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					--SET Imported = 1, ImportedOn = GetDate()
					--FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					--WHERE otn.Imported = 0 AND otn.CaseNote IS NOT NULL AND otn.CaseNote <>''

					-- Update existing tagged note
					UPDATE [dbo].AdditionalCaseData 
					SET 
						[Value] = otn.TagValue,
						[LoadedOn] = otn.DateAdded
					FROM [dbo].AdditionalCaseData ac
					INNER JOIN [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL
				
					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					INNER JOIN [dbo].AdditionalCaseData ac ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL

					-- Insert new tagged notes
					INSERT INTO [dbo].AdditionalCaseData (CaseId,[Name],[Value],LoadedOn)
					SELECT DISTINCT cCaseId,TagName,MAX(TagValue),MAX(DateAdded)
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL
					GROUP BY cCaseId, TagName

					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO




