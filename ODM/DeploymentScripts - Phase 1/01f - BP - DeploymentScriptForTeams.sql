DECLARE @teamId INT,
		@RossBrandId INT = (SELECT [Id] FROM [Brands].[Brands] WHERE [Name] = 'Rossendales'),
		@MarstonRBrandId INT = (SELECT [Id] FROM [Brands].[Brands] WHERE [Name] = 'Marston CTAX'),
		@SwiftRBrandId INT = (SELECT [Id] FROM [Brands].[Brands] WHERE [Name] = 'Swift TMA & CTAX')

INSERT [dbo].[Teams]([name],[description],[maxCases],[ExcludeFromWorkflows])
VALUES ('Helmshore Enforcement Support Team','Team for Helmshore Enforcement Support cases to be assigned to',9999999,0)
SET @teamId = SCOPE_IDENTITY();

INSERT [Brands].[TeamsBrands] VALUES(@teamId, @RossBrandId)
INSERT [Brands].[TeamsBrands] VALUES(@teamId, @MarstonRBrandId)
INSERT [Brands].[TeamsBrands] VALUES(@teamId, @SwiftRBrandId)

INSERT [dbo].[Teams]([name],[description],[maxCases],[ExcludeFromWorkflows])
VALUES ('Helmshore Business Rates Team','Team for cases to be assigned to the Helmshore Business Rates team',9999999,0)
SET @teamId = SCOPE_IDENTITY();

INSERT [Brands].[TeamsBrands] VALUES(@teamId, @RossBrandId)
INSERT [Brands].[TeamsBrands] VALUES(@teamId, @MarstonRBrandId)
INSERT [Brands].[TeamsBrands] VALUES(@teamId, @SwiftRBrandId)
