

/****** Object:  Table [oso].[Staging_OS_Payments]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[Staging_OS_Payments]
GO

/****** Object:  Table [oso].[Staging_OS_Payments]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Payments](
	[cCaseId] [int] NOT NULL,
	[CSchemePaymentId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaymentStatus] varchar(5) NOT NULL ,
	[PaymentSource] varchar(50) NULL ,
	[ClearedOn] [datetime] NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseId]    Script Date: 25/04/2019 09:45:11 ******/
DROP PROCEDURE [oso].[usp_OS_GetCaseId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseId]    Script Date: 11/04/2019 10:13:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as cCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_DT]    Script Date: 10/05/2020 09:43:53 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Payments_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_DT]    Script Date: 10/05/2020 09:43:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId                    INT
                    , @Amount                      DECIMAL(18,4)
                    , @UserId                      INT
                    , @AddedOn                     DATETIME
                    , @PaymentId                   INT
                    , @SchemePaymentId             INT
					, @PaymentStatus			   VARCHAR (5)
					, @PaymentSource			   VARCHAR (50)
                    , @ClearedOn                   DATETIME
                    , @ClientCaseReference         VARCHAR(200)
                    , @OriginalCaseStatus          VARCHAR(50)
                    , @OriginalCaseStatusId        INT
                    , @PaidStatusId                INT
                    , @PartPaidStatusId            INT
                    , @FinalClearedStatusId        INT
                    , @FinalUnclearedStatusId      INT
                    , @PaymentStatusId             INT
                    , @HistoryId_PaymentCleared    INT
                    , @HistoryId_CaseStatusChanged INT
                    , @NewCaseStatusId             INT
					, @OfficerId				   INT
                    , @OldCaseNumber				   VARCHAR(20)
					, @ClientPaymentRunId		   INT


                SELECT
                       @PaidStatusId = cs.[Id]
                FROM
                       [dbo].[CaseStatus] cs
                WHERE
                       cs.[name] = 'Paid'

                SELECT
                       @PartPaidStatusId = cs.[Id]
                FROM
                       [dbo].[CaseStatus] cs
                WHERE
                       cs.[name] = 'Part-Paid'

                SELECT
                       @FinalClearedStatusId = [Id]
                FROM
                       [dbo].[PaymentStatus]
                WHERE
                       [name] = 'FINAL CLEARED'

                SELECT
                       @FinalUnclearedStatusId = [Id]
                FROM
                       [dbo].[PaymentStatus]
                WHERE
                       [name] = 'FINAL UNCLEARED'

                SELECT
                       @HistoryId_PaymentCleared = [Id]
                FROM
                       [dbo].[HistoryTypes]
                WHERE
                       [name] = 'PaymentCleared'

                SELECT
                       @HistoryId_CaseStatusChanged = [Id]
                FROM
                       [dbo].[HistoryTypes]
                WHERE
                       [name] = 'CaseStatusChanged'

                DECLARE cursorT
                CURSOR
                    --LOCAL STATIC
                    --LOCAL FAST_FORWARD
                    --LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT
						cCaseId,
						CSchemePaymentId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId,
						PaymentStatus,
						PaymentSource,
						ClearedOn
                    FROM
                        oso.[Staging_OS_Payments]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
                          @CaseId
                        , @SchemePaymentId
                        , @Amount
                        , @AddedOn
                        , @UserId
                        , @OfficerId
						, @PaymentStatus
						, @PaymentSource
						, @ClearedOn
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        SELECT
                                   @OriginalCaseStatus   = cs.[name]
                                 , @OriginalCaseStatusId = cs.[Id]
                                 , @ClientCaseReference  = c.clientCaseReference
								 , @OldCaseNumber = oc.OldCaseNumber
                        FROM
                                   [dbo].[CaseStatus] cs
                                   INNER JOIN Cases c on cs.Id = c.StatusId
								   INNER JOIN OldCaseNumbers oc ON oc.CaseId = cs.Id
                        WHERE
                                   c.Id = @CaseId

						SET @PaymentStatusId = @FinalClearedStatusId
						SET @ClientPaymentRunId = 2369;

						IF ((@PaymentStatus = 'W'))
						BEGIN
							SET @ClientPaymentRunId = NULL
						END

						IF ((@ClearedOn IS NULL))
						BEGIN
							SET @PaymentStatusId = @FinalUnclearedStatusId
						END
						

                        -- Add new payment
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]                   --1
                                    , [transactionDate]         --2
                                    , [amount]                  --3
                                    , [clearedOn]               --4
                                    , [bouncedOn]               --5
                                    , [recordedBy]              --6
                                    , [officerId]               --7
                                    , [REFERENCE]               --8
                                    , [authorisationNumber]     --9
                                    , [journalStatusId]         --10
                                    , [receiptTypeId]           --11
                                    , [trusted]                 --12
                                    , [schemePaymentId]         --13
                                    , [paymentStatusId]         --14
                                    , [surchargeAmount]         --15
                                    , [paymentID]               --16
                                    , [reconciledOn]            --17
                                    , [reconcileReference]      --18
                                    , [reversedOn]              --19
                                    , [parentId]                --20
                                    , [previousPaymentStatusId] --21
                                    , [reconciledBy]            --22
                                    , [keepGoodsRemovedStatus]  --23
                                    , [cardStatementReference]  --24
                                    , [bankReconciledOn]        --25
                                    , [bounceReference]         --26
                                    , [reconciledBouncedOn]     --27
                                    , [reconciledBouncedBy]     --28
                                    , [exceptionalAdjustmentId] --29
                                    , [bankReconciledBy]        --30
                                    , [isReversedChargePayment] --31
                                    , [paymentOriginId]         --32
                                    , [uploadedFileReference]   --33
                                    , [ReverseOrBounceNotes]    --34
                               )
                               VALUES
                               ( @AddedOn                                                                                              --1
                                    , @AddedOn                                                                                         --2
                                    , @Amount                                                                                          --3
                                    , @ClearedOn                                                                                         --4
                                    , NULL                                                                                             --5
                                    , @UserId                                                                                          --6
                                    , @OfficerId                                                                                             --7
                                    , RTRIM(LTRIM(@OldCaseNumber + '/' + CAST(@Amount AS varchar) + '/' + CONVERT(VARCHAR(8), @AddedOn, 105))) --8
                                    , NULL                                                                                             --9
                                    , NULL                                                                                             --10
                                    , 1                                                                                                --11
                                    , 1                                                                                                --12
                                    , @SchemePaymentId                                                                                 --13
                                    , @PaymentStatusId                                                                                 --14
                                    , 0.00                                                                                             --15
                                    , 'ODMPayment'                                                                              --16
                                    , @AddedOn                                                                                             --17
                                    , '2020-05-11_ONESTEPMIGRATION'                                                                                             --18
                                    , NULL                                                                                             --19
                                    , NULL                                                                                             --20
                                    , NULL                                                                                             --21
                                    , 2                                                                                             --22
                                    , 1                                                                                                --23
                                    , NULL                                                                                             --24
                                    , @AddedOn                                                                                             --25
                                    , NULL                                                                                             --26
                                    , NULL                                                                                             --27
                                    , NULL                                                                                             --28
                                    , NULL                                                                                             --29
                                    , 2                                                                                             --30
                                    , 0                                                                                                --31
                                    , 1                                                                                                --32
                                    , NULL                                                                                             --33
                                    , NULL                                                                                             --34
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , @ClientPaymentRunId
                                    , 0
                                    , 0
                                    , 0
                                    , 0
                               )
                        ;

                        SELECT
                               @NewCaseStatusId =
                                      (
                                             case
                                                    when OutstandingBalance >0
                                                           then @PartPaidStatusId
                                                    when OutstandingBalance <=0
                                                           then @PaidStatusId
                                             end
                                      )
                        FROM
                               [dbo].[vw_CaseBalance]
                        WHERE
                               [Id] = @CaseId
                        
						IF (@OriginalCaseStatusId <> @NewCaseStatusId)
                        BEGIN
                            --Status changed, so update the case and insert a history for change of status.
                            UPDATE
                                   dbo.[Cases]
                            SET    [StatusId] = @NewCaseStatusId
                            WHERE
                                   [Id] = @CaseId
                            ;
            
                        END;
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
                          @CaseId
                        , @SchemePaymentId
                        , @Amount
                        , @AddedOn
                        , @UserId
                        , @OfficerId
						, @PaymentStatus
						, @PaymentSource
						, @ClearedOn
					END
                    CLOSE cursorT
                    DEALLOCATE cursorT 
					
					UPDATE [oso].[Staging_OS_Payments]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0

					COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO




/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetSchemePaymentXMapping]    Script Date: 04/11/2019 15:57:25 ******/
DROP PROCEDURE [oso].[usp_OS_Payments_GetSchemePaymentXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetSchemePaymentXMapping]    Script Date: 04/11/2019 15:57:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep payment type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetSchemePaymentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemePaymentName]
      ,[OSSchemePaymentName] AS PaymentType
	  ,[CSchemePaymentId]
  FROM [oso].[OneStep_SchemePayment_CrossRef]
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetUsersXMapping]    Script Date: 04/11/2019 16:03:11 ******/
DROP PROCEDURE [oso].[usp_OS_Payments_GetUsersXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetUsersXMapping]    Script Date: 04/11/2019 16:03:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId] AS UserId,
			[ColId] AS CUserId
		FROM [oso].[OSColUsersXRef]
    END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetOfficersXMapping]    Script Date: 04/11/2019 16:06:32 ******/
DROP PROCEDURE [oso].[usp_OS_Payments_GetOfficersXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetOfficersXMapping]    Script Date: 04/11/2019 16:06:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId] AS OfficerId
	,[ColId] AS COfficerId
  FROM [oso].[OSColOfficersXRef]
    END
END
GO