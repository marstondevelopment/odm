
/****** Object:  Table [oso].[stg_OneStep_ReturnCode_CrossRef]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE [oso].[stg_OneStep_ReturnCode_CrossRef]
GO

/****** Object:  Table [oso].[stg_OneStep_ReturnCode_CrossRef]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [oso].[stg_OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [oso].[OneStep_ReturnCode_CrossRef]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE [oso].[OneStep_ReturnCode_CrossRef]
GO

/****** Object:  Table [oso].[OneStep_ReturnCode_CrossRef]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL,
	[CId] [int] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCodes_CrossRef_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_ReturnCodes_CrossRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCodes_CrossRef_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ReturnCodes_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_ReturnCode_CrossRef] 
				(
					CReturnCode
					,OSReturnCode
				)
				SELECT
					CReturnCode
					,OSReturnCode
				FROM
					[oso].[stg_OneStep_ReturnCode_CrossRef]	 
			COMMIT TRANSACTION
			
			BEGIN TRANSACTION
				UPDATE rcc 
				SET rcc.CId = drc.id
				FROM
					oso.[OneStep_ReturnCode_CrossRef] rcc
				INNER JOIN DrakeReturnCodes drc on drc.code = rcc.CReturnCode
			COMMIT TRANSACTION

			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCode_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_ReturnCode_DC_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCode_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ReturnCode_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_ReturnCode_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO




/****** Object:  StoredProcedure [oso].[usp_OS_GetReturnCodeXMapping]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_GetReturnCodeXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetReturnCodeXMapping]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetReturnCodeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CReturnCode]
      ,[OSReturnCode]
  FROM [oso].[OneStep_ReturnCode_CrossRef]
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_ODM_GetReturnCodeXMapping]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_ODM_GetReturnCodeXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_GetReturnCodeXMapping]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_ODM_GetReturnCodeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT CId AS CReturnCode
      ,[OSReturnCode] AS ReturnCode
  FROM [oso].[OneStep_ReturnCode_CrossRef]
    END
END
GO

