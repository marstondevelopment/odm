/****** Object:  StoredProcedure [dbo].[usp_InsertEmailInfo]    Script Date: 30/03/2020 15:41:38 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_InsertEmailInfo]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertEmailInfo]    Script Date: 30/03/2020 15:41:38 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_InsertEmailInfo] @smsEmailRef VARCHAR(100)
	,@CaseIds [udt_ListOfIds] READONLY
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SMSEmailIDs TABLE (
		[smsEmailId] INT
		,[CaseId] INT
		);

	INSERT INTO [dbo].[SMSEmailReceipt] (
		 [CaseId]
		,[DigitalRef]
		,[MarstonRef]
		,[ClientCaseRef]
		,[DefaultersName]
		,[AddressFirstLine]
		,[Address]
		,[PostCode]
		,[ClientOutstandingBalance]
		,[MarstonOutstandingBalance]
		,[MarstonOutstandingBalanceVAT]
		,[OutstandingBalance]
		,[OffenceDescription]
		,[OffenceDate]
		,[ClientName]
		,[VRM]
		,[MarstonCostOutstandingBalance]
		,[MarstonCostOutstandingBalanceVAT]
		,[CaseNumber]
		,[CallCentreTelephoneNumber]
		,[AutomatedLineTelephoneNumber]
		,[UploadReference]
		,[MobileNumber]
		,[EmailAddress]
		)
	OUTPUT [inserted].[Id]
		,[inserted].[CaseId]
	INTO @SMSEmailIDs
	SELECT [C].[Id]
		,@smsEmailRef AS [DigitalRef]
		,[C].[CaseNumber] AS [MarstonRef]
		,[C].[clientCaseReference] AS [ClientCaseRef]
		,[D].[name] AS [DefaultersName]
		,[D].[firstLine] AS [AddressFirstLine]
		,REPLACE(REPLACE(ISNULL([D].[address], ''), CHAR(10), ','), CHAR(13), ',') AS [Address]
		,[D].[postCode] AS [PostCode]
		,[CF].[OutstandingFine] AS [ClientOutstandingBalance]
		,[CF].[OutstandingFees] + [CF].[OutstandingCosts] AS [MarstonOutstandingBalance]
		,[CF].OutstandingVat AS [MarstonOutstandingBalanceVAT]
		,[CF].[GrandTotal] AS [OutstandingBalance]
		,[OFN].[Description] AS [OffenceDescription]
		,[CO].[OffenceDate] AS [OffenceDate]
		,[cl].[Name] AS [ClientName]
		,REPLACE(REPLACE([V].[VRM], CHAR(10), ''), CHAR(13), '') AS [VRM]
		,0.00 AS [MarstonCostOutstandingBalance]
		,0.00 AS [MarstonCostOutstandingBalanceVAT]
		,[C].[CaseNumber] AS [CaseNumber]
		,[CCT].[ContactCentrePhoneNumber]
		,[CCT].[AutomatedLinePhoneNumber]
		,NULL AS [UploadReference]
		,NULL AS [MobileNumber]
		,[DE].[Email] AS [EmailAddress]
	FROM [dbo].[Cases] AS [C]
	JOIN [dbo].[Batches] AS [B] ON [B].[Id] = [C].[batchId]
	JOIN [dbo].[ClientCaseType] AS [CCT] ON [CCT].[Id] = [B].[clientCaseTypeId]
	JOIN [dbo].[Clients] AS [cl] ON [cl].[Id] = [CCT].[ClientId]
	JOIN [dbo].[DefaulterCases] AS [DC] ON [DC].[caseId] = [C].[Id]
	JOIN [dbo].[Defaulters] AS [D] ON [D].[Id] = [DC].[defaulterId]
	JOIN [dbo].[DefaulterEmails] AS [DE] ON [DE].[DefaulterId] = [D].[Id]
	JOIN (
		SELECT [caseid]
			,MIN([id]) AS [offenceid]
		FROM [caseoffences]
		GROUP BY [caseid]
		) AS [MCO] ON [MCO].[CaseId] = [C].[Id]
	JOIN [dbo].[CaseOffences] AS [CO] ON [CO].[Id] = [MCO].[OffenceId]
	JOIN [dbo].[Offences] AS [OFN] ON [OFN].[Id] = [CO].[OffenceId]
	JOIN [dbo].[vw_actualcasebalances] AS [CF] ON [CF].[CaseId] = [C].[Id]
	LEFT JOIN [dbo].[CaseVehicles] AS [CV] ON [CV].[CaseId] = [C].[Id]
	LEFT JOIN [dbo].[Vehicles] AS [V] ON [V].[Id] = [CV].[VehicleId] AND [V].[IsWarrantVRM] = 1
	WHERE EXISTS (
			SELECT 1
			FROM @CaseIds
			WHERE [Id] = [C].[Id]
			)
		AND [DE].StatusId <> 3	
		

	SELECT [smsEmailId]
		,[CaseId]
	FROM @SMSEmailIDs;

	SET NOCOUNT OFF;
END
GO


