/****** Object:  StoredProcedure [dbo].[usp_InsertSMSEmails]    Script Date: 16/03/2020 15:58:39 ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_InsertSMSEmails]
GO

/****** Object:  StoredProcedure [dbo].[usp_InsertSMSEmails]    Script Date: 16/03/2020 15:58:39 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_InsertSMSEmails] @caseIds [udt_ListOfIds] READONLY
	,@smsEmailName VARCHAR(50)
	,@smsEmailRef VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @smsEmailTypeId INT = (
			SELECT TOP 1 SE.[Id]
			FROM [dbo].[SMSEmailTypes] SE
			WHERE SE.[Name] = @smsEmailName
			)

	IF 	@smsEmailName IN ('Standard SMS-Email', 'Letter SMS-Email')	
		AND @smsEmailRef IN (select distinct Reference from SMSEmailTemplates)
	BEGIN
		EXECUTE [dbo].[usp_InsertSMSInfo] @smsEmailRef
			,@caseIds;

		EXECUTE [dbo].[usp_InsertEmailInfo] @smsEmailRef
			,@caseIds;
	END;
	IF @smsEmailName IN ('Debt Collection')
		AND @smsEmailRef IN (SELECT DISTINCT Reference FROM SMSEmailTemplates)
		BEGIN
			EXECUTE [dbo].[usp_InsertEmailInfoDebtCollection] @smsEmailRef, @caseIds;
		END
	ELSE
	IF @smsEmailName IN ('Compliance SMS', 'Non-Committal SMS', 'Pre-Committal SMS', 'Case Monitoring SMS')
		AND @smsEmailRef IN (SELECT DISTINCT Reference FROM SMSEmailTemplates)
		BEGIN
			EXECUTE [dbo].[usp_InsertSMSInfo] @smsEmailRef
			,@caseIds;
		END
	IF @smsEmailName IN ('Compliance Email', 'Non-Committal Email', 'Pre-Committal Email', 'Case Monitoring Email')
		AND @smsEmailRef IN (SELECT DISTINCT Reference FROM SMSEmailTemplates)
		BEGIN
			EXECUTE [dbo].[usp_InsertEmailInfo] @smsEmailRef
			,@caseIds;
		END
	ELSE
	BEGIN
		RAISERROR (
				'Argument exception - usp_InsertSMSEmails'
				,10
				,1
				);
	END;

	SET NOCOUNT OFF;
END
GO


