/****** Object:  Table [oso].[stg_OneStep_ContactAddresses]    Script Date: 10/05/2020 09:52:10 ******/
DROP TABLE IF EXISTS [oso].[stg_OneStep_ContactAddresses]
GO

/****** Object:  Table [oso].[stg_OneStep_ContactAddresses]    Script Date: 10/05/2020 09:52:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_ContactAddresses](
	[AddressLine1] [nvarchar](80) NOT NULL,
	[AddressLine2] [nvarchar](730) NULL,
	[AddressLine3] [nvarchar](100) NULL,
	[AddressLine4] [nvarchar](100) NULL,
	[AddressLine5] [nvarchar](100) NULL,
	[Postcode] [varchar](12) NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_OneStep_ContactAddresses] ADD  DEFAULT ((0)) FOR [Imported]
GO



DROP PROCEDURE IF EXISTS [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_ContactAddresses]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO	

DROP PROCEDURE IF EXISTS [oso].[usp_OS_ContactAddresses_CrossRef_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
		INSERT [dbo].[ContactAddresses] 
		(
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		)
		SELECT 
			[addressline1], 
			[addressline1], 
			[addressline2], 
			[addressline2], 
			[addressline3], 
			[addressline4], 
			[addressline5], 
			[Postcode], 
			[cDefaulterId], 
			1, 
			2, 
			231, 
			0 
		FROM 
			[oso].[stg_OneStep_ContactAddresses]
		WHERE 
			Imported = 0
		EXCEPT
		SELECT 
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		FROM 
			[dbo].[ContactAddresses]

		UPDATE [oso].[stg_OneStep_ContactAddresses]
		SET Imported = 1, ImportedOn = GetDate()
		WHERE Imported = 0	

		COMMIT TRANSACTION	
		SELECT
			1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_CA_GetExistingAddress]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CA_GetExistingAddress] @cDefaulterId int, @AddressLine1 varchar(80), @Postcode varchar(12) 
AS
BEGIN


	SELECT TOP 1 
		CanonicalAddress
	FROM
		ContactAddresses ca
	WHERE
		ca.defaulterId = @cDefaulterId and ca.CanonicalAddress = [dbo].[udf_GetCanonicalAddress](@AddressLine1,@Postcode)
END
GO

GRANT EXECUTE ON OBJECT::[oso].[usp_OS_ContactAddresses_CrossRef_DT] TO FUTUser;
GRANT ALTER ON OBJECT::[oso].[stg_OneStep_ContactAddresses] TO FUTUser;