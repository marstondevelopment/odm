DROP TABLE IF EXISTS [oso].[stg_OneStep_AssignmentsHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_AssignmentsHistory](
	[cCaseId] [INT] NOT NULL,
	[COfficerId] [INT] NULL,
	[AssignmentDate] [DATETIME] NULL,
	[CUserId] [INT] NOT NULL,
	[TeamId] [INT] NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO



DROP PROCEDURE IF EXISTS [oso].[usp_OS_AssignmentsHistory_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_AssignmentsHistory_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_AssignmentsHistory]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
DROP PROCEDURE IF EXISTS [oso].[usp_OS_AssignmentsHistory_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_AssignmentsHistory_DT]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @GroupName INT;
			SET @GroupName = (SELECT TOP 1 groupName FROM AssignedCasesHistory ORDER BY Id DESC) + 1;
			
			INSERT [dbo].[AssignedCasesHistory]
			(
				[caseId], 
				[officerId], 
				[assignmentDate], 
				[userId], 
				[teamId], 
				[groupName]
			)
			SELECT
				[cCaseId], 
				[COfficerId], 
				[assignmentDate], 
				[CUserId], 
				[teamId], 
				@GroupName
			FROM
				[oso].[stg_OneStep_AssignmentsHistory]
			WHERE Imported = 0

			UPDATE [oso].[stg_OneStep_AssignmentsHistory]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION				
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END