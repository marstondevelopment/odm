
INSERT INTO [dbo].[OfficerTypes] VALUES
 ('Van Self Employed Officer',	'Van SEO',	0,	0,	0,	0,	0,	0)
,('First Call Self Employed Officer',	'FC SEO',	0,	0,	0,	0,	0,	0)
,('Van PAYE Officer',	'Van PAYE',	0,	0,	0,	0,	0,	0)
,('First Call PAYE Officer',	'FC PAYE',	0,	0,	0,	0,	0,	0)


/****** Object:  Table [oso].[stg_Onestep_Officers]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[stg_Onestep_Officers]
GO

/****** Object:  Table [oso].[stg_Onestep_Officers]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_Officers](
					[officerno] [int] NOT NULL,
					[id] [int] NULL,
					[drakeOfficerId] [int] NULL,
					[firstName] [varchar](80) NULL,
					[middleName] [varchar](80) NULL,
					[lastName] [varchar](80) NULL,
					[email] [varchar](100) NULL,
					[phone] [varchar](25) NULL,
					[started] [datetime] NOT NULL,
					[leftDate] [datetime] NULL,
					[typeId] [int] NOT NULL,
					[maxCases] [int] NOT NULL,
					[minFee] [decimal](18, 4) NOT NULL,
					[averageFee] [decimal](18, 4) NOT NULL,
					[minExecs] [int] NOT NULL,
					[signature] [varchar](50) NULL,
					[enabled] [bit] NOT NULL,
					[performanceTarget]  [decimal](10, 2) NOT NULL,
					[outcodes] [varchar](1000) NOT NULL,
					[Manager] [bit] NULL,
					[parentId] [int] NULL,
					[isRtaCertified] [bit] NOT NULL,
					[rtaCertificationExpiryDate] [datetime] NULL,
					[marston] [bit] NOT NULL,
					[rossendales] [bit] NOT NULL,
					[swift] [bit] NOT NULL,
					[ctax] [bit] NOT NULL,
					[nndr] [bit] NOT NULL,
					[tma] [bit] NOT NULL,
					[cmg] [bit] NOT NULL,
					[Imported]	[bit] NULL DEFAULT 0,
					[ImportedOn] [datetime] NULL

) ON [PRIMARY]
GO

/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE oso.[OSColOfficersXRef]
GO

/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE oso.[OSColOfficersXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSOfficerNumber] [int] NOT NULL,
	[OSId] [int] NULL,
	[COfficerNumber] [int] NOT NULL,
	[ColId] [int] NOT NULL,
 CONSTRAINT [PK_OSColOfficersXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE [oso].[usp_CheckOSOfficer]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSOfficer]
@drakeOfficerId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;				
		Select @Id = o.Id	  
	   from  dbo.[Officers] o 
		where 
		o.[officerNumber] = @drakeOfficerId 
		OR
		o.[drakeOfficerId] = @drakeOfficerId 		 
		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO

 
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Officers_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Officers from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 

					@officerno [int],
					@id [int] ,
					@drakeOfficerId [int] ,
					@firstName [varchar](80),
					@middleName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@phone [varchar](25),
					@started [datetime] ,
					@leftDate [datetime],
					@typeId [int] ,
					@maxCases [int],
					@minFee [decimal](18, 4),
					@averageFee [decimal](18, 4),
					@minExecs [int],
					@signature [varchar](50) ,
					@enabled [bit], 
					@performanceTarget  [decimal](10, 2),
					@outcodes [varchar](1000) ,
					@Manager [bit],
					@parentId [int] ,
					@isRtaCertified [bit] ,
					@rtaCertificationExpiryDate [datetime] ,
					@marston [bit] ,
					@rossendales [bit],
					@swift [bit] ,
					@ctax [bit] ,
					@nndr [bit] ,
					@tma [bit] ,
					@cmg [bit] ,				
				
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@RTM_CTId int,
					@CTAX_CTId int,
					@NNDR_CTId int,
					@CMG_CTId int
					
					SELECT @RTM_CTId = Id FROM [dbo].[CaseType] WHERE name = 'RTA/TMA (RoadTraffic/Traffic Management)'				
					SELECT @CTAX_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Council Tax Liability Order'				
					SELECT @NNDR_CTId = Id FROM [dbo].[CaseType] WHERE name = 'National Non Domestic Rates'				
					SELECT @CMG_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Child Maintenance Group'				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						[officerno]
						,[id]
						,[drakeOfficerId]
						,[firstName]
						,[middleName]
						,[lastName]
						,[email]
						,[phone]
						,[started]
						,[leftDate]
						,[typeId]
						,[maxCases]
						,[minFee]
						,[averageFee]
						,[minExecs]
						,[signature]
						,[enabled]
						,[performanceTarget]
						,[outcodes]
						,[Manager]
						,[parentId]
						,[isRtaCertified]
						,[rtaCertificationExpiryDate]
						,[marston]
						,[rossendales]
						,[swift]
						,[ctax]
						,[nndr]
						,[tma]
						,[cmg]

                    FROM
                        oso.[stg_Onestep_Officers]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@ctax,
						@nndr,
						@tma,
						@cmg
                    WHILE @@FETCH_STATUS = 0
                    BEGIN

						INSERT INTO dbo.[Officers]
							(
							  [officerNumber]
							  ,[drakeOfficerId]
							  ,[firstName]
							  ,[middleName]
							  ,[lastName]
							  ,[email]
							  ,[phone]
							  ,[started]
							  ,[leftDate]
							  ,[typeId]
							  ,[maxCases]
							  ,[minFee]
							  ,[averageFee]
							  ,[minExecs]
							  ,[signature]
							  ,[enabled]
							  ,[performanceTarget]
							  ,[outcodes]
							  ,[isManager]
							  ,[parentId]
							  ,[isRtaCertified]
							  ,[rtaCertificationExpiryDate]	
							)

							VALUES
							(

							  @officerno
							  ,@officerno
							  ,@firstName
							  ,@middleName
							  ,@lastName
							  ,@email
							  ,@phone
							  ,@started
							  ,@leftDate
							  ,@typeId
							  ,@maxCases
							  ,@minFee
							  ,@averageFee
							  ,@minExecs
							  ,@signature
							  ,@enabled
							  ,@performanceTarget
							  ,@outcodes
							  ,@Manager
							  ,@parentId
							  ,@isRtaCertified
							  ,@rtaCertificationExpiryDate								  
							)

							SET @ColId = SCOPE_IDENTITY();

							-- Add OfficersBrands
							IF (@marston = 1)
								BEGIN
									SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
									INSERT INTO [Brands].[OfficersBrands] 
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonId
									)
								END
			
							IF (@rossendales = 1)
								BEGIN
									SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendalesId
									)
								END			
						
							IF (@swift = 1)
								BEGIN
									SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftId
									)
								END								
							
							-- Add to CaseTypeInclusion
							INSERT INTO dbo.[CaseTypeInclusion]
									(
										[caseTypeId]
										,[officerId]
									)

									VALUES
									(
										@RTM_CTId
										,@ColId
									),
									
									(
										@CTAX_CTId
										,@ColId
									),
									(
										@NNDR_CTId
										,@ColId
									),
									(
										@CMG_CTId
										,@ColId
									)

							-- Add to OfficerBrandedSettings table
							INSERT INTO dbo.OfficerBrandedSettings
								(
									officerid
									, BrandTypeId
									, OfficerPerformanceBonusSchemeId
									, OfficerRateId
								) 
								
								VALUES 
								( 
									@ColId
									, 1
									, 1
									, 6
								)

							-- Add to XRef table
							INSERT INTO oso.[OSColOfficersXRef]
								(
									[OSOfficerNumber]
									,[OSId]
									,[COfficerNumber]
									,[ColId]
								)

								VALUES
								(
									@officerno
									,@id
									,@officerno
									,@ColId
								)										

										-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@ctax,
						@nndr,
						@tma,
						@cmg
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_Officers]
				  SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0	

				  COMMIT
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Officers_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Officers_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Officers staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From oso.stg_Onestep_Officers
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


DROP PROCEDURE [oso].[usp_OS_GetOfficersXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId]
	,[ColId]
  FROM [oso].[OSColOfficersXRef]
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Officers_GetOfficerCount]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_Officers_GetOfficerCount]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Officers_GetOfficerCount]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_GetOfficerCount]
@firstName nvarchar (250),
@lastName nvarchar (250),
@drakeOfficerId int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Officers where (firstName = @firstName and lastName = @lastName) OR (drakeOfficerId = @drakeOfficerId)
    END
END
GO

-- Insert existing officer ids from Columbus into cross-ref table
/*
firstName	lastName
Tomas		Hutchings
Michael		Hancock
Craig		Barlow
Matthew 		North
Darren		Evans
Katrina		McDonald
Zakariya		Osman
Azad		Ali
Rodney		Curson

*/
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4421,	3754,	13151,	13151	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4761,	4703,	24103,	24102	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4985,	5018,	36963,	36963	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4589,	4252,	36141,	36141	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4412,	3202,	13023,	13023	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4687,	4709,	36668,	36668	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4788,	4783,	36718,	36718	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	4629,	4522,	36313,	36313	)
INSERT INTO [oso].[OSColOfficersXRef] ([OSId],[ColId],[OSOfficerNumber],[COfficerNumber]) Values (	3929,	2456,	33030,	33030	)


GO
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetOfficerTypesCrossRef]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetOfficerTypesCrossRef]
AS
BEGIN
	SELECT Id typeId, abbreviation OfficerType FROM OfficerTypes 
	WHERE isDeleted = 0
END

