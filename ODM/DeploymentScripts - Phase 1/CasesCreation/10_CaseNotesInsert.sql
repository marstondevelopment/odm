﻿/****** Object:  StoredProcedure [oso].[usp_OS_AddCaseNote]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [oso].[usp_OS_AddCaseNote]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_AddCaseNote]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Case notes and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddCaseNote]

AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			INSERT into dbo.CaseNotes
					( [caseId]
					, [userId]
					, [officerId]
					, [text]
					, [occured]
					, [visible]
					, [groupId]
					, [TypeId]
				)

			select c.id as caseId, NULL AS userId, NULL AS officerId, 'Add notes here' as text, osc.CreatedOn,1,NULL,NULL
			from oso.stg_Onestep_cases osc
			join oldcasenumbers ocn on ocn.oldcasenumber = osc.OneStepCaseNumber
			join cases c on ocn.caseid = c.id
			WHERE osc.Imported = 0 -- AND Note Field that needs to be uploaded
			
		END
    END
GO