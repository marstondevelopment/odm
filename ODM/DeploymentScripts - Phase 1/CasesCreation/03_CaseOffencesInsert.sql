﻿/****** Object:  StoredProcedure [oso].[[usp_OS_AddCaseOffences]]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [oso].[usp_OS_AddCaseOffences]
GO

/****** Object:  StoredProcedure [oso].[[usp_OS_AddCaseOffences]]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  JD
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddCaseOffences]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid)
			select c.id, osc.offencedate, osc.offencelocation, osc.offencedescription, offe.id
			from oso.stg_Onestep_cases osc
			join offences offe on osc.offencecode = offe.code
			join oldcasenumbers ocn on ocn.oldcasenumber = osc.onestepcasenumber
			join cases c on ocn.caseid = c.id
			WHERE osc.Imported = 0
			except 
			select caseid, offencedate, offencelocation, notes, offenceid from caseoffences

		END
    END
GO
