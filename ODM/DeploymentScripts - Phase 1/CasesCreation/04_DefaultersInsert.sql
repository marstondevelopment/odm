﻿DROP PROCEDURE [oso].[usp_OS_AddDefaulters]
GO

/****** Object:  StoredProcedure [oso].[[usp_OS_AddDefaulters]] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  JD
-- Create date: 02-04-2020
-- Description: Copy data from staging table to Defaulters, ContactAddresses and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddDefaulters]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN

BEGIN TRAN --ROLLBACK TRAN
		DROP TABLE IF EXISTS #DefaulterTempData
		SELECT top 0 
			ca.firstline 						[ca_firstline], 
			ca.postcode							[ca_postcode], 
			ca.[address]						[ca_address], 
			ca.addressline1						[ca_addressline1], 
			ca.addressline2						[ca_addressline2], 
			ca.addressline3						[ca_addressline3], 
			ca.addressline4						[ca_addressline4], 
			dc.caseid							[CaseNumber], 
			ca.countryid						[ca_countryid], 
			ca.business							[ca_business], 
			ca.defaulterid						[ca_defaulterid], 
			ca.createdon						[ca_createdon], 
			ca.forcesaddress					[ca_forcesaddress], 
			ca.isoverseas						[ca_isoverseas], 
			ca.statusid							[ca_statusid], 
			ca.isprimary						[ca_isprimary], 
			ca.isconfirmed						[ca_isconfirmed], 
			ca.sourceid							[ca_sourceid], 
			ca.tidreviewedid					[ca_tidreviewedid]

		into #DefaulterTempData from contactaddresses ca
		join defaulterdetails dd on 1=1
		join defaultercases dc on 1=1
		 
--DEFAULTERS

		INSERT INTO Defaulters (
			[name],
			[title],
			[firstName],
			[middleName],
			[lastName],
			[firstLine],
			[postCode],
			[address],
			[addressLine1],
			[addressLine2],
			[addressLine3],
			[addressLine4],
			[addressLine5],
			[countryId],
			[business],
			[forcesAddress],
			[isOverseas],
			[addressCreatedOn],
			[isLeadCustomer])
		OUTPUT 
			inserted.firstline [ca_firstLine],
			inserted.postcode [ca_postCode],
			inserted.[address] [ca_address],
			inserted.addressline1 [ca_addressLine1],
			inserted.addressline2 [ca_addressLine2],
			inserted.addressline3 [ca_addressLine3],
			inserted.addressline4 [ca_addressLine4],
			inserted.addressline5 [CaseNumber],
			231 [ca_countryId],
			inserted.business [ca_business],
			inserted.id [ca_defaulterId],
			inserted.addressCreatedOn [ca_createdOn],
			0 [ca_forcesAddress],
			0 [ca_isOverseas],
			3 [ca_StatusId], --Potential
			1 [ca_IsPrimary],
			0 [ca_IsConfirmed],
			1 [ca_SourceId],
			CASE WHEN inserted.isLeadCustomer <> 1 THEN 1  --Defaulters 2 through 5 as 'From Warrant'
				 WHEN inserted.isLeadCustomer = 1 THEN 3 END --Lead Defaulter as 'Manually Reviewed' (to be refined later)
			AS [ca_TidReviewedId]

		into #DefaulterTempData

		--Defaulter1
		select 
			osc.fullnameliable1 [name],
			osc.titleliable1 [title],
			osc.firstnameliable1 [firstName],
			osc.middlenameliable1 [middleName],
			osc.lastnameliable1 [lastName],
			osc.add1liable1 [firstLine],
			osc.addpostcodeliable1 [postCode],
				concat(
					osc.add2liable1, ' ',
					CASE WHEN osc.add3liable1 is not null then CONCAT(osc.add3liable1, ' ') ELSE '' END,
					CASE WHEN osc.add4liable1 is not null then CONCAT(osc.add4liable1, ' ') ELSE '' END,
					CASE WHEN osc.add5liable1 is not null then CONCAT(osc.add5liable1, ' ') ELSE '' END
				)[address],
			osc.add1liable1 [addressLine1],
			osc.add2liable1 [addressLine2],
			osc.add3liable1 [addressLine3],
			osc.add4liable1 [addressLine4],
			CAST(ocn.caseid as varchar(12)) [addressLine5],
			231 [countryId],
			CASE
				WHEN ct.name = 'National Non Domestic Rates' then 1
				ELSE 0
				END AS [business],
			0 [forcesAddress],
			0 [isOverseas],
			osc.createdon [addressCreatedOn],
			1 [isLeadCustomer]
			from oso.stg_Onestep_cases osc
			join clients cli on osc.clientid = cli.id
			join clientcasetype cct on cli.id = cct.clientid
			join casetype ct on cct.casetypeid = ct.id
			join OldCaseNumbers ocn on osc.OneStepCaseNumber = ocn.OldCaseNumber
			where osc.fullnameliable1 IS NOT NULL AND LEN(osc.fullnameliable1) >0
			and osc.imported = 1

			--Defaulter2
			UNION ALL
			select 
			osc.fullnameliable2 [name],
			osc.titleliable2 [title],
			osc.firstnameliable2 [firstName],
			osc.middlenameliable2 [middleName],
			osc.lastnameliable2 [lastName],
			osc.add1liable2 [firstLine],
			osc.addpostcodeliable2 [postCode],
				concat(
					osc.add2liable2, ' ',
					CASE WHEN osc.add3liable2 is not null then CONCAT(osc.add3liable2, ' ') ELSE '' END,
					CASE WHEN osc.add4liable2 is not null then CONCAT(osc.add4liable2, ' ') ELSE '' END,
					CASE WHEN osc.add5liable2 is not null then CONCAT(osc.add5liable2, ' ') ELSE '' END
				)[address],
			osc.add1liable2 [addressLine1],
			osc.add2liable2 [addressLine2],
			osc.add3liable2 [addressLine3],
			osc.add4liable2 [addressLine4],
			CAST(ocn.caseid as varchar(12)) [addressLine5],
			231 [countryId],
			CASE
				WHEN ct.name = 'National Non Domestic Rates' then 1
				ELSE 0
				END AS [business],
			0 [forcesAddress],
			0 [isOverseas],
			osc.createdon [addressCreatedOn],
			0 [isLeadCustomer]
			from oso.stg_Onestep_cases osc
			join clients cli on osc.clientid = cli.id
			join clientcasetype cct on cli.id = cct.clientid
			join casetype ct on cct.casetypeid = ct.id
			join OldCaseNumbers ocn on osc.OneStepCaseNumber = ocn.OldCaseNumber
			where osc.fullnameliable2 IS NOT NULL AND LEN(osc.fullnameliable2) >0
			and osc.imported = 1

			--Defaulter3
			UNION ALL
			select 
			osc.fullnameliable3 [name],
			osc.titleliable3 [title],
			osc.firstnameliable3 [firstName],
			osc.middlenameliable3 [middleName],
			osc.lastnameliable3 [lastName],
			osc.add1liable3 [firstLine],
			osc.addpostcodeliable3 [postCode],
				concat(
					osc.add2liable3, ' ',
					CASE WHEN osc.add3liable3 is not null then CONCAT(osc.add3liable3, ' ') ELSE '' END,
					CASE WHEN osc.add4liable3 is not null then CONCAT(osc.add4liable3, ' ') ELSE '' END,
					CASE WHEN osc.add5liable3 is not null then CONCAT(osc.add5liable3, ' ') ELSE '' END
				)[address],
			osc.add1liable3 [addressLine1],
			osc.add2liable3 [addressLine2],
			osc.add3liable3 [addressLine3],
			osc.add4liable3 [addressLine4],
			CAST(ocn.caseid as varchar(12)) [addressLine5],
			231 [countryId],
			CASE
				WHEN ct.name = 'National Non Domestic Rates' then 1
				ELSE 0
				END AS [business],
			0 [forcesAddress],
			0 [isOverseas],
			osc.createdon [addressCreatedOn],
			0 [isLeadCustomer]
			from oso.stg_Onestep_cases osc
			join clients cli on osc.clientid = cli.id
			join clientcasetype cct on cli.id = cct.clientid
			join casetype ct on cct.casetypeid = ct.id
			join OldCaseNumbers ocn on osc.OneStepCaseNumber = ocn.OldCaseNumber
			where osc.fullnameliable3 IS NOT NULL AND LEN(osc.fullnameliable3) >0
			and osc.imported = 1

			--Defaulter4
			UNION ALL
			select 
			osc.fullnameliable4 [name],
			osc.titleliable4 [title],
			osc.firstnameliable4 [firstName],
			osc.middlenameliable4 [middleName],
			osc.lastnameliable4 [lastName],
			osc.add1liable4 [firstLine],
			osc.addpostcodeliable4 [postCode],
				concat(
					osc.add2liable4, ' ',
					CASE WHEN osc.add3liable4 is not null then CONCAT(osc.add3liable4, ' ') ELSE '' END,
					CASE WHEN osc.add4liable4 is not null then CONCAT(osc.add4liable4, ' ') ELSE '' END,
					CASE WHEN osc.add5liable4 is not null then CONCAT(osc.add5liable4, ' ') ELSE '' END
				)[address],
			osc.add1liable4 [addressLine1],
			osc.add2liable4 [addressLine2],
			osc.add3liable4 [addressLine3],
			osc.add4liable4 [addressLine4],
			CAST(ocn.caseid as varchar(12)) [addressLine5],
			231 [countryId],
			CASE
				WHEN ct.name = 'National Non Domestic Rates' then 1
				ELSE 0
				END AS [business],
			0 [forcesAddress],
			0 [isOverseas],
			osc.createdon [addressCreatedOn],
			0 [isLeadCustomer]
			from oso.stg_Onestep_cases osc
			join clients cli on osc.clientid = cli.id
			join clientcasetype cct on cli.id = cct.clientid
			join casetype ct on cct.casetypeid = ct.id
			join OldCaseNumbers ocn on osc.OneStepCaseNumber = ocn.OldCaseNumber
			where osc.fullnameliable4 IS NOT NULL AND LEN(osc.fullnameliable4) >0
			and osc.imported = 1

			--Defaulter4
			UNION ALL
			select 
			osc.fullnameliable5 [name],
			osc.titleliable5 [title],
			osc.firstnameliable5 [firstName],
			osc.middlenameliable5 [middleName],
			osc.lastnameliable5 [lastName],
			osc.add1liable5 [firstLine],
			osc.addpostcodeliable5 [postCode],
				concat(
					osc.add2liable5, ' ',
					CASE WHEN osc.add3liable5 is not null then CONCAT(osc.add3liable5, ' ') ELSE '' END,
					CASE WHEN osc.add4liable5 is not null then CONCAT(osc.add4liable5, ' ') ELSE '' END,
					CASE WHEN osc.add5liable5 is not null then CONCAT(osc.add5liable5, ' ') ELSE '' END
				)[address],
			osc.add1liable5 [addressLine1],
			osc.add2liable5 [addressLine2],
			osc.add3liable5 [addressLine3],
			osc.add4liable5 [addressLine4],
			CAST(ocn.caseid as varchar(12)) [addressLine5],
			231 [countryId],
			CASE
				WHEN ct.name = 'National Non Domestic Rates' then 1
				ELSE 0
				END AS [business],
			0 [forcesAddress],
			0 [isOverseas],
			osc.createdon [addressCreatedOn],
			0 [isLeadCustomer]
			from oso.stg_Onestep_cases osc
			join clients cli on osc.clientid = cli.id
			join clientcasetype cct on cli.id = cct.clientid
			join casetype ct on cct.casetypeid = ct.id
			join OldCaseNumbers ocn on osc.OneStepCaseNumber = ocn.OldCaseNumber
			where osc.fullnameliable5 IS NOT NULL AND LEN(osc.fullnameliable5) >0
			and osc.imported = 1

--CONTACT ADDRESSES

			Insert Into ContactAddresses ([firstLine],
			[postCode],
			[address],
			[addressLine1],
			[addressLine2],
			[addressLine3],
			[addressLine4],
			[addressLine5],
			[countryId],
			[business],
			[defaulterId],
			[createdOn],
			[forcesAddress],
			[isOverseas],
			[StatusId],
			[IsPrimary],
			[IsConfirmed],
			[SourceId],
			[TidReviewedId])
			SELECT 
			[ca_firstLine],
			[ca_postCode],
			[ca_address],
			[ca_addressLine1],
			[ca_addressLine2],
			[ca_addressLine3],
			[ca_addressLine4],
			NULL,
			[ca_countryId],
			[ca_business],
			[ca_defaulterId],
			[ca_createdOn],
			[ca_forcesAddress],
			[ca_isOverseas],
			[ca_StatusId],
			[ca_IsPrimary],
			[ca_IsConfirmed],
			[ca_SourceId],
			[ca_TidReviewedId]
			from #DefaulterTempData

--DEFAULTER DETAILS
			INSERT INTO DefaulterDetails (
			[DefaulterId],
			[BirthDate],
			[IsBirthDateConfirmed],
			[IsTreatedAsMinor],
			[PotentialBirthDate],
			[IsPotentialBirthDateConfirmed],
			[NINO],
			[IsNINOConfirmed],
			[PotentialNINO],
			[IsPotentialNINOConfirmed],
			[BirthDateConfirmedBy],
			[NINOConfirmedBy])
			SELECT 
			dtd.ca_defaulterid,
			NULL,
			0,
			0,
			NULL,
			0,
			NULL,
			0,
			NULL,
			0,
			NULL,
			NULL
			from #DefaulterTempData dtd
			
			INSERT INTO DefaulterCases
			(caseid, defaulterid)
			
			SELECT dtd.casenumber, dtd.ca_defaulterid 
			from #DefaulterTempData dtd 

-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
			
			UPDATE ca 
			Set TidReviewedId = 1
			from ContactAddresses ca
			join #DefaulterTempData dtd on dtd.ca_defaulterid = ca.defaulterid
			join defaultercases dc on ca.defaulterid = dc.defaulterid
			join cases c on dc.caseid = c.id
			where 
			c.addressline1 = ca.addressline1 AND
			c.postcode = ca.postcode			

			 
		END
    END
GO
