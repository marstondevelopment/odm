﻿/****** Object:  StoredProcedure [oso].[usp_OS_AddCaseVehicle]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [oso].[usp_OS_AddCaseVehicle]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_AddCaseVehicle]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_AddCaseVehicle]

AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN

			INSERT into dbo.Vehicles (
				[vrm]
				,[vrmOwner]
				,[defaulterId]
				,[responseStatusId]
				,[responseDate]
				,[responseLoadedDate]
				,[keeperId]
				,[responseStringId]
				,[eventDate]
				,[licenceExpiry]
				,[exportDate]
				,[scrapDate]
				,[theftDate]
				,[recoveryDate]
				,[make]
				,[model]
				,[colour]
				,[taxClass]
				,[seatingCapacity]
				,[previousKeepers]
				,[Motability]
				,[Check]
				,[LastRequestedOn]
				,[isWarrantVrm]
				,[officerId]
				,[VrmReceivedOn]
			)

			select 
				osc.VehicleVRM AS vrm
				,0 AS vrmOwner
				,MAX(dc.defaulterId) AS defaulterId
				,NULL AS responseStatusId
				,NULL AS responseDate
				,NULL AS responseLoadedDate
				,NULL AS keeperId
				,NULL AS responseStringId
				,NULL AS eventDate
				,NULL AS licenceExpiry
				,NULL AS exportDate
				,NULL AS scrapDate
				,NULL AS theftDate
				,NULL AS recoveryDate
				,NULL AS make
				,NULL AS model
				,NULL AS colour
				,NULL AS taxClass
				,NULL AS seatingCapacity
				,NULL AS previousKeepers
				,0 AS Motability
				,0 AS [Check]
				,NULL AS LastRequestedOn
				,1 AS isWarrantVrm
				,NULL AS officerId
				,osc.CreatedOn AS VrmReceivedOn

			from oso.stg_Onestep_cases osc
			INNER JOIN oldcasenumbers ocn on ocn.oldcasenumber = osc.OneStepCaseNumber
			INNER JOIN cases c on ocn.caseid = c.id
			INNER JOIN DefaulterCases dc ON c.Id = dc.caseId
			WHERE osc.Imported = 0 AND osc.VehicleVRM IS NOT NULL  -- AND Note Field that needs to be uploaded
			GROUP BY c.id, osc.VehicleVRM, osc.CreatedOn


			INSERT INTO dbo.CaseVehicles
			(
				caseId,
				vehicleId
			)
			Select c.id as caseId, v.Id AS vehicleId
			from oso.stg_Onestep_cases osc
			INNER JOIN dbo.oldcasenumbers ocn on ocn.oldcasenumber = osc.OneStepCaseNumber
			INNER JOIN dbo.cases c on ocn.caseid = c.id
			INNER JOIN dbo.DefaulterCases dc ON c.Id = dc.caseId
			INNER JOIN dbo.Vehicles v ON v.defaulterId = dc.defaulterId
			WHERE osc.Imported = 0 AND osc.VehicleVRM IS NOT NULL  -- AND Note Field that needs to be uploaded


		END
    END
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Vehicles and relevant tables.
-- =============================================
--CREATE PROCEDURE [oso].[usp_OS_AddCaseVehicle]
--@CaseId int,
--@VehicleVRM varchar(7) NOT NULL,
--@DefaulterId int NULL,
--@CreatedOn datetime NULL
--AS
--    BEGIN
--        SET NOCOUNT ON;
--		BEGIN
--			DECLARE @VehicleId int

--			INSERT into dbo.Vehicles (
--				[vrm]
--				,[vrmOwner]
--				,[defaulterId]
--				,[responseStatusId]
--				,[responseDate]
--				,[responseLoadedDate]
--				,[keeperId]
--				,[responseStringId]
--				,[eventDate]
--				,[licenceExpiry]
--				,[exportDate]
--				,[scrapDate]
--				,[theftDate]
--				,[recoveryDate]
--				,[make]
--				,[model]
--				,[colour]
--				,[taxClass]
--				,[seatingCapacity]
--				,[previousKeepers]
--				,[Motability]
--				,[Check]
--				,[LastRequestedOn]
--				,[isWarrantVrm]
--				,[officerId]
--				,[VrmReceivedOn]
--			)
--			VALUES
--			(
--				@VehicleVRM
--				,FALSE
--				,@DefaulterId
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,NULL
--				,FALSE
--				,FALSE
--				,NULL
--				,TRUE
--				,NULL
--				,@CreatedOn
--			)

--			SET @VehicleId = SCOPE_IDENTITY();
			
--			INSERT INTO dbo.CaseVehicles 
--			VALUES (@CaseId, @VehicleId)

--		END
--    END
--GO