
DROP TABLE IF EXISTS [oso].[stg_OneStep_DefaulterPhones]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_DefaulterPhones](	
	[TelephoneNumber] [varchar](50) NOT NULL,
	[TypeID] [int] NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterPhones_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_DefaulterPhones]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
	
DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterPhones_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterPhones] 
			(
				[defaulterId], 
				[phone], 
				[TypeId],		 
				[LoadedOn], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[TelephoneNumber],
				[TypeID],		
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterPhones]
			WHERE 
				Imported = 0
			EXCEPT
			SELECT 
				[defaulterId], 
				[phone], 
				[TypeId], 		
				[LoadedOn], 
				[SourceId]		
			FROM 
				[dbo].[DefaulterPhones]

			UPDATE [oso].[stg_OneStep_DefaulterPhones]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION	
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_GetDefaultersXMapping]    Script Date: 08/05/2020 12:35:38 ******/
DROP PROCEDURE [oso].[usp_ODM_GetDefaultersXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_GetDefaultersXMapping]    Script Date: 08/05/2020 12:35:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_ODM_GetDefaultersXMapping]
AS
BEGIN
	select [d].[Id] [cDefaulterId],d.name as DefaulterName, ocn.OldCaseNumber as CaseNumber
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	JOIN [OldCaseNumbers] [ocn] on [c].[Id] = [ocn].CaseId
END
GO

GRANT EXECUTE ON OBJECT::[oso].[usp_OS_DefaulterPhones_DT] TO FUTUser;
GRANT ALTER ON OBJECT::[oso].[stg_OneStep_DefaulterPhones] TO FUTUser;