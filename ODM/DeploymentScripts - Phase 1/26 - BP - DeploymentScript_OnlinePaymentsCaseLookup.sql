﻿DROP TABLE IF EXISTS [oso].[Staging_OS_OnlinePaymentsCaseLookup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_OnlinePaymentsCaseLookup]
(
	ClientRef VARCHAR(200) NOT NULL,
	CaseNumber VARCHAR(7) NOT NULL,
	Client VARCHAR(80) NOT NULL,
	Scheme VARCHAR(50) NOT NULL,
	Amount DECIMAL(18,4) NOT NULL,
	PaymentDate DATETIME NOT NULL
)ON [PRIMARY]
GO
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetColCaseNumbersXMapping]
GO 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetColCaseNumbersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

SELECT c.caseNumber as CaseNumber, ocn.OldCaseNumber AS OurRef 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		END
END