
/* Staging Table Start */
DROP TABLE IF EXISTS [oso].[stg_OneStep_SchemeCharge_CrossRef]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [oso].[stg_OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] varchar(250) NOT NULL,
	[OSSchemeChargeName] varchar(250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL    
	
) ON [PRIMARY]
GO
/* Staging Table End */

/* Real Table Start */
DROP TABLE IF EXISTS [oso].[OneStep_SchemeCharge_CrossRef]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] varchar(250) NOT NULL,
	[OSSchemeChargeName] varchar(250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL    
) ON [PRIMARY]
GO
/* Real Table End */


/* Stored procedure start */
DROP PROCEDURE IF EXISTS [oso].[usp_OS_SchemeCharge_CrossRef_DT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemeCharge_CrossRef_DT] AS
BEGIN	
	INSERT [oso].[OneStep_SchemeCharge_CrossRef]	
	(
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId     
	)
	SELECT
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId   
	FROM
		[oso].[stg_OneStep_SchemeCharge_CrossRef]		

		 update scx 
		  set scx.cschemechargeid = new.id 
		  --select scx.*,new.id [newid]
		  from 
		   oso.OneStep_SchemeCharge_Crossref scx
		  join schemecharges new on new.name = scx.cschemechargename AND 
		  new.chargingschemeid = (select id from chargingscheme where schemename = 'Rossendales Standard TCE Charging Scheme')

	SELECT
        1 as Succeed
	
END
GO
/* Stored procedure end */

DROP PROCEDURE IF EXISTS [oso].[usp_OS_SchemeCharge_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================

CREATE PROCEDURE [oso].[usp_OS_SchemeCharge_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_SchemeCharge_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetSchemeChargeXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemeChargeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemeChargeName]
      ,[OSSchemeChargeName] AS ChargeType
	  ,[CSchemeChargeId]
  FROM [oso].[OneStep_SchemeCharge_CrossRef]
    END
END
GO


DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetSchemeChargeId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		US
-- Create date: 25-07-2018
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemeChargeId]
@CSchemeChargeName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @ChargingSchemeId INT;		

		SELECT @ChargingSchemeId = id FROM ChargingScheme
		WHERE [schemeName] = 'Rossendales Standard TCE Charging Scheme'

		SELECT sc.ID AS CSchemeChargeId
		FROM SchemeCharges sc 
		WHERE chargingSchemeId = @ChargingSchemeId and sc.name = @CSchemeChargeName
		
    END
END
GO

