/*******				Note				******/
/*******	Stages CrossRef is redundant	******/
/*************************************************/

DROP TABLE IF EXISTS [oso].[stg_OneStep_Stages_CrossRef]
GO

/****** Object:  Table [oso].[stg_OneStep_Stages_CrossRef]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_Stages_CrossRef](	
	PhaseTemplateId int not null,
	PhaseTemplateName varchar(50) not null,
	StageTemplateId int not null,
	StageTemplateName varchar(50) not null,
	
) ON [PRIMARY]
GO


DROP TABLE IF EXISTS [oso].[OneStep_Stages_CrossRef]
GO

/****** Object:  Table [oso].[OneStep_Stages_CrossRef]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_Stages_CrossRef](	
	OSStageName varchar(50) not null,
	CStageTemplateId int not null,
	CPhaseTemplateId int not null	
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Stages_DT]  ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Stages_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Stages_DT]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Stages_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[stg_OneStep_Stages_CrossRef]
			(
				[PhaseTemplateId]
				,[PhaseTemplateName]
				,[StageTemplateId]
				,[StageTemplateName]
			)
			SELECT
				pt.Id PhaseTempId	
				,pt.Name PhaseTemplate
				,st.Id StageTemplateId
				,st.Name Stage
			FROM 
				WorkflowTemplates wt
				JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
				JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
			
			WHERE 	
				wt.Name = 'Rossendales 14 days 1 Letter'
			ORDER BY
				PhaseTemplate
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END 
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetStageTemplateId]
GO
-- =============================================
-- Author:		BP
-- Create date: 21-10-2019
-- Description:	Extract Stage Template Id for OS(Rossendales)
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetStageTemplateId]
@CStageName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @CStageTemplateId int;
		DECLARE @CPhaseTemplateId int;
		SELECT @CStageTemplateId = st.Id, @CPhaseTemplateId = pt.Id 
		FROM 
			WorkflowTemplates wt
			JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
			JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
		WHERE 
			wt.Name = 'Rossendales 14 days 1 Letter' and st.Name = @CStageName

		SELECT 
			StageTemplateId [CStageTemplateId], PhaseTemplateId [CPhaseTemplateId]
		FROM 
			[oso].[stg_OneStep_Stages_CrossRef]
		WHERE
			StageTemplateId = @CStageTemplateId and PhaseTemplateId = @CPhaseTemplateId
    END
END
GO

--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_Stages_DT] TO FUTUser;
--GRANT ALTER ON OBJECT::[oso].[stg_OneStep_Stages_CrossRef] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_GetStageTemplateId] TO FUTUser;
