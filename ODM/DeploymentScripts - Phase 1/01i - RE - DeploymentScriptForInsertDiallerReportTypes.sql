BEGIN TRAN
INSERT INTO [dbo].[DiallerReportTypes]
	([Id],[Name])
VALUES 
	(28,'Compliance Dialler 1'),
	(29,'Compliance Dialler 2'),	
	(30,'Compliance Dialler 3'),	
	(31,'Compliance Dialler 4'),	
	(32,'Pre-Committal Dialler 1'),	
	(33,'Non-Committal Dialler 1'),	
	(34,'Case Monitoring Dialler'),	
	(35,'Compliance Dialler 1 Sweep'),	
	(36,'Compliance Dialler 2 Sweep'),	
	(37,'Compliance Dialler 3 Sweep'),	
	(38,'Compliance Dialler 4 Sweep')
COMMIT TRANSACTION
