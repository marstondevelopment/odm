

/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[Staging_OS_History]
GO

/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_History](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Comment] NVARCHAR(500) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[HistoryTypeId] [int] NULL,
	[CaseNoteId] [int] NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Payments_DT]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [oso].[usp_OS_History_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DT]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
				
				INSERT INTO dbo.[CaseHistory] 
				( 
					[caseId],
					[userId],
					[officerId],
					[Comment],
					[occured],
					[typeId],
					[CaseNoteId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Comment],
					[Occurred],
					[HistoryTypeId],
					[CaseNoteId]
                FROM
                    oso.[Staging_OS_History]
				WHERE 
					Imported = 0
					
				UPDATE [oso].[Staging_OS_History]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0	

				COMMIT TRANSACTION
                SELECT
                    1 as Succeed
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION
                END
                INSERT INTO oso.[SQLErrors] VALUES
                        (Error_number()
                            , Error_severity()
                            , Error_state()
                            , Error_procedure()
                            , Error_line()
                            , Error_message()
                            , Getdate()
                        )
                SELECT
                    0 as Succeed
            END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCode_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_History_DC_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_History]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

