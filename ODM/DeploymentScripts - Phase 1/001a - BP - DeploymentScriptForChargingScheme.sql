INSERT INTO [dbo].[ChargeTypes] ([name],[description],[cost],[officerApplicable],[userApplicable],[removable],[vat],[deleted],[isStatuary],[IsCommissionable],[TranslationId])
VALUES
('Overpayment Distribution','Distribution of overpayment to the client',0,0,1,1,0,0,0,0,null)

DECLARE @OverpaymentPlusID int = SCOPE_IDENTITY();

INSERT INTO [dbo].[ChargingScheme]([schemeName],[description]) 
SELECT 'Rossendales Standard TCE Charging Scheme','Rossendales Standard TCE Charging Scheme'
EXCEPT 
SELECT schemename,description from [dbo].[ChargingScheme]

DECLARE @ChargingSchemeID int = SCOPE_IDENTITY();
DECLARE @SchemeChargesInserted table(id int, name varchar(50));

Insert Into SchemeCharges ([chargeTypeId],[note],[chargingSchemeId],[type],[percentageOf],[name],[applications],[isValid],[isDefaulterChargedVat],[ShortName],[Priority],[RemitType],[PayableBy])
OUTPUT Inserted.id AS [id], Inserted.[name] AS [name] INTO @SchemeChargesInserted
Values 
(6,N'',@ChargingSchemeID,2,4,N'Immobilisation',0,0,0,N'',NULL,2,2)
,(21,N'',@ChargingSchemeID,2,4,N'Additional Costs',0,1,0,N'',NULL,2,2)
,(2,N'',@ChargingSchemeID,2,4,N'Attendance to Remove',0,0,0,N'',NULL,2,2)
,(5,N'',@ChargingSchemeID,2,4,N'Vehicle Removal',0,0,0,N'',NULL,2,2)
,(7,N'',@ChargingSchemeID,2,4,N'EA Attendance',0,0,0,N'',NULL,2,2)
,(10,N'',@ChargingSchemeID,2,4,N'Bounced Cheque',0,0,0,N'',NULL,2,2)
,(23,N'',@ChargingSchemeID,2,4,N'HPI Charge',0,0,0,N'',NULL,2,2)
,(1,N'',@ChargingSchemeID,2,4,N'Letter Fee',0,0,0,N'',NULL,2,2)
,(3,N'',@ChargingSchemeID,2,4,N'Goods Removal',0,0,0,N'',NULL,2,2)
,(11,N'',@ChargingSchemeID,2,4,N'Chargeback',0,0,0,N'',NULL,2,2)
,(33,N'',@ChargingSchemeID,0,2,N'Compliance Stage',1,1,0,N'Compliance',5,2,2)
,(38,N'',@ChargingSchemeID,2,4,N'Write Off',0,0,0,N'',NULL,2,2)
,(37,N'',@ChargingSchemeID,2,4,N'Additional Fees',0,1,0,N'',NULL,2,2)
,(34,N'',@ChargingSchemeID,5,2,N'Enforcement Stage',1,1,0,N'Enforcement',NULL,2,2)
,(35,N'',@ChargingSchemeID,5,2,N'Sale & Disposal',1,1,0,N'Sale',NULL,2,2)
,(21,N'',@ChargingSchemeID,2,4,N'Surcharge',0,0,0,N'Surcharge',NULL,2,2)
,(32,N'',@ChargingSchemeID,2,4,N'Legacy Fee',0,0,0,N'',NULL,2,2)
,(26,N'',@ChargingSchemeID,2,4,N'Levy Fee',0,0,0,N'',NULL,2,2)
,(@OverpaymentPlusID,N'',@ChargingSchemeID,2,4,N'Overplus to Client',0,1,0,N'',NULL,1,2)
,(27,N'',@ChargingSchemeID,2,4,N'Waiting',0,0,0,N'',NULL,2,2)
,(28,N'',@ChargingSchemeID,2,4,N'Walking Possession',0,0,0,N'',NULL,2,2)

Insert Into ChargeRanges ([lowervalue],[uppervalue],[SchemeChargeID],[value],[valuetype],[chargerangeindex],[rounding],[fractionofpound],[fractionofpenny])
Values 
 (0.0000,0.0000,            (SELECT id from @SchemeChargesInserted where name = 'Compliance Stage'), 75.0000,0,1,0,NULL,NULL)
,(0.0000,1500.0000,         (SELECT id from @SchemeChargesInserted where name = 'Enforcement Stage'), 235.0000,0,1,0,NULL,NULL)
,(1500.0100,99999999.9900,  (SELECT id from @SchemeChargesInserted where name = 'Enforcement Stage'), 0.0750,1,2,1,1,1)
,(0.0000,1500.0000,         (SELECT id from @SchemeChargesInserted where name = 'Sale & Disposal'), 110.0000,0,1,0,NULL,NULL)
,(1500.0100,99999999.9900,  (SELECT id from @SchemeChargesInserted where name = 'Sale & Disposal'), 0.0750,1,2,1,1,1)