INSERT [dbo].[DrakeReturnCodes]
(
	[code], 
	[name], 
	[abbreviation], 
	[IsForStaff], 
	[IsForOfficer], 
	[statisticalPositive], 
	[ForceReturn],	 
	[IsClientReturn]
)
VALUES
	(33,'Client return request - Per Guidelines','CPG',1,0,0,1,1),
	(49,'Case return - Duplicate Case','DC',1,0,0,1,0),
	(53,'Case return - Debtor Not Liable','DNL',1,0,0,1,0),
	(51,'Contact outcome - Voluntary Arrangement','COVA',0,1,0,0,0),
	(52,'Case return - Small Outstanding Balance','SOB',1,0,0,1,0),
	(55,'Contact outcome - Forfeiture Completed','FC',0,1,0,0,0)
GO
UPDATE 
	[dbo].[DrakeReturnCodes] 
SET 
	name = 'Client return request' 
WHERE 
	code = 1
GO
UPDATE [dbo].[DrakeReturnCodes] 
SET 
	name = 'Case return - Income Support/Benefits', 
	abbreviation = 'ISB', 
	isEnabled = 1, 
	IsForStaff = 1, 
	IsForOfficer = 0, 
	statisticalPositive = 0, 
	nullaBona = 0, 
	returnMatchigCaseOnLoad = 0,
	ForceReturn = 1, 
	IsClientReturn = 0 
WHERE code = 50

