
/****** Object:  Table [oso].[stg_Onestep_Users]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[stg_Onestep_Users]
GO

/****** Object:  Table [oso].[stg_Onestep_Users]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_Users](
					[firstName] [varchar](80) NULL,
					[lastName] [varchar](80) NULL,
					[email] [varchar](100) NULL,
					[Department] [varchar](200) NULL,
					[JobTitle] [varchar](250) NULL,
					[marston] [bit] NOT NULL,
					[swift] [bit] NOT NULL,
					[rossendales] [bit] NOT NULL,
					[onestepid] [int] NOT NULL,
					[username] [nvarchar] (50)NOT NULL,
					[Imported]	[bit] NULL DEFAULT 0,
					[ImportedOn] [datetime] NULL

) ON [PRIMARY]
GO

/****** Object:  Table [oso].[OSColUsersXRef]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE oso.[OSColUsersXRef]
GO

/****** Object:  Table [oso].[OSColUsersXRef]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE oso.[OSColUsersXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar] (50) NOT NULL,
	[OSId] [int] NULL,
	[ColId] [int] NOT NULL,
 CONSTRAINT [PK_OSColUsersXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_GetUserCount]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Get Count for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUserCount]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
    END
END
GO




/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE [oso].[usp_CheckOSUser]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS user is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSUser]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @RecCount INT;				
		Select @RecCount = count(*) From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
		Select 
			CASE
				WHEN @RecCount = 0 THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO

 /****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 05/12/2019 11:46:04 ******/
DROP PROCEDURE [oso].[usp_OS_Users_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 05/12/2019 11:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Users from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@firstName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@Department [varchar](200),
					@JobTitle [varchar](250),
					@marston [bit],
					@swift [bit],
					@rossendales [bit],
					@onestepid [int],
					@username [nvarchar] (50),
					
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@DEPTID int,
					@OFFICEID int,
					@RoleId int,
					@Comment [varchar] (250),
					@UBCount int,
					@ColUserName [varchar] (50),
					@ColUserCount int

					
				SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
				SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
				SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
				
				---- DEPARTMENT
				--INSERT INTO Departments ([name])
				--VALUES ('Rossendales')
				--SET @DEPTID = SCOPE_IDENTITY();
				
				-- OFFICE
				SELECT @OFFICEID = id FROM [dbo].[Offices] WHERE [Name] = 'Helmshore'
				--IF (@OFFICEID IS NULL)
				--BEGIN
				--	INSERT INTO Offices ([Name], [Firstline], [Postcode], [Address])
				--	VALUES ('Helmshore',NULL,NULL,NULL)
				--	SET @OFFICEID = SCOPE_IDENTITY();
				--END

				
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
					[firstName],
					[lastName],
					[email],
					[Department],
					[JobTitle],
					[marston],
					[swift],
					[rossendales],
					[onestepid],
					[username]

                    FROM
                        oso.[stg_Onestep_Users]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@onestepid,
						@username
                    WHILE @@FETCH_STATUS = 0
                    BEGIN

							SELECT @DEPTID = Id FROM [dbo].[Departments] WHERE [name] = @Department
							--IF (@DEPTID is null)
							--	BEGIN

							--		SET @Comment = 'Id not found for department - ' +  @Department;
							--		THROW 51000, @Comment, 163;  
							--	END	
							SET @ColId = NULL
							SELECT @ColId = ColId FROM [oso].[OSColUsersXRef] WHERE username = @username

							IF (@ColId IS NULL)
							BEGIN
								SET @ColUserName = @username
								SELECT @ColUserCount = COUNT(*) FROM dbo.Users WHERE name = @username
								IF (@ColUserCount > 0)
								BEGIN
									SET @ColUserName += '_os';  
								END

								INSERT INTO USERS (
								[name]
								, [password]
								, [firstName]
								, [lastName]
								, [email]
								, [departmentId]
								, [enabled]
								, [officeId]
								, [changePasswordOnLogin]
								, [passwordReset]
								, [defaultResultsExpected]
								, [sendEmailNotification]
								, [sendEmailNotificationEnabledOn]
								, [emailNotificationInterval])
								VALUES (
								@ColUserName
								,'1pF5kXsqfQdLufiK43b1Qw=='
								,@firstName
								,@lastName
								,@email
								,@DEPTID
								, 1
								, @OFFICEID
								, 1
								, 0
								, 1000
								, 0
								, NULL
								, 1)
								SET @ColId = SCOPE_IDENTITY();

								-- Add User ROLE
								IF (@JobTitle IS NOT NULL)
								BEGIN
									SELECT @RoleId = Id FROM Roles WHERE name = @JobTitle
								
									IF (@RoleId is null)
										BEGIN

											SET @Comment = 'RoleId not found for ' +  @JobTitle;
											THROW 51000, @Comment, 163;  
										END	
															
									INSERT INTO dbo.[UserRoles]
										(
											[userId]
											,[roleId]
										)

										VALUES
										(
											@ColId
											,@RoleId									
										)																			
								END							
						
								-- Add to XRef table
								INSERT INTO oso.[OSColUsersXRef]
									(
										[username],
										[OSId],
										[ColId]
									)

									VALUES
									(
										@username
										,@onestepid
										,@ColId
									)										
							END

							-- Add OfficersBrands
							IF (@marston = 1)
								BEGIN
									SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
										WHERE UserId = @ColId and BrandId = @BMarstonId
									IF (@UBCount = 0)
									BEGIN
										INSERT INTO [Brands].[UsersBrands] 
										(
											[UserId]
											,[BrandId]
										)
										VALUES
										(
											@ColId
											,@BMarstonId
										)								
									END

								END
			
							IF (@rossendales = 1)
								BEGIN
									SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
										WHERE UserId = @ColId and BrandId = @BRossendalesId
									IF (@UBCount = 0)
									BEGIN
										INSERT INTO [Brands].[UsersBrands] 
										(
											[UserId]
											,[BrandId]
										)
										VALUES
										(
											@ColId
											,@BRossendalesId
										)							
									END

								END			
						
							IF (@swift = 1)
								BEGIN
									SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
										WHERE UserId = @ColId and BrandId = @BSwiftId
									IF (@UBCount = 0)
									BEGIN
										INSERT INTO [Brands].[UsersBrands] 
										(
											[UserId]
											,[BrandId]
										)
										VALUES
										(
											@ColId
											,@BSwiftId
										)					
									END

								END								
							
					-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@onestepid,
						@username
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_Users]
			      SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0		

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Users_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Users_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Users_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Users staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_Users]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


DROP PROCEDURE [oso].[usp_OS_GetUsersXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId],
			[ColId]
		FROM [oso].[OSColUsersXRef]
    END
END
GO

DELETE * FROM [oso].[OSColUsersXRef]
GO
-- Insert cross reference for existing users in Columbus
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4113	,	9772	,	'dmicallef'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3887	,	10902	,	'hnolan'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4482	,	9550	,	'nmccarthy'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4779	,	8431	,	'cbramwell'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4943	,	7970	,	'jmanuel'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4480	,	9540	,	'akendry1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4454	,	4656	,	'hprice'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4455	,	9548	,	'lmorgan'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4925	,	11128	,	'acoleshill'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4972	,	10249	,	'rphillips3'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2217	,	9233	,	'ktyhurst'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2565	,	9164	,	'clambert'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4245	,	9156	,	'ctaylor3'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4083	,	9230	,	'apadgett'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4246	,	9154	,	'jharper1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4389	,	10178	,	'nberry'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4659	,	10057	,	'gstott'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4564	,	9940	,	'jbroadhurst'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3898	,	10086	,	'dpyrah'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4741	,	9922	,	'amiah4'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4743	,	9926	,	'cmccarthy1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4736	,	9927	,	'gbannister'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4573	,	6578	,	'apickering1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2362	,	11052	,	'nbrandon'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4701	,	11538	,	'aakhtar'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	66	,	10223	,	'aperkins'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4782	,	79	,	'apeters'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	8946	,	'ajohns'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2111	,	10840	,	'awilkinson'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3209	,	11056	,	'aryden'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4276	,	11462	,	'akay1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3557	,	9271	,	'cashworth'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4297	,	7890	,	'cturner'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3885	,	9153	,	'cfairclough'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4487	,	8703	,	'cstewart'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11365	,	'camos'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4565	,	9793	,	'cmetcalfe'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4787	,	8325	,	'callen'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3412	,	11557	,	'cdaly1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3550	,	8469	,	'dmayhew'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4300	,	2838	,	'dstuart'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4679	,	9919	,	'Dmills1'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4892	,	9765	,	'ewilkinson2'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4945	,	11275	,	'erobinson'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	5002	,	11679	,	'ebrotherton'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11503	,	'eeastwood'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2597	,	10059	,	'gdobson'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3769	,	9774	,	'gmacro'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	55	,	11459	,	'gwade'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2128	,	11063	,	'ghastings'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4736	,	11669	,	'gbannister'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4454	,	7213	,	'hprice'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3567	,	9330	,	'jwalmsley'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	9756	,	'jharker'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3200	,	26	,	'jbradshaw'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4295	,	2835	,	'jdemetriou'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4597	,	7331	,	'jprice'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	5487	,	'jodonnell'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3295	,	11059	,	'jbolton'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11446	,	'jmurphy'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4830	,	11728	,	'jahmed'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4246	,	11668	,	'jrobertshaw'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	10402	,	'kfaybooth'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3616	,	41	,	'kblair'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4657	,	9770	,	'kpreston'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11707	,	'kagatiello'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11429	,	'kwalmsley'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4982	,	11530	,	'ksambrook'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2585	,	11066	,	'kthompson'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4938	,	11110	,	'kmortlock'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4880	,	10768	,	'lcasey'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2159	,	10222	,	'lbullock'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3480	,	11458	,	'lhoyle'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4970	,	11687	,	'lfoudy'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4692	,	8027	,	'lharangozo'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2062	,	8086	,	'lashworth'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4274	,	9213	,	'lcurness'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2056	,	11460	,	'mkitching'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4751	,	6961	,	'mbeynon'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4062	,	9151	,	'ohildebrandt'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4912	,	10999	,	'phorwood'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	363	,	'pbliss'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3570	,	11280	,	'swalmsley'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11308	,	'smehran'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4557	,	11065	,	'staylor5'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	4928	,	11283	,	'sbegum7'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2577	,	11055	,	'sireson'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	5019	,	11781	,	'salsford'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	9221	,	'svalli'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	2982	,	11061	,	'sflynn'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	44	,	7825	,	'scrooks'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	3917	,	3521	,	'dorridget'	)
INSERT INTO [oso].[OSColUsersXRef] ([OSId],[ColId],[username]) Values (	9999	,	11637	,	'hgreenridge'	)

GO

-- Insert new roles for OneStep users in Columbus
INSERT	
	[dbo].[Roles] ([name], [type])
VALUES	

	('Allocations Officer', 0),
	('AR Assistant', 0),
	('Assistant Management Accountant', 0),
	('Banking Assistant', 0),
	('Client Liaison Officer', 0),
	('Credit and Collections assistant', 0),
	('Customer Care Administrator', 0),
	('Customer Contact  Senior Team Leader', 0),
	('Customer Contact Agent', 0),
	('Customer Contact Senior Agent', 0),
	('Data Processing administrator', 0),
	('Enforcement Lead Officer', 0),
	('National enforcement director', 0),
	('Operational Services Administrator', 0),
	('Operational Services Team leader', 0)

 
