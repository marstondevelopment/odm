USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOfficerTypesCrossRef]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetOfficerTypesCrossRef]
AS
BEGIN
	SELECT Id typeId, abbreviation OfficerType FROM OfficerTypes 
	WHERE isDeleted = 0
END

GO
