USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetStageTemplateId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BP
-- Create date: 21-10-2019
-- Description:	Extract Stage Template Id for OS(Rossendales)
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetStageTemplateId]
@CStageName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @CStageTemplateId int;
		DECLARE @CPhaseTemplateId int;
		SELECT @CStageTemplateId = st.Id, @CPhaseTemplateId = pt.Id 
		FROM 
			WorkflowTemplates wt
			JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
			JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
		WHERE 
			wt.Name = 'Rossendales 14 days 1 Letter' and st.Name = @CStageName

		SELECT 
			StageTemplateId [CStageTemplateId], PhaseTemplateId [CPhaseTemplateId]
		FROM 
			[oso].[stg_OneStep_Stages_CrossRef]
		WHERE
			StageTemplateId = @CStageTemplateId and PhaseTemplateId = @CPhaseTemplateId
    END
END
GO
