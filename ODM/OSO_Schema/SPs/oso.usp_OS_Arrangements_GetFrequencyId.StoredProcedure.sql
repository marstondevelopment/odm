USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_GetFrequencyId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Arrangements_GetFrequencyId]
@Frequency int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT TOP 1 [CFrequencyId]
		FROM [oso].[OSColInstalmentFrequencyXRef]
		WHERE [OSFrequencyDays] = @Frequency
    END
END
GO
