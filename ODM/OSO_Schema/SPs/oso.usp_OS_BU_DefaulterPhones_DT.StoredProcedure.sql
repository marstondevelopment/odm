USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterPhones_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@TypeID int ,
			@TelephoneNumber varchar(50) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDP')>=-1
		BEGIN
			DEALLOCATE cursorDP
		END

        DECLARE cursorDP CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			TypeID
			,TelephoneNumber
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterPhones]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDP
        FETCH NEXT
        FROM
              cursorDP
        INTO
			@TypeID
			,@TelephoneNumber 
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterPhones] 
					(
						[defaulterId], 
						[phone], 
						[TypeId],		 
						[LoadedOn], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@TelephoneNumber,
						@TypeID,		
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDP
            INTO
				@TypeID
				,@TelephoneNumber 
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDP
        DEALLOCATE cursorDP



	SELECT @Succeed

END
END

GO
