USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_old-10-08-2020]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_old-10-08-2020] 
AS 
  BEGIN 
	SET NOCOUNT ON;

	WHILE Exists(Select Top 1 * From oso.staging_os_newcase_additionalcasedata 
							WHERE  imported = 0 
							AND    errorid IS NULL )
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		
		DECLARE @ClientCaseReference [varchar](200);
		DECLARE @ClientId [int];
		DECLARE @nvpString [nvarchar](max);
		DECLARE @concatenatednvpString nvarchar(max);
		DECLARE @CaseId INT; 

		SELECT @ClientCaseReference = ClientCaseReference, @ClientId = ClientId, @nvpString = NameValuePairs, @concatenatednvpString = ConcatenatedNameValuePairs
		FROM oso.Staging_OS_NewCase_AdditionalCaseData
		WHERE  imported = 0 AND    errorid IS NULL 

        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClientId 
        AND        c.clientcasereference = @ClientCaseReference 
		
        IF( @CaseId IS NOT NULL ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                        ( 
							id            INT IDENTITY(1, 1) NOT NULL,
							namevaluepair NVARCHAR(max),
							namevalueid   INT
                        ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                        namevaluepair, 
                        namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @nvpString, 
            251, 
            0; 
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
				
				
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
               
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 
            END 

			IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
		  IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
          DROP TABLE #SINGLENAMEVALUEPAIR 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
		  

          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @ClientCaseReference 
          AND    clientid = @ClientId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @ClientCaseReference 
        AND    clientid = @ClientId 
       -- AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @ 
        SELECT 0 AS succeed 
      END catch 
    END 
  END
GO
