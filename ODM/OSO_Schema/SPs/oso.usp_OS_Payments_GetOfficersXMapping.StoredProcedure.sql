USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetOfficersXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId] AS OfficerId
	,[ColId] AS COfficerId
  FROM [oso].[OSColOfficersXRef]
    END
END
GO
