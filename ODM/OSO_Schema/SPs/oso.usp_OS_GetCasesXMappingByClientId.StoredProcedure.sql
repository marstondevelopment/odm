USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMappingByClientId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCasesXMappingByClientId]
@ClientId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, c.ClientCaseReference, c.ClientDefaulterReference, c.CaseNumber as cCaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cl.Id = @ClientId
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference,'YYYYYYYYYYYYYY' AS ClientDefaulterReference,'ZZZZZZZ' AS cCaseNumber
		
		ORDER BY c.Id DESC

    END
END
GO
