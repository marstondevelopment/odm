USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientParentXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO
