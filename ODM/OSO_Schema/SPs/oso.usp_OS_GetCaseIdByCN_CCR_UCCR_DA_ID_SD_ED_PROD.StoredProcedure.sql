USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseIdByCN_CCR_UCCR_DA_ID_SD_ED_PROD]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 06-Jul-2021
-- Description:	Extract case id by CN_CCR_UCCR_DA_ID_SD_ED
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseIdByCN_CCR_UCCR_DA_ID_SD_ED_PROD]
@ClientName nvarchar (250),
@ClientCaseReference varchar(200),
@UCCR varchar(200),
@FineAmount Decimal(18,4),
@IssueDate DateTime,
@StartDate DateTime,
@EndDate DateTime

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		Declare @DupRecCount int = 0;

		Set @IssueDate = cast(convert(char(11), @IssueDate, 113) as datetime) 
		Set @StartDate = cast(convert(char(11), @StartDate, 113) as datetime) 
		Set @EndDate = cast(convert(char(11), @EndDate, 113) as datetime) 

		Select @DupRecCount = Count (*) from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 

		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
			AND (c.clientCaseReference = @UCCR OR c.clientCaseReference = @ClientCaseReference)
			AND c.originalBalance = @FineAmount 
			AND (@IssueDate IS NULL OR c.issueDate = @IssueDate)
			AND (@StartDate IS NULL OR c.startDate = @StartDate)
			AND (@EndDate IS NULL OR c.endDate = @EndDate)
	  IF (@DupRecCount >0)
	  BEGIN
		  Select TOP 1 c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId, c.issueDate as IssueDate, 
		  c.startDate as StartDate, c.endDate as EndDate, c.originalBalance as FineAmount,
		  0 AS OCCRImported

		   from cases c 
			  inner join dbo.[Batches] b on c.batchid = b.id 
			  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			  inner join dbo.Clients cl on cct.clientId = cl.Id 

			where 
				cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
				AND (c.clientCaseReference = @UCCR OR c.clientCaseReference = @ClientCaseReference )
				AND c.originalBalance = @FineAmount 
				AND (@IssueDate IS NULL OR c.issueDate = @IssueDate)
				AND (@StartDate IS NULL OR c.startDate = @StartDate)
				AND (@EndDate IS NULL OR c.endDate = @EndDate)
			order by c.Id desc
	  END
	  ELSE
	  BEGIN
		  Select TOP 1 c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId, c.issueDate as IssueDate, 
		  c.startDate as StartDate, c.endDate as EndDate, c.originalBalance as FineAmount,
		  1 AS OCCRImported

		   from cases c 
			  inner join dbo.[Batches] b on c.batchid = b.id 
			  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			  inner join dbo.Clients cl on cct.clientId = cl.Id 

			where 
				cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
				AND (c.clientCaseReference = @ClientCaseReference)
	  END

    END
END

GO
