USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedPhoneNo]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedPhoneNo] @cDefaulterId int, @TelephoneNumber varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], dp.phone as cTelephoneNumber
	from [Defaulters] [d]
	Inner Join DefaulterPhones dp on d.Id = dp.defaulterId
	where [d].[Id] = @cDefaulterId
	AND dp.phone = @TelephoneNumber
END
GO
