USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseAgeUpdate_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CaseAgeUpdate_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesAgeDetails TABLE
				(
					[CaseId] [int],
					[CreatedOn] [datetime],
					[BatchDate] [datetime],
					[ExpireOn] [datetime],
					[BatchId] [int]
				)

				INSERT @CasesAgeDetails
				(
					[CaseId],
					[CreatedOn],
					[BatchDate],
					[ExpireOn],
					[BatchId]
				)
				SELECT 
					[ocn].[CaseId],
					[osc].[CreatedOn],
					DATEADD(DAY, [cli].[LifeExpectancy], [osc].[CreatedOn]) [ExpireOn],
					convert(date, [osc].[BatchDate]) BatchDate,
					[c].[batchId]	
				FROM
					[oso].[stg_Onestep_Cases] [osc]
					INNER JOIN [oso].[OSColClentsXRef] [clxref] ON [osc].[ClientId] = [clxref].[CClientId]
					INNER JOIN [oso].[stg_Onestep_Clients] [cli] ON [clxref].[Connid] = [cli].[connid]
					INNER JOIN [dbo].[OldCaseNumbers] [ocn] ON [osc].[OneStepCaseNumber] = [ocn].[OldCaseNumber]
					INNER JOIN [dbo].[Cases] [c] on [ocn].[CaseId] = [c].[Id]

				UPDATE [dbo].[Cases]
				SET 
					[Cases].[createdOn] = [cad].[CreatedOn],
					[Cases].[expiresOn] = [cad].[ExpireOn],
					[Cases].[batchLoadDate] = [cad].[BatchDate]
				FROM
					[Cases] [c]
					INNER JOIN @CasesAgeDetails [cad] ON [c].[Id] = [cad].[CaseId]

				UPDATE [dbo].[Batches]
				SET
					[Batches].[batchDate] = [cad].[BatchDate]
				FROM
					[dbo].[Batches] [ba]
					INNER JOIN (
						SELECT BatchId, MIN(BatchDate) BatchDate
						FROM @CasesAgeDetails
						GROUP BY BatchId
					) [cad] on [ba].[Id] = [cad].[BatchId]
				
					
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
