USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_GetReturnCodeXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_ODM_GetReturnCodeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT CId AS CReturnCode
      ,[OSReturnCode] AS ReturnCode
  FROM [oso].[OneStep_ReturnCode_CrossRef]
    END
END
GO
