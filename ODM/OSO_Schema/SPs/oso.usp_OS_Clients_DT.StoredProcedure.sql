USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					--@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,
					@IsReturnForAPartPaidCasePermitted		bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						--,RegionId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,IsReturnForAPartPaidCasePermitted

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						--,@RegionId
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,@IsReturnForAPartPaidCasePermitted
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  --,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									--,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
									,PermitReturnForPartPaidCase
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
										,@IsReturnForAPartPaidCasePermitted
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							--SELECT @PriorityChargeId = sc.id
							--FROM 
							--	clientcasetype cct
							--	INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
							--	INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
							--	INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							--WHERE 
							--	cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							--INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							--VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									--,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									,@IsReturnForAPartPaidCasePermitted
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
