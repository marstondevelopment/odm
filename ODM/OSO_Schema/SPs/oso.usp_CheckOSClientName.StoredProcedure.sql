USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
--CREATE PROCEDURE [oso].[usp_CheckOSClientName]
--@Name nvarchar (250),
--@CaseType nvarchar (250),
--@Brand int

--AS
--BEGIN
--	SET NOCOUNT ON;
--    BEGIN

--		DECLARE @Id INT;		
--		DECLARE @OSClientName varchar(500)
--		Set @OSClientName = @Name + ' ' + @CaseType
		
--		Select @Id = c.Id	  
--	   from dbo.Clients c 
--		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
--		where 
--		c.[name] = @OSClientName
--		AND cct.[BrandId] = @Brand

--		Select 
--			CASE
--				WHEN @Id is null THEN 1
--				ELSE 0
--			END AS 	Valid

--    END
--END
--GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
