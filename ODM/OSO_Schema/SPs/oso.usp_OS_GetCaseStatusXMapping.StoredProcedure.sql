USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- Description:	Select columbus and onestep case status
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [OSCaseStatusName]
      ,[CCaseStatusName]
	  ,[CCaseStatusId]
  FROM [oso].[OneStep_CaseStatus_CrossRef]
    END
END
GO
