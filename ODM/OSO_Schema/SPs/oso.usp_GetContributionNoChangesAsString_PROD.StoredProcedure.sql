USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_GetContributionNoChangesAsString_PROD]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetContributionNoChangesAsString_PROD] @CCaseId int
AS
BEGIN

With AdditionalCaseDataForSummonNumberCTE as
(
	Select acd.[Value] offence_number, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'SummonsNum'
),
AdditionalCaseDataForEquityAmountVerifiedCTE as
(
	Select acd.[Value] EquityAmountVerified, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'Equity Amt Verified'
),
AdditionalCaseDataForMonthlyAmountCTE as
(
	Select cast(acd.[Value] as decimal(18,2)) costs, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'MonthlyAmount'
),
AdditionalCaseDataForUpfrontAmountCTE as
(
	Select cast(acd.[Value] as decimal(18,2)) originalBalance, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'UpfrontAmount'
),
AdditionalCaseDataForIncomeContribCapCTE as
(
	Select cast(acd.[Value] as decimal(18,2)) ClientOutstandingBalance, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'IncomeContributionCap'
),
DefaulterDetailsCTE as
(
	select dd.nino empNI, c2.Id CaseId from defaulterdetails dd  
	join Defaulters d on dd.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId
),
DefaulterPhonesCTE as
(
	 
	Select Top(1)  
	case dp.TypeId when (select Id from defaulterphonetypes where name = 'home') then dp.phone else ' ' end add_phone_LandLine, 
	case dp.TypeId when (select Id from defaulterphonetypes where name = 'mobile') then dp.phone else ' ' end add_phone_Mobile,
	dp.phone add_fax, c2.Id CaseId from defaulterphones dp  
	join Defaulters d on dp.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId
	AND dp.typeId IN (select Id from defaulterphonetypes where name in ('home','mobile')) 
),

DefaulterEmailsCTE as
(
	select de.email addEmail, c2.Id CaseId from defaulteremails de  
    join Defaulters d on de.defaulterid = d.id  
    join DefaulterCases dc on dc.defaulterid = d.id  
    join Cases c2 on dc.caseid = c2.id  
    WHERE c2.Id = @CCaseId     
),
ContactAddressesCTE as
(
	select concat(ca.firstLine, iif(ca.address is null, '', concat(' ', ca.address)), ' ', ca.postcode) address, c2.Id CaseId from contactaddresses ca  
	join Defaulters d on ca.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId AND ca.IsPrimary=1 
)

select
	CONCAT(	
	ISNULL(c.Id,'Empty'),
	'|', ISNULL(c.clientCaseReference, 'Empty'),
	--'|', 'Debt Costs','|',ISNULL(acdm.costs, fd.Costs),  
	--'|', 'Debt amount','|',ISNULL(acdu.originalBalance, c.originalBalance),
	--'|', 'Offence value','|',ISNULL(acdi.ClientOutstandingBalance, fd.ClientOutstandingBalance), 
	'|', 'Debt Costs','|',fd.costs,  
	'|', 'Debt amount','|',c.originalBalance,
	'|', 'Offence value','|',fd.ClientOutstandingBalance,  
	'|', 'Effective date(LO date)','|',ISNULL(FORMAT(c.issueDate, 'dd/MM/yyyy HH:mm:ss'), 'Empty'),  
	'|', 'Summons number','|',ISNULL(acds.offence_number, 'Empty'),  
	'|', 'NI No','|',ISNULL(dd.empNI, 'Empty'), 
	'|', 'Phone No','|',ISNULL(dp.add_phone_LandLine,'Empty'),
	'|', 'Mobile No','|',ISNULL(dp.add_phone_Mobile,'Empty'),	
	'|', 'Email','|',ISNULL(de.addEmail, 'Empty'),               
	'|', 'Current Address','|',ISNULL(ca.address, 'Empty'),
	'|', 'Equity Amt Verified','|',ISNULL(acda.EquityAmountVerified, 'Empty')	
	) ContributionNoChanges   
from cases c   
	inner join batches b on c.batchid = b.id  
	inner join clientcasetype cct on b.clientcasetypeid = cct.id  
	inner join clients cli on cct.clientid = cli.id  
	left outer join vw_FinancialDetails as fd ON fd.Id=c.Id		
	left outer join AdditionalCaseDataForSummonNumberCTE acds ON c.Id = acds.CaseId
	left outer join AdditionalCaseDataForEquityAmountVerifiedCTE acda on c.Id = acda.CaseId 
	left outer join AdditionalCaseDataForMonthlyAmountCTE acdm on c.Id = acdm.CaseId
	left outer join AdditionalCaseDataForUpfrontAmountCTE acdu on c.Id = acdu.CaseId
	left outer join AdditionalCaseDataForIncomeContribCapCTE acdi on c.Id = acdi.CaseId	
	left outer join DefaulterDetailsCTE dd on c.Id = dd.CaseId
	left outer join DefaulterPhonesCTE dp on c.Id = dp.CaseId	
	left outer join DefaulterEmailsCTE de on c.Id = de.CaseId
	left outer join ContactAddressesCTE ca on c.Id = ca.CaseId	
where 
	c.id= @CCaseId
END
GO
