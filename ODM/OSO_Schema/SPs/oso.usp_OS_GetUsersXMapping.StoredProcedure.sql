USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetUsersXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId],
			[ColId]
		FROM [oso].[OSColUsersXRef]
    END
END
GO
