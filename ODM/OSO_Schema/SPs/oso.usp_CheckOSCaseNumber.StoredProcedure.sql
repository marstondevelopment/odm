USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSCaseNumber]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_CheckOSCaseNumber]
@CaseNumber nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		SELECT 
			@Id = [ocn].[CaseId]	  
		FROM 
			[dbo].[OldCaseNumbers] [ocn]		  
		WHERE 
			[ocn].[OldCaseNumber] = @CaseNumber		

		SELECT 
			CASE WHEN @Id is null THEN 1	ELSE 0 END AS Valid

    END
END
GO
