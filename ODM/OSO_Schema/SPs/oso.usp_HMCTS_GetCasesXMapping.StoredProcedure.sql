USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_GetCasesXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_HMCTS_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CaseId, c.ClientCaseReference AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cct.BrandId = 1
			AND ct.name IN ('Arrest Breach Bail','Arrest Breach No Bail')
		
		UNION
		SELECT 99999999 AS CaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber
    END
END
GO
