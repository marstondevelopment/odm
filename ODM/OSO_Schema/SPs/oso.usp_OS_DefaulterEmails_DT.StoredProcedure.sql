USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterEmails_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterEmails] 
			(
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[EmailAddress],				
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterEmails]
			WHERE 
				(Imported = 0 OR Imported IS NULL	)
			EXCEPT
			SELECT 
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			FROM 
				[dbo].[DefaulterEmails]

			UPDATE [oso].[stg_OneStep_DefaulterEmails]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL	)

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION 
				END

				INSERT INTO oso.[SQLErrors] 
				VALUES (Error_number(), 
						Error_severity(), 
						Error_state(), 
						Error_procedure(), 
						Error_line(), 
						Error_message(), 
						Getdate()) 

				SELECT 0 as Succeed
		END CATCH
	END
END
GO
