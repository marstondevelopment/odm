USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCStatusId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCStatusId] @OSCaseStatusName VARCHAR(50)
AS
BEGIN 
	SELECT TOP 1 CCaseStatusId 
	FROM [oso].[OneStep_CaseStatus_CrossRef] 
	WHERE OSCaseStatusName = @OSCaseStatusName
END
GO
