USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CStatus_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CStatus_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesVariableTable TABLE
				(	
					[CaseId] INT,
					[StatusId] INT,
					[StatusDate] DATETIME,
					[PreviousStatusId] INT,
					[PreviousStatusDate] DATETIME
				)

				INSERT @CasesVariableTable 
				(
					[CaseId],
					[StatusId],
					[StatusDate],
					[PreviousStatusId],
					[PreviousStatusDate]
				)
				SELECT 
					[c].[Id], 
					[oss].[CCaseStatusId], 
					GETDATE(),	 
					[c].[statusId], 
					[c].[statusDate]
				FROM [dbo].[Cases] [c]
				INNER JOIN [oso].[stg_OneStep_CaseStatuses] [oss] on [c].[Id] = [oss].[cCaseId]

				UPDATE [dbo].[Cases]
				SET
					[Cases].[statusId] = [cvt].[StatusId],
					[Cases].[statusDate] = [cvt].[StatusDate],
					[Cases].[previousStatusDate] = [cvt].[PreviousStatusDate],
					[Cases].[previousStatusId] = [cvt].[PreviousStatusId]  
				FROM 
					[dbo].[Cases] [c]
					INNER JOIN @CasesVariableTable [cvt] on [c].[Id] = [cvt].[CaseId]
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
