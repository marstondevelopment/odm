USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterEmails_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterEmails_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@EmailAddress varchar(250) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDE')>=-1
		BEGIN
			DEALLOCATE cursorDE
		END

        DECLARE cursorDE CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			EmailAddress
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterEmails]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDE
        FETCH NEXT
        FROM
              cursorDE
        INTO
			@EmailAddress
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterEmails] 
					(
						[DefaulterId], 
						[Email], 
						[DateLoaded], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@EmailAddress,
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDE
            INTO
				@EmailAddress
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDE
        DEALLOCATE cursorDE



	SELECT @Succeed

END
END


/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 18/01/2021 10:25:48 ******/
SET ANSI_NULLS ON
GO
