USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_Cases_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_HMCTS_AddBatches;
		--UPDATE oso.stg_HMCTS_Cases SET [BatchNoGenerated] = 1 WHERE imported = 0 AND ErrorID IS NULL AND [BatchNoGenerated] = 0
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@BatchDate datetime ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(80) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod varchar(20) ,
			@AddCountryLiable1 int ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,	
			@OffenceValue decimal(18,4),
			@IssuingCourt nvarchar(200) ,	
			@SittingCourt nvarchar(200) ,	
			@WarningMarkers nvarchar(1000) ,	
			@CaseNotes nvarchar(1000) ,	
			@AttachmentInfo nvarchar(1000) 	,
			@BailDate datetime 
     
			, @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @UnPaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int
			,@FileInfo nvarchar(500) 	
			,@FileName nvarchar(500) 	
			,@FullFileName nvarchar(500) 	
			,@WM nvarchar(500)
			,@WMCode nvarchar(15)
			,@WMDate nvarchar(50)
			,@WMDescription nvarchar(500) 
			,@UserId int = 2;
		
		DECLARE @WMTbl TABLE(VALUE nvarchar(500));

		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			  ClientCaseReference
			, ClientDefaulterReference
			, IssueDate
			, OffenceCode
			, OffenceDescription
			, OffenceLocation
			, OffenceCourt
			, DebtAddress1
			, DebtAddress2
			, DebtAddress3
			, DebtAddress4
			, DebtAddress5
			, DebtAddressCountry
			, DebtAddressPostcode
			, StartDate
			, EndDate
			, TECDate
			, FineAmount
			, Currency
			, TitleLiable1
			, FirstnameLiable1
			, MiddlenameLiable1
			, LastnameLiable1
			, FullnameLiable1
			, CompanyNameLiable1
			, DOBLiable1
			, NINOLiable1
			, MinorLiable1
			, Add1Liable1
			, Add2Liable1
			, Add3Liable1
			, Add4Liable1
			, Add5Liable1
			, AddPostCodeLiable1
			, IsBusinessAddress
			, Phone1Liable1
			, Phone2Liable1
			, Phone3Liable1
			, Phone4Liable1
			, Phone5Liable1
			, Email1Liable1
			, Email2Liable1
			, Email3Liable1
			, BatchDate
			, ClientId
			, ClientName
			, PhaseDate
			, StageDate
			, IsAssignable
			, OffenceDate
			, CreatedOn
			, BillNumber
			, ClientPeriod
			, AddCountryLiable1
			, RollNumber
			, Occupation
			, BenefitIndicator
			, CStatus
			, CReturnCode
			, OffenceNotes
			, OffenceValue
			, IssuingCourt
			, SittingCourt
			, WarningMarkers
			, CaseNotes
			, AttachmentInfo
			, BailDate

		FROM
                 oso.[stg_HMCTS_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientCaseReference
			, @ClientDefaulterReference
			, @IssueDate
			, @OffenceCode
			, @OffenceDescription
			, @OffenceLocation
			, @OffenceCourt
			, @DebtAddress1
			, @DebtAddress2
			, @DebtAddress3
			, @DebtAddress4
			, @DebtAddress5
			, @DebtAddressCountry
			, @DebtAddressPostcode
			, @StartDate
			, @EndDate
			, @TECDate
			, @FineAmount
			, @Currency
			, @TitleLiable1
			, @FirstnameLiable1
			, @MiddlenameLiable1
			, @LastnameLiable1
			, @FullnameLiable1
			, @CompanyNameLiable1
			, @DOBLiable1
			, @NINOLiable1
			, @MinorLiable1
			, @Add1Liable1
			, @Add2Liable1
			, @Add3Liable1
			, @Add4Liable1
			, @Add5Liable1
			, @AddPostCodeLiable1
			, @IsBusinessAddress
			, @Phone1Liable1
			, @Phone2Liable1
			, @Phone3Liable1
			, @Phone4Liable1
			, @Phone5Liable1
			, @Email1Liable1
			, @Email2Liable1
			, @Email3Liable1
			, @BatchDate
			, @ClientId
			, @ClientName
			, @PhaseDate
			, @StageDate
			, @IsAssignable
			, @OffenceDate
			, @CreatedOn
			, @BillNumber
			, @ClientPeriod
			, @AddCountryLiable1
			, @RollNumber
			, @Occupation
			, @BenefitIndicator
			, @CStatus
			, @CReturnCode
			, @OffenceNotes
			, @OffenceValue
			, @IssuingCourt
			, @SittingCourt
			, @WarningMarkers
			, @CaseNotes
			, @AttachmentInfo
			, @BailDate

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						-- Check and update BatchId
						SET @CurrentBatchId = NULL
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						IF (@CurrentBatchId IS NULL)
						BEGIN
							DECLARE @CurrentDate DateTime = GetDate();
							INSERT into batches
								(
								[referenceNumber],
								[batchDate],
								[closedDate],
								[createdBy],
								[manual],
								[deleted],
								[crossReferenceSent],
								[crossReferenceSentDate],
								[clientCaseTypeId],
								[loadedOn],
								[loadedBy],
								[receivedOn],
								[summaryDate]
								)

							SELECT 
								CONCAT(cct.batchrefprefix,CONVERT(varchar(8),@BatchDate,112))[referenceNumber]
								,CAST(CONVERT(date,@BatchDate) AS DateTime) [batchDate]
								,@CurrentDate [closedDate]
								,2 [createdBy]
								,1 [manual]
								,0 [deleted]
								,1 [crossReferenceSent]
								,@CurrentDate [crossReferenceSentDate]
								,cct.id [clientCaseTypeId]
								,@CurrentDate [loadedOn]
								,2 [loadedBy]
								,@CurrentDate [receivedOn]
								,NULL [summaryDate]
							FROM
								clientcasetype cct
							WHERE
								cct.clientid = @ClientId
						
							SET @CurrentBatchId = SCOPE_IDENTITY();

							-- Set the BatchIDGenerated flag to true in staging
							UPDATE
							[oso].[stg_HMCTS_Cases]
							SET    BatchNoGenerated = 1
							WHERE
							Imported = 0 
							AND ErrorId is NULL
							AND ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						END


						
						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT
							@StageId = (s.id)
						FROM
							clients cl
							join stages s on s.clientId = cl.id
						WHERE
							s.onLoad = 1
							and cl.id = @Clientid
						
						SET @CStatusId = @UnPaidStatusId;
						SET @ReturnRunId = NULL

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL
						
						IF (@CaseTypeId = 3 )
						BEGIN
							
							SET @OffenceCode =  'BWB'
						END
						IF (@CaseTypeId = 19 )
						BEGIN
							
							SET @OffenceCode =  'BWNB'
						END

						IF (@CaseTypeId = 1 )
						BEGIN
							
							SET @OffenceCode =  'AWB'
						END

						SET @cnid = (  
                                    SELECT MIN(cn.Id)  
                                    FROM CaseNumbers AS cn WITH (  
                                        XLOCK  
                                        ,HOLDLOCK  
                                        )  
                                    WHERE cn.Reserved = 0)

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,''))) --[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @ClientPeriod--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						
					--  Insert Case Note
						INSERT INTO dbo.[CaseNotes] 
						( [caseId]
							, [userId]
							, [officerId]
							, [text]
							, [occured]
							, [visible]
							, [groupId]
							, [TypeId]
						)
						VALUES
						(
							@CaseId,
							@UserId,
							NULL,
							@CaseNotes,
							@CreatedOn,
							1,
							NULL,
							NULL
						)

					-- Insert Warning Markers as Offence Code.
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						VALUES
						(
							@CaseId,
							CONVERT(datetime, @createdon, 103),
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId,
							@OffenceValue
						);

					IF (@WarningMarkers IS NOT NULL)
					BEGIN
						INSERT INTO @WMTbl
						SELECT Value 
						FROM oso.splitstring(@WarningMarkers,'|');

						WHILE EXISTS(SELECT TOP 1 Value FROM @WMTbl)
						BEGIN
							SELECT TOP 1 @WM = Value FROM @WMTbl
							SELECT @WMCode = Value FROM oso.SplitString(@WM,';') WHERE idx = 1;
							SELECT @WMDate = TRY_CONVERT(datetime, Value, 103) FROM oso.SplitString(@WM,';') WHERE idx = 2;
							SELECT @WMDescription = Value FROM oso.SplitString(@WM,';') WHERE idx = 3;

							SELECT @OffenceCodeId = Id From Offences WHERE Code = @WMCode;
							INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
							VALUES
							(
								@CaseId,
								TRY_CONVERT(datetime, @WMDate, 103),
								NULL,
								@WMDescription,
								@OffenceCodeId,
								NULL
							);

							DELETE FROM @WMTbl WHERE VALUE = @WM;
						END
					END

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DOBLiable1,
						0,
						0,
						NULL,
						0,
						@NINOLiable1,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone1Liable1,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone2Liable1,
								2,
								@CreatedOn,
								1
							)
						END

						IF (@Phone3Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone3Liable1,
								2,
								@CreatedOn,
								1
							)
						END
						IF (@Phone4Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone4Liable1,
								2,
								@CreatedOn,
								1
							)
						END
						IF (@Phone5Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone5Liable1,
								2,
								@CreatedOn,
								1
							)
						END

						-- Insert Defaulter Email
						IF (@Email1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Email1Liable1,
								@CreatedOn,
								1
							)
						END
					END

					-- Insert Name Value Pair
					INSERT INTO dbo.[AdditionalCaseData]
						([CaseId]
						 ,[Name]
						 ,[Value]
						 ,[LoadedOn]
						 )
						 VALUES
						 (
						 @CaseId
						 ,'IssuingCourt'
						 ,@IssuingCourt
						 ,@CreatedOn
						 )

					IF (@BailDate IS NOT NULL)
					BEGIN
						INSERT INTO dbo.[AdditionalCaseData]
							([CaseId]
							 ,[Name]
							 ,[Value]
							 ,[LoadedOn]
							 )
							 VALUES
							 (
							 @CaseId
							 ,'BailDate'
							 ,CONVERT(VARCHAR(24),@BailDate,121)
							 ,@CreatedOn
							 )
					END

					-- Insert Attachments
					IF (@AttachmentInfo IS NOT NULL)
					BEGIN
						INSERT INTO @WMTbl
						SELECT Value 
						FROM oso.splitstring(@AttachmentInfo,'|');

						WHILE EXISTS(SELECT TOP 1 Value FROM @WMTbl)
						BEGIN
							SELECT TOP 1 @FileInfo = Value FROM @WMTbl
							SELECT @FileName = Value FROM oso.SplitString(@FileInfo,';') WHERE idx = 1;
							SELECT @FullFileName = Value FROM oso.SplitString(@FileInfo,';') WHERE idx = 2;

							INSERT INTO dbo.Attachments (
							   [caseId]
							  ,[userId]
							  ,[attachmentTypeId]
							  ,[fileName]
							  ,[attachedOn]
							  ,[fullName]
							  ,[convertedOn]
							  ,[conversionError]
							)
							VALUES
							(
								@CaseId
								,@UserId
								,1
								,@FileName
								,@CreatedOn
								,@FullFileName
								,NULL
								,0						
							)

							DELETE FROM @WMTbl WHERE VALUE = @FileInfo;
						END
					END

					-- Insert PhaseLog

					INSERT INTO [dbo].[PhaseLog]
							   ([CaseId]
							   ,[PhaseTypeId]
							   ,[PhaseStart]
							   ,[PhaseEnd]
							   ,[LastProcessedOn]
							   ,[OriginalDuration]
							   ,[CountOnlyActiveDays]
							   ,[ActiveDaysInPhase]
							   ,[WaitingDays]
							   ,[WaitingDaysIncreasedOn]
							   ,[DaysFromEnforcement])
					select
						c.Id,
						p.PhaseTypeId,
						getdate(),
						null,
						getdate(),
						null,
						p.CountOnlyActiveCaseDays,
						0,
						0,
						null,
						0
					from
						cases c
						join batches b on b.id = c.batchid
						join ClientCaseType cct on cct.id = b.clientCaseTypeId
						join clients cl on cl.id = cct.clientId
						join stages s on s.clientId = cl.Id and s.onLoad = 1
						join phases p on p.id = s.PhaseId
					where
						c.Id = @CaseId
						and cl.Id = @ClientId


					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[stg_HMCTS_Cases]
					SET    
					Imported   = 1
					, ImportedOn = GetDate()
					, CaseId = @CaseId
					WHERE
					Imported = 0 
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_HMCTS_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					Imported = 0
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				@ClientCaseReference
				, @ClientDefaulterReference
				, @IssueDate
				, @OffenceCode
				, @OffenceDescription
				, @OffenceLocation
				, @OffenceCourt
				, @DebtAddress1
				, @DebtAddress2
				, @DebtAddress3
				, @DebtAddress4
				, @DebtAddress5
				, @DebtAddressCountry
				, @DebtAddressPostcode
				, @StartDate
				, @EndDate
				, @TECDate
				, @FineAmount
				, @Currency
				, @TitleLiable1
				, @FirstnameLiable1
				, @MiddlenameLiable1
				, @LastnameLiable1
				, @FullnameLiable1
				, @CompanyNameLiable1
				, @DOBLiable1
				, @NINOLiable1
				, @MinorLiable1
				, @Add1Liable1
				, @Add2Liable1
				, @Add3Liable1
				, @Add4Liable1
				, @Add5Liable1
				, @AddPostCodeLiable1
				, @IsBusinessAddress
				, @Phone1Liable1
				, @Phone2Liable1
				, @Phone3Liable1
				, @Phone4Liable1
				, @Phone5Liable1
				, @Email1Liable1
				, @Email2Liable1
				, @Email3Liable1
				, @BatchDate
				, @ClientId
				, @ClientName
				, @PhaseDate
				, @StageDate
				, @IsAssignable
				, @OffenceDate
				, @CreatedOn
				, @BillNumber
				, @ClientPeriod
				, @AddCountryLiable1
				, @RollNumber
				, @Occupation
				, @BenefitIndicator
				, @CStatus
				, @CReturnCode
				, @OffenceNotes
				, @OffenceValue
				, @IssuingCourt
				, @SittingCourt
				, @WarningMarkers
				, @CaseNotes
				, @AttachmentInfo
				, @BailDate
        END
        CLOSE cursorT
        DEALLOCATE cursorT

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
