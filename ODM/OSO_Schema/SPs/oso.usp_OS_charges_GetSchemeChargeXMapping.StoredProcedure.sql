USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_charges_GetSchemeChargeXMapping]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep charge type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_charges_GetSchemeChargeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT [CSchemeChargeName]
		  ,[OSSchemeChargeName] AS ChargeType
		  ,[CSchemeChargeId]
		FROM [oso].[OneStep_SchemeCharge_CrossRef]	
    END
END
GO
