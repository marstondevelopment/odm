USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AdditionalCaseData_Staging_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_AdditionalCaseData_Staging_DT]   
AS   
  BEGIN   
   SET NOCOUNT ON;  
      DECLARE @CaseId VARCHAR(10);   
      DECLARE @Pairs NVARCHAR(MAX);   
      DECLARE @MultipleNameValuePairs TABLE   
        (   
           caseId     VARCHAR(10),   
           namevaluepairs NVARCHAR(MAX)   
        )   
  
      INSERT INTO @MultipleNameValuePairs   
                  (caseId,   
                   namevaluepairs)   
      SELECT CaseId,   
             NameValuePairs   
      FROM   oso.Staging_OS_AdditionalCaseData   
      WHERE  ErrorId IS NULL AND Imported = 0  
  
      DECLARE namevaluepairs CURSOR fast_forward FOR   
        SELECT caseId,   
               namevaluepairs   
        FROM   @MultipleNameValuePairs   
  
      OPEN namevaluepairs   
  
      FETCH next FROM namevaluepairs INTO @CaseId, @Pairs   
  
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
            BEGIN try   
                BEGIN TRANSACTION   
  
    IF (@Pairs LIKE '%;from%')  
    BEGIN  
     SET @Pairs = REVERSE(SUBSTRING(REVERSE(@Pairs), CHARINDEX(';', REVERSE(@Pairs)), LEN(@Pairs)))  
    END  
  
      
                IF( @CaseId IS NOT NULL )   
                  BEGIN  
                    DECLARE @Name NVARCHAR(MAX)   
                    DECLARE @Value NVARCHAR(MAX)   
         
     --If name value pairs have more than one set of name values, consider them for case note insertion, otherwise additional case data  
     IF((SELECT Count(value) FROM dbo.Fnsplit((@Pairs), ':')) > 2)   
      BEGIN   
       DECLARE @CaseNoteExists INT  
       Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @Pairs  
       IF (@CaseNoteExists = 0)  
       BEGIN  
        INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)  
        SELECT @CaseId,   
          NULL,   
          2,   
          Rtrim(Ltrim(@Pairs)),   
          Getdate(),   
          1,   
          NULL,   
          NULL   
       END  
      END   
      ELSE   
      BEGIN   
          
       IF OBJECT_ID('tempdb..#NameValue') IS NOT NULL  
        DROP TABLE #NameValue  
  
       CREATE TABLE #NameValue   
        (   
         id   INT IDENTITY(1, 1) NOT NULL,   
         word NVARCHAR(MAX)   
        )   
  
       INSERT INTO #NameValue (word)   
       SELECT value   
       FROM   dbo.Fnsplit(@Pairs, ':')   
         
       SET @Name = (SELECT Rtrim(Ltrim(word))   
       FROM   #NameValue   
       WHERE  id = 1 )  
  
       SET @Value = (SELECT Rtrim(Ltrim(word))   
       FROM   #NameValue   
       WHERE  id = 2 )  
        
	   SET @Value = REPLACE(@Value,';','')
       DECLARE @Counter INT = (SELECT Count(*)   
        FROM   dbo.AdditionalCaseData   
        WHERE  NAME = @Name   
          AND caseid = @CaseId)   
        
       IF( @Counter > 0 )   
        BEGIN   
         UPDATE dbo.AdditionalCaseData   
         SET    value = Rtrim(Ltrim(@Value)), LoadedOn = GETDATE()  
         WHERE  caseid = @CaseId   
           AND NAME = @Name   
        END   
      ELSE   
      BEGIN  
       INSERT INTO dbo.AdditionalCaseData   
           (caseid,   
           NAME,   
           value,  
           LoadedOn)   
        VALUES      (@CaseId,   
           @Name,   
           Rtrim(Ltrim(@Value)),  
           GETDATE())   
       END   
  
      END  
        
                      UPDATE [oso].[Staging_OS_AdditionalCaseData]   
                      SET    imported = 1,   
                             importedon = Getdate()   
                      WHERE  CaseId = @CaseId   
                             AND NameValuePairs LIKE '%'+ @Pairs +'%'  
                             AND (Imported IS NULL OR Imported = 0)  
    END  
                COMMIT   
            END try   
  
            BEGIN catch   
                ROLLBACK TRANSACTION   
  
                INSERT INTO oso.[sqlerrors]   
                VALUES      (Error_number(),   
                             Error_severity(),   
                             Error_state(),   
                             Error_procedure(),   
                             Error_line(),   
                             Error_message(),   
                             Getdate() )   
  
                DECLARE @ErrorId INT = Scope_identity();   
  
                UPDATE [oso].[Staging_OS_AdditionalCaseData]   
                SET    errorid = @ErrorId   
                WHERE  CaseId = @CaseId   
                       AND namevaluepairs = @Pairs   
                       AND imported = 0   
  
                SELECT 0 AS Succeed   
            END catch   
  
            FETCH next FROM namevaluepairs INTO @CaseId, @Pairs   
        END   
  
      CLOSE namevaluepairs   
  
      DEALLOCATE namevaluepairs   
  END 
GO
