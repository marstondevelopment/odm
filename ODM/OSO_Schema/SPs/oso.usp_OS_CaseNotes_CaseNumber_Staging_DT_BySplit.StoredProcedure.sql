USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    
AS    
    
BEGIN    
SET NOCOUNT ON;    
IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL    
    DROP TABLE #CaseNotes    
    
CREATE TABLE #CaseNotes    
(    
    [Id] INT IDENTITY(1,1)  NOT NULL,     
    [CaseId] [INT] NOT NULL,    
 [CUserId] [int] NULL,    
 [COfficerId] [int] NULL,    
 [Text] [nvarchar](max) NOT NULL,    
 [Occurred] [datetime] NOT NULL,    
 [Imported] [bit] NULL,    
 [ImportedOn] [datetime] NULL    
)    
    
INSERT into #CaseNotes([CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])    
SELECT [CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]    
      FROM oso.Staging_OS_NewCase_Notes_CaseNumber WHERE Imported = 0 AND ErrorId IS NULL    
         
  DECLARE @CaseNoteID INT;      
  DECLARE @CaseId INT;    
  DECLARE @CaseNumber varchar(10);    
  DECLARE @StringVal NVARCHAR(MAX);    
     
  DECLARE CaseNotes CURSOR FAST_FORWARD FOR    
  SELECT Id, CaseId ,Text    
  FROM   #CaseNotes      
     
  OPEN CaseNotes    
  FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @CaseId , @StringVal    
     
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
  BEGIN TRY    
  BEGIN TRANSACTION    
  --SELECT @CaseId = c.Id   FROM  Cases c     
  --WHERE C.caseNumber = @CaseNumber    
  --Select @CaseId as caseiD    
  IF(@CaseId IS NOT NULL)    
  BEGIN     
   DECLARE @TempNotes TABLE (    
     Content NVARCHAR(2000),     
     Id int    
    )    
       
   INSERT INTO @TempNotes    
   EXEC [oso].[usp_SplitString] @StringVal, 2000, 0;    
    
   ----Remove spaces.    
   Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content)     
    
   INSERT INTO dbo.[CaseNotes]     
     ( [caseId]    
      , [userId]    
      , [officerId]    
      , [text]    
      , [occured]    
      , [visible]    
      , [groupId]    
      , [TypeId]    
     )    
     SELECT    
      @CaseId,    
      2, -- System admin    
      NULL,         
      s.Content,    
      GETDATE(),    
      1,    
      NULL,    
      NULL    
     FROM    
      @TempNotes S    
         
   DELETE FROM @TempNotes    
   UPDATE [oso].[Staging_OS_NewCase_Notes_CaseNumber]    
     SET Imported = 1, ImportedOn = GetDate()    
     WHERE CaseId = @CaseId and Imported = 0    
    
  END    
  COMMIT    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRANSACTION    
    INSERT INTO oso.[SQLErrors] VALUES    
       (Error_number()    
         , Error_severity()    
         , Error_state()    
         , Error_procedure()    
         , Error_line()    
         , Error_message()    
         , Getdate()    
       )    
    
     DECLARE @ErrorId INT = SCOPE_IDENTITY();    
    
     UPDATE    
     [oso].[Staging_OS_NewCase_Notes_CaseNumber]    
     SET    ErrorId = @ErrorId    
     WHERE CaseId = @CaseId and Imported = 0    
    
    SELECT 0 as Succeed    
  END CATCH     
      
    
  FETCH NEXT FROM CaseNotes INTO  @CaseNoteID, @CaseId , @StringVal      
  END    
  CLOSE CaseNotes    
  DEALLOCATE CaseNotes     
END 
GO
