USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[GetCaseStatusfromDB_PROD]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[GetCaseStatusfromDB_PROD] 
@CCaseId int
AS
BEGIN
	SELECT 
		cs.name Status
	FROM
		Cases c 
		INNER JOIN CaseStatus cs ON c.statusId = cs.Id
	WHERE
		c.Id = @CCaseId
END
GO
