USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_CrossRef_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_CaseStatus_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[OneStep_CaseStatus_CrossRef] 
			(
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			)
			SELECT
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			FROM
				[oso].[stg_OneStep_CaseStatus_CrossRef]	 
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
