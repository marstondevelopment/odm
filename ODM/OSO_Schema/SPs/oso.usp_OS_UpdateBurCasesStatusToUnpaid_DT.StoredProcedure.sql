USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_UpdateBurCasesStatusToUnpaid_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  BP
-- Create date: 18-05-2021
-- Description: To updated Burlington cases to previous status 
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_UpdateBurCasesStatusToUnpaid_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
				BEGIN TRANSACTION			   
				
				DECLARE  @CaseNumbers TABLE (caseNumber varchar(7))
				INSERT @CaseNumbers
				select CaseNo from [oso].[Staging_Bur_CasesForStatusUpdate] where Updated = 0 
				
				UPDATE dbo.Cases
				SET 
					previousStatusId = statusId,
					previousStatusDate = statusDate,
					statusId = 1,
					statusDate = GETDATE()
				FROM dbo.Cases
				WHERE caseNumber in (select caseNumber from @CaseNumbers)				
				
				UPDATE
						[oso].[Staging_Bur_CasesForStatusUpdate]
						SET    Updated   = 1
						, UpdatedOn = GetDate()
						WHERE
						Updated = 0 						
						AND CaseNo in (select caseNumber from @CaseNumbers)
					
				COMMIT TRANSACTION
				SELECT
					1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
