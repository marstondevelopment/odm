USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemeChargeId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		US
-- Create date: 25-07-2018
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemeChargeId]
@CSchemeChargeName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @ChargingSchemeId INT;		

		SELECT @ChargingSchemeId = id FROM ChargingScheme
		WHERE [schemeName] = 'Rossendales Standard TCE Charging Scheme'

		SELECT sc.ID AS CSchemeChargeId
		FROM SchemeCharges sc 
		WHERE chargingSchemeId = @ChargingSchemeId and sc.name = @CSchemeChargeName
		
    END
END
GO
