USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSOfficer]
@drakeOfficerId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;				
		Select @Id = o.Id	  
	   from  dbo.[Officers] o 
		where 
		o.[officerNumber] = @drakeOfficerId 
		OR
		o.[drakeOfficerId] = @drakeOfficerId 		 
		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
