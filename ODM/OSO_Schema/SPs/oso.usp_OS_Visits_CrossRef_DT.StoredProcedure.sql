USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Visits_CrossRef_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			INSERT [dbo].[Visits] 
			(
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			)
			SELECT 
				[import].[CCaseId],
				[import].[COfficerId], 
				[import].[VisitDate],
				[import].[DoorColour],
				[import].[BuildingType],
				[import].[PersonContacted],	
				[import].[ProofOfIdentity],
				[import].[Notes],
				0,
				53.68456,
				-2.2769,
				0.00,
				[import].[VisitDate],
				[import].[VisitDate]
			FROM 		
				[oso].[stg_OneStep_Visits] [import]	
			WHERE 
				Imported = 0 OR Imported IS NULL
			EXCEPT
			SELECT 
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			FROM 
				[dbo].[Visits]

			UPDATE [oso].[stg_OneStep_Visits]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0 OR Imported IS NULL


			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO
