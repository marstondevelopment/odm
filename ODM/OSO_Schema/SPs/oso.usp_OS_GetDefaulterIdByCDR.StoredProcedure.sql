USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterIdByCDR]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDefaulterIdByCDR] @DefaulterName nvarchar(150), @CCaseId int
AS
BEGIN
	select Top 1 [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	where [d].[name] = @DefaulterName and c.id = @CCaseId
END
GO
