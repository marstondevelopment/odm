USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusId]
@OSCaseStatusName varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT 
			Id [CCaseStatusId]
			,name [CCaseStatusName]
		FROM CaseStatus
		WHERE 
			name = CASE @OSCaseStatusName
						WHEN 'Arrangement' THEN 'Unpaid'
						WHEN 'Cancelled' THEN 'Returned'
						WHEN 'Live' THEN 'Unpaid'
						WHEN 'Fully Paid' THEN 'Paid'
						WHEN 'Successful' THEN 'Paid'
						WHEN 'Trace' THEN 'Tracing'
						WHEN 'Expired' THEN 'Returned'
					END
    END
END
GO
