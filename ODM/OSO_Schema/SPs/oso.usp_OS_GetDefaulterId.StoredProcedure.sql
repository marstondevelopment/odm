USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetDefaulterId] @DefaulterName nvarchar(150), @CaseNumber nvarchar(10)
AS
BEGIN
	select [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	JOIN [OldCaseNumbers] [ocn] on [c].[Id] = [ocn].CaseId
	where [d].[name] = @DefaulterName and [ocn].[OldCaseNumber] = @CaseNumber
END
GO
