USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Officers from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 

					@officerno [int],
					@id [int] ,
					@drakeOfficerId [int] ,
					@firstName [varchar](80),
					@middleName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@phone [varchar](25),
					@started [datetime] ,
					@leftDate [datetime],
					@typeId [int] ,
					@maxCases [int],
					@minFee [decimal](18, 4),
					@averageFee [decimal](18, 4),
					@minExecs [int],
					@signature [varchar](50) ,
					@enabled [bit], 
					@performanceTarget  [decimal](10, 2),
					@outcodes [varchar](1000) ,
					@Manager [bit],
					@parentId [int] ,
					@isRtaCertified [bit] ,
					@rtaCertificationExpiryDate [datetime] ,
					@marston [bit] ,
					@rossendales [bit],
					@swift [bit] ,
					@rossendalesdca [bit] ,
					@marstonlaa [bit] ,
					@ctax [bit] ,
					@nndr [bit] ,
					@tma [bit] ,
					@cmg [bit] ,
					@dca [bit] ,
					@laa [bit] ,
					@Existing [bit] ,					
				
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BRossendalesDCAId int,
					@BMarstonLAAId int,
					@RTM_CTId int,
					@CTAX_CTId int,
					@NNDR_CTId int,
					@CMG_CTId int,
					@DCA_CTId int,
					@LAA_CTId int,
					@Comment [varchar] (250)
					
					SELECT @RTM_CTId = Id FROM [dbo].[CaseType] WHERE name = 'RTA/TMA (RoadTraffic/Traffic Management)'				
					SELECT @CTAX_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Council Tax Liability Order'				
					SELECT @NNDR_CTId = Id FROM [dbo].[CaseType] WHERE name = 'National Non Domestic Rates'				
					SELECT @CMG_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Child Maintenance Group'
					SELECT @DCA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Debt Collection'
					SELECT @LAA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Legal Aid'
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						[officerno]
						,[id]
						,[drakeOfficerId]
						,[firstName]
						,[middleName]
						,[lastName]
						,[email]
						,[phone]
						,[started]
						,[leftDate]
						,[typeId]
						,[maxCases]
						,[minFee]
						,[averageFee]
						,[minExecs]
						,[signature]
						,[enabled]
						,[performanceTarget]
						,[outcodes]
						,[Manager]
						,[parentId]
						,[isRtaCertified]
						,[rtaCertificationExpiryDate]
						,[marston]
						,[rossendales]
						,[swift]
						,[rossendalesdca]
						,[marstonlaa]
						,[ctax]
						,[nndr]
						,[tma]
						,[cmg]
						,[dca]
						,[laa]
						,[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Officers]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
					IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColOfficersXRef] WHERE OSId = @Id

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' + @firstName + ' ' + @middleName + ' ' + @lastName;
									THROW 51000, @Comment, 163; 
								
								END								

							END
					ELSE
					BEGIN
						IF NOT EXISTS (SELECT * FROM dbo.[Officers])
						INSERT INTO dbo.[Officers]
							(
							  [officerNumber]
							  ,[drakeOfficerId]
							  ,[firstName]
							  ,[middleName]
							  ,[lastName]
							  ,[email]
							  ,[phone]
							  ,[started]
							  ,[leftDate]
							  ,[typeId]
							  ,[maxCases]
							  ,[minFee]
							  ,[averageFee]
							  ,[minExecs]
							  ,[signature]
							  ,[enabled]
							  ,[performanceTarget]
							  ,[outcodes]
							  ,[isManager]
							  ,[parentId]
							  ,[isRtaCertified]
							  ,[rtaCertificationExpiryDate]	
							)

							VALUES
							(

							  @officerno
							  ,@officerno
							  ,@firstName
							  ,@middleName
							  ,@lastName
							  ,@email
							  ,@phone
							  ,@started
							  ,@leftDate
							  ,@typeId
							  ,@maxCases
							  ,@minFee
							  ,@averageFee
							  ,@minExecs
							  ,@signature
							  ,@enabled
							  ,@performanceTarget
							  ,@outcodes
							  ,@Manager
							  ,@parentId
							  ,@isRtaCertified
							  ,@rtaCertificationExpiryDate								  
							)

							SET @ColId = SCOPE_IDENTITY();

							-- Add OfficersBrands
							IF (@marston = 1)
								BEGIN
									SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands] 
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonId
									)
								END
			
							IF (@rossendales = 1)
								BEGIN
									SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendalesId
									)
								END			
						
							IF (@swift = 1)
								BEGIN
									SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftId
									)
								END		
								
							-- Add to CaseTypeInclusion
							IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
							INSERT INTO dbo.[CaseTypeInclusion]
									(
										[caseTypeId]
										,[officerId]
									)

									VALUES
									(
										@RTM_CTId
										,@ColId
									),									
									(
										@CTAX_CTId
										,@ColId
									),
									(
										@NNDR_CTId
										,@ColId
									),
									(
										@CMG_CTId
										,@ColId
									)

							-- Add to OfficerBrandedSettings table
							IF NOT EXISTS (SELECT * FROM dbo.[OfficerBrandedSettings])
							INSERT INTO dbo.OfficerBrandedSettings
								(
									officerid
									, BrandTypeId
									, OfficerPerformanceBonusSchemeId
									, OfficerRateId
								) 
								
								VALUES 
								( 
									@ColId
									, 1
									, 1
									, 6
								)

							-- Add to XRef table
							IF NOT EXISTS (SELECT * FROM oso.[OSColOfficersXRef])
							INSERT INTO oso.[OSColOfficersXRef]
								(
									[OSOfficerNumber]
									,[OSId]
									,[COfficerNumber]
									,[ColId]
								)

								VALUES
								(
									@officerno
									,@id
									,@officerno
									,@ColId
								)										

							-- New rows creation completed	

					END

					IF (@rossendalesdca = 1)
						BEGIN
							SELECT @BRossendalesDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BRossendalesDCAId
							)
						END	

					IF (@marstonlaa = 1)
						BEGIN
							SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BMarstonLAAId
							)
						END	

					-- Add to CaseTypeInclusion
					IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
					INSERT INTO dbo.[CaseTypeInclusion]
							(
								[caseTypeId]
								,[officerId]
							)
							VALUES
							(
								@DCA_CTId
								,@ColId
							),									
							(
								@LAA_CTId
								,@ColId
							)

					FETCH NEXT
					FROM
						cursorT
					INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Officers]
				  SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0	

				  COMMIT
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
