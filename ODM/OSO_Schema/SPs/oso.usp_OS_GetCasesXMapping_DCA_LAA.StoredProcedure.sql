USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMapping_DCA_LAA]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping_DCA_LAA]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (10,11,12,13)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO
