USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Stages_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Stages_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[stg_OneStep_Stages_CrossRef]
			(
				[PhaseTemplateId]
				,[PhaseTemplateName]
				,[StageTemplateId]
				,[StageTemplateName]
			)
			SELECT
				pt.Id PhaseTempId	
				,pt.Name PhaseTemplate
				,st.Id StageTemplateId
				,st.Name Stage
			FROM 
				WorkflowTemplates wt
				JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
				JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
			
			WHERE 	
				wt.Name = 'Rossendales 14 days 1 Letter'
			ORDER BY
				PhaseTemplate
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END 
END
GO
