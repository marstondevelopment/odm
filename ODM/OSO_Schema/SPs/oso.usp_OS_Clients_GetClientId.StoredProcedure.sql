USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_GetClientId]
@ClientName nvarchar (250),
@CaseType nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @OSClientName nvarchar(500)
		Set @OSClientName = @ClientName + ' (' + @CaseType + ')'
		
		Select top 1 c.Id	  
	   from Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @OSClientName
		AND cct.[abbreviationName] = @CaseType
    END
END
GO
