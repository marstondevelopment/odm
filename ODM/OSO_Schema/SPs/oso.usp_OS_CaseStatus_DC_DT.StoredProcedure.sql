USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_DC_DT]    Script Date: 28/06/2022 16:25:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 22-10-2019
-- Description: To clear [oso].[stg_OneStep_CaseStatus_CrossRef] staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_CaseStatus_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_CaseStatus_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
