USE [Columbus]
GO

/****** Object:  Table [oso].[OneStep_SchemeCharge_CrossRef]    Script Date: 28/06/2022 15:10:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] [varchar](250) NOT NULL,
	[OSSchemeChargeName] [varchar](250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL
) ON [PRIMARY]
GO


