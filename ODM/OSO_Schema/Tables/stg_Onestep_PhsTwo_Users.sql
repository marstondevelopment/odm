USE [Columbus]
GO

/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Users]    Script Date: 28/06/2022 15:27:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_Users](
	[firstName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[Department] [varchar](200) NULL,
	[JobTitle] [varchar](250) NULL,
	[marston] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[marstonLAA] [bit] NOT NULL,
	[rossendaleDCA] [bit] NOT NULL,
	[SwiftDCA] [bit] NOT NULL,
	[MarstonDCA] [bit] NOT NULL,
	[DCA] [bit] NOT NULL,
	[EXP] [bit] NOT NULL,
	[LAA] [bit] NOT NULL,
	[onestepid] [int] NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[Existing] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


