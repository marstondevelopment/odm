USE [Columbus]
GO

/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 28/06/2022 15:12:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OSColOfficersXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSOfficerNumber] [int] NOT NULL,
	[OSId] [int] NULL,
	[COfficerNumber] [int] NOT NULL,
	[ColId] [int] NOT NULL,
 CONSTRAINT [PK_OSColOfficersXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


