USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_Payments]    Script Date: 28/06/2022 15:22:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Payments](
	[cCaseId] [int] NOT NULL,
	[CSchemePaymentId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaymentStatus] [varchar](5) NOT NULL,
	[PaymentSource] [varchar](50) NULL,
	[ClearedOn] [datetime] NULL,
	[SplitDebt] [decimal](18, 4) NULL,
	[SplitCosts] [decimal](18, 4) NULL,
	[SplitFees] [decimal](18, 4) NULL,
	[SplitOther] [decimal](18, 4) NULL,
	[SplitVan] [decimal](18, 4) NULL,
	[SplitVat] [decimal](18, 4) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


