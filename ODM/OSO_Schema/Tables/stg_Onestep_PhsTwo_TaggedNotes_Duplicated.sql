USE [Columbus]
GO

/****** Object:  Table [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated]    Script Date: 28/06/2022 15:27:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[CaseNote] [nvarchar](1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar](250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


