USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_Charges]    Script Date: 28/06/2022 15:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Charges](
	[CCaseId] [int] NOT NULL,
	[CSchemeChargeId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaidAmount] [decimal](18, 4) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


