USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_ContactAddresses]    Script Date: 28/06/2022 15:25:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_ContactAddresses](
	[AddressLine1] [nvarchar](80) NOT NULL,
	[AddressLine2] [nvarchar](730) NULL,
	[AddressLine3] [nvarchar](100) NULL,
	[AddressLine4] [nvarchar](100) NULL,
	[AddressLine5] [nvarchar](100) NULL,
	[Postcode] [varchar](12) NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


