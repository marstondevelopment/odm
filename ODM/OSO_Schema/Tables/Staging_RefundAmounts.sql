USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_RefundAmounts]    Script Date: 28/06/2022 15:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_RefundAmounts](
	[CaseNumber] [varchar](7) NOT NULL,
	[RefundAmount] [decimal](18, 4) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_RefundAmounts] ADD  CONSTRAINT [DF_imported_SRA]  DEFAULT ((0)) FOR [Imported]
GO


