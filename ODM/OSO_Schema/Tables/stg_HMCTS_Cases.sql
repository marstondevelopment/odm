USE [Columbus]
GO

/****** Object:  Table [oso].[stg_HMCTS_Cases]    Script Date: 28/06/2022 15:23:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_HMCTS_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[Currency] [int] NULL,
	[TitleLiable1] [varchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,
	[AddCountryLiable1] [int] NULL,
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[OffenceValue] [decimal](18, 4) NULL,
	[IssuingCourt] [nvarchar](200) NULL,
	[SittingCourt] [nvarchar](200) NULL,
	[WarningMarkers] [nvarchar](1000) NULL,
	[CaseNotes] [nvarchar](1000) NULL,
	[AttachmentInfo] [nvarchar](1000) NULL,
	[BailDate] [datetime] NULL,
	[FUTUserId] [int] NULL,
	[CaseId] [int] NULL,
	[BatchNoGenerated] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_HMCTS_Cases] ADD  CONSTRAINT [DF__stg_HMCTS__Batch__2A5AC89A]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO

ALTER TABLE [oso].[stg_HMCTS_Cases] ADD  CONSTRAINT [DF__stg_HMCTS__Impor__2B4EECD3]  DEFAULT ((0)) FOR [Imported]
GO


