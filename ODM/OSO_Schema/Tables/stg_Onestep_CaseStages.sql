USE [Columbus]
GO

/****** Object:  Table [oso].[stg_Onestep_CaseStages]    Script Date: 28/06/2022 15:24:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_CaseStages](
	[OnestepCaseNumber] [int] NULL,
	[Stage] [varchar](250) NULL,
	[StageDate] [datetime] NULL,
	[Mapped] [bit] NULL,
	[ManualFix] [bit] NULL,
	[Closed] [bit] NULL
) ON [PRIMARY]
GO


