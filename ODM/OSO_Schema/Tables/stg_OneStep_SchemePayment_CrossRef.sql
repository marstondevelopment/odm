USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_SchemePayment_CrossRef]    Script Date: 28/06/2022 15:27:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_SchemePayment_CrossRef](
	[CSchemePaymentName] [varchar](250) NOT NULL,
	[OSSchemePaymentName] [varchar](250) NOT NULL,
	[CSchemePaymentId] [int] NOT NULL
) ON [PRIMARY]
GO


