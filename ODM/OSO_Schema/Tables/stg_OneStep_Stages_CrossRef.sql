USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_Stages_CrossRef]    Script Date: 28/06/2022 15:28:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_Stages_CrossRef](
	[PhaseTemplateId] [int] NOT NULL,
	[PhaseTemplateName] [varchar](50) NOT NULL,
	[StageTemplateId] [int] NOT NULL,
	[StageTemplateName] [varchar](50) NOT NULL
) ON [PRIMARY]
GO


