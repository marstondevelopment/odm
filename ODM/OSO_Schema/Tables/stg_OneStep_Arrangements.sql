USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_Arrangements]    Script Date: 28/06/2022 15:23:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_Arrangements](
	[cCaseId] [int] NOT NULL,
	[Reference] [varchar](500) NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[FirstInstalmentAmount] [decimal](18, 4) NOT NULL,
	[CFrequencyId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[StatusId] [int] NULL,
	[StartDate] [datetime] NULL,
	[LastInstalmentDate] [datetime] NOT NULL,
	[FirstInstalmentDate] [datetime] NOT NULL,
	[NextInstalmentDate] [datetime] NOT NULL,
	[NoOfIntervals] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


