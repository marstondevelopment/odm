USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_Bur_CasesForStatusUpdate]    Script Date: 28/06/2022 15:12:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_Bur_CasesForStatusUpdate](
	[CaseNo] [varchar](7) NOT NULL,
	[Updated] [bit] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_Bur_CasesForStatusUpdate] ADD  DEFAULT ((0)) FOR [Updated]
GO


