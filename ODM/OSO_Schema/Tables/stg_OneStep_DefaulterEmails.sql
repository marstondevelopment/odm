USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_DefaulterEmails]    Script Date: 28/06/2022 15:25:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_DefaulterEmails](
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


