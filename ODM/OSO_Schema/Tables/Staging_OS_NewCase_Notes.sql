USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_NewCase_Notes]    Script Date: 28/06/2022 15:15:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_NewCase_Notes](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_NewCase_Notes] ADD  CONSTRAINT [DF_Staging_OS_NewCase_Notes_Imported]  DEFAULT ((0)) FOR [Imported]
GO


