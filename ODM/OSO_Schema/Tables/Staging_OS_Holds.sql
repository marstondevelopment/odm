USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_Holds]    Script Date: 28/06/2022 15:14:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Holds](
	[cCaseId] [int] NOT NULL,
	[cUserId] [int] NOT NULL,
	[HoldReason] [varchar](700) NULL,
	[HoldOn] [datetime] NOT NULL,
	[HoldUntil] [datetime] NOT NULL,
	[ClientHold] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


