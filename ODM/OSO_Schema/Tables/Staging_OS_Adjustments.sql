USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_Adjustments]    Script Date: 28/06/2022 15:13:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Adjustments](
	[CCaseId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


