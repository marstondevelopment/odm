USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_Vehicles]    Script Date: 28/06/2022 15:28:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_Vehicles](
	[VRM] [varchar](7) NOT NULL,
	[VRMOwner] [bit] NULL,
	[cDefaulterId] [int] NULL,
	[cOfficerId] [int] NULL,
	[DateLoaded] [datetime] NULL,
	[CUserId] [int] NULL,
	[cCaseId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_OneStep_Vehicles] ADD  CONSTRAINT [DF_stg_OneStep_Vehicles_VRMOwner]  DEFAULT ((0)) FOR [VRMOwner]
GO


