USE [Columbus]
GO

/****** Object:  Table [oso].[stg_AW_Cases_Update]    Script Date: 28/06/2022 15:23:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_AW_Cases_Update](
	[CaseId] [int] NOT NULL,
	[CasePrefix] [varchar](1) NOT NULL,
	[TitleLiable1] [varchar](50) NULL,
	[FirstnameLiable1] [varchar](50) NULL,
	[LastnameLiable1] [varchar](50) NULL,
	[TitleLiable2] [varchar](50) NULL,
	[FirstnameLiable2] [varchar](50) NULL,
	[LastnameLiable2] [varchar](50) NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[UploadedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_AW_Cases_Update] ADD  CONSTRAINT [DF__stg_AW_Cases_Update__Imported]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [oso].[stg_AW_Cases_Update] ADD  CONSTRAINT [DF_stg_AW_Cases_Update_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO


