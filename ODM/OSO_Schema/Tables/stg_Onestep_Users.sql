USE [Columbus]
GO

/****** Object:  Table [oso].[stg_Onestep_Users]    Script Date: 28/06/2022 15:28:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_Users](
	[firstName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[Department] [varchar](200) NULL,
	[JobTitle] [varchar](250) NULL,
	[marston] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[onestepid] [int] NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


