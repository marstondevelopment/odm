USE [Columbus]
GO

/****** Object:  Table [oso].[OneStep_ReturnCode_CrossRef]    Script Date: 28/06/2022 15:10:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL,
	[CId] [int] NULL
) ON [PRIMARY]
GO


