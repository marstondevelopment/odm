USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 28/06/2022 15:14:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_History](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Comment] [nvarchar](500) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[HistoryTypeName] [varchar](200) NULL,
	[CaseNoteId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


