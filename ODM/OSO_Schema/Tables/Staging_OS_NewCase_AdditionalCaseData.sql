USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_NewCase_AdditionalCaseData]    Script Date: 28/06/2022 15:14:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData](
	[ClientCaseReference] [varchar](100) NULL,
	[ClientId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](max) NOT NULL,
	[ConcatenatedNameValuePairs] [nvarchar](max) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData] ADD  CONSTRAINT [DF_Staging_OS_NewCase_AdditionalCaseData_Imported]  DEFAULT ((0)) FOR [Imported]
GO


