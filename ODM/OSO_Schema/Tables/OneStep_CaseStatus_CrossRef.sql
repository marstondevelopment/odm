USE [Columbus]
GO

/****** Object:  Table [oso].[OneStep_CaseStatus_CrossRef]    Script Date: 28/06/2022 15:10:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_CaseStatus_CrossRef](
	[OSCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO


