USE [Columbus]
GO

/****** Object:  Table [oso].[stg_OneStep_DefaulterPhones]    Script Date: 28/06/2022 15:25:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_DefaulterPhones](
	[TelephoneNumber] [varchar](50) NOT NULL,
	[TypeID] [int] NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


