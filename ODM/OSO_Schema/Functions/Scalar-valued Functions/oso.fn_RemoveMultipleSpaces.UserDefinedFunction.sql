USE [Columbus]
GO
/****** Object:  UserDefinedFunction [oso].[fn_RemoveMultipleSpaces]    Script Date: 30/06/2022 16:07:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [oso].[fn_RemoveMultipleSpaces] (
	@inputString nvarchar(max) 
)
RETURNS nvarchar(max)
AS
BEGIN
	WHILE CHARINDEX(SPACE(2), @inputString) > 0
	BEGIN
		SET @inputString = REPLACE(@inputString, SPACE(2), SPACE(1))
	END

	RETURN @inputString
END
GO
