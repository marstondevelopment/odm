USE [Columbus]
GO
/****** Object:  Schema [oso]    Script Date: 21/04/2021 14:52:57 ******/
CREATE SCHEMA [oso]
GO
/****** Object:  UserDefinedFunction [oso].[fn_RemoveMultipleSpaces]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [oso].[fn_RemoveMultipleSpaces] (
	@inputString nvarchar(max) 
)
RETURNS nvarchar(max)
AS
BEGIN
	WHILE CHARINDEX(SPACE(2), @inputString) > 0
	BEGIN
		SET @inputString = REPLACE(@inputString, SPACE(2), SPACE(1))
	END

	RETURN @inputString
END
GO
/****** Object:  UserDefinedFunction [oso].[SplitString]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [oso].[SplitString]
    (
        @List NVARCHAR(MAX),
        @Delim VARCHAR(255)
    )
    RETURNS TABLE
    AS
        RETURN ( SELECT [Value], idx = RANK() OVER (ORDER BY n) FROM 
          ( 
            SELECT n = Number, 
              [Value] = LTRIM(RTRIM(SUBSTRING(@List, [Number],
              CHARINDEX(@Delim, @List + @Delim, [Number]) - [Number])))
            FROM (SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
              FROM sys.all_objects) AS x
              WHERE Number <= LEN(@List)
              AND SUBSTRING(@Delim + @List, [Number], LEN(@Delim)) = @Delim
          ) AS y
        );
GO
/****** Object:  Table [oso].[OneStep_CaseStatus_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_CaseStatus_CrossRef](
	[OSCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_ReturnCode_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL,
	[CId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_SchemeCharge_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] [varchar](250) NOT NULL,
	[OSSchemeChargeName] [varchar](250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_SchemePayment_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_SchemePayment_CrossRef](
	[CSchemePaymentName] [varchar](250) NOT NULL,
	[OSSchemePaymentName] [varchar](250) NOT NULL,
	[CSchemePaymentId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_Stages_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_Stages_CrossRef](
	[OSStageName] [varchar](50) NOT NULL,
	[CStageTemplateId] [int] NOT NULL,
	[CPhaseTemplateId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_Workflow_CaseMap]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_Workflow_CaseMap](
	[StageName] [nvarchar](255) NULL,
	[Workflow] [nvarchar](255) NULL,
	[Phase Index] [float] NULL,
	[Columbus Phase] [nvarchar](255) NULL,
	[Stage Index] [float] NULL,
	[Columbus Stage] [nvarchar](255) NULL,
	[Days Dependant] [nvarchar](255) NULL,
	[Day00] [nvarchar](255) NULL,
	[Day01] [nvarchar](255) NULL,
	[Day02] [nvarchar](255) NULL,
	[Day03] [nvarchar](255) NULL,
	[Day04] [nvarchar](255) NULL,
	[Day05] [nvarchar](255) NULL,
	[Day06] [nvarchar](255) NULL,
	[Day07] [nvarchar](255) NULL,
	[Day08] [nvarchar](255) NULL,
	[Day09] [nvarchar](255) NULL,
	[Day10] [nvarchar](255) NULL,
	[Day11] [nvarchar](255) NULL,
	[Day12] [nvarchar](255) NULL,
	[Day13] [nvarchar](255) NULL,
	[Day14] [nvarchar](255) NULL,
	[Day15] [nvarchar](255) NULL,
	[Day16] [nvarchar](255) NULL,
	[Day17] [nvarchar](255) NULL,
	[Day18] [nvarchar](255) NULL,
	[Day19] [nvarchar](255) NULL,
	[Day20] [nvarchar](255) NULL,
	[Day21] [nvarchar](255) NULL,
	[Day22] [nvarchar](255) NULL,
	[Day23] [nvarchar](255) NULL,
	[F32] [nvarchar](255) NULL,
	[WorkflowID] [int] NULL,
	[StageTemplateID] [int] NULL,
	[PhaseTemplateID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OneStep_Workflow_CaseMapP2]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_Workflow_CaseMapP2](
	[StageName] [nvarchar](255) NULL,
	[Workflow] [nvarchar](255) NULL,
	[Phase Index] [float] NULL,
	[Columbus Phase] [nvarchar](255) NULL,
	[Stage Index] [float] NULL,
	[Columbus Stage] [nvarchar](255) NULL,
	[Days Dependant] [nvarchar](255) NULL,
	[Day00] [nvarchar](255) NULL,
	[Day01] [nvarchar](255) NULL,
	[Day02] [nvarchar](255) NULL,
	[Day03] [nvarchar](255) NULL,
	[Day04] [nvarchar](255) NULL,
	[Day05] [nvarchar](255) NULL,
	[Day06] [nvarchar](255) NULL,
	[Day07] [nvarchar](255) NULL,
	[Day08] [nvarchar](255) NULL,
	[Day09] [nvarchar](255) NULL,
	[Day10] [nvarchar](255) NULL,
	[Day11] [nvarchar](255) NULL,
	[Day12] [nvarchar](255) NULL,
	[Day13] [nvarchar](255) NULL,
	[Day14] [nvarchar](255) NULL,
	[Day15] [nvarchar](255) NULL,
	[Day16] [nvarchar](255) NULL,
	[Day17] [nvarchar](255) NULL,
	[Day18] [nvarchar](255) NULL,
	[Day19] [nvarchar](255) NULL,
	[Day20] [nvarchar](255) NULL,
	[Day21] [nvarchar](255) NULL,
	[Day22] [nvarchar](255) NULL,
	[Day23] [nvarchar](255) NULL,
	[Day24] [nvarchar](255) NULL,
	[Day25] [nvarchar](255) NULL,
	[Day26] [nvarchar](255) NULL,
	[Day27] [nvarchar](255) NULL,
	[Day28] [nvarchar](255) NULL,
	[Day29] [nvarchar](255) NULL,
	[Day30] [nvarchar](255) NULL,
	[Day31] [nvarchar](255) NULL,
	[Day32] [nvarchar](255) NULL,
	[Day33] [nvarchar](255) NULL,
	[Day34] [nvarchar](255) NULL,
	[Day35] [nvarchar](255) NULL,
	[Day36] [nvarchar](255) NULL,
	[Day37] [nvarchar](255) NULL,
	[Day38] [nvarchar](255) NULL,
	[Day39] [nvarchar](255) NULL,
	[Day40] [nvarchar](255) NULL,
	[Day41] [nvarchar](255) NULL,
	[Day42] [nvarchar](255) NULL,
	[Day43] [nvarchar](255) NULL,
	[Day44] [nvarchar](255) NULL,
	[Day45] [nvarchar](255) NULL,
	[WorkflowID] [int] NULL,
	[StageTemplateID] [int] NULL,
	[PhaseTemplateID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColCaseTypeXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColCaseTypeXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CaseType] [nvarchar](100) NOT NULL,
	[CaseTypeId] [int] NOT NULL,
 CONSTRAINT [PK_OSColCaseTypeXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColClentsXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColClentsXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Connid] [int] NOT NULL,
	[CClientId] [int] NOT NULL,
	[ClientName] [nvarchar](250) NULL,
 CONSTRAINT [PK_OSColClentsXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColFrequencyXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColFrequencyXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSFrequency] [int] NOT NULL,
	[CFrequency] [varchar](50) NOT NULL,
	[CFrequencyId] [int] NOT NULL,
 CONSTRAINT [PK_OSColFrequencyXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColInstalmentFrequencyXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColInstalmentFrequencyXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSFrequencyDays] [int] NOT NULL,
	[CFrequencyName] [varchar](50) NOT NULL,
	[CFrequencyId] [int] NOT NULL,
 CONSTRAINT [PK_OSColInstalmentFrequencyXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColOfficersXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSOfficerNumber] [int] NOT NULL,
	[OSId] [int] NULL,
	[COfficerNumber] [int] NOT NULL,
	[ColId] [int] NOT NULL,
 CONSTRAINT [PK_OSColOfficersXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[OSColUsersXRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColUsersXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[OSId] [int] NULL,
	[ColId] [int] NOT NULL,
 CONSTRAINT [PK_OSColUsersXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[SQLErrors]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[SQLErrors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ErrorNumber] [int] NULL,
	[Severity] [int] NULL,
	[State] [int] NULL,
	[ErrorProcedure] [nvarchar](128) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[ExecutedOn] [datetime] NULL,
 CONSTRAINT [PK_SQLErrors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_AdditionalCaseData]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_AdditionalCaseData](
	[CaseId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](2000) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_Adjustments]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Adjustments](
	[CCaseId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_BU_DefaulterEmails]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_BU_DefaulterEmails](
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar](7) NULL,
	[cDefaulterId] [int] NOT NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_BU_DefaulterPhones]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_BU_DefaulterPhones](
	[TypeID] [int] NOT NULL,
	[TelephoneNumber] [varchar](50) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar](7) NULL,
	[cDefaulterId] [int] NOT NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_Charges]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Charges](
	[CCaseId] [int] NOT NULL,
	[CSchemeChargeId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaidAmount] [decimal](18, 4) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_DirectPayment]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_DirectPayment](
	[CCaseId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[ClearedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_History](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Comment] [nvarchar](500) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[HistoryTypeName] [varchar](200) NULL,
	[CaseNoteId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_Holds]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Holds](
	[cCaseId] [int] NOT NULL,
	[cUserId] [int] NOT NULL,
	[HoldReason] [varchar](700) NULL,
	[HoldOn] [datetime] NOT NULL,
	[HoldUntil] [datetime] NOT NULL,
	[ClientHold] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_AdditionalCaseData]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData](
	[ClientCaseReference] [varchar](100) NULL,
	[ClientId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](max) NOT NULL,
	[ConcatenatedNameValuePairs] [nvarchar](max) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_Notes]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_Notes](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_Notes_CaseNumber]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber](
	[CaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_Notes]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Notes](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](1000) NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_OnlinePaymentsCaseLookup]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_OnlinePaymentsCaseLookup](
	[ClientRef] [varchar](200) NOT NULL,
	[CaseNumber] [varchar](7) NOT NULL,
	[Client] [varchar](80) NOT NULL,
	[Scheme] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[PaymentDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[Staging_OS_Payments]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Payments](
	[cCaseId] [int] NOT NULL,
	[CSchemePaymentId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaymentStatus] [varchar](5) NOT NULL,
	[PaymentSource] [varchar](50) NULL,
	[ClearedOn] [datetime] NULL,
	[SplitDebt] [decimal](18, 4) NULL,
	[SplitCosts] [decimal](18, 4) NULL,
	[SplitFees] [decimal](18, 4) NULL,
	[SplitOther] [decimal](18, 4) NULL,
	[SplitVan] [decimal](18, 4) NULL,
	[SplitVat] [decimal](18, 4) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_HMCTS_Cases]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_HMCTS_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[Currency] [int] NULL,
	[TitleLiable1] [varchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,
	[AddCountryLiable1] [int] NULL,
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[OffenceValue] [decimal](18, 4) NULL,
	[IssuingCourt] [nvarchar](200) NULL,
	[SittingCourt] [nvarchar](200) NULL,
	[WarningMarkers] [nvarchar](1000) NULL,
	[CaseNotes] [nvarchar](1000) NULL,
	[AttachmentInfo] [nvarchar](1000) NULL,
	[BailDate] [datetime] NULL,
	[FUTUserId] [int] NULL,
	[CaseId] [int] NULL,
	[BatchNoGenerated] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_Arrangements]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Arrangements](
	[cCaseId] [int] NOT NULL,
	[Reference] [varchar](500) NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[FirstInstalmentAmount] [decimal](18, 4) NOT NULL,
	[CFrequencyId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[StatusId] [int] NULL,
	[StartDate] [datetime] NULL,
	[LastInstalmentDate] [datetime] NOT NULL,
	[FirstInstalmentDate] [datetime] NOT NULL,
	[NextInstalmentDate] [datetime] NOT NULL,
	[NoOfIntervals] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_Assignments]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Assignments](
	[cCaseId] [int] NOT NULL,
	[COfficerId] [int] NULL,
	[AssignmentDate] [datetime] NULL,
	[CUserId] [int] NOT NULL,
	[TeamId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_AssignmentsHistory]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_AssignmentsHistory](
	[cCaseId] [int] NOT NULL,
	[COfficerId] [int] NULL,
	[AssignmentDate] [datetime] NULL,
	[CUserId] [int] NOT NULL,
	[TeamId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_Cases]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[VehicleVRM] [varchar](7) NULL,
	[VehicleMake] [varchar](50) NULL,
	[VehicleModel] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[Currency] [int] NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[TitleLiable1] [varchar](50) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[MiddlenameLiable2] [nvarchar](50) NULL,
	[FullnameLiable2] [nvarchar](50) NULL,
	[TitleLiable2] [varchar](50) NULL,
	[CompanyNameLiable2] [nvarchar](50) NULL,
	[FirstnameLiable2] [nvarchar](50) NULL,
	[DOBLiable2] [datetime] NULL,
	[LastnameLiable2] [nvarchar](50) NULL,
	[NINOLiable2] [varchar](13) NULL,
	[MinorLiable2] [bit] NULL,
	[Add1Liable2] [nvarchar](80) NULL,
	[Add2Liable2] [nvarchar](320) NULL,
	[Add3Liable2] [nvarchar](100) NULL,
	[Add4Liable2] [nvarchar](100) NULL,
	[Add5Liable2] [nvarchar](100) NULL,
	[AddCountryLiable2] [int] NULL,
	[AddPostCodeLiable2] [varchar](12) NULL,
	[Phone1Liable2] [varchar](50) NULL,
	[Phone2Liable2] [varchar](50) NULL,
	[Phone3Liable2] [varchar](50) NULL,
	[Phone4Liable2] [varchar](50) NULL,
	[Phone5Liable2] [varchar](50) NULL,
	[Email1Liable2] [varchar](250) NULL,
	[Email2Liable2] [varchar](250) NULL,
	[Email3Liable2] [varchar](250) NULL,
	[MiddlenameLiable3] [nvarchar](50) NULL,
	[FullnameLiable3] [nvarchar](150) NULL,
	[TitleLiable3] [varchar](50) NULL,
	[CompanyNameLiable3] [nvarchar](50) NULL,
	[FirstnameLiable3] [nvarchar](50) NULL,
	[DOBLiable3] [datetime] NULL,
	[LastnameLiable3] [nvarchar](50) NULL,
	[NINOLiable3] [varchar](13) NULL,
	[MinorLiable3] [bit] NULL,
	[Add1Liable3] [nvarchar](80) NULL,
	[Add2Liable3] [nvarchar](320) NULL,
	[Add3Liable3] [nvarchar](100) NULL,
	[Add4Liable3] [nvarchar](100) NULL,
	[Add5Liable3] [nvarchar](100) NULL,
	[AddCountryLiable3] [int] NULL,
	[AddPostCodeLiable3] [varchar](12) NULL,
	[Phone1Liable3] [varchar](50) NULL,
	[Phone2Liable3] [varchar](50) NULL,
	[Phone3Liable3] [varchar](50) NULL,
	[Phone4Liable3] [varchar](50) NULL,
	[Phone5Liable3] [varchar](50) NULL,
	[Email1Liable3] [varchar](250) NULL,
	[Email2Liable3] [varchar](250) NULL,
	[Email3Liable3] [varchar](250) NULL,
	[MiddlenameLiable4] [nvarchar](50) NULL,
	[FullnameLiable4] [nvarchar](150) NULL,
	[TitleLiable4] [varchar](50) NULL,
	[CompanyNameLiable4] [nvarchar](50) NULL,
	[FirstnameLiable4] [nvarchar](50) NULL,
	[DOBLiable4] [datetime] NULL,
	[LastnameLiable4] [nvarchar](50) NULL,
	[NINOLiable4] [varchar](13) NULL,
	[MinorLiable4] [bit] NULL,
	[Add1Liable4] [nvarchar](80) NULL,
	[Add2Liable4] [nvarchar](320) NULL,
	[Add3Liable4] [nvarchar](100) NULL,
	[Add4Liable4] [nvarchar](100) NULL,
	[Add5Liable4] [nvarchar](100) NULL,
	[AddCountryLiable4] [int] NULL,
	[AddPostCodeLiable4] [varchar](12) NULL,
	[Phone1Liable4] [varchar](50) NULL,
	[Phone2Liable4] [varchar](50) NULL,
	[Phone3Liable4] [varchar](50) NULL,
	[Phone4Liable4] [varchar](50) NULL,
	[Phone5Liable4] [varchar](50) NULL,
	[Email1Liable4] [varchar](250) NULL,
	[Email2Liable4] [varchar](250) NULL,
	[Email3Liable4] [varchar](250) NULL,
	[MiddlenameLiable5] [nvarchar](50) NULL,
	[FullnameLiable5] [nvarchar](150) NULL,
	[TitleLiable5] [varchar](50) NULL,
	[CompanyNameLiable5] [nvarchar](50) NULL,
	[FirstnameLiable5] [nvarchar](50) NULL,
	[DOBLiable5] [datetime] NULL,
	[LastnameLiable5] [nvarchar](50) NULL,
	[NINOLiable5] [varchar](13) NULL,
	[MinorLiable5] [bit] NULL,
	[Add1Liable5] [nvarchar](80) NULL,
	[Add2Liable5] [nvarchar](320) NULL,
	[Add3Liable5] [nvarchar](100) NULL,
	[Add4Liable5] [nvarchar](100) NULL,
	[Add5Liable5] [nvarchar](100) NULL,
	[AddCountryLiable5] [int] NULL,
	[AddPostCodeLiable5] [varchar](12) NULL,
	[Phone1Liable5] [varchar](50) NULL,
	[Phone2Liable5] [varchar](50) NULL,
	[Phone3Liable5] [varchar](50) NULL,
	[Phone4Liable5] [varchar](50) NULL,
	[Phone5Liable5] [varchar](50) NULL,
	[Email1Liable5] [varchar](250) NULL,
	[Email2Liable5] [varchar](250) NULL,
	[Email3Liable5] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[OneStepCaseNumber] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[ConnId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[DefaultersNames] [nvarchar](1000) NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,
	[EmployerName] [nvarchar](150) NULL,
	[EmployerAddressLine1] [nvarchar](80) NULL,
	[EmployerAddressLine2] [nvarchar](320) NULL,
	[EmployerAddressLine3] [nvarchar](100) NULL,
	[EmployerAddressLine4] [nvarchar](100) NULL,
	[EmployerPostcode] [varchar](12) NULL,
	[EmployerEmailAddress] [varchar](50) NULL,
	[AddCountryLiable1] [int] NULL,
	[EmployerFaxNumber] [varchar](50) NULL,
	[EmployerTelephone] [varchar](50) NULL,
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[Welfare] [bit] NULL,
	[DateOfBirth] [datetime] NULL,
	[NINO] [nvarchar](13) NULL,
	[OffenceValue] [decimal](18, 4) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[BatchNoGenerated] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_CaseStages]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_CaseStages](
	[OnestepCaseNumber] [int] NULL,
	[Stage] [varchar](250) NULL,
	[StageDate] [datetime] NULL,
	[Mapped] [bit] NULL,
	[ManualFix] [bit] NULL,
	[Closed] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_CaseStatus_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_CaseStatus_CrossRef](
	[OSCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_CaseStatuses]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_CaseStatuses](
	[cCaseId] [int] NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_Clients]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_Clients](
	[connid] [int] NULL,
	[Id] [int] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Abbreviation] [nvarchar](80) NOT NULL,
	[ParentClientId] [int] NULL,
	[firstLine] [nvarchar](80) NULL,
	[PostCode] [varchar](12) NOT NULL,
	[Address] [nvarchar](500) NULL,
	[CountryId] [nvarchar](100) NULL,
	[RegionId] [int] NULL,
	[Brand] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CaseTypeId] [int] NOT NULL,
	[AbbreviationName] [nvarchar](80) NOT NULL,
	[BatchRefPrefix] [varchar](50) NULL,
	[ContactName] [nvarchar](256) NULL,
	[ContactAddress] [nvarchar](500) NULL,
	[OfficeId] [int] NOT NULL,
	[PaymentDistributionId] [int] NOT NULL,
	[PriorityCharges] [varchar](50) NOT NULL,
	[LifeExpectancy] [int] NOT NULL,
	[FeeCap] [decimal](18, 4) NOT NULL,
	[minFees] [decimal](18, 4) NOT NULL,
	[MaximumArrangementLength] [int] NULL,
	[ClientPaid] [bit] NOT NULL,
	[OfficerPaid] [bit] NOT NULL,
	[permitArrangement] [bit] NOT NULL,
	[NotifyAddressChange] [bit] NOT NULL,
	[Reinstate] [bit] NOT NULL,
	[AssignMatches] [bit] NOT NULL,
	[showNotes] [bit] NOT NULL,
	[showHistory] [bit] NOT NULL,
	[IsDirectHoldWithLifeExtPastExpiryAllowed] [bit] NOT NULL,
	[xrefEmail] [varchar](512) NULL,
	[PaymentRunFrequencyId] [int] NULL,
	[InvoiceRunFrequencyId] [int] NULL,
	[commissionInvoiceFrequencyId] [int] NULL,
	[isSuccessfulCompletionFee] [bit] NOT NULL,
	[successfulCompletionFee] [decimal](18, 4) NOT NULL,
	[completionFeeType] [bit] NOT NULL,
	[feeInvoiceFrequencyId] [int] NULL,
	[standardReturn] [bit] NOT NULL,
	[paymentMethod] [int] NULL,
	[chargingScheme] [varchar](50) NOT NULL,
	[paymentScheme] [varchar](50) NOT NULL,
	[enforceAtNewAddress] [bit] NOT NULL,
	[defaultHoldTimePeriod] [int] NOT NULL,
	[addressChangeEmail] [varchar](512) NULL,
	[addressChangeStage] [varchar](150) NULL,
	[newAddressReturnCode] [int] NULL,
	[autoReturnExpiredCases] [bit] NOT NULL,
	[generateBrokenLetter] [bit] NOT NULL,
	[useMessageExchange] [bit] NOT NULL,
	[costCap] [decimal](18, 4) NOT NULL,
	[includeUnattendedReturns] [bit] NOT NULL,
	[assignmentSchemeCharge] [int] NULL,
	[expiredCasesAssignable] [bit] NOT NULL,
	[useLocksmithCard] [bit] NOT NULL,
	[EnableAutomaticLinking] [bit] NOT NULL,
	[linkedCasesMoneyDistribution] [int] NOT NULL,
	[isSecondReferral] [bit] NOT NULL,
	[PermitGoodsRemoved] [bit] NOT NULL,
	[EnableManualLinking] [bit] NOT NULL,
	[PermitVRM] [bit] NOT NULL,
	[IsClientInvoiceRunPermitted] [bit] NOT NULL,
	[IsNegativeRemittancePermitted] [bit] NOT NULL,
	[ContactCentrePhoneNumber] [varchar](50) NULL,
	[AutomatedLinePhoneNumber] [varchar](50) NULL,
	[TraceCasesViaWorkflow] [bit] NOT NULL,
	[IsTecAuthorizationApplicable] [bit] NOT NULL,
	[TecDefaultHoldPeriod] [int] NOT NULL,
	[MinimumDaysRemainingForCoa] [int] NOT NULL,
	[TecMinimumDaysRemaining] [int] NOT NULL,
	[IsDecisioningViaWorkflowUsed] [bit] NOT NULL,
	[AllowReverseFeesOnPrimaryAddressChanged] [bit] NOT NULL,
	[IsClientInformationExchangeScheme] [bit] NOT NULL,
	[ClientLifeExpectancy] [int] NULL,
	[DaysToExtendCaseDueToTrace] [int] NULL,
	[DaysToExtendCaseDueToArrangement] [int] NULL,
	[IsWelfareReferralPermitted] [bit] NOT NULL,
	[IsFinancialDifficultyReferralPermitted] [bit] NOT NULL,
	[IsReturnCasePrematurelyPermitted] [bit] NOT NULL,
	[CaseAgeForTBTPEnquiry] [int] NULL,
	[PermitDvlaEnquiries] [bit] NOT NULL,
	[IsAutomaticDvlaRecheckEnabled] [bit] NOT NULL,
	[DaysToAutomaticallyRecheckDvla] [int] NULL,
	[IsComplianceFeeAutoReversed] [bit] NOT NULL,
	[LetterActionTemplateRequiredForCoaInDataCleanseId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInComplianceId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInEnforcementId] [int] NULL,
	[LetterActionTemplateRequiredForCoaInCaseMonitoringId] [int] NULL,
	[CategoryRequiredForCoaInDataCleanseId] [int] NULL,
	[CategoryRequiredForCoaInComplianceId] [int] NULL,
	[CategoryRequiredForCoaInEnforcementId] [int] NULL,
	[CategoryRequiredForCoaInCaseMonitoringId] [int] NULL,
	[IsCoaWithActiveArrangementAllowed] [bit] NOT NULL,
	[ReturnCap] [int] NULL,
	[CurrentReturnCap] [int] NULL,
	[IsLifeExpectancyForCoaEnabled] [bit] NOT NULL,
	[LifeExpectancyForCoa] [int] NOT NULL,
	[ExpiredCaseReturnCode] [int] NULL,
	[WorkflowName] [varchar](255) NULL,
	[OneOffArrangementTolerance] [int] NULL,
	[DailyArrangementTolerance] [int] NULL,
	[TwoDaysArrangementTolerance] [int] NULL,
	[WeeklyArrangementTolerance] [int] NULL,
	[FortnightlyArrangementTolerance] [int] NULL,
	[ThreeWeeksArrangementTolerance] [int] NULL,
	[FourWeeksArrangementTolerance] [int] NULL,
	[MonthlyArrangementTolerance] [int] NULL,
	[ThreeMonthsArrangementTolerance] [int] NULL,
	[PDAPaymentMethodId] [varchar](50) NOT NULL,
	[Payee] [varchar](150) NULL,
	[IsDirectPaymentValid] [bit] NOT NULL,
	[IsDirectPaymentValidForAutomaticallyLinkedCases] [bit] NOT NULL,
	[IsChargeConsidered] [bit] NOT NULL,
	[Interest] [bit] NOT NULL,
	[InvoicingCurrencyId] [int] NOT NULL,
	[RemittanceCurrencyId] [int] NOT NULL,
	[IsPreviousAddressToBePrimaryAllowed] [bit] NOT NULL,
	[TriggerCOA] [bit] NOT NULL,
	[ShouldOfficerRemainAssigned] [bit] NOT NULL,
	[ShouldOfficerRemainAssignedForEB] [bit] NOT NULL,
	[PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun] [bit] NOT NULL,
	[AllowWorkflowToBlockProcessingIfLinkedToGANTCase] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[AreaId] [int] NULL,
	[CourtId] [int] NULL,
	[IsReturnForAPartPaidCasePermitted] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_ContactAddresses]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_ContactAddresses](
	[AddressLine1] [nvarchar](80) NOT NULL,
	[AddressLine2] [nvarchar](730) NULL,
	[AddressLine3] [nvarchar](100) NULL,
	[AddressLine4] [nvarchar](100) NULL,
	[AddressLine5] [nvarchar](100) NULL,
	[Postcode] [varchar](12) NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_DefaulterEmails]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_DefaulterEmails](
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_DefaulterPhones]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_DefaulterPhones](
	[TelephoneNumber] [varchar](50) NOT NULL,
	[TypeID] [int] NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_Officers]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_Officers](
	[officerno] [int] NOT NULL,
	[id] [int] NULL,
	[drakeOfficerId] [int] NULL,
	[firstName] [varchar](80) NULL,
	[middleName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[phone] [varchar](25) NULL,
	[started] [datetime] NOT NULL,
	[leftDate] [datetime] NULL,
	[typeId] [int] NOT NULL,
	[maxCases] [int] NOT NULL,
	[minFee] [decimal](18, 4) NOT NULL,
	[averageFee] [decimal](18, 4) NOT NULL,
	[minExecs] [int] NOT NULL,
	[signature] [varchar](50) NULL,
	[enabled] [bit] NOT NULL,
	[performanceTarget] [decimal](10, 2) NOT NULL,
	[outcodes] [varchar](1000) NOT NULL,
	[Manager] [bit] NULL,
	[parentId] [int] NULL,
	[isRtaCertified] [bit] NOT NULL,
	[rtaCertificationExpiryDate] [datetime] NULL,
	[marston] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[ctax] [bit] NOT NULL,
	[nndr] [bit] NOT NULL,
	[tma] [bit] NOT NULL,
	[cmg] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Officers]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_Officers](
	[officerno] [int] NOT NULL,
	[id] [int] NULL,
	[drakeOfficerId] [int] NULL,
	[firstName] [varchar](80) NULL,
	[middleName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[phone] [varchar](25) NULL,
	[started] [datetime] NOT NULL,
	[leftDate] [datetime] NULL,
	[typeId] [int] NOT NULL,
	[maxCases] [int] NOT NULL,
	[minFee] [decimal](18, 4) NOT NULL,
	[averageFee] [decimal](18, 4) NOT NULL,
	[minExecs] [int] NOT NULL,
	[signature] [varchar](50) NULL,
	[enabled] [bit] NOT NULL,
	[performanceTarget] [decimal](10, 2) NOT NULL,
	[outcodes] [varchar](1000) NOT NULL,
	[Manager] [bit] NULL,
	[parentId] [int] NULL,
	[isRtaCertified] [bit] NOT NULL,
	[rtaCertificationExpiryDate] [datetime] NULL,
	[marston] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendalesdca] [bit] NOT NULL,
	[marstonlaa] [bit] NOT NULL,
	[ctax] [bit] NULL,
	[nndr] [bit] NULL,
	[tma] [bit] NULL,
	[cmg] [bit] NULL,
	[dca] [bit] NOT NULL,
	[laa] [bit] NOT NULL,
	[Existing] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_TaggedNotes]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[CaseNote] [nvarchar](1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar](250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[CaseNote] [nvarchar](1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar](250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[CaseNote] [nvarchar](1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar](250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Users]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_Users](
	[firstName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[Department] [varchar](200) NULL,
	[JobTitle] [varchar](250) NULL,
	[marston] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[marstonLAA] [bit] NOT NULL,
	[rossendaleDCA] [bit] NOT NULL,
	[SwiftDCA] [bit] NOT NULL,
	[MarstonDCA] [bit] NOT NULL,
	[DCA] [bit] NOT NULL,
	[EXP] [bit] NOT NULL,
	[LAA] [bit] NOT NULL,
	[onestepid] [int] NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[Existing] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_ReturnCode_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_SchemeCharge_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] [varchar](250) NOT NULL,
	[OSSchemeChargeName] [varchar](250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_SchemePayment_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_SchemePayment_CrossRef](
	[CSchemePaymentName] [varchar](250) NOT NULL,
	[OSSchemePaymentName] [varchar](250) NOT NULL,
	[CSchemePaymentId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_Stages_CrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Stages_CrossRef](
	[PhaseTemplateId] [int] NOT NULL,
	[PhaseTemplateName] [varchar](50) NOT NULL,
	[StageTemplateId] [int] NOT NULL,
	[StageTemplateName] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_Onestep_Users]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_Users](
	[firstName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[Department] [varchar](200) NULL,
	[JobTitle] [varchar](250) NULL,
	[marston] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[onestepid] [int] NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_Vehicles]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Vehicles](
	[VRM] [varchar](7) NOT NULL,
	[VRMOwner] [bit] NULL,
	[cDefaulterId] [int] NULL,
	[cOfficerId] [int] NULL,
	[DateLoaded] [datetime] NULL,
	[CUserId] [int] NULL,
	[cCaseId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [oso].[stg_OneStep_Visits]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Visits](
	[CCaseId] [int] NOT NULL,
	[DefaulterName] [nvarchar](150) NOT NULL,
	[VisitType] [varchar](50) NOT NULL,
	[VisitDate] [datetime] NULL,
	[AddressLine1] [varchar](80) NULL,
	[AddressLine2] [varchar](100) NULL,
	[AddressLine3] [varchar](100) NULL,
	[AddressLine4] [varchar](100) NULL,
	[AddressLine5] [varchar](100) NULL,
	[Postcode] [varchar](12) NULL,
	[COfficerId] [int] NOT NULL,
	[BuildingType] [varchar](50) NULL,
	[DoorColour] [varchar](50) NULL,
	[Notes] [varchar](1000) NULL,
	[ProofOfIdentity] [varchar](50) NULL,
	[PersonContacted] [varchar](50) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_AdditionalCaseData] ADD  CONSTRAINT [DF_Staging_OS_AdditionalCaseData_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterEmails] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterEmails_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterEmails] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterEmails_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterPhones_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterPhones_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData] ADD  CONSTRAINT [DF_Staging_OS_NewCase_AdditionalCaseData_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_Notes] ADD  CONSTRAINT [DF_Staging_OS_NewCase_Notes_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber] ADD  CONSTRAINT [DF_Staging_OS_NewCase_Notes_CaseNumber_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[Staging_OS_Notes] ADD  CONSTRAINT [DF__Staging_O__Impor__19E70EA4]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_HMCTS_Cases] ADD  CONSTRAINT [DF__stg_HMCTS__Batch__2A5AC89A]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO
ALTER TABLE [oso].[stg_HMCTS_Cases] ADD  CONSTRAINT [DF__stg_HMCTS__Impor__2B4EECD3]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_Onestep_Cases] ADD  CONSTRAINT [DF_stg_Onestep_Cases_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_Onestep_Cases] ADD  CONSTRAINT [DF_stg_Onestep_Cases_BatchNoGenerated]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO
ALTER TABLE [oso].[stg_Onestep_Clients] ADD  CONSTRAINT [DF_stg_Onestep_Clients_Imported]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_OneStep_Vehicles] ADD  CONSTRAINT [DF_stg_OneStep_Vehicles_VRMOwner]  DEFAULT ((0)) FOR [VRMOwner]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSCaseNumber]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_CheckOSCaseNumber]
@CaseNumber nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		SELECT 
			@Id = [ocn].[CaseId]	  
		FROM 
			[dbo].[OldCaseNumbers] [ocn]		  
		WHERE 
			[ocn].[OldCaseNumber] = @CaseNumber		

		SELECT 
			CASE WHEN @Id is null THEN 1	ELSE 0 END AS Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
--CREATE PROCEDURE [oso].[usp_CheckOSClientName]
--@Name nvarchar (250),
--@CaseType nvarchar (250),
--@Brand int

--AS
--BEGIN
--	SET NOCOUNT ON;
--    BEGIN

--		DECLARE @Id INT;		
--		DECLARE @OSClientName varchar(500)
--		Set @OSClientName = @Name + ' ' + @CaseType
		
--		Select @Id = c.Id	  
--	   from dbo.Clients c 
--		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
--		where 
--		c.[name] = @OSClientName
--		AND cct.[BrandId] = @Brand

--		Select 
--			CASE
--				WHEN @Id is null THEN 1
--				ELSE 0
--			END AS 	Valid

--    END
--END
--GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSOfficer]
@drakeOfficerId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;				
		Select @Id = o.Id	  
	   from  dbo.[Officers] o 
		where 
		o.[officerNumber] = @drakeOfficerId 
		OR
		o.[drakeOfficerId] = @drakeOfficerId 		 
		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS user is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSUser]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @RecCount INT;				
		Select @RecCount = count(*) From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
		Select 
			CASE
				WHEN @RecCount = 0 THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetAreasXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetAreasXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		--SELECT DISTINCT	
		--	[a].[Id] [AreaId],
		--	[a].[Name] [Area]		
		--FROM
		--	[dbo].[Regions] [r]
		--	INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
		--	INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		SELECT 
			[Id] [AreaId], 
			[Name] [Area] 
		FROM 
			[Regions] 
		WHERE 
			[ParentId] IN (select [Id] from [Regions] where [ParentId] IS NULL)
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetCourtsXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetCourtsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT DISTINCT	
			[c].[Id] [CourtId],
			[c].[Name] [Court]	
		FROM
			[dbo].[Regions] [r]
			INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
			INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		--select Id CourtId, Name Court from Regions where ParentId in (select Id from Regions where ParentId in (select Id from Regions where ParentId is null))
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_GetRegionsXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetRegionsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

	SELECT 
		[r].[Id] [RegionId],	
		[r].[Name] [Region]
		
	FROM
		[dbo].[Regions] [r]
	WHERE
		[r].[ParentId] IS NULL

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_AddBatches]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_AddBatches]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			DECLARE @CurrentDate DateTime = GetDate();

            INSERT into batches
                (
                [referenceNumber],
                [batchDate],
                [closedDate],
                [createdBy],
                [manual],
                [deleted],
                [crossReferenceSent],
                [crossReferenceSentDate],
                [clientCaseTypeId],
                [loadedOn],
                [loadedBy],
                [receivedOn],
                [summaryDate]
                )
            SELECT DISTINCT
                CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112))[referenceNumber]
                ,CAST(CONVERT(date,osc.batchdate) AS DateTime) [batchDate]
                ,@CurrentDate [closedDate]
                ,2 [createdBy]
                ,1 [manual]
                ,0 [deleted]
                ,1 [crossReferenceSent]
                ,@CurrentDate [crossReferenceSentDate]
                ,cct.id [clientCaseTypeId]
                ,@CurrentDate [loadedOn]
                ,2 [loadedBy]
                ,@CurrentDate [receivedOn]
                ,NULL [summaryDate]
            FROM
                oso.stg_HMCTS_Cases osc
                join clientcasetype cct on osc.clientid = cct.clientid
            WHERE
                osc.imported = 0
                AND osc.ErrorID IS NULL
                AND osc.[BatchNoGenerated] = 0
                AND CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) NOT IN (
               
                    SELECT DISTINCT
                        b.[referenceNumber]
                    FROM
                        batches b
                        join clientcasetype cct on b.clientcasetypeid = cct.id
                        join oso.stg_HMCTS_Cases osc on b.batchDate = CAST(CONVERT(date,osc.batchdate) AS DateTime) AND cct.clientid = osc.ClientId
                    WHERE
                        CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) = b.referenceNumber
                                        AND osc.imported = 1
                        AND osc.[BatchNoGenerated] = 1
                )
            GROUP BY
                cct.id,
                cct.batchrefprefix,
                osc.batchdate

		END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_Cases_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_HMCTS_AddBatches;
		--UPDATE oso.stg_HMCTS_Cases SET [BatchNoGenerated] = 1 WHERE imported = 0 AND ErrorID IS NULL AND [BatchNoGenerated] = 0
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@BatchDate datetime ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(80) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod varchar(20) ,
			@AddCountryLiable1 int ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,	
			@OffenceValue decimal(18,4),
			@IssuingCourt nvarchar(200) ,	
			@SittingCourt nvarchar(200) ,	
			@WarningMarkers nvarchar(1000) ,	
			@CaseNotes nvarchar(1000) ,	
			@AttachmentInfo nvarchar(1000) 	,
			@BailDate datetime 
     
			, @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @UnPaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int
			,@FileInfo nvarchar(500) 	
			,@FileName nvarchar(500) 	
			,@FullFileName nvarchar(500) 	
			,@WM nvarchar(500)
			,@WMCode nvarchar(15)
			,@WMDate nvarchar(50)
			,@WMDescription nvarchar(500) 
			,@UserId int = 2;
		
		DECLARE @WMTbl TABLE(VALUE nvarchar(500));

		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			  ClientCaseReference
			, ClientDefaulterReference
			, IssueDate
			, OffenceCode
			, OffenceDescription
			, OffenceLocation
			, OffenceCourt
			, DebtAddress1
			, DebtAddress2
			, DebtAddress3
			, DebtAddress4
			, DebtAddress5
			, DebtAddressCountry
			, DebtAddressPostcode
			, StartDate
			, EndDate
			, TECDate
			, FineAmount
			, Currency
			, TitleLiable1
			, FirstnameLiable1
			, MiddlenameLiable1
			, LastnameLiable1
			, FullnameLiable1
			, CompanyNameLiable1
			, DOBLiable1
			, NINOLiable1
			, MinorLiable1
			, Add1Liable1
			, Add2Liable1
			, Add3Liable1
			, Add4Liable1
			, Add5Liable1
			, AddPostCodeLiable1
			, IsBusinessAddress
			, Phone1Liable1
			, Phone2Liable1
			, Phone3Liable1
			, Phone4Liable1
			, Phone5Liable1
			, Email1Liable1
			, Email2Liable1
			, Email3Liable1
			, BatchDate
			, ClientId
			, ClientName
			, PhaseDate
			, StageDate
			, IsAssignable
			, OffenceDate
			, CreatedOn
			, BillNumber
			, ClientPeriod
			, AddCountryLiable1
			, RollNumber
			, Occupation
			, BenefitIndicator
			, CStatus
			, CReturnCode
			, OffenceNotes
			, OffenceValue
			, IssuingCourt
			, SittingCourt
			, WarningMarkers
			, CaseNotes
			, AttachmentInfo
			, BailDate

		FROM
                 oso.[stg_HMCTS_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientCaseReference
			, @ClientDefaulterReference
			, @IssueDate
			, @OffenceCode
			, @OffenceDescription
			, @OffenceLocation
			, @OffenceCourt
			, @DebtAddress1
			, @DebtAddress2
			, @DebtAddress3
			, @DebtAddress4
			, @DebtAddress5
			, @DebtAddressCountry
			, @DebtAddressPostcode
			, @StartDate
			, @EndDate
			, @TECDate
			, @FineAmount
			, @Currency
			, @TitleLiable1
			, @FirstnameLiable1
			, @MiddlenameLiable1
			, @LastnameLiable1
			, @FullnameLiable1
			, @CompanyNameLiable1
			, @DOBLiable1
			, @NINOLiable1
			, @MinorLiable1
			, @Add1Liable1
			, @Add2Liable1
			, @Add3Liable1
			, @Add4Liable1
			, @Add5Liable1
			, @AddPostCodeLiable1
			, @IsBusinessAddress
			, @Phone1Liable1
			, @Phone2Liable1
			, @Phone3Liable1
			, @Phone4Liable1
			, @Phone5Liable1
			, @Email1Liable1
			, @Email2Liable1
			, @Email3Liable1
			, @BatchDate
			, @ClientId
			, @ClientName
			, @PhaseDate
			, @StageDate
			, @IsAssignable
			, @OffenceDate
			, @CreatedOn
			, @BillNumber
			, @ClientPeriod
			, @AddCountryLiable1
			, @RollNumber
			, @Occupation
			, @BenefitIndicator
			, @CStatus
			, @CReturnCode
			, @OffenceNotes
			, @OffenceValue
			, @IssuingCourt
			, @SittingCourt
			, @WarningMarkers
			, @CaseNotes
			, @AttachmentInfo
			, @BailDate

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						-- Check and update BatchId
						SET @CurrentBatchId = NULL
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						IF (@CurrentBatchId IS NULL)
						BEGIN
							DECLARE @CurrentDate DateTime = GetDate();
							INSERT into batches
								(
								[referenceNumber],
								[batchDate],
								[closedDate],
								[createdBy],
								[manual],
								[deleted],
								[crossReferenceSent],
								[crossReferenceSentDate],
								[clientCaseTypeId],
								[loadedOn],
								[loadedBy],
								[receivedOn],
								[summaryDate]
								)

							SELECT 
								CONCAT(cct.batchrefprefix,CONVERT(varchar(8),@BatchDate,112))[referenceNumber]
								,CAST(CONVERT(date,@BatchDate) AS DateTime) [batchDate]
								,@CurrentDate [closedDate]
								,2 [createdBy]
								,1 [manual]
								,0 [deleted]
								,1 [crossReferenceSent]
								,@CurrentDate [crossReferenceSentDate]
								,cct.id [clientCaseTypeId]
								,@CurrentDate [loadedOn]
								,2 [loadedBy]
								,@CurrentDate [receivedOn]
								,NULL [summaryDate]
							FROM
								clientcasetype cct
							WHERE
								cct.clientid = @ClientId
						
							SET @CurrentBatchId = SCOPE_IDENTITY();

							-- Set the BatchIDGenerated flag to true in staging
							UPDATE
							[oso].[stg_HMCTS_Cases]
							SET    BatchNoGenerated = 1
							WHERE
							Imported = 0 
							AND ErrorId is NULL
							AND ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						END


						
						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT
							@StageId = (s.id)
						FROM
							clients cl
							join stages s on s.clientId = cl.id
						WHERE
							s.onLoad = 1
							and cl.id = @Clientid
						
						SET @CStatusId = @UnPaidStatusId;
						SET @ReturnRunId = NULL

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL
						
						IF (@CaseTypeId = 3 )
						BEGIN
							
							SET @OffenceCode =  'BWB'
						END
						IF (@CaseTypeId = 19 )
						BEGIN
							
							SET @OffenceCode =  'BWNB'
						END

						IF (@CaseTypeId = 1 )
						BEGIN
							
							SET @OffenceCode =  'AWB'
						END

						SET @cnid = (  
                                    SELECT MIN(cn.Id)  
                                    FROM CaseNumbers AS cn WITH (  
                                        XLOCK  
                                        ,HOLDLOCK  
                                        )  
                                    WHERE cn.Reserved = 0)

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,''))) --[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @ClientPeriod--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						
					--  Insert Case Note
						INSERT INTO dbo.[CaseNotes] 
						( [caseId]
							, [userId]
							, [officerId]
							, [text]
							, [occured]
							, [visible]
							, [groupId]
							, [TypeId]
						)
						VALUES
						(
							@CaseId,
							@UserId,
							NULL,
							@CaseNotes,
							@CreatedOn,
							1,
							NULL,
							NULL
						)

					-- Insert Warning Markers as Offence Code.
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						VALUES
						(
							@CaseId,
							CONVERT(datetime, @createdon, 103),
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId,
							@OffenceValue
						);

					IF (@WarningMarkers IS NOT NULL)
					BEGIN
						INSERT INTO @WMTbl
						SELECT Value 
						FROM oso.splitstring(@WarningMarkers,'|');

						WHILE EXISTS(SELECT TOP 1 Value FROM @WMTbl)
						BEGIN
							SELECT TOP 1 @WM = Value FROM @WMTbl
							SELECT @WMCode = Value FROM oso.SplitString(@WM,';') WHERE idx = 1;
							SELECT @WMDate = Value FROM oso.SplitString(@WM,';') WHERE idx = 2;
							SELECT @WMDescription = Value FROM oso.SplitString(@WM,';') WHERE idx = 3;

							SELECT @OffenceCodeId = Id From Offences WHERE Code = @WMCode;
							INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
							VALUES
							(
								@CaseId,
								CONVERT(datetime, @WMDate, 103),
								NULL,
								@WMDescription,
								@OffenceCodeId,
								NULL
							);

							DELETE FROM @WMTbl WHERE VALUE = @WM;
						END
					END

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DOBLiable1,
						0,
						0,
						NULL,
						0,
						@NINOLiable1,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone1Liable1,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone2Liable1,
								2,
								@CreatedOn,
								1
							)
						END

						IF (@Phone3Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone3Liable1,
								2,
								@CreatedOn,
								1
							)
						END
						IF (@Phone4Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone4Liable1,
								2,
								@CreatedOn,
								1
							)
						END
						IF (@Phone5Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone5Liable1,
								2,
								@CreatedOn,
								1
							)
						END

						-- Insert Defaulter Email
						IF (@Email1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Email1Liable1,
								@CreatedOn,
								1
							)
						END
					END

					-- Insert Name Value Pair
					INSERT INTO dbo.[AdditionalCaseData]
						([CaseId]
						 ,[Name]
						 ,[Value]
						 ,[LoadedOn]
						 )
						 VALUES
						 (
						 @CaseId
						 ,'IssuingCourt'
						 ,@IssuingCourt
						 ,@CreatedOn
						 )

					IF (@BailDate IS NOT NULL)
					BEGIN
						INSERT INTO dbo.[AdditionalCaseData]
							([CaseId]
							 ,[Name]
							 ,[Value]
							 ,[LoadedOn]
							 )
							 VALUES
							 (
							 @CaseId
							 ,'BailDate'
							 ,CONVERT(VARCHAR(24),@BailDate,121)
							 ,@CreatedOn
							 )
					END

					-- Insert Attachments
					IF (@AttachmentInfo IS NOT NULL)
					BEGIN
						INSERT INTO @WMTbl
						SELECT Value 
						FROM oso.splitstring(@AttachmentInfo,'|');

						WHILE EXISTS(SELECT TOP 1 Value FROM @WMTbl)
						BEGIN
							SELECT TOP 1 @FileInfo = Value FROM @WMTbl
							SELECT @FileName = Value FROM oso.SplitString(@FileInfo,';') WHERE idx = 1;
							SELECT @FullFileName = Value FROM oso.SplitString(@FileInfo,';') WHERE idx = 2;

							INSERT INTO dbo.Attachments (
							   [caseId]
							  ,[userId]
							  ,[attachmentTypeId]
							  ,[fileName]
							  ,[attachedOn]
							  ,[fullName]
							  ,[convertedOn]
							  ,[conversionError]
							)
							VALUES
							(
								@CaseId
								,@UserId
								,1
								,@FileName
								,@CreatedOn
								,@FullFileName
								,NULL
								,0						
							)

							DELETE FROM @WMTbl WHERE VALUE = @FileInfo;
						END
					END

					-- Insert PhaseLog

					INSERT INTO [dbo].[PhaseLog]
							   ([CaseId]
							   ,[PhaseTypeId]
							   ,[PhaseStart]
							   ,[PhaseEnd]
							   ,[LastProcessedOn]
							   ,[OriginalDuration]
							   ,[CountOnlyActiveDays]
							   ,[ActiveDaysInPhase]
							   ,[WaitingDays]
							   ,[WaitingDaysIncreasedOn]
							   ,[DaysFromEnforcement])
					select
						c.Id,
						p.PhaseTypeId,
						getdate(),
						null,
						getdate(),
						null,
						p.CountOnlyActiveCaseDays,
						0,
						0,
						null,
						0
					from
						cases c
						join batches b on b.id = c.batchid
						join ClientCaseType cct on cct.id = b.clientCaseTypeId
						join clients cl on cl.id = cct.clientId
						join stages s on s.clientId = cl.Id and s.onLoad = 1
						join phases p on p.id = s.PhaseId
					where
						c.Id = @CaseId
						and cl.Id = @ClientId


					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[stg_HMCTS_Cases]
					SET    
					Imported   = 1
					, ImportedOn = GetDate()
					, CaseId = @CaseId
					WHERE
					Imported = 0 
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_HMCTS_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					Imported = 0
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				@ClientCaseReference
				, @ClientDefaulterReference
				, @IssueDate
				, @OffenceCode
				, @OffenceDescription
				, @OffenceLocation
				, @OffenceCourt
				, @DebtAddress1
				, @DebtAddress2
				, @DebtAddress3
				, @DebtAddress4
				, @DebtAddress5
				, @DebtAddressCountry
				, @DebtAddressPostcode
				, @StartDate
				, @EndDate
				, @TECDate
				, @FineAmount
				, @Currency
				, @TitleLiable1
				, @FirstnameLiable1
				, @MiddlenameLiable1
				, @LastnameLiable1
				, @FullnameLiable1
				, @CompanyNameLiable1
				, @DOBLiable1
				, @NINOLiable1
				, @MinorLiable1
				, @Add1Liable1
				, @Add2Liable1
				, @Add3Liable1
				, @Add4Liable1
				, @Add5Liable1
				, @AddPostCodeLiable1
				, @IsBusinessAddress
				, @Phone1Liable1
				, @Phone2Liable1
				, @Phone3Liable1
				, @Phone4Liable1
				, @Phone5Liable1
				, @Email1Liable1
				, @Email2Liable1
				, @Email3Liable1
				, @BatchDate
				, @ClientId
				, @ClientName
				, @PhaseDate
				, @StageDate
				, @IsAssignable
				, @OffenceDate
				, @CreatedOn
				, @BillNumber
				, @ClientPeriod
				, @AddCountryLiable1
				, @RollNumber
				, @Occupation
				, @BenefitIndicator
				, @CStatus
				, @CReturnCode
				, @OffenceNotes
				, @OffenceValue
				, @IssuingCourt
				, @SittingCourt
				, @WarningMarkers
				, @CaseNotes
				, @AttachmentInfo
				, @BailDate
        END
        CLOSE cursorT
        DEALLOCATE cursorT

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_Clients_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						,CourtId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						,@RegionId 
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  ,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							--SELECT @PriorityChargeId = sc.id
							--FROM 
							--	clientcasetype cct
							--	INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
							--	INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
							--	INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							--WHERE 
							--	cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							--INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							--VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_GetCasesXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_HMCTS_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CaseId, c.ClientCaseReference AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cct.BrandId = 1
			AND ct.name IN ('Arrest Breach Bail','Arrest Breach No Bail')
		
		UNION
		SELECT 99999999 AS CaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_GetCasesXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_ODM_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_GetDefaultersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_ODM_GetDefaultersXMapping]
AS
BEGIN
	select [d].[Id] [cDefaulterId],d.name as DefaulterName, ocn.OldCaseNumber as CaseNumber
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	JOIN [OldCaseNumbers] [ocn] on [c].[Id] = [ocn].CaseId
END
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_GetReturnCodeXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_ODM_GetReturnCodeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT CId AS CReturnCode
      ,[OSReturnCode] AS ReturnCode
  FROM [oso].[OneStep_ReturnCode_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_Phase3_Cases_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_ODM_Phase3_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_OS_AddBatches;
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@VehicleVRM varchar(7) ,
			@VehicleMake varchar(50) ,
			@VehicleModel varchar(50) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@TitleLiable2 varchar(50) ,
			@FirstnameLiable2 nvarchar(50) ,
			@MiddlenameLiable2 nvarchar(50) ,
			@LastnameLiable2 nvarchar(50) ,
			@FullnameLiable2 nvarchar(50) ,
			@CompanyNameLiable2 nvarchar(50) ,
			@DOBLiable2 datetime ,
			@NINOLiable2 varchar(13) ,
			@MinorLiable2 bit ,
			@Add1Liable2 nvarchar(80) ,
			@Add2Liable2 nvarchar(320) ,
			@Add3Liable2 nvarchar(100) ,
			@Add4Liable2 nvarchar(100) ,
			@Add5Liable2 nvarchar(100) ,
			@AddCountryLiable2 int ,
			@AddPostCodeLiable2 varchar(12) ,
			@Phone1Liable2 varchar(50) ,
			@Phone2Liable2 varchar(50) ,
			@Phone3Liable2 varchar(50) ,
			@Phone4Liable2 varchar(50) ,
			@Phone5Liable2 varchar(50) ,
			@Email1Liable2 varchar(250) ,
			@Email2Liable2 varchar(250) ,
			@Email3Liable2 varchar(250) ,
			@TitleLiable3 varchar(50) ,
			@FirstnameLiable3 nvarchar(50) ,
			@MiddlenameLiable3 nvarchar(50) ,
			@LastnameLiable3 nvarchar(50) ,
			@FullnameLiable3 nvarchar(150) ,
			@CompanyNameLiable3 nvarchar(50) ,
			@DOBLiable3 datetime ,
			@NINOLiable3 varchar(13) ,
			@MinorLiable3 bit ,
			@Add1Liable3 nvarchar(80) ,
			@Add2Liable3 nvarchar(320) ,
			@Add3Liable3 nvarchar(100) ,
			@Add4Liable3 nvarchar(100) ,
			@Add5Liable3 nvarchar(100) ,
			@AddCountryLiable3 int ,
			@AddPostCodeLiable3 varchar(12) ,
			@Phone1Liable3 varchar(50) ,
			@Phone2Liable3 varchar(50) ,
			@Phone3Liable3 varchar(50) ,
			@Phone4Liable3 varchar(50) ,
			@Phone5Liable3 varchar(50) ,
			@Email1Liable3 varchar(250) ,
			@Email2Liable3 varchar(250) ,
			@Email3Liable3 varchar(250) ,
			@TitleLiable4 varchar(50) ,
			@FirstnameLiable4 nvarchar(50) ,
			@MiddlenameLiable4 nvarchar(50) ,
			@LastnameLiable4 nvarchar(50) ,
			@FullnameLiable4 nvarchar(150) ,
			@CompanyNameLiable4 nvarchar(50) ,
			@DOBLiable4 datetime ,
			@NINOLiable4 varchar(13) ,
			@MinorLiable4 bit ,
			@Add1Liable4 nvarchar(80) ,
			@Add2Liable4 nvarchar(320) ,
			@Add3Liable4 nvarchar(100) ,
			@Add4Liable4 nvarchar(100) ,
			@Add5Liable4 nvarchar(100) ,
			@AddCountryLiable4 int ,
			@AddPostCodeLiable4 varchar(12) ,
			@Phone1Liable4 varchar(50) ,
			@Phone2Liable4 varchar(50) ,
			@Phone3Liable4 varchar(50) ,
			@Phone4Liable4 varchar(50) ,
			@Phone5Liable4 varchar(50) ,
			@Email1Liable4 varchar(250) ,
			@Email2Liable4 varchar(250) ,
			@Email3Liable4 varchar(250) ,
			@TitleLiable5 varchar(50) ,
			@FirstnameLiable5 nvarchar(50) ,
			@MiddlenameLiable5 nvarchar(50) ,
			@LastnameLiable5 nvarchar(50) ,
			@FullnameLiable5 nvarchar(150) ,
			@CompanyNameLiable5 nvarchar(50) ,
			@DOBLiable5 datetime ,
			@NINOLiable5 varchar(13) ,
			@MinorLiable5 bit ,
			@Add1Liable5 nvarchar(80) ,
			@Add2Liable5 nvarchar(320) ,
			@Add3Liable5 nvarchar(100) ,
			@Add4Liable5 nvarchar(100) ,
			@Add5Liable5 nvarchar(100) ,
			@AddCountryLiable5 int ,
			@AddPostCodeLiable5 varchar(12) ,
			@Phone1Liable5 varchar(50) ,
			@Phone2Liable5 varchar(50) ,
			@Phone3Liable5 varchar(50) ,
			@Phone4Liable5 varchar(50) ,
			@Phone5Liable5 varchar(50) ,
			@Email1Liable5 varchar(250) ,
			@Email2Liable5 varchar(250) ,
			@Email3Liable5 varchar(250) ,
			@BatchDate datetime ,
			@OneStepCaseNumber int ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(80) ,
			@DefaultersNames nvarchar(1000) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod varchar(20) ,
			@EmployerName nvarchar(150) ,
			@EmployerAddressLine1 nvarchar(80) ,
			@EmployerAddressLine2 nvarchar(320) ,
			@EmployerAddressLine3 nvarchar(100) ,
			@EmployerAddressLine4 nvarchar(100) ,
			@EmployerPostcode varchar(12) ,
			@EmployerEmailAddress varchar(50) ,
			@AddCountryLiable1 int ,
			@EmployerFaxNumber varchar(50) ,
			@EmployerTelephone varchar(50) ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,	
			@Welfare bit,
			@DateOfBirth datetime,
			@NINO nvarchar(13),
			@OffenceValue decimal(18,4),
			--@OffenceNumber nvarchar(1000) ,	



             @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @PaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int





		SET @succeed = 1

        SELECT
               @PaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Paid'

        SELECT
               @ReturnedStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Returned'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			   [ClientCaseReference]
			  ,[ClientDefaulterReference]
			  ,[IssueDate]
			  ,[OffenceCode]
			  ,[OffenceDescription]
			  ,[OffenceLocation]
			  ,[OffenceCourt]
			  ,[DebtAddress1]
			  ,[DebtAddress2]
			  ,[DebtAddress3]
			  ,[DebtAddress4]
			  ,[DebtAddress5]
			  ,[DebtAddressCountry]
			  ,[DebtAddressPostcode]
			  ,[VehicleVRM]
			  ,[VehicleMake]
			  ,[VehicleModel]
			  ,[StartDate]
			  ,[EndDate]
			  ,[TECDate]
			  ,[FineAmount]
			  ,[Currency]
			  ,[MiddlenameLiable1]
			  ,[FullnameLiable1]
			  ,[TitleLiable1]
			  ,[CompanyNameLiable1]
			  ,[FirstnameLiable1]
			  ,[DOBLiable1]
			  ,[LastnameLiable1]
			  ,[NINOLiable1]
			  ,[MinorLiable1]
			  ,[Add1Liable1]
			  ,[Add2Liable1]
			  ,[Add3Liable1]
			  ,[Add4Liable1]
			  ,[Add5Liable1]
			  ,[AddPostCodeLiable1]
			  ,[IsBusinessAddress]
			  ,[Phone1Liable1]
			  ,[Phone2Liable1]
			  ,[Phone3Liable1]
			  ,[Phone4Liable1]
			  ,[Phone5Liable1]
			  ,[Email1Liable1]
			  ,[Email2Liable1]
			  ,[Email3Liable1]
			  ,[MiddlenameLiable2]
			  ,[FullnameLiable2]
			  ,[TitleLiable2]
			  ,[CompanyNameLiable2]
			  ,[FirstnameLiable2]
			  ,[DOBLiable2]
			  ,[LastnameLiable2]
			  ,[NINOLiable2]
			  ,[MinorLiable2]
			  ,[Add1Liable2]
			  ,[Add2Liable2]
			  ,[Add3Liable2]
			  ,[Add4Liable2]
			  ,[Add5Liable2]
			  ,[AddCountryLiable2]
			  ,[AddPostCodeLiable2]
			  ,[Phone1Liable2]
			  ,[Phone2Liable2]
			  ,[Phone3Liable2]
			  ,[Phone4Liable2]
			  ,[Phone5Liable2]
			  ,[Email1Liable2]
			  ,[Email2Liable2]
			  ,[Email3Liable2]
			  ,[TitleLiable3]
			  ,[FirstnameLiable3]
			  ,[MiddlenameLiable3]
			  ,[LastnameLiable3]
			  ,[FullnameLiable3]
			  ,[CompanyNameLiable3]
			  ,[DOBLiable3]
			  ,[NINOLiable3]
			  ,[MinorLiable3]
			  ,[Add1Liable3]
			  ,[Add2Liable3]
			  ,[Add3Liable3]
			  ,[Add4Liable3]
			  ,[Add5Liable3]
			  ,[AddCountryLiable3]
			  ,[AddPostCodeLiable3]
			  ,[Phone1Liable3]
			  ,[Phone2Liable3]
			  ,[Phone3Liable3]
			  ,[Phone4Liable3]
			  ,[Phone5Liable3]
			  ,[Email1Liable3]
			  ,[Email2Liable3]
			  ,[Email3Liable3]
			  ,[TitleLiable4]
			  ,[FirstnameLiable4]
			  ,[MiddlenameLiable4]
			  ,[LastnameLiable4]
			  ,[FullnameLiable4]
			  ,[CompanyNameLiable4]
			  ,[DOBLiable4]
			  ,[NINOLiable4]
			  ,[MinorLiable4]
			  ,[Add1Liable4]
			  ,[Add2Liable4]
			  ,[Add3Liable4]
			  ,[Add4Liable4]
			  ,[Add5Liable4]
			  ,[AddCountryLiable4]
			  ,[AddPostCodeLiable4]
			  ,[Phone1Liable4]
			  ,[Phone2Liable4]
			  ,[Phone3Liable4]
			  ,[Phone4Liable4]
			  ,[Phone5Liable4]
			  ,[Email1Liable4]
			  ,[Email2Liable4]
			  ,[Email3Liable4]
			  ,[TitleLiable5]
			  ,[FirstnameLiable5]
			  ,[MiddlenameLiable5]
			  ,[LastnameLiable5]
			  ,[FullnameLiable5]
			  ,[CompanyNameLiable5]
			  ,[DOBLiable5]
			  ,[NINOLiable5]
			  ,[MinorLiable5]
			  ,[Add1Liable5]
			  ,[Add2Liable5]
			  ,[Add3Liable5]
			  ,[Add4Liable5]
			  ,[Add5Liable5]
			  ,[AddCountryLiable5]
			  ,[AddPostCodeLiable5]
			  ,[Phone1Liable5]
			  ,[Phone2Liable5]
			  ,[Phone3Liable5]
			  ,[Phone4Liable5]
			  ,[Phone5Liable5]
			  ,[Email1Liable5]
			  ,[Email2Liable5]
			  ,[Email3Liable5]
			  ,[BatchDate]
			  ,[OneStepCaseNumber]
			  ,[ClientId]
			  ,[ConnId]
			  ,[ClientName]
			  ,[DefaultersNames]
			  ,[PhaseDate]
			  ,[StageDate]
			  ,[IsAssignable]
			  ,[OffenceDate]
			  ,[CreatedOn]
			  ,[BillNumber]
			  ,[ClientPeriod]
			  ,[EmployerName]
			  ,[EmployerAddressLine1]
			  ,[EmployerAddressLine2]
			  ,[EmployerAddressLine3]
			  ,[EmployerAddressLine4]
			  ,[EmployerPostcode]
			  ,[EmployerEmailAddress]
			  ,[AddCountryLiable1]
			  ,[EmployerFaxNumber]
			  ,[EmployerTelephone]
			  ,[RollNumber]
			  ,[Occupation]
			  ,[BenefitIndicator]
			  ,[CStatus]
			  ,[CReturnCode]
			  ,[OffenceNotes]
			  ,[Welfare]
		      ,[DateOfBirth]
			  ,[NINO]
			  ,[OffenceValue]
			  --,[OffenceNumber]
		FROM
                 oso.[stg_Onestep_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				 -- ,@OffenceNumber
        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						--SET @CurrentBatchId = NULL
						----Select @CurrentBatchId = Id from Batches where batchDate = @BatchDate and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId)
      --                  Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						
						-- Check and update BatchId
						SET @CurrentBatchId = NULL
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						IF (@CurrentBatchId IS NULL)
						BEGIN
							DECLARE @CurrentDate DateTime = GetDate();
							INSERT into batches
								(
								[referenceNumber],
								[batchDate],
								[closedDate],
								[createdBy],
								[manual],
								[deleted],
								[crossReferenceSent],
								[crossReferenceSentDate],
								[clientCaseTypeId],
								[loadedOn],
								[loadedBy],
								[receivedOn],
								[summaryDate]
								)

							SELECT 
								CONCAT(cct.batchrefprefix,CONVERT(varchar(8),@BatchDate,112))[referenceNumber]
								,CAST(CONVERT(date,@BatchDate) AS DateTime) [batchDate]
								,@CurrentDate [closedDate]
								,2 [createdBy]
								,1 [manual]
								,0 [deleted]
								,1 [crossReferenceSent]
								,@CurrentDate [crossReferenceSentDate]
								,cct.id [clientCaseTypeId]
								,@CurrentDate [loadedOn]
								,2 [loadedBy]
								,@CurrentDate [receivedOn]
								,NULL [summaryDate]
							FROM
								clientcasetype cct
							WHERE
								cct.clientid = @ClientId
						
							SET @CurrentBatchId = SCOPE_IDENTITY();

							-- Set the BatchIDGenerated flag to true in staging
							UPDATE
							[oso].[stg_Onestep_Cases]
							SET    BatchNoGenerated = 1
							WHERE
							Imported = 0 
							AND ErrorId is NULL
							AND ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						END

						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT @StageId = (s.id) from clients cli
                        INNER JOIN stages s on s.clientid = cli.id
                        where cli.id = @ClientId
                                and s.Name = 'Return Case'

						IF (@StageId IS NULL)	-- Client is inactive, so look for stageid from Empty Workflow
						BEGIN
						  SET @StageId = (select s.id from clients cli
												INNER JOIN stages s on s.clientid = cli.id
												INNER JOIN StageTemplates st on s.stagetemplateid = st.id
												INNER JOIN PhaseTemplates pt on st.phasetemplateid = pt.id
												INNER JOIN WorkflowTemplates wft on pt.workflowtemplateid = wft.id
												where cli.id = @ClientId
														and (
														(wft.name <> 'Empty Workflow' and s.onload = 1)
														or
														(wft.name = 'Empty Workflow' and s.name = 'Return Case')
														)
										)
						END

						
						SET @CStatusId = @PaidStatusId; -- change this to PaidStatusId
						SET @ReturnRunId = NULL
						IF (@CStatus = 'Returned')
						BEGIN
							SET @CStatusId = @ReturnedStatusId
							SET @ReturnRunId = 1244
						END

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL

						IF (@CaseTypeId = 6)
						BEGIN
							SET @VTECDate = @TECDate
							SET @ANPR = 1
						END

						
						IF (@CaseTypeId = 24)
						BEGIN
							SET @VTECDate = NULL
							SET @ANPR = 0
						END
						
						SET @cnid = (  
									SELECT MIN(cn.Id)  
									FROM CaseNumbers AS cn WITH (  
										XLOCK  
										,HOLDLOCK  
										)  
									WHERE cn.Reserved = 0  
									--AND cn.id > = 13999996
									AND cn.alphaNumericCaseNumber > 'X000000' --Remove this line for LIVE ACTIVE cases.
									)  

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
									--, @FullnameLiable1 --[debtName]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,'') + ISNULL(@DefaultersNames,'')))--[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @ClientPeriod--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						INSERT INTO dbo.OldCaseNumbers (CaseId, OldCaseNumber)
						VALUES (@CaseId, convert(varchar(20), @OneStepCaseNumber)
);
						
					-- 3) Insert Case Offences
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						VALUES
						(
							@CaseId,
							@OffenceDate,
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId,
							@OffenceValue
						);

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DateOfBirth,
						0,
						0,
						NULL,
						0,
						@NINO,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Vehicle
						IF (@CaseTypeId = 6 AND @VehicleVRM IS NOT NULL)
							BEGIN
								INSERT INTO dbo.Vehicles (
									[vrm]
									,[vrmOwner]
									,[defaulterId]
									,[responseStatusId]
									,[responseDate]
									,[responseLoadedDate]
									,[keeperId]
									,[responseStringId]
									,[eventDate]
									,[licenceExpiry]
									,[exportDate]
									,[scrapDate]
									,[theftDate]
									,[recoveryDate]
									,[make]
									,[model]
									,[colour]
									,[taxClass]
									,[seatingCapacity]
									,[previousKeepers]
									,[Motability]
									,[Check]
									,[LastRequestedOn]
									,[isWarrantVrm]
									,[officerId]
									,[VrmReceivedOn]
								)
								VALUES
								(
									@VehicleVRM -- vrm
									,0 -- vrmOwner
									,@LeadDefaulterId
									,NULL -- responseStatusId
									,NULL -- responseDate
									,NULL -- responseLoadedDate
									,NULL -- keeperId
									,NULL -- responseStringId
									,NULL -- eventDate
									,NULL -- licenceExpiry
									,NULL -- exportDate
									,NULL -- scrapDate
									,NULL -- theftDate
									,NULL -- recoveryDate
									,NULL -- make
									,NULL -- model
									,NULL -- colour
									,NULL -- taxCl--s
									,NULL -- seatingCapacity
									,NULL -- previousKeepers
									,0 -- Motability
									,0 -- [Check]
									,NULL -- L--tRequestedOn
									,1 -- isWarrantVrm
									,NULL -- officerId
									,@CreatedOn -- VrmReceivedOn
									);

								SET @VehicleId = SCOPE_IDENTITY();

								INSERT INTO dbo.CaseVehicles
								(
									caseId,
									vehicleId
								)
								VALUES
								(
									@CaseId,
									@VehicleId
								)

							END

					END

					-- Defaulter2
					IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

					END

					-- Defaulter3
					IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			
					-- Defaulter4
					IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END

					-- Defaulter5
					IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			

					-- 5) Insert Contact Addresses


					-- 6) Insert Defaulter Details
					-- 7) Insert Defaulter Cases
					-- 8 ) Insert Vehicles
							-- Note: LeadDefaulterId is required


					-- 9) Insert Case Note
					-- Not required for now


						UPDATE
						[oso].[stg_Onestep_Cases]
						SET    Imported   = 1
						, ImportedOn = GetDate()
						WHERE
						Imported = 0 
						AND OneStepCaseNumber = @OneStepCaseNumber
						AND ClientId = @ClientId
						AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_Onestep_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					OneStepCaseNumber = @OneStepCaseNumber
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				  --,@OffenceNumber

        END
        CLOSE cursorT
        DEALLOCATE cursorT

		
		-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
		BEGIN TRANSACTION	
			UPDATE ca 
			Set TidReviewedId = 1
			from dbo.ContactAddresses ca
			join dbo.defaultercases dc on ca.defaulterid = dc.defaulterid
			join dbo.cases c on dc.caseid = c.id
			INNER join dbo.OldCaseNumbers oc on c.Id = oc.CaseId
			INNER join oso.stg_Onestep_Cases osc on convert(varchar(20), osc.OneStepCaseNumber) = oc.OldCaseNumber
			where 
			osc.Imported = 1 AND
			c.addressline1 = ca.addressline1 AND
			c.postcode = ca.postcode	
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS__BU_DefaulterPhones_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS__BU_DefaulterPhones_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[Staging_OS_BU_DefaulterPhones]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ActionMappingJD]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jamie Darlington
-- Create date: 08 May, 2020
-- Description:	Used to pick up the latest action to be applied when mapping OneStep workflow actions
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ActionMappingJD] 
	@DaysInPhase int, 
	@Wftid int,
	@OneStepStageName varchar(255),
	@LatestAction varchar(255) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

SET @LatestAction = (select 
CASE 
	WHEN @DaysInPhase >= 23 THEN COALESCE(Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 22 THEN COALESCE(Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 21 THEN COALESCE(Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 20 THEN COALESCE(Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 19 THEN COALESCE(Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 18 THEN COALESCE(Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 17 THEN COALESCE(Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 16 THEN COALESCE(Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 15 THEN COALESCE(Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 14 THEN COALESCE(Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 13 THEN COALESCE(Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 12 THEN COALESCE(Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 11 THEN COALESCE(Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 10 THEN COALESCE(Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  9 THEN COALESCE(Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  8 THEN COALESCE(Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  7 THEN COALESCE(Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  6 THEN COALESCE(Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  5 THEN COALESCE(Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  4 THEN COALESCE(Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  3 THEN COALESCE(Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  2 THEN COALESCE(Day02, Day01, Day00)
	WHEN @DaysInPhase  =  1 THEN COALESCE(Day01, Day00)
	WHEN @DaysInPhase <=  0 THEN Day00 END As LastAction
	from oso.OneStep_Workflow_CaseMap
where workflowid = @wftid
and StageName = @OneStepStageName
)
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ActionMappingJDP2]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jamie Darlington
-- Create date: 02 Aug, 2020
-- Description:	Used to pick up the latest action to be applied when mapping Phase 2 (DCA/LAA) OneStep workflow actions
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ActionMappingJDP2] 
	@DaysInPhase int, 
	@Wftid int,
	@OneStepStageName varchar(255),
	@LatestAction varchar(255) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

SET @LatestAction = (select 
CASE 
	WHEN @DaysInPhase >= 45 THEN COALESCE(Day45, Day44, Day43, Day42, Day41, Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 44 THEN COALESCE(Day44, Day43, Day42, Day41, Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 43 THEN COALESCE(Day43, Day42, Day41, Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 42 THEN COALESCE(Day42, Day41, Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 41 THEN COALESCE(Day41, Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 40 THEN COALESCE(Day40, Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 39 THEN COALESCE(Day39, Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 38 THEN COALESCE(Day38, Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 37 THEN COALESCE(Day37, Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 36 THEN COALESCE(Day36, Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 35 THEN COALESCE(Day35, Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 34 THEN COALESCE(Day34, Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 33 THEN COALESCE(Day33, Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 32 THEN COALESCE(Day32, Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 31 THEN COALESCE(Day31, Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 30 THEN COALESCE(Day30, Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 29 THEN COALESCE(Day29, Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 28 THEN COALESCE(Day28, Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 27 THEN COALESCE(Day27, Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 26 THEN COALESCE(Day26, Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 25 THEN COALESCE(Day25, Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 24 THEN COALESCE(Day24, Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 23 THEN COALESCE(Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 22 THEN COALESCE(Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 21 THEN COALESCE(Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 20 THEN COALESCE(Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 19 THEN COALESCE(Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 18 THEN COALESCE(Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 17 THEN COALESCE(Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 16 THEN COALESCE(Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 15 THEN COALESCE(Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 14 THEN COALESCE(Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 13 THEN COALESCE(Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 12 THEN COALESCE(Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 11 THEN COALESCE(Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 10 THEN COALESCE(Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  9 THEN COALESCE(Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  8 THEN COALESCE(Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  7 THEN COALESCE(Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  6 THEN COALESCE(Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  5 THEN COALESCE(Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  4 THEN COALESCE(Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  3 THEN COALESCE(Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  2 THEN COALESCE(Day02, Day01, Day00)
	WHEN @DaysInPhase  =  1 THEN COALESCE(Day01, Day00)
	WHEN @DaysInPhase <=  0 THEN Day00 END As LastAction
	from oso.OneStep_Workflow_CaseMapP2
where workflowid = @wftid
and StageName = @OneStepStageName
)
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AddBatches]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddBatches]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			INSERT into batches
				(
				[referenceNumber],
				[batchDate],
				[closedDate],
				[createdBy],
				[manual],
				[deleted],
				[crossReferenceSent],
				[crossReferenceSentDate],
				[clientCaseTypeId],
				[loadedOn],
				[loadedBy],
				[receivedOn],
				[summaryDate]
				)

				select DISTINCT
					CONCAT(
					cct.batchrefprefix,
					datepart(year,CAST(CONVERT(date,osc.batchdate) AS DateTime)),
					FORMAT(CAST(CONVERT(date,osc.batchdate) AS DateTime),'MM'),
					FORMAT(CAST(CONVERT(date,osc.batchdate) AS DateTime),'dd')--,
					--FORMAT(osc.batchdate,'hhmmss')
					 )[referenceNumber]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [batchDate]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [closedDate]
					,2 [createdBy]
					,0 [manual]
					,0 [deleted]
					,1 [crossReferenceSent]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [crossReferenceSentDate]
					,cct.id [clientCaseTypeId]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [loadedOn]
					,2 [loadedBy]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [receivedOn]
					,NULL [summaryDate]
				From oso.stg_Onestep_Cases osc
				join clientcasetype cct on osc.clientid = cct.clientid
				where osc.imported = 0 AND osc.ErrorID IS NULL
				GROUP BY cct.id, cct.batchrefprefix, CAST(CONVERT(date,osc.batchdate) AS DateTime)

				EXCEPT 

				SELECT
				b.[referenceNumber],
				b.[batchDate],
				b.[closedDate],
				b.[createdBy],
				b.[manual],
				b.[deleted],
				b.[crossReferenceSent],
				b.[crossReferenceSentDate],
				b.[clientCaseTypeId],
				b.[loadedOn],
				b.[loadedBy],
				b.[receivedOn],
				b.[summaryDate]

				from batches b
			    join clientcasetype cct on b.clientcasetypeid = cct.id AND cct.brandid IN (10,11,12,13)
                join oso.stg_Onestep_cases osc on b.batchDate = CAST(CONVERT(date,osc.batchdate) AS DateTime) AND cct.clientid = osc.ClientId

		END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AdditionalCaseData_Staging_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_AdditionalCaseData_Staging_DT]   
AS   
  BEGIN   
   SET NOCOUNT ON;  
      DECLARE @CaseId VARCHAR(10);   
      DECLARE @Pairs NVARCHAR(MAX);   
      DECLARE @MultipleNameValuePairs TABLE   
        (   
           caseId     VARCHAR(10),   
           namevaluepairs NVARCHAR(MAX)   
        )   
  
      INSERT INTO @MultipleNameValuePairs   
                  (caseId,   
                   namevaluepairs)   
      SELECT CaseId,   
             NameValuePairs   
      FROM   oso.Staging_OS_AdditionalCaseData   
      WHERE  ErrorId IS NULL AND Imported = 0  
  
      DECLARE namevaluepairs CURSOR fast_forward FOR   
        SELECT caseId,   
               namevaluepairs   
        FROM   @MultipleNameValuePairs   
  
      OPEN namevaluepairs   
  
      FETCH next FROM namevaluepairs INTO @CaseId, @Pairs   
  
      WHILE @@FETCH_STATUS = 0   
        BEGIN   
            BEGIN try   
                BEGIN TRANSACTION   
  
    IF (@Pairs LIKE '%;from%')  
    BEGIN  
     SET @Pairs = REVERSE(SUBSTRING(REVERSE(@Pairs), CHARINDEX(';', REVERSE(@Pairs)), LEN(@Pairs)))  
    END  
  
      
                IF( @CaseId IS NOT NULL )   
                  BEGIN  
                    DECLARE @Name NVARCHAR(MAX)   
                    DECLARE @Value NVARCHAR(MAX)   
         
     --If name value pairs have more than one set of name values, consider them for case note insertion, otherwise additional case data  
     IF((SELECT Count(value) FROM dbo.Fnsplit((@Pairs), ':')) > 2)   
      BEGIN   
       DECLARE @CaseNoteExists INT  
       Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @Pairs  
       IF (@CaseNoteExists = 0)  
       BEGIN  
        INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)  
        SELECT @CaseId,   
          NULL,   
          2,   
          Rtrim(Ltrim(@Pairs)),   
          Getdate(),   
          1,   
          NULL,   
          NULL   
       END  
      END   
      ELSE   
      BEGIN   
          
       IF OBJECT_ID('tempdb..#NameValue') IS NOT NULL  
        DROP TABLE #NameValue  
  
       CREATE TABLE #NameValue   
        (   
         id   INT IDENTITY(1, 1) NOT NULL,   
         word NVARCHAR(MAX)   
        )   
  
       INSERT INTO #NameValue (word)   
       SELECT value   
       FROM   dbo.Fnsplit(@Pairs, ':')   
         
       SET @Name = (SELECT Rtrim(Ltrim(word))   
       FROM   #NameValue   
       WHERE  id = 1 )  
  
       SET @Value = (SELECT Rtrim(Ltrim(word))   
       FROM   #NameValue   
       WHERE  id = 2 )  
        
	   SET @Value = REPLACE(@Value,';','')
       DECLARE @Counter INT = (SELECT Count(*)   
        FROM   dbo.AdditionalCaseData   
        WHERE  NAME = @Name   
          AND caseid = @CaseId)   
        
       IF( @Counter > 0 )   
        BEGIN   
         UPDATE dbo.AdditionalCaseData   
         SET    value = Rtrim(Ltrim(@Value)), LoadedOn = GETDATE()  
         WHERE  caseid = @CaseId   
           AND NAME = @Name   
        END   
      ELSE   
      BEGIN  
       INSERT INTO dbo.AdditionalCaseData   
           (caseid,   
           NAME,   
           value,  
           LoadedOn)   
        VALUES      (@CaseId,   
           @Name,   
           Rtrim(Ltrim(@Value)),  
           GETDATE())   
       END   
  
      END  
        
                      UPDATE [oso].[Staging_OS_AdditionalCaseData]   
                      SET    imported = 1,   
                             importedon = Getdate()   
                      WHERE  CaseId = @CaseId   
                             AND NameValuePairs LIKE '%'+ @Pairs +'%'  
                             AND (Imported IS NULL OR Imported = 0)  
    END  
                COMMIT   
            END try   
  
            BEGIN catch   
                ROLLBACK TRANSACTION   
  
                INSERT INTO oso.[sqlerrors]   
                VALUES      (Error_number(),   
                             Error_severity(),   
                             Error_state(),   
                             Error_procedure(),   
                             Error_line(),   
                             Error_message(),   
                             Getdate() )   
  
                DECLARE @ErrorId INT = Scope_identity();   
  
                UPDATE [oso].[Staging_OS_AdditionalCaseData]   
                SET    errorid = @ErrorId   
                WHERE  CaseId = @CaseId   
                       AND namevaluepairs = @Pairs   
                       AND imported = 0   
  
                SELECT 0 AS Succeed   
            END catch   
  
            FETCH next FROM namevaluepairs INTO @CaseId, @Pairs   
        END   
  
      CLOSE namevaluepairs   
  
      DEALLOCATE namevaluepairs   
  END 
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Adjustment_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Adjustment_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId        INT
                    , @Amount          DECIMAL(18,4)
                    , @UserId          INT
                    , @OfficerId          INT
                    , @AddedOn         DATETIME
                    , @PaymentId       INT
                    , @PaymentStatusId INT
                    , @SchemePaymentId INT
                    , @CaseNoteId      INT
                    , @HistoryTypeId   INT

				SELECT
					   @PaymentStatusId = [Id]
				FROM
					   [dbo].[PaymentStatus]
				WHERE
					   [name] = 'BALANCE ADJUSTMENT'
				SELECT
					   @SchemePaymentId = [Id]
				FROM
					   [dbo].[SchemePayments]
				WHERE
					   [name] = 'Balance Adjustment'
				SELECT
					   @HistoryTypeId = [Id]
				FROM
					   [dbo].[HistoryTypes]
				WHERE
					   [name] = 'UpdateClientBalance'

                DECLARE cursorT
                CURSOR
                    --LOCAL STATIC
                    --LOCAL FAST_FORWARD
                    --LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT top 200000
						CCaseId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId
                    FROM
                        oso.[Staging_OS_Adjustments]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@CaseId,
						@Amount,
						@AddedOn,
						@UserId,
						@OfficerId
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        -- Add new rows
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]
                                    , [transactionDate]
                                    , [amount]
                                    , [clearedOn]
                                    , [bouncedOn]
                                    , [recordedBy]
                                    , [officerId]
                                    , [REFERENCE]
                                    , [authorisationNumber]
                                    , [journalStatusId]
                                    , [receiptTypeId]
                                    , [trusted]
                                    , [schemePaymentId]
                                    , [paymentStatusId]
                                    , [surchargeAmount]
                                    , [paymentID]
                                    , [reconciledOn]
                                    , [reconcileReference]
                                    , [reversedOn]
                                    , [parentId]
                                    , [previousPaymentStatusId]
                                    , [reconciledBy]
                                    , [keepGoodsRemovedStatus]
                                    , [cardStatementReference]
                                    , [bankReconciledOn]
                                    , [bounceReference]
                                    , [reconciledBouncedOn]
                                    , [reconciledBouncedBy]
                                    , [exceptionalAdjustmentId]
                                    , [bankReconciledBy]
                                    , [isReversedChargePayment]
                                    , [paymentOriginId]
                                    , [uploadedFileReference]
                                    , [ReverseOrBounceNotes]
                               )
                               VALUES
                               ( @AddedOn
                                    , @AddedOn
                                    , @Amount
                                    , NULL
                                    , NULL
                                    , @UserId
                                    , @OfficerId
                                    , NUll
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 1
                                    , @SchemePaymentId
                                    , @PaymentStatusId
                                    , 0.00
                                    , 'MigratedPayment'
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , NULL
                                    , 0
                                    , 0
                                    , 0
                                    , 0
                               )
                        ;
                        
                        -- Add new Case Notes
                        INSERT INTO dbo.[CaseNotes]
                               ( [caseId]
                                    , [officerId]
                                    , [userId]
                                    , [text]
                                    , [occured]
                                    , [visible]
                                    , [groupId]
                                    , [TypeId]
                               )
                               VALUES
                               ( @CaseId
                                    , @OfficerId
                                    , @UserId
                                    , 'Add balance adjustment of ' + FORMAT(@Amount,'N2', 'en-US') 
                                    , @AddedOn
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @CaseNoteId = SCOPE_IDENTITY();
                        -- Add new Case History
                        INSERT INTO dbo.[CaseHistory]
                               ( [caseId]
                                    , [userId]
                                    , [officerId]
                                    , [COMMENT]
                                    , [occured]
                                    , [typeId]
                                    , [CaseNoteId]
                               )
                               VALUES
                               ( @CaseId
                                    , @UserId
                                    , @OfficerId
                                    , 'Balance adjustment has been completed. Adjustment amount = ' + FORMAT(@Amount,'N2', 'en-US')  + '.'
                                    , @AddedOn
                                    , @HistoryTypeId
                                    , @CaseNoteId
                               )
                        ;
                        
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
							@CaseId,
							@Amount,
							@AddedOn,
							@UserId,
							@OfficerId
                    END
                    CLOSE cursorT
                    DEALLOCATE cursorT 

					UPDATE [oso].[Staging_OS_Adjustments]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0	

					COMMIT TRANSACTION
					SELECT 1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT 0 as Succeed
                END CATCH
            END
        END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Adjustments_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear charges staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Adjustments_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From [oso].[Staging_OS_Adjustments]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Arrangements_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_Arrangements]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Arrangements_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			DECLARE	@Reference VARCHAR(50),
					@Amount DECIMAL(18,4), 
					@FirstInstalmentAmount DECIMAL(18,4),
					@CFrequencyId INT,
					@PaymentType INT,
					@CUserId INT,
					@StatusId INT,
					@StartDate DATETIME,
					@LastEndPeriodDate DATETIME,
					@LastInstalmentDate DATETIME,
					@FirstInstalmentDate DATETIME,
					@ArrangementId INT,
					@NextInstalmentDate	DATETIME,
					@NoOfIntervals INT							
			DECLARE cursorT
				CURSOR FOR
					select distinct
						reference,
						amount,
						FirstInstalmentAmount,
						CFrequencyId,
						2,
						isnull(cuserid, 2),
						StatusId,
						StartDate,
						case
							when CFrequencyId = 1 then startdate
							when CFrequencyId in (2,4,5,6,7,8) then
								case
									when dateadd(day,-fr.NumberOfDays,nextinstalmentdate) > startdate then dateadd(day,-fr.NumberOfDays,nextinstalmentdate) 
									else startdate
								end
							when CFrequencyId in (3,9) then
								case
									when dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) > startdate then dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) 
									else startdate
								end
							else null
						end as LastEndPeriodDate,
						LastInstalmentDate,
						FirstInstalmentDate,
						NextInstalmentDate,
						case
							when CFrequencyId = 1 then 1
							else NoOfIntervals
						end
					from
						oso.[stg_OneStep_Arrangements] stg
						join InstalmentFrequency fr on fr.id = stg.CFrequencyId
					where
						imported = 0

				OPEN cursorT
				FETCH NEXT
					FROM
						cursorT
					INTO
						@Reference, 
						@Amount, 
						@FirstInstalmentAmount, 
						@CFrequencyId, 
						@PaymentType, 
						@CUserId,
						@StatusId, 
						@StartDate, 
						@LastEndPeriodDate,
						@LastInstalmentDate, 
						@FirstInstalmentDate,
						@NextInstalmentDate,
						@NoOfIntervals
						
				WHILE 
					@@FETCH_STATUS = 0
					BEGIN
						INSERT [dbo].[Arrangements] 
						(
							[reference],
							[amount],
							[firstInstalmentAmount],
							[frequencyId],
							[paymentMethodId], 
							[userId],
							[statusId],
							[startDate],
							[lastEndPeriodDate],
							[lastInstalmentDate],
							[FirstInstalmentDate],
							[DeferedPaymentDate],
							[payments]
						)
						VALUES
						(
							CAST(@Reference AS VARCHAR(50)), 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							ISNULL(@StartDate, @FirstInstalmentDate),
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						)
						SET @ArrangementId = SCOPE_IDENTITY();					
							

						INSERT dbo.CaseArrangements
						(
							[caseId],
							[arrangementId]
						)
						select
							cCaseId,
							@ArrangementId
						from
							oso.[stg_OneStep_Arrangements]
						where
							Reference = @Reference
							and Imported = 0

						FETCH NEXT
						FROM
							cursorT
						INTO
							@Reference, 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							@StartDate, 
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						
					END			
				CLOSE cursorT
				DEALLOCATE cursorT
			
			UPDATE [oso].[stg_OneStep_Arrangements]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_GetFrequencyId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Arrangements_GetFrequencyId]
@Frequency int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT TOP 1 [CFrequencyId]
		FROM [oso].[OSColInstalmentFrequencyXRef]
		WHERE [OSFrequencyDays] = @Frequency
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Assignments_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Assignments_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_Assignments]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
/****** Object:  StoredProcedure [oso].[usp_OS_Assignments_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Assignments_DT]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @GroupName INT;
			SET @GroupName = (SELECT TOP 1 groupName FROM AssignedCases ORDER BY Id DESC);
						
			SELECT
				osa.[cCaseId], 
				a.[COfficerId], 
				osa.[AssignmentDate], 
				a.[CUserId], 
				a.[TeamId], 	
				@GroupName [grpName]
			INTO
				[#AssignedCasesTemp]
			FROM
				(SELECT cCaseId, MAX(AssignmentDate) AssignmentDate
				FROM oso.stg_OneStep_Assignments
				GROUP BY cCaseId) osa
				INNER JOIN oso.stg_OneStep_Assignments a ON osa.cCaseId = a.cCaseId and osa.AssignmentDate = a.AssignmentDate
			WHERE 
				Imported = 0
			UPDATE 
				[#AssignedCasesTemp]

			SET @GroupName = grpName = @GroupName + 1
			
			INSERT [dbo].[AssignedCases]
			(
				[caseId], 
				[officerId], 
				[assignmentDate], 
				[userId], 
				[teamId], 
				[groupName]
			)
			SELECT
				[cCaseId], 
				[COfficerId], 
				[assignmentDate], 
				[CUserId], 
				[teamId], 
				[grpName]
			FROM
				[#AssignedCasesTemp]	
			
			DROP TABLE [#AssignedCasesTemp]				
			
			UPDATE [dbo].[Cases] 
			SET [assignable] = 1 
			WHERE [id] in (SELECT DISTINCT cCaseId FROM [oso].[stg_OneStep_Assignments])

			UPDATE [oso].[stg_OneStep_Assignments]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION				
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AssignmentsHistory_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_AssignmentsHistory_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_AssignmentsHistory]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
/****** Object:  StoredProcedure [oso].[usp_OS_AssignmentsHistory_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_AssignmentsHistory_DT]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @GroupName INT;
			SET @GroupName = (SELECT TOP 1 groupName FROM AssignedCasesHistory ORDER BY Id DESC) + 1;
			
			INSERT [dbo].[AssignedCasesHistory]
			(
				[caseId], 
				[officerId], 
				[assignmentDate], 
				[userId], 
				[teamId], 
				[groupName]
			)
			SELECT
				[cCaseId], 
				[COfficerId], 
				[assignmentDate], 
				[CUserId], 
				[teamId], 
				@GroupName
			FROM
				[oso].[stg_OneStep_AssignmentsHistory]
			WHERE Imported = 0

			UPDATE [oso].[stg_OneStep_AssignmentsHistory]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION				
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterEmails_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterEmails_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@EmailAddress varchar(250) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDE')>=-1
		BEGIN
			DEALLOCATE cursorDE
		END

        DECLARE cursorDE CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			EmailAddress
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterEmails]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDE
        FETCH NEXT
        FROM
              cursorDE
        INTO
			@EmailAddress
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterEmails] 
					(
						[DefaulterId], 
						[Email], 
						[DateLoaded], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@EmailAddress,
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDE
            INTO
				@EmailAddress
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDE
        DEALLOCATE cursorDE



	SELECT @Succeed

END
END


/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 18/01/2021 10:25:48 ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [oso].[usp_OS_BU_DefaulterPhones_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterPhones_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@TypeID int ,
			@TelephoneNumber varchar(50) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDP')>=-1
		BEGIN
			DEALLOCATE cursorDP
		END

        DECLARE cursorDP CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			TypeID
			,TelephoneNumber
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterPhones]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDP
        FETCH NEXT
        FROM
              cursorDP
        INTO
			@TypeID
			,@TelephoneNumber 
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterPhones] 
					(
						[defaulterId], 
						[phone], 
						[TypeId],		 
						[LoadedOn], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@TelephoneNumber,
						@TypeID,		
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDP
            INTO
				@TypeID
				,@TelephoneNumber 
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDP
        DEALLOCATE cursorDP



	SELECT @Succeed

END
END

GO
/****** Object:  StoredProcedure [oso].[usp_OS_CA_GetExistingAddress]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CA_GetExistingAddress] @cDefaulterId int, @AddressLine1 varchar(80), @Postcode varchar(12) 
AS
BEGIN


	SELECT TOP 1 
		CanonicalAddress
	FROM
		ContactAddresses ca
	WHERE
		ca.defaulterId = @cDefaulterId and ca.CanonicalAddress = [dbo].[udf_GetCanonicalAddress](@AddressLine1,@Postcode)
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseAgeUpdate_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CaseAgeUpdate_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesAgeDetails TABLE
				(
					[CaseId] [int],
					[CreatedOn] [datetime],
					[BatchDate] [datetime],
					[ExpireOn] [datetime],
					[BatchId] [int]
				)

				INSERT @CasesAgeDetails
				(
					[CaseId],
					[CreatedOn],
					[BatchDate],
					[ExpireOn],
					[BatchId]
				)
				SELECT 
					[ocn].[CaseId],
					[osc].[CreatedOn],
					DATEADD(DAY, [cli].[LifeExpectancy], [osc].[CreatedOn]) [ExpireOn],
					convert(date, [osc].[BatchDate]) BatchDate,
					[c].[batchId]	
				FROM
					[oso].[stg_Onestep_Cases] [osc]
					INNER JOIN [oso].[OSColClentsXRef] [clxref] ON [osc].[ClientId] = [clxref].[CClientId]
					INNER JOIN [oso].[stg_Onestep_Clients] [cli] ON [clxref].[Connid] = [cli].[connid]
					INNER JOIN [dbo].[OldCaseNumbers] [ocn] ON [osc].[OneStepCaseNumber] = [ocn].[OldCaseNumber]
					INNER JOIN [dbo].[Cases] [c] on [ocn].[CaseId] = [c].[Id]

				UPDATE [dbo].[Cases]
				SET 
					[Cases].[createdOn] = [cad].[CreatedOn],
					[Cases].[expiresOn] = [cad].[ExpireOn],
					[Cases].[batchLoadDate] = [cad].[BatchDate]
				FROM
					[Cases] [c]
					INNER JOIN @CasesAgeDetails [cad] ON [c].[Id] = [cad].[CaseId]

				UPDATE [dbo].[Batches]
				SET
					[Batches].[batchDate] = [cad].[BatchDate]
				FROM
					[dbo].[Batches] [ba]
					INNER JOIN (
						SELECT BatchId, MIN(BatchDate) BatchDate
						FROM @CasesAgeDetails
						GROUP BY BatchId
					) [cad] on [ba].[Id] = [cad].[BatchId]
				
					
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    
AS    
    
BEGIN    
SET NOCOUNT ON;    
IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL    
    DROP TABLE #CaseNotes    
    
CREATE TABLE #CaseNotes    
(    
    [Id] INT IDENTITY(1,1)  NOT NULL,     
    [CaseId] [INT] NOT NULL,    
 [CUserId] [int] NULL,    
 [COfficerId] [int] NULL,    
 [Text] [nvarchar](max) NOT NULL,    
 [Occurred] [datetime] NOT NULL,    
 [Imported] [bit] NULL,    
 [ImportedOn] [datetime] NULL    
)    
    
INSERT into #CaseNotes([CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])    
SELECT [CaseId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]    
      FROM oso.Staging_OS_NewCase_Notes_CaseNumber WHERE Imported = 0 AND ErrorId IS NULL    
         
  DECLARE @CaseNoteID INT;      
  DECLARE @CaseId INT;    
  DECLARE @CaseNumber varchar(10);    
  DECLARE @StringVal NVARCHAR(MAX);    
     
  DECLARE CaseNotes CURSOR FAST_FORWARD FOR    
  SELECT Id, CaseId ,Text    
  FROM   #CaseNotes      
     
  OPEN CaseNotes    
  FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @CaseId , @StringVal    
     
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
  BEGIN TRY    
  BEGIN TRANSACTION    
  --SELECT @CaseId = c.Id   FROM  Cases c     
  --WHERE C.caseNumber = @CaseNumber    
  --Select @CaseId as caseiD    
  IF(@CaseId IS NOT NULL)    
  BEGIN     
   DECLARE @TempNotes TABLE (    
     Content NVARCHAR(2000),     
     Id int    
    )    
       
   INSERT INTO @TempNotes    
   EXEC [oso].[usp_SplitString] @StringVal, 2000, 0;    
    
   ----Remove spaces.    
   Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content)     
    
   INSERT INTO dbo.[CaseNotes]     
     ( [caseId]    
      , [userId]    
      , [officerId]    
      , [text]    
      , [occured]    
      , [visible]    
      , [groupId]    
      , [TypeId]    
     )    
     SELECT    
      @CaseId,    
      2, -- System admin    
      NULL,         
      s.Content,    
      GETDATE(),    
      1,    
      NULL,    
      NULL    
     FROM    
      @TempNotes S    
         
   DELETE FROM @TempNotes    
   UPDATE [oso].[Staging_OS_NewCase_Notes_CaseNumber]    
     SET Imported = 1, ImportedOn = GetDate()    
     WHERE CaseId = @CaseId and Imported = 0    
    
  END    
  COMMIT    
  END TRY    
  BEGIN CATCH    
  ROLLBACK TRANSACTION    
    INSERT INTO oso.[SQLErrors] VALUES    
       (Error_number()    
         , Error_severity()    
         , Error_state()    
         , Error_procedure()    
         , Error_line()    
         , Error_message()    
         , Getdate()    
       )    
    
     DECLARE @ErrorId INT = SCOPE_IDENTITY();    
    
     UPDATE    
     [oso].[Staging_OS_NewCase_Notes_CaseNumber]    
     SET    ErrorId = @ErrorId    
     WHERE CaseId = @CaseId and Imported = 0    
    
    SELECT 0 as Succeed    
  END CATCH     
      
    
  FETCH NEXT FROM CaseNotes INTO  @CaseNoteID, @CaseId , @StringVal      
  END    
  CLOSE CaseNotes    
  DEALLOCATE CaseNotes     
END 
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]
AS

BEGIN

IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL
    DROP TABLE #CaseNotes

CREATE TABLE #CaseNotes
(
    [Id] INT IDENTITY(1,1)  NOT NULL, 
    [ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
)

INSERT into #CaseNotes([ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])
SELECT [ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]
					 FROM oso.Staging_OS_NewCase_Notes WHERE Imported = 0 AND ErrorId IS NULL

		DECLARE @CaseNoteID INT;		
		DECLARE @CaseId INT;
		DECLARE @ClientCaseReference varchar(200);
		DECLARE @ClientId INT;
		DECLARE @StringVal NVARCHAR(MAX);
 
		DECLARE CaseNotes CURSOR FAST_FORWARD FOR
		SELECT Id, ClientCaseReference , ClientId ,Text
		FROM   #CaseNotes		
 
		OPEN CaseNotes
		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal
 
		WHILE @@FETCH_STATUS = 0
		BEGIN
  BEGIN TRY
  BEGIN TRANSACTION
		SET @CaseId = 0
		SELECT @CaseId = c.Id   FROM  Cases c 
		INNER JOIN  Batches b  ON c.batchid = b.id
        INNER JOIN  ClientCaseType CT ON  b.clientCaseTypeId = CT.Id
        WHERE CT.clientId = @ClientId 
		AND C.clientCaseReference = @ClientCaseReference
		
		IF(@CaseId IS NOT NULL AND @CaseId <>0)
		BEGIN 
			DECLARE @TempNotes TABLE (
					Content NVARCHAR(2000), 
					Id int
				)

			INSERT INTO @TempNotes
			EXEC [oso].[usp_SplitString] @StringVal, 1950;

			--Remove spaces.
			Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content) 

			INSERT INTO dbo.[CaseNotes] 
					( [caseId]
						, [userId]
						, [officerId]
						, [text]
						, [occured]
						, [visible]
						, [groupId]
						, [TypeId]
					)
					SELECT
						@CaseId,
						2, -- System admin
						NULL,					
						S.Content,
						GETDATE(),
						1,
						NULL,
						NULL
					FROM
						@TempNotes S

			UPDATE [oso].[Staging_OS_NewCase_Notes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0
			
			DELETE FROM @TempNotes
		END
  COMMIT
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )

					DECLARE @ErrorId INT = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_NewCase_Notes]
					SET    ErrorId = @ErrorId
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0

				SELECT 0 as Succeed
  END CATCH	
  

		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal		
		END
		CLOSE CaseNotes
		DEALLOCATE CaseNotes	
END 
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_OS_AddBatches;
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@VehicleVRM varchar(7) ,
			@VehicleMake varchar(50) ,
			@VehicleModel varchar(50) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@TitleLiable2 varchar(50) ,
			@FirstnameLiable2 nvarchar(50) ,
			@MiddlenameLiable2 nvarchar(50) ,
			@LastnameLiable2 nvarchar(50) ,
			@FullnameLiable2 nvarchar(50) ,
			@CompanyNameLiable2 nvarchar(50) ,
			@DOBLiable2 datetime ,
			@NINOLiable2 varchar(13) ,
			@MinorLiable2 bit ,
			@Add1Liable2 nvarchar(80) ,
			@Add2Liable2 nvarchar(320) ,
			@Add3Liable2 nvarchar(100) ,
			@Add4Liable2 nvarchar(100) ,
			@Add5Liable2 nvarchar(100) ,
			@AddCountryLiable2 int ,
			@AddPostCodeLiable2 varchar(12) ,
			@Phone1Liable2 varchar(50) ,
			@Phone2Liable2 varchar(50) ,
			@Phone3Liable2 varchar(50) ,
			@Phone4Liable2 varchar(50) ,
			@Phone5Liable2 varchar(50) ,
			@Email1Liable2 varchar(250) ,
			@Email2Liable2 varchar(250) ,
			@Email3Liable2 varchar(250) ,
			@TitleLiable3 varchar(50) ,
			@FirstnameLiable3 nvarchar(50) ,
			@MiddlenameLiable3 nvarchar(50) ,
			@LastnameLiable3 nvarchar(50) ,
			@FullnameLiable3 nvarchar(150) ,
			@CompanyNameLiable3 nvarchar(50) ,
			@DOBLiable3 datetime ,
			@NINOLiable3 varchar(13) ,
			@MinorLiable3 bit ,
			@Add1Liable3 nvarchar(80) ,
			@Add2Liable3 nvarchar(320) ,
			@Add3Liable3 nvarchar(100) ,
			@Add4Liable3 nvarchar(100) ,
			@Add5Liable3 nvarchar(100) ,
			@AddCountryLiable3 int ,
			@AddPostCodeLiable3 varchar(12) ,
			@Phone1Liable3 varchar(50) ,
			@Phone2Liable3 varchar(50) ,
			@Phone3Liable3 varchar(50) ,
			@Phone4Liable3 varchar(50) ,
			@Phone5Liable3 varchar(50) ,
			@Email1Liable3 varchar(250) ,
			@Email2Liable3 varchar(250) ,
			@Email3Liable3 varchar(250) ,
			@TitleLiable4 varchar(50) ,
			@FirstnameLiable4 nvarchar(50) ,
			@MiddlenameLiable4 nvarchar(50) ,
			@LastnameLiable4 nvarchar(50) ,
			@FullnameLiable4 nvarchar(150) ,
			@CompanyNameLiable4 nvarchar(50) ,
			@DOBLiable4 datetime ,
			@NINOLiable4 varchar(13) ,
			@MinorLiable4 bit ,
			@Add1Liable4 nvarchar(80) ,
			@Add2Liable4 nvarchar(320) ,
			@Add3Liable4 nvarchar(100) ,
			@Add4Liable4 nvarchar(100) ,
			@Add5Liable4 nvarchar(100) ,
			@AddCountryLiable4 int ,
			@AddPostCodeLiable4 varchar(12) ,
			@Phone1Liable4 varchar(50) ,
			@Phone2Liable4 varchar(50) ,
			@Phone3Liable4 varchar(50) ,
			@Phone4Liable4 varchar(50) ,
			@Phone5Liable4 varchar(50) ,
			@Email1Liable4 varchar(250) ,
			@Email2Liable4 varchar(250) ,
			@Email3Liable4 varchar(250) ,
			@TitleLiable5 varchar(50) ,
			@FirstnameLiable5 nvarchar(50) ,
			@MiddlenameLiable5 nvarchar(50) ,
			@LastnameLiable5 nvarchar(50) ,
			@FullnameLiable5 nvarchar(150) ,
			@CompanyNameLiable5 nvarchar(50) ,
			@DOBLiable5 datetime ,
			@NINOLiable5 varchar(13) ,
			@MinorLiable5 bit ,
			@Add1Liable5 nvarchar(80) ,
			@Add2Liable5 nvarchar(320) ,
			@Add3Liable5 nvarchar(100) ,
			@Add4Liable5 nvarchar(100) ,
			@Add5Liable5 nvarchar(100) ,
			@AddCountryLiable5 int ,
			@AddPostCodeLiable5 varchar(12) ,
			@Phone1Liable5 varchar(50) ,
			@Phone2Liable5 varchar(50) ,
			@Phone3Liable5 varchar(50) ,
			@Phone4Liable5 varchar(50) ,
			@Phone5Liable5 varchar(50) ,
			@Email1Liable5 varchar(250) ,
			@Email2Liable5 varchar(250) ,
			@Email3Liable5 varchar(250) ,
			@BatchDate datetime ,
			@OneStepCaseNumber int ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(50) ,
			@DefaultersNames nvarchar(1000) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod int ,
			@EmployerName nvarchar(150) ,
			@EmployerAddressLine1 nvarchar(80) ,
			@EmployerAddressLine2 nvarchar(320) ,
			@EmployerAddressLine3 nvarchar(100) ,
			@EmployerAddressLine4 nvarchar(100) ,
			@EmployerPostcode varchar(12) ,
			@EmployerEmailAddress varchar(50) ,
			@AddCountryLiable1 int ,
			@EmployerFaxNumber varchar(50) ,
			@EmployerTelephone varchar(50) ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,
	
             @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @UnPaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int


		--DECLARE @ResultSet table (alphaNumericCaseNumber varchar(7))

		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

        SELECT
               @ReturnedStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Returned'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			   [ClientCaseReference]
			  ,[ClientDefaulterReference]
			  ,[IssueDate]
			  ,[OffenceCode]
			  ,[OffenceDescription]
			  ,[OffenceLocation]
			  ,[OffenceCourt]
			  ,[DebtAddress1]
			  ,[DebtAddress2]
			  ,[DebtAddress3]
			  ,[DebtAddress4]
			  ,[DebtAddress5]
			  ,[DebtAddressCountry]
			  ,[DebtAddressPostcode]
			  ,[VehicleVRM]
			  ,[VehicleMake]
			  ,[VehicleModel]
			  ,[StartDate]
			  ,[EndDate]
			  ,[TECDate]
			  ,[FineAmount]
			  ,[Currency]
			  ,[MiddlenameLiable1]
			  ,[FullnameLiable1]
			  ,[TitleLiable1]
			  ,[CompanyNameLiable1]
			  ,[FirstnameLiable1]
			  ,[DOBLiable1]
			  ,[LastnameLiable1]
			  ,[NINOLiable1]
			  ,[MinorLiable1]
			  ,[Add1Liable1]
			  ,[Add2Liable1]
			  ,[Add3Liable1]
			  ,[Add4Liable1]
			  ,[Add5Liable1]
			  ,[AddPostCodeLiable1]
			  ,[IsBusinessAddress]
			  ,[Phone1Liable1]
			  ,[Phone2Liable1]
			  ,[Phone3Liable1]
			  ,[Phone4Liable1]
			  ,[Phone5Liable1]
			  ,[Email1Liable1]
			  ,[Email2Liable1]
			  ,[Email3Liable1]
			  ,[MiddlenameLiable2]
			  ,[FullnameLiable2]
			  ,[TitleLiable2]
			  ,[CompanyNameLiable2]
			  ,[FirstnameLiable2]
			  ,[DOBLiable2]
			  ,[LastnameLiable2]
			  ,[NINOLiable2]
			  ,[MinorLiable2]
			  ,[Add1Liable2]
			  ,[Add2Liable2]
			  ,[Add3Liable2]
			  ,[Add4Liable2]
			  ,[Add5Liable2]
			  ,[AddCountryLiable2]
			  ,[AddPostCodeLiable2]
			  ,[Phone1Liable2]
			  ,[Phone2Liable2]
			  ,[Phone3Liable2]
			  ,[Phone4Liable2]
			  ,[Phone5Liable2]
			  ,[Email1Liable2]
			  ,[Email2Liable2]
			  ,[Email3Liable2]
			  ,[TitleLiable3]
			  ,[FirstnameLiable3]
			  ,[MiddlenameLiable3]
			  ,[LastnameLiable3]
			  ,[FullnameLiable3]
			  ,[CompanyNameLiable3]
			  ,[DOBLiable3]
			  ,[NINOLiable3]
			  ,[MinorLiable3]
			  ,[Add1Liable3]
			  ,[Add2Liable3]
			  ,[Add3Liable3]
			  ,[Add4Liable3]
			  ,[Add5Liable3]
			  ,[AddCountryLiable3]
			  ,[AddPostCodeLiable3]
			  ,[Phone1Liable3]
			  ,[Phone2Liable3]
			  ,[Phone3Liable3]
			  ,[Phone4Liable3]
			  ,[Phone5Liable3]
			  ,[Email1Liable3]
			  ,[Email2Liable3]
			  ,[Email3Liable3]
			  ,[TitleLiable4]
			  ,[FirstnameLiable4]
			  ,[MiddlenameLiable4]
			  ,[LastnameLiable4]
			  ,[FullnameLiable4]
			  ,[CompanyNameLiable4]
			  ,[DOBLiable4]
			  ,[NINOLiable4]
			  ,[MinorLiable4]
			  ,[Add1Liable4]
			  ,[Add2Liable4]
			  ,[Add3Liable4]
			  ,[Add4Liable4]
			  ,[Add5Liable4]
			  ,[AddCountryLiable4]
			  ,[AddPostCodeLiable4]
			  ,[Phone1Liable4]
			  ,[Phone2Liable4]
			  ,[Phone3Liable4]
			  ,[Phone4Liable4]
			  ,[Phone5Liable4]
			  ,[Email1Liable4]
			  ,[Email2Liable4]
			  ,[Email3Liable4]
			  ,[TitleLiable5]
			  ,[FirstnameLiable5]
			  ,[MiddlenameLiable5]
			  ,[LastnameLiable5]
			  ,[FullnameLiable5]
			  ,[CompanyNameLiable5]
			  ,[DOBLiable5]
			  ,[NINOLiable5]
			  ,[MinorLiable5]
			  ,[Add1Liable5]
			  ,[Add2Liable5]
			  ,[Add3Liable5]
			  ,[Add4Liable5]
			  ,[Add5Liable5]
			  ,[AddCountryLiable5]
			  ,[AddPostCodeLiable5]
			  ,[Phone1Liable5]
			  ,[Phone2Liable5]
			  ,[Phone3Liable5]
			  ,[Phone4Liable5]
			  ,[Phone5Liable5]
			  ,[Email1Liable5]
			  ,[Email2Liable5]
			  ,[Email3Liable5]
			  ,[BatchDate]
			  ,[OneStepCaseNumber]
			  ,[ClientId]
			  ,[ConnId]
			  ,[ClientName]
			  ,[DefaultersNames]
			  ,[PhaseDate]
			  ,[StageDate]
			  ,[IsAssignable]
			  ,[OffenceDate]
			  ,[CreatedOn]
			  ,[BillNumber]
			  ,[ClientPeriod]
			  ,[EmployerName]
			  ,[EmployerAddressLine1]
			  ,[EmployerAddressLine2]
			  ,[EmployerAddressLine3]
			  ,[EmployerAddressLine4]
			  ,[EmployerPostcode]
			  ,[EmployerEmailAddress]
			  ,[AddCountryLiable1]
			  ,[EmployerFaxNumber]
			  ,[EmployerTelephone]
			  ,[RollNumber]
			  ,[Occupation]
			  ,[BenefitIndicator]
			  ,[CStatus]
			  ,[CReturnCode]
			  ,[OffenceNotes]
		FROM
                 oso.[stg_Onestep_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						SET @CurrentBatchId = NULL
						--Select @CurrentBatchId = Id from Batches where batchDate = @BatchDate and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId)
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches 
						--where referenceNumber = CONCAT(
						--	(select batchRefPrefix from ClientCaseType where clientId = @ClientId),
						--	datepart(year,CAST(CONVERT(date,@BatchDate) AS DateTime)),
						--	FORMAT(CAST(CONVERT(date,@BatchDate) AS DateTime),'MM'),
						--	FORMAT(CAST(CONVERT(date,@BatchDate) AS DateTime),'dd')--,
						--	--FORMAT(osc.batchdate,'hhmmss')
						--	)
						where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) 
						and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) 
						order by Id desc)
						
						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT @StageId = (s.id) from clients cli
                        INNER JOIN stages s on s.clientid = cli.id
                        where cli.id = @ClientId
                                and s.Name = 'Return Case'

						IF (@StageId IS NULL)	-- Client is inactive, so look for stageid from Empty Workflow
						BEGIN
						  SET @StageId = (select s.id from clients cli
												INNER JOIN stages s on s.clientid = cli.id
												INNER JOIN StageTemplates st on s.stagetemplateid = st.id
												INNER JOIN PhaseTemplates pt on st.phasetemplateid = pt.id
												INNER JOIN WorkflowTemplates wft on pt.workflowtemplateid = wft.id
												where cli.id = @ClientId
														and (
														(wft.name <> 'Empty Workflow' and s.onload = 1)
														or
														(wft.name = 'Empty Workflow' and s.name = 'Return Case')
														)
										)
						END

						
						SET @CStatusId = @UnPaidStatusId;
						SET @ReturnRunId = NULL
						IF (@CStatus = 'Returned')
						BEGIN
							SET @CStatusId = @ReturnedStatusId
							SET @ReturnRunId = 1244
						END

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL

						IF (@CaseTypeId = 9)
						BEGIN
							SET @VTECDate = @TECDate
							SET @ANPR = 1
							SET @StartDate =  NULL
							SET @EndDate = NULL
						END

						IF (@CaseTypeId = 21 OR @IsBusinessAddress = 1)
						BEGIN
							SET @IsBusinessAddress = 1
						END

						--Set Offence Date = NULL for CTAX and NNDR and CMS cases 
						IF (@CaseTypeId = 20 OR @CaseTypeId = 21 OR @CaseTypeId = 22)
						BEGIN
							SET @OffenceDate =  NULL
						END
						
						SET @cnid = (  
									SELECT MIN(cn.Id)  
									FROM CaseNumbers AS cn WITH (  
										XLOCK  
										,HOLDLOCK  
										)  
									WHERE cn.Reserved = 0  
									--AND cn.id > = 13999996
									--AND cn.alphaNumericCaseNumber > 'X000000' --Remove this line for LIVE ACTIVE cases.
									)  

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
									--, @FullnameLiable1 --[debtName]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,'') + ISNULL(@DefaultersNames,'')))--[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @BillNumber--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						INSERT INTO dbo.OldCaseNumbers (CaseId, OldCaseNumber)
						VALUES (@CaseId, @OneStepCaseNumber);
						
					-- 3) Insert Case Offences
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid)
						VALUES
						(
							@CaseId,
							@OffenceDate,
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId
						);

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Vehicle
						IF (@CaseTypeId = 9 AND @VehicleVRM IS NOT NULL)
							BEGIN
								INSERT INTO dbo.Vehicles (
									[vrm]
									,[vrmOwner]
									,[defaulterId]
									,[responseStatusId]
									,[responseDate]
									,[responseLoadedDate]
									,[keeperId]
									,[responseStringId]
									,[eventDate]
									,[licenceExpiry]
									,[exportDate]
									,[scrapDate]
									,[theftDate]
									,[recoveryDate]
									,[make]
									,[model]
									,[colour]
									,[taxClass]
									,[seatingCapacity]
									,[previousKeepers]
									,[Motability]
									,[Check]
									,[LastRequestedOn]
									,[isWarrantVrm]
									,[officerId]
									,[VrmReceivedOn]
								)
								VALUES
								(
									@VehicleVRM -- vrm
									,0 -- vrmOwner
									,@LeadDefaulterId
									,NULL -- responseStatusId
									,NULL -- responseDate
									,NULL -- responseLoadedDate
									,NULL -- keeperId
									,NULL -- responseStringId
									,NULL -- eventDate
									,NULL -- licenceExpiry
									,NULL -- exportDate
									,NULL -- scrapDate
									,NULL -- theftDate
									,NULL -- recoveryDate
									,NULL -- make
									,NULL -- model
									,NULL -- colour
									,NULL -- taxCl--s
									,NULL -- seatingCapacity
									,NULL -- previousKeepers
									,0 -- Motability
									,0 -- [Check]
									,NULL -- L--tRequestedOn
									,1 -- isWarrantVrm
									,NULL -- officerId
									,@CreatedOn -- VrmReceivedOn
									);

								SET @VehicleId = SCOPE_IDENTITY();

								INSERT INTO dbo.CaseVehicles
								(
									caseId,
									vehicleId
								)
								VALUES
								(
									@CaseId,
									@VehicleId
								)

							END

					END

					-- Defaulter2
					IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

					END

					-- Defaulter3
					IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			
					-- Defaulter4
					IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END

					-- Defaulter5
					IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			

					-- 5) Insert Contact Addresses


					-- 6) Insert Defaulter Details
					-- 7) Insert Defaulter Cases
					-- 8 ) Insert Vehicles
							-- Note: LeadDefaulterId is required


					-- 9) Insert Case Note
					-- Not required for now


						UPDATE
						[oso].[stg_Onestep_Cases]
						SET    Imported   = 1
						, ImportedOn = GetDate()
						WHERE
						Imported = 0 
						AND OneStepCaseNumber = @OneStepCaseNumber
						AND ClientId = @ClientId
						AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_Onestep_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					OneStepCaseNumber = @OneStepCaseNumber
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
        END
        CLOSE cursorT
        DEALLOCATE cursorT

		
		-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
		BEGIN TRANSACTION	
			UPDATE ca 
			Set TidReviewedId = 1
			from dbo.ContactAddresses ca
			join dbo.defaultercases dc on ca.defaulterid = dc.defaulterid
			join dbo.cases c on dc.caseid = c.id
			INNER join dbo.OldCaseNumbers oc on c.Id = oc.CaseId
			INNER join oso.stg_Onestep_Cases osc on osc.OneStepCaseNumber = oc.OldCaseNumber
			where 
			osc.Imported = 1 AND
			c.addressline1 = ca.addressline1 AND
			c.postcode = ca.postcode	
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_GetClientsXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Cases_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			c.[ConnId]
			,c.[CClientId] as ClientId
			,c.[ClientName]
			,CASE
			WHEN ct.name ='Council Tax Liability Order' THEN 'CT'
			WHEN ct.name ='National Non Domestic Rates' THEN 'ND'
			WHEN ct.name ='RTA/TMA (RoadTraffic/Traffic Management)' THEN 'PC'
			WHEN ct.name ='Child Maintenance Group' THEN 'CS'
			WHEN ct.name ='Legal Aid' THEN 'LAA'
			WHEN ct.name ='Debt Collection' THEN 'DCA'
		END AS OffenceCode

		FROM [oso].[OSColClentsXRef] c 
		INNER JOIN dbo.ClientCaseType cct ON c.CClientId = cct.clientId
		INNER JOIN dbo.CaseType ct ON cct.caseTypeId = ct.Id
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Cases staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_XRef_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_Cases]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Cases from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_XRef_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

				INSERT INTO dbo.[OldCaseNumbers] (CaseId, OldCaseNumber)
				SELECT c.Id as CaseId, osC.OneStepCaseNumber as OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
				Except
				SELECT c.Id as CaseId, ocn.OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
					inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId AND osC.OneStepCaseNumber = ocn.OldCaseNumber)
				ORDER BY CaseId DESC

				COMMIT
				SELECT 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_CaseStatus_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[OneStep_CaseStatus_CrossRef] 
			(
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			)
			SELECT
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			FROM
				[oso].[stg_OneStep_CaseStatus_CrossRef]	 
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 22-10-2019
-- Description: To clear [oso].[stg_OneStep_CaseStatus_CrossRef] staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_CaseStatus_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_CaseStatus_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear charges staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From [oso].[Staging_OS_Charges]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 08-04-2019
-- Description:	Copy data from staging table to CaseCharges table.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_DT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						INSERT INTO [dbo].[CaseCharges]( 
								[caseId]
								,[userId]
								,[officerId]
								,[amount]
								,[chargeDate]
								,[schemeChargeId]
								,[reversedOn]
								,[vatAmount]
								,[paidAmount]
								,[paidVatAmount]
								,[casePaymentId]
								,[refundId]
								,[CaseNoteId]
								,[isCaseBalanceAdjust]
								,[ClientInvoiceRunId])
						SELECT [cCaseId]
								,[CUserId]
								,[COfficerId]
								,[Amount]
								,[AddedOn]
								,[CSchemeChargeId]
								,NULL
								,0.00
								,[PaidAmount]
								,0.00
								,NULL
								,NULL
								,NULL
								,0
								,NULL
						FROM [oso].[Staging_OS_Charges] 
						WHERE 
							Imported = 0

						UPDATE [oso].[Staging_OS_Charges]
						SET Imported = 1, ImportedOn = GetDate()
						WHERE Imported = 0
					COMMIT TRANSACTION
					SELECT 1 as Succeed
				END TRY
				BEGIN CATCH        
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION 
					END

					INSERT INTO oso.[SQLErrors] 
					VALUES (Error_number(), 
							Error_severity(), 
							Error_state(), 
							Error_procedure(), 
							Error_line(), 
							Error_message(), 
							Getdate()) 

					SELECT 0 as Succeed
				END CATCH
	
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Charges_GetCaseId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as CCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_charges_GetSchemeChargeXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep charge type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_charges_GetSchemeChargeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT [CSchemeChargeName]
		  ,[OSSchemeChargeName] AS ChargeType
		  ,[CSchemeChargeId]
		FROM [oso].[OneStep_SchemeCharge_CrossRef]	
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM oso.[stg_Onestep_Clients]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					--@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,
					@IsReturnForAPartPaidCasePermitted		bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						--,RegionId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,IsReturnForAPartPaidCasePermitted

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						--,@RegionId
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						,@IsReturnForAPartPaidCasePermitted
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  --,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									--,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
									,PermitReturnForPartPaidCase
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
										,@IsReturnForAPartPaidCasePermitted
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							SELECT @PriorityChargeId = sc.id
							FROM 
								clientcasetype cct
								INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
								INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
								INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							WHERE 
								cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									--,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									,@IsReturnForAPartPaidCasePermitted
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_GetClientId]
@ClientName nvarchar (250),
@CaseType nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @OSClientName nvarchar(500)
		Set @OSClientName = @ClientName + ' (' + @CaseType + ')'
		
		Select top 1 c.Id	  
	   from Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @OSClientName
		AND cct.[abbreviationName] = @CaseType
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_ContactAddresses]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
		INSERT [dbo].[ContactAddresses] 
		(
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		)
		SELECT 
			[addressline1], 
			[addressline1], 
			[addressline2], 
			[addressline2], 
			[addressline3], 
			[addressline4], 
			[addressline5], 
			[Postcode], 
			[cDefaulterId], 
			1, 
			2, 
			231, 
			0 
		FROM 
			[oso].[stg_OneStep_ContactAddresses]
		WHERE 
			(Imported = 0	OR Imported IS NULL)
		EXCEPT
		SELECT 
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		FROM 
			[dbo].[ContactAddresses]

		UPDATE [oso].[stg_OneStep_ContactAddresses]
		SET Imported = 1, ImportedOn = GetDate()
		WHERE (Imported = 0	OR Imported IS NULL)

		COMMIT TRANSACTION	
		SELECT
			1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CopyCCRToCDR]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To copy CCR to CDR if CDR is null or empty
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_CopyCCRToCDR]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
				BEGIN TRANSACTION
				
				UPDATE c SET c.ClientDefaulterReference = CASE CHARINDEX('_', c.clientCaseReference)
										 WHEN 0 THEN c.clientCaseReference
										 WHEN 1 THEN ''
										 ELSE LEFT(c.clientCaseReference, CHARINDEX('_', c.clientCaseReference)-1)
						   END
				FROM cases c inner join dbo.[Batches] b on c.batchid = b.id 
				inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id    
				inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
				inner join dbo.CaseStatus cs on c.statusId = cs.Id

				WHERE cct.BrandId in (7,8,9)
				AND (c.ClientDefaulterReference IS NULL OR  c.ClientDefaulterReference = '')
				AND b.batchDate >= CAST( GETDATE() AS Date )

				COMMIT TRANSACTION				
				SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CStatus_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CStatus_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesVariableTable TABLE
				(	
					[CaseId] INT,
					[StatusId] INT,
					[StatusDate] DATETIME,
					[PreviousStatusId] INT,
					[PreviousStatusDate] DATETIME
				)

				INSERT @CasesVariableTable 
				(
					[CaseId],
					[StatusId],
					[StatusDate],
					[PreviousStatusId],
					[PreviousStatusDate]
				)
				SELECT 
					[c].[Id], 
					[oss].[CCaseStatusId], 
					GETDATE(),	 
					[c].[statusId], 
					[c].[statusDate]
				FROM [dbo].[Cases] [c]
				INNER JOIN [oso].[stg_OneStep_CaseStatuses] [oss] on [c].[Id] = [oss].[cCaseId]

				UPDATE [dbo].[Cases]
				SET
					[Cases].[statusId] = [cvt].[StatusId],
					[Cases].[statusDate] = [cvt].[StatusDate],
					[Cases].[previousStatusDate] = [cvt].[PreviousStatusDate],
					[Cases].[previousStatusId] = [cvt].[PreviousStatusId]  
				FROM 
					[dbo].[Cases] [c]
					INNER JOIN @CasesVariableTable [cvt] on [c].[Id] = [cvt].[CaseId]
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterEmails_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_DefaulterEmails]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END

DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterEmails_DT]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterEmails_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterEmails] 
			(
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[EmailAddress],				
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterEmails]
			WHERE 
				(Imported = 0 OR Imported IS NULL	)
			EXCEPT
			SELECT 
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			FROM 
				[dbo].[DefaulterEmails]

			UPDATE [oso].[stg_OneStep_DefaulterEmails]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL	)

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION 
				END

				INSERT INTO oso.[SQLErrors] 
				VALUES (Error_number(), 
						Error_severity(), 
						Error_state(), 
						Error_procedure(), 
						Error_line(), 
						Error_message(), 
						Getdate()) 

				SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterPhones_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_DefaulterPhones]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterPhones_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterPhones] 
			(
				[defaulterId], 
				[phone], 
				[TypeId],		 
				[LoadedOn], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[TelephoneNumber],
				[TypeID],		
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterPhones]
			WHERE 
				(Imported = 0 OR Imported IS NULL)
			EXCEPT
			SELECT 
				[defaulterId], 
				[phone], 
				[TypeId], 		
				[LoadedOn], 
				[SourceId]		
			FROM 
				[dbo].[DefaulterPhones]

			UPDATE [oso].[stg_OneStep_DefaulterPhones]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL)

			COMMIT TRANSACTION	
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DirectPayment_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 04-05-2020
-- Description: To clear DirectPayment staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_DirectPayment_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From [oso].[Staging_OS_DirectPayment]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DirectPayment_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  BP
-- Create date: 04-05-2020
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_DirectPayment_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId				INT
                    , @Amount				DECIMAL(18,4)
                    , @UserId				INT
                    , @OfficerId			INT
                    , @AddedOn				DATETIME
                    , @PaymentId			INT
                    , @PaymentStatusId		INT
                    , @SchemePaymentId		INT
                    , @CaseNoteId			INT
                    , @HistoryTypeId		INT
					, @ClearedOn			DATETIME					

				SELECT
						@PaymentStatusId = [Id]
				FROM
						[dbo].[PaymentStatus]
				WHERE
						[name] = 'DIRECT PAYMENT'

				SELECT
						@SchemePaymentId = [Id]
				FROM
					   [dbo].[SchemePayments] 
				WHERE						
						[name] = 'Direct Payment'
				
				SELECT
						@HistoryTypeId = [Id]
				FROM
						[dbo].[HistoryTypes]
				WHERE
						[name] = 'UpdateClientBalance'
                DECLARE cursorT
                CURSOR
						--LOCAL STATIC
						--LOCAL FAST_FORWARD
						--LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT
						CCaseId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId,						
						ClearedOn
                    FROM
                        oso.[Staging_OS_DirectPayment]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@CaseId,
						@Amount,
						@AddedOn,
						@UserId,
						@OfficerId,						
						@ClearedOn
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        -- Add new rows
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]
                                    , [transactionDate]
                                    , [amount]
                                    , [clearedOn]
                                    , [bouncedOn]
                                    , [recordedBy]
                                    , [officerId]
                                    , [REFERENCE]
                                    , [authorisationNumber]
                                    , [journalStatusId]
                                    , [receiptTypeId]
                                    , [trusted]
                                    , [schemePaymentId]
                                    , [paymentStatusId]
                                    , [surchargeAmount]
                                    , [paymentID]
                                    , [reconciledOn]
                                    , [reconcileReference]
                                    , [reversedOn]
                                    , [parentId]
                                    , [previousPaymentStatusId]
                                    , [reconciledBy]
                                    , [keepGoodsRemovedStatus]
                                    , [cardStatementReference]
                                    , [bankReconciledOn]
                                    , [bounceReference]
                                    , [reconciledBouncedOn]
                                    , [reconciledBouncedBy]
                                    , [exceptionalAdjustmentId]
                                    , [bankReconciledBy]
                                    , [isReversedChargePayment]
                                    , [paymentOriginId]
                                    , [uploadedFileReference]
                                    , [ReverseOrBounceNotes]
                               )
                               VALUES
                               ( @AddedOn
                                    , @AddedOn
                                    , @Amount
                                    , @ClearedOn
                                    , NULL
                                    , @UserId
                                    , @OfficerId
                                    , NUll
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 1
                                    , @SchemePaymentId
                                    , @PaymentStatusId
                                    , 0.00
                                    , 'MigratedPayment'
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , NULL
                                    , 0
                                    , 0
                                    , 0
                                    , 0
                               )
                        ;
                        
                        -- Add new Case Notes
                        INSERT INTO dbo.[CaseNotes]
                               ( [caseId]
                                    , [officerId]
                                    , [userId]
                                    , [text]
                                    , [occured]
                                    , [visible]
                                    , [groupId]
                                    , [TypeId]
                               )
                               VALUES
                               ( @CaseId
                                    , @OfficerId
                                    , @UserId
                                    , 'Add direct payment of ' + FORMAT(@Amount,'N2', 'en-US') 
                                    , @AddedOn
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @CaseNoteId = SCOPE_IDENTITY();
                        -- Add new Case History
                        INSERT INTO dbo.[CaseHistory]
                               ( [caseId]
                                    , [userId]
                                    , [officerId]
                                    , [COMMENT]
                                    , [occured]
                                    , [typeId]
                                    , [CaseNoteId]
                               )
                               VALUES
                               ( @CaseId
                                    , @UserId
                                    , @OfficerId
                                    , 'Direct Payment has been completed. Direct Payment amount = ' + FORMAT(@Amount,'N2', 'en-US')  + '.'
                                    , @AddedOn
                                    , @HistoryTypeId
                                    , @CaseNoteId
                               )
                        ;
                        
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
							@CaseId,
							@Amount,
							@AddedOn,
							@UserId,
							@OfficerId,							
							@ClearedOn
                    END
                    CLOSE cursorT
                    DEALLOCATE cursorT 

					UPDATE [oso].[Staging_OS_DirectPayment]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0	

					COMMIT TRANSACTION
					SELECT 1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT 0 as Succeed
                END CATCH
            END
        END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as cCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_PROD]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rituraj Choudhury
-- Create date: 11-08-2020
-- Description:	Extract CaseNumber, CaseId and ClientCaseReference using ClientName
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_PROD]
@ClientName nvarchar (max)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CaseId, c.ClientCaseReference, c.caseNumber as CaseNumber	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusId]
@OSCaseStatusName varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT 
			Id [CCaseStatusId]
			,name [CCaseStatusName]
		FROM CaseStatus
		WHERE 
			name = CASE @OSCaseStatusName
						WHEN 'Arrangement' THEN 'Unpaid'
						WHEN 'Cancelled' THEN 'Returned'
						WHEN 'Live' THEN 'Unpaid'
						WHEN 'Fully Paid' THEN 'Paid'
						WHEN 'Successful' THEN 'Paid'
						WHEN 'Trace' THEN 'Tracing'
						WHEN 'Expired' THEN 'Returned'
					END
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- Description:	Select columbus and onestep case status
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [OSCaseStatusName]
      ,[CCaseStatusName]
	  ,[CCaseStatusId]
  FROM [oso].[OneStep_CaseStatus_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMapping_DCA_LAA]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping_DCA_LAA]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (10,11,12,13)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMappingByClientId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCasesXMappingByClientId]
@ClientId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, c.ClientCaseReference, c.ClientDefaulterReference, c.CaseNumber as cCaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cl.Id = @ClientId
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference,'YYYYYYYYYYYYYY' AS ClientDefaulterReference,'ZZZZZZZ' AS cCaseNumber
		
		ORDER BY c.Id DESC

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseTypeXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseTypeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	CaseType
	,CaseTypeId
  FROM [oso].[OSColCaseTypeXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientIdByName]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 19-04-2021
-- Description:	Extract client id by name
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientIdByName]
@ClientName nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		
		Select top 1 c.Id as ClientId, c.name As ClientName	  
		From Clients c 
		Where c.[name] = @ClientName
		UNION
		SELECT 99999999 AS ClientId, 'XXXXXXXXXXXXXXXX' AS ClientName

    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientParentXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientsXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[ConnId]
	,[CClientId]
	,[ClientName]
  FROM [oso].[OSColClentsXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetColCaseNumbersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetColCaseNumbersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

SELECT c.caseNumber as CaseNumber, ocn.OldCaseNumber AS OurRef 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCStatusId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCStatusId] @OSCaseStatusName VARCHAR(50)
AS
BEGIN 
	SELECT TOP 1 CCaseStatusId 
	FROM [oso].[OneStep_CaseStatus_CrossRef] 
	WHERE OSCaseStatusName = @OSCaseStatusName
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetDefaulterId] @DefaulterName nvarchar(150), @CaseNumber nvarchar(10)
AS
BEGIN
	select [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	JOIN [OldCaseNumbers] [ocn] on [c].[Id] = [ocn].CaseId
	where [d].[name] = @DefaulterName and [ocn].[OldCaseNumber] = @CaseNumber
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterIdByCDR]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDefaulterIdByCDR] @DefaulterName nvarchar(150), @CCaseId int
AS
BEGIN
	select Top 1 [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	where [d].[name] = @DefaulterName and c.id = @CCaseId
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedEmailAddress]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedEmailAddress] @cDefaulterId int, @EmailAddress varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], de.Email as cEmailAddress
	from [Defaulters] [d]
	Inner Join DefaulterEmails de on d.Id = de.defaulterId
	where [d].[Id] = @cDefaulterId
	AND de.Email = @EmailAddress
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedPhoneNo]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedPhoneNo] @cDefaulterId int, @TelephoneNumber varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], dp.phone as cTelephoneNumber
	from [Defaulters] [d]
	Inner Join DefaulterPhones dp on d.Id = dp.defaulterId
	where [d].[Id] = @cDefaulterId
	AND dp.phone = @TelephoneNumber
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOfficersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId]
	,[ColId]
  FROM [oso].[OSColOfficersXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOfficerTypesCrossRef]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetOfficerTypesCrossRef]
AS
BEGIN
	SELECT Id typeId, abbreviation OfficerType FROM OfficerTypes 
	WHERE isDeleted = 0
END

GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetReturnCodeXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetReturnCodeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CReturnCode]
      ,[OSReturnCode]
  FROM [oso].[OneStep_ReturnCode_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemeChargeId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		US
-- Create date: 25-07-2018
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemeChargeId]
@CSchemeChargeName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @ChargingSchemeId INT;		

		SELECT @ChargingSchemeId = id FROM ChargingScheme
		WHERE [schemeName] = 'Rossendales Standard TCE Charging Scheme'

		SELECT sc.ID AS CSchemeChargeId
		FROM SchemeCharges sc 
		WHERE chargingSchemeId = @ChargingSchemeId and sc.name = @CSchemeChargeName
		
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemeChargeXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemeChargeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemeChargeName]
      ,[OSSchemeChargeName] AS ChargeType
	  ,[CSchemeChargeId]
  FROM [oso].[OneStep_SchemeCharge_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Updated:		BP
-- Create date: 25-07-2018
-- Update date: 06-02-2020
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentId]
@CSchemePaymentName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @PaymentSchemeIds TABLE (PaymentSchemeId INT)

		INSERT INTO @PaymentSchemeIds (PaymentSchemeId)
		SELECT id FROM PaymentScheme
		WHERE [name] in ('Rossendales Generic Payment Scheme', 'Rossendales RTA Generic Payment Scheme')

		SELECT sp.ID AS CSchemePaymentId
		FROM SchemePayments sp		
		WHERE paymentSchemeId in (SELECT PaymentSchemeId FROM @PaymentSchemeIds) and sp.name = @CSchemePaymentName		
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep payment type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemePaymentName]
      ,[OSSchemePaymentName]
	  ,[CSchemePaymentId]
  FROM [oso].[OneStep_SchemePayment_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetStageTemplateId]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BP
-- Create date: 21-10-2019
-- Description:	Extract Stage Template Id for OS(Rossendales)
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetStageTemplateId]
@CStageName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @CStageTemplateId int;
		DECLARE @CPhaseTemplateId int;
		SELECT @CStageTemplateId = st.Id, @CPhaseTemplateId = pt.Id 
		FROM 
			WorkflowTemplates wt
			JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
			JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
		WHERE 
			wt.Name = 'Rossendales 14 days 1 Letter' and st.Name = @CStageName

		SELECT 
			StageTemplateId [CStageTemplateId], PhaseTemplateId [CPhaseTemplateId]
		FROM 
			[oso].[stg_OneStep_Stages_CrossRef]
		WHERE
			StageTemplateId = @CStageTemplateId and PhaseTemplateId = @CPhaseTemplateId
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Get Count for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUserCount]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetUsersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId],
			[ColId]
		FROM [oso].[OSColUsersXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_History_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_History]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_History_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
				
				INSERT INTO dbo.[CaseHistory] 
				( 
					[caseId],
					[userId],
					[officerId],
					[Comment],
					[occured],
					[typeId],
					[CaseNoteId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Comment],
					[Occurred],	
					case [HistoryTypeName]
						when 'Broken' then 28
						when 'Clear arrange' then 29
						when 'Clear plan' then 29
						when 'Payment status' then 32
						when 'Status changed' then 42
						when 'Success' then 42
						when 'DVLA info' then 49
						when 'Removed agent' then 53
						when 'Allocated' then 54
						when 'Arrangement' then 56
						when 'Payment plan' then 56
						when 'Cancelled' then 57
						when 'Documents' then 59
						when 'Address' then 68
						when 'Linked' then 71
						when 'Unlinked' then 72
						when 'Fee added' then 74
						when 'Fee status' then 74
						when 'Debt Changed' then 75
						when 'Stage' then 97
						when 'DVLA Enquiry' then 102
						when 'Return Details' then 110
						when 'Off hold' then 111
						when 'Hold' then 112
						when 'Phone' then 123
						when 'COG Visit' then 126
						when 'No COG visit' then 126
						when 'Contact details' then 137
						when 'PayOnline' then 140
						when 'Moved' then 145
						when 'Figures changed' then 150
						when 'Phone (inbound)' then 181
						when 'Letter' then 302
						when 'SMS to debtor' then 608
						else 0
					end,					
					[CaseNoteId]
                FROM
                    oso.[Staging_OS_History]
				WHERE 
					Imported = 0 OR Imported IS NULL
					
				UPDATE [oso].[Staging_OS_History]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0 OR Imported IS NULL	

				COMMIT TRANSACTION
                SELECT
                    1 as Succeed
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION
                END
                INSERT INTO oso.[SQLErrors] VALUES
                        (Error_number()
                            , Error_severity()
                            , Error_state()
                            , Error_procedure()
                            , Error_line()
                            , Error_message()
                            , Getdate()
                        )
                SELECT
                    0 as Succeed
            END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Holds staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Holds_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_Holds]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Holds_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @CaseId                        INT
			, @UserId                          INT
			, @CaseNoteId                      INT
			, @HistoryTypeId_CaseStatusChanged INT
			, @HistoryTypeId_HoldCase          INT
			, @CaseStatusId_OnHold             INT
			, @CaseStatusId_Current            INT
			, @StatusDate_Current              DATETIME
			, @HoldReasonId                    INT
			, @CaseExpiryDate                  DATETIME
			, @CaseStatusName_Current          VARCHAR(250)
			, @CaseNumber                      VARCHAR(250)
			, @Comment                         VARCHAR(700)
			, @AddedOn                         DATETIME
			, @StageId                         INT
			, @StageName					   VARCHAR(250)
			, @ClientId                        INT
			, @PhaseTypeId                     INT
			, @StageCategory                       VARCHAR(250)
			, @HoldOn              DATETIME
			, @HoldUntil              DATETIME
			, @ClientHold			bit 
			, @HoldReasonName					   VARCHAR(128)
                
		SELECT
				@HistoryTypeId_CaseStatusChanged = [Id]
		FROM
				[dbo].[HistoryTypes]
		WHERE
				[name] = 'CaseStatusChanged'

		SELECT
				@HistoryTypeId_HoldCase = [Id]
		FROM
				[dbo].[HistoryTypes]
		WHERE
				[name] = 'HoldCase'

		SELECT
				@CaseStatusId_OnHold = [Id]
		FROM
				[dbo].[CaseStatus]
		WHERE
				[name] = 'On-Hold'



		DECLARE cursorT
		CURSOR
			--LOCAL STATIC
			--LOCAL FAST_FORWARD
			--LOCAL READ_ONLY FORWARD_ONLY
			FOR
			SELECT
				[cCaseId],
				[cUserId],
				[HoldReason],
				[HoldOn],
				[HoldUntil],
				[ClientHold]


			FROM
				oso.[Staging_OS_Holds]
			WHERE 
				Imported = 0
			OPEN cursorT
			FETCH NEXT
			FROM
					cursorT
			INTO
					@CaseId
				, @UserId
				, @Comment  
				, @HoldOn
				, @HoldUntil
				, @ClientHold

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT
							@CaseStatusId_Current   = c.statusId
							, @CaseStatusName_Current = cs.[name]
							, @StatusDate_Current     = c.statusDate
							, @CaseExpiryDate         = c.expiresOn
							, @CaseNumber             = c.caseNumber
							, @StageId             = c.stageId

				FROM
							dbo.[Cases] c
							INNER JOIN
										dbo.[CaseStatus] cs
										on
													c.statusId = cs.Id
				WHERE
							c.id = @CaseId

				SET @HoldReasonName = 'Client Request'

				IF (@ClientHold = 0)
					SET @HoldReasonName = 'Other'		

				SELECT
						@HoldReasonId = [Id]
				FROM
						[dbo].[HoldReasons]
				WHERE
						[name] = @HoldReasonName						


				-- Update stage and status of the case
				UPDATE
						dbo.[Cases]
				SET    [previousStatusId]   = @CaseStatusId_Current
						, [previousStatusDate] = @StatusDate_Current
						, [statusId]           = @CaseStatusId_OnHold
						, [statusDate]         = GetDate()
				WHERE
						[ID] = @CaseId

				INSERT INTO [dbo].[HoldCases]
				( 
					[caseId],
					[holdOn],
					[holdUntil],
					[recommencedOn],
					[holdReason],
					[recommenceReason],
					[lateStatDec],
					[holdReasonId],
					[AmendedByHoldCaseId]
				)
				VALUES
				( 
					@CaseId,
					@HoldOn,
					@HoldUntil,
					null,
					@Comment,
					null,
					0,
					@HoldReasonId,
					null
				)

			-- Add new Case Notes for case status changes
			--INSERT INTO dbo.[CaseNotes]
			--       ( [caseId]
			--            , [officerId]
			--            , [userId]
			--            , [text]
			--            , [occured]
			--            , [visible]
			--            , [groupId]
			--            , [TypeId]
			--       )
			--       VALUES
			--       ( @CaseId
			--            , NULL
			--            , @UserId
			--            , @Comment
			--            , GetDate()
			--            , 1
			--            , NULL
			--            , NULL
			--       )
			--       SET @CaseNoteId = @@IDENTITY;
			--       -- Case Status Chaged
			--       INSERT INTO dbo.[CaseHistory]
			--              ( [caseId]
			--                   , [userId]
			--                   , [officerId]
			--                   , [COMMENT]
			--                   , [occured]
			--                   , [typeId]
			--                   , [CaseNoteId]
			--              )
			--              VALUES
			--              ( @CaseId
			--                   , @UserId
			--                   , NULL
			--                   , 'Case status changed to: On-Hold from: ' + @CaseStatusName_Current + '.'
			--                   , GetDate()
			--                   , @HistoryTypeId_CaseStatusChanged
			--                   , null
			--              )
			--       -- On-Hold
			--       INSERT INTO dbo.[CaseHistory]
			--              ( [caseId]
			--                   , [userId]
			--                   , [officerId]
			--                   , [COMMENT]
			--                   , [occured]
			--                   , [typeId]
			--                   , [CaseNoteId]
			--              )
			--              VALUES
			--              ( @CaseId
			--                   , @UserId
			--                   , NULL
			--                   , 'HoldCase: CaseNumber = ' + @CaseNumber + 'has been put on hold for ' + CONVERT(varchar(10),DATEDIFF(day, GetDate(), @CaseExpiryDate)) + ' days. The last day of hold period is ' + convert(varchar, @CaseExpiryDate, 103) + '.'
			--                   , GetDate()
			--                   , @HistoryTypeId_HoldCase
			--                   , @CaseNoteId
			--              )
					-- New rows creation completed	
				FETCH NEXT
				FROM
					cursorT
				INTO
					@CaseId
				, @UserId
				, @Comment  
				, @HoldOn
				, @HoldUntil
				, @ClientHold
			END
			CLOSE cursorT
			DEALLOCATE cursorT

			UPDATE [oso].[Staging_OS_Holds]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0	

			COMMIT
			SELECT
					1 as Succeed
			END TRY
			BEGIN CATCH
			ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
				SELECT
						0 as Succeed
			END CATCH
		END
	END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Live_Cases_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Live_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_OS_AddBatches;
		--COMMIT TRANSACTION

        DECLARE 
			@ClientCaseReference varchar(200) ,
			@ClientDefaulterReference varchar(30) ,
			@IssueDate datetime ,
			@OffenceCode varchar(5) ,
			@OffenceDescription nvarchar(1000) ,
			@OffenceLocation nvarchar(300) ,
			@OffenceCourt nvarchar(50) ,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320) ,
			@DebtAddress3 nvarchar(100) ,
			@DebtAddress4 nvarchar(100) ,
			@DebtAddress5 nvarchar(100) ,
			@DebtAddressCountry int ,
			@DebtAddressPostcode varchar(12) ,
			@VehicleVRM varchar(7) ,
			@VehicleMake varchar(50) ,
			@VehicleModel varchar(50) ,
			@StartDate datetime ,
			@EndDate datetime ,
			@TECDate datetime ,
			@FineAmount decimal(18, 4) ,
			@Currency int ,
			@TitleLiable1 varchar(50) ,
			@FirstnameLiable1 nvarchar(50) ,
			@MiddlenameLiable1 nvarchar(50) ,
			@LastnameLiable1 nvarchar(50) ,
			@FullnameLiable1 nvarchar(150) ,
			@CompanyNameLiable1 nvarchar(50) ,
			@DOBLiable1 datetime ,
			@NINOLiable1 varchar(13) ,
			@MinorLiable1 bit ,
			@Add1Liable1 nvarchar(80) ,
			@Add2Liable1 nvarchar(320) ,
			@Add3Liable1 nvarchar(100) ,
			@Add4Liable1 nvarchar(100) ,
			@Add5Liable1 nvarchar(100) ,
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@Phone1Liable1 varchar(50) ,
			@Phone2Liable1 varchar(50) ,
			@Phone3Liable1 varchar(50) ,
			@Phone4Liable1 varchar(50) ,
			@Phone5Liable1 varchar(50) ,
			@Email1Liable1 varchar(250) ,
			@Email2Liable1 varchar(250) ,
			@Email3Liable1 varchar(250) ,
			@TitleLiable2 varchar(50) ,
			@FirstnameLiable2 nvarchar(50) ,
			@MiddlenameLiable2 nvarchar(50) ,
			@LastnameLiable2 nvarchar(50) ,
			@FullnameLiable2 nvarchar(50) ,
			@CompanyNameLiable2 nvarchar(50) ,
			@DOBLiable2 datetime ,
			@NINOLiable2 varchar(13) ,
			@MinorLiable2 bit ,
			@Add1Liable2 nvarchar(80) ,
			@Add2Liable2 nvarchar(320) ,
			@Add3Liable2 nvarchar(100) ,
			@Add4Liable2 nvarchar(100) ,
			@Add5Liable2 nvarchar(100) ,
			@AddCountryLiable2 int ,
			@AddPostCodeLiable2 varchar(12) ,
			@Phone1Liable2 varchar(50) ,
			@Phone2Liable2 varchar(50) ,
			@Phone3Liable2 varchar(50) ,
			@Phone4Liable2 varchar(50) ,
			@Phone5Liable2 varchar(50) ,
			@Email1Liable2 varchar(250) ,
			@Email2Liable2 varchar(250) ,
			@Email3Liable2 varchar(250) ,
			@TitleLiable3 varchar(50) ,
			@FirstnameLiable3 nvarchar(50) ,
			@MiddlenameLiable3 nvarchar(50) ,
			@LastnameLiable3 nvarchar(50) ,
			@FullnameLiable3 nvarchar(150) ,
			@CompanyNameLiable3 nvarchar(50) ,
			@DOBLiable3 datetime ,
			@NINOLiable3 varchar(13) ,
			@MinorLiable3 bit ,
			@Add1Liable3 nvarchar(80) ,
			@Add2Liable3 nvarchar(320) ,
			@Add3Liable3 nvarchar(100) ,
			@Add4Liable3 nvarchar(100) ,
			@Add5Liable3 nvarchar(100) ,
			@AddCountryLiable3 int ,
			@AddPostCodeLiable3 varchar(12) ,
			@Phone1Liable3 varchar(50) ,
			@Phone2Liable3 varchar(50) ,
			@Phone3Liable3 varchar(50) ,
			@Phone4Liable3 varchar(50) ,
			@Phone5Liable3 varchar(50) ,
			@Email1Liable3 varchar(250) ,
			@Email2Liable3 varchar(250) ,
			@Email3Liable3 varchar(250) ,
			@TitleLiable4 varchar(50) ,
			@FirstnameLiable4 nvarchar(50) ,
			@MiddlenameLiable4 nvarchar(50) ,
			@LastnameLiable4 nvarchar(50) ,
			@FullnameLiable4 nvarchar(150) ,
			@CompanyNameLiable4 nvarchar(50) ,
			@DOBLiable4 datetime ,
			@NINOLiable4 varchar(13) ,
			@MinorLiable4 bit ,
			@Add1Liable4 nvarchar(80) ,
			@Add2Liable4 nvarchar(320) ,
			@Add3Liable4 nvarchar(100) ,
			@Add4Liable4 nvarchar(100) ,
			@Add5Liable4 nvarchar(100) ,
			@AddCountryLiable4 int ,
			@AddPostCodeLiable4 varchar(12) ,
			@Phone1Liable4 varchar(50) ,
			@Phone2Liable4 varchar(50) ,
			@Phone3Liable4 varchar(50) ,
			@Phone4Liable4 varchar(50) ,
			@Phone5Liable4 varchar(50) ,
			@Email1Liable4 varchar(250) ,
			@Email2Liable4 varchar(250) ,
			@Email3Liable4 varchar(250) ,
			@TitleLiable5 varchar(50) ,
			@FirstnameLiable5 nvarchar(50) ,
			@MiddlenameLiable5 nvarchar(50) ,
			@LastnameLiable5 nvarchar(50) ,
			@FullnameLiable5 nvarchar(150) ,
			@CompanyNameLiable5 nvarchar(50) ,
			@DOBLiable5 datetime ,
			@NINOLiable5 varchar(13) ,
			@MinorLiable5 bit ,
			@Add1Liable5 nvarchar(80) ,
			@Add2Liable5 nvarchar(320) ,
			@Add3Liable5 nvarchar(100) ,
			@Add4Liable5 nvarchar(100) ,
			@Add5Liable5 nvarchar(100) ,
			@AddCountryLiable5 int ,
			@AddPostCodeLiable5 varchar(12) ,
			@Phone1Liable5 varchar(50) ,
			@Phone2Liable5 varchar(50) ,
			@Phone3Liable5 varchar(50) ,
			@Phone4Liable5 varchar(50) ,
			@Phone5Liable5 varchar(50) ,
			@Email1Liable5 varchar(250) ,
			@Email2Liable5 varchar(250) ,
			@Email3Liable5 varchar(250) ,
			@BatchDate datetime ,
			@OneStepCaseNumber int ,
			@ClientId int ,
			@ConnId int ,
			@ClientName nvarchar(80) ,
			@DefaultersNames nvarchar(1000) ,
			@ExpiresOn datetime ,
			@Status varchar(50) ,
			@Phase varchar(50) ,
			@PhaseDate datetime ,
			@Stage varchar(50) ,
			@StageDate datetime ,
			@IsAssignable bit ,
			@OffenceDate datetime ,
			@CreatedOn datetime ,
			@BillNumber varchar(20) ,
			@ClientPeriod varchar(20) ,
			@EmployerName nvarchar(150) ,
			@EmployerAddressLine1 nvarchar(80) ,
			@EmployerAddressLine2 nvarchar(320) ,
			@EmployerAddressLine3 nvarchar(100) ,
			@EmployerAddressLine4 nvarchar(100) ,
			@EmployerPostcode varchar(12) ,
			@EmployerEmailAddress varchar(50) ,
			@AddCountryLiable1 int ,
			@EmployerFaxNumber varchar(50) ,
			@EmployerTelephone varchar(50) ,
			@RollNumber nvarchar(1) ,
			@Occupation nvarchar(1) ,
			@BenefitIndicator nvarchar(1) ,
			@CStatus varchar(20) ,
			@CReturnCode int,
			@OffenceNotes nvarchar(1000) ,	
			@Welfare bit,
			@DateOfBirth datetime,
			@NINO nvarchar(13),
			@OffenceValue decimal(18,4),
			--@OffenceNumber nvarchar(1000) ,	



             @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @CaseId                INT
            , @UnPaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit
			, @ErrorId INT
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int





		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

        SELECT
               @ReturnedStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Returned'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			   [ClientCaseReference]
			  ,[ClientDefaulterReference]
			  ,[IssueDate]
			  ,[OffenceCode]
			  ,[OffenceDescription]
			  ,[OffenceLocation]
			  ,[OffenceCourt]
			  ,[DebtAddress1]
			  ,[DebtAddress2]
			  ,[DebtAddress3]
			  ,[DebtAddress4]
			  ,[DebtAddress5]
			  ,[DebtAddressCountry]
			  ,[DebtAddressPostcode]
			  ,[VehicleVRM]
			  ,[VehicleMake]
			  ,[VehicleModel]
			  ,[StartDate]
			  ,[EndDate]
			  ,[TECDate]
			  ,[FineAmount]
			  ,[Currency]
			  ,[MiddlenameLiable1]
			  ,[FullnameLiable1]
			  ,[TitleLiable1]
			  ,[CompanyNameLiable1]
			  ,[FirstnameLiable1]
			  ,[DOBLiable1]
			  ,[LastnameLiable1]
			  ,[NINOLiable1]
			  ,[MinorLiable1]
			  ,[Add1Liable1]
			  ,[Add2Liable1]
			  ,[Add3Liable1]
			  ,[Add4Liable1]
			  ,[Add5Liable1]
			  ,[AddPostCodeLiable1]
			  ,[IsBusinessAddress]
			  ,[Phone1Liable1]
			  ,[Phone2Liable1]
			  ,[Phone3Liable1]
			  ,[Phone4Liable1]
			  ,[Phone5Liable1]
			  ,[Email1Liable1]
			  ,[Email2Liable1]
			  ,[Email3Liable1]
			  ,[MiddlenameLiable2]
			  ,[FullnameLiable2]
			  ,[TitleLiable2]
			  ,[CompanyNameLiable2]
			  ,[FirstnameLiable2]
			  ,[DOBLiable2]
			  ,[LastnameLiable2]
			  ,[NINOLiable2]
			  ,[MinorLiable2]
			  ,[Add1Liable2]
			  ,[Add2Liable2]
			  ,[Add3Liable2]
			  ,[Add4Liable2]
			  ,[Add5Liable2]
			  ,[AddCountryLiable2]
			  ,[AddPostCodeLiable2]
			  ,[Phone1Liable2]
			  ,[Phone2Liable2]
			  ,[Phone3Liable2]
			  ,[Phone4Liable2]
			  ,[Phone5Liable2]
			  ,[Email1Liable2]
			  ,[Email2Liable2]
			  ,[Email3Liable2]
			  ,[TitleLiable3]
			  ,[FirstnameLiable3]
			  ,[MiddlenameLiable3]
			  ,[LastnameLiable3]
			  ,[FullnameLiable3]
			  ,[CompanyNameLiable3]
			  ,[DOBLiable3]
			  ,[NINOLiable3]
			  ,[MinorLiable3]
			  ,[Add1Liable3]
			  ,[Add2Liable3]
			  ,[Add3Liable3]
			  ,[Add4Liable3]
			  ,[Add5Liable3]
			  ,[AddCountryLiable3]
			  ,[AddPostCodeLiable3]
			  ,[Phone1Liable3]
			  ,[Phone2Liable3]
			  ,[Phone3Liable3]
			  ,[Phone4Liable3]
			  ,[Phone5Liable3]
			  ,[Email1Liable3]
			  ,[Email2Liable3]
			  ,[Email3Liable3]
			  ,[TitleLiable4]
			  ,[FirstnameLiable4]
			  ,[MiddlenameLiable4]
			  ,[LastnameLiable4]
			  ,[FullnameLiable4]
			  ,[CompanyNameLiable4]
			  ,[DOBLiable4]
			  ,[NINOLiable4]
			  ,[MinorLiable4]
			  ,[Add1Liable4]
			  ,[Add2Liable4]
			  ,[Add3Liable4]
			  ,[Add4Liable4]
			  ,[Add5Liable4]
			  ,[AddCountryLiable4]
			  ,[AddPostCodeLiable4]
			  ,[Phone1Liable4]
			  ,[Phone2Liable4]
			  ,[Phone3Liable4]
			  ,[Phone4Liable4]
			  ,[Phone5Liable4]
			  ,[Email1Liable4]
			  ,[Email2Liable4]
			  ,[Email3Liable4]
			  ,[TitleLiable5]
			  ,[FirstnameLiable5]
			  ,[MiddlenameLiable5]
			  ,[LastnameLiable5]
			  ,[FullnameLiable5]
			  ,[CompanyNameLiable5]
			  ,[DOBLiable5]
			  ,[NINOLiable5]
			  ,[MinorLiable5]
			  ,[Add1Liable5]
			  ,[Add2Liable5]
			  ,[Add3Liable5]
			  ,[Add4Liable5]
			  ,[Add5Liable5]
			  ,[AddCountryLiable5]
			  ,[AddPostCodeLiable5]
			  ,[Phone1Liable5]
			  ,[Phone2Liable5]
			  ,[Phone3Liable5]
			  ,[Phone4Liable5]
			  ,[Phone5Liable5]
			  ,[Email1Liable5]
			  ,[Email2Liable5]
			  ,[Email3Liable5]
			  ,[BatchDate]
			  ,[OneStepCaseNumber]
			  ,[ClientId]
			  ,[ConnId]
			  ,[ClientName]
			  ,[DefaultersNames]
			  ,[PhaseDate]
			  ,[StageDate]
			  ,[IsAssignable]
			  ,[OffenceDate]
			  ,[CreatedOn]
			  ,[BillNumber]
			  ,[ClientPeriod]
			  ,[EmployerName]
			  ,[EmployerAddressLine1]
			  ,[EmployerAddressLine2]
			  ,[EmployerAddressLine3]
			  ,[EmployerAddressLine4]
			  ,[EmployerPostcode]
			  ,[EmployerEmailAddress]
			  ,[AddCountryLiable1]
			  ,[EmployerFaxNumber]
			  ,[EmployerTelephone]
			  ,[RollNumber]
			  ,[Occupation]
			  ,[BenefitIndicator]
			  ,[CStatus]
			  ,[CReturnCode]
			  ,[OffenceNotes]
			  ,[Welfare]
		      ,[DateOfBirth]
			  ,[NINO]
			  ,[OffenceValue]
			  --,[OffenceNumber]
		FROM
                 oso.[stg_Onestep_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				 -- ,@OffenceNumber
        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						SET @CurrentBatchId = NULL
						--Select @CurrentBatchId = Id from Batches where batchDate = @BatchDate and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId)
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						
						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT @StageId = (s.id) from clients cli
                        INNER JOIN stages s on s.clientid = cli.id
                        where cli.id = @ClientId
                                and s.Name = 'Migration Hold Stage'

						IF (@StageId IS NULL)	-- Client is inactive, so look for stageid from Empty Workflow
						BEGIN
						  SET @StageId = (select s.id from clients cli
												INNER JOIN stages s on s.clientid = cli.id
												INNER JOIN StageTemplates st on s.stagetemplateid = st.id
												INNER JOIN PhaseTemplates pt on st.phasetemplateid = pt.id
												INNER JOIN WorkflowTemplates wft on pt.workflowtemplateid = wft.id
												where cli.id = @ClientId
														and (
														(wft.name <> 'Empty Workflow' and s.onload = 1)
														or
														(wft.name = 'Empty Workflow' and s.name = 'Return Case')
														)
										)
						END

						
						SET @CStatusId = @UnPaidStatusId;
						SET @ReturnRunId = NULL
						IF (@CStatus = 'Returned')
						BEGIN
							SET @CStatusId = @ReturnedStatusId
							SET @ReturnRunId = 1244
						END

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL

						IF (@CaseTypeId = 9)
						BEGIN
							SET @VTECDate = @TECDate
							SET @ANPR = 1
							SET @StartDate =  NULL
							SET @EndDate = NULL
						END

						IF (@CaseTypeId = 21 OR @IsBusinessAddress = 1)
						BEGIN
							SET @IsBusinessAddress = 1
						END

						--Set Offence Date = NULL for CTAX and NNDR and CMS cases 
						IF (@CaseTypeId = 20 OR @CaseTypeId = 21 OR @CaseTypeId = 22)
						BEGIN
							SET @OffenceDate =  NULL
						END
						
						SET @cnid = (  
									SELECT MIN(cn.Id)  
									FROM CaseNumbers AS cn WITH (  
										XLOCK  
										,HOLDLOCK  
										)  
									WHERE cn.Reserved = 0  
									--AND cn.id > = 13999996
									--AND cn.alphaNumericCaseNumber > 'X000000' --Remove this line for LIVE ACTIVE cases.
									)  

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, @ReturnRunId --[returnRunId]
									, NULL --[defaulterEmail]
									--, @FullnameLiable1 --[debtName]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,'') + ISNULL(@DefaultersNames,'')))--[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, @ClientPeriod--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						INSERT INTO dbo.OldCaseNumbers (CaseId, OldCaseNumber)
						VALUES (@CaseId, convert(varchar(20), @OneStepCaseNumber)
);
						
					-- 3) Insert Case Offences
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						VALUES
						(
							@CaseId,
							@OffenceDate,
							@OffenceLocation,
							@OffenceNotes,
							@OffenceCodeId,
							@OffenceValue
						);

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DateOfBirth,
						0,
						0,
						NULL,
						0,
						@NINO,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Vehicle
						IF (@CaseTypeId = 9 AND @VehicleVRM IS NOT NULL)
							BEGIN
								INSERT INTO dbo.Vehicles (
									[vrm]
									,[vrmOwner]
									,[defaulterId]
									,[responseStatusId]
									,[responseDate]
									,[responseLoadedDate]
									,[keeperId]
									,[responseStringId]
									,[eventDate]
									,[licenceExpiry]
									,[exportDate]
									,[scrapDate]
									,[theftDate]
									,[recoveryDate]
									,[make]
									,[model]
									,[colour]
									,[taxClass]
									,[seatingCapacity]
									,[previousKeepers]
									,[Motability]
									,[Check]
									,[LastRequestedOn]
									,[isWarrantVrm]
									,[officerId]
									,[VrmReceivedOn]
								)
								VALUES
								(
									@VehicleVRM -- vrm
									,0 -- vrmOwner
									,@LeadDefaulterId
									,NULL -- responseStatusId
									,NULL -- responseDate
									,NULL -- responseLoadedDate
									,NULL -- keeperId
									,NULL -- responseStringId
									,NULL -- eventDate
									,NULL -- licenceExpiry
									,NULL -- exportDate
									,NULL -- scrapDate
									,NULL -- theftDate
									,NULL -- recoveryDate
									,NULL -- make
									,NULL -- model
									,NULL -- colour
									,NULL -- taxCl--s
									,NULL -- seatingCapacity
									,NULL -- previousKeepers
									,0 -- Motability
									,0 -- [Check]
									,NULL -- L--tRequestedOn
									,1 -- isWarrantVrm
									,NULL -- officerId
									,@CreatedOn -- VrmReceivedOn
									);

								SET @VehicleId = SCOPE_IDENTITY();

								INSERT INTO dbo.CaseVehicles
								(
									caseId,
									vehicleId
								)
								VALUES
								(
									@CaseId,
									@VehicleId
								)

							END

					END

					-- Defaulter2
					IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

					END

					-- Defaulter3
					IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			
					-- Defaulter4
					IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END

					-- Defaulter5
					IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
			

					-- 5) Insert Contact Addresses


					-- 6) Insert Defaulter Details
					-- 7) Insert Defaulter Cases
					-- 8 ) Insert Vehicles
							-- Note: LeadDefaulterId is required


					-- 9) Insert Case Note
					-- Not required for now


						UPDATE
						[oso].[stg_Onestep_Cases]
						SET    Imported   = 1
						, ImportedOn = GetDate()
						WHERE
						Imported = 0 
						AND OneStepCaseNumber = @OneStepCaseNumber
						AND ClientId = @ClientId
						AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_Onestep_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					OneStepCaseNumber = @OneStepCaseNumber
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				   @ClientCaseReference
				  ,@ClientDefaulterReference
				  ,@IssueDate
				  ,@OffenceCode
				  ,@OffenceDescription
				  ,@OffenceLocation
				  ,@OffenceCourt
				  ,@DebtAddress1
				  ,@DebtAddress2
				  ,@DebtAddress3
				  ,@DebtAddress4
				  ,@DebtAddress5
				  ,@DebtAddressCountry
				  ,@DebtAddressPostcode
				  ,@VehicleVRM
				  ,@VehicleMake
				  ,@VehicleModel
				  ,@StartDate
				  ,@EndDate
				  ,@TECDate
				  ,@FineAmount
				  ,@Currency
				  ,@MiddlenameLiable1
				  ,@FullnameLiable1
				  ,@TitleLiable1
				  ,@CompanyNameLiable1
				  ,@FirstnameLiable1
				  ,@DOBLiable1
				  ,@LastnameLiable1
				  ,@NINOLiable1
				  ,@MinorLiable1
				  ,@Add1Liable1
				  ,@Add2Liable1
				  ,@Add3Liable1
				  ,@Add4Liable1
				  ,@Add5Liable1
				  ,@AddPostCodeLiable1
				  ,@IsBusinessAddress
				  ,@Phone1Liable1
				  ,@Phone2Liable1
				  ,@Phone3Liable1
				  ,@Phone4Liable1
				  ,@Phone5Liable1
				  ,@Email1Liable1
				  ,@Email2Liable1
				  ,@Email3Liable1
				  ,@MiddlenameLiable2
				  ,@FullnameLiable2
				  ,@TitleLiable2
				  ,@CompanyNameLiable2
				  ,@FirstnameLiable2
				  ,@DOBLiable2
				  ,@LastnameLiable2
				  ,@NINOLiable2
				  ,@MinorLiable2
				  ,@Add1Liable2
				  ,@Add2Liable2
				  ,@Add3Liable2
				  ,@Add4Liable2
				  ,@Add5Liable2
				  ,@AddCountryLiable2
				  ,@AddPostCodeLiable2
				  ,@Phone1Liable2
				  ,@Phone2Liable2
				  ,@Phone3Liable2
				  ,@Phone4Liable2
				  ,@Phone5Liable2
				  ,@Email1Liable2
				  ,@Email2Liable2
				  ,@Email3Liable2
				  ,@TitleLiable3
				  ,@FirstnameLiable3
				  ,@MiddlenameLiable3
				  ,@LastnameLiable3
				  ,@FullnameLiable3
				  ,@CompanyNameLiable3
				  ,@DOBLiable3
				  ,@NINOLiable3
				  ,@MinorLiable3
				  ,@Add1Liable3
				  ,@Add2Liable3
				  ,@Add3Liable3
				  ,@Add4Liable3
				  ,@Add5Liable3
				  ,@AddCountryLiable3
				  ,@AddPostCodeLiable3
				  ,@Phone1Liable3
				  ,@Phone2Liable3
				  ,@Phone3Liable3
				  ,@Phone4Liable3
				  ,@Phone5Liable3
				  ,@Email1Liable3
				  ,@Email2Liable3
				  ,@Email3Liable3
				  ,@TitleLiable4
				  ,@FirstnameLiable4
				  ,@MiddlenameLiable4
				  ,@LastnameLiable4
				  ,@FullnameLiable4
				  ,@CompanyNameLiable4
				  ,@DOBLiable4
				  ,@NINOLiable4
				  ,@MinorLiable4
				  ,@Add1Liable4
				  ,@Add2Liable4
				  ,@Add3Liable4
				  ,@Add4Liable4
				  ,@Add5Liable4
				  ,@AddCountryLiable4
				  ,@AddPostCodeLiable4
				  ,@Phone1Liable4
				  ,@Phone2Liable4
				  ,@Phone3Liable4
				  ,@Phone4Liable4
				  ,@Phone5Liable4
				  ,@Email1Liable4
				  ,@Email2Liable4
				  ,@Email3Liable4
				  ,@TitleLiable5
				  ,@FirstnameLiable5
				  ,@MiddlenameLiable5
				  ,@LastnameLiable5
				  ,@FullnameLiable5
				  ,@CompanyNameLiable5
				  ,@DOBLiable5
				  ,@NINOLiable5
				  ,@MinorLiable5
				  ,@Add1Liable5
				  ,@Add2Liable5
				  ,@Add3Liable5
				  ,@Add4Liable5
				  ,@Add5Liable5
				  ,@AddCountryLiable5
				  ,@AddPostCodeLiable5
				  ,@Phone1Liable5
				  ,@Phone2Liable5
				  ,@Phone3Liable5
				  ,@Phone4Liable5
				  ,@Phone5Liable5
				  ,@Email1Liable5
				  ,@Email2Liable5
				  ,@Email3Liable5
				  ,@BatchDate
				  ,@OneStepCaseNumber
				  ,@ClientId
				  ,@ConnId
				  ,@ClientName
				  ,@DefaultersNames
				  ,@PhaseDate
				  ,@StageDate
				  ,@IsAssignable
				  ,@OffenceDate
				  ,@CreatedOn
				  ,@BillNumber
				  ,@ClientPeriod
				  ,@EmployerName
				  ,@EmployerAddressLine1
				  ,@EmployerAddressLine2
				  ,@EmployerAddressLine3
				  ,@EmployerAddressLine4
				  ,@EmployerPostcode
				  ,@EmployerEmailAddress
				  ,@AddCountryLiable1
				  ,@EmployerFaxNumber
				  ,@EmployerTelephone
				  ,@RollNumber
				  ,@Occupation
				  ,@BenefitIndicator
				  ,@CStatus
				  ,@CReturnCode
				  ,@OffenceNotes
				  ,@Welfare
				  ,@DateOfBirth
				  ,@NINO
				  ,@OffenceValue
				  --,@OffenceNumber

        END
        CLOSE cursorT
        DEALLOCATE cursorT

		
		-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
		BEGIN TRANSACTION	
			UPDATE ca 
			Set TidReviewedId = 1
			from dbo.ContactAddresses ca
			join dbo.defaultercases dc on ca.defaulterid = dc.defaulterid
			join dbo.cases c on dc.caseid = c.id
			INNER join dbo.OldCaseNumbers oc on c.Id = oc.CaseId
			INNER join oso.stg_Onestep_Cases osc on convert(varchar(20), osc.OneStepCaseNumber) = oc.OldCaseNumber
			where 
			osc.Imported = 1 AND
			c.addressline1 = ca.addressline1 AND
			c.postcode = ca.postcode	
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT] 
AS 
  BEGIN 
	SET NOCOUNT ON;
    CREATE TABLE #nvpairs 
                 ( 
                              [Id] int IDENTITY(1, 1) NOT NULL, 
                              [ClientCaseReference] [varchar](200) NOT NULL, 
                              [ClientId] [int] NOT NULL, 
                              [nvpString] [nvarchar](max) NOT NULL, 
                              [concatenatednvpString] nvarchar(max) NULL 
                 ) 
    INSERT INTO #nvpairs 
    SELECT clientcasereference, 
           clientid, 
           namevaluepairs             AS nvpstring, 
           concatenatednamevaluepairs AS concatenatednvpstring 
    FROM   oso.staging_os_newcase_additionalcasedata 
    WHERE  imported = 0 
    AND    errorid IS NULL 


    DECLARE @NVPID                 INT; 
    DECLARE @CaseId                INT; 
    DECLARE @CCRef                 VARCHAR(200); 
    DECLARE @ClId                  INT; 
    DECLARE @StringVal             NVARCHAR(max); 
    DECLARE @concatenatednvpString NVARCHAR(max); 
    DECLARE nvp CURSOR fast_forward FOR 
    SELECT id, 
           clientcasereference, 
           clientid, 
           nvpstring, 
           concatenatednvpstring 
    FROM   #nvpairs 
    OPEN nvp 
    FETCH next 
    FROM  nvp 
    INTO  @NVPID, 
          @CCRef, 
          @ClId, 
          @StringVal, 
          @concatenatednvpString 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		SET @CaseId = 0
        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClId 
        AND        c.clientcasereference = @CCRef 
		
		--debug
		--Select @CaseId, @ClId, @CCRef

        IF( @CaseId IS NOT NULL AND @CaseId <>0 ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                                             ( 
												id            INT IDENTITY(1, 1) NOT NULL,
												namevaluepair NVARCHAR(max),
												namevalueid   INT
                                             ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                                  namevaluepair, 
                                  namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @StringVal, 
            251, 
            0; 

			DELETE FROM #SingleNameValuePair where namevaluepair = ''
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
              
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
              
			  SET @Value = REPLACE(@Value,';','')
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 

			  IF Object_id('tempdb..#NameValue') IS NOT NULL 
			  DROP TABLE #namevalue

            END 
            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @CCRef 
          AND    clientid = @ClId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @CCRef 
        AND    clientid = @ClId 
        --AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @StringVal 
        SELECT 0 AS succeed 
      END catch 
      FETCH next 
      FROM  nvp 
      INTO  @NVPID, 
            @CCRef, 
            @ClId, 
            @StringVal, 
            @concatenatednvpString 
    END 
    CLOSE nvp 
    DEALLOCATE nvp 
  END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_old-10-08-2020]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT_old-10-08-2020] 
AS 
  BEGIN 
	SET NOCOUNT ON;

	WHILE Exists(Select Top 1 * From oso.staging_os_newcase_additionalcasedata 
							WHERE  imported = 0 
							AND    errorid IS NULL )
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		
		DECLARE @ClientCaseReference [varchar](200);
		DECLARE @ClientId [int];
		DECLARE @nvpString [nvarchar](max);
		DECLARE @concatenatednvpString nvarchar(max);
		DECLARE @CaseId INT; 

		SELECT @ClientCaseReference = ClientCaseReference, @ClientId = ClientId, @nvpString = NameValuePairs, @concatenatednvpString = ConcatenatedNameValuePairs
		FROM oso.Staging_OS_NewCase_AdditionalCaseData
		WHERE  imported = 0 AND    errorid IS NULL 

        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClientId 
        AND        c.clientcasereference = @ClientCaseReference 
		
        IF( @CaseId IS NOT NULL ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                        ( 
							id            INT IDENTITY(1, 1) NOT NULL,
							namevaluepair NVARCHAR(max),
							namevalueid   INT
                        ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                        namevaluepair, 
                        namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @nvpString, 
            251, 
            0; 
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
				
				
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
               
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 
            END 

			IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
		  IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
          DROP TABLE #SINGLENAMEVALUEPAIR 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
		  

          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @ClientCaseReference 
          AND    clientid = @ClientId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @ClientCaseReference 
        AND    clientid = @ClientId 
       -- AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @ 
        SELECT 0 AS succeed 
      END catch 
    END 
  END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId 	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewClients_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 03-11-2020
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewClients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					--@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						--,RegionId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						--,@RegionId
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  --,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									--,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						--DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						--INSERT @SplitAddressChangeStage (Words) 
						--SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						--SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						--from @SplitAddressChangeStage WHERE Id = 1

						--SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						--FROM @SplitAddressChangeStage WHERE Id = 2  
						
						--SELECT @AddressChangeStageId = s.Id 
						--FROM
						--	StageTemplates st
						--	INNER JOIN Stages s on st.Id = s.StageTemplateId  
						--	INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						--WHERE 
						--	st.name = @AddressChangeStageName 
						--	and pht.Name = @AddressChangePhaseName
						--	and s.clientId = @ClientId 
						--	and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
									else 0									
								end
								FROM InstalmentFrequency						
							
							/* uncommented this section BP & US start*/							
								SELECT @PriorityChargeId = sc.id
								FROM 
									clientcasetype cct
									INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
									INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
									INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
								WHERE 
									cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)
							
								INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
								VALUES (@ClientCaseTypeID, @PriorityChargeId)
							--/* uncommented this section BP & US end*/
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							--INSERT INTO oso.[OSColClentsXRef]
							--	(
							--		[ConnId]
							--		,[CClientId]
							--		,[ClientName]
							--	)

							--	VALUES
							--	(
							--		@connid
							--		,@ClientId
							--		,@Name
							--	)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									--,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Notes_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Notes_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_Notes]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Notes_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Notes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

				INSERT INTO dbo.[CaseNotes] 
				( [caseId]
					, [userId]
					, [officerId]
					, [text]
					, [occured]
					, [visible]
					, [groupId]
					, [TypeId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Text],
					[Occurred],
					1,
					NULL,
					NULL
				FROM
					oso.[Staging_OS_Notes]
				WHERE 
					Imported = 0

				UPDATE [oso].[Staging_OS_Notes]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0

				COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Officers staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From oso.stg_Onestep_Officers
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Officers from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 

					@officerno [int],
					@id [int] ,
					@drakeOfficerId [int] ,
					@firstName [varchar](80),
					@middleName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@phone [varchar](25),
					@started [datetime] ,
					@leftDate [datetime],
					@typeId [int] ,
					@maxCases [int],
					@minFee [decimal](18, 4),
					@averageFee [decimal](18, 4),
					@minExecs [int],
					@signature [varchar](50) ,
					@enabled [bit], 
					@performanceTarget  [decimal](10, 2),
					@outcodes [varchar](1000) ,
					@Manager [bit],
					@parentId [int] ,
					@isRtaCertified [bit] ,
					@rtaCertificationExpiryDate [datetime] ,
					@marston [bit] ,
					@rossendales [bit],
					@swift [bit] ,
					@rossendalesdca [bit] ,
					@marstonlaa [bit] ,
					@ctax [bit] ,
					@nndr [bit] ,
					@tma [bit] ,
					@cmg [bit] ,
					@dca [bit] ,
					@laa [bit] ,
					@Existing [bit] ,					
				
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BRossendalesDCAId int,
					@BMarstonLAAId int,
					@RTM_CTId int,
					@CTAX_CTId int,
					@NNDR_CTId int,
					@CMG_CTId int,
					@DCA_CTId int,
					@LAA_CTId int,
					@Comment [varchar] (250)
					
					SELECT @RTM_CTId = Id FROM [dbo].[CaseType] WHERE name = 'RTA/TMA (RoadTraffic/Traffic Management)'				
					SELECT @CTAX_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Council Tax Liability Order'				
					SELECT @NNDR_CTId = Id FROM [dbo].[CaseType] WHERE name = 'National Non Domestic Rates'				
					SELECT @CMG_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Child Maintenance Group'
					SELECT @DCA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Debt Collection'
					SELECT @LAA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Legal Aid'
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						[officerno]
						,[id]
						,[drakeOfficerId]
						,[firstName]
						,[middleName]
						,[lastName]
						,[email]
						,[phone]
						,[started]
						,[leftDate]
						,[typeId]
						,[maxCases]
						,[minFee]
						,[averageFee]
						,[minExecs]
						,[signature]
						,[enabled]
						,[performanceTarget]
						,[outcodes]
						,[Manager]
						,[parentId]
						,[isRtaCertified]
						,[rtaCertificationExpiryDate]
						,[marston]
						,[rossendales]
						,[swift]
						,[rossendalesdca]
						,[marstonlaa]
						,[ctax]
						,[nndr]
						,[tma]
						,[cmg]
						,[dca]
						,[laa]
						,[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Officers]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
					IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColOfficersXRef] WHERE OSId = @Id

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' + @firstName + ' ' + @middleName + ' ' + @lastName;
									THROW 51000, @Comment, 163; 
								
								END								

							END
					ELSE
					BEGIN
						IF NOT EXISTS (SELECT * FROM dbo.[Officers])
						INSERT INTO dbo.[Officers]
							(
							  [officerNumber]
							  ,[drakeOfficerId]
							  ,[firstName]
							  ,[middleName]
							  ,[lastName]
							  ,[email]
							  ,[phone]
							  ,[started]
							  ,[leftDate]
							  ,[typeId]
							  ,[maxCases]
							  ,[minFee]
							  ,[averageFee]
							  ,[minExecs]
							  ,[signature]
							  ,[enabled]
							  ,[performanceTarget]
							  ,[outcodes]
							  ,[isManager]
							  ,[parentId]
							  ,[isRtaCertified]
							  ,[rtaCertificationExpiryDate]	
							)

							VALUES
							(

							  @officerno
							  ,@officerno
							  ,@firstName
							  ,@middleName
							  ,@lastName
							  ,@email
							  ,@phone
							  ,@started
							  ,@leftDate
							  ,@typeId
							  ,@maxCases
							  ,@minFee
							  ,@averageFee
							  ,@minExecs
							  ,@signature
							  ,@enabled
							  ,@performanceTarget
							  ,@outcodes
							  ,@Manager
							  ,@parentId
							  ,@isRtaCertified
							  ,@rtaCertificationExpiryDate								  
							)

							SET @ColId = SCOPE_IDENTITY();

							-- Add OfficersBrands
							IF (@marston = 1)
								BEGIN
									SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands] 
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonId
									)
								END
			
							IF (@rossendales = 1)
								BEGIN
									SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendalesId
									)
								END			
						
							IF (@swift = 1)
								BEGIN
									SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftId
									)
								END		
								
							-- Add to CaseTypeInclusion
							IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
							INSERT INTO dbo.[CaseTypeInclusion]
									(
										[caseTypeId]
										,[officerId]
									)

									VALUES
									(
										@RTM_CTId
										,@ColId
									),									
									(
										@CTAX_CTId
										,@ColId
									),
									(
										@NNDR_CTId
										,@ColId
									),
									(
										@CMG_CTId
										,@ColId
									)

							-- Add to OfficerBrandedSettings table
							IF NOT EXISTS (SELECT * FROM dbo.[OfficerBrandedSettings])
							INSERT INTO dbo.OfficerBrandedSettings
								(
									officerid
									, BrandTypeId
									, OfficerPerformanceBonusSchemeId
									, OfficerRateId
								) 
								
								VALUES 
								( 
									@ColId
									, 1
									, 1
									, 6
								)

							-- Add to XRef table
							IF NOT EXISTS (SELECT * FROM oso.[OSColOfficersXRef])
							INSERT INTO oso.[OSColOfficersXRef]
								(
									[OSOfficerNumber]
									,[OSId]
									,[COfficerNumber]
									,[ColId]
								)

								VALUES
								(
									@officerno
									,@id
									,@officerno
									,@ColId
								)										

							-- New rows creation completed	

					END

					IF (@rossendalesdca = 1)
						BEGIN
							SELECT @BRossendalesDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BRossendalesDCAId
							)
						END	

					IF (@marstonlaa = 1)
						BEGIN
							SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BMarstonLAAId
							)
						END	

					-- Add to CaseTypeInclusion
					IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
					INSERT INTO dbo.[CaseTypeInclusion]
							(
								[caseTypeId]
								,[officerId]
							)
							VALUES
							(
								@DCA_CTId
								,@ColId
							),									
							(
								@LAA_CTId
								,@ColId
							)

					FETCH NEXT
					FROM
						cursorT
					INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Officers]
				  SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0	

				  COMMIT
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_GetOfficerCount]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_GetOfficerCount]
@firstName nvarchar (250),
@lastName nvarchar (250),
@drakeOfficerId int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Officers where (firstName = @firstName and lastName = @lastName) OR (drakeOfficerId = @drakeOfficerId)
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Payments_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId                    INT
                    , @Amount                      DECIMAL(18,4)
                    , @UserId                      INT
                    , @AddedOn                     DATETIME
                    , @PaymentId                   INT
                    , @SchemePaymentId             INT
					, @PaymentStatus			   VARCHAR (5)
					, @PaymentSource			   VARCHAR (50)
                    , @ClearedOn                   DATETIME
                    , @ClientCaseReference         VARCHAR(200)
                    , @OriginalCaseStatus          VARCHAR(50)
                    , @OriginalCaseStatusId        INT
                    , @PaidStatusId                INT
                    , @PartPaidStatusId            INT
                    , @FinalClearedStatusId        INT
                    , @FinalUnclearedStatusId      INT
                    , @PaymentStatusId             INT
                    , @HistoryId_PaymentCleared    INT
                    , @HistoryId_CaseStatusChanged INT
                    , @NewCaseStatusId             INT
					, @OfficerId				   INT
                    , @OldCaseNumber				   VARCHAR(20)
					, @ClientPaymentRunId		   INT
					, @SplitDebt						decimal(18, 4)
					, @SplitCosts					decimal(18, 4)
					, @SplitFees						decimal(18, 4)
					, @SplitOther					decimal(18, 4)
					, @SplitVan						decimal(18, 4)
					, @SplitVat						decimal(18, 4)
					, @PaidClient						decimal(18, 4)
					, @PaidEnforcementAgency			decimal(18, 4)



                SELECT
                       @PaidStatusId = cs.[Id]
                FROM
                       [dbo].[CaseStatus] cs
                WHERE
                       cs.[name] = 'Paid'

                SELECT
                       @PartPaidStatusId = cs.[Id]
                FROM
                       [dbo].[CaseStatus] cs
                WHERE
                       cs.[name] = 'Part-Paid'

                SELECT
                       @FinalClearedStatusId = [Id]
                FROM
                       [dbo].[PaymentStatus]
                WHERE
                       [name] = 'FINAL CLEARED'

                SELECT
                       @FinalUnclearedStatusId = [Id]
                FROM
                       [dbo].[PaymentStatus]
                WHERE
                       [name] = 'FINAL UNCLEARED'

                SELECT
                       @HistoryId_PaymentCleared = [Id]
                FROM
                       [dbo].[HistoryTypes]
                WHERE
                       [name] = 'PaymentCleared'

                SELECT
                       @HistoryId_CaseStatusChanged = [Id]
                FROM
                       [dbo].[HistoryTypes]
                WHERE
                       [name] = 'CaseStatusChanged'

                DECLARE cursorT
                CURSOR
                    --LOCAL STATIC
                    --LOCAL FAST_FORWARD
                    --LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT
						cCaseId,
						CSchemePaymentId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId,
						PaymentStatus,
						PaymentSource,
						ClearedOn,
						SplitDebt,
						SplitCosts,
						SplitFees,	
						SplitOther,	
						SplitVan,	
						SplitVat	
                    FROM
                        oso.[Staging_OS_Payments]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
                          @CaseId
                        , @SchemePaymentId
                        , @Amount
                        , @AddedOn
                        , @UserId
                        , @OfficerId
						, @PaymentStatus
						, @PaymentSource
						, @ClearedOn
						, @SplitDebt						
						, @SplitCosts					
						, @SplitFees						
						, @SplitOther					
						, @SplitVan						
						, @SplitVat						
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        SELECT
                                   @OriginalCaseStatus   = cs.[name]
                                 , @OriginalCaseStatusId = cs.[Id]
                                 , @ClientCaseReference  = c.clientCaseReference
								 , @OldCaseNumber = oc.OldCaseNumber
                        FROM
                                   [dbo].[CaseStatus] cs
                                   INNER JOIN Cases c on cs.Id = c.StatusId
								   INNER JOIN OldCaseNumbers oc ON oc.CaseId = cs.Id
                        WHERE
                                   c.Id = @CaseId

						SET @PaymentStatusId = @FinalClearedStatusId
						SET @ClientPaymentRunId = 2369;

						IF ((@PaymentStatus = 'W'))
						BEGIN
							SET @ClientPaymentRunId = NULL
						END

						IF ((@ClearedOn IS NULL))
						BEGIN
							SET @PaymentStatusId = @FinalUnclearedStatusId
						END
						

                        -- Add new payment
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]                   --1
                                    , [transactionDate]         --2
                                    , [amount]                  --3
                                    , [clearedOn]               --4
                                    , [bouncedOn]               --5
                                    , [recordedBy]              --6
                                    , [officerId]               --7
                                    , [REFERENCE]               --8
                                    , [authorisationNumber]     --9
                                    , [journalStatusId]         --10
                                    , [receiptTypeId]           --11
                                    , [trusted]                 --12
                                    , [schemePaymentId]         --13
                                    , [paymentStatusId]         --14
                                    , [surchargeAmount]         --15
                                    , [paymentID]               --16
                                    , [reconciledOn]            --17
                                    , [reconcileReference]      --18
                                    , [reversedOn]              --19
                                    , [parentId]                --20
                                    , [previousPaymentStatusId] --21
                                    , [reconciledBy]            --22
                                    , [keepGoodsRemovedStatus]  --23
                                    , [cardStatementReference]  --24
                                    , [bankReconciledOn]        --25
                                    , [bounceReference]         --26
                                    , [reconciledBouncedOn]     --27
                                    , [reconciledBouncedBy]     --28
                                    , [exceptionalAdjustmentId] --29
                                    , [bankReconciledBy]        --30
                                    , [isReversedChargePayment] --31
                                    , [paymentOriginId]         --32
                                    , [uploadedFileReference]   --33
                                    , [ReverseOrBounceNotes]    --34
                               )
                               VALUES
                               ( @AddedOn                                                                                              --1
                                    , @AddedOn                                                                                         --2
                                    , @Amount                                                                                          --3
                                    , @ClearedOn                                                                                         --4
                                    , NULL                                                                                             --5
                                    , @UserId                                                                                          --6
                                    , @OfficerId                                                                                             --7
                                    , RTRIM(LTRIM(@OldCaseNumber + '/' + CAST(@Amount AS varchar) + '/' + CONVERT(VARCHAR(8), @AddedOn, 105))) --8
                                    , NULL                                                                                             --9
                                    , NULL                                                                                             --10
                                    , 1                                                                                                --11
                                    , 1                                                                                                --12
                                    , @SchemePaymentId                                                                                 --13
                                    , @PaymentStatusId                                                                                 --14
                                    , 0.00                                                                                             --15
                                    , 'ODM2Payment'                                                                              --16
                                    , @AddedOn                                                                                             --17
                                    , CONVERT(VARCHAR(10), GETDATE(), 105) + '_ODM2'														--18
                                    , NULL                                                                                             --19
                                    , NULL                                                                                             --20
                                    , NULL                                                                                             --21
                                    , 2                                                                                             --22
                                    , 1                                                                                                --23
                                    , NULL                                                                                             --24
                                    , @AddedOn                                                                                             --25
                                    , NULL                                                                                             --26
                                    , NULL                                                                                             --27
                                    , NULL                                                                                             --28
                                    , NULL                                                                                             --29
                                    , 2                                                                                             --30
                                    , 0                                                                                                --31
                                    , 1                                                                                                --32
                                    , NULL                                                                                             --33
                                    , NULL                                                                                             --34
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
						SET @PaidClient = @SplitDebt + @SplitCosts
						SET @PaidEnforcementAgency = @SplitFees + @SplitOther + @SplitVan + @SplitVat



                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , @ClientPaymentRunId
                                    , @PaidClient
                                    , @PaidEnforcementAgency
                                    , 0
                                    , 0
                               )
                        ;

      --                  SELECT
      --                         @NewCaseStatusId =
      --                                (
      --                                       case
      --                                              when OutstandingBalance >0
      --                                                     then @PartPaidStatusId
      --                                              when OutstandingBalance <=0
      --                                                     then @PaidStatusId
      --                                       end
      --                                )
      --                  FROM
      --                         [dbo].[vw_CaseBalance] v
						--	   JOIN cases c on c.id = v.Id
      --                  WHERE
      --                        c.[Id] = @CaseId
                        
						--IF (@OriginalCaseStatusId <> @NewCaseStatusId)
      --                  BEGIN
      --                      --Status changed, so update the case and insert a history for change of status.
      --                      UPDATE
      --                             dbo.[Cases]
      --                      SET    
						--		[StatusId] = @NewCaseStatusId

      --                      WHERE
      --                             [Id] = @CaseId
						--		   and statusid not in ()
      --                      ;
            
      --                  END;
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
                          @CaseId
                        , @SchemePaymentId
                        , @Amount
                        , @AddedOn
                        , @UserId
                        , @OfficerId
						, @PaymentStatus
						, @PaymentSource
						, @ClearedOn
						, @SplitDebt						
						, @SplitCosts					
						, @SplitFees						
						, @SplitOther					
						, @SplitVan						
						, @SplitVat	
					END
                    CLOSE cursorT
                    DEALLOCATE cursorT 
					
					UPDATE [oso].[Staging_OS_Payments]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0

					COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetOfficersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId] AS OfficerId
	,[ColId] AS COfficerId
  FROM [oso].[OSColOfficersXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetSchemePaymentXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep payment type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetSchemePaymentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemePaymentName]
      ,[OSSchemePaymentName] AS PaymentType
	  ,[CSchemePaymentId]
  FROM [oso].[OneStep_SchemePayment_CrossRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Payments_GetUsersXMapping]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Payments_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId] AS UserId,
			[ColId] AS CUserId
		FROM [oso].[OSColUsersXRef]
    END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCode_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ReturnCode_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_ReturnCode_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCodes_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ReturnCodes_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_ReturnCode_CrossRef] 
				(
					CReturnCode
					,OSReturnCode
				)
				SELECT
					CReturnCode
					,OSReturnCode
				FROM
					[oso].[stg_OneStep_ReturnCode_CrossRef]	 
			COMMIT TRANSACTION
			
			BEGIN TRANSACTION
				UPDATE rcc 
				SET rcc.CId = drc.id
				FROM
					oso.[OneStep_ReturnCode_CrossRef] rcc
				INNER JOIN DrakeReturnCodes drc on drc.code = rcc.CReturnCode
			COMMIT TRANSACTION

			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemeCharge_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemeCharge_CrossRef_DT] AS
BEGIN	
	INSERT [oso].[OneStep_SchemeCharge_CrossRef]	
	(
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId     
	)
	SELECT
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId   
	FROM
		[oso].[stg_OneStep_SchemeCharge_CrossRef]		

		 update scx 
		  set scx.cschemechargeid = new.id 
		  --select scx.*,new.id [newid]
		  from 
		   oso.OneStep_SchemeCharge_Crossref scx
		  join schemecharges new on new.name = scx.cschemechargename AND 
		  new.chargingschemeid = (select id from chargingscheme where schemename = 'Rossendales Standard TCE Charging Scheme')

	SELECT
        1 as Succeed
	
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemeCharge_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================

CREATE PROCEDURE [oso].[usp_OS_SchemeCharge_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_SchemeCharge_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemePayment_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_SchemePayment_CrossRef] 
				(
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				)
				SELECT
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				FROM
					[oso].[stg_OneStep_SchemePayment_CrossRef]	 

			  update spx 
			  set spx.cschemepaymentid = new.id 
			  --select spx.*,new.id [newid]
			  from 
			   oso.OneStep_SchemePayment_Crossref spx
			  join schemepayments new on new.name = spx.cschemepaymentname AND 
			  new.paymentschemeid = (select id from paymentscheme where name = 'Rossendales Generic Payment Scheme')


			COMMIT TRANSACTION
			SELECT
				1 as Succeed
			
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description: To clear [oso].[stg_OneStep_SchemePayment_CrossRef] staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_SchemePayment_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_SchemePayment_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Stages_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Stages_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[stg_OneStep_Stages_CrossRef]
			(
				[PhaseTemplateId]
				,[PhaseTemplateName]
				,[StageTemplateId]
				,[StageTemplateName]
			)
			SELECT
				pt.Id PhaseTempId	
				,pt.Name PhaseTemplate
				,st.Id StageTemplateId
				,st.Name Stage
			FROM 
				WorkflowTemplates wt
				JOIN PhaseTemplates pt on wt.Id = pt.WorkflowTemplateId
				JOIN StageTemplates st on pt.Id = st.PhaseTemplateId
			
			WHERE 	
				wt.Name = 'Rossendales 14 days 1 Letter'
			ORDER BY
				PhaseTemplate
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END 
END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 09-07-2020
-- Description: To clear Tagged Notes staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
					---- Insert case notes
					--INSERT INTO dbo.[CaseNotes] 
					--( [caseId]
					--	, [userId]
					--	, [officerId]
					--	, [text]
					--	, [occured]
					--	, [visible]
					--	, [groupId]
					--	, [TypeId]
					--)
					--SELECT
					--	[CCaseId],
					--	[CUserId],
					--	NULL,
					--	[CaseNote],
					--	[DateAdded],
					--	1,
					--	NULL,
					--	NULL
					--FROM
					--	[oso].[stg_Onestep_PhsTwo_TaggedNotes] 
					--WHERE 
					--	Imported = 0 AND CaseNote IS NOT NULL AND CaseNote <>''

					--UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					--SET Imported = 1, ImportedOn = GetDate()
					--FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					--WHERE otn.Imported = 0 AND otn.CaseNote IS NOT NULL AND otn.CaseNote <>''

					-- Update existing tagged note
					UPDATE [dbo].AdditionalCaseData 
					SET 
						[Value] = otn.TagValue,
						[LoadedOn] = otn.DateAdded
					FROM [dbo].AdditionalCaseData ac
					INNER JOIN [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL
				
					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					INNER JOIN [dbo].AdditionalCaseData ac ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL

					-- Insert new tagged notes
					INSERT INTO [dbo].AdditionalCaseData (CaseId,[Name],[Value],LoadedOn)
					SELECT DISTINCT cCaseId,TagName,MAX(TagValue),MAX(DateAdded)
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL
					GROUP BY cCaseId, TagName

					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Users_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Users staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_PhsTwo_Users]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Users from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@firstName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@Department [varchar](200),
					@JobTitle [varchar](250),
					@marston [bit],
					@swift [bit],
					@rossendales [bit],
					@marstonLAA [bit],
					@rossendaleDCA [bit],
					@SwiftDCA [bit],
					@MarstonDCA [bit],
					@DCA [bit],
					@EXP [bit],
					@LAA [bit],
					@onestepid [int],
					@username [nvarchar] (50),
					@Existing [bit],
					
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BMarstonLAAId int, 
					@BRossendaleDCAId int,
					@BSwiftDCAId int,
					@BMarstonDCAId int,
					@DEPTID int,
					@OFFICEID int,
					@RoleId int,
					@Comment [varchar] (250),
					@UBCount int,
					@ColUserName [varchar] (50),
					@ColUserCount int
					
					
					
				SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
				SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
				SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
				SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
				SELECT @BRossendaleDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
				SELECT @BSwiftDCAId = Id From [Brands].[Brands] WHERE Name = 'Swift DCA'
				SELECT @BMarstonDCAId = Id From [Brands].[Brands] WHERE Name = 'Marston DCA'
				
				---- DEPARTMENT
				--INSERT INTO Departments ([name])
				--VALUES ('Rossendales')
				--SET @DEPTID = SCOPE_IDENTITY();
				
				-- OFFICE
				SELECT @OFFICEID = id FROM [dbo].[Offices] WHERE [Name] = 'Helmshore'
				--IF (@OFFICEID IS NULL)
				--BEGIN
				--	INSERT INTO Offices ([Name], [Firstline], [Postcode], [Address])
				--	VALUES ('Helmshore',NULL,NULL,NULL)
				--	SET @OFFICEID = SCOPE_IDENTITY();
				--END

				IF CURSOR_STATUS('global','cursorT')>=-1
				BEGIN
					DEALLOCATE cursorT
				END

                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
					[firstName],
					[lastName],
					[email],
					[Department],
					[JobTitle],
					[marston],
					[swift],
					[rossendales],
					[marstonLAA],
					[rossendaleDCA],
					[SwiftDCA],
					[MarstonDCA],
					[DCA],
					[EXP],
					[LAA],
					[onestepid],
					[username],
					[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Users]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
                    WHILE @@FETCH_STATUS = 0
                    BEGIN

							SELECT @DEPTID = Id FROM [dbo].[Departments] WHERE [name] = @Department
							SET @ColId = NULL
							IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColUsersXRef] WHERE OSId = @onestepId

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' +  @username;
									THROW 51000, @Comment, 163; 
								
								END								

							END
							ELSE
							BEGIN
									--Add new user here
									SET @ColUserName = @username
									SELECT @ColUserCount = COUNT(*) FROM dbo.Users WHERE name = @username
									IF (@ColUserCount > 0)
									BEGIN
										SET @ColUserName += '_os';  
									END

									INSERT INTO USERS (
									[name]
									, [password]
									, [firstName]
									, [lastName]
									, [email]
									, [departmentId]
									, [enabled]
									, [officeId]
									, [changePasswordOnLogin]
									, [passwordReset]
									, [defaultResultsExpected]
									, [sendEmailNotification]
									, [sendEmailNotificationEnabledOn]
									, [emailNotificationInterval])
									VALUES (
									@ColUserName
									,'1pF5kXsqfQdLufiK43b1Qw=='
									,@firstName
									,@lastName
									,@email
									,@DEPTID
									, 1
									, @OFFICEID
									, 1
									, 0
									, 1000
									, 0
									, NULL
									, 1)
									SET @ColId = SCOPE_IDENTITY();

							
						
									-- Add to XRef table
									INSERT INTO oso.[OSColUsersXRef]
										(
											[username],
											[OSId],
											[ColId]
										)

										VALUES
										(
											@username
											,@onestepid
											,@ColId
										)										
								
									-- Add OfficersBrands
								IF (@marston = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BMarstonId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BMarstonId
											)								
										END

									END
			
								IF (@rossendales = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BRossendalesId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BRossendalesId
											)							
										END

									END			
						
								IF (@swift = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BSwiftId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BSwiftId
											)					
										END

									END											
							END

							IF (@marstonLAA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonLAAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonLAAId
									)					
								END

							END	
								
							IF (@rossendaleDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BRossendaleDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendaleDCAId
									)					
								END

							END				

							IF (@SwiftDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BSwiftDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftDCAId
									)					
								END

							END				

							IF (@MarstonDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonDCAId
									)					
								END

							END				

							-- Add User ROLE
							IF (@JobTitle IS NOT NULL)
							BEGIN
								SELECT @RoleId = Id FROM Roles WHERE name = @JobTitle
								
								IF (@RoleId is null)
									BEGIN

										SET @Comment = 'RoleId not found for ' +  @JobTitle;
										THROW 51000, @Comment, 163;  
									END	
								DECLARE @UserRoleId int
								SELECT @UserRoleId = UserId From UserRoles WHERE UserId = @ColId AND RoleId = @RoleId

								IF (@UserRoleId IS NULL)
								BEGIN
									INSERT INTO dbo.[UserRoles]
										(
											[userId]
											,[roleId]
										)

										VALUES
										(
											@ColId
											,@RoleId									
										)		
								END
																	
							END	
					-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Users]
			      SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0		

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Vehicles_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Vehicles_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_Vehicles]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END	
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Vehicles_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Vehicles_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			DECLARE
				@VRM VARCHAR(7),
				@vrmOwner BIT,
				@DefaulterId INT,
				@OfficerId INT,
				@DateLoaded DATETIME,
				@CaseId INT,
				@VehicleId INT

			DECLARE cursorT 
				CURSOR FOR
					SELECT
						[VRM], 
						[VRMOwner], 
						[cDefaulterId], 
						[cOfficerId], 
						[DateLoaded],						 
						[cCaseId]
					FROM
						[oso].[stg_OneStep_Vehicles]
					WHERE 
						Imported = 0
				OPEN cursorT
				FETCH NEXT
					FROM
						cursorT
					INTO
						@VRM,
						@vrmOwner,
						@DefaulterId,
						@OfficerId,
						@DateLoaded,
						@CaseId
				
				WHILE
					@@FETCH_STATUS = 0
					BEGIN
						INSERT [dbo].[Vehicles] 
						(
							[vrm],
							[vrmOwner],
							[defaulterId],
							[officerId],
							[VrmReceivedOn]
						)
						VALUES
						(
							@VRM,
							@vrmOwner,
							@DefaulterId,
							@OfficerId,
							@DateLoaded
						)
						SET @VehicleId = SCOPE_IDENTITY();

						INSERT [dbo].[CaseVehicles] 
						(
							[caseId], 
							[vehicleId]
						)
						VALUES
						(
							@CaseId,
							@VehicleId
						)

						FETCH NEXT
							FROM
								cursorT
							INTO
								@VRM,
								@vrmOwner,
								@DefaulterId,
								@OfficerId,
								@DateLoaded,
								@CaseId
					END
				CLOSE cursorT
				DEALLOCATE cursorT			
			
			UPDATE [oso].[stg_OneStep_Vehicles]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
			END TRY	
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END

END
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Visits_CrossRef_DC_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_Visits]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
/****** Object:  StoredProcedure [oso].[usp_OS_Visits_CrossRef_DT]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			INSERT [dbo].[Visits] 
			(
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			)
			SELECT 
				[import].[CCaseId],
				[import].[COfficerId], 
				[import].[VisitDate],
				[import].[DoorColour],
				[import].[BuildingType],
				[import].[PersonContacted],	
				[import].[ProofOfIdentity],
				[import].[Notes],
				0,
				53.68456,
				-2.2769,
				0.00,
				[import].[VisitDate],
				[import].[VisitDate]
			FROM 		
				[oso].[stg_OneStep_Visits] [import]	
			WHERE 
				Imported = 0 OR Imported IS NULL
			EXCEPT
			SELECT 
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			FROM 
				[dbo].[Visits]

			UPDATE [oso].[stg_OneStep_Visits]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0 OR Imported IS NULL


			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO
/****** Object:  StoredProcedure [oso].[usp_PopulateValidPostcodeOrDefault]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_PopulateValidPostcodeOrDefault] @DebtAddressPostcode VARCHAR(50),@AddPostCodeLiable1 VARCHAR(50),@AddPostCodeLiable2 VARCHAR(50)
,@AddPostCodeLiable3 VARCHAR(50),@AddPostCodeLiable4 VARCHAR(50),@AddPostCodeLiable5 VARCHAR(50)

AS

BEGIN
	DECLARE @DefaultPostCode VARCHAR(50) = 'G1 1UG'

	DECLARE @normalizedDebtPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
	DECLARE @normalizedDef1Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
	DECLARE @normalizedDef2Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable2))
	DECLARE @normalizedDef3Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable3))
	DECLARE @normalizedDef4Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable4))
	DECLARE @normalizedDef5Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable5))

	DECLARE @isValidDebtPostcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDebtPostcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef1Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef1Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef2Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef2Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef3Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef3Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef4Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef4Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef5Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef5Postcode, ' '),'AnyCompleteness','AnyType' )


	SELECT
    Case When @isValidDebtPostcode = 0 THEN CASE WHEN (@normalizedDebtPostcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDebtPostcode END AS DebtAddressPostcode,
    Case When @isValidDef1Postcode = 0 THEN CASE WHEN (@normalizedDef1Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef1Postcode END AS AddPostCodeLiable1,
    Case When @isValidDef2Postcode = 0 THEN CASE WHEN (@normalizedDef2Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef2Postcode END AS AddPostCodeLiable2,
    Case When @isValidDef3Postcode = 0 THEN CASE WHEN (@normalizedDef3Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef3Postcode END AS AddPostCodeLiable3,
    Case When @isValidDef4Postcode = 0 THEN CASE WHEN (@normalizedDef4Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef4Postcode END AS AddPostCodeLiable4,
    Case When @isValidDef5Postcode = 0 THEN CASE WHEN (@normalizedDef5Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef5Postcode END AS AddPostCodeLiable5

END
GO
/****** Object:  StoredProcedure [oso].[usp_SplitString]    Script Date: 21/04/2021 14:52:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  VJ
-- Create date: 20-07-2020
-- Description: Split the string to a size and insert the results to CaseNotes table.
-- =============================================

CREATE PROCEDURE  [oso].[usp_SplitString]
(
     @Str NVARCHAR(Max),
	 @Split INT,
	 @RemoveFirstRow bit = 1
)

AS

BEGIN 
DECLARE @End INT
DECLARE @rowNo INT = 1

declare @RowSplitTable table
(
  Id INT IDENTITY(1,1)   NOT NULL, 
  Content Nvarchar(max)
)

declare @FinalRowTable table
(
  Id INT  NOT NULL, 
  Content Nvarchar(max)
)

WHILE (LEN(@Str) > 0)
BEGIN
    IF (LEN(@Str) > @Split)
    BEGIN
        SET @End = LEN(LEFT(@Str, @Split)) - CHARINDEX(' ', REVERSE(LEFT(@Str, @Split)))
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(LEFT(LEFT(@Str, @Split), @End + 1))))
        SET @Str = SUBSTRING(@Str, @End + 2, LEN(@Str))
    END
    ELSE
    BEGIN
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(@Str)))
        SET @Str = ''
    END
END


IF @RemoveFirstRow = 1

BEGIN

INSERT INTO @FinalRowTable
SELECT id, Content FROM (
        SELECT ROW_NUMBER() OVER(ORDER BY id) AS RoNum
              , id , Content
        FROM @RowSplitTable 
) AS tbl 
WHERE @rowNo < RoNum
ORDER BY tbl.Id 

END 
ELSE 
BEGIN 

INSERT INTO @FinalRowTable SELECT *  FROM @RowSplitTable

END


SELECT s.Content,s.Id
FROM @FinalRowTable s

END
GO

--GRANT SELECT ON SCHEMA::de TO FUTUser
--GRANT SELECT,INSERT,DELETE,UPDATE,EXECUTE ON SCHEMA::oso TO FUTUser
--GRANT SELECT,INSERT ON SCHEMA::Brands TO FUTUser