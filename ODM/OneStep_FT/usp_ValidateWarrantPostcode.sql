/****** Object:  StoredProcedure [oso].[usp_ValidateWarrantPostcode]    Script Date: 04/05/2020 10:12:06 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_ValidateWarrantPostcode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_ValidateWarrantPostcode] @DebtAddressPostcode [nvarchar](50)
AS
BEGIN
	DECLARE @normalizedPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
	DECLARE @result BIT = dbo.udf_ValidatePostcode( @normalizedPostcode,'AnyCompleteness','AnyType' ) --as IsValidPostcode
	IF(@result = 1)
		Select 1
		Else
		Select 0
END

