/****** Object:  StoredProcedure [oso].[usp_ValidateDefaulterPostcode]    Script Date: 04/05/2020 10:12:06 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_ValidateDefaulterPostcode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [oso].[usp_ValidateDefaulterPostcode] @AddPostCodeLiable1 [nvarchar](50)
AS
BEGIN
	DECLARE @normalizedPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
	DECLARE @result BIT = dbo.udf_ValidatePostcode( @normalizedPostcode,'AnyCompleteness','AnyType' )-- as IsValidPostcode
	IF(@result = 1)
		Select 1
		Else
		Select 0
END