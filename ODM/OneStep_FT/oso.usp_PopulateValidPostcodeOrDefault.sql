SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [oso].[usp_PopulateValidPostcodeOrDefault] @DebtAddressPostcode VARCHAR(50),@AddPostCodeLiable1 VARCHAR(50),@AddPostCodeLiable2 VARCHAR(50)
,@AddPostCodeLiable3 VARCHAR(50),@AddPostCodeLiable4 VARCHAR(50),@AddPostCodeLiable5 VARCHAR(50)

AS

BEGIN

       DECLARE @DefaultPostCode VARCHAR(50) = 'BB4 4NB' 

	   --Normalie postcode
       DECLARE @normalizedDebtPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
       DECLARE @normalizedDef1Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
       DECLARE @normalizedDef2Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable2))
       DECLARE @normalizedDef3Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable3))
       DECLARE @normalizedDef4Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable4))
       DECLARE @normalizedDef5Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable5)) 
	   
	   --Validate postcode and if normalized postcode is null, replace that with a space, so that valid postcode returns 0 for that. 
       DECLARE @isValidDebtPostcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDebtPostcode, ' '),'AnyCompleteness','AnyType' )
       DECLARE @isValidDef1Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef1Postcode, ' '),'AnyCompleteness','AnyType' )
       DECLARE @isValidDef2Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef2Postcode, ' '),'AnyCompleteness','AnyType' )
       DECLARE @isValidDef3Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef3Postcode, ' '),'AnyCompleteness','AnyType' )
       DECLARE @isValidDef4Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef4Postcode, ' '),'AnyCompleteness','AnyType' )
       DECLARE @isValidDef5Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef5Postcode, ' '),'AnyCompleteness','AnyType' ) 
	   
	   --Select postcode if valid one otherwise select default postcode
       SELECT
	   Case When @isValidDebtPostcode = 0 THEN CASE WHEN (@normalizedDebtPostcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDebtPostcode END AS DebtAddressPostcode,
	   Case When @isValidDef1Postcode = 0 THEN CASE WHEN (@normalizedDef1Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef1Postcode END AS AddPostCodeLiable1,
	   Case When @isValidDef2Postcode = 0 THEN CASE WHEN (@normalizedDef2Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef2Postcode END AS AddPostCodeLiable2,
	   Case When @isValidDef3Postcode = 0 THEN CASE WHEN (@normalizedDef3Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef3Postcode END AS AddPostCodeLiable3,
	   Case When @isValidDef4Postcode = 0 THEN CASE WHEN (@normalizedDef4Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef4Postcode END AS AddPostCodeLiable4,
	   Case When @isValidDef5Postcode = 0 THEN CASE WHEN (@normalizedDef5Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef5Postcode END AS AddPostCodeLiable5
	   
END