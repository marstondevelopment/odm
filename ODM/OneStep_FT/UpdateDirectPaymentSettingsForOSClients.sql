﻿/****** Object:  StoredProcedure [oso].[usp_OS_Config_Direct Payment_Settings]    Script Date: 12/02/2021 11:30:47 ******/
DROP PROCEDURE [oso].[usp_OS_Config_Direct Payment_Settings]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Config_Direct Payment_Settings]    Script Date: 12/02/2021 11:30:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Update direct payment settings for OS clients
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Config_Direct Payment_Settings] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE 
			@ClientCaseTypeId int ,
			@ClientName nvarchar(80),
			@CCTCId_MarstonRemittableCharges int ,
			@CCTCId_ConsiderMarstonChargesCIR int ,
			@CCTCId_IsReverseAllowedForDirectPayment int ,
			@CCTCId_PaymentDistrubutionIDDP int ,
			@CCTCMId int ,
			@succeed bit,
			@ErrorId int

		SET @succeed = 1

        SELECT @CCTCId_MarstonRemittableCharges = Id 
        FROM dbo.ClientCaseTypeConfiguration
        WHERE Name = 'MarstonRemittableCharges'

        SELECT @CCTCId_ConsiderMarstonChargesCIR = Id 
        FROM dbo.ClientCaseTypeConfiguration
        WHERE Name = 'ConsiderMarstonChargesCIR'

        SELECT @CCTCId_IsReverseAllowedForDirectPayment = Id 
        FROM dbo.ClientCaseTypeConfiguration
        WHERE Name = 'IsReverseAllowedForDirectPayment'

        SELECT @CCTCId_PaymentDistrubutionIDDP = Id 
        FROM dbo.ClientCaseTypeConfiguration
        WHERE Name = 'PaymentDistrubutionIDDP'


		BEGIN TRANSACTION

		--Brand 7,8,9
		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY 
		FOR
			SELECT distinct cct.Id, cl.name 	  
			FROM dbo.ClientCaseType cct 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
			WHERE
				cct.BrandId in (7,8,9 )
				and cl.IsActive = 1
				--and cl.name NOT in 
				--			(
				--			'Neath Port Talbot CBC CT',
				--			'Neath Port Talbot CBC CT Pre - TCE'
				--			)
        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientCaseTypeId,
			@ClientName

        WHILE @@FETCH_STATUS = 0
        BEGIN

					-- MarstonRemittableCharges
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_MarstonRemittableCharges
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_MarstonRemittableCharges,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- ConsiderMarstonChargesCIR
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_ConsiderMarstonChargesCIR
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_ConsiderMarstonChargesCIR,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- IsReverseAllowedForDirectPayment
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_IsReverseAllowedForDirectPayment
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_IsReverseAllowedForDirectPayment,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- PaymentDistrubutionIDDP
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_PaymentDistrubutionIDDP
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						IF (@ClientName = 'Neath Port Talbot CBC CT' OR @ClientName = 'Neath Port Talbot CBC CT Pre - TCE')
						BEGIN
							UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = '6' WHERE ID = @CCTCMId
						END
						ELSE
						BEGIN 
							UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = '5' WHERE ID = @CCTCMId
						END
					END
					ELSE
					BEGIN
						IF (@ClientName = 'Neath Port Talbot CBC CT' OR @ClientName = 'Neath Port Talbot CBC CT Pre - TCE')
						BEGIN
							INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
							(
								@CCTCId_PaymentDistrubutionIDDP,
								@ClientCaseTypeId,
								'6',
								1
							)
						END
						ELSE
						BEGIN 
							INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
							(
								@CCTCId_PaymentDistrubutionIDDP,
								@ClientCaseTypeId,
								'5',
								1
							)
						END
					END
					-- End 

					UPDATE ClientCaseType 
					SET IsChargeConsidered = 0,
						IsDirectPaymentValidForAutomaticallyLinkedCases = 0
					WHERE Id = @ClientCaseTypeId
            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				@ClientCaseTypeId,
				@ClientName
        END
        CLOSE cursorT
        DEALLOCATE cursorT

		--Brand 10,12,13
		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY 
		FOR
			SELECT distinct cct.Id 	  
			FROM dbo.ClientCaseType cct 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
			WHERE
				cct.BrandId in (10,12,13 )
				and cl.IsActive = 1

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientCaseTypeId

        WHILE @@FETCH_STATUS = 0
        BEGIN

					-- MarstonRemittableCharges
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_MarstonRemittableCharges
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_MarstonRemittableCharges,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- ConsiderMarstonChargesCIR
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_ConsiderMarstonChargesCIR
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_ConsiderMarstonChargesCIR,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- IsReverseAllowedForDirectPayment
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_IsReverseAllowedForDirectPayment
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = 'True' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_IsReverseAllowedForDirectPayment,
							@ClientCaseTypeId,
							'True',
							1
						)
					END
					-- End 

					-- PaymentDistrubutionIDDP
					SET @CCTCMId = NULL
					SELECT 
						@CCTCMId = Id From [dbo].[ClientCaseTypeConfigurationMapping] 
					WHERE 
						ClientCaseTypeConfigurationID = @CCTCId_PaymentDistrubutionIDDP
						AND ClientCaseTypeID = @ClientCaseTypeId
					IF (@CCTCMId IS NOT NULL)
					BEGIN
						UPDATE [dbo].[ClientCaseTypeConfigurationMapping] SET [Value] = '5' WHERE ID = @CCTCMId
					END
					ELSE
					BEGIN
						INSERT INTO [dbo].[ClientCaseTypeConfigurationMapping] VALUES
						(
							@CCTCId_PaymentDistrubutionIDDP,
							@ClientCaseTypeId,
							'5',
							1
						)
					END
					-- End 

					UPDATE ClientCaseType 
					SET IsChargeConsidered = 1,
						IsDirectPaymentValidForAutomaticallyLinkedCases = 0
					WHERE Id = @ClientCaseTypeId

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				@ClientCaseTypeId
        END
        CLOSE cursorT
        DEALLOCATE cursorT
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO


			SELECT distinct cl.[name], cct.Id, cct.IsChargeConsidered,cct.IsDirectPaymentValidForAutomaticallyLinkedCases, cct.BrandId
			FROM dbo.ClientCaseType cct 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
			WHERE
				cct.BrandId in (7,8,9)
--				cct.BrandId in (10,12,13)
				and cl.IsActive = 1

			SELECT distinct m.*, cctc.Name
			FROM dbo.ClientCaseType cct 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
				inner join dbo.ClientCaseTypeConfigurationMapping m on cct.Id = m.ClientCaseTypeID
				inner join dbo.ClientCaseTypeConfiguration cctc on m.ClientCaseTypeConfigurationID = cctc.ID
			WHERE
				cct.BrandId in (7,8,9)
--				cct.BrandId in (10,12,13)
				and cl.IsActive = 1

--exec [oso].[usp_OS_Config_Direct Payment_Settings]