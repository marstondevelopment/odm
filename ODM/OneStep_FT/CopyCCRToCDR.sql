﻿-- Update the CDR for uploaded cases
BEGIN TRANSACTION

Select c.Id,c.clientCaseReference,  
CASE CHARINDEX('_', c.clientCaseReference)
                         WHEN 0 THEN c.clientCaseReference
                         WHEN 1 THEN ''
                         ELSE LEFT(c.clientCaseReference, CHARINDEX('_', c.clientCaseReference)-1)
           END
AS OriginalCCR,
cs.name as [Status],
c.ClientDefaulterReference as OriginalCDR
INTO #CCROriginalVals 
FROM cases c inner join dbo.[Batches] b on c.batchid = b.id 
inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id    
inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id 
inner join dbo.Clients cl on cct.clientId = cl.Id 
inner join dbo.CaseStatus cs on c.statusId = cs.Id

WHERE cct.BrandId in (7,8,9)
AND (c.ClientDefaulterReference IS NULL OR  c.ClientDefaulterReference = '')
Order By c.createdOn desc

UPDATE #CCROriginalVals SET OriginalCCR = 
CASE CHARINDEX('-', OriginalCCR)
                         WHEN 0 THEN OriginalCCR
                         WHEN 1 THEN ''
                         ELSE LEFT(OriginalCCR, CHARINDEX('-', OriginalCCR)-1)
           END

Select Id,clientCaseReference, [Status], OriginalCDR, OriginalCCR AS NewCDR  
, LEN(clientCaseReference) AS [LENGTH]
FROM #CCROriginalVals 

UPDATE c SET c.ClientDefaulterReference = ccr.OriginalCCR
FROM cases c inner join #CCROriginalVals ccr ON c.Id = ccr.Id

COMMIT TRANSACTION

--ROLLBACK TRANSACTION 

Drop Table #CCROriginalVals


-- Add SP for SQL job
/****** Object:  StoredProcedure [oso].[[usp_OS_CopyCCRToCDR]]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_CopyCCRToCDR]
GO

/****** Object:  StoredProcedure [oso].[[usp_OS_CopyCCRToCDR]]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To copy CCR to CDR if CDR is null or empty
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_CopyCCRToCDR]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
				BEGIN TRANSACTION
				
				UPDATE c SET c.ClientDefaulterReference = CASE CHARINDEX('_', c.clientCaseReference)
										 WHEN 0 THEN c.clientCaseReference
										 WHEN 1 THEN ''
										 ELSE LEFT(c.clientCaseReference, CHARINDEX('_', c.clientCaseReference)-1)
						   END
				FROM cases c inner join dbo.[Batches] b on c.batchid = b.id 
				inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id    
				inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id 
				inner join dbo.Clients cl on cct.clientId = cl.Id 
				inner join dbo.CaseStatus cs on c.statusId = cs.Id

				WHERE cct.BrandId in (7,8,9)
				AND (c.ClientDefaulterReference IS NULL OR  c.ClientDefaulterReference = '')
				AND b.batchDate >= CAST( GETDATE() AS Date )

				COMMIT TRANSACTION				
				SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
