USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_NewCase_Notes]    Script Date: 06/08/2020 09:39:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_NewCase_Notes](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_NewCase_Notes] ADD  DEFAULT ((0)) FOR [Imported]
GO

USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]    Script Date: 06/08/2020 09:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [oso].[usp_OS_CaseNotes_Staging_DT_BySplit]
AS

BEGIN

IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL
    DROP TABLE #CaseNotes

CREATE TABLE #CaseNotes
(
    [Id] INT IDENTITY(1,1)  NOT NULL, 
    [ClientCaseReference] [varchar](200) NOT NULL,
	[ClientId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
)

INSERT into #CaseNotes([ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])
SELECT [ClientCaseReference],[ClientId],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]
					 FROM oso.Staging_OS_NewCase_Notes WHERE Imported = 0 AND ErrorId IS NULL

		DECLARE @CaseNoteID INT;		
		DECLARE @CaseId INT;
		DECLARE @ClientCaseReference varchar(200);
		DECLARE @ClientId INT;
		DECLARE @StringVal NVARCHAR(MAX);
 
		DECLARE CaseNotes CURSOR FAST_FORWARD FOR
		SELECT Id, ClientCaseReference , ClientId ,Text
		FROM   #CaseNotes		
 
		OPEN CaseNotes
		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal
 
		WHILE @@FETCH_STATUS = 0
		BEGIN
  BEGIN TRY
  BEGIN TRANSACTION
		SET @CaseId = 0
		SELECT @CaseId = c.Id   FROM  Cases c 
		INNER JOIN  Batches b  ON c.batchid = b.id
        INNER JOIN  ClientCaseType CT ON  b.clientCaseTypeId = CT.Id
        WHERE CT.clientId = @ClientId 
		AND C.clientCaseReference = @ClientCaseReference
		
		IF(@CaseId IS NOT NULL AND @CaseId <>0)
		BEGIN 
			DECLARE @TempNotes TABLE (
					Content NVARCHAR(2000), 
					Id int
				)

			INSERT INTO @TempNotes
			EXEC [oso].[usp_SplitString] @StringVal, 1950;

			--Remove spaces.
			Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content) 

			INSERT INTO dbo.[CaseNotes] 
					( [caseId]
						, [userId]
						, [officerId]
						, [text]
						, [occured]
						, [visible]
						, [groupId]
						, [TypeId]
					)
					SELECT
						@CaseId,
						2, -- System admin
						NULL,					
						S.Content,
						GETDATE(),
						1,
						NULL,
						NULL
					FROM
						@TempNotes S

			UPDATE [oso].[Staging_OS_NewCase_Notes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0
			
			DELETE FROM @TempNotes
		END
  COMMIT
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )

					DECLARE @ErrorId INT = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_NewCase_Notes]
					SET    ErrorId = @ErrorId
					WHERE ClientId = @ClientId and ClientCaseReference = @ClientCaseReference and Imported = 0

				SELECT 0 as Succeed
  END CATCH	
  

		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @ClientCaseReference , @ClientId , @StringVal		
		END
		CLOSE CaseNotes
		DEALLOCATE CaseNotes	
END 

------------------------------------------------------------------------------------------------------------------


/****** Object:  Table [oso].[Staging_OS_NewCase_Notes_CaseNumber]    Script Date: 05/08/2020 15:22:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber](
	[CaseNumber] [varchar](10) NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber] ADD  DEFAULT ((0)) FOR [Imported]
GO



/****** Object:  StoredProcedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]    Script Date: 05/08/2020 15:22:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [oso].[usp_OS_CaseNotes_CaseNumber_Staging_DT_BySplit]
AS

BEGIN

IF OBJECT_ID('tempdb..#CaseNotes') IS NOT NULL
    DROP TABLE #CaseNotes

CREATE TABLE #CaseNotes
(
    [Id] INT IDENTITY(1,1)  NOT NULL, 
    [CaseNumber] [varchar](10) NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
)

INSERT into #CaseNotes([CaseNumber],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn])
SELECT [CaseNumber],[CUserId],[COfficerId],[Text],[Occurred],[Imported],[ImportedOn]
					 FROM oso.Staging_OS_NewCase_Notes_CaseNumber WHERE Imported = 0 AND ErrorId IS NULL
					 Select * from #CaseNotes
		DECLARE @CaseNoteID INT;		
		DECLARE @CaseId INT;
		DECLARE @CaseNumber varchar(10);
		DECLARE @StringVal NVARCHAR(MAX);
 
		DECLARE CaseNotes CURSOR FAST_FORWARD FOR
		SELECT Id, CaseNumber ,Text
		FROM   #CaseNotes		
 
		OPEN CaseNotes
		FETCH NEXT FROM CaseNotes INTO @CaseNoteID, @CaseNumber , @StringVal
 
		WHILE @@FETCH_STATUS = 0
		BEGIN
  BEGIN TRY
  BEGIN TRANSACTION
		SELECT @CaseId = c.Id   FROM  Cases c 
		WHERE C.caseNumber = @CaseNumber
		Select @CaseId as caseiD
		IF(@CaseId IS NOT NULL)
		BEGIN 
			--DECLARE @TempNotes TABLE (
			--		Content NVARCHAR(2000), 
			--		Id int
			--	)

			--INSERT INTO @TempNotes
			--EXEC [oso].[usp_SplitString] @StringVal, 2000;

			----Remove spaces.
			--Update @TempNotes set Content = oso.fn_RemoveMultipleSpaces(Content) 
			SELECT
						@CaseId,
						2, -- System admin
						NULL,					
						@StringVal,
						GETDATE(),
						1,
						NULL,
						NULL

			INSERT INTO dbo.[CaseNotes] 
					( [caseId]
						, [userId]
						, [officerId]
						, [text]
						, [occured]
						, [visible]
						, [groupId]
						, [TypeId]
					)
					SELECT
						@CaseId,
						2, -- System admin
						NULL,					
						@StringVal,
						GETDATE(),
						1,
						NULL,
						NULL
					

			UPDATE [oso].[Staging_OS_NewCase_Notes_CaseNumber]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE CaseNumber = @CaseNumber and Imported = 0

		END
  COMMIT
  END TRY
  BEGIN CATCH
  ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )

					DECLARE @ErrorId INT = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_NewCase_Notes_CaseNumber]
					SET    ErrorId = @ErrorId
					WHERE CaseNumber = @CaseNumber and Imported = 0

				SELECT 0 as Succeed
  END CATCH	
  

		FETCH NEXT FROM CaseNotes INTO  @CaseNoteID, @CaseNumber , @StringVal		
		END
		CLOSE CaseNotes
		DEALLOCATE CaseNotes	
END 