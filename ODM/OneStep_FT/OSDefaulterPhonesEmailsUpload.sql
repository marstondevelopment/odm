
DROP TABLE IF EXISTS [oso].[Staging_OS_BU_DefaulterPhones]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_BU_DefaulterPhones](	
	[TypeID] [int] NOT NULL,
	[TelephoneNumber] [varchar](50) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar] (7) NULL,
	[cDefaulterId] [int] NOT NULL,
	ClientId int NULL, 
	FUTUserId int NOT NULL,
	[UploadedOn] [datetime] DEFAULT GETDATE(),
	[Imported] [bit] DEFAULT 0,
	[ImportedOn] [datetime] NULL,
	[ErrorId] Int NULL
) ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS__BU_DefaulterPhones_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS__BU_DefaulterPhones_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[Staging_OS_BU_DefaulterPhones]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
	
DROP PROCEDURE IF EXISTS [oso].[usp_OS_BU_DefaulterPhones_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterPhones_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@TypeID int ,
			@TelephoneNumber varchar(50) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDP')>=-1
		BEGIN
			DEALLOCATE cursorDP
		END

        DECLARE cursorDP CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			TypeID
			,TelephoneNumber
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterPhones]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDP
        FETCH NEXT
        FROM
              cursorDP
        INTO
			@TypeID
			,@TelephoneNumber 
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterPhones] 
					(
						[defaulterId], 
						[phone], 
						[TypeId],		 
						[LoadedOn], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@TelephoneNumber,
						@TypeID,		
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterPhones]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND TelephoneNumber = @TelephoneNumber
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDP
            INTO
				@TypeID
				,@TelephoneNumber 
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDP
        DEALLOCATE cursorDP



	SELECT @Succeed

END
END

GO



/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMappingByClientId]    Script Date: 27/11/2020 17:07:07 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCasesXMappingByClientId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesXMappingByClientId]    Script Date: 27/11/2020 17:07:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCasesXMappingByClientId]
@ClientId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, c.ClientCaseReference, c.ClientDefaulterReference, c.CaseNumber as cCaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
		WHERE
			cl.Id = @ClientId
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference,'YYYYYYYYYYYYYY' AS ClientDefaulterReference,'ZZZZZZZ' AS cCaseNumber
		
		ORDER BY c.Id DESC

    END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterIdByCDR]    Script Date: 30/11/2020 11:56:52 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetDefaulterIdByCDR]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetDefaulterIdByCDR]    Script Date: 30/11/2020 11:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDefaulterIdByCDR] @DefaulterName nvarchar(150), @CCaseId int
AS
BEGIN
	select Top 1 [d].[Id] [cDefaulterId]
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	where [d].[name] = @DefaulterName and c.id = @CCaseId
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedPhoneNo]    Script Date: 30/11/2020 11:56:52 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetDuplicatedPhoneNo]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedPhoneNo]   Script Date: 30/11/2020 11:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedPhoneNo] @cDefaulterId int, @TelephoneNumber varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], dp.phone as cTelephoneNumber
	from [Defaulters] [d]
	Inner Join DefaulterPhones dp on d.Id = dp.defaulterId
	where [d].[Id] = @cDefaulterId
	AND dp.phone = @TelephoneNumber
END
GO


DROP TABLE IF EXISTS [oso].[Staging_OS_BU_DefaulterEmails]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_BU_DefaulterEmails](	
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar] (7) NULL,
	[cDefaulterId] [int] NOT NULL,
	ClientId int NULL, 
	FUTUserId int NOT NULL,
	[UploadedOn] [datetime] DEFAULT GETDATE(),
	[Imported] [bit] DEFAULT 0,
	[ImportedOn] [datetime] NULL,
	[ErrorId] Int NULL
) ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_BU_DefaulterEmails_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_BU_DefaulterEmails_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
        DECLARE 
			@EmailAddress varchar(250) ,
			@DateLoaded datetime ,
			@Source int ,
			@CCaseId int ,
			@CDefaulterId int,
			@ErrorId int,
			@succeed bit;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorDE')>=-1
		BEGIN
			DEALLOCATE cursorDE
		END

        DECLARE cursorDE CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			EmailAddress
			,DateLoaded
			,[Source]
			,cCaseId
			,cDefaulterId
		FROM
                 oso.[Staging_OS_BU_DefaulterEmails]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorDE
        FETCH NEXT
        FROM
              cursorDE
        INTO
			@EmailAddress
			,@DateLoaded 
			,@Source 
			,@CCaseId
			,@CDefaulterId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
					INSERT [dbo].[DefaulterEmails] 
					(
						[DefaulterId], 
						[Email], 
						[DateLoaded], 
						[SourceId]
					)
					VALUES
					(
						@CDefaulterId, 
						@EmailAddress,
						@DateLoaded, 
						@Source					
					)

					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    
						Imported   = 1
						, ImportedOn = GetDate()
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_BU_DefaulterEmails]
					SET    ErrorId = @ErrorId
					WHERE
						Imported = 0 
						AND ErrorId is NULL
						AND cCaseId = @CCaseId
						AND EmailAddress = @EmailAddress
						AND cDefaulterId = @CDefaulterId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorDE
            INTO
				@EmailAddress
				,@DateLoaded 
				,@Source 
				,@CCaseId
				,@CDefaulterId
        END
        CLOSE cursorDE
        DEALLOCATE cursorDE



	SELECT @Succeed

END
END

GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedEmailAddress]    Script Date: 30/11/2020 11:56:52 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetDuplicatedEmailAddress]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedEmailAddress]   Script Date: 30/11/2020 11:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedEmailAddress] @cDefaulterId int, @EmailAddress varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], de.Email as cEmailAddress
	from [Defaulters] [d]
	Inner Join DefaulterEmails de on d.Id = de.defaulterId
	where [d].[Id] = @cDefaulterId
	AND de.Email = @EmailAddress
END
GO


/*
FUT CMDTxt Scripts


10	15	OS_BU_DefaulterPhones	SELECT TOP {max_no_row} [cCaseNumber] AS CaseNumber,[cDefaulterId] AS DefaulterId,[TelephoneNumber],[TypeID],[DateLoaded],[Source],ClientId,[FUTUserId],[UploadedOn],[Imported],[ImportedOn], e.ErrorMessage FROM oso.Staging_OS_BU_DefaulterPhones LEFT OUTER JOIN oso.SQLErrors e on e.Id = errorId	Get BU report for defaulter phones	NULL
11	15	OS_BU_DefaulterEmails	SELECT TOP {max_no_row}  [cCaseNumber] AS CaseNumber,[cDefaulterId] AS DefaulterId,[EmailAddress], [Source],ClientId,[FUTUserId],[UploadedOn],[Imported],[ImportedOn], e.ErrorMessage FROM oso.Staging_OS_BU_DefaulterEmails LEFT OUTER JOIN oso.SQLErrors e on e.Id = errorId	Get BU report for defaulter emails 	NULL




*/