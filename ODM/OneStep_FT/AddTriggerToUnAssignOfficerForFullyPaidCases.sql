﻿SELECT distinct ac.*
FROM cases c inner join dbo.[Batches] b on c.batchid = b.id 
inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id	
inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id 
inner join dbo.Clients cl on cct.clientId = cl.Id 
inner join dbo.CaseStatus cs on c.statusId = cs.Id
inner join dbo.AssignedCases ac on c.id = ac.caseId
left outer join dbo.Officers o on ac.officerId = o.Id
left outer join dbo.Teams t on ac.teamId = t.Id

WHERE cct.BrandId in (7,8,9,10,12,13)
AND assignable = 1
AND (ac.officerId is not null or ac.teamId is not null)
AND c.statusId = 3


-- Unassign all officers for cases with status 3 and brand id in (7,8,9,10,12,13)
BEGIN TRANSACTION
Update AssignedCases SET teamId=null, officerId=null, assignmentDate = GETDATE(), userId = 2, pendingStatus = 0, status = 0
WHERE Id in (SELECT distinct ac.Id
FROM cases c inner join dbo.[Batches] b on c.batchid = b.id 
inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id	
inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id 
inner join dbo.Clients cl on cct.clientId = cl.Id 
inner join dbo.CaseStatus cs on c.statusId = cs.Id
inner join dbo.AssignedCases ac on c.id = ac.caseId
left outer join dbo.Officers o on ac.officerId = o.Id
left outer join dbo.Teams t on ac.teamId = t.Id

WHERE cct.BrandId in (7,8,9,10,12,13)
AND assignable = 1
AND (ac.officerId is not null or ac.teamId is not null)
AND c.statusId = 3)

COMMIT TRANSACTION

--ROLLBACK TRANSACTION





-- Add trigger to unassign officer on each case update
DROP TRIGGER IF EXISTS trUnassignOfficer
GO

CREATE TRIGGER trUnassignOfficer ON dbo.Cases
AFTER UPDATE
AS
BEGIN
   SET NOCOUNT ON;
   DECLARE @CaseId INT;
   SELECT @CaseId = c.Id
	FROM inserted c inner join dbo.[Batches] b ON c.batchid = b.id 
	inner join dbo.ClientCaseType cct ON b.clientCaseTypeId = cct.id	
	inner join dbo.CaseType ct ON cct.CaseTypeId = ct.Id 
	inner join dbo.Clients cl ON cct.clientId = cl.Id 
	inner join dbo.CaseStatus cs ON c.statusId = cs.Id
	inner join dbo.AssignedCases ac ON c.id = ac.caseId
	left outer join dbo.Officers o ON ac.officerId = o.Id
	left outer join dbo.Teams t ON ac.teamId = t.Id

	WHERE cct.BrandId in (7,8,9,10,12,13)
	AND c.assignable = 1
	AND (ac.officerId is not null or ac.teamId is not null)
	AND c.statusId = 3

	IF (@CaseId IS NOT NULL)
	BEGIN
		UPDATE AssignedCases SET teamId=null, officerId=null, assignmentDate = GETDATE(), userId = 2, pendingStatus = 0, status = 0
		WHERE caseId = @CaseId
	END

END
GO


