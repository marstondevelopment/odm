﻿IF NOT EXISTS ( SELECT  *
                FROM    sys.schemas
                WHERE   name = N'oso' )
    EXEC('CREATE SCHEMA [oso]');
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_CheckOSClientName]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO 

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Clients_GetClientId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_GetClientId]
@ClientName nvarchar (250),
@CaseType nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @OSClientName nvarchar(500)
		Set @OSClientName = @ClientName + ' (' + @CaseType + ')'
		
		Select top 1 c.Id	  
	   from Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @OSClientName
		AND cct.[abbreviationName] = @CaseType
    END
END
GO

-- US deploy script area

-- First one

/****** Object:  StoredProcedure [oso].[usp_ODM_GetCasesXMapping]    Script Date: 23/04/2020 10:26:50 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_ODM_GetCasesXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_GetCasesXMapping]    Script Date: 23/04/2020 10:26:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_ODM_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO

-- Second one

DROP PROCEDURE [oso].[usp_OS_GetCaseId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseId]    Script Date: 11/04/2019 10:13:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as cCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO

-- Third one

DROP PROCEDURE IF EXISTS [oso].[usp_CheckOSCaseNumber]
GO

CREATE PROCEDURE [oso].[usp_CheckOSCaseNumber]
@CaseNumber nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		SELECT 
			@Id = [ocn].[CaseId]	  
		FROM 
			[dbo].[OldCaseNumbers] [ocn]		  
		WHERE 
			[ocn].[OldCaseNumber] = @CaseNumber		

		SELECT 
			CASE WHEN @Id is null THEN 1	ELSE 0 END AS Valid

    END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_NewCase_GetXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId 	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId
    END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCasesXMapping]
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO

-- Fourth one

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetClientParentXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO

-- Five one

DROP PROCEDURE IF EXISTS [oso].[usp_PopulateValidPostcodeOrDefault]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_PopulateValidPostcodeOrDefault] @DebtAddressPostcode VARCHAR(50),@AddPostCodeLiable1 VARCHAR(50),@AddPostCodeLiable2 VARCHAR(50)
,@AddPostCodeLiable3 VARCHAR(50),@AddPostCodeLiable4 VARCHAR(50),@AddPostCodeLiable5 VARCHAR(50)

AS

BEGIN
	DECLARE @DefaultPostCode VARCHAR(50) = 'CM16 5LL'

	--Normalie postcode
	DECLARE @normalizedDebtPostcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@DebtAddressPostcode))
	DECLARE @normalizedDef1Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable1))
	DECLARE @normalizedDef2Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable2))
	DECLARE @normalizedDef3Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable3))
	DECLARE @normalizedDef4Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable4))
	DECLARE @normalizedDef5Postcode [nvarchar](50) = (Select dbo.udf_NormalizePostcode(@AddPostCodeLiable5))
	
	--Validate postcode and if normalized postcode is null, replace that with a space, so that valid postcode returns 0 for that. 
	DECLARE @isValidDebtPostcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDebtPostcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef1Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef1Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef2Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef2Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef3Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef3Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef4Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef4Postcode, ' '),'AnyCompleteness','AnyType' )
	DECLARE @isValidDef5Postcode BIT = dbo.udf_ValidatePostcode( ISNULL(@normalizedDef5Postcode, ' '),'AnyCompleteness','AnyType' )
	
	--Select postcode if valid one otherwise select default postcode
	SELECT
    Case When @isValidDebtPostcode = 0 THEN CASE WHEN (@normalizedDebtPostcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDebtPostcode END AS DebtAddressPostcode,
    Case When @isValidDef1Postcode = 0 THEN CASE WHEN (@normalizedDef1Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef1Postcode END AS AddPostCodeLiable1,
    Case When @isValidDef2Postcode = 0 THEN CASE WHEN (@normalizedDef2Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef2Postcode END AS AddPostCodeLiable2,
    Case When @isValidDef3Postcode = 0 THEN CASE WHEN (@normalizedDef3Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef3Postcode END AS AddPostCodeLiable3,
    Case When @isValidDef4Postcode = 0 THEN CASE WHEN (@normalizedDef4Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef4Postcode END AS AddPostCodeLiable4,
    Case When @isValidDef5Postcode = 0 THEN CASE WHEN (@normalizedDef5Postcode IS NULL) THEN NULL ELSE @DefaultPostCode END ELSE @normalizedDef5Postcode END AS AddPostCodeLiable5

END

GRANT SELECT ON SCHEMA::de TO FUTUser
GRANT SELECT,INSERT,DELETE,UPDATE,EXECUTE ON SCHEMA::oso TO FUTUser
GRANT SELECT,INSERT ON SCHEMA::Brands TO FUTUser