USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_NewCase_AdditionalCaseData]    Script Date: 05/08/2020 15:16:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData](
	[ClientCaseReference] [varchar](100) NULL,
	[ClientId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](max) NOT NULL,
	[ConcatenatedNameValuePairs] [nvarchar](max) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_NewCase_AdditionalCaseData] ADD  DEFAULT ((0)) FOR [Imported]
GO


USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT]    Script Date: 06/08/2020 11:56:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_NewCase_AdditionalCaseData_Staging_DT] 
AS 
  BEGIN 
	SET NOCOUNT ON;
    CREATE TABLE #nvpairs 
                 ( 
                              [Id] int IDENTITY(1, 1) NOT NULL, 
                              [ClientCaseReference] [varchar](200) NOT NULL, 
                              [ClientId] [int] NOT NULL, 
                              [nvpString] [nvarchar](max) NOT NULL, 
                              [concatenatednvpString] nvarchar(max) NULL 
                 ) 
    INSERT INTO #nvpairs 
    SELECT clientcasereference, 
           clientid, 
           namevaluepairs             AS nvpstring, 
           concatenatednamevaluepairs AS concatenatednvpstring 
    FROM   oso.staging_os_newcase_additionalcasedata 
    WHERE  imported = 0 
    AND    errorid IS NULL 


    DECLARE @NVPID                 INT; 
    DECLARE @CaseId                INT; 
    DECLARE @CCRef                 VARCHAR(200); 
    DECLARE @ClId                  INT; 
    DECLARE @StringVal             NVARCHAR(max); 
    DECLARE @concatenatednvpString NVARCHAR(max); 
    DECLARE nvp CURSOR fast_forward FOR 
    SELECT id, 
           clientcasereference, 
           clientid, 
           nvpstring, 
           concatenatednvpstring 
    FROM   #nvpairs 
    OPEN nvp 
    FETCH next 
    FROM  nvp 
    INTO  @NVPID, 
          @CCRef, 
          @ClId, 
          @StringVal, 
          @concatenatednvpString 
    WHILE @@FETCH_STATUS = 0 
    BEGIN 
      BEGIN try 
        BEGIN TRANSACTION 
		SET @CaseId = 0
        SELECT     @CaseId = c.id 
        FROM       cases c 
        INNER JOIN batches b ON   c.batchid = b.id 
        INNER JOIN clientcasetype CT  ON  b.clientcasetypeid = ct.id 
        WHERE      ct.clientid = @ClId 
        AND        c.clientcasereference = @CCRef 
		
		--debug
		--Select @CaseId, @ClId, @CCRef

        IF( @CaseId IS NOT NULL AND @CaseId <>0 ) 
        BEGIN 
         
          CREATE TABLE  #SINGLENAMEVALUEPAIR  
                                             ( 
												id            INT IDENTITY(1, 1) NOT NULL,
												namevaluepair NVARCHAR(max),
												namevalueid   INT
                                             ) 


          INSERT INTO #SingleNameValuePair 
                      ( 
                                  namevaluepair, 
                                  namevalueid 
                      ) 

          EXEC [oso].[usp_SplitString] 
            @StringVal, 
            251, 
            0; 

			DELETE FROM #SingleNameValuePair where namevaluepair = ''
          
          DECLARE @count INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #SingleNameValuePair 
          ) 
          BEGIN 
		  
            DECLARE @SingleNameValue NVARCHAR(max) 
            DECLARE @Name            NVARCHAR(2000) 
            DECLARE @Value           NVARCHAR(max) 
            DECLARE @Id              INT 
			
            SET @SingleNameValue = 
            ( 
                     SELECT TOP 1 
                              namevaluepair
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
            SET @Id = 
            ( 
                     SELECT TOP 1 
                              id 
                     FROM     #SingleNameValuePair 
                     ORDER BY id 
            ) 
           
           
            IF( 
                ( 
                SELECT Count(value) 
                FROM   dbo.Fnsplit( 
                       ( 
                              SELECT TOP 1 
                                     namevaluepair 
                              FROM   #SingleNameValuePair 
                              WHERE  id = @count), ':')) > 2) 
            BEGIN 
				DECLARE @CaseNoteExists INT
				Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @SingleNameValue
				IF (@CaseNoteExists = 0)
				BEGIN
              
                INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
                SELECT @CaseId, 
                     NULL, 
                     2, 
					 @SingleNameValue,
					 --CASE @SingleNameValue WHEN NULL THEN '' 
					 --ELSE (
						-- CASE LEN(@SingleNameValue) WHEN 0 THEN @SingleNameValue 
						--	ELSE LEFT(@SingleNameValue, LEN(@SingleNameValue) - 1) 
						-- END 
					 --) END AS finalValue,                     
                     Getdate(), 
                     1, 
                     NULL, 
                     NULL 
					 END
            END 
            ELSE 
            BEGIN 
               
              CREATE TABLE #namevalue 
                           ( 
                                        id   int IDENTITY(1, 1) NOT NULL, 
                                        word nvarchar(max) 
                           ) 
              INSERT INTO #namevalue 
                          ( 
                                      word 
                          ) 
              SELECT value 
              FROM   dbo.Fnsplit(@SingleNameValue, ':') 

              SET @Name = 
              ( 
                     SELECT Rtrim(Ltrim(word)) 
                     FROM   #namevalue 
                     WHERE  id = 1 ) 
              SET @Value = 
              ( 
                     SELECT Rtrim(Ltrim(word))
                     FROM   #namevalue 
                     WHERE  id = 2 ) 
              
			  SET @Value = REPLACE(@Value,';','')
              IF EXISTS 
              ( 
                     SELECT * 
                     FROM   dbo.additionalcasedata 
                     WHERE  caseid = @CaseId 
                     AND    NAME = @Name ) 
              BEGIN 
                UPDATE dbo.additionalcasedata 
                SET    value = Rtrim(Ltrim(@Value)) 
                WHERE  caseid = @CaseId 
                AND    NAME = @Name 
              END 
              ELSE 
              BEGIN 
                INSERT INTO dbo.additionalcasedata 
                SELECT @CaseId, 
                       @Name, 
                       Rtrim(Ltrim(@Value)), 
                       Getdate() 
              END 

			  IF Object_id('tempdb..#NameValue') IS NOT NULL 
			  DROP TABLE #namevalue

            END 
            DELETE 
            FROM   #SingleNameValuePair 
            WHERE  id = @Id 
            --AND    namevaluepair = @SingleNameValue 
            SET @count = @count + 1 
          END 
			
		 
          --For concatenatednvpString 
			 
          CREATE TABLE  #CONCATENATEDNVPSTRINGTABLE  
                            ( 
                                id   INT IDENTITY(1, 1) NOT NULL,
                                text NVARCHAR(max)
                            ) 
          INSERT INTO #CONCATENATEDNVPSTRINGTABLE 
                      ( 
                                  text 
                      ) 
          SELECT value 
          FROM   dbo.Fnsplit(@concatenatednvpString, ';') 

          DECLARE @concCount INT = 1 
          WHILE EXISTS 
          ( 
                 SELECT * 
                 FROM   #CONCATENATEDNVPSTRINGTABLE ) 
          BEGIN 
		  
            CREATE TABLE #concatenatednamevalue 
                         ( 
                                      id   int IDENTITY(1, 1) NOT NULL, 
                                      word nvarchar(max) 
                         ) 
            INSERT INTO #concatenatednamevalue 
                        ( 
                                    word 
                        ) 
            SELECT value FROM 
                   dbo.Fnsplit( 
                   ( 
                          SELECT TOP 1 
                                 text 
                          FROM   #CONCATENATEDNVPSTRINGTABLE 
                          WHERE  id = @concCount), ':') 
            SET @Name = 
            ( 
                   SELECT Rtrim(Ltrim(word)) 
                   FROM   #concatenatednamevalue 
                   WHERE  id = 1 ) 
            SET @Value = 
            ( 
                   SELECT Rtrim(Ltrim(word))
                   FROM   #concatenatednamevalue 
                   WHERE  id = 2 ) 
            
            IF EXISTS 
            ( 
                   SELECT * 
                   FROM   dbo.additionalcasedata 
                   WHERE  caseid = @CaseId 
                   AND    NAME = @Name and Name is not null ) 
            BEGIN 
              UPDATE dbo.additionalcasedata 
              SET    value = Rtrim(Ltrim(@Value)) 
              WHERE  caseid = @CaseId 
              AND    NAME = @Name 
            END 
            ELSE 
            BEGIN 
				If(@Value IS NOT NULL)
				BEGIN
				  INSERT INTO dbo.additionalcasedata 
				  SELECT @CaseId, 
						 @Name, 
						 Rtrim(Ltrim(@Value)), 
						 Getdate() 
				END
            END 

			IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 

            DELETE 
            FROM   #CONCATENATEDNVPSTRINGTABLE 
            WHERE  id = @concCount 
            SET @concCount = @concCount + 1 
          END 

		  IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
          ----------------------- 
          UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
          SET    imported = 1, 
                 importedon = Getdate() 
          WHERE  clientcasereference = @CCRef 
          AND    clientid = @ClId --AND [oso].[fn_RemoveMultipleSpaces](NameValuePairs) =@StringVal 
          AND    ( 
                        imported IS NULL 
                 OR     imported = 0 ) 
        END 

		IF Object_id('tempdb..#SINGLENAMEVALUEPAIR') IS NOT NULL 
             DROP TABLE #SINGLENAMEVALUEPAIR 
		IF Object_id('tempdb..#CONCATENATEDNVPSTRINGTABLE') IS NOT NULL 
            DROP TABLE #CONCATENATEDNVPSTRINGTABLE
        IF Object_id('tempdb..#ConcatenatedNameValue') IS NOT NULL 
            DROP TABLE #concatenatednamevalue 
		IF Object_id('tempdb..#NameValue') IS NOT NULL 
            DROP TABLE #namevalue

        COMMIT 
      END try 
      BEGIN catch 
        ROLLBACK TRANSACTION 
        INSERT INTO oso.[SQLErrors] VALUES 
                    ( 
                                Error_number(), 
                                Error_severity(), 
                                Error_state(), 
                                Error_procedure(), 
                                Error_line(), 
                                Error_message(), 
                                Getdate() 
                    ) 
        DECLARE @ErrorId INT = Scope_identity(); 
        UPDATE [oso].[Staging_OS_NewCase_AdditionalCaseData] 
        SET    errorid = @ErrorId 
        WHERE  clientcasereference = @CCRef 
        AND    clientid = @ClId 
        --AND    [oso].[Fn_removemultiplespaces]( namevaluepairs) = @StringVal 
        SELECT 0 AS succeed 
      END catch 
      FETCH next 
      FROM  nvp 
      INTO  @NVPID, 
            @CCRef, 
            @ClId, 
            @StringVal, 
            @concatenatednvpString 
    END 
    CLOSE nvp 
    DEALLOCATE nvp 
  END
  
---------------------------------------------------------------------------------------------------------------------
USE [Columbus]
GO

/****** Object:  Table [oso].[Staging_OS_AdditionalCaseData]    Script Date: 05/08/2020 15:18:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_AdditionalCaseData](
	[CaseNumber] [varchar](10) NOT NULL,
	[NameValuePairs] [nvarchar](2000) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[Staging_OS_AdditionalCaseData] ADD  DEFAULT ((0)) FOR [Imported]
GO




USE [Columbus]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AdditionalCaseData_Staging_DT]    Script Date: 05/08/2020 15:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [oso].[usp_OS_AdditionalCaseData_Staging_DT] 
AS 
  BEGIN 
      DECLARE @CaseNumber VARCHAR(10); 
      DECLARE @Pairs NVARCHAR(MAX); 
      DECLARE @CaseId INT; 
      DECLARE @MultipleNameValuePairs TABLE 
        ( 
           casenumber     VARCHAR(10), 
           namevaluepairs NVARCHAR(MAX) 
        ) 

      INSERT INTO @MultipleNameValuePairs 
                  (casenumber, 
                   namevaluepairs) 
      SELECT CaseNumber, 
             NameValuePairs 
      FROM   oso.Staging_OS_AdditionalCaseData 
      WHERE  ErrorId IS NULL AND Imported = 0

      DECLARE namevaluepairs CURSOR fast_forward FOR 
        SELECT casenumber, 
               namevaluepairs 
        FROM   @MultipleNameValuePairs 

      OPEN namevaluepairs 

      FETCH next FROM namevaluepairs INTO @CaseNumber, @Pairs 

      WHILE @@FETCH_STATUS = 0 
        BEGIN 
            BEGIN try 
                BEGIN TRANSACTION 

				SET @Pairs=LEFT(@Pairs, LEN(@Pairs)-23);

                SELECT @CaseId = id 
                FROM   [dbo].[cases] 
                WHERE  casenumber = @CaseNumber 
				
                IF( @CaseId IS NOT NULL ) 
                  BEGIN
                    DECLARE @Name NVARCHAR(MAX) 
                    DECLARE @Value NVARCHAR(MAX) 
							
					--If name value pairs have more than one set of name values, consider them for case note insertion, otherwise additional case data
					IF((SELECT Count(value) FROM dbo.Fnsplit((@Pairs), ':')) > 2) 
						BEGIN 
							DECLARE @CaseNoteExists INT
							Select @CaseNoteExists = Count(*) from dbo.CaseNotes Where caseId = @CaseId AND text = @Pairs
							IF (@CaseNoteExists = 0)
							BEGIN
								INSERT INTO dbo.casenotes (caseId,officerId,userId,text,occured,visible,groupId,TypeId)
								SELECT @CaseId, 
										NULL, 
										2, 
										Rtrim(Ltrim(@Pairs)), 
										Getdate(), 
										1, 
										NULL, 
										NULL 
							END
						END 
						ELSE 
						BEGIN 
								
							IF OBJECT_ID('tempdb..#NameValue') IS NOT NULL
								DROP TABLE #NameValue

							CREATE TABLE #NameValue 
								( 
									id   INT IDENTITY(1, 1) NOT NULL, 
									word NVARCHAR(MAX) 
								) 

							INSERT INTO #NameValue (word) 
							SELECT value 
							FROM   dbo.Fnsplit(@Pairs, ':') 
							
							SET @Name = (SELECT Rtrim(Ltrim(word)) 
							FROM   #NameValue 
							WHERE  id = 1 )

							SET @Value = (SELECT Rtrim(Ltrim(word)) 
							FROM   #NameValue 
							WHERE  id = 2 )
							
							DECLARE @Counter INT = (SELECT Count(*) 
								FROM   dbo.AdditionalCaseData 
								WHERE  NAME = @Name 
										AND caseid = @CaseId) 
						
							IF( @Counter > 0 ) 
								BEGIN 
									UPDATE dbo.AdditionalCaseData 
									SET    value = Rtrim(Ltrim(@Value)), LoadedOn = GETDATE()
									WHERE  caseid = @CaseId 
											AND NAME = @Name 
								END 
						ELSE 
						BEGIN
							INSERT INTO dbo.AdditionalCaseData 
											(caseid, 
											NAME, 
											value,
											LoadedOn) 
								VALUES      (@CaseId, 
											@Name, 
											Rtrim(Ltrim(@Value)),
											GETDATE()) 
							END 

						END
						
                      UPDATE [oso].[Staging_OS_AdditionalCaseData] 
                      SET    imported = 1, 
                             importedon = Getdate() 
                      WHERE  CaseNumber = @CaseNumber 
                             AND NameValuePairs LIKE '%'+ @Pairs +'%'
                             AND (Imported IS NULL OR Imported = 0)
				END
                COMMIT 
            END try 

            BEGIN catch 
                ROLLBACK TRANSACTION 

                INSERT INTO oso.[sqlerrors] 
                VALUES      (Error_number(), 
                             Error_severity(), 
                             Error_state(), 
                             Error_procedure(), 
                             Error_line(), 
                             Error_message(), 
                             Getdate() ) 

                DECLARE @ErrorId INT = Scope_identity(); 

                UPDATE [oso].[Staging_OS_AdditionalCaseData] 
                SET    errorid = @ErrorId 
                WHERE  casenumber = @CaseNumber 
                       AND namevaluepairs = @Pairs 
                       AND imported = 0 

                SELECT 0 AS Succeed 
            END catch 

            FETCH next FROM namevaluepairs INTO @CaseNumber, @Pairs 
        END 

      CLOSE namevaluepairs 

      DEALLOCATE namevaluepairs 
  END 