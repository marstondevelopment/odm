USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OneStep_Stages_CrossRef]    Script Date: 29/06/2022 16:23:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_Stages_CrossRef](
	[OSStageName] [varchar](50) NOT NULL,
	[CStageTemplateId] [int] NOT NULL,
	[CPhaseTemplateId] [int] NOT NULL
) ON [PRIMARY]
GO
