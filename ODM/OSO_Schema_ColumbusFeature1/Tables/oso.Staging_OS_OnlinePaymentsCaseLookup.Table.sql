USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_OS_OnlinePaymentsCaseLookup]    Script Date: 29/06/2022 16:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_OnlinePaymentsCaseLookup](
	[ClientRef] [varchar](200) NOT NULL,
	[CaseNumber] [varchar](7) NOT NULL,
	[Client] [varchar](80) NOT NULL,
	[Scheme] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[PaymentDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
