USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OneStep_Workflow_CaseMap]    Script Date: 29/06/2022 16:23:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_Workflow_CaseMap](
	[StageName] [nvarchar](255) NULL,
	[Workflow] [nvarchar](255) NULL,
	[Phase Index] [float] NULL,
	[Columbus Phase] [nvarchar](255) NULL,
	[Stage Index] [float] NULL,
	[Columbus Stage] [nvarchar](255) NULL,
	[Days Dependant] [nvarchar](255) NULL,
	[Day00] [nvarchar](255) NULL,
	[Day01] [nvarchar](255) NULL,
	[Day02] [nvarchar](255) NULL,
	[Day03] [nvarchar](255) NULL,
	[Day04] [nvarchar](255) NULL,
	[Day05] [nvarchar](255) NULL,
	[Day06] [nvarchar](255) NULL,
	[Day07] [nvarchar](255) NULL,
	[Day08] [nvarchar](255) NULL,
	[Day09] [nvarchar](255) NULL,
	[Day10] [nvarchar](255) NULL,
	[Day11] [nvarchar](255) NULL,
	[Day12] [nvarchar](255) NULL,
	[Day13] [nvarchar](255) NULL,
	[Day14] [nvarchar](255) NULL,
	[Day15] [nvarchar](255) NULL,
	[Day16] [nvarchar](255) NULL,
	[Day17] [nvarchar](255) NULL,
	[Day18] [nvarchar](255) NULL,
	[Day19] [nvarchar](255) NULL,
	[Day20] [nvarchar](255) NULL,
	[Day21] [nvarchar](255) NULL,
	[Day22] [nvarchar](255) NULL,
	[Day23] [nvarchar](255) NULL,
	[F32] [nvarchar](255) NULL,
	[WorkflowID] [int] NULL,
	[StageTemplateID] [int] NULL,
	[PhaseTemplateID] [int] NULL
) ON [PRIMARY]
GO
