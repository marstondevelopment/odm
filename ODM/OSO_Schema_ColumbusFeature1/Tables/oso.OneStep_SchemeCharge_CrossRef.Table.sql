USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OneStep_SchemeCharge_CrossRef]    Script Date: 29/06/2022 16:23:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_SchemeCharge_CrossRef](
	[CSchemeChargeName] [varchar](250) NOT NULL,
	[OSSchemeChargeName] [varchar](250) NOT NULL,
	[CSchemeChargeId] [int] NOT NULL
) ON [PRIMARY]
GO
