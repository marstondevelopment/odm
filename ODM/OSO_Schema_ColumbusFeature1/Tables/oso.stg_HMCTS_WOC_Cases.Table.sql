USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_HMCTS_WOC_Cases]    Script Date: 29/06/2022 16:23:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_HMCTS_WOC_Cases](
	[ClientDefaulterReference] [varchar](30) NULL,
	[ClientCaseReference] [varchar](200) NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[StartDate] [datetime] NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[TitleLiable1] [varchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[NINOLiable1] [varchar](13) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceDate] [datetime] NULL,
	[OffenceCode] [varchar](5) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[TECDate] [datetime] NULL,
	[Currency] [int] NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[MinorLiable1] [bit] NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[EndDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[AddCountryLiable1] [int] NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](max) NULL,
	[CaseNotes] [nvarchar](1000) NULL,
	[FUTUserId] [int] NULL,
	[CaseId] [int] NULL,
	[BatchReferenceNo] [varchar](500) NULL,
	[BatchNoGenerated] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[UploadedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF__stg_HMCTS_WOC__Batch__2A5AC89A]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO
ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF__stg_HMCTS_WOC__Impor__2B4EECD3]  DEFAULT ((0)) FOR [Imported]
GO
ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF_stg_HMCTS_WOC_Cases_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO
