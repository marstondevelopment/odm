USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OSColInstalmentFrequencyXRef]    Script Date: 29/06/2022 16:23:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColInstalmentFrequencyXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OSFrequencyDays] [int] NOT NULL,
	[CFrequencyName] [varchar](50) NOT NULL,
	[CFrequencyId] [int] NOT NULL,
 CONSTRAINT [PK_OSColInstalmentFrequencyXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
