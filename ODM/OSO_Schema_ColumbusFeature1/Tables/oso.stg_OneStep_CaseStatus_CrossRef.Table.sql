USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_CaseStatus_CrossRef]    Script Date: 29/06/2022 16:23:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_CaseStatus_CrossRef](
	[OSCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusName] [varchar](50) NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
