USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_OS_Notes]    Script Date: 29/06/2022 16:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_Notes](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](1000) NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_Notes] ADD  CONSTRAINT [DF__Staging_O__Impor__19E70EA4]  DEFAULT ((0)) FOR [Imported]
GO
