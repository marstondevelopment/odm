USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_OS_NewCase_Notes_CaseNumber]    Script Date: 29/06/2022 16:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber](
	[CaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_NewCase_Notes_CaseNumber] ADD  CONSTRAINT [DF_Staging_OS_NewCase_Notes_CaseNumber_Imported]  DEFAULT ((0)) FOR [Imported]
GO
