USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_SchemePayment_CrossRef]    Script Date: 29/06/2022 16:23:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_SchemePayment_CrossRef](
	[CSchemePaymentName] [varchar](250) NOT NULL,
	[OSSchemePaymentName] [varchar](250) NOT NULL,
	[CSchemePaymentId] [int] NOT NULL
) ON [PRIMARY]
GO
