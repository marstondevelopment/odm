USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_OS_BU_DefaulterPhones]    Script Date: 29/06/2022 16:23:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_BU_DefaulterPhones](
	[TypeID] [int] NOT NULL,
	[TelephoneNumber] [varchar](50) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NOT NULL,
	[cCaseId] [int] NOT NULL,
	[cCaseNumber] [varchar](7) NULL,
	[cDefaulterId] [int] NOT NULL,
	[ClientId] [int] NULL,
	[FUTUserId] [int] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterPhones_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO
ALTER TABLE [oso].[Staging_OS_BU_DefaulterPhones] ADD  CONSTRAINT [DF_Staging_OS_BU_DefaulterPhones_Imported]  DEFAULT ((0)) FOR [Imported]
GO
