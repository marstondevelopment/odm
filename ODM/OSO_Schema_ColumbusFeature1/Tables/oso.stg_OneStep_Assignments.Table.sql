USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_Assignments]    Script Date: 29/06/2022 16:23:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Assignments](
	[cCaseId] [int] NOT NULL,
	[COfficerId] [int] NULL,
	[AssignmentDate] [datetime] NULL,
	[CUserId] [int] NOT NULL,
	[TeamId] [int] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
