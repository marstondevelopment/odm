USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_OS_AdditionalCaseData]    Script Date: 29/06/2022 16:23:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_OS_AdditionalCaseData](
	[CaseId] [int] NOT NULL,
	[NameValuePairs] [nvarchar](2000) NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [oso].[Staging_OS_AdditionalCaseData] ADD  CONSTRAINT [DF_Staging_OS_AdditionalCaseData_Imported]  DEFAULT ((0)) FOR [Imported]
GO
