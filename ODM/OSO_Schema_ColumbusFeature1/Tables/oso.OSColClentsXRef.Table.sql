USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OSColClentsXRef]    Script Date: 29/06/2022 16:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OSColClentsXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Connid] [int] NOT NULL,
	[CClientId] [int] NOT NULL,
	[ClientName] [nvarchar](250) NULL,
 CONSTRAINT [PK_OSColClentsXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
