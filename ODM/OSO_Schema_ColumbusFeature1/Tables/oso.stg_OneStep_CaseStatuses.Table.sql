USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_CaseStatuses]    Script Date: 29/06/2022 16:23:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_CaseStatuses](
	[cCaseId] [int] NOT NULL,
	[CCaseStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
