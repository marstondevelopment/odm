USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Officers]    Script Date: 29/06/2022 16:23:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_Officers](
	[officerno] [int] NOT NULL,
	[id] [int] NULL,
	[drakeOfficerId] [int] NULL,
	[firstName] [varchar](80) NULL,
	[middleName] [varchar](80) NULL,
	[lastName] [varchar](80) NULL,
	[email] [varchar](100) NULL,
	[phone] [varchar](25) NULL,
	[started] [datetime] NOT NULL,
	[leftDate] [datetime] NULL,
	[typeId] [int] NOT NULL,
	[maxCases] [int] NOT NULL,
	[minFee] [decimal](18, 4) NOT NULL,
	[averageFee] [decimal](18, 4) NOT NULL,
	[minExecs] [int] NOT NULL,
	[signature] [varchar](50) NULL,
	[enabled] [bit] NOT NULL,
	[performanceTarget] [decimal](10, 2) NOT NULL,
	[outcodes] [varchar](1000) NOT NULL,
	[Manager] [bit] NULL,
	[parentId] [int] NULL,
	[isRtaCertified] [bit] NOT NULL,
	[rtaCertificationExpiryDate] [datetime] NULL,
	[marston] [bit] NOT NULL,
	[rossendales] [bit] NOT NULL,
	[swift] [bit] NOT NULL,
	[rossendalesdca] [bit] NOT NULL,
	[marstonlaa] [bit] NOT NULL,
	[ctax] [bit] NULL,
	[nndr] [bit] NULL,
	[tma] [bit] NULL,
	[cmg] [bit] NULL,
	[dca] [bit] NOT NULL,
	[laa] [bit] NOT NULL,
	[Existing] [bit] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
