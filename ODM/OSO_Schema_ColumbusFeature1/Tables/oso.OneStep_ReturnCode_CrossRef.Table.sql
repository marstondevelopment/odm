USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[OneStep_ReturnCode_CrossRef]    Script Date: 29/06/2022 16:23:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[OneStep_ReturnCode_CrossRef](
	[CReturnCode] [int] NOT NULL,
	[OSReturnCode] [int] NOT NULL,
	[CId] [int] NULL
) ON [PRIMARY]
GO
