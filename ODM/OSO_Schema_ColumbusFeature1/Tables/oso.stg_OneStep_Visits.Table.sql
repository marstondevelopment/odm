USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_Visits]    Script Date: 29/06/2022 16:23:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_Visits](
	[CCaseId] [int] NOT NULL,
	[DefaulterName] [nvarchar](150) NOT NULL,
	[VisitType] [varchar](50) NOT NULL,
	[VisitDate] [datetime] NULL,
	[AddressLine1] [varchar](80) NULL,
	[AddressLine2] [varchar](100) NULL,
	[AddressLine3] [varchar](100) NULL,
	[AddressLine4] [varchar](100) NULL,
	[AddressLine5] [varchar](100) NULL,
	[Postcode] [varchar](12) NULL,
	[COfficerId] [int] NOT NULL,
	[BuildingType] [varchar](50) NULL,
	[DoorColour] [varchar](50) NULL,
	[Notes] [varchar](1000) NULL,
	[ProofOfIdentity] [varchar](50) NULL,
	[PersonContacted] [varchar](50) NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
