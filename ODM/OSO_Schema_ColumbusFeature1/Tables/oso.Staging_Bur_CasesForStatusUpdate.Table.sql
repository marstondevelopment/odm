USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[Staging_Bur_CasesForStatusUpdate]    Script Date: 29/06/2022 16:23:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[Staging_Bur_CasesForStatusUpdate](
	[CaseNo] [varchar](7) NOT NULL,
	[Updated] [bit] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
