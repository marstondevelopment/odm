USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_OneStep_DefaulterEmails]    Script Date: 29/06/2022 16:23:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_OneStep_DefaulterEmails](
	[EmailAddress] [varchar](250) NOT NULL,
	[DateLoaded] [datetime] NULL,
	[Source] [int] NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
