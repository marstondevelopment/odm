USE [ColumbusFeature1]
GO
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_TaggedNotes]    Script Date: 29/06/2022 16:23:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[CaseNote] [nvarchar](1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar](250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO
