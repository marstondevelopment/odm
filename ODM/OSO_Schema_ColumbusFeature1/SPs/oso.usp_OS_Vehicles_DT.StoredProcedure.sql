USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Vehicles_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Vehicles_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			DECLARE
				@VRM VARCHAR(7),
				@vrmOwner BIT,
				@DefaulterId INT,
				@OfficerId INT,
				@DateLoaded DATETIME,
				@CaseId INT,
				@VehicleId INT

			DECLARE cursorT 
				CURSOR FOR
					SELECT
						[VRM], 
						[VRMOwner], 
						[cDefaulterId], 
						[cOfficerId], 
						[DateLoaded],						 
						[cCaseId]
					FROM
						[oso].[stg_OneStep_Vehicles]
					WHERE 
						Imported = 0
				OPEN cursorT
				FETCH NEXT
					FROM
						cursorT
					INTO
						@VRM,
						@vrmOwner,
						@DefaulterId,
						@OfficerId,
						@DateLoaded,
						@CaseId
				
				WHILE
					@@FETCH_STATUS = 0
					BEGIN
						INSERT [dbo].[Vehicles] 
						(
							[vrm],
							[vrmOwner],
							[defaulterId],
							[officerId],
							[VrmReceivedOn]
						)
						VALUES
						(
							@VRM,
							@vrmOwner,
							@DefaulterId,
							@OfficerId,
							@DateLoaded
						)
						SET @VehicleId = SCOPE_IDENTITY();

						INSERT [dbo].[CaseVehicles] 
						(
							[caseId], 
							[vehicleId]
						)
						VALUES
						(
							@CaseId,
							@VehicleId
						)

						FETCH NEXT
							FROM
								cursorT
							INTO
								@VRM,
								@vrmOwner,
								@DefaulterId,
								@OfficerId,
								@DateLoaded,
								@CaseId
					END
				CLOSE cursorT
				DEALLOCATE cursorT			
			
			UPDATE [oso].[stg_OneStep_Vehicles]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
			END TRY	
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END

END
GO
