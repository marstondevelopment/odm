USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Get Count for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUserCount]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
    END
END
GO
