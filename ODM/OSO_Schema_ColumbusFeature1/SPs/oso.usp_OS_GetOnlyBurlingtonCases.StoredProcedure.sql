USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOnlyBurlingtonCases]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetOnlyBurlingtonCases]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT
			c.caseNumber CaseNumber, c.caseNumber CaseNo
		FROM Cases c
			INNER JOIN Batches ba ON c.batchId = ba.Id
			INNER JOIN ClientCaseType cct ON ba.clientCaseTypeId = cct.Id
			INNER JOIN Brands.Brands bb ON cct.brandId = bb.Id
		WHERE 
							--Changed because this stops the template working. Users have been made aware to be careful 
							--to check statuses before running
			cct.brandId = 5 and c.statusId in (2,3)
    END
END
GO
