USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ActionMappingJD]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jamie Darlington
-- Create date: 08 May, 2020
-- Description:	Used to pick up the latest action to be applied when mapping OneStep workflow actions
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ActionMappingJD] 
	@DaysInPhase int, 
	@Wftid int,
	@OneStepStageName varchar(255),
	@LatestAction varchar(255) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

SET @LatestAction = (select 
CASE 
	WHEN @DaysInPhase >= 23 THEN COALESCE(Day23, Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 22 THEN COALESCE(Day22, Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 21 THEN COALESCE(Day21, Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 20 THEN COALESCE(Day20, Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 19 THEN COALESCE(Day19, Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 18 THEN COALESCE(Day18, Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 17 THEN COALESCE(Day17, Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 16 THEN COALESCE(Day16, Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 15 THEN COALESCE(Day15, Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 14 THEN COALESCE(Day14, Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 13 THEN COALESCE(Day13, Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 12 THEN COALESCE(Day12, Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 11 THEN COALESCE(Day11, Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  = 10 THEN COALESCE(Day10, Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  9 THEN COALESCE(Day09, Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  8 THEN COALESCE(Day08, Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  7 THEN COALESCE(Day07, Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  6 THEN COALESCE(Day06, Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  5 THEN COALESCE(Day05, Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  4 THEN COALESCE(Day04, Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  3 THEN COALESCE(Day03, Day02, Day01, Day00)
	WHEN @DaysInPhase  =  2 THEN COALESCE(Day02, Day01, Day00)
	WHEN @DaysInPhase  =  1 THEN COALESCE(Day01, Day00)
	WHEN @DaysInPhase <=  0 THEN Day00 END As LastAction
	from oso.OneStep_Workflow_CaseMap
where workflowid = @wftid
and StageName = @OneStepStageName
)
END
GO
