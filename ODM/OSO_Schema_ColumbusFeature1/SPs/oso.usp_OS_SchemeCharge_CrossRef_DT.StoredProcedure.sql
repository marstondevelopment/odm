USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemeCharge_CrossRef_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemeCharge_CrossRef_DT] AS
BEGIN	
	INSERT [oso].[OneStep_SchemeCharge_CrossRef]	
	(
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId     
	)
	SELECT
		CSchemeChargeName
		,OSSchemeChargeName
		,CSchemeChargeId   
	FROM
		[oso].[stg_OneStep_SchemeCharge_CrossRef]		

		 update scx 
		  set scx.cschemechargeid = new.id 
		  --select scx.*,new.id [newid]
		  from 
		   oso.OneStep_SchemeCharge_Crossref scx
		  join schemecharges new on new.name = scx.cschemechargename AND 
		  new.chargingschemeid = (select id from chargingscheme where schemename = 'Rossendales Standard TCE Charging Scheme')

	SELECT
        1 as Succeed
	
END
GO
