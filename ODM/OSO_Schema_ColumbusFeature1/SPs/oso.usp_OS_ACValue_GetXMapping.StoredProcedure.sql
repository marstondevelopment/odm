USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ACValue_GetXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract ACValue for open cases
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ACValue_GetXMapping]
@ClientName nvarchar (500)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT C.Id AS CCaseId, C.clientCaseReference, oso.udf_GetOutstandingBalanceIncludingLinkedCases(C.Id) as ACValue  
FROM dbo.Cases AS C
JOIN dbo.CaseStatus AS CS ON C.statusId = CS.Id  
Join Batches b on C.batchId = b.Id
Join ClientCaseType cct on b.clientCaseTypeId = cct.Id
Join Clients cl on cct.clientId = cl.id
LEFT JOIN dbo.CurrencyDetails AS CD ON C.currencyId = CD.Id  
WHERE 
cl.name in (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
AND c.statusId IN (
		1 --Unpaid
		,2 --Part-Paid
		,6 --On-Hold
		)

		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 0 AS ACValue
    END
END

GO
