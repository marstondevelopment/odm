USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_ConvertFixedStringAddress-DefaulterAdd]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  LP
-- Create date: 12-11-2021
-- Description: Splits a Fixed Length String for upto five address lines in one string
-- =============================================

CREATE PROCEDURE  [oso].[usp_ConvertFixedStringAddress-DefaulterAdd]
     @strDef NVARCHAR(Max)

AS

DECLARE @str1 VARCHAR(MAX)
DECLARE @str2 VARCHAR(MAX)
DECLARE @str3 VARCHAR(MAX)
DECLARE @str4 VARCHAR(MAX)
DECLARE @str5 VARCHAR(MAX)
BEGIN 
		SET @str1 = CASE	WHEN CHARINDEX('  ', @strDef) = 0 THEN @strDef
							ELSE LTRIM(RTRIM(LEFT(@strDef, CHARINDEX('  ', @strDef) ))) END
		SET @strDef = LTRIM(RTRIM(RIGHT(@strDef, LEN(@strDef)-CHARINDEX('  ', @strDef))))
--		SELECT @str1 AS [Str1]
--		SELECT @strDef
		SET @str2 = CASE	WHEN CHARINDEX('   ',@strDef) = 0 AND LTRIM(RTRIM(@strDef)) = @str1 THEN ''
							WHEN CHARINDEX('   ',@strDef) = 0 THEN LTRIM(RTRIM(@strDef))  
							ELSE LTRIM(RTRIM(LEFT(@strDef, CHARINDEX('  ', @strDef) ))) END
		SET @strDef = LTRIM(RIGHT(@strDef, LEN(@strDef)-CHARINDEX('  ', @strDef)))
--		SELECT @str2 AS [Str2]
		SET @str3 = CASE	WHEN CHARINDEX('   ',@strDef) = 0 AND (LTRIM(RTRIM(@strDef)) = @str1 OR LTRIM(RTRIM(@strDef)) = @str2) THEN ''
							WHEN CHARINDEX('   ',@strDef) = 0 THEN LTRIM(RTRIM(@strDef))  
							ELSE LTRIM(RTRIM(LEFT(@strDef, CHARINDEX('  ', @strDef) ))) END
		SET @strDef = LTRIM(RIGHT(@strDef, LEN(@strDef)-CHARINDEX('  ', @strDef)))
--		SELECT @str3 AS [Str3]
		SET @str4 = CASE	WHEN CHARINDEX('   ',@strDef) = 0 AND (LTRIM(RTRIM(@strDef)) = @str1 OR LTRIM(RTRIM(@strDef)) = @str1 OR 
															LTRIM(RTRIM(@strDef)) = @str2 OR LTRIM(RTRIM(@strDef)) = @str3) THEN ''
							WHEN CHARINDEX('   ',@strDef) = 0 THEN LTRIM(RTRIM(@strDef))  
							ELSE LTRIM(RTRIM(LEFT(@strDef, CHARINDEX('  ', @strDef) ))) END
		SET @strDef = LTRIM(RIGHT(@strDef, LEN(@strDef)-CHARINDEX('  ', @strDef)))
--		SELECT @str4 AS [Str4]
		SET @str5 = CASE WHEN CHARINDEX('   ',@strDef) = 0 AND (LTRIM(RTRIM(@strDef)) = @str1 OR LTRIM(RTRIM(@strDef)) = @str1 OR 
															LTRIM(RTRIM(@strDef)) = @str2 OR LTRIM(RTRIM(@strDef)) = @str3 OR LTRIM(RTRIM(@strDef)) = @str4) 
															THEN ''
							WHEN CHARINDEX('   ',@strDef) = 0 THEN LTRIM(RTRIM(@strDef))  
							ELSE LTRIM(RTRIM(LEFT(@strDef, CHARINDEX('  ', @strDef) ))) END
--		SELECT @strDef
--		SELECT @str5 AS [Str5]
		SET @str1 = CASE WHEN  @str1 ='' THEN '' ELSE @str1 END
		SET @str2 = CASE WHEN  @str2 ='' THEN '' ELSE CONCAT(', ', @str2) END
		SET @str3 = CASE WHEN  @str3 ='' THEN '' ELSE CONCAT(', ', @str3) END
		SET @str4 = CASE WHEN  @str4 ='' THEN '' ELSE CONCAT(', ', @str4) END
		SET @str5 = CASE WHEN  @str5 ='' THEN '' ELSE CONCAT(', ', @str5) END
		SELECT CONCAT(@str1, @str2, @str3, @str4, @str5) AS DefaulterAddress


END
GO
