USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
		INSERT [dbo].[ContactAddresses] 
		(
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		)
		SELECT 
			[addressline1], 
			[addressline1], 
			[addressline2], 
			[addressline2], 
			[addressline3], 
			[addressline4], 
			[addressline5], 
			[Postcode], 
			[cDefaulterId], 
			1, 
			2, 
			231, 
			0 
		FROM 
			[oso].[stg_OneStep_ContactAddresses]
		WHERE 
			(Imported = 0	OR Imported IS NULL)
		EXCEPT
		SELECT 
			[firstLine], 
			[addressLine1], 
			[address], 
			[addressLine2], 
			[addressLine3], 
			[addressLine4], 
			[addressLine5], 
			[postCode], 
			[defaulterId], 
			[SourceId], 
			[StatusId], 
			[countryId], 
			[business]
		FROM 
			[dbo].[ContactAddresses]

		UPDATE [oso].[stg_OneStep_ContactAddresses]
		SET Imported = 1, ImportedOn = GetDate()
		WHERE (Imported = 0	OR Imported IS NULL)

		COMMIT TRANSACTION	
		SELECT
			1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
