USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AssignmentsHistory_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_AssignmentsHistory_DT]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @GroupName INT;
			SET @GroupName = (SELECT TOP 1 groupName FROM AssignedCasesHistory ORDER BY Id DESC) + 1;
			
			INSERT [dbo].[AssignedCasesHistory]
			(
				[caseId], 
				[officerId], 
				[assignmentDate], 
				[userId], 
				[teamId], 
				[groupName]
			)
			SELECT
				[cCaseId], 
				[COfficerId], 
				[assignmentDate], 
				[CUserId], 
				[teamId], 
				@GroupName
			FROM
				[oso].[stg_OneStep_AssignmentsHistory]
			WHERE Imported = 0

			UPDATE [oso].[stg_OneStep_AssignmentsHistory]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION				
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
