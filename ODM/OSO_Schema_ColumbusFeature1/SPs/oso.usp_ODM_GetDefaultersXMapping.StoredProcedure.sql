USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_ODM_GetDefaultersXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_ODM_GetDefaultersXMapping]
AS
BEGIN
	select [d].[Id] [cDefaulterId],d.name as DefaulterName, ocn.OldCaseNumber as CaseNumber
	from [Defaulters] [d]
	JOIN [DefaulterCases] [dc] on [d].[Id] = [dc].[defaulterId]
	JOIN [Cases] [c] ON [dc].[caseId] = [c].[Id]
	JOIN [OldCaseNumbers] [ocn] on [c].[Id] = [ocn].CaseId
END
GO
