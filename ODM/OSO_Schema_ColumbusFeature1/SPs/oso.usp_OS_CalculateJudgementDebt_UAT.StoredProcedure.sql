USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CalculateJudgementDebt_UAT]    Script Date: 30/06/2022 11:31:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CalculateJudgementDebt_UAT] @InitialJudgmentDebt decimal(18,4), @JudgmentCosts decimal(18, 4)
AS
BEGIN
	SELECT (@InitialJudgmentDebt - @JudgmentCosts) JudgmentDebt
END
GO
