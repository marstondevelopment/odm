USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentId]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Updated:		BP
-- Create date: 25-07-2018
-- Update date: 06-02-2020
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentId]
@CSchemePaymentName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @PaymentSchemeIds TABLE (PaymentSchemeId INT)

		INSERT INTO @PaymentSchemeIds (PaymentSchemeId)
		SELECT id FROM PaymentScheme
		WHERE [name] in ('Rossendales Generic Payment Scheme', 'Rossendales RTA Generic Payment Scheme')

		SELECT sp.ID AS CSchemePaymentId
		FROM SchemePayments sp		
		WHERE paymentSchemeId in (SELECT PaymentSchemeId FROM @PaymentSchemeIds) and sp.name = @CSchemePaymentName		
    END
END
GO
