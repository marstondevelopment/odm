USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[GetCaseStatusfromDB_UAT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[GetCaseStatusfromDB_UAT] 
@CCaseId int
AS
BEGIN
	SELECT 
		cs.name Status
	FROM
		Cases c 
		INNER JOIN CaseStatus cs ON c.statusId = cs.Id
	WHERE
		c.Id = @CCaseId
END
GO
