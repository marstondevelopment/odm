USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_CrossRef_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemePayment_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_SchemePayment_CrossRef] 
				(
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				)
				SELECT
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				FROM
					[oso].[stg_OneStep_SchemePayment_CrossRef]	 

			  update spx 
			  set spx.cschemepaymentid = new.id 
			  --select spx.*,new.id [newid]
			  from 
			   oso.OneStep_SchemePayment_Crossref spx
			  join schemepayments new on new.name = spx.cschemepaymentname AND 
			  new.paymentschemeid = (select id from paymentscheme where name = 'Rossendales Generic Payment Scheme')


			COMMIT TRANSACTION
			SELECT
				1 as Succeed
			
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
