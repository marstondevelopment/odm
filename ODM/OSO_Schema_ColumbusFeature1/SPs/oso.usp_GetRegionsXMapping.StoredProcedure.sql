USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_GetRegionsXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetRegionsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

	SELECT 
		[r].[Id] [RegionId],	
		[r].[Name] [Region]
		
	FROM
		[dbo].[Regions] [r]
	WHERE
		[r].[ParentId] IS NULL

    END
END
GO
