USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Assignments_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Assignments_DT]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @GroupName INT;
			SET @GroupName = (SELECT TOP 1 groupName FROM AssignedCases ORDER BY Id DESC);
						
			SELECT
				osa.[cCaseId], 
				a.[COfficerId], 
				osa.[AssignmentDate], 
				a.[CUserId], 
				a.[TeamId], 	
				@GroupName [grpName]
			INTO
				[#AssignedCasesTemp]
			FROM
				(SELECT cCaseId, MAX(AssignmentDate) AssignmentDate
				FROM oso.stg_OneStep_Assignments
				GROUP BY cCaseId) osa
				INNER JOIN oso.stg_OneStep_Assignments a ON osa.cCaseId = a.cCaseId and osa.AssignmentDate = a.AssignmentDate
			WHERE 
				Imported = 0
			UPDATE 
				[#AssignedCasesTemp]

			SET @GroupName = grpName = @GroupName + 1
			
			INSERT [dbo].[AssignedCases]
			(
				[caseId], 
				[officerId], 
				[assignmentDate], 
				[userId], 
				[teamId], 
				[groupName]
			)
			SELECT
				[cCaseId], 
				[COfficerId], 
				[assignmentDate], 
				[CUserId], 
				[teamId], 
				[grpName]
			FROM
				[#AssignedCasesTemp]	
			
			DROP TABLE [#AssignedCasesTemp]				
			
			UPDATE [dbo].[Cases] 
			SET [assignable] = 1 
			WHERE [id] in (SELECT DISTINCT cCaseId FROM [oso].[stg_OneStep_Assignments])

			UPDATE [oso].[stg_OneStep_Assignments]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION				
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
