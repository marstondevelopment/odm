USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseDetailsForClientName_UAT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Suhail Shaikh
-- Create date: 27-08-2021
-- Description:	Extract case details for client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseDetailsForClientName_UAT]
@ClientName nvarchar (500)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

	  SELECT c.clientCaseReference as ClientCaseReference, c.Id as CaseId   
	  FROM cases c with (nolock)
	  join Batches (nolock) b on c.batchId = b.Id
		join ClientCaseType (nolock) cct on cct.id = b.clientCaseTypeId
		join clients (nolock) cl on cct.clientId = cl.Id
      where cl.[name]IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
	  UNION
		SELECT 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 99999999 AS CaseId
	END
END
GO
