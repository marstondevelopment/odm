USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetDuplicatedEmailAddress]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetDuplicatedEmailAddress] @cDefaulterId int, @EmailAddress varchar(50)
AS
BEGIN
	select Top 1 [d].[Id] as [cDefaulterId], de.Email as cEmailAddress
	from [Defaulters] [d]
	Inner Join DefaulterEmails de on d.Id = de.defaulterId
	where [d].[Id] = @cDefaulterId
	AND de.Email = @EmailAddress
END
GO
