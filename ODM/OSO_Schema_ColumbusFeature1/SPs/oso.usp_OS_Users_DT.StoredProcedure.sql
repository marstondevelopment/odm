USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Users from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@firstName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@Department [varchar](200),
					@JobTitle [varchar](250),
					@marston [bit],
					@swift [bit],
					@rossendales [bit],
					@marstonLAA [bit],
					@rossendaleDCA [bit],
					@SwiftDCA [bit],
					@MarstonDCA [bit],
					@DCA [bit],
					@EXP [bit],
					@LAA [bit],
					@onestepid [int],
					@username [nvarchar] (50),
					@Existing [bit],
					
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BMarstonLAAId int, 
					@BRossendaleDCAId int,
					@BSwiftDCAId int,
					@BMarstonDCAId int,
					@DEPTID int,
					@OFFICEID int,
					@RoleId int,
					@Comment [varchar] (250),
					@UBCount int,
					@ColUserName [varchar] (50),
					@ColUserCount int
					
					
					
				SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
				SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
				SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
				SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
				SELECT @BRossendaleDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
				SELECT @BSwiftDCAId = Id From [Brands].[Brands] WHERE Name = 'Swift DCA'
				SELECT @BMarstonDCAId = Id From [Brands].[Brands] WHERE Name = 'Marston DCA'
				
				---- DEPARTMENT
				--INSERT INTO Departments ([name])
				--VALUES ('Rossendales')
				--SET @DEPTID = SCOPE_IDENTITY();
				
				-- OFFICE
				SELECT @OFFICEID = id FROM [dbo].[Offices] WHERE [Name] = 'Helmshore'
				--IF (@OFFICEID IS NULL)
				--BEGIN
				--	INSERT INTO Offices ([Name], [Firstline], [Postcode], [Address])
				--	VALUES ('Helmshore',NULL,NULL,NULL)
				--	SET @OFFICEID = SCOPE_IDENTITY();
				--END

				IF CURSOR_STATUS('global','cursorT')>=-1
				BEGIN
					DEALLOCATE cursorT
				END

                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
					[firstName],
					[lastName],
					[email],
					[Department],
					[JobTitle],
					[marston],
					[swift],
					[rossendales],
					[marstonLAA],
					[rossendaleDCA],
					[SwiftDCA],
					[MarstonDCA],
					[DCA],
					[EXP],
					[LAA],
					[onestepid],
					[username],
					[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Users]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
                    WHILE @@FETCH_STATUS = 0
                    BEGIN

							SELECT @DEPTID = Id FROM [dbo].[Departments] WHERE [name] = @Department
							SET @ColId = NULL
							IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColUsersXRef] WHERE OSId = @onestepId

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' +  @username;
									THROW 51000, @Comment, 163; 
								
								END								

							END
							ELSE
							BEGIN
									--Add new user here
									SET @ColUserName = @username
									SELECT @ColUserCount = COUNT(*) FROM dbo.Users WHERE name = @username
									IF (@ColUserCount > 0)
									BEGIN
										SET @ColUserName += '_os';  
									END

									INSERT INTO USERS (
									[name]
									, [password]
									, [firstName]
									, [lastName]
									, [email]
									, [departmentId]
									, [enabled]
									, [officeId]
									, [changePasswordOnLogin]
									, [passwordReset]
									, [defaultResultsExpected]
									, [sendEmailNotification]
									, [sendEmailNotificationEnabledOn]
									, [emailNotificationInterval])
									VALUES (
									@ColUserName
									,'1pF5kXsqfQdLufiK43b1Qw=='
									,@firstName
									,@lastName
									,@email
									,@DEPTID
									, 1
									, @OFFICEID
									, 1
									, 0
									, 1000
									, 0
									, NULL
									, 1)
									SET @ColId = SCOPE_IDENTITY();

							
						
									-- Add to XRef table
									INSERT INTO oso.[OSColUsersXRef]
										(
											[username],
											[OSId],
											[ColId]
										)

										VALUES
										(
											@username
											,@onestepid
											,@ColId
										)										
								
									-- Add OfficersBrands
								IF (@marston = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BMarstonId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BMarstonId
											)								
										END

									END
			
								IF (@rossendales = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BRossendalesId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BRossendalesId
											)							
										END

									END			
						
								IF (@swift = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BSwiftId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BSwiftId
											)					
										END

									END											
							END

							IF (@marstonLAA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonLAAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonLAAId
									)					
								END

							END	
								
							IF (@rossendaleDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BRossendaleDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendaleDCAId
									)					
								END

							END				

							IF (@SwiftDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BSwiftDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftDCAId
									)					
								END

							END				

							IF (@MarstonDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonDCAId
									)					
								END

							END				

							-- Add User ROLE
							IF (@JobTitle IS NOT NULL)
							BEGIN
								SELECT @RoleId = Id FROM Roles WHERE name = @JobTitle
								
								IF (@RoleId is null)
									BEGIN

										SET @Comment = 'RoleId not found for ' +  @JobTitle;
										THROW 51000, @Comment, 163;  
									END	
								DECLARE @UserRoleId int
								SELECT @UserRoleId = UserId From UserRoles WHERE UserId = @ColId AND RoleId = @RoleId

								IF (@UserRoleId IS NULL)
								BEGIN
									INSERT INTO dbo.[UserRoles]
										(
											[userId]
											,[roleId]
										)

										VALUES
										(
											@ColId
											,@RoleId									
										)		
								END
																	
							END	
					-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Users]
			      SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0		

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
