USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseTypeXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseTypeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	CaseType
	,CaseTypeId
  FROM [oso].[OSColCaseTypeXRef]
    END
END
GO
