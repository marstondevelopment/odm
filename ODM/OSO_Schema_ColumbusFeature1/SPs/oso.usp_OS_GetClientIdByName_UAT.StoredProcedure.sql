USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetClientIdByName_UAT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 19-04-2021
-- Description:	Extract client id by name
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientIdByName_UAT]
@ClientName nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		
		Select top 1 c.Id as ClientId, c.name As ClientName	  
		From Clients c 
		Where c.[name] = @ClientName
		UNION
		SELECT 99999999 AS ClientId, 'XXXXXXXXXXXXXXXX' AS ClientName

    END
END
GO
