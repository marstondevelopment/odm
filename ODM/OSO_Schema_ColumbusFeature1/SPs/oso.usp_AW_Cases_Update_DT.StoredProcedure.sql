USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_AW_Cases_Update_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_AW_Cases_Update_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE 
			@CaseId int, 
			@CasePrefix varchar(1),
			@StatusId int,
			@TitleLiable1 varchar(50),
			@FirstnameLiable1 varchar(50),	
			@LastnameLiable1 varchar(50),
			@TitleLiable2 varchar(50),
			@FirstnameLiable2 varchar(50),
			@LastnameLiable2  varchar(50)

            ,@StageId        INT

			,@Succeed bit
			,@ErrorId INT
			,@CStatusId int
			,@UserId int = 2

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorAW')>=-1
		BEGIN
			DEALLOCATE cursorAW
		END

        DECLARE cursorAW CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
		SELECT 
			CaseId, 
			CasePrefix,
			CASE CasePrefix 
				WHEN 'H' THEN 6 
				WHEN 'W' THEN 7		 
				--WHEN 'C' THEN 16		
			END StatusId,
			TitleLiable1, 
			FirstnameLiable1, 
			LastnameLiable1, 
			TitleLiable2,
			FirstnameLiable2, 
			LastnameLiable2 	
		FROM 
			[oso].[stg_AW_Cases_Update]
		WHERE 
			Imported = 0 
			AND ErrorId IS NULL

	OPEN cursorAW
	FETCH NEXT
	FROM
		cursorAW
	INTO
		@CaseId, 
		@CasePrefix,
		@StatusId,
		@TitleLiable1,
		@FirstnameLiable1,	
		@LastnameLiable1,
		@TitleLiable2,
		@FirstnameLiable2,
		@LastnameLiable2
	WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION

					--UPDATE CASE WHEN RECOMMENCED
					IF(@CasePrefix in ('C'))
					BEGIN
						UPDATE dbo.Cases
						SET 
							statusId = previousStatusId, 
							statusDate = GETDATE(),					
							previousStatusId = statusId,
							previousStatusDate = statusDate	
						FROM Cases
						WHERE Id = @caseId AND statusId = 6

						--UPDATE HOLD CASES
						UPDATE dbo.HoldCases
						SET
							recommencedOn = GETDATE()
						FROM dbo.HoldCases AS hc
						WHERE hc.caseId = @CaseId
						AND hc.recommencedOn IS NULL

						--INSERT CASEHISTORY RECORD
						INSERT INTO dbo.CaseHistory
						(
							caseId,
							userId,
							officerId,
							comment,
							occured,
							typeId,
							CaseNoteId
						)
						VALUES
						(
							@CaseId,
							2,
							NULL,
							'RecommenceCase: CaseNumber = ' + (select caseNumber from dbo.Cases where Id = @CaseId),
							GETDATE(),
							4,
							NULL
						)

					END

					--UPDATE CASE WHEN IS PLACED ON-HOLD
					IF(@CasePrefix in ('H'))
					BEGIN
						UPDATE dbo.Cases
						SET 
							previousStatusId = statusId,
							previousStatusDate = statusDate,	
							statusId = @StatusId, 
							statusDate = GETDATE()					
						FROM Cases
						WHERE Id = @caseId and statusId in (select Id from dbo.CaseStatus where name in ('Unpaid','Part-Paid','Goods Removed','Tracing'))

						--INSERT HOLDCASES RECORD
						INSERT INTO dbo.HoldCases
						(							 
							[caseId],							 
							[holdUntil], 
							[recommencedOn], 
							[holdReason], 
							[recommenceReason],							
							[holdReasonId], 
							[AmendedByHoldCaseId]
						)
						VALUES
						(
							@caseId,
							getdate(),
							NULL,
							NULL,
							NULL,
							2,
							NULL
						)

						--INSERT CASEHISTORY RECORD
						INSERT INTO dbo.CaseHistory
						(
							caseId,
							userId,
							officerId,
							comment,
							occured,
							typeId,
							CaseNoteId
						)
						VALUES
						(
							@CaseId,
							2,
							NULL,							
							'Case status changed to: On-Hold from: ' + convert(varchar(50),(select name from CaseStatus where id =(select previousStatusId from dbo.Cases where Id = @CaseId))),
							GETDATE(),
							3,
							NULL
						)
					END

					IF(@CasePrefix in ('W'))
					BEGIN
						UPDATE dbo.Cases
						SET 
							previousStatusId = statusId,
							previousStatusDate = statusDate,	
							statusId = @StatusId, 
							statusDate = GETDATE()					
						FROM Cases
						WHERE Id = @caseId

						INSERT INTO [dbo].[ReturnedCases]
						(
							[CaseId], 
							[DrakeReturnCodeId], 
							[UserId], 
							[Reason] 
						
						)
						VALUES
						(
							@caseId,
							63,
							2,
							'Case return - Case out of time'
						)

						--INSERT CASEHISTORY RECORD
						INSERT INTO dbo.CaseHistory
						(
							caseId,
							userId,
							officerId,
							comment,
							occured,
							typeId,
							CaseNoteId
						)
						VALUES
						(
							@CaseId,
							2,
							NULL,							
							'Case status changed to: Pending Return from: ' + convert(varchar(50),(select name from CaseStatus where id =(select previousStatusId from dbo.Cases where Id = @CaseId))),
							GETDATE(),
							5,
							NULL
						)	
					END

					IF(@CasePrefix = 'U')
					BEGIN
						UPDATE dbo.Cases
						SET 				
							debtName = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1))
						FROM dbo.Cases
						WHERE Id = @caseId 

						-- Update Lead Defaulter Name
						UPDATE dbo.Defaulters
						SET	
							name = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1)),
							firstName = @FirstnameLiable1,
							lastName = @LastnameLiable1
						from 
							dbo.Defaulters 
							join dbo.DefaulterCases dc on dbo.Defaulters.Id = dc.defaulterId		
						where 
						dc.caseId = @caseId
						AND isLeadCustomer = 1

						--Update Second Defaulter Name
						DECLARE @DefaulterId int;
						
						SELECT TOP 1 @DefaulterId = dc.defaulterId  
						FROM DefaulterCases dc
						INNER JOIN Defaulters d on d.Id = dc.defaulterId
						WHERE dc.caseId = @CaseId AND isLeadCustomer = 0
						ORDER BY dc.defaulterId ASC

						IF (@FirstnameLiable2 IS NOT NULL OR @LastnameLiable2 IS NOT NULL)
						BEGIN 
							IF (@DefaulterId IS NOT NULL )
							BEGIN
								--Update 2nd defaulter
								UPDATE dbo.Defaulters
								SET	
									name = (select Concat(@TitleLiable2,' ', @FirstnameLiable2, ' ', @LastnameLiable2)),
									firstName = @FirstnameLiable2,
									lastName = @LastnameLiable2
								from 
									dbo.Defaulters 		
								where 
								Id = @DefaulterId						
							END
							--ELSE
							--BEGIN
							----Create 2nd defaulter
							--END

						END
					END		


					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[stg_AW_Cases_Update]
					SET
					Imported   = 1
					, ImportedOn = GetDate()
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND CaseId = @CaseId;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_AW_Cases_Update]
					SET    ErrorId = @ErrorId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND CaseId = @CaseId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorAW
            INTO
				@caseId, 
				@CasePrefix,
				@StatusId,
				@TitleLiable1,
				@FirstnameLiable1,	
				@LastnameLiable1,
				@TitleLiable2,
				@FirstnameLiable2,
				@LastnameLiable2	
        END
        CLOSE cursorAW
        DEALLOCATE cursorAW

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
