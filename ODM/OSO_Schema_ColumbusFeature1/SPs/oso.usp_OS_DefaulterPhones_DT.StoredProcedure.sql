USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_DefaulterPhones_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_DefaulterPhones_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterPhones] 
			(
				[defaulterId], 
				[phone], 
				[TypeId],		 
				[LoadedOn], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[TelephoneNumber],
				[TypeID],		
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterPhones]
			WHERE 
				(Imported = 0 OR Imported IS NULL)
			EXCEPT
			SELECT 
				[defaulterId], 
				[phone], 
				[TypeId], 		
				[LoadedOn], 
				[SourceId]		
			FROM 
				[dbo].[DefaulterPhones]

			UPDATE [oso].[stg_OneStep_DefaulterPhones]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE (Imported = 0 OR Imported IS NULL)

			COMMIT TRANSACTION	
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
