USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetColCaseNumbersXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetColCaseNumbersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

SELECT c.caseNumber as CaseNumber, ocn.OldCaseNumber AS OurRef 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		END
END
GO
