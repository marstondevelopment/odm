USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ContactAddresses_CrossRef_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_ContactAddresses]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
GO
