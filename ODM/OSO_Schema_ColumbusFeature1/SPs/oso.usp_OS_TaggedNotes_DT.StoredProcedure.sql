USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
					---- Insert case notes
					--INSERT INTO dbo.[CaseNotes] 
					--( [caseId]
					--	, [userId]
					--	, [officerId]
					--	, [text]
					--	, [occured]
					--	, [visible]
					--	, [groupId]
					--	, [TypeId]
					--)
					--SELECT
					--	[CCaseId],
					--	[CUserId],
					--	NULL,
					--	[CaseNote],
					--	[DateAdded],
					--	1,
					--	NULL,
					--	NULL
					--FROM
					--	[oso].[stg_Onestep_PhsTwo_TaggedNotes] 
					--WHERE 
					--	Imported = 0 AND CaseNote IS NOT NULL AND CaseNote <>''

					--UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					--SET Imported = 1, ImportedOn = GetDate()
					--FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					--WHERE otn.Imported = 0 AND otn.CaseNote IS NOT NULL AND otn.CaseNote <>''

					-- Update existing tagged note
					UPDATE [dbo].AdditionalCaseData 
					SET 
						[Value] = otn.TagValue,
						[LoadedOn] = otn.DateAdded
					FROM [dbo].AdditionalCaseData ac
					INNER JOIN [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL
				
					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					INNER JOIN [dbo].AdditionalCaseData ac ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL

					-- Insert new tagged notes
					INSERT INTO [dbo].AdditionalCaseData (CaseId,[Name],[Value],LoadedOn)
					SELECT DISTINCT cCaseId,TagName,MAX(TagValue),MAX(DateAdded)
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL
					GROUP BY cCaseId, TagName

					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
