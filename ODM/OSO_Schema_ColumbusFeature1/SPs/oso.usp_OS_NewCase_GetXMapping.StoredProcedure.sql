USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId,	cs.name as CStatus 	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		  inner join dbo.CaseStatus cs on c.statusId = cs.Id
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId, 'XXXXXX' AS CStatus
    END
END
GO
