USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_SplitString]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  VJ
-- Create date: 20-07-2020
-- Description: Split the string to a size and insert the results to CaseNotes table.
-- =============================================

CREATE PROCEDURE  [oso].[usp_SplitString]
(
     @Str NVARCHAR(Max),
	 @Split INT,
	 @RemoveFirstRow bit = 1
)

AS

BEGIN 
DECLARE @End INT
DECLARE @rowNo INT = 1

declare @RowSplitTable table
(
  Id INT IDENTITY(1,1)   NOT NULL, 
  Content Nvarchar(max)
)

declare @FinalRowTable table
(
  Id INT  NOT NULL, 
  Content Nvarchar(max)
)

WHILE (LEN(@Str) > 0)
BEGIN
    IF (LEN(@Str) > @Split)
    BEGIN
        SET @End = LEN(LEFT(@Str, @Split)) - CHARINDEX(' ', REVERSE(LEFT(@Str, @Split)))
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(LEFT(LEFT(@Str, @Split), @End + 1))))
        SET @Str = SUBSTRING(@Str, @End + 2, LEN(@Str))
    END
    ELSE
    BEGIN
        INSERT INTO @RowSplitTable VALUES (RTRIM(LTRIM(@Str)))
        SET @Str = ''
    END
END


IF @RemoveFirstRow = 1

BEGIN

INSERT INTO @FinalRowTable
SELECT id, Content FROM (
        SELECT ROW_NUMBER() OVER(ORDER BY id) AS RoNum
              , id , Content
        FROM @RowSplitTable 
) AS tbl 
WHERE @rowNo < RoNum
ORDER BY tbl.Id 

END 
ELSE 
BEGIN 

INSERT INTO @FinalRowTable SELECT *  FROM @RowSplitTable

END


SELECT s.Content,s.Id
FROM @FinalRowTable s

END
GO
