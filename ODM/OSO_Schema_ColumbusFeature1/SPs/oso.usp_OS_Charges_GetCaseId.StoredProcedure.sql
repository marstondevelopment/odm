USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Charges_GetCaseId]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as CCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO
