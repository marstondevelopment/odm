USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[GetContributions_ChangesAsString]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[GetContributions_ChangesAsString] @CCaseId int
AS
BEGIN
With AdditionalCaseDataCTE as
(
	Select acd.[Value] offence_number, acd.CaseId FROM AdditionalCaseData as acd 
	WHERE acd.CaseId = @CCaseId AND acd.[Name] = 'SummonsNum'
),
DefaulterDetailsCTE as
(
	select dd.nino empNI, c2.Id CaseId from defaulterdetails dd  
	join Defaulters d on dd.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId
),
DefaulterPhonesCTE as
(
	 
	Select Top(1)  
	dp.phone add_phone, dp.phone add_fax, c2.Id CaseId from defaulterphones dp  
	join Defaulters d on dp.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId
	AND dp.typeId IN (select Id from defaulterphonetypes where name in ('home','mobile'))  --???
),

DefaulterEmailsCTE as
(
	select de.email addEmail, c2.Id CaseId from defaulteremails de  
    join Defaulters d on de.defaulterid = d.id  
    join DefaulterCases dc on dc.defaulterid = d.id  
    join Cases c2 on dc.caseid = c2.id  
    WHERE c2.Id = @CCaseId     
),
ContactAddressesCTE as
(
	select ca.postcode addpostcode, ca.CanonicalAddress address, c2.Id CaseId from contactaddresses ca  
	join Defaulters d on ca.defaulterid = d.id  
	join DefaulterCases dc on dc.defaulterid = d.id  
	join Cases c2 on dc.caseid = c2.id  
	WHERE c2.Id = @CCaseId AND ca.IsPrimary=1 
)

select 
	CONCAT(
	c.id,
	'|',ISNULL(acd.offence_number, ''),  
	'|',FORMAT(c.issueDate, 'dd/MM/yyyy hh:mm'),  
	'|',fd.ClientOutstandingBalance,  
	'|',fd.costs,  
	'|',c.originalBalance,
	'|',ISNULL(dd.empNI, '') , 
	'|',ISNULL(dp.add_phone, '') ,
	'|',ISNULL(dp.add_fax, '') ,
	'|',ISNULL(de.addEmail, '') ,               
	'|',c.ClientDefaulterReference, 
	'|',ISNULL(ca.addpostcode, '') ,
	'|',ISNULL(ca.address, '') ,              
	'|',cli.Id)  
from cases c   
	inner join batches b on c.batchid = b.id  
	inner join clientcasetype cct on b.clientcasetypeid = cct.id  
	inner join clients cli on cct.clientid = cli.id  
	inner join vw_FinancialDetails as fd ON fd.Id=c.Id	
	left outer join AdditionalCaseDataCTE acd ON c.Id = acd.CaseId 
	left outer join DefaulterDetailsCTE dd on c.Id = dd.CaseId
	left outer join DefaulterPhonesCTE dp on c.Id = dp.CaseId	
	left outer join DefaulterEmailsCTE de on c.Id = de.CaseId
	left outer join ContactAddressesCTE ca on c.Id = ca.CaseId
where 
	c.id= @CCaseId
END
GO
