USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOfficerTypesCrossRef]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetOfficerTypesCrossRef]
AS
BEGIN
	SELECT Id typeId, abbreviation OfficerType FROM OfficerTypes 
	WHERE isDeleted = 0
END

GO
