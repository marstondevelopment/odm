USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_GetAreasXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetAreasXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		--SELECT DISTINCT	
		--	[a].[Id] [AreaId],
		--	[a].[Name] [Area]		
		--FROM
		--	[dbo].[Regions] [r]
		--	INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
		--	INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		SELECT 
			[Id] [AreaId], 
			[Name] [Area] 
		FROM 
			[Regions] 
		WHERE 
			[ParentId] IN (select [Id] from [Regions] where [ParentId] IS NULL)
    END
END
GO
