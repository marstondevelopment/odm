USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Adjustment_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Adjustment_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId        INT
                    , @Amount          DECIMAL(18,4)
                    , @UserId          INT
                    , @OfficerId          INT
                    , @AddedOn         DATETIME
                    , @PaymentId       INT
                    , @PaymentStatusId INT
                    , @SchemePaymentId INT
                    , @CaseNoteId      INT
                    , @HistoryTypeId   INT

				SELECT
					   @PaymentStatusId = [Id]
				FROM
					   [dbo].[PaymentStatus]
				WHERE
					   [name] = 'BALANCE ADJUSTMENT'
				SELECT
					   @SchemePaymentId = [Id]
				FROM
					   [dbo].[SchemePayments]
				WHERE
					   [name] = 'Balance Adjustment'
				SELECT
					   @HistoryTypeId = [Id]
				FROM
					   [dbo].[HistoryTypes]
				WHERE
					   [name] = 'UpdateClientBalance'

                DECLARE cursorT
                CURSOR
                    --LOCAL STATIC
                    --LOCAL FAST_FORWARD
                    --LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT top 200000
						CCaseId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId
                    FROM
                        oso.[Staging_OS_Adjustments]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@CaseId,
						@Amount,
						@AddedOn,
						@UserId,
						@OfficerId
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        -- Add new rows
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]
                                    , [transactionDate]
                                    , [amount]
                                    , [clearedOn]
                                    , [bouncedOn]
                                    , [recordedBy]
                                    , [officerId]
                                    , [REFERENCE]
                                    , [authorisationNumber]
                                    , [journalStatusId]
                                    , [receiptTypeId]
                                    , [trusted]
                                    , [schemePaymentId]
                                    , [paymentStatusId]
                                    , [surchargeAmount]
                                    , [paymentID]
                                    , [reconciledOn]
                                    , [reconcileReference]
                                    , [reversedOn]
                                    , [parentId]
                                    , [previousPaymentStatusId]
                                    , [reconciledBy]
                                    , [keepGoodsRemovedStatus]
                                    , [cardStatementReference]
                                    , [bankReconciledOn]
                                    , [bounceReference]
                                    , [reconciledBouncedOn]
                                    , [reconciledBouncedBy]
                                    , [exceptionalAdjustmentId]
                                    , [bankReconciledBy]
                                    , [isReversedChargePayment]
                                    , [paymentOriginId]
                                    , [uploadedFileReference]
                                    , [ReverseOrBounceNotes]
                               )
                               VALUES
                               ( @AddedOn
                                    , @AddedOn
                                    , @Amount
                                    , NULL
                                    , NULL
                                    , @UserId
                                    , @OfficerId
                                    , NUll
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 1
                                    , @SchemePaymentId
                                    , @PaymentStatusId
                                    , 0.00
                                    , 'MigratedPayment'
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , NULL
                                    , 0
                                    , 0
                                    , 0
                                    , 0
                               )
                        ;
                        
                        -- Add new Case Notes
                        INSERT INTO dbo.[CaseNotes]
                               ( [caseId]
                                    , [officerId]
                                    , [userId]
                                    , [text]
                                    , [occured]
                                    , [visible]
                                    , [groupId]
                                    , [TypeId]
                               )
                               VALUES
                               ( @CaseId
                                    , @OfficerId
                                    , @UserId
                                    , 'Add balance adjustment of ' + FORMAT(@Amount,'N2', 'en-US') 
                                    , @AddedOn
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @CaseNoteId = SCOPE_IDENTITY();
                        -- Add new Case History
                        INSERT INTO dbo.[CaseHistory]
                               ( [caseId]
                                    , [userId]
                                    , [officerId]
                                    , [COMMENT]
                                    , [occured]
                                    , [typeId]
                                    , [CaseNoteId]
                               )
                               VALUES
                               ( @CaseId
                                    , @UserId
                                    , @OfficerId
                                    , 'Balance adjustment has been completed. Adjustment amount = ' + FORMAT(@Amount,'N2', 'en-US')  + '.'
                                    , @AddedOn
                                    , @HistoryTypeId
                                    , @CaseNoteId
                               )
                        ;
                        
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
							@CaseId,
							@Amount,
							@AddedOn,
							@UserId,
							@OfficerId
                    END
                    CLOSE cursorT
                    DEALLOCATE cursorT 

					UPDATE [oso].[Staging_OS_Adjustments]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0	

					COMMIT TRANSACTION
					SELECT 1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT 0 as Succeed
                END CATCH
            END
        END
GO
