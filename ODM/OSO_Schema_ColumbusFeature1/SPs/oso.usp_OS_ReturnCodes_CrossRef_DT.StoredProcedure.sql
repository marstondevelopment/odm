USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCodes_CrossRef_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_ReturnCodes_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_ReturnCode_CrossRef] 
				(
					CReturnCode
					,OSReturnCode
				)
				SELECT
					CReturnCode
					,OSReturnCode
				FROM
					[oso].[stg_OneStep_ReturnCode_CrossRef]	 
			COMMIT TRANSACTION
			
			BEGIN TRANSACTION
				UPDATE rcc 
				SET rcc.CId = drc.id
				FROM
					oso.[OneStep_ReturnCode_CrossRef] rcc
				INNER JOIN DrakeReturnCodes drc on drc.code = rcc.CReturnCode
			COMMIT TRANSACTION

			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO
