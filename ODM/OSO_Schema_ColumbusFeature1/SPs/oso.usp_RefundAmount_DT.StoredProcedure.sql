USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_RefundAmount_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_RefundAmount_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY

		DECLARE 
			@CaseNumber VARCHAR(7),
			@RefundAmount DECIMAL(18,4),
			@ErrorId int,
			@Succeed BIT,
			@GroupName VARCHAR(30) = '',
			@AutoResolveExceptionalBalances BIT = 0,
			@ClientRemitted DECIMAL(18, 4),
			@MarstonRemitted DECIMAL(18, 4),
			@UnRemitted DECIMAL(18, 4),
			@RefundId INT = 0,
			@ExceptionalBalanceId INT = 0,
			@CaseId INT,
			@RefundClientRemitted DECIMAL(18, 4) = 0,
			@RefundMarstonRemitted DECIMAL(18 ,4) = 0,
			@RefundUnRemitted DECIMAL(18, 4) = 0;

		SET @Succeed = 1

		IF CURSOR_STATUS('global','cursorAW')>=-1
		BEGIN
			DEALLOCATE cursorAW
		END

        DECLARE cursorAW CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
		SELECT 
			[CaseNumber], 
			[RefundAmount]
		FROM
                 [oso].[Staging_RefundAmounts]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

	OPEN cursorAW
	FETCH NEXT
	FROM
		cursorAW
	INTO
		@CaseNumber,
		@RefundAmount
	WHILE @@FETCH_STATUS = 0
        BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

					-- Insert from staging table into [MDST].[LAA_Refunds] table
					INSERT [MDST].[LAA_Refunds](
						[CaseNumber], 
						[RefundAmount]
						)
					VALUES
					(
						@CaseNumber,
						@RefundAmount			
					)
					

					-- Stage 2
					SET @CaseId = (SELECT Id FROM dbo.Cases WHERE caseNumber = @CaseNumber)					
					
					SELECT @ClientRemitted = SUM(cp.PaidClient)
					FROM dbo.CasePayments AS cp
					JOIN dbo.Payments AS p ON p.Id = cp.paymentId
					JOIN dbo.SchemePayments AS sp ON sp.Id = p.schemePaymentId
					JOIN dbo.Cases AS c ON c.Id = cp.caseId
					WHERE c.caseNumber = @CaseNumber
					AND sp.Id NOT IN (1, 2, 3) --NOT DIRECT PAYMENTS, BALANCE ADJUSTMENTS, EXCEPTIONAL ADJUSTMENTS

					SELECT @MarstonRemitted = SUM(cp.PaidEnforcementAgency)
					FROM dbo.CasePayments AS cp
					JOIN dbo.Payments AS p ON p.Id = cp.paymentId
					JOIN dbo.SchemePayments AS sp ON sp.Id = p.schemePaymentId
					JOIN dbo.Cases AS c ON c.Id = cp.caseId
					WHERE c.caseNumber = @CaseNumber
					AND sp.Id NOT IN (1, 2, 3) --NOT DIRECT PAYMENTS, BALANCE ADJUSTMENTS, EXCEPTIONAL ADJUSTMENTS
					
					SELECT @UnRemitted = SUM(IIF(sp.Id = 157, 0, cp.amount)) - SUM(cp.PaidClient) - SUM(cp.PaidEnforcementAgency)
					FROM dbo.CasePayments AS cp
					JOIN dbo.Payments AS p ON p.Id = cp.paymentId
					JOIN dbo.SchemePayments AS sp ON sp.Id = p.schemePaymentId
					JOIN dbo.Cases AS c ON c.Id = cp.caseId
					WHERE c.caseNumber = @CaseNumber
					AND sp.Id NOT IN (1, 2, 3) --NOT DIRECT PAYMENTS, BALANCE ADJUSTMENTS, EXCEPTIONAL ADJUSTMENTS


					--SET AMOUNTS FOR REFUND
					IF @RefundAmount <= @UnRemitted
					BEGIN

						SET @RefundUnRemitted = @RefundAmount
						SET @RefundClientRemitted = 0
						SET @RefundMarstonRemitted = 0

					END

					IF @RefundAmount > @UnRemitted
					BEGIN						
						SET @RefundUnRemitted = @UnRemitted

						DECLARE @RemainingRefundAmount DECIMAL(18, 4) = @RefundAmount - @RefundUnRemitted

						SET @RefundMarstonRemitted = CASE
								WHEN @MarstonRemitted < 0 THEN 0
								WHEN @MarstonRemitted > @RemainingRefundAmount THEN @RemainingRefundAmount
								ELSE @MarstonRemitted 
							END

						SET @RemainingRefundAmount = CASE
								WHEN @MarstonRemitted > @RefundMarstonRemitted THEN 0
								ELSE @RemainingRefundAmount - @RefundMarstonRemitted
							END

						SET @RefundClientRemitted = @RemainingRefundAmount
					END
	 
								   

					--CREATE REFUND
					SET @GroupName = 'LAA_' + REPLACE(CONVERT(VARCHAR(8), GETDATE(),101),'/','') + REPLACE(CONVERT(VARCHAR(6), GETDATE(),108),':','') + '_' + @CaseNumber	

					INSERT INTO [dbo].[Refunds]
							   ([refundReasonId]
							   ,[amount]
							   ,[personName]
							   ,[personAddress]
							   ,[caseId]
							   ,[status]
							   ,[refundDate]
							   ,[refundTypeId]
							   ,[methodOfPayment]
							   ,[returnToRefundRequester]
							   ,[cardHolderPhone]
							   ,[createdBy]
							   ,[reference]
							   ,[completedBy]
							   ,[completedOn]
							   ,[Processed]
							   ,[refundFineAmount]
							   ,[refundChargeAmount]
							   ,[approvedBy]
							   ,[approvedOn]
							   ,[rejectedBy]
							   ,[rejectedOn]
							   ,[cancelledBy]
							   ,[cancelledOn]
							   ,[personAddressFirstLine]
							   ,[personAddressPostCode]
							   ,[groupName]
							   ,[RefundUnremittedFundsAmount])
					SELECT
						5, --Client Request
						@RefundAmount,
						'',
						'',
						c.Id,
						4, --Completed
						GETDATE(),
						2, --Client Request
						5, --External
						0,
						NULL,
						2,
						NULL,
						2,
						GETDATE(),
						1,
						@RefundClientRemitted,
						@RefundMarstonRemitted,
						2,
						GETDATE(),
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						@GroupName,
						@RefundUnRemitted
					FROM dbo.Cases AS c
					WHERE c.caseNumber = @CaseNumber

					SET @RefundId = SCOPE_IDENTITY()


					--INSERT CASE HISTORY
					INSERT INTO [dbo].[CaseHistory]
							   ([caseId]
							   ,[userId]
							   ,[officerId]
							   ,[comment]
							   ,[occured]
							   ,[typeId]
							   ,[CaseNoteId])
					VALUES
					(
						@CaseId,
						2,
						NULL,
						'Refund requested: Refund Type: Client Request, Refund Reason: Client Request, amount: ' + format(convert(numeric(18,2), @RefundAmount), '0.00') + ' GBP.',
						GETDATE(),
						43,
						NULL
					)
					,(
						@CaseId,
						2,
						NULL,
						'Refund request approved.',
						DATEADD(SECOND, 1, GETDATE()),
						44,
						NULL
					)
					,(
						@CaseId,
						2,
						NULL,
						'Refund completed. Payment made by external.',
						DATEADD(SECOND, 2, GETDATE()),
						46,
						NULL
					)


					--CREATE EXCEPTIONAL BALANCE
					IF @RefundClientRemitted > 0 OR @RefundMarstonRemitted > 0
					BEGIN
						INSERT INTO [eb].[ExceptionalBalanceReasons]
								   ([CasePaymentId]
								   ,[RefundId]
								   ,[TransferredCasePaymentId]
								   ,[OriginalClientAmount]
								   ,[OriginalEAAmount]
								   ,[OccurredOn]
								   ,[CaseId]
								   ,[IsMigrated])
						VALUES
						(
							NULL,
							@RefundId,
							NULL,
							@RefundClientRemitted,
							@RefundMarstonRemitted+@RefundUnRemitted,
							GETDATE(),
							@CaseId,
							0
						)
						
						SET @ExceptionalBalanceId = SCOPE_IDENTITY()


						--NOTE:  AUTO RESOLVE CAN ONLY BE USED IN SITUATIONS WHERE RECLAIM WITHOUT CPR IS TO BE USED
						IF @RefundClientRemitted > 0 AND @AutoResolveExceptionalBalances = 1
						BEGIN
							INSERT INTO [eb].[ExceptionalBalanceResolutions]
								   ([ExceptionalBalanceReasonId]
								   ,[CasePaymentId]
								   ,[Amount]
								   ,[TypeId]
								   ,[HolderId]
								   ,[CompletedOn]
								   ,[OccurredOn]
								   ,[AssociationId]
								   ,[CancelledOn])
							VALUES
							(
								@ExceptionalBalanceId,
								NULL,
								@RefundClientRemitted,
								3, --RECLAIM WITHOUT CPR
								2,
								GETDATE(),
								GETDATE(),
								NULL,
								NULL
							)
						END

						--NOTE:  AUTO RESOLVE CAN ONLY BE USED IN SITUATIONS WHERE RECLAIM WITHOUT CPR IS TO BE USED
						IF @RefundMarstonRemitted > 0 AND @AutoResolveExceptionalBalances = 1
						BEGIN
							INSERT INTO [eb].[ExceptionalBalanceResolutions]
								   ([ExceptionalBalanceReasonId]
								   ,[CasePaymentId]
								   ,[Amount]
								   ,[TypeId]
								   ,[HolderId]
								   ,[CompletedOn]
								   ,[OccurredOn]
								   ,[AssociationId]
								   ,[CancelledOn])
							VALUES
							(
								@ExceptionalBalanceId,
								NULL,
								@RefundMarstonRemitted,
								3, --RECLAIM WITHOUT CPR
								1,
								GETDATE(),
								GETDATE(),
								NULL,
								NULL
							)
						END

						IF @AutoResolveExceptionalBalances = 0
						BEGIN
							UPDATE dbo.Cases
							SET IsLocked = 1
							FROM dbo.Cases
							WHERE Id = @CaseId
						END

					END

					COMMIT TRANSACTION
					Update MDST.LAA_Refunds set Processed=1 where CaseNumber=@CaseNumber
					
			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_RefundAmounts]
					SET    ErrorId = @ErrorId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					--AND CaseId = @CaseId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorAW
            INTO
				@CaseNumber,
				@RefundAmount	
        END
        CLOSE cursorAW
        DEALLOCATE cursorAW
		UPDATE
			[oso].[Staging_RefundAmounts]
		SET    
			Imported = 1, 
			ImportedOn = GETDATE()
		WHERE
			(Imported = 0 OR Imported IS NULL)
			AND ErrorId is NULL
    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
