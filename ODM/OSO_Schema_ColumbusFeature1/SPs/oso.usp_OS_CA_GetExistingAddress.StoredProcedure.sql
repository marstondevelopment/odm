USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_CA_GetExistingAddress]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_CA_GetExistingAddress] @cDefaulterId int, @AddressLine1 varchar(80), @Postcode varchar(12) 
AS
BEGIN


	SELECT TOP 1 
		CanonicalAddress
	FROM
		ContactAddresses ca
	WHERE
		ca.defaulterId = @cDefaulterId and ca.CanonicalAddress = [dbo].[udf_GetCanonicalAddress](@AddressLine1,@Postcode)
END
GO
