USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_PREWRIT_Cases_DT_UAT]    Script Date: 30/06/2022 12:25:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  BP
-- Create date: 16-05-2022
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_PREWRIT_Cases_DT_UAT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
  --      BEGIN TRANSACTION
  --      -- 1) Create batches for all import cases group by clientid and BatchDate		
		--EXEC oso.usp_OS_AddBatches;
		--COMMIT TRANSACTION
				
        DECLARE 
			@ClientCaseReference varchar(200),
			@TitleLiable1 varchar(50),
			@FullnameLiable1 nvarchar(150),
			@DOBLiable1 datetime,
			@DebtAddress1 nvarchar(80) ,
			@DebtAddress2 nvarchar(320),
			@DebtAddress3 nvarchar(100),
			@DebtAddress4 nvarchar(100),
			@DebtAddress5 nvarchar(100),
			@DebtAddressPostcode varchar(12) ,
			@Phone1Liable1 varchar(50),
			@Phone2Liable1 varchar(50),
			@Phone3Liable1 varchar(50),
			@ClientDefaulterReference varchar(30),			
			@IssueDate datetime,
			@FineAmount decimal(18,4) ,
			@IssuingCourt nvarchar(100),
			@Add1Liable1 nvarchar(80),
			@Add2Liable1 nvarchar(320),
			@Add3Liable1 nvarchar(100),
			@Add4Liable1 nvarchar(100),
			@Add5Liable1 nvarchar(100),
			@AddPostCodeLiable1 varchar(12) ,
			@IsBusinessAddress bit ,
			@StartDate datetime,
			@EndDate datetime,
			@ClientComments nvarchar (500),
			@Email1Liable1 varchar(250),
			@EmployerTelephone varchar (50),
			@OffenceCode varchar(50),
			@OffenceDescription nvarchar(1000),
			@OffenceLocation nvarchar (150),
			@DebtAddressCountry int,
			@VehicleVRM varchar(7),
			@VehicleMake varchar(50),
			@VehicleModel varchar(50),
			@TECDate datetime,
			@Currency int,
			@FirstnameLiable1 nvarchar(50),
			@MiddlenameLiable1 nvarchar(50),
			@LastnameLiable1 nvarchar(50),
			@CompanyNameLiable1 nvarchar(50),
			@NINOLiable1 varchar(13),
			@MinorLiable1 bit,
			@AddCountryLiable1 int,
			@Phone4Liable1 varchar(50),
			@Phone5Liable1 varchar(50),
			@Email2Liable1 varchar(250),
			@Email3Liable1 varchar(250),
			@TitleLiable2 varchar(50),
			@FirstnameLiable2 nvarchar(50),
			@MiddlenameLiable2 nvarchar(50),
			@LastnameLiable2 nvarchar(50),
			@FullnameLiable2 nvarchar(150),
			@CompanyNameLiable2 nvarchar(50),
			@DOBLiable2 datetime,
			@NINOLiable2 varchar(13),
			@MinorLiable2 bit,
			@Add1Liable2 nvarchar(80),
			@Add2Liable2 nvarchar(320),
			@Add3Liable2 nvarchar(100),
			@Add4Liable2 nvarchar(100),
			@Add5Liable2 nvarchar(100),
			@AddCountryLiable2 int,
			@AddPostCodeLiable2 varchar(12),
			@Phone1Liable2 varchar(50),
			@Phone2Liable2 varchar(50),
			@Phone3Liable2 varchar(50),
			@Phone4Liable2 varchar(50),
			@Phone5Liable2 varchar(50),
			@Email1Liable2 varchar(250),
			@Email2Liable2 varchar(250),
			@Email3Liable2 varchar(250),
			@TitleLiable3 varchar(50),
			@FirstnameLiable3 nvarchar(50),
			@MiddlenameLiable3 nvarchar(50),
			@LastnameLiable3 nvarchar(50),
			@FullnameLiable3 nvarchar(150),
			@CompanyNameLiable3 nvarchar(50),
			@DOBLiable3 datetime,
			@NINOLiable3 varchar(13),
			@MinorLiable3 bit,
			@Add1Liable3 nvarchar(80),
			@Add2Liable3 nvarchar(320),
			@Add3Liable3 nvarchar(100),
			@Add4Liable3 nvarchar(100),
			@Add5Liable3 nvarchar(100),
			@AddCountryLiable3 int,
			@AddPostCodeLiable3 varchar(12),
			@Phone1Liable3 varchar(50),
			@Phone2Liable3 varchar(50),
			@Phone3Liable3 varchar(50),
			@Phone4Liable3 varchar(50),
			@Phone5Liable3 varchar(50),
			@Email1Liable3 varchar(250),
			@Email2Liable3 varchar(250),
			@Email3Liable3 varchar(250),
			@TitleLiable4 varchar(50),
			@FirstnameLiable4 nvarchar(50),
			@MiddlenameLiable4 nvarchar(50),
			@LastnameLiable4 nvarchar(50),
			@FullnameLiable4 nvarchar(150),
			@CompanyNameLiable4 nvarchar(50),
			@DOBLiable4 datetime,
			@NINOLiable4 varchar(13),
			@MinorLiable4 bit,
			@Add1Liable4 nvarchar(80),
			@Add2Liable4 nvarchar(320),
			@Add3Liable4 nvarchar(100),
			@Add4Liable4 nvarchar(100),
			@Add5Liable4 nvarchar(100),
			@AddCountryLiable4 int,
			@AddPostCodeLiable4 varchar(12),
			@Phone1Liable4 varchar(50),
			@Phone2Liable4 varchar(50),
			@Phone3Liable4 varchar(50),
			@Phone4Liable4 varchar(50),
			@Phone5Liable4 varchar(50),
			@Email1Liable4 varchar(250),
			@Email2Liable4 varchar(250),
			@Email3Liable4 varchar(250),
			@TitleLiable5 varchar(50),
			@FirstnameLiable5 nvarchar(50),
			@MiddlenameLiable5 nvarchar(50),
			@LastnameLiable5 nvarchar(50),
			@FullnameLiable5 nvarchar(150),
			@CompanyNameLiable5 nvarchar(50),
			@DOBLiable5 datetime,
			@NINOLiable5 varchar(13),
			@MinorLiable5 bit,
			@Add1Liable5 nvarchar(80),
			@Add2Liable5 nvarchar(320),
			@Add3Liable5 nvarchar(100),
			@Add4Liable5 nvarchar(100),
			@Add5Liable5 nvarchar(100),
			@AddCountryLiable5 int,
			@AddPostCodeLiable5 varchar(12),
			@Phone1Liable5 varchar(50),
			@Phone2Liable5 varchar(50),
			@Phone3Liable5 varchar(50),
			@Phone4Liable5 varchar(50),
			@Phone5Liable5 varchar(50),
			@Email1Liable5 varchar(250),
			@Email2Liable5 varchar(250),
			@Email3Liable5 varchar(250),
			@BatchDate DateTime,
			@ClientId int ,
			@ClientName nvarchar(80),			
			@Status nvarchar(50),
			@Phase nvarchar(50),
			@PhaseDate datetime,
			@Stage nvarchar(50),
			@StageDate datetime,
			@IsAssignable bit,
			@OffenceDate datetime,
			@CreatedOn datetime,
			@CCaseId int, --CaseId
			@DefaultersNames nvarchar(500),
			@BillNumber varchar(50),
			@EmployerName nvarchar(150),
			@EmployerAddressLine1 nvarchar(80),
			@EmployerAddressLine2 nvarchar(320),
			@EmployerAddressLine3 nvarchar(100),
			@EmployerAddressLine4 nvarchar(100),
			@EmployerPostcode varchar(12),
			@EmployerEmailAddress varchar(250),
			@EmployerFaxNumber varchar(50),
			@Occupation varchar(50),
			@RollNumber varchar(50),
			@BenefitIndicator varchar(50),
			@CaseNote nvarchar(2000), -- CaseNotes
			@Additional1 nvarchar(301),
			@Additional2 nvarchar(301),
			@Additional3 nvarchar(301),
			@Additional4 nvarchar(301),
			@Additional5 nvarchar(301),
			@Additional6 nvarchar(301),
			@Additional7 nvarchar(301),
			@Additional8 nvarchar(301),
			@Additional9 nvarchar(301),
			@Additional10 nvarchar(301),
			@CasePrefix varchar(1),
			@Imported bit,
			@ImportedOn datetime,		
			@UploadedOn datetime,
	
			@PreviousClientId INT,			
			@PreviousBatchDate  DateTime,
			@CurrentBatchId INT,
			@CaseId INT,
			@UnPaidStatusId INT,
			@ReturnedStatusId INT,
			@StageId INT,
			@ExpiredOn	DateTime,
			@LifeExpectancy INT,
			@NewCaseNumber varchar(7),
			@CaseTypeId INT,
			@VTECDate	DateTime,
			@ANPR bit,
			@cnid INT,
			@Succeed bit,
			@ErrorId INT,
			@OffenceCodeId int,
			@LeadDefaulterId int,
			@VehicleId int,
			@DefaulterId int,
			@CStatusId int,
			@CStatus nvarchar(150),
			@ReturnRunId int,
			@NotImportedCount int,			
			@StatusId int

		SET @NotImportedCount = (SELECT COUNT(1) FROM oso.stg_PREWRIT_Cases where imported = 0)

		IF (@NotImportedCount > 0)
		BEGIN
			EXEC oso.usp_OS_AddBatchesForCases
		END
		--DECLARE @ResultSet table (alphaNumericCaseNumber varchar(7))

		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

        SELECT
               @ReturnedStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Returned'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			[ClientCaseReference], 
			[TitleLiable1], 
			[FullnameLiable1], 
			[DOBLiable1], 
			[DebtAddress1],
			[DebtAddress2],
			[DebtAddress3],
			[DebtAddress4],
			[DebtAddress5],
			[DebtAddressPostcode], 
			[Phone1Liable1],
			[Phone2Liable1],
			[Phone3Liable1],
			[ClientDefaulterReference], 			
			[IssueDate], 
			[FineAmount], 
			[IssuingCourt],
			[Add1Liable1], 
			[Add2Liable1], 
			[Add3Liable1], 
			[Add4Liable1], 
			[Add5Liable1], 
			[AddPostCodeLiable1],
			[IsBusinessAddress], 
			[StartDate], 
			[EndDate], 
			[ClientComments], 
			[Email1Liable1], 
			[EmployerTelephone], 
			[OffenceCode],
			[OffenceDescription],
			[OffenceLocation], 
			[DebtAddressCountry], 
			[VehicleVRM],  
			[VehicleMake], 
			[VehicleModel],
			[TECDate], 
			[Currency],
			[FirstnameLiable1], 
			[MiddlenameLiable1], 
			[LastnameLiable1], 
			[CompanyNameLiable1],
			[NINOLiable1], 
			[MinorLiable1], 
			[AddCountryLiable1],
			[Phone4Liable1],
			[Phone5Liable1],
			[Email2Liable1],
			[Email3Liable1],
			[TitleLiable2], 
			[FirstnameLiable2], 
			[MiddlenameLiable2],
			[LastnameLiable2], 
			[FullnameLiable2], 
			[CompanyNameLiable2], 
			[DOBLiable2],
			[NINOLiable2], 
			[MinorLiable2],
			[Add1Liable2], 
			[Add2Liable2], 
			[Add3Liable2], 
			[Add4Liable2], 
			[Add5Liable2], 
			[AddCountryLiable2], 
			[AddPostCodeLiable2],
			[Phone1Liable2],
			[Phone2Liable2],
			[Phone3Liable2],
			[Phone4Liable2],
			[Phone5Liable2],
			[Email1Liable2],
			[Email2Liable2],
			[Email3Liable2],
			[TitleLiable3], 
			[FirstnameLiable3], 
			[MiddlenameLiable3],
			[LastnameLiable3], 
			[FullnameLiable3], 
			[CompanyNameLiable3], 
			[DOBLiable3],
			[NINOLiable3], 
			[MinorLiable3],
			[Add1Liable3],
			[Add2Liable3],
			[Add3Liable3],
			[Add4Liable3],
			[Add5Liable3],
			[AddCountryLiable3], 
			[AddPostCodeLiable3],
			[Phone1Liable3],
			[Phone2Liable3],
			[Phone3Liable3],
			[Phone4Liable3],
			[Phone5Liable3],
			[Email1Liable3],
			[Email2Liable3],
			[Email3Liable3],
			[TitleLiable4], 
			[FirstnameLiable4], 
			[MiddlenameLiable4],
			[LastnameLiable4], 
			[FullnameLiable4], 
			[CompanyNameLiable4], 
			[DOBLiable4], 
			[NINOLiable4], 
			[MinorLiable4],
			[Add1Liable4], 
			[Add2Liable4], 
			[Add3Liable4], 
			[Add4Liable4], 
			[Add5Liable4], 
			[AddCountryLiable4], 
			[AddPostCodeLiable4],
			[Phone1Liable4], 
			[Phone2Liable4], 
			[Phone3Liable4], 
			[Phone4Liable4], 
			[Phone5Liable4], 
			[Email1Liable4], 
			[Email2Liable4], 
			[Email3Liable4], 
			[TitleLiable5], 
			[FirstnameLiable5], 
			[MiddlenameLiable5],
			[LastnameLiable5], 
			[FullnameLiable5], 
			[CompanyNameLiable5], 
			[DOBLiable5], 
			[NINOLiable5], 
			[MinorLiable5],
			[Add1Liable5], 
			[Add2Liable5], 
			[Add3Liable5], 
			[Add4Liable5], 
			[Add5Liable5], 
			[AddCountryLiable5], 
			[AddPostCodeLiable5],
			[Phone1Liable5], 
			[Phone2Liable5], 
			[Phone3Liable5], 
			[Phone4Liable5], 
			[Phone5Liable5], 
			[Email1Liable5], 
			[Email2Liable5], 
			[Email3Liable5],
			[BatchDate], 
			[ClientId], 
			[ClientName], 			 
			[Status], 
			[Phase], 
			[PhaseDate], 
			[Stage], 
			[StageDate], 
			[IsAssignable],
			[OffenceDate],
			[CreatedOn], 
			[CCaseId], 
			[DefaultersNames], 
			[BillNumber], 
			[EmployerName], 
			[EmployerAddressLine1], 
			[EmployerAddressLine2], 
			[EmployerAddressLine3], 
			[EmployerAddressLine4], 
			[EmployerPostcode], 
			[EmployerEmailAddress], 
			[EmployerFaxNumber], 
			[Occupation], 
			[RollNumber], 
			[BenefitIndicator], 
			[CaseNote],
			[Additional1],
			[Additional2],
			[Additional3],
			[Additional4],
			[Additional5],
			[Additional6],
			[Additional7],
			[Additional8],
			[Additional9],
			[Additional10],
			[CasePrefix],
			CASE CasePrefix 
				WHEN 'H' THEN 6 
				WHEN 'W' THEN 7		 
				--WHEN 'C' THEN 16		
			END StatusId
				
		FROM
            oso.stg_PREWRIT_Cases
        WHERE
            Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientCaseReference, 
			@TitleLiable1, 
			@FullnameLiable1, 
			@DOBLiable1, 
			@DebtAddress1,
			@DebtAddress2,
			@DebtAddress3,
			@DebtAddress4,
			@DebtAddress5,
			@DebtAddressPostcode, 
			@Phone1Liable1,
			@Phone2Liable1,
			@Phone3Liable1,
			@ClientDefaulterReference, 			
			@IssueDate, 
			@FineAmount, 
			@IssuingCourt,
			@Add1Liable1, 
			@Add2Liable1, 
			@Add3Liable1, 
			@Add4Liable1, 
			@Add5Liable1, 
			@AddPostCodeLiable1,
			@IsBusinessAddress, 
			@StartDate, 
			@EndDate, 
			@ClientComments, 
			@Email1Liable1, 
			@EmployerTelephone, 
			@OffenceCode,
			@OffenceDescription,
			@OffenceLocation, 
			@DebtAddressCountry, 
			@VehicleVRM,  
			@VehicleMake, 
			@VehicleModel,
			@TECDate, 
			@Currency,
			@FirstnameLiable1, 
			@MiddlenameLiable1, 
			@LastnameLiable1, 
			@CompanyNameLiable1,
			@NINOLiable1, 
			@MinorLiable1, 
			@AddCountryLiable1,
			@Phone4Liable1,
			@Phone5Liable1,
			@Email2Liable1,
			@Email3Liable1,
			@TitleLiable2, 
			@FirstnameLiable2, 
			@MiddlenameLiable2,
			@LastnameLiable2, 
			@FullnameLiable2, 
			@CompanyNameLiable2,
			@DOBLiable2, 
			@NINOLiable2, 
			@MinorLiable2,
			@Add1Liable2, 
			@Add2Liable2, 
			@Add3Liable2, 
			@Add4Liable2, 
			@Add5Liable2, 
			@AddCountryLiable2, 
			@AddPostCodeLiable2,
			@Phone1Liable2,
			@Phone2Liable2,
			@Phone3Liable2,
			@Phone4Liable2,
			@Phone5Liable2,
			@Email1Liable2,
			@Email2Liable2,
			@Email3Liable2,
			@TitleLiable3, 
			@FirstnameLiable3, 
			@MiddlenameLiable3,
			@LastnameLiable3, 
			@FullnameLiable3, 
			@CompanyNameLiable3, 
			@DOBLiable3,
			@NINOLiable3, 
			@MinorLiable3,
			@Add1Liable3,
			@Add2Liable3,
			@Add3Liable3,
			@Add4Liable3,
			@Add5Liable3,
			@AddCountryLiable3, 
			@AddPostCodeLiable3,
			@Phone1Liable3,
			@Phone2Liable3,
			@Phone3Liable3,
			@Phone4Liable3,
			@Phone5Liable3,
			@Email1Liable3,
			@Email2Liable3,
			@Email3Liable3,
			@TitleLiable4, 
			@FirstnameLiable4, 
			@MiddlenameLiable4,
			@LastnameLiable4, 
			@FullnameLiable4, 
			@CompanyNameLiable4, 
			@DOBLiable4, 
			@NINOLiable4, 
			@MinorLiable4,
			@Add1Liable4, 
			@Add2Liable4, 
			@Add3Liable4, 
			@Add4Liable4, 
			@Add5Liable4, 
			@AddCountryLiable4, 
			@AddPostCodeLiable4,
			@Phone1Liable4, 
			@Phone2Liable4, 
			@Phone3Liable4, 
			@Phone4Liable4, 
			@Phone5Liable4, 
			@Email1Liable4, 
			@Email2Liable4, 
			@Email3Liable4, 
			@TitleLiable5, 
			@FirstnameLiable5, 
			@MiddlenameLiable5,
			@LastnameLiable5, 
			@FullnameLiable5, 
			@CompanyNameLiable5, 
			@DOBLiable5, 
			@NINOLiable5, 
			@MinorLiable5,
			@Add1Liable5, 
			@Add2Liable5, 
			@Add3Liable5, 
			@Add4Liable5, 
			@Add5Liable5, 
			@AddCountryLiable5, 
			@AddPostCodeLiable5,
			@Phone1Liable5, 
			@Phone2Liable5, 
			@Phone3Liable5, 
			@Phone4Liable5, 
			@Phone5Liable5, 
			@Email1Liable5, 
			@Email2Liable5, 
			@Email3Liable5, 
			@BatchDate,
			@ClientId, 
			@ClientName,			 
			@Status, 
			@Phase, 
			@PhaseDate, 
			@Stage, 
			@StageDate, 
			@IsAssignable,
			@OffenceDate, 
			@CreatedOn, 
			@CCaseId, 
			@DefaultersNames, 
			@BillNumber, 
			@EmployerName, 
			@EmployerAddressLine1, 
			@EmployerAddressLine2, 
			@EmployerAddressLine3, 
			@EmployerAddressLine4, 
			@EmployerPostcode, 
			@EmployerEmailAddress, 
			@EmployerFaxNumber, 
			@Occupation, 
			@RollNumber, 
			@BenefitIndicator, 
			@CaseNote,
			@Additional1,
			@Additional2,
			@Additional3,
			@Additional4,
			@Additional5,
			@Additional6,
			@Additional7,
			@Additional8,
			@Additional9,
			@Additional10,
			@CasePrefix,
			@StatusId
        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						IF (@CasePrefix IS NULL OR @CasePrefix = 'N' OR  @CasePrefix = 'R')
						BEGIN

							SET @CurrentBatchId = NULL
							--Select @CurrentBatchId = Id from Batches where batchDate = @BatchDate and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId)
							Select @CurrentBatchId = (SELECT TOP 1 Id from Batches						
							where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) 
							and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) 
							order by Id desc)
						
							SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
							INNER JOIN clientcasetype cct on cli.id = cct.clientid
							where cli.id = @ClientId
						
							SET @StageId = NULL

							SELECT
								@StageId = (s.id)
							FROM
								clients cl
								join stages s on s.clientId = cl.id
							WHERE
								s.onLoad = 1
								and cl.id = @Clientid

							IF (@StageId IS NULL)	-- Client is inactive, so look for stageid from Empty Workflow
							BEGIN
							  SET @StageId = (select s.id from clients cli
													INNER JOIN stages s on s.clientid = cli.id
													INNER JOIN StageTemplates st on s.stagetemplateid = st.id
													INNER JOIN PhaseTemplates pt on st.phasetemplateid = pt.id
													INNER JOIN WorkflowTemplates wft on pt.workflowtemplateid = wft.id
													where cli.id = @ClientId
															and (
															(wft.name <> 'Empty Workflow' and s.onload = 1)
															or
															(wft.name = 'Empty Workflow' and s.name = 'Return Case')
															)
											)
							END

						
							SET @CStatusId = @UnPaidStatusId;
							SET @ReturnRunId = NULL
							IF (@CStatus = 'Returned')
							BEGIN
								SET @CStatusId = @ReturnedStatusId
								SET @ReturnRunId = 1244
							END

							Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
							SET @ANPR = 0
							SET @VTECDate = NULL

							IF (@CaseTypeId = 9)
							BEGIN
								SET @VTECDate = @TECDate
								SET @ANPR = 1
								SET @StartDate =  NULL
								SET @EndDate = NULL
							END

							IF (@CaseTypeId = 21)
							BEGIN
								SET @IsBusinessAddress = 1
							END

							--Set Offence Date = NULL for CTAX and NNDR and CMS cases 
							IF (@CaseTypeId = 20 OR @CaseTypeId = 21 OR @CaseTypeId = 22)
							BEGIN
								SET @OffenceDate =  NULL
							END
						
							SET @cnid = (  
										SELECT MIN(cn.Id)  
										FROM CaseNumbers AS cn WITH (  
											XLOCK  
											,HOLDLOCK  
											)  
										WHERE cn.Reserved = 0  
										--AND cn.id > = 13999996
										--AND cn.alphaNumericCaseNumber > 'X000000' --Remove this line for LIVE ACTIVE cases.
										)  

							UPDATE CaseNumbers  
							SET Reserved = 1  
							WHERE Id = @cnid;  
  
							SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

							-- 2) Insert Case	
							INSERT INTO dbo.[Cases]
								   ( 
										[batchId]
										, [originalBalance]
										, [statusId]
										, [statusDate]
										, [previousStatusId]
										, [previousStatusDate]
										, [stageId]
										, [stageDate]
										, [PreviousStageId]
										, [drakeReturnCodeId]
										, [assignable]
										, [expiresOn]
										, [createdOn]
										, [createdBy]
										, [clientCaseReference]
										, [caseNumber]
										, [note]
										, [tecDate]
										, [issueDate]
										, [startDate]
										, [endDate]
										, [firstLine]
										, [postCode]
										, [address]
										, [addressLine1]
										, [addressLine2]
										, [addressLine3]
										, [addressLine4]
										, [addressLine5]
										, [countryId]
										, [currencyId]
										, [businessAddress]
										, [manualInput]
										, [traced]
										, [extendedDays]
										, [payableOnline]
										, [payableOnlineEndDate]
										, [returnRunId]
										, [defaulterEmail]
										, [debtName]
										, [officerPaymentRunId]
										, [isDefaulterWeb]
										, [grade]
										, [defaulterAddressChanged]
										, [isOverseas]
										, [batchLoadDate]
										, [anpr]
										, [h_s_risk]
										, [billNumber]
										, [propertyBand]
										, [LinkId]
										, [IsAutoLink]
										, [IsManualLink]
										, [PropertyId]
										, [IsLocked]
										, [ClientDefaulterReference]
										, [ClientLifeExpectancy]
										, [CommissionRate]
										, [CommissionValueType]
								   )
								   VALUES
								   (
										@CurrentBatchId --[batchId]
										, @FineAmount --[originalBalance]
										, @CStatusId	--[statusId]
										, GetDate()	--[statusDate]
										, NULL	--[previousStatusId]
										, NULL	--[previousStatusDate]
										, @StageId --[stageId]
										, GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
										, NULL --[PreviousStageId]
										, NULL --@CReturnCode --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
										, @IsAssignable --[assignable]
										, @ExpiredOn --[expiresOn]
										, @CreatedOn --[createdOn]
										, 2 -- [createdBy]
										, @ClientCaseReference --[clientCaseReference]
										, @NewCaseNumber --[caseNumber]
										, NULL --[note]
										, @VTECDate --[tecDate]
										, ISNULL(@TECDate,@IssueDate) --[issueDate]
										, @StartDate --[startDate]
										, @EndDate --[endDate]
										, @DebtAddress1--[firstLine]
										, @DebtAddressPostcode--[postCode]
										, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
										, @DebtAddress1 --[addressLine1]
										, @DebtAddress2--[addressLine2]
										, @DebtAddress3--[addressLine3]
										, @DebtAddress4--[addressLine4]
										, @DebtAddress5--[addressLine5]
										, @DebtAddressCountry --[countryId]
										, @Currency --[currencyId]
										, @IsBusinessAddress --[businessAddress]
										, 0--[manualInput]
										, 0--[traced]
										, 0--[extendedDays]
										, 1 --[payableOnline]
										, GETDATE() --[payableOnlineEndDate]
										, @ReturnRunId --[returnRunId]
										, NULL --[defaulterEmail]
										--, @FullnameLiable1 --[debtName]
										, RTRIM(LTRIM(ISNULL(@FullnameLiable1,'') + IIF(@DefaultersNames IS NULL OR @DefaultersNames = '','', ' ' + @DefaultersNames)))--[debtName]
										, NULL--[officerPaymentRunId]
										, 0--[isDefaulterWeb]
										, -1 --[grade]
										, 0--[defaulterAddressChanged]
										, 0--[isOverseas]
										, @BatchDate--[batchLoadDate]
										, @ANPR--[anpr]
										, 0 --[h_s_risk]
										, @BillNumber--[billNumber]
										, NULL --[propertyBand]
										, NULL --[LinkId]
										, 1 --[IsAutoLink]
										, 0 --[IsManualLink]
										, NULL --[PropertyId]
										, 0 --[IsLocked]
										, @ClientDefaulterReference --[ClientDefaulterReference]
										, @LifeExpectancy --[ClientLifeExpectancy]
										, NULL --[CommissionRate]
										, NULL --[CommissionValueType]	
						
								   )
							;
            

			
							SET @CaseId = SCOPE_IDENTITY();
						
						
						--  Insert Case Note
							INSERT INTO dbo.[CaseNotes] 
							( [caseId]
								, [userId]
								, [officerId]
								, [text]
								, [occured]
								, [visible]
								, [groupId]
								, [TypeId]
							)
							VALUES
							(
								@CaseId,
								2,
								NULL,
								@CaseNote,
								@CreatedOn,
								1,
								NULL,
								NULL
							)

							IF @Additional1 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional1, 1, CHARINDEX('|', @Additional1) - 1),
									SUBSTRING(@Additional1, CHARINDEX('|', @Additional1) + 1, 250),
									GETDATE()
								)
							END

							IF @Additional2 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional2, 1, CHARINDEX('|', @Additional2) - 1),
									SUBSTRING(@Additional2, CHARINDEX('|', @Additional2) + 1, 250),
									GETDATE()
								)
							END

							IF @Additional3 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional3, 1, CHARINDEX('|', @Additional3) - 1),
									SUBSTRING(@Additional3, CHARINDEX('|', @Additional3) + 1, 250),
									GETDATE()
								)
							END

							IF @Additional4 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional4, 1, CHARINDEX('|', @Additional4) - 1),
									SUBSTRING(@Additional4, CHARINDEX('|', @Additional4) + 1, 250),
									GETDATE()
								)
							END

							IF @Additional5 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional5, 1, CHARINDEX('|', @Additional5) - 1),
									SUBSTRING(@Additional5, CHARINDEX('|', @Additional5) + 1, 250),
									GETDATE()
								)
							END

							IF @Additional6 IS NOT NULL
							BEGIN
								INSERT INTO [dbo].[AdditionalCaseData]
								(
									[CaseId]
									,[Name]
									,[Value]
									,[LoadedOn])
								VALUES
								(
									@CaseId,
									SUBSTRING(@Additional6, 1, CHARINDEX('|', @Additional6) - 1),
									SUBSTRING(@Additional6, CHARINDEX('|', @Additional6) + 1, 250),
									GETDATE()
								)
							END

							--SET @CaseId = SCOPE_IDENTITY();
						
							--INSERT INTO dbo.OldCaseNumbers (CaseId, OldCaseNumber)
							--VALUES (@CaseId, @OneStepCaseNumber);
						
						-- 3) Insert Case Offences
							SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
							INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid)
							VALUES
							(
								@CaseId,
								NULL,
								@OffenceLocation,
								NULL,--@CaseNote,
								@OffenceCodeId
							);

						-- 4) Insert Defaulters	
						--Lead Defaulter
						IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
						BEGIN
							INSERT INTO dbo.Defaulters (
								[name],
								[title],
								[firstName],
								[middleName],
								[lastName],
								[firstLine],
								[postCode],
								[address],
								[addressLine1],
								[addressLine2],
								[addressLine3],
								[addressLine4],
								[addressLine5],
								[countryId],
								[business],
								[forcesAddress],
								[isOverseas],
								[addressCreatedOn],
								[IsLeadCustomer]
								)

								VALUES
								(
								@FullnameLiable1 --[name],
								,@TitleLiable1  --[title],
								,@FirstnameLiable1 --[firstName],
								,@MiddlenameLiable1 --[middleName],
								,@LastnameLiable1 --[lastName],
								,@Add1Liable1 --[firstLine],
								,@AddPostcodeLiable1 --[postCode],
								, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
								,@Add1Liable1 --[addressLine1],
								,@Add2Liable1 --[addressLine2],
								,@Add3Liable1 --[addressLine3],
								,@Add4Liable1 --[addressLine4],
								,@Add5Liable1 --[addressLine5],
								,231 --[countryId],
								,CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END --[business],
								,0 --[forcesAddress],
								,0 --[isOverseas],
								,@createdon --[addressCreatedOn],
								,1
								);
							Set @LeadDefaulterId = SCOPE_IDENTITY();

							-- Insert Defaulter Details
							INSERT INTO dbo.DefaulterDetails (
							[DefaulterId],
							[BirthDate],
							[IsBirthDateConfirmed],
							[IsTreatedAsMinor],
							[PotentialBirthDate],
							[IsPotentialBirthDateConfirmed],
							[NINO],
							[IsNINOConfirmed],
							[PotentialNINO],
							[IsPotentialNINOConfirmed],
							[BirthDateConfirmedBy],
							[NINOConfirmedBy])
							VALUES
							(
							@LeadDefaulterId,
							IIF(@DOBLiable1 = '1753-01-01 00:00:00.000', NULL,@DOBLiable1),
							0,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							NULL
							)

							-- Insert Contact Address
							Insert Into ContactAddresses (
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[defaulterId],
							[createdOn],
							[forcesAddress],
							[isOverseas],
							[StatusId],
							[IsPrimary],
							[IsConfirmed],
							[SourceId],
							[TidReviewedId])
							VALUES
							(
								@Add1Liable1,
								@AddPostcodeLiable1,
								RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
								@Add1Liable1, --[addressLine1],
								@Add2Liable1, --[addressLine2],
								@Add3Liable1, --[addressLine3],
								@Add4Liable1, --[addressLine4],
								NULL,
								231,
								CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END, --[business],
								@LeadDefaulterId,
								@CreatedOn,
								0,
								0,
								3,
								1,
								0,
								1,
								3
							)

							-- Insert Defaulter Case
							INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
							VALUES (@CaseId, @LeadDefaulterId)

							-- Insert Vehicle
							IF (@CaseTypeId = 9 AND @VehicleVRM IS NOT NULL)
								BEGIN
									INSERT INTO dbo.Vehicles (
										[vrm]
										,[vrmOwner]
										,[defaulterId]
										,[responseStatusId]
										,[responseDate]
										,[responseLoadedDate]
										,[keeperId]
										,[responseStringId]
										,[eventDate]
										,[licenceExpiry]
										,[exportDate]
										,[scrapDate]
										,[theftDate]
										,[recoveryDate]
										,[make]
										,[model]
										,[colour]
										,[taxClass]
										,[seatingCapacity]
										,[previousKeepers]
										,[Motability]
										,[Check]
										,[LastRequestedOn]
										,[isWarrantVrm]
										,[officerId]
										,[VrmReceivedOn]
									)
									VALUES
									(
										@VehicleVRM -- vrm
										,0 -- vrmOwner
										,@LeadDefaulterId
										,NULL -- responseStatusId
										,NULL -- responseDate
										,NULL -- responseLoadedDate
										,NULL -- keeperId
										,NULL -- responseStringId
										,NULL -- eventDate
										,NULL -- licenceExpiry
										,NULL -- exportDate
										,NULL -- scrapDate
										,NULL -- theftDate
										,NULL -- recoveryDate
										,NULL -- make
										,NULL -- model
										,NULL -- colour
										,NULL -- taxCl--s
										,NULL -- seatingCapacity
										,NULL -- previousKeepers
										,0 -- Motability
										,0 -- [Check]
										,NULL -- L--tRequestedOn
										,1 -- isWarrantVrm
										,NULL -- officerId
										,@CreatedOn -- VrmReceivedOn
										);

									SET @VehicleId = SCOPE_IDENTITY();

									INSERT INTO dbo.CaseVehicles
									(
										caseId,
										vehicleId
									)
									VALUES
									(
										@CaseId,
										@VehicleId
									)

								END

						END

						-- Defaulter2
						IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
						BEGIN
							INSERT INTO dbo.Defaulters (
								[name],
								[title],
								[firstName],
								[middleName],
								[lastName],
								[firstLine],
								[postCode],
								[address],
								[addressLine1],
								[addressLine2],
								[addressLine3],
								[addressLine4],
								[addressLine5],
								[countryId],
								[business],
								[forcesAddress],
								[isOverseas],
								[addressCreatedOn],
								[IsLeadCustomer]
								)

								VALUES
								(
								@FullnameLiable2 --[name],
								,@TitleLiable2  --[title],
								,@FirstnameLiable2 --[firstName],
								,@MiddlenameLiable2 --[middleName],
								,@LastnameLiable2 --[lastName],
								,@Add1Liable2 --[firstLine],
								,@AddPostcodeLiable2 --[postCode],
								, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
								,@Add1Liable2 --[addressLine1],
								,@Add2Liable2 --[addressLine2],
								,@Add3Liable2 --[addressLine3],
								,@Add4Liable2 --[addressLine4],
								,@Add5Liable2 --[addressLine5],
								,231 --[countryId],
								,CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END --[business],
								,0 --[forcesAddress],
								,0 --[isOverseas],
								,@createdon --[addressCreatedOn],
								,0
								);

							Set @DefaulterId = SCOPE_IDENTITY();

							-- Insert Defaulter Details
							INSERT INTO dbo.DefaulterDetails (
							[DefaulterId],
							[BirthDate],
							[IsBirthDateConfirmed],
							[IsTreatedAsMinor],
							[PotentialBirthDate],
							[IsPotentialBirthDateConfirmed],
							[NINO],
							[IsNINOConfirmed],
							[PotentialNINO],
							[IsPotentialNINOConfirmed],
							[BirthDateConfirmedBy],
							[NINOConfirmedBy])
							VALUES
							(
							@DefaulterId,
							IIF(@DOBLiable2 = '1753-01-01 00:00:00.000', NULL,@DOBLiable2),
							0,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							NULL
							)

							-- Insert Contact Address
							Insert Into ContactAddresses (
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[defaulterId],
							[createdOn],
							[forcesAddress],
							[isOverseas],
							[StatusId],
							[IsPrimary],
							[IsConfirmed],
							[SourceId],
							[TidReviewedId])
							VALUES
							(
								@Add1Liable2,
								@AddPostcodeLiable2,
								RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
								@Add1Liable2, --[addressLine1],
								@Add2Liable2, --[addressLine2],
								@Add3Liable2, --[addressLine3],
								@Add4Liable2, --[addressLine4],
								NULL,
								231,
								CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END, --[business],
								@DefaulterId,
								@CreatedOn,
								0,
								0,
								3,
								1,
								0,
								1,
								1
							)

							-- Insert Defaulter Case
							INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
							VALUES (@CaseId, @DefaulterId)

						END

						-- Defaulter3
						IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
						BEGIN
							INSERT INTO dbo.Defaulters (
								[name],
								[title],
								[firstName],
								[middleName],
								[lastName],
								[firstLine],
								[postCode],
								[address],
								[addressLine1],
								[addressLine2],
								[addressLine3],
								[addressLine4],
								[addressLine5],
								[countryId],
								[business],
								[forcesAddress],
								[isOverseas],
								[addressCreatedOn],
								[IsLeadCustomer]
								)

								VALUES
								(
								@FullnameLiable3 --[name],
								,@TitleLiable3  --[title],
								,@FirstnameLiable3 --[firstName],
								,@MiddlenameLiable3 --[middleName],
								,@LastnameLiable3 --[lastName],
								,@Add1Liable3 --[firstLine],
								,@AddPostcodeLiable3 --[postCode],
								, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
								,@Add1Liable3 --[addressLine1],
								,@Add2Liable3 --[addressLine2],
								,@Add3Liable3 --[addressLine3],
								,@Add4Liable3 --[addressLine4],
								,@Add5Liable3 --[addressLine5],
								,231 --[countryId],
								,CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END --[business],
								,0 --[forcesAddress],
								,0 --[isOverseas],
								,@createdon --[addressCreatedOn],
								,0
								);

							Set @DefaulterId = SCOPE_IDENTITY();

							-- Insert Defaulter Details
							INSERT INTO dbo.DefaulterDetails (
							[DefaulterId],
							[BirthDate],
							[IsBirthDateConfirmed],
							[IsTreatedAsMinor],
							[PotentialBirthDate],
							[IsPotentialBirthDateConfirmed],
							[NINO],
							[IsNINOConfirmed],
							[PotentialNINO],
							[IsPotentialNINOConfirmed],
							[BirthDateConfirmedBy],
							[NINOConfirmedBy])
							VALUES
							(
							@DefaulterId,
							@DOBLiable3,
							0,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							NULL
							)

							-- Insert Contact Address
							Insert Into ContactAddresses (
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[defaulterId],
							[createdOn],
							[forcesAddress],
							[isOverseas],
							[StatusId],
							[IsPrimary],
							[IsConfirmed],
							[SourceId],
							[TidReviewedId])
							VALUES
							(
								@Add1Liable3,
								@AddPostcodeLiable3,
								RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
								@Add1Liable3, --[addressLine1],
								@Add2Liable3, --[addressLine2],
								@Add3Liable3, --[addressLine3],
								@Add4Liable3, --[addressLine4],
								NULL,
								231,
								CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END, --[business],
								@DefaulterId,
								@CreatedOn,
								0,
								0,
								3,
								1,
								0,
								1,
								1
							)

							-- Insert Defaulter Case
							INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
							VALUES (@CaseId, @DefaulterId)
						END
			
						-- Defaulter4
						IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
						BEGIN
							INSERT INTO dbo.Defaulters (
								[name],
								[title],
								[firstName],
								[middleName],
								[lastName],
								[firstLine],
								[postCode],
								[address],
								[addressLine1],
								[addressLine2],
								[addressLine3],
								[addressLine4],
								[addressLine5],
								[countryId],
								[business],
								[forcesAddress],
								[isOverseas],
								[addressCreatedOn],
								[IsLeadCustomer]
								)

								VALUES
								(
								@FullnameLiable4 --[name],
								,@TitleLiable4  --[title],
								,@FirstnameLiable4 --[firstName],
								,@MiddlenameLiable4 --[middleName],
								,@LastnameLiable4 --[lastName],
								,@Add1Liable4 --[firstLine],
								,@AddPostcodeLiable4 --[postCode],
								, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
								,@Add1Liable4 --[addressLine1],
								,@Add2Liable4 --[addressLine2],
								,@Add3Liable4 --[addressLine3],
								,@Add4Liable4 --[addressLine4],
								,@Add5Liable4 --[addressLine5],
								,231 --[countryId],
								,CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END --[business],
								,0 --[forcesAddress],
								,0 --[isOverseas],
								,@createdon --[addressCreatedOn],
								,0
								);

							Set @DefaulterId = SCOPE_IDENTITY();

							-- Insert Defaulter Details
							INSERT INTO dbo.DefaulterDetails (
							[DefaulterId],
							[BirthDate],
							[IsBirthDateConfirmed],
							[IsTreatedAsMinor],
							[PotentialBirthDate],
							[IsPotentialBirthDateConfirmed],
							[NINO],
							[IsNINOConfirmed],
							[PotentialNINO],
							[IsPotentialNINOConfirmed],
							[BirthDateConfirmedBy],
							[NINOConfirmedBy])
							VALUES
							(
							@DefaulterId,
							@DOBLiable4,
							0,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							NULL
							)

							-- Insert Contact Address
							Insert Into ContactAddresses (
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[defaulterId],
							[createdOn],
							[forcesAddress],
							[isOverseas],
							[StatusId],
							[IsPrimary],
							[IsConfirmed],
							[SourceId],
							[TidReviewedId])
							VALUES
							(
								@Add1Liable4,
								@AddPostcodeLiable4,
								RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
								@Add1Liable4, --[addressLine1],
								@Add2Liable4, --[addressLine2],
								@Add3Liable4, --[addressLine3],
								@Add4Liable4, --[addressLine4],
								NULL,
								231,
								CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END, --[business],
								@DefaulterId,
								@CreatedOn,
								0,
								0,
								3,
								1,
								0,
								1,
								1
							)
						
							-- Insert Defaulter Case
							INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
							VALUES (@CaseId, @DefaulterId)
						END

						-- Defaulter5
						IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
						BEGIN
							INSERT INTO dbo.Defaulters (
								[name],
								[title],
								[firstName],
								[middleName],
								[lastName],
								[firstLine],
								[postCode],
								[address],
								[addressLine1],
								[addressLine2],
								[addressLine3],
								[addressLine4],
								[addressLine5],
								[countryId],
								[business],
								[forcesAddress],
								[isOverseas],
								[addressCreatedOn],
								[IsLeadCustomer]
								)

								VALUES
								(
								@FullnameLiable5 --[name],
								,@TitleLiable5  --[title],
								,@FirstnameLiable5 --[firstName],
								,@MiddlenameLiable5 --[middleName],
								,@LastnameLiable5 --[lastName],
								,@Add1Liable5 --[firstLine],
								,@AddPostcodeLiable5 --[postCode],
								, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
								,@Add1Liable5 --[addressLine1],
								,@Add2Liable5 --[addressLine2],
								,@Add3Liable5 --[addressLine3],
								,@Add4Liable5 --[addressLine4],
								,@Add5Liable5 --[addressLine5],
								,231 --[countryId],
								,CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END --[business],
								,0 --[forcesAddress],
								,0 --[isOverseas],
								,@createdon --[addressCreatedOn],
								,0
								);
							Set @DefaulterId = SCOPE_IDENTITY();

							-- Insert Defaulter Details
							INSERT INTO dbo.DefaulterDetails (
							[DefaulterId],
							[BirthDate],
							[IsBirthDateConfirmed],
							[IsTreatedAsMinor],
							[PotentialBirthDate],
							[IsPotentialBirthDateConfirmed],
							[NINO],
							[IsNINOConfirmed],
							[PotentialNINO],
							[IsPotentialNINOConfirmed],
							[BirthDateConfirmedBy],
							[NINOConfirmedBy])
							VALUES
							(
							@DefaulterId,
							@DOBLiable5,
							0,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							0,
							NULL,
							NULL
							)

							-- Insert Contact Address
							Insert Into ContactAddresses (
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[defaulterId],
							[createdOn],
							[forcesAddress],
							[isOverseas],
							[StatusId],
							[IsPrimary],
							[IsConfirmed],
							[SourceId],
							[TidReviewedId])
							VALUES
							(
								@Add1Liable5,
								@AddPostcodeLiable5,
								RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
								@Add1Liable5, --[addressLine1],
								@Add2Liable5, --[addressLine2],
								@Add3Liable5, --[addressLine3],
								@Add4Liable5, --[addressLine4],
								NULL,
								231,
								CASE
									WHEN (@CaseTypeId = 21) then 1
									ELSE 0
									END, --[business],
								@DefaulterId,
								@CreatedOn,
								0,
								0,
								3,
								1,
								0,
								1,
								1
							)
						
							-- Insert Defaulter Case
							INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
							VALUES (@CaseId, @DefaulterId)
						END
			

						-- 5) Insert Contact Addresses


						-- 6) Insert Defaulter Details
						-- 7) Insert Defaulter Cases
						-- 8 ) Insert Vehicles
								-- Note: LeadDefaulterId is required


						-- 9) Insert Case Note
						-- Not required for now


							UPDATE
							[oso].[stg_PREWRIT_Cases]
							SET    Imported   = 1
							, ImportedOn = GetDate()
							WHERE
							Imported = 0 
							AND --OneStepCaseNumber = @OneStepCaseNumber
							 ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						-- New rows creation completed
						END

						ELSE
						BEGIN
							--UPDATE CASE WHEN RECOMMENCED
							IF(@CasePrefix in ('C'))
							BEGIN
								UPDATE dbo.Cases
								SET 
									statusId = previousStatusId, 
									statusDate = GETDATE(),					
									previousStatusId = statusId,
									previousStatusDate = statusDate	
								FROM Cases
								WHERE Id = @CCaseId AND statusId = 6

								--UPDATE HOLD CASES
								UPDATE dbo.HoldCases
								SET
									recommencedOn = GETDATE()
								FROM dbo.HoldCases AS hc
								WHERE hc.caseId = @CCaseId
								AND hc.recommencedOn IS NULL

								--INSERT CASEHISTORY RECORD
								INSERT INTO dbo.CaseHistory
								(
									caseId,
									userId,
									officerId,
									comment,
									occured,
									typeId,
									CaseNoteId
								)
								VALUES
								(
									@CCaseId,
									2,
									NULL,
									'RecommenceCase: CaseNumber = ' + (select caseNumber from dbo.Cases where Id = @CCaseId),
									GETDATE(),
									4,
									NULL
								)

							END

							--UPDATE CASE WHEN IS PLACED ON-HOLD
							IF(@CasePrefix in ('H'))
							BEGIN
								UPDATE dbo.Cases
								SET 
									previousStatusId = statusId,
									previousStatusDate = statusDate,	
									statusId = @StatusId, 
									statusDate = GETDATE()					
								FROM Cases
								WHERE Id = @CCaseId and statusId in (select Id from dbo.CaseStatus where name in ('Unpaid','Part-Paid','Goods Removed','Tracing'))

								--INSERT HOLDCASES RECORD
								INSERT INTO dbo.HoldCases
								(							 
									[caseId],							 
									[holdUntil], 
									[recommencedOn], 
									[holdReason], 
									[recommenceReason],							
									[holdReasonId], 
									[AmendedByHoldCaseId]
								)
								VALUES
								(
									@CCaseId,
									getdate(),
									NULL,
									NULL,
									NULL,
									2,
									NULL
								)

								--INSERT CASEHISTORY RECORD
								INSERT INTO dbo.CaseHistory
								(
									caseId,
									userId,
									officerId,
									comment,
									occured,
									typeId,
									CaseNoteId
								)
								VALUES
								(
									@CCaseId,
									2,
									NULL,							
									'Case status changed to: On-Hold from: ' + convert(varchar(50),(select name from CaseStatus where id =(select previousStatusId from dbo.Cases where Id = @CCaseId))),
									GETDATE(),
									3,
									NULL
								)
							END

							IF(@CasePrefix in ('W'))
							BEGIN
								UPDATE dbo.Cases
								SET 
									previousStatusId = statusId,
									previousStatusDate = statusDate,	
									statusId = @StatusId, 
									statusDate = GETDATE()					
								FROM Cases
								WHERE Id = @CCaseId

								INSERT INTO [dbo].[ReturnedCases]
								(
									[CaseId], 
									[DrakeReturnCodeId], 
									[UserId], 
									[Reason] 
						
								)
								VALUES
								(
									@CCaseId,
									63,
									2,
									'Case return - Case out of time'
								)

								--INSERT CASEHISTORY RECORD
								INSERT INTO dbo.CaseHistory
								(
									caseId,
									userId,
									officerId,
									comment,
									occured,
									typeId,
									CaseNoteId
								)
								VALUES
								(
									@CCaseId,
									2,
									NULL,							
									'Case status changed to: Pending Return from: ' + convert(varchar(50),(select name from CaseStatus where id =(select previousStatusId from dbo.Cases where Id = @CCaseId))),
									GETDATE(),
									5,
									NULL
								)	
							END

							IF(@CasePrefix = 'U')
							BEGIN
								UPDATE dbo.Cases
								SET 				
									debtName = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1))
								FROM dbo.Cases
								WHERE Id = @CCaseId 

								-- Update Lead Defaulter Name
								UPDATE dbo.Defaulters
								SET	
									name = (select Concat(@TitleLiable1,' ', @FirstnameLiable1, ' ', @LastnameLiable1)),
									firstName = @FirstnameLiable1,
									lastName = @LastnameLiable1
								from 
									dbo.Defaulters 
									join dbo.DefaulterCases dc on dbo.Defaulters.Id = dc.defaulterId		
								where 
								dc.caseId = @CCaseId
								AND isLeadCustomer = 1

								--Update Second Defaulter Name
								--DECLARE @DefaulterId int;
						
								SELECT TOP 1 @DefaulterId = dc.defaulterId  
								FROM DefaulterCases dc
								INNER JOIN Defaulters d on d.Id = dc.defaulterId
								WHERE dc.caseId = @CCaseId AND isLeadCustomer = 0
								ORDER BY dc.defaulterId ASC

								IF (@FirstnameLiable2 IS NOT NULL OR @LastnameLiable2 IS NOT NULL)
								BEGIN 
									IF (@DefaulterId IS NOT NULL )
									BEGIN
										--Update 2nd defaulter
										UPDATE dbo.Defaulters
										SET	
											name = (select Concat(@TitleLiable2,' ', @FirstnameLiable2, ' ', @LastnameLiable2)),
											firstName = @FirstnameLiable2,
											lastName = @LastnameLiable2
										from 
											dbo.Defaulters 		
										where 
										Id = @DefaulterId						
									END
									--ELSE
									--BEGIN
									----Create 2nd defaulter
									--END

								END
							END
							
							UPDATE
							[oso].[stg_PREWRIT_Cases]
							SET
							Imported   = 1
							, ImportedOn = GetDate()
							WHERE
							(Imported = 0 OR Imported IS NULL)
							AND ErrorId is NULL
							AND CCaseId = @CCaseId;

					-- New rows creation completed
						END
					COMMIT TRANSACTION
			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].stg_PREWRIT_Cases
					SET    ErrorId = @ErrorId
					WHERE
					--OneStepCaseNumber = @OneStepCaseNumber AND 
					ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				   @ClientCaseReference, 
					@TitleLiable1, 
					@FullnameLiable1, 
					@DOBLiable1, 
					@DebtAddress1,
					@DebtAddress2,
					@DebtAddress3,
					@DebtAddress4,
					@DebtAddress5,
					@DebtAddressPostcode, 
					@Phone1Liable1,
					@Phone2Liable1,
					@Phone3Liable1,
					@ClientDefaulterReference, 					
					@IssueDate, 
					@FineAmount, 
					@IssuingCourt,
					@Add1Liable1, 
					@Add2Liable1, 
					@Add3Liable1, 
					@Add4Liable1, 
					@Add5Liable1, 
					@AddPostCodeLiable1, 
					@IsBusinessAddress,
					@StartDate, 
					@EndDate, 
					@ClientComments, 
					@Email1Liable1, 
					@EmployerTelephone, 
					@OffenceCode,
					@OffenceDescription,
					@OffenceLocation, 
					@DebtAddressCountry, 
					@VehicleVRM,  
					@VehicleMake, 
					@VehicleModel,
					@TECDate, 
					@Currency,
					@FirstnameLiable1, 
					@MiddlenameLiable1, 
					@LastnameLiable1, 
					@CompanyNameLiable1,
					@NINOLiable1, 
					@MinorLiable1, 
					@AddCountryLiable1,
					@Phone4Liable1,
					@Phone5Liable1,
					@Email2Liable1,
					@Email3Liable1,
					@TitleLiable2, 
					@FirstnameLiable2, 
					@MiddlenameLiable2,
					@LastnameLiable2, 
					@FullnameLiable2, 
					@CompanyNameLiable2, 
					@DOBLiable2,
					@NINOLiable2, 
					@MinorLiable2,
					@Add1Liable2, 
					@Add2Liable2, 
					@Add3Liable2, 
					@Add4Liable2, 
					@Add5Liable2, 
					@AddCountryLiable2, 
					@AddPostCodeLiable2,
					@Phone1Liable2,
					@Phone2Liable2,
					@Phone3Liable2,
					@Phone4Liable2,
					@Phone5Liable2,
					@Email1Liable2,
					@Email2Liable2,
					@Email3Liable2,
					@TitleLiable3, 
					@FirstnameLiable3, 
					@MiddlenameLiable3,
					@LastnameLiable3, 
					@FullnameLiable3, 
					@CompanyNameLiable3, 
					@DOBLiable3,
					@NINOLiable3, 
					@MinorLiable3,
					@Add1Liable3,
					@Add2Liable3,
					@Add3Liable3,
					@Add4Liable3,
					@Add5Liable3,
					@AddCountryLiable3, 
					@AddPostCodeLiable3,
					@Phone1Liable3,
					@Phone2Liable3,
					@Phone3Liable3,
					@Phone4Liable3,
					@Phone5Liable3,
					@Email1Liable3,
					@Email2Liable3,
					@Email3Liable3,
					@TitleLiable4, 
					@FirstnameLiable4, 
					@MiddlenameLiable4,
					@LastnameLiable4, 
					@FullnameLiable4, 
					@CompanyNameLiable4, 
					@DOBLiable4, 
					@NINOLiable4, 
					@MinorLiable4,
					@Add1Liable4, 
					@Add2Liable4, 
					@Add3Liable4, 
					@Add4Liable4, 
					@Add5Liable4, 
					@AddCountryLiable4, 
					@AddPostCodeLiable4,
					@Phone1Liable4, 
					@Phone2Liable4, 
					@Phone3Liable4, 
					@Phone4Liable4, 
					@Phone5Liable4, 
					@Email1Liable4, 
					@Email2Liable4, 
					@Email3Liable4, 
					@TitleLiable5, 
					@FirstnameLiable5, 
					@MiddlenameLiable5,
					@LastnameLiable5, 
					@FullnameLiable5, 
					@CompanyNameLiable5, 
					@DOBLiable5, 
					@NINOLiable5, 
					@MinorLiable5,
					@Add1Liable5, 
					@Add2Liable5, 
					@Add3Liable5, 
					@Add4Liable5, 
					@Add5Liable5, 
					@AddCountryLiable5, 
					@AddPostCodeLiable5,
					@Phone1Liable5, 
					@Phone2Liable5, 
					@Phone3Liable5, 
					@Phone4Liable5, 
					@Phone5Liable5, 
					@Email1Liable5, 
					@Email2Liable5, 
					@Email3Liable5,
					@BatchDate, 
					@ClientId, 
					@ClientName,					 
					@Status, 
					@Phase, 
					@PhaseDate, 
					@Stage, 
					@StageDate, 
					@IsAssignable, 
					@OffenceDate, 
					@CreatedOn, 
					@CCaseId, 
					@DefaultersNames, 
					@BillNumber, 
					@EmployerName, 
					@EmployerAddressLine1, 
					@EmployerAddressLine2, 
					@EmployerAddressLine3, 
					@EmployerAddressLine4, 
					@EmployerPostcode, 
					@EmployerEmailAddress, 
					@EmployerFaxNumber, 
					@Occupation, 
					@RollNumber, 
					@BenefitIndicator, 
					@CaseNote,
					@Additional1, 
					@Additional2, 
					@Additional3, 
					@Additional4, 
					@Additional5, 
					@Additional6, 
					@Additional7, 
					@Additional8, 
					@Additional9, 
					@Additional10,
					@CasePrefix,
					@StatusId
        END
        CLOSE cursorT
        DEALLOCATE cursorT

		
		-- GO BACK AND CORRECT TIDREVIEWED = MANUAL IN CONTACTADDRESSES FOR LEAD DEFAULTERS WITH NON-MATCHING ADDRESS TO WARRANT ADDRESS
		--BEGIN TRANSACTION	
		--	UPDATE ca 
		--	Set TidReviewedId = 1
		--	from dbo.ContactAddresses ca
		--	join dbo.defaultercases dc on ca.defaulterid = dc.defaulterid
		--	join dbo.cases c on dc.caseid = c.id
		--	INNER join dbo.OldCaseNumbers oc on c.Id = oc.CaseId
		--    INNER join oso.stg_PREWRIT_Cases osc on osc.OneStepCaseNumber = oc.OldCaseNumber
		--	where 
		--	osc.Imported = 1 AND
		--	c.addressline1 = ca.addressline1 AND
		--	c.postcode = ca.postcode	
		--COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
