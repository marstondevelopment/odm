USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_GetCourtsXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_GetCourtsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT DISTINCT	
			[c].[Id] [CourtId],
			[c].[Name] [Court]	
		FROM
			[dbo].[Regions] [r]
			INNER JOIN [dbo].[Regions] [a] ON [r].[Id] = [a].[ParentId]
			INNER JOIN [dbo].[Regions] [c] ON [a].[Id] = [c].[ParentId]
		--select Id CourtId, Name Court from Regions where ParentId in (select Id from Regions where ParentId in (select Id from Regions where ParentId is null))
    END
END
GO
