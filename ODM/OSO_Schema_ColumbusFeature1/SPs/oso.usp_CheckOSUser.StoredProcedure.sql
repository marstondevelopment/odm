USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS user is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSUser]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @RecCount INT;				
		Select @RecCount = count(*) From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
		Select 
			CASE
				WHEN @RecCount = 0 THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO
