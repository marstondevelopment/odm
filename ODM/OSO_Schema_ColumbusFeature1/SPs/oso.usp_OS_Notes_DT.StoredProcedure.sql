USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Notes_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Notes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

				INSERT INTO dbo.[CaseNotes] 
				( [caseId]
					, [userId]
					, [officerId]
					, [text]
					, [occured]
					, [visible]
					, [groupId]
					, [TypeId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Text],
					[Occurred],
					1,
					NULL,
					NULL
				FROM
					oso.[Staging_OS_Notes]
				WHERE 
					Imported = 0

				UPDATE [oso].[Staging_OS_Notes]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0

				COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO
