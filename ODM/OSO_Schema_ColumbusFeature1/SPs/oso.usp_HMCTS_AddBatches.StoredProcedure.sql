USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_HMCTS_AddBatches]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_AddBatches]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			DECLARE @CurrentDate DateTime = GetDate();

            INSERT into batches
                (
                [referenceNumber],
                [batchDate],
                [closedDate],
                [createdBy],
                [manual],
                [deleted],
                [crossReferenceSent],
                [crossReferenceSentDate],
                [clientCaseTypeId],
                [loadedOn],
                [loadedBy],
                [receivedOn],
                [summaryDate]
                )
            SELECT DISTINCT
                CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112))[referenceNumber]
                ,CAST(CONVERT(date,osc.batchdate) AS DateTime) [batchDate]
                ,@CurrentDate [closedDate]
                ,2 [createdBy]
                ,1 [manual]
                ,0 [deleted]
                ,1 [crossReferenceSent]
                ,@CurrentDate [crossReferenceSentDate]
                ,cct.id [clientCaseTypeId]
                ,@CurrentDate [loadedOn]
                ,2 [loadedBy]
                ,@CurrentDate [receivedOn]
                ,NULL [summaryDate]
            FROM
                oso.stg_HMCTS_Cases osc
                join clientcasetype cct on osc.clientid = cct.clientid
            WHERE
                osc.imported = 0
                AND osc.ErrorID IS NULL
                AND osc.[BatchNoGenerated] = 0
                AND CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) NOT IN (
               
                    SELECT DISTINCT
                        b.[referenceNumber]
                    FROM
                        batches b
                        join clientcasetype cct on b.clientcasetypeid = cct.id
                        join oso.stg_HMCTS_Cases osc on b.batchDate = CAST(CONVERT(date,osc.batchdate) AS DateTime) AND cct.clientid = osc.ClientId
                    WHERE
                        CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) = b.referenceNumber
                                        AND osc.imported = 1
                        AND osc.[BatchNoGenerated] = 1
                )
            GROUP BY
                cct.id,
                cct.batchrefprefix,
                osc.batchdate

		END
    END
GO
