USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetAdditionalCaseData_UAT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_GetAdditionalCaseData_UAT]
@ParameterName nvarchar (50)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN 
		WITH AdditionalCaseDataCTE AS
		(SELECT 	
			CaseId,
			CASE Name WHEN 'FinalDefenceCost' THEN 'FinalCost' ELSE Name END Name,
			max(Value) Value,
			max(LoadedOn)LoadedOn
	
		FROM
			AdditionalCaseData
		GROUP BY
			CaseId,
			CASE Name WHEN 'FinalDefenceCost' THEN 'FinalCost' ELSE Name END)

		SELECT 
			c.clientCaseReference AS ClientCaseReference, 
			acd.Name AS ParameterName, 
			acd.Value AS ParameterValue  
		FROM 
			cases c WITH (NOLOCK)
			JOIN AdditionalCaseDataCTE acd  WITH (NOLOCK) ON c.Id = acd.CaseId 
		where 
			acd.[Name] in (@ParameterName)
	  
		UNION
			SELECT 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, Null ParameterName, Null ParameterValue		
    END
END
GO
