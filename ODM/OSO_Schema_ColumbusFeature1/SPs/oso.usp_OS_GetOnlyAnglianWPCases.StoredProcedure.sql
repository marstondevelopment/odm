USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetOnlyAnglianWPCases]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetOnlyAnglianWPCases]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT
			c.Id CCaseId, c.Id CaseId
		FROM Cases c
			INNER JOIN Batches ba ON c.batchId = ba.Id
			INNER JOIN ClientCaseType cct ON ba.clientCaseTypeId = cct.Id
			INNER JOIN Brands.Brands bb ON cct.brandId = bb.Id
		WHERE 
			cct.clientId = (select Id from Clients where name = 'Anglian Water Pre-Writ')
    END
END

GO
