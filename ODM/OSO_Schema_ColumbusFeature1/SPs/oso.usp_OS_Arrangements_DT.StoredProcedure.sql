USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Arrangements_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			DECLARE	@Reference VARCHAR(50),
					@Amount DECIMAL(18,4), 
					@FirstInstalmentAmount DECIMAL(18,4),
					@CFrequencyId INT,
					@PaymentType INT,
					@CUserId INT,
					@StatusId INT,
					@StartDate DATETIME,
					@LastEndPeriodDate DATETIME,
					@LastInstalmentDate DATETIME,
					@FirstInstalmentDate DATETIME,
					@ArrangementId INT,
					@NextInstalmentDate	DATETIME,
					@NoOfIntervals INT							
			DECLARE cursorT
				CURSOR FOR
					select distinct
						reference,
						amount,
						FirstInstalmentAmount,
						CFrequencyId,
						2,
						isnull(cuserid, 2),
						StatusId,
						StartDate,
						case
							when CFrequencyId = 1 then startdate
							when CFrequencyId in (2,4,5,6,7,8) then
								case
									when dateadd(day,-fr.NumberOfDays,nextinstalmentdate) > startdate then dateadd(day,-fr.NumberOfDays,nextinstalmentdate) 
									else startdate
								end
							when CFrequencyId in (3,9) then
								case
									when dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) > startdate then dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) 
									else startdate
								end
							else null
						end as LastEndPeriodDate,
						LastInstalmentDate,
						FirstInstalmentDate,
						NextInstalmentDate,
						case
							when CFrequencyId = 1 then 1
							else NoOfIntervals
						end
					from
						oso.[stg_OneStep_Arrangements] stg
						join InstalmentFrequency fr on fr.id = stg.CFrequencyId
					where
						imported = 0

				OPEN cursorT
				FETCH NEXT
					FROM
						cursorT
					INTO
						@Reference, 
						@Amount, 
						@FirstInstalmentAmount, 
						@CFrequencyId, 
						@PaymentType, 
						@CUserId,
						@StatusId, 
						@StartDate, 
						@LastEndPeriodDate,
						@LastInstalmentDate, 
						@FirstInstalmentDate,
						@NextInstalmentDate,
						@NoOfIntervals
						
				WHILE 
					@@FETCH_STATUS = 0
					BEGIN
						INSERT [dbo].[Arrangements] 
						(
							[reference],
							[amount],
							[firstInstalmentAmount],
							[frequencyId],
							[paymentMethodId], 
							[userId],
							[statusId],
							[startDate],
							[lastEndPeriodDate],
							[lastInstalmentDate],
							[FirstInstalmentDate],
							[DeferedPaymentDate],
							[payments]
						)
						VALUES
						(
							CAST(@Reference AS VARCHAR(50)), 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							ISNULL(@StartDate, @FirstInstalmentDate),
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						)
						SET @ArrangementId = SCOPE_IDENTITY();					
							

						INSERT dbo.CaseArrangements
						(
							[caseId],
							[arrangementId]
						)
						select
							cCaseId,
							@ArrangementId
						from
							oso.[stg_OneStep_Arrangements]
						where
							Reference = @Reference
							and Imported = 0

						FETCH NEXT
						FROM
							cursorT
						INTO
							@Reference, 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							@StartDate, 
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						
					END			
				CLOSE cursorT
				DEALLOCATE cursorT
			
			UPDATE [oso].[stg_OneStep_Arrangements]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO
