USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep payment type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemePaymentName]
      ,[OSSchemePaymentName]
	  ,[CSchemePaymentId]
  FROM [oso].[OneStep_SchemePayment_CrossRef]
    END
END
GO
