USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_PROD]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rituraj Choudhury
-- Create date: 11-08-2020
-- Description:	Extract CaseNumber, CaseId and ClientCaseReference using ClientName
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseNumberXMap_with_ClientName_PROD]
@ClientName nvarchar (max)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CaseId, c.ClientCaseReference, c.caseNumber as CaseNumber	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
    END
END
GO
