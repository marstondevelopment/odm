USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_LSC_GetHeaderFileIdXMapping_PROD]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		TN
-- Create date: 11-10-2021
-- Description:	Check if the LSC file already imported in Columbus
-- =============================================


CREATE PROCEDURE [oso].[usp_LSC_GetHeaderFileIdXMapping_PROD]
@FileId int,
@ClientName nvarchar (1000)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @MaxFileId INT, @RetVal INT = 1;

			select  @MaxFileId = MAX(cast (acd.Value AS int))
			from cases c 
				  inner join dbo.[Batches] b on c.batchid = b.id 
				  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
				  inner join dbo.Clients cl on cct.clientId = cl.Id 
				  inner join [dbo].[AdditionalCaseData] acd on c.Id = acd.CaseId
			where 
						cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
						AND acd.Name = 'HeaderFileId'

		SELECT @FileId AS HeaderFileId, @MaxFileId AS MaxHeaderFileId
    END
END
GO
