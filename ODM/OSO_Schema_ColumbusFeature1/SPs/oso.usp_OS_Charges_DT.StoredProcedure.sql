USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 08-04-2019
-- Description:	Copy data from staging table to CaseCharges table.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_DT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						INSERT INTO [dbo].[CaseCharges]( 
								[caseId]
								,[userId]
								,[officerId]
								,[amount]
								,[chargeDate]
								,[schemeChargeId]
								,[reversedOn]
								,[vatAmount]
								,[paidAmount]
								,[paidVatAmount]
								,[casePaymentId]
								,[refundId]
								,[CaseNoteId]
								,[isCaseBalanceAdjust]
								,[ClientInvoiceRunId])
						SELECT [cCaseId]
								,[CUserId]
								,[COfficerId]
								,[Amount]
								,[AddedOn]
								,[CSchemeChargeId]
								,NULL
								,0.00
								,[PaidAmount]
								,0.00
								,NULL
								,NULL
								,NULL
								,0
								,NULL
						FROM [oso].[Staging_OS_Charges] 
						WHERE 
							Imported = 0

						UPDATE [oso].[Staging_OS_Charges]
						SET Imported = 1, ImportedOn = GetDate()
						WHERE Imported = 0
					COMMIT TRANSACTION
					SELECT 1 as Succeed
				END TRY
				BEGIN CATCH        
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION 
					END

					INSERT INTO oso.[SQLErrors] 
					VALUES (Error_number(), 
							Error_severity(), 
							Error_state(), 
							Error_procedure(), 
							Error_line(), 
							Error_message(), 
							Getdate()) 

					SELECT 0 as Succeed
				END CATCH
	
    END
END
GO
