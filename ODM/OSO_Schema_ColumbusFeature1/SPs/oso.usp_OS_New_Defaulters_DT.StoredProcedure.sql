USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_New_Defaulters_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_New_Defaulters_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE                          
			@ClientDefaulterRef nvarchar(350) ,
			@TitleLiable1 nvarchar(350) ,
			@FirstnameLiable1 nvarchar(350) ,
			@MiddlenameLiable1 nvarchar(350) ,
			@LastnameLiable1 nvarchar(350) ,
			@FullnameLiable1 nvarchar(350) ,
			@CompanyNameLiable1 nvarchar(350) ,
			@AddPostCodeLiable1 nvarchar(350) ,
			@DebtAddressPostcode nvarchar(350) ,
			@TitleLiable2 nvarchar(350) ,
			@FirstnameLiable2 nvarchar(350) ,
			@LastNameLiable2 nvarchar(350) ,
			@DefaultersNames nvarchar(350) ,
			@DebtAddress1 nvarchar(350) ,
			@DebtAddress2 nvarchar(350) ,
			@DebtAddress3 nvarchar(350) ,
			@DebtAddress4 nvarchar(350) ,
			@DebtAddress5 nvarchar(350) ,
			@Add1Liable1 nvarchar(350) ,
			@Add2Liable1 nvarchar(350) ,
			@Add3Liable1 nvarchar(350) ,
			@Add4Liable1 nvarchar(350) ,
			@Add5Liable1 nvarchar(350) ,
			@AddCountryLiable1 nvarchar(350) ,
			@MiddlenameLiable2 nvarchar(350) ,
			@FullnameLiable2 nvarchar(350) ,
			@CompanyNameLiable2 nvarchar(350) ,
			@Add1Liable2 nvarchar(350) ,
			@Add2Liable2 nvarchar(350) ,
			@Add3Liable2 nvarchar(350) ,
			@Add4Liable2 nvarchar(350) ,
			@Add5Liable2 nvarchar(350) ,
			@AddCountryLiable2 nvarchar(350) ,
			@AddPostCodeLiable2 nvarchar(350) ,
			@TitleLiable3 nvarchar(350) ,
			@FirstnameLiable3 nvarchar(350) ,
			@MiddlenameLiable3 nvarchar(350) ,
			@LastnameLiable3 nvarchar(350) ,
			@FullnameLiable3 nvarchar(350) ,
			@CompanyNameLiable3 nvarchar(350) ,
			@Add1Liable3 nvarchar(350) ,
			@Add2Liable3 nvarchar(350) ,
			@Add3Liable3 nvarchar(350) ,
			@Add4Liable3 nvarchar(350) ,
			@Add5Liable3 nvarchar(350) ,
			@AddCountryLiable3 nvarchar(350) ,
			@AddPostCodeLiable3 nvarchar(350) ,
			@TitleLiable4 nvarchar(350) ,
			@FirstnameLiable4 nvarchar(350) ,
			@MiddlenameLiable4 nvarchar(350) ,
			@LastnameLiable4 nvarchar(350) ,
			@FullnameLiable4 nvarchar(350) ,
			@CompanyNameLiable4 nvarchar(350) ,
			@Add1Liable4 nvarchar(350) ,
			@Add2Liable4 nvarchar(350) ,
			@Add3Liable4 nvarchar(350) ,
			@Add4Liable4 nvarchar(350) ,
			@Add5Liable4 nvarchar(350) ,
			@AddCountryLiable4 nvarchar(350) ,
			@AddPostCodeLiable4 nvarchar(350) ,
			@TitleLiable5 nvarchar(350) ,
			@FirstnameLiable5 nvarchar(350) ,
			@MiddlenameLiable5 nvarchar(350) ,
			@LastnameLiable5 nvarchar(350) ,
			@FullnameLiable5 nvarchar(350) ,
			@CompanyNameLiable5 nvarchar(350) ,
			@Add1Liable5 nvarchar(350) ,
			@Add2Liable5 nvarchar(350) ,
			@Add3Liable5 nvarchar(350) ,
			@Add4Liable5 nvarchar(350) ,
			@Add5Liable5 nvarchar(350) ,
			@AddCountryLiable5 nvarchar(350) ,
			@AddPostCodeLiable5 nvarchar(350) ,
			@Address nvarchar(800) ,
			@ClientCaseReference nvarchar(350) ,
			@CaseId int ,
			@ClientId int ,
			@ClientName nvarchar(350) ,
			@EmployerAddressLine1 nvarchar(350) ,
			@EmployerAddressLine2 nvarchar(350) ,
			@EmployerAddressLine3 nvarchar(350) ,
			@EmployerAddressLine4 nvarchar(350) ,
			@EmployerPostcode nvarchar(350) ,
			@Imported bit ,
			@ImportedOn datetime ,
			@ErrorId int , 
														
			--@OffenceNumber nvarchar(1000) ,	



             @PreviousClientId      INT
            , @PreviousBatchDate     DateTime
            , @CurrentBatchId        INT
            , @UnPaidStatusId        INT
            , @ReturnedStatusId      INT
            , @StageId        INT
			, @ExpiredOn	DateTime
			, @LifeExpectancy INT
			, @NewCaseNumber varchar(7)
			, @CaseTypeId INT
			, @VTECDate	DateTime
			, @ANPR bit
			, @cnid INT 
			, @Succeed bit			
			, @OffenceCodeId int
			, @LeadDefaulterId int
			, @VehicleId int
			, @DefaulterId int
			, @CStatusId int
			,@ReturnRunId int
			,@IsBusinessAddress int
			,@CreatedOn datetime


        SET @CreatedOn = Getdate();
		SET @succeed = 1


		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END
		-- US Change 2
        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
		SELECT 
			   [ClientDefaulterRef]
			  ,[TitleLiable1]
			  ,[FirstnameLiable1]
			  ,[MiddlenameLiable1]
			  ,[LastnameLiable1]
			  ,[FullnameLiable1]
			  ,[CompanyNameLiable1]
			  ,[AddPostCodeLiable1]
			  ,[DebtAddressPostcode]
			  ,[TitleLiable2]
			  ,[FirstnameLiable2]
			  ,[LastNameLiable2]
			  ,[DefaultersNames]
			  ,[DebtAddress1]
			  ,[DebtAddress2]
			  ,[DebtAddress3]
			  ,[DebtAddress4]
			  ,[DebtAddress5]
			  ,[Add1Liable1]
			  ,[Add2Liable1]
			  ,[Add3Liable1]
			  ,[Add4Liable1]
			  ,[Add5Liable1]
			  ,[AddCountryLiable1]
			  ,[MiddlenameLiable2]
			  ,[FullnameLiable2]
			  ,[CompanyNameLiable2]
			  ,[Add1Liable2]
			  ,[Add2Liable2]
			  ,[Add3Liable2]
			  ,[Add4Liable2]
			  ,[Add5Liable2]
			  ,[AddCountryLiable2]
			  ,[AddPostCodeLiable2]
			  ,[TitleLiable3]
			  ,[FirstnameLiable3]
			  ,[MiddlenameLiable3]
			  ,[LastnameLiable3]
			  ,[FullnameLiable3]
			  ,[CompanyNameLiable3]
			  ,[Add1Liable3]
			  ,[Add2Liable3]
			  ,[Add3Liable3]
			  ,[Add4Liable3]
			  ,[Add5Liable3]
			  ,[AddCountryLiable3]
			  ,[AddPostCodeLiable3]
			  ,[TitleLiable4]
			  ,[FirstnameLiable4]
			  ,[MiddlenameLiable4]
			  ,[LastnameLiable4]
			  ,[FullnameLiable4]
			  ,[CompanyNameLiable4]
			  ,[Add1Liable4]
			  ,[Add2Liable4]
			  ,[Add3Liable4]
			  ,[Add4Liable4]
			  ,[Add5Liable4]
			  ,[AddCountryLiable4]
			  ,[AddPostCodeLiable4]
			  ,[TitleLiable5]
			  ,[FirstnameLiable5]
			  ,[MiddlenameLiable5]
			  ,[LastnameLiable5]
			  ,[FullnameLiable5]
			  ,[CompanyNameLiable5]
			  ,[Add1Liable5]
			  ,[Add2Liable5]
			  ,[Add3Liable5]
			  ,[Add4Liable5]
			  ,[Add5Liable5]
			  ,[AddCountryLiable5]
			  ,[AddPostCodeLiable5]
			  ,[Address]
			  ,[ClientCaseReference]
			  ,[CaseId]
			  ,[ClientId]
			  ,[ClientName]
			  ,[EmployerAddressLine1]
			  ,[EmployerAddressLine2]
			  ,[EmployerAddressLine3]
			  ,[EmployerAddressLine4]
			  ,[EmployerPostcode]
			  ,[Imported]
			  ,[ImportedOn]
			  ,[ErrorId]
		FROM 
				oso.[Staging_OS_NewDefaulters]
        WHERE
                 (Imported = 0 OR Imported IS NULL) AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
					 @ClientDefaulterRef
					,@TitleLiable1
					,@FirstnameLiable1
					,@MiddlenameLiable1
					,@LastnameLiable1
					,@FullnameLiable1
					,@CompanyNameLiable1
					,@AddPostCodeLiable1
					,@DebtAddressPostcode
					,@TitleLiable2
					,@FirstnameLiable2
					,@LastNameLiable2
					,@DefaultersNames
					,@DebtAddress1
					,@DebtAddress2
					,@DebtAddress3
					,@DebtAddress4
					,@DebtAddress5
					,@Add1Liable1
					,@Add2Liable1
					,@Add3Liable1
					,@Add4Liable1
					,@Add5Liable1
					,@AddCountryLiable1
					,@MiddlenameLiable2
					,@FullnameLiable2
					,@CompanyNameLiable2
					,@Add1Liable2
					,@Add2Liable2
					,@Add3Liable2
					,@Add4Liable2
					,@Add5Liable2
					,@AddCountryLiable2
					,@AddPostCodeLiable2
					,@TitleLiable3
					,@FirstnameLiable3
					,@MiddlenameLiable3
					,@LastnameLiable3
					,@FullnameLiable3
					,@CompanyNameLiable3
					,@Add1Liable3
					,@Add2Liable3
					,@Add3Liable3
					,@Add4Liable3
					,@Add5Liable3
					,@AddCountryLiable3
					,@AddPostCodeLiable3
					,@TitleLiable4
					,@FirstnameLiable4
					,@MiddlenameLiable4
					,@LastnameLiable4
					,@FullnameLiable4
					,@CompanyNameLiable4
					,@Add1Liable4
					,@Add2Liable4
					,@Add3Liable4
					,@Add4Liable4
					,@Add5Liable4
					,@AddCountryLiable4
					,@AddPostCodeLiable4
					,@TitleLiable5
					,@FirstnameLiable5
					,@MiddlenameLiable5
					,@LastnameLiable5
					,@FullnameLiable5
					,@CompanyNameLiable5
					,@Add1Liable5
					,@Add2Liable5
					,@Add3Liable5
					,@Add4Liable5
					,@Add5Liable5
					,@AddCountryLiable5
					,@AddPostCodeLiable5
					,@Address
					,@ClientCaseReference
					,@CaseId
					,@ClientId
					,@ClientName
					,@EmployerAddressLine1
					,@EmployerAddressLine2
					,@EmployerAddressLine3
					,@EmployerAddressLine4
					,@EmployerPostcode
					,@Imported
					,@ImportedOn
					,@ErrorId

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION


					-- 4) Insert Defaulters	
					--Lead Defaulter
					/*
					IF (@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DateOfBirth,
						0,
						0,
						NULL,
						0,
						@NINO,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Vehicle
						IF (@CaseTypeId = 9 AND @VehicleVRM IS NOT NULL)
							BEGIN
								INSERT INTO dbo.Vehicles (
									[vrm]
									,[vrmOwner]
									,[defaulterId]
									,[responseStatusId]
									,[responseDate]
									,[responseLoadedDate]
									,[keeperId]
									,[responseStringId]
									,[eventDate]
									,[licenceExpiry]
									,[exportDate]
									,[scrapDate]
									,[theftDate]
									,[recoveryDate]
									,[make]
									,[model]
									,[colour]
									,[taxClass]
									,[seatingCapacity]
									,[previousKeepers]
									,[Motability]
									,[Check]
									,[LastRequestedOn]
									,[isWarrantVrm]
									,[officerId]
									,[VrmReceivedOn]
								)
								VALUES
								(
									@VehicleVRM -- vrm
									,0 -- vrmOwner
									,@LeadDefaulterId
									,NULL -- responseStatusId
									,NULL -- responseDate
									,NULL -- responseLoadedDate
									,NULL -- keeperId
									,NULL -- responseStringId
									,NULL -- eventDate
									,NULL -- licenceExpiry
									,NULL -- exportDate
									,NULL -- scrapDate
									,NULL -- theftDate
									,NULL -- recoveryDate
									,NULL -- make
									,NULL -- model
									,NULL -- colour
									,NULL -- taxCl--s
									,NULL -- seatingCapacity
									,NULL -- previousKeepers
									,0 -- Motability
									,0 -- [Check]
									,NULL -- L--tRequestedOn
									,1 -- isWarrantVrm
									,NULL -- officerId
									,@CreatedOn -- VrmReceivedOn
									);

								SET @VehicleId = SCOPE_IDENTITY();

								INSERT INTO dbo.CaseVehicles
								(
									caseId,
									vehicleId
								)
								VALUES
								(
									@CaseId,
									@VehicleId
								)

							END

					END
					*/
					-- Defaulter2
					IF (@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							0, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case

						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

					END
					
					-- Defaulter3
					
					IF (@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							0, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
				    
					-- Defaulter4
					
					IF (@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							0, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
					
					-- Defaulter5
					
					IF (@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0)
					BEGIN
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,0 --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							CASE
								WHEN (@CaseTypeId = 21 OR @IsBusinessAddress = 1) then 1
								ELSE 0
								END, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							1,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)
					END
					

					--US Change 3
						UPDATE
						[oso].[Staging_OS_NewDefaulters]
						SET    Imported   = 1
						, ImportedOn = GetDate()
						WHERE
						Imported = 0 						
						AND ClientId = @ClientId
						AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[Staging_OS_NewDefaulters]
					SET    ErrorId = @ErrorId
					WHERE					
					  ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
					 @ClientDefaulterRef
					,@TitleLiable1
					,@FirstnameLiable1
					,@MiddlenameLiable1
					,@LastnameLiable1
					,@FullnameLiable1
					,@CompanyNameLiable1
					,@AddPostCodeLiable1
					,@DebtAddressPostcode
					,@TitleLiable2
					,@FirstnameLiable2
					,@LastNameLiable2
					,@DefaultersNames
					,@DebtAddress1
					,@DebtAddress2
					,@DebtAddress3
					,@DebtAddress4
					,@DebtAddress5
					,@Add1Liable1
					,@Add2Liable1
					,@Add3Liable1
					,@Add4Liable1
					,@Add5Liable1
					,@AddCountryLiable1
					,@MiddlenameLiable2
					,@FullnameLiable2
					,@CompanyNameLiable2
					,@Add1Liable2
					,@Add2Liable2
					,@Add3Liable2
					,@Add4Liable2
					,@Add5Liable2
					,@AddCountryLiable2
					,@AddPostCodeLiable2
					,@TitleLiable3
					,@FirstnameLiable3
					,@MiddlenameLiable3
					,@LastnameLiable3
					,@FullnameLiable3
					,@CompanyNameLiable3
					,@Add1Liable3
					,@Add2Liable3
					,@Add3Liable3
					,@Add4Liable3
					,@Add5Liable3
					,@AddCountryLiable3
					,@AddPostCodeLiable3
					,@TitleLiable4
					,@FirstnameLiable4
					,@MiddlenameLiable4
					,@LastnameLiable4
					,@FullnameLiable4
					,@CompanyNameLiable4
					,@Add1Liable4
					,@Add2Liable4
					,@Add3Liable4
					,@Add4Liable4
					,@Add5Liable4
					,@AddCountryLiable4
					,@AddPostCodeLiable4
					,@TitleLiable5
					,@FirstnameLiable5
					,@MiddlenameLiable5
					,@LastnameLiable5
					,@FullnameLiable5
					,@CompanyNameLiable5
					,@Add1Liable5
					,@Add2Liable5
					,@Add3Liable5
					,@Add4Liable5
					,@Add5Liable5
					,@AddCountryLiable5
					,@AddPostCodeLiable5
					,@Address
					,@ClientCaseReference
					,@CaseId
					,@ClientId
					,@ClientName
					,@EmployerAddressLine1
					,@EmployerAddressLine2
					,@EmployerAddressLine3
					,@EmployerAddressLine4
					,@EmployerPostcode
					,@Imported
					,@ImportedOn
					,@ErrorId

        END
        CLOSE cursorT
        DEALLOCATE cursorT

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
