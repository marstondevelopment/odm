USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_GetFrequencyId]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Arrangements_GetFrequencyId]
@Frequency int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT TOP 1 [CFrequencyId]
		FROM [oso].[OSColInstalmentFrequencyXRef]
		WHERE [OSFrequencyDays] = @Frequency
    END
END
GO
