USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_GetClientsXMapping]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Cases_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			c.[ConnId]
			,c.[CClientId] as ClientId
			,c.[ClientName]
			,CASE
			WHEN ct.name ='Council Tax Liability Order' THEN 'CT'
			WHEN ct.name ='National Non Domestic Rates' THEN 'ND'
			WHEN ct.name ='RTA/TMA (RoadTraffic/Traffic Management)' THEN 'PC'
			WHEN ct.name ='Child Maintenance Group' THEN 'CS'
			WHEN ct.name ='Legal Aid' THEN 'LAA'
			WHEN ct.name ='Debt Collection' THEN 'DCA'
		END AS OffenceCode

		FROM [oso].[OSColClentsXRef] c 
		INNER JOIN dbo.ClientCaseType cct ON c.CClientId = cct.clientId
		INNER JOIN dbo.CaseType ct ON cct.caseTypeId = ct.Id
    END
END
GO
