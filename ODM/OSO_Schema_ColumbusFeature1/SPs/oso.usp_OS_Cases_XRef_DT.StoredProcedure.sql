USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Cases from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_XRef_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

				INSERT INTO dbo.[OldCaseNumbers] (CaseId, OldCaseNumber)
				SELECT c.Id as CaseId, osC.OneStepCaseNumber as OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
				Except
				SELECT c.Id as CaseId, ocn.OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
					inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId AND osC.OneStepCaseNumber = ocn.OldCaseNumber)
				ORDER BY CaseId DESC

				COMMIT
				SELECT 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO
