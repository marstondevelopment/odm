USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_ConvertFixedStringAddress-WarrantAdd]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  LP
-- Create date: 12-11-2021
-- Description: Splits a Fixed Length String for upto five address lines in one string
-- =============================================

CREATE PROCEDURE  [oso].[usp_ConvertFixedStringAddress-WarrantAdd]

     @str NVARCHAR(Max)


AS

DECLARE @str1 VARCHAR(MAX)
DECLARE @str2 VARCHAR(MAX)
DECLARE @str3 VARCHAR(MAX)
DECLARE @str4 VARCHAR(MAX)
DECLARE @str5 VARCHAR(MAX)
BEGIN 
		SET @str1 = CASE	WHEN CHARINDEX('  ', @str) = 0 THEN @str
							ELSE LTRIM(RTRIM(LEFT(@str, CHARINDEX('  ', @str) ))) END
		SET @str = LTRIM(RTRIM(RIGHT(@str, LEN(@str)-CHARINDEX('  ', @str))))
--		SELECT @str1 AS [Str1]
--		SELECT @str
		SET @str2 = CASE	WHEN CHARINDEX('   ',@str) = 0 AND LTRIM(RTRIM(@str)) = @str1 THEN ''
							WHEN CHARINDEX('   ',@str) = 0 THEN LTRIM(RTRIM(@str))  
							ELSE LTRIM(RTRIM(LEFT(@str, CHARINDEX('  ', @str) ))) END
		SET @str = LTRIM(RIGHT(@str, LEN(@str)-CHARINDEX('  ', @str)))
--		SELECT @str2 AS [Str2]
		SET @str3 = CASE	WHEN CHARINDEX('   ',@str) = 0 AND (LTRIM(RTRIM(@str)) = @str1 OR LTRIM(RTRIM(@str)) = @str2) THEN ''
							WHEN CHARINDEX('   ',@str) = 0 THEN LTRIM(RTRIM(@str))  
							ELSE LTRIM(RTRIM(LEFT(@str, CHARINDEX('  ', @str) ))) END
		SET @str = LTRIM(RIGHT(@str, LEN(@str)-CHARINDEX('  ', @str)))
--		SELECT @str3 AS [Str3]
		SET @str4 = CASE	WHEN CHARINDEX('   ',@str) = 0 AND (LTRIM(RTRIM(@str)) = @str1 OR LTRIM(RTRIM(@str)) = @str1 OR 
															LTRIM(RTRIM(@str)) = @str2 OR LTRIM(RTRIM(@str)) = @str3) THEN ''
							WHEN CHARINDEX('   ',@str) = 0 THEN LTRIM(RTRIM(@str))  
							ELSE LTRIM(RTRIM(LEFT(@str, CHARINDEX('  ', @str) ))) END
		SET @str = LTRIM(RIGHT(@str, LEN(@str)-CHARINDEX('  ', @str)))
--		SELECT @str4 AS [Str4]
		SET @str5 = CASE WHEN CHARINDEX('   ',@str) = 0 AND (LTRIM(RTRIM(@str)) = @str1 OR LTRIM(RTRIM(@str)) = @str1 OR 
															LTRIM(RTRIM(@str)) = @str2 OR LTRIM(RTRIM(@str)) = @str3 OR LTRIM(RTRIM(@str)) = @str4) 
															THEN ''
							WHEN CHARINDEX('   ',@str) = 0 THEN LTRIM(RTRIM(@str))  
							ELSE LTRIM(RTRIM(LEFT(@str, CHARINDEX('  ', @str) ))) END
--		SELECT @str
--		SELECT @str5 AS [Str5]
		SET @str1 = CASE WHEN  @str1 ='' THEN '' ELSE @str1 END
		SET @str2 = CASE WHEN  @str2 ='' THEN '' ELSE CONCAT(', ', @str2) END
		SET @str3 = CASE WHEN  @str3 ='' THEN '' ELSE CONCAT(', ', @str3) END
		SET @str4 = CASE WHEN  @str4 ='' THEN '' ELSE CONCAT(', ', @str4) END
		SET @str5 = CASE WHEN  @str5 ='' THEN '' ELSE CONCAT(', ', @str5) END
		SELECT CONCAT(@str1, @str2, @str3, @str4, @str5) as WarrantAddress
		--SELECT CASE WHEN @str1 = '' THEN CONCAT(@str1, @str2, @str3, @str4, @str5) ELSE '' END as WarrantAddress


END
GO
