USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_AddDefaulterDetails_DT]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddDefaulterDetails_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE 
			@ClientDefaulterRef varchar(30),
			@ClientCaseReference varchar(200)  ,
			@IssueDate datetime  ,
			@FineAmount decimal(18, 4)  ,
			@StartDate datetime  ,
			@EndDate datetime  ,
			@OffenceCode varchar(5)  ,

			@ClientId int  ,
			@ClientName nvarchar(80)  ,
			@CreatedOn datetime  ,     
			@TitleLiable2 varchar(50) ,
			@FirstnameLiable2 nvarchar(50) ,
			@MiddlenameLiable2 nvarchar(50) ,
			@LastnameLiable2 nvarchar(50) ,
			@FullnameLiable2 nvarchar(50) ,
			@CompanyNameLiable2 nvarchar(50) ,
			@DOBLiable2 datetime ,
			@NINOLiable2 varchar(13) ,
			@MinorLiable2 bit ,
			@Add1Liable2 nvarchar(80) ,
			@Add2Liable2 nvarchar(320) ,
			@Add3Liable2 nvarchar(100) ,
			@Add4Liable2 nvarchar(100) ,
			@Add5Liable2 nvarchar(100) ,
			@AddCountryLiable2 int ,
			@AddPostCodeLiable2 varchar(12) ,
			@Phone1Liable2 varchar(50) ,
			@Phone2Liable2 varchar(50) ,
			@Phone3Liable2 varchar(50) ,
			@Phone4Liable2 varchar(50) ,
			@Phone5Liable2 varchar(50) ,
			@Email1Liable2 varchar(250) ,
			@Email2Liable2 varchar(250) ,
			@Email3Liable2 varchar(250) ,
			@TitleLiable3 varchar(50) ,
			@FirstnameLiable3 nvarchar(50) ,
			@MiddlenameLiable3 nvarchar(50) ,
			@LastnameLiable3 nvarchar(50) ,
			@FullnameLiable3 nvarchar(150) ,
			@CompanyNameLiable3 nvarchar(50) ,
			@DOBLiable3 datetime ,
			@NINOLiable3 varchar(13) ,
			@MinorLiable3 bit ,
			@Add1Liable3 nvarchar(80) ,
			@Add2Liable3 nvarchar(320) ,
			@Add3Liable3 nvarchar(100) ,
			@Add4Liable3 nvarchar(100) ,
			@Add5Liable3 nvarchar(100) ,
			@AddCountryLiable3 int ,
			@AddPostCodeLiable3 varchar(12) ,
			@Phone1Liable3 varchar(50) ,
			@Phone2Liable3 varchar(50) ,
			@Phone3Liable3 varchar(50) ,
			@Phone4Liable3 varchar(50) ,
			@Phone5Liable3 varchar(50) ,
			@Email1Liable3 varchar(250) ,
			@Email2Liable3 varchar(250) ,
			@Email3Liable3 varchar(250) ,
			@TitleLiable4 varchar(50) ,
			@FirstnameLiable4 nvarchar(50) ,
			@MiddlenameLiable4 nvarchar(50) ,
			@LastnameLiable4 nvarchar(50) ,
			@FullnameLiable4 nvarchar(150) ,
			@CompanyNameLiable4 nvarchar(50) ,
			@DOBLiable4 datetime ,
			@NINOLiable4 varchar(13) ,
			@MinorLiable4 bit ,
			@Add1Liable4 nvarchar(80) ,
			@Add2Liable4 nvarchar(320) ,
			@Add3Liable4 nvarchar(100) ,
			@Add4Liable4 nvarchar(100) ,
			@Add5Liable4 nvarchar(100) ,
			@AddCountryLiable4 int ,
			@AddPostCodeLiable4 varchar(12) ,
			@Phone1Liable4 varchar(50) ,
			@Phone2Liable4 varchar(50) ,
			@Phone3Liable4 varchar(50) ,
			@Phone4Liable4 varchar(50) ,
			@Phone5Liable4 varchar(50) ,
			@Email1Liable4 varchar(250) ,
			@Email2Liable4 varchar(250) ,
			@Email3Liable4 varchar(250) ,
			@TitleLiable5 varchar(50) ,
			@FirstnameLiable5 nvarchar(50) ,
			@MiddlenameLiable5 nvarchar(50) ,
			@LastnameLiable5 nvarchar(50) ,
			@FullnameLiable5 nvarchar(150) ,
			@CompanyNameLiable5 nvarchar(50) ,
			@DOBLiable5 datetime ,
			@NINOLiable5 varchar(13) ,
			@MinorLiable5 bit ,
			@Add1Liable5 nvarchar(80) ,
			@Add2Liable5 nvarchar(320) ,
			@Add3Liable5 nvarchar(100) ,
			@Add4Liable5 nvarchar(100) ,
			@Add5Liable5 nvarchar(100) ,
			@AddCountryLiable5 int ,
			@AddPostCodeLiable5 varchar(12) ,
			@Phone1Liable5 varchar(50) ,
			@Phone2Liable5 varchar(50) ,
			@Phone3Liable5 varchar(50) ,
			@Phone4Liable5 varchar(50) ,
			@Phone5Liable5 varchar(50) ,
			@Email1Liable5 varchar(250) ,
			@Email2Liable5 varchar(250) ,
			@Email3Liable5 varchar(250) ,
			@CaseId int,
			
			@Succeed bit,
			@ErrorId INT,
			@BatchRef varchar(500),
			@DefaulterId int,
			@IsBusinessAddress bit,
			@DPhoneId int,
			@DEmailId int;

		SET @succeed = 1

		IF CURSOR_STATUS('global','cursorKL')>=-1
		BEGIN
			DEALLOCATE cursorKL
		END

        DECLARE cursorKL CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			ClientDefaulterRef,
			ClientCaseReference,
			IssueDate  ,
			FineAmount ,
			StartDate ,
			EndDate ,
			OffenceCode ,
			ClientId ,
			ClientName    ,
			CreatedOn ,     
			TitleLiable2   ,
			FirstnameLiable2 ,
			MiddlenameLiable2 ,
			LastnameLiable2 ,
			FullnameLiable2 ,
			CompanyNameLiable2 ,
			DOBLiable2 ,
			NINOLiable2 ,
			MinorLiable2 ,
			Add1Liable2   ,
			Add2Liable2   ,
			Add3Liable2   ,
			Add4Liable2   ,
			Add5Liable2   ,
			AddCountryLiable2 ,
			AddPostCodeLiable2 ,
			Phone1Liable2   ,
			Phone2Liable2   ,
			Phone3Liable2   ,
			Phone4Liable2   ,
			Phone5Liable2   ,
			Email1Liable2   ,
			Email2Liable2   ,
			Email3Liable2   ,
			TitleLiable3   ,
			FirstnameLiable3   ,
			MiddlenameLiable3   ,
			LastnameLiable3   ,
			FullnameLiable3 ,
			CompanyNameLiable3   ,
			DOBLiable3  ,
			NINOLiable3 ,
			MinorLiable3 ,
			Add1Liable3   ,
			Add2Liable3   ,
			Add3Liable3   ,
			Add4Liable3   ,
			Add5Liable3   ,
			AddCountryLiable3 ,
			AddPostCodeLiable3 ,
			Phone1Liable3   ,
			Phone2Liable3   ,
			Phone3Liable3   ,
			Phone4Liable3   ,
			Phone5Liable3   ,
			Email1Liable3   ,
			Email2Liable3   ,
			Email3Liable3   ,
			TitleLiable4   ,
			FirstnameLiable4   ,
			MiddlenameLiable4   ,
			LastnameLiable4   ,
			FullnameLiable4 ,
			CompanyNameLiable4   ,
			DOBLiable4 ,
			NINOLiable4 ,
			MinorLiable4 ,
			Add1Liable4   ,
			Add2Liable4   ,
			Add3Liable4   ,
			Add4Liable4   ,
			Add5Liable4   ,
			AddCountryLiable4 ,
			AddPostCodeLiable4 ,
			Phone1Liable4   ,
			Phone2Liable4   ,
			Phone3Liable4   ,
			Phone4Liable4   ,
			Phone5Liable4   ,
			Email1Liable4   ,
			Email2Liable4   ,
			Email3Liable4   ,
			TitleLiable5   ,
			FirstnameLiable5 ,
			MiddlenameLiable5 ,
			LastnameLiable5 ,
			FullnameLiable5 ,
			CompanyNameLiable5 ,
			DOBLiable5 ,
			NINOLiable5 ,
			MinorLiable5 ,
			Add1Liable5   ,
			Add2Liable5   ,
			Add3Liable5   ,
			Add4Liable5   ,
			Add5Liable5   ,
			AddCountryLiable5 ,
			AddPostCodeLiable5 ,
			Phone1Liable5   ,
			Phone2Liable5   ,
			Phone3Liable5   ,
			Phone4Liable5   ,
			Phone5Liable5   ,
			Email1Liable5   ,
			Email2Liable5   ,
			Email3Liable5   ,
			CaseId


		FROM
                 [oso].[stg_KL_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorKL
        FETCH NEXT
        FROM
              cursorKL
        INTO
			@ClientDefaulterRef,
			@ClientCaseReference,
			@IssueDate  ,
			@FineAmount ,
			@StartDate ,
			@EndDate ,
			@OffenceCode ,
			@ClientId ,
			@ClientName    ,
			@CreatedOn ,     
			@TitleLiable2   ,
			@FirstnameLiable2 ,
			@MiddlenameLiable2 ,
			@LastnameLiable2 ,
			@FullnameLiable2 ,
			@CompanyNameLiable2 ,
			@DOBLiable2 ,
			@NINOLiable2 ,
			@MinorLiable2 ,
			@Add1Liable2   ,
			@Add2Liable2   ,
			@Add3Liable2   ,
			@Add4Liable2   ,
			@Add5Liable2   ,
			@AddCountryLiable2 ,
			@AddPostCodeLiable2 ,
			@Phone1Liable2   ,
			@Phone2Liable2   ,
			@Phone3Liable2   ,
			@Phone4Liable2   ,
			@Phone5Liable2   ,
			@Email1Liable2   ,
			@Email2Liable2   ,
			@Email3Liable2   ,
			@TitleLiable3   ,
			@FirstnameLiable3   ,
			@MiddlenameLiable3   ,
			@LastnameLiable3   ,
			@FullnameLiable3 ,
			@CompanyNameLiable3   ,
			@DOBLiable3  ,
			@NINOLiable3 ,
			@MinorLiable3 ,
			@Add1Liable3   ,
			@Add2Liable3   ,
			@Add3Liable3   ,
			@Add4Liable3   ,
			@Add5Liable3   ,
			@AddCountryLiable3 ,
			@AddPostCodeLiable3 ,
			@Phone1Liable3   ,
			@Phone2Liable3   ,
			@Phone3Liable3   ,
			@Phone4Liable3   ,
			@Phone5Liable3   ,
			@Email1Liable3   ,
			@Email2Liable3   ,
			@Email3Liable3   ,
			@TitleLiable4   ,
			@FirstnameLiable4   ,
			@MiddlenameLiable4   ,
			@LastnameLiable4   ,
			@FullnameLiable4 ,
			@CompanyNameLiable4   ,
			@DOBLiable4 ,
			@NINOLiable4 ,
			@MinorLiable4 ,
			@Add1Liable4   ,
			@Add2Liable4   ,
			@Add3Liable4   ,
			@Add4Liable4   ,
			@Add5Liable4   ,
			@AddCountryLiable4 ,
			@AddPostCodeLiable4 ,
			@Phone1Liable4   ,
			@Phone2Liable4   ,
			@Phone3Liable4   ,
			@Phone4Liable4   ,
			@Phone5Liable4   ,
			@Email1Liable4   ,
			@Email2Liable4   ,
			@Email3Liable4   ,
			@TitleLiable5   ,
			@FirstnameLiable5 ,
			@MiddlenameLiable5 ,
			@LastnameLiable5 ,
			@FullnameLiable5 ,
			@CompanyNameLiable5 ,
			@DOBLiable5 ,
			@NINOLiable5 ,
			@MinorLiable5 ,
			@Add1Liable5   ,
			@Add2Liable5   ,
			@Add3Liable5   ,
			@Add4Liable5   ,
			@Add5Liable5   ,
			@AddCountryLiable5 ,
			@AddPostCodeLiable5 ,
			@Phone1Liable5   ,
			@Phone2Liable5   ,
			@Phone3Liable5   ,
			@Phone4Liable5   ,
			@Phone5Liable5   ,
			@Email1Liable5   ,
			@Email2Liable5   ,
			@Email3Liable5   ,
			@CaseId


        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						-- Check and update BatchId
						SELECT @BatchRef = [referenceNumber] FROM [dbo].[Batches] WHERE Id = ( Select batchid From Cases WHERE id = @CaseId)

					-- 4) Insert Defaulters	
					--Lead Defaulter
					--IF ((@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0) OR (@CompanyNameLiable1 IS NOT NULL AND LEN(@CompanyNameLiable1) >0))
					--BEGIN
					--	SET @IsPrimary = 1
					--	SET @IsBusinessAddress = 0
					--	IF (@CompanyNameLiable1 IS NOT NULL AND LEN(@CompanyNameLiable1) >0)
					--	BEGIN
					--		SET @IsBusinessAddress = 1
					--		SET @FullnameLiable1 = @CompanyNameLiable1
					--	END
					--	INSERT INTO dbo.Defaulters (
					--		[name],
					--		[title],
					--		[firstName],
					--		[middleName],
					--		[lastName],
					--		[firstLine],
					--		[postCode],
					--		[address],
					--		[addressLine1],
					--		[addressLine2],
					--		[addressLine3],
					--		[addressLine4],
					--		[addressLine5],
					--		[countryId],
					--		[business],
					--		[forcesAddress],
					--		[isOverseas],
					--		[addressCreatedOn],
					--		[IsLeadCustomer]
					--		)

					--		VALUES
					--		(
					--		@FullnameLiable1 --[name],
					--		,@TitleLiable1  --[title],
					--		,@FirstnameLiable1 --[firstName],
					--		,@MiddlenameLiable1 --[middleName],
					--		,@LastnameLiable1 --[lastName],
					--		,@Add1Liable1 --[firstLine],
					--		,@AddPostcodeLiable1 --[postCode],
					--		, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
					--		,@Add1Liable1 --[addressLine1],
					--		,@Add2Liable1 --[addressLine2],
					--		,@Add3Liable1 --[addressLine3],
					--		,@Add4Liable1 --[addressLine4],
					--		,@Add5Liable1 --[addressLine5],
					--		,231 --[countryId],
					--		,@IsBusinessAddress --[business],
					--		,0 --[forcesAddress],
					--		,0 --[isOverseas],
					--		,@createdon --[addressCreatedOn],
					--		,1
					--		);
					--	Set @DefaulterId = SCOPE_IDENTITY();

					--	-- Insert Defaulter Details
					--	INSERT INTO dbo.DefaulterDetails (
					--	[DefaulterId],
					--	[BirthDate],
					--	[IsBirthDateConfirmed],
					--	[IsTreatedAsMinor], 
					--	[PotentialBirthDate],
					--	[IsPotentialBirthDateConfirmed],
					--	[NINO],
					--	[IsNINOConfirmed],
					--	[PotentialNINO],
					--	[IsPotentialNINOConfirmed],
					--	[BirthDateConfirmedBy],
					--	[NINOConfirmedBy])
					--	VALUES
					--	(
					--	@DefaulterId,
					--	@DOBLiable1,
					--	0,
					--	@MinorLiable1,
					--	NULL,
					--	0,
					--	@NINOLiable1,
					--	0,
					--	NULL,
					--	0,
					--	NULL,
					--	NULL
					--	)

					--	-- Insert Contact Address
					--	Insert Into ContactAddresses (
					--	[firstLine],
					--	[postCode],
					--	[address],
					--	[addressLine1],
					--	[addressLine2],
					--	[addressLine3],
					--	[addressLine4],
					--	[addressLine5],
					--	[countryId],
					--	[business],
					--	[defaulterId],
					--	[createdOn],
					--	[forcesAddress],
					--	[isOverseas],
					--	[StatusId],
					--	[IsPrimary],
					--	[IsConfirmed],
					--	[SourceId],
					--	[TidReviewedId])
					--	VALUES
					--	(
					--		@Add1Liable1,
					--		@AddPostcodeLiable1,
					--		RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
					--		@Add1Liable1, --[addressLine1],
					--		@Add2Liable1, --[addressLine2],
					--		@Add3Liable1, --[addressLine3],
					--		@Add4Liable1, --[addressLine4],
					--		NULL,
					--		231,
					--		@IsBusinessAddress, --[business],
					--		@DefaulterId,
					--		@CreatedOn,
					--		0,
					--		0,
					--		3,
					--		@IsPrimary,
					--		0,
					--		1,
					--		3
					--	)

					--	-- Insert Defaulter Case
					--	INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
					--	VALUES (@CaseId, @DefaulterId)





					-- Set the Imported flag to true in staging

					-- Defaulter2
					IF ((@FullnameLiable2 IS NOT NULL AND LEN(@FullnameLiable2) >0) OR (@CompanyNameLiable2 IS NOT NULL AND LEN(@CompanyNameLiable2) >0))
					BEGIN
						SET @IsBusinessAddress = 0
						IF (@CompanyNameLiable2 IS NOT NULL AND LEN(@CompanyNameLiable2) >0)
						BEGIN
							SET @IsBusinessAddress = 1
							SET @FullnameLiable2 = @CompanyNameLiable2
						END

						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable2 --[name],
							,@TitleLiable2  --[title],
							,@FirstnameLiable2 --[firstName],
							,@MiddlenameLiable2 --[middleName],
							,@LastnameLiable2 --[lastName],
							,@Add1Liable2 --[firstLine],
							,@AddPostcodeLiable2 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))) --[address]
							,@Add1Liable2 --[addressLine1],
							,@Add2Liable2 --[addressLine2],
							,@Add3Liable2 --[addressLine3],
							,@Add4Liable2 --[addressLine4],
							,@Add5Liable2 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress--[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@CreatedOn --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable2,
							@AddPostcodeLiable2,
							RTRIM(LTRIM(IsNull(@Add2Liable2,'') + ' ' + IsNull(@Add3Liable2,'') + ' ' + IsNull(@Add4Liable2,'') + ' ' + IsNull(@Add5Liable2,''))), --[address]
							@Add1Liable2, --[addressLine1],
							@Add2Liable2, --[addressLine2],
							@Add3Liable2, --[addressLine3],
							@Add4Liable2, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							0,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable2 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Phone1Liable2,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable2 IS NOT NULL)
						BEGIN
							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone2Liable2
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone2Liable2,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone3Liable2 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone3Liable2
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone3Liable2,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone4Liable2 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone4Liable2
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone4Liable2,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone5Liable2 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone5Liable2
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone5Liable2,
									2,
									@CreatedOn,
									1
								)
							END
						END

						-- Insert Defaulter Email
						IF (@Email1Liable2 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Email1Liable2,
								@CreatedOn,
								1
							)
						END

						IF (@Email2Liable2 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email2Liable2
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email2Liable2,
									@CreatedOn,
									1
								)
							END			
						END

						IF (@Email3Liable2 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email3Liable2
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email3Liable2,
									@CreatedOn,
									1
								)
							END			
						END

					END

					-- Defaulter3
					IF ((@FullnameLiable3 IS NOT NULL AND LEN(@FullnameLiable3) >0) OR (@CompanyNameLiable3 IS NOT NULL AND LEN(@CompanyNameLiable3) >0))
					BEGIN
						SET @IsBusinessAddress = 0
						IF (@CompanyNameLiable3 IS NOT NULL AND LEN(@CompanyNameLiable3) >0)
						BEGIN
							SET @IsBusinessAddress = 1
							SET @FullnameLiable3 = @CompanyNameLiable3
						END
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable3 --[name],
							,@TitleLiable3  --[title],
							,@FirstnameLiable3 --[firstName],
							,@MiddlenameLiable3 --[middleName],
							,@LastnameLiable3 --[lastName],
							,@Add1Liable3 --[firstLine],
							,@AddPostcodeLiable3 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))) --[address]
							,@Add1Liable3 --[addressLine1],
							,@Add2Liable3 --[addressLine2],
							,@Add3Liable3 --[addressLine3],
							,@Add4Liable3 --[addressLine4],
							,@Add5Liable3 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable3,
							@AddPostcodeLiable3,
							RTRIM(LTRIM(IsNull(@Add2Liable3,'') + ' ' + IsNull(@Add3Liable3,'') + ' ' + IsNull(@Add4Liable3,'') + ' ' + IsNull(@Add5Liable3,''))), --[address]
							@Add1Liable3, --[addressLine1],
							@Add2Liable3, --[addressLine2],
							@Add3Liable3, --[addressLine3],
							@Add4Liable3, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							0,
							0,
							1,
							1
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable3 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Phone1Liable3,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable3 IS NOT NULL)
						BEGIN
							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone2Liable3
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone2Liable3,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone3Liable3 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone3Liable3
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone3Liable3,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone4Liable3 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone4Liable3
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone4Liable3,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone5Liable3 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone5Liable3
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone5Liable3,
									2,
									@CreatedOn,
									1
								)
							END
						END

						-- Insert Defaulter Email
						IF (@Email1Liable3 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Email1Liable3,
								@CreatedOn,
								1
							)
						END

						IF (@Email2Liable3 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email2Liable3
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email2Liable3,
									@CreatedOn,
									1
								)
							END			
						END

						IF (@Email3Liable3 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email3Liable3
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email3Liable3,
									@CreatedOn,
									1
								)
							END			
						END
					END
			
					-- Defaulter4
					IF ((@FullnameLiable4 IS NOT NULL AND LEN(@FullnameLiable4) >0) OR (@CompanyNameLiable4 IS NOT NULL AND LEN(@CompanyNameLiable4) >0))
					BEGIN
						SET @IsBusinessAddress = 0
						IF (@CompanyNameLiable4 IS NOT NULL AND LEN(@CompanyNameLiable4) >0)
						BEGIN
							SET @IsBusinessAddress = 1
							SET @FullnameLiable4 = @CompanyNameLiable4
						END
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable4 --[name],
							,@TitleLiable4  --[title],
							,@FirstnameLiable4 --[firstName],
							,@MiddlenameLiable4 --[middleName],
							,@LastnameLiable4 --[lastName],
							,@Add1Liable4 --[firstLine],
							,@AddPostcodeLiable4 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))) --[address]
							,@Add1Liable4 --[addressLine1],
							,@Add2Liable4 --[addressLine2],
							,@Add3Liable4 --[addressLine3],
							,@Add4Liable4 --[addressLine4],
							,@Add5Liable4 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);

						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable4,
							@AddPostcodeLiable4,
							RTRIM(LTRIM(IsNull(@Add2Liable4,'') + ' ' + IsNull(@Add3Liable4,'') + ' ' + IsNull(@Add4Liable4,'') + ' ' + IsNull(@Add5Liable4,''))), --[address]
							@Add1Liable4, --[addressLine1],
							@Add2Liable4, --[addressLine2],
							@Add3Liable4, --[addressLine3],
							@Add4Liable4, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							0,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable4 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Phone1Liable4,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable4 IS NOT NULL)
						BEGIN
							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone2Liable4
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone2Liable4,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone3Liable4 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone3Liable4
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone3Liable4,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone4Liable4 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone4Liable4
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone4Liable4,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone5Liable4 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone5Liable4
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone5Liable4,
									2,
									@CreatedOn,
									1
								)
							END
						END

						-- Insert Defaulter Email
						IF (@Email1Liable4 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Email1Liable4,
								@CreatedOn,
								1
							)
						END

						IF (@Email2Liable4 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email2Liable4
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email2Liable4,
									@CreatedOn,
									1
								)
							END			
						END

						IF (@Email3Liable4 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email3Liable4
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email3Liable4,
									@CreatedOn,
									1
								)
							END			
						END
					END

					-- Defaulter5
					IF ((@FullnameLiable5 IS NOT NULL AND LEN(@FullnameLiable5) >0) OR (@CompanyNameLiable5 IS NOT NULL AND LEN(@CompanyNameLiable5) >0))
					BEGIN
						SET @IsBusinessAddress = 0
						IF (@CompanyNameLiable5 IS NOT NULL AND LEN(@CompanyNameLiable5) >0)
						BEGIN
							SET @IsBusinessAddress = 1
							SET @FullnameLiable5 = @CompanyNameLiable5
						END
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable5 --[name],
							,@TitleLiable5  --[title],
							,@FirstnameLiable5 --[firstName],
							,@MiddlenameLiable5 --[middleName],
							,@LastnameLiable5 --[lastName],
							,@Add1Liable5 --[firstLine],
							,@AddPostcodeLiable5 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))) --[address]
							,@Add1Liable5 --[addressLine1],
							,@Add2Liable5 --[addressLine2],
							,@Add3Liable5 --[addressLine3],
							,@Add4Liable5 --[addressLine4],
							,@Add5Liable5 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,0
							);
						Set @DefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor],
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@DefaulterId,
						NULL,
						0,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable5,
							@AddPostcodeLiable5,
							RTRIM(LTRIM(IsNull(@Add2Liable5,'') + ' ' + IsNull(@Add3Liable5,'') + ' ' + IsNull(@Add4Liable5,'') + ' ' + IsNull(@Add5Liable5,''))), --[address]
							@Add1Liable5, --[addressLine1],
							@Add2Liable5, --[addressLine2],
							@Add3Liable5, --[addressLine3],
							@Add4Liable5, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@DefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							0,
							0,
							1,
							1
						)
						
						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @DefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable5 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Phone1Liable5,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable5 IS NOT NULL)
						BEGIN
							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone2Liable5
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone2Liable5,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone3Liable5 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone3Liable5
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone3Liable5,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone4Liable5 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone4Liable5
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone4Liable5,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone5Liable5 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @DefaulterId AND phone = @Phone5Liable5
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Phone5Liable5,
									2,
									@CreatedOn,
									1
								)
							END
						END

						-- Insert Defaulter Email
						IF (@Email1Liable5 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@DefaulterId,
								@Email1Liable5,
								@CreatedOn,
								1
							)
						END

						IF (@Email2Liable5 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email2Liable5
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email2Liable5,
									@CreatedOn,
									1
								)
							END			
						END

						IF (@Email3Liable5 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @DefaulterId AND Email = @Email3Liable5
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@DefaulterId,
									@Email3Liable5,
									@CreatedOn,
									1
								)
							END			
						END

					END

					UPDATE
					[oso].[stg_KL_Cases]
					SET
					[BatchReferenceNo] = @BatchRef
					,Imported   = 1
					, ImportedOn = GetDate()
					, CaseId = @CaseId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_KL_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND CaseId = @CaseId;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorKL
            INTO
				@ClientDefaulterRef,
				@ClientCaseReference,
				@IssueDate  ,
				@FineAmount ,
				@StartDate ,
				@EndDate ,
				@OffenceCode ,
				@ClientId ,
				@ClientName    ,
				@CreatedOn ,     
				@TitleLiable2   ,
				@FirstnameLiable2 ,
				@MiddlenameLiable2 ,
				@LastnameLiable2 ,
				@FullnameLiable2 ,
				@CompanyNameLiable2 ,
				@DOBLiable2 ,
				@NINOLiable2 ,
				@MinorLiable2 ,
				@Add1Liable2   ,
				@Add2Liable2   ,
				@Add3Liable2   ,
				@Add4Liable2   ,
				@Add5Liable2   ,
				@AddCountryLiable2 ,
				@AddPostCodeLiable2 ,
				@Phone1Liable2   ,
				@Phone2Liable2   ,
				@Phone3Liable2   ,
				@Phone4Liable2   ,
				@Phone5Liable2   ,
				@Email1Liable2   ,
				@Email2Liable2   ,
				@Email3Liable2   ,
				@TitleLiable3   ,
				@FirstnameLiable3   ,
				@MiddlenameLiable3   ,
				@LastnameLiable3   ,
				@FullnameLiable3 ,
				@CompanyNameLiable3   ,
				@DOBLiable3  ,
				@NINOLiable3 ,
				@MinorLiable3 ,
				@Add1Liable3   ,
				@Add2Liable3   ,
				@Add3Liable3   ,
				@Add4Liable3   ,
				@Add5Liable3   ,
				@AddCountryLiable3 ,
				@AddPostCodeLiable3 ,
				@Phone1Liable3   ,
				@Phone2Liable3   ,
				@Phone3Liable3   ,
				@Phone4Liable3   ,
				@Phone5Liable3   ,
				@Email1Liable3   ,
				@Email2Liable3   ,
				@Email3Liable3   ,
				@TitleLiable4   ,
				@FirstnameLiable4   ,
				@MiddlenameLiable4   ,
				@LastnameLiable4   ,
				@FullnameLiable4 ,
				@CompanyNameLiable4   ,
				@DOBLiable4 ,
				@NINOLiable4 ,
				@MinorLiable4 ,
				@Add1Liable4   ,
				@Add2Liable4   ,
				@Add3Liable4   ,
				@Add4Liable4   ,
				@Add5Liable4   ,
				@AddCountryLiable4 ,
				@AddPostCodeLiable4 ,
				@Phone1Liable4   ,
				@Phone2Liable4   ,
				@Phone3Liable4   ,
				@Phone4Liable4   ,
				@Phone5Liable4   ,
				@Email1Liable4   ,
				@Email2Liable4   ,
				@Email3Liable4   ,
				@TitleLiable5   ,
				@FirstnameLiable5 ,
				@MiddlenameLiable5 ,
				@LastnameLiable5 ,
				@FullnameLiable5 ,
				@CompanyNameLiable5 ,
				@DOBLiable5 ,
				@NINOLiable5 ,
				@MinorLiable5 ,
				@Add1Liable5   ,
				@Add2Liable5   ,
				@Add3Liable5   ,
				@Add4Liable5   ,
				@Add5Liable5   ,
				@AddCountryLiable5 ,
				@AddPostCodeLiable5 ,
				@Phone1Liable5   ,
				@Phone2Liable5   ,
				@Phone3Liable5   ,
				@Phone4Liable5   ,
				@Phone5Liable5   ,
				@Email1Liable5   ,
				@Email2Liable5   ,
				@Email3Liable5   ,
				@CaseId
        END
        CLOSE cursorKL
        DEALLOCATE cursorKL

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO
