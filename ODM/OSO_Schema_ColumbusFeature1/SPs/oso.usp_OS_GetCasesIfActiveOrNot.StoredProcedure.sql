USE [ColumbusFeature1]
GO
/****** Object:  StoredProcedure [oso].[usp_OS_GetCasesIfActiveOrNot]    Script Date: 29/06/2022 16:34:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCasesIfActiveOrNot]  
@CCaseId INT, @maat_id VARCHAR(20)
AS
BEGIN
	SELECT c.Id CCaseId, cs.active IsActive
	FROM 
		dbo.Cases c
		inner join dbo.CaseStatus cs ON c.statusId = cs.Id
	
	WHERE c.clientcasereference = @maat_id and c.Id = @CCaseId
END
GO
