--USE [FUT]
--GO

--IF NOT EXISTS ( SELECT  *
--                FROM    sys.schemas
--                WHERE   name = N'dbo' )
--    EXEC('CREATE SCHEMA [dbo]');
--GO

/****** Object:  View [dbo].[vw_LatestUserEvents]    Script Date: 27/05/2020 16:15:56 ******/
DROP VIEW IF EXISTS [dbo].[vw_LatestUserEvents]
GO

/****** Object:  View [dbo].[vw_LatestUserEvents]    Script Date: 27/05/2020 16:15:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vw_LatestUserEvents]
AS
SELECT        TOP (1000) u.UserId, ft.Name AS FileTemplateName, 
                         CASE WHEN el.Action = 1 THEN 'Validate' WHEN el.Action = 2 THEN 'Bulk Upload' WHEN el.Action = 3 THEN 'Data Transformation' WHEN el.Action = 4 THEN 'Data Pre-processing' WHEN el.Action = 5 THEN 'Upload To Repository'
                          ELSE 'Unknown Action' END AS UserAction, el.Id, el.UserId AS Expr1, el.IPAddress, el.FileTemplateId, el.Action, el.FileName, el.ProcessStartedOn, el.ProcessEndedOn, el.Succeeded, el.Note
FROM            dbo.EventLog AS el INNER JOIN
                         dbo.[User] AS u ON u.Id = el.UserId INNER JOIN
                         dbo.FileTemplate AS ft ON ft.Id = el.FileTemplateId
ORDER BY el.Id DESC
GO


/****** Object:  Table [dbo].[SQLErrors]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE [dbo].[SQLErrors]
GO

/****** Object:  Table [dbo].[SQLErrors]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SQLErrors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ErrorNumber] [int] NULL,
	[Severity] [int] NULL,
	[State] [int] NULL,
	[ErrorProcedure] [nvarchar](128) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[ExecutedOn] [datetime] NULL,
 CONSTRAINT [PK_SQLErrors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[Stg_FileTemplate]    Script Date: 25/02/2020 10:35:05 ******/
DROP TABLE [dbo].[Stg_FileTemplate]
GO

/****** Object:  Table [dbo].[Stg_FileTemplate]    Script Date: 25/02/2020 10:35:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Stg_FileTemplate](
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[DBConnectionId] [int] NULL,
	[MimeTypeId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[BulkUpload] [bit] NOT NULL DEFAULT 0,
	[BUStagingTable] [nvarchar](50) NULL,
	[TransformData] [bit] NOT NULL DEFAULT 0,
	[DTStoredProcedureId] [int] NULL,
	[DTFileWriter] [int] NULL  DEFAULT 2,
	[RemoveHeader] [bit] NOT NULL  DEFAULT 0,
	[RemoveFooter] [bit] NOT NULL DEFAULT 0,
	[NoColumnName] [bit] NOT NULL DEFAULT 0,
	[DelimiterId] [int] NULL,
	[GenerateOutputFile] [bit] NOT NULL DEFAULT 1,
	[OutputFileDelimiterId] [int] NULL DEFAULT 1,
	[XMLRootNodeXPath] [nvarchar](500) NULL,
	[HeaderRowsToSkip] [int] NULL,
	[FooterRowsToSkip] [int] NULL,
	[OFExcludeColumnHeader] [bit] NOT NULL DEFAULT 0,
	[OutputFileEncoding] [int] NULL DEFAULT 1,
	[UploadToFileRepository] [bit] NOT NULL DEFAULT 1,
	[RepositoryFilePath] [varchar](500) NULL,
	[FNPAsSubfolder] [bit] NOT NULL DEFAULT 0,
	[FNTimestamp] [bit] NOT NULL DEFAULT 0,
	[OverrideDefaultDTActionSequence] [bit] NOT NULL DEFAULT 1,
	[UserName] [nvarchar](100) NOT NULL,
	[Imported] [bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
)

GO

 /****** Object:  StoredProcedure [dbo].[usp_OS_FileTemplates_DT]    Script Date: 05/12/2019 11:46:04 ******/
DROP PROCEDURE [dbo].[usp_OS_FileTemplates_DT]
GO

/****** Object:  StoredProcedure [dbo].[usp_OS_FileTemplates_DT]    Script Date: 05/12/2019 11:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import file templates from OS Data File
-- =============================================
CREATE PROCEDURE [dbo].[usp_OS_FileTemplates_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@Name [nvarchar](250) ,
					@Description [nvarchar](250),
					@DBConnectionId [int] ,
					@MimeTypeId [int]  ,
					@ClientId [int]  ,
					@BulkUpload [bit]   ,
					@BUStagingTable [nvarchar](50) ,
					@TransformData [bit]   ,
					@DTStoredProcedureId [int] ,
					@DTFileWriter [int]   ,
					@RemoveHeader [bit]    ,
					@RemoveFooter [bit]   ,
					@NoColumnName [bit]   ,
					@DelimiterId [int] ,
					@GenerateOutputFile [bit]   ,
					@OutputFileDelimiterId [int]  ,
					@XMLRootNodeXPath [nvarchar](500) ,
					@HeaderRowsToSkip [int] ,
					@FooterRowsToSkip [int] ,
					@OFExcludeColumnHeader [bit]   ,
					@OutputFileEncoding [int]  ,
					@UploadToFileRepository [bit]   ,
					@RepositoryFilePath [varchar](500) ,
					@FNPAsSubfolder [bit]   ,
					@FNTimestamp [bit]   ,
					@OverrideDefaultDTActionSequence [bit],
					@UserName [nvarchar](100),

					@FileTemplateId [int],
					@UserId [int],
					@MaxDFId [int]


				
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT 
						[Name]
					  ,[Description]
					  ,[DBConnectionId]
					  ,[MimeTypeId]
					  ,[ClientId]
					  ,[BulkUpload]
					  ,[BUStagingTable]
					  ,[TransformData]
					  ,[DTStoredProcedureId]
					  ,[DTFileWriter]
					  ,[RemoveHeader]
					  ,[RemoveFooter]
					  ,[NoColumnName]
					  ,[DelimiterId]
					  ,[GenerateOutputFile]
					  ,[OutputFileDelimiterId]
					  ,[XMLRootNodeXPath]
					  ,[HeaderRowsToSkip]
					  ,[FooterRowsToSkip]
					  ,[OFExcludeColumnHeader]
					  ,[OutputFileEncoding]
					  ,[UploadToFileRepository]
					  ,[RepositoryFilePath]
					  ,[FNPAsSubfolder]
					  ,[FNTimestamp]
					  ,[OverrideDefaultDTActionSequence]
					  ,[UserName]
                    FROM
                           dbo.[Stg_FileTemplate]
					WHERE Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@Name ,
						@Description,
						@DBConnectionId ,
						@MimeTypeId ,
						@ClientId ,
						@BulkUpload ,
						@BUStagingTable ,
						@TransformData,
						@DTStoredProcedureId,
						@DTFileWriter ,
						@RemoveHeader ,
						@RemoveFooter ,
						@NoColumnName ,
						@DelimiterId ,
						@GenerateOutputFile ,
						@OutputFileDelimiterId ,
						@XMLRootNodeXPath ,
						@HeaderRowsToSkip ,
						@FooterRowsToSkip ,
						@OFExcludeColumnHeader ,
						@OutputFileEncoding ,
						@UploadToFileRepository ,
						@RepositoryFilePath ,
						@FNPAsSubfolder ,
						@FNTimestamp ,
						@OverrideDefaultDTActionSequence,
						@UserName 

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
						SELECT @UserId = Id FROM [dbo].[User] WHERE UserId = @UserName
						SELECT @MaxDFId = Max(Id) From DataField 
							INSERT INTO dbo.[FileTemplate] (
								[Name],
								[Description],
								[CreatedBy],
								[DateCreated],
								[UpdatedBy],
								[DateUpdated],
								[DBConnectionId] ,
								[Disabled],
								[MimeTypeId] ,
								[ClientId] ,
								[BulkUpload] ,
								[BUStagingTable] ,
								[TransformData],
								[DTStoredProcedureId],
								[DTFileWriter] ,
								[RemoveHeader] ,
								[RemoveFooter] ,
								[NoColumnName] ,
								[DelimiterId] ,
								[GenerateOutputFile] ,
								[OutputFileDelimiterId] ,
								[XMLRootNodeXPath] ,
								[HeaderRowsToSkip] ,
								[FooterRowsToSkip] ,
								[OFExcludeColumnHeader] ,
								[OutputFileEncoding] ,
								[UploadToFileRepository] ,
								[RepositoryFilePath] ,
								[FNPAsSubfolder] ,
								[FNTimestamp] ,
								[UseXsd],
								[OverrideDefaultDTActionSequence]

							)
							VALUES (
								@Name ,
								@Description,
								@UserId,
								GetDate(),
								@UserId,
								GetDate(),
								@DBConnectionId ,
								0,
								@MimeTypeId ,
								@ClientId ,
								@BulkUpload ,
								@BUStagingTable ,
								@TransformData,
								@DTStoredProcedureId,
								@DTFileWriter ,
								@RemoveHeader ,
								@RemoveFooter ,
								@NoColumnName ,
								@DelimiterId ,
								@GenerateOutputFile ,
								@OutputFileDelimiterId ,
								@XMLRootNodeXPath ,
								@HeaderRowsToSkip ,
								@FooterRowsToSkip ,
								@OFExcludeColumnHeader ,
								@OutputFileEncoding ,
								@UploadToFileRepository ,
								@RepositoryFilePath ,
								@FNPAsSubfolder ,
								@FNTimestamp ,
								0,
								@OverrideDefaultDTActionSequence
							)

							SET @FileTemplateId = SCOPE_IDENTITY();

							INSERT INTO DataField 
							(
								[Name],
								[Description],		
								[FieldType],	
								[CreatedBy],		
								[DateCreated],		
								[UpdatedBy],		
								[DateUpdated],		
								[Disabled],	
								[ExcludeFromOutput],	
								[OutputColumnName],		
								[XMLChildNodeXPath],	
								[HasMultipleNodes],	
								[OutputFieldOnly],	
								[FileSection],	
								[OutputFormat],		
								[DefaultValue],		
								[CoordinateForVariable]
							)


							VALUES  			
								('ClientCaseReference',	'OS - ' + @Name + ' - ClientCaseReference',5,@UserId,GetDate(),@UserId,GetDate(),0,	0,'ClientCaseReference',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaultersNames','OS - ' + @Name + ' - DefaultersNames',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DefaultersNames',NULL,0,0,1,NULL,NULL,NULL),			
								('WarrantAddressLine1','OS - ' + @Name + ' - WarrantAddressLine1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddress1',NULL,0,0,1,NULL,NULL,NULL),			
								('WarrantAddressLine2','OS - ' + @Name + ' - WarrantAddressLine2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddress2',NULL,0,0,1,NULL,NULL,NULL),			
								('WarrantAddressLine3','OS - ' + @Name + ' - WarrantAddressLine3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddress3',NULL,0,1,1,NULL,NULL,NULL),			
								('WarrantAddressLine4','OS - ' + @Name + ' - WarrantAddressLine4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddress4',NULL,0,0,1,NULL,NULL,NULL),			
								('WarrantAddressLine5','OS - ' + @Name + ' - WarrantAddressLine5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddress5',NULL,0,0,1,NULL,NULL,NULL),			
								('WarrantPostcode','OS - ' + @Name + ' - WarrantPostcode',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddressPostcode',NULL,0,0,1,NULL,NULL,NULL),			
								('OriginalFineAmount','OS - ' + @Name + ' - OriginalFineAmount',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FineAmount',NULL,0,0,1,	'0',NULL,NULL),			
								('Empty-USCol10','OS - ' + @Name + ' - Empty-Shashi',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'Empty-USCol10',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaulterAddressLine1','OS - ' + @Name + ' - DefaulterAddressLine1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add1Liable1',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaulterAddressLine2','OS - ' + @Name + ' - DefaulterAddressLine2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add2Liable1',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaulterAddressLine3','OS - ' + @Name + ' - DefaulterAddressLine3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add3Liable1',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaulterAddressLine4','OS - ' + @Name + ' - DefaulterAddressLine4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add4Liable1',NULL,0,0,1,NULL,NULL,NULL),			
								('WL16_US','OS - ' + @Name + ' - WL16',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'WL16_US',NULL,0,0,1,NULL,NULL,NULL),			
								('WL17_US','OS - ' + @Name + ' - WL17',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'WL17_US',NULL,0,0,1,NULL,NULL,NULL),			
								('DefaulterPostcode','OS - ' + @Name + ' - DefaulterPostcode',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddPostCodeLiable1',NULL,0,0,1,NULL,NULL,NULL),			
								('ClientDefaulterReference','OS - ' + @Name + ' - ClientDefaulterReference',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'ClientDefaulterRef',NULL,0,0,1,NULL,NULL,NULL),			
								('IssueDate','OS - ' + @Name + ' - IssueDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'IssueDate',NULL,0,0,1,	'dd/MM/yyyy HH:mm','12/12/2019  00:00:00',NULL),			
								('StartDate','OS - ' + @Name + ' - StartDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'StartDate',NULL,0,0,1,	'dd/MM/yyyy HH:mm',NULL,NULL),			
								('EndDate','OS - ' + @Name + ' - EndDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'EndDate',NULL,0,1,1,	'dd/MM/yyyy HH:mm',NULL,NULL),			
								('Exclude1','OS - ' + @Name + ' - Exclude1',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'Exclude1',NULL,0,1,1,NULL,NULL,NULL),		
								('Exclude2','OS - ' + @Name + ' - Exclude2',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'Exclude2',NULL,0,0,1,NULL,NULL,NULL),		
								('Exclude3','OS - ' + @Name + ' - Exclude3',5,@UserId,GetDate(),@UserId,GetDate(),0,1,'Exclude3',NULL,0,0,1,NULL,NULL,NULL),		
								('DefaulterAddressLine5','OS - ' + @Name + ' - DefaulterAddressLine5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add5Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('LeadDefaulter','OS - ' + @Name + ' - LeadDefaulter',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FullnameLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('OffenceCode','OS - ' + @Name + ' - OffenceCode',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'OffenceCode',NULL,0,1,1,NULL,NULL,NULL),		
								('OffenceDescription','OS - ' + @Name + ' - OffenceDescription',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'OffenceDescription',NULL,0,1,1,NULL,NULL,NULL),		
								('OffenceLocation','OS - ' + @Name + ' - OffenceLocation',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'OffenceLocation',NULL,0,1,1,NULL,NULL,NULL),		
								('Court','OS - ' + @Name + ' - Court',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'OffenceCourt',NULL,0,1,1,NULL,NULL,NULL),		
								('DebtAddressCountry','OS - ' + @Name + ' - DebtAddressCountry',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DebtAddressCountry',NULL,0,1,1,NULL,NULL,NULL),		
								('VRM','OS - ' + @Name + ' - VRM',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'VehicleVRM',NULL,0,1,1,NULL,NULL,NULL),		
								('VehicleMake','OS - ' + @Name + ' - VehicleMake',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'VehicleMake',NULL,0,1,1,NULL,NULL,NULL),		
								('VehicleModel','OS - ' + @Name + ' - VehicleModel',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'VehicleModel',NULL,0,1,1,NULL,NULL,NULL),		
								('TECDate','OS - ' + @Name + ' - TECDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TECDate',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('Currency','OS - ' + @Name + ' - Currency',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Currency',NULL,0,1,1,NULL,NULL,NULL),		
								('TitleLiable1','OS - ' + @Name + ' - TitleLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TitleLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('FirstnameLiable1','OS - ' + @Name + ' - FirstnameLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FirstnameLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('MiddlenameLiable1','OS - ' + @Name + ' - MiddlenameLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MiddlenameLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('LastnameLiable1','OS - ' + @Name + ' - LastnameLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'LastnameLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('CompanyName','OS - ' + @Name + ' - CompanyName',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'CompanyNameLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('DOBLiable1','OS - ' + @Name + ' - DOBLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DOBLiable1',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('NINOLiable1','OS - ' + @Name + ' - NINOLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'NINOLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('MinorLiable1','OS - ' + @Name + ' - MinorLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MinorLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('AddCountryLiable1','OS - ' + @Name + ' - AddCountryLiable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddCountryLiable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone1Liable1','OS - ' + @Name + '- Phone1Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone1Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone2Liable1','OS - ' + @Name + '- Phone2Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone2Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone3Liable1','OS - ' + @Name + '- Phone3Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone3Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone4Liable1','OS - ' + @Name + '- Phone4Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone4Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone5Liable1','OS - ' + @Name + '- Phone5Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone5Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Email1Liable1','OS - ' + @Name + '- Email1Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email1Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Email2Liable1','OS - ' + @Name + '- Email2Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email2Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('Email3Liable1','OS - ' + @Name + '- Email3Liable1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email3Liable1',NULL,0,1,1,NULL,NULL,NULL),		
								('TitleLiable2','OS - ' + @Name + '- TitleLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TitleLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('FirstnameLiable2','OS - ' + @Name + '- FirstnameLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FirstnameLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('MiddlenameLiable2','OS - ' + @Name + '- MiddlenameLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MiddlenameLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('LastnameLiable2','OS - ' + @Name + '- LastnameLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'LastnameLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('FullnameLiable2','OS - ' + @Name + '- FullnameLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FullnameLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('CompanyNameLiable2','OS - ' + @Name + '- CompanyNameLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'CompanyNameLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('DOBLiable2','OS - ' + @Name + '- DOBLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DOBLiable2',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('NINOLiable2','OS - ' + @Name + '- NINOLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'NINOLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('MinorLiable2','OS - ' + @Name + '- MinorLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MinorLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Add1Liable2','OS - ' + @Name + '- Add1Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add1Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Add2Liable2','OS - ' + @Name + '- Add2Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add2Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Add3Liable2','OS - ' + @Name + '- Add3Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add3Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Add4Liable2','OS - ' + @Name + '- Add4Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add4Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Add5Liable2','OS - ' + @Name + '- Add5Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add5Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('AddCountryLiable2','OS - ' + @Name + '- AddCountryLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddCountryLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('AddPostCodeLiable2','OS - ' + @Name + '- AddPostCodeLiable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddPostCodeLiable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone1Liable2','OS - ' + @Name + '- Phone1Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone1Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone2Liable2','OS - ' + @Name + '- Phone2Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone2Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone3Liable2','OS - ' + @Name + '- Phone3Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone3Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone4Liable2','OS - ' + @Name + '- Phone4Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone4Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone5Liable2','OS - ' + @Name + '- Phone5Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone5Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Email1Liable2','OS - ' + @Name + '- Email1Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email1Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Email2Liable2','OS - ' + @Name + '- Email2Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email2Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('Email3Liable2','OS - ' + @Name + '- Email3Liable2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email3Liable2',NULL,0,1,1,NULL,NULL,NULL),		
								('TitleLiable3','OS - ' + @Name + '- TitleLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TitleLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('FirstnameLiable3','OS - ' + @Name + '- FirstnameLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FirstnameLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('MiddlenameLiable3','OS - ' + @Name + '- MiddlenameLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MiddlenameLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('LastnameLiable3','OS - ' + @Name + '- LastnameLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'LastnameLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('FullnameLiable3','OS - ' + @Name + '- FullnameLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FullnameLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('CompanyNameLiable3','OS - ' + @Name + '- CompanyNameLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'CompanyNameLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('DOBLiable3','OS - ' + @Name + '- DOBLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DOBLiable3',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('NINOLiable3','OS - ' + @Name + '- NINOLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'NINOLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('MinorLiable3','OS - ' + @Name + '- MinorLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MinorLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Add1Liable3','OS - ' + @Name + '- Add1Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add1Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Add2Liable3','OS - ' + @Name + '- Add2Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add2Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Add3Liable3','OS - ' + @Name + '- Add3Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add3Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Add4Liable3','OS - ' + @Name + '- Add4Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add4Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Add5Liable3','OS - ' + @Name + '- Add5Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add5Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('AddCountryLiable3','OS - ' + @Name + '- AddCountryLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddCountryLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('AddPostCodeLiable3','OS - ' + @Name + '- AddPostCodeLiable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddPostCodeLiable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone1Liable3','OS - ' + @Name + '- Phone1Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone1Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone2Liable3','OS - ' + @Name + '- Phone2Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone2Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone3Liable3','OS - ' + @Name + '- Phone3Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone3Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone4Liable3','OS - ' + @Name + '- Phone4Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone4Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone5Liable3','OS - ' + @Name + '- Phone5Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone5Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Email1Liable3','OS - ' + @Name + '- Email1Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email1Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Email2Liable3','OS - ' + @Name + '- Email2Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email2Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('Email3Liable3','OS - ' + @Name + '- Email3Liable3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email3Liable3',NULL,0,1,1,NULL,NULL,NULL),		
								('TitleLiable4','OS - ' + @Name + '- TitleLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TitleLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('FirstnameLiable4','OS - ' + @Name + '- FirstnameLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FirstnameLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('MiddlenameLiable4','OS - ' + @Name + '- MiddlenameLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MiddlenameLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('LastnameLiable4','OS - ' + @Name + '- LastnameLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'LastnameLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('FullnameLiable4','OS - ' + @Name + '- FullnameLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FullnameLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('CompanyNameLiable4','OS - ' + @Name + '- CompanyNameLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'CompanyNameLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('DOBLiable4','OS - ' + @Name + '- DOBLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DOBLiable4',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('NINOLiable4','OS - ' + @Name + '- NINOLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'NINOLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('MinorLiable4','OS - ' + @Name + '- MinorLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MinorLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Add1Liable4','OS - ' + @Name + '- Add1Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add1Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Add2Liable4','OS - ' + @Name + '- Add2Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add2Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Add3Liable4','OS - ' + @Name + '- Add3Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add3Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Add4Liable4','OS - ' + @Name + '- Add4Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add4Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Add5Liable4','OS - ' + @Name + '- Add5Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add5Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('AddCountryLiable4','OS - ' + @Name + '- AddCountryLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddCountryLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('AddPostCodeLiable4','OS - ' + @Name + '- AddPostCodeLiable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddPostCodeLiable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone1Liable4','OS - ' + @Name + '- Phone1Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone1Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone2Liable4','OS - ' + @Name + '- Phone2Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone2Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone3Liable4','OS - ' + @Name + '- Phone3Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone3Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone4Liable4','OS - ' + @Name + '- Phone4Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone4Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone5Liable4','OS - ' + @Name + '- Phone5Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone5Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Email1Liable4','OS - ' + @Name + '- Email1Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email1Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Email2Liable4','OS - ' + @Name + '- Email2Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email2Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('Email3Liable4','OS - ' + @Name + '- Email3Liable4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email3Liable4',NULL,0,1,1,NULL,NULL,NULL),		
								('TitleLiable5','OS - ' + @Name + '- TitleLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'TitleLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('FirstnameLiable5','OS - ' + @Name + '- FirstnameLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FirstnameLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('MiddlenameLiable5','OS - ' + @Name + '- MiddlenameLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MiddlenameLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('LastnameLiable5','OS - ' + @Name + '- LastnameLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'LastnameLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('FullnameLiable5','OS - ' + @Name + '- FullnameLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'FullnameLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('CompanyNameLiable5','OS - ' + @Name + '- CompanyNameLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'CompanyNameLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('DOBLiable5','OS - ' + @Name + '- DOBLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'DOBLiable5',NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('NINOLiable5','OS - ' + @Name + '- NINOLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'NINOLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('MinorLiable5','OS - ' + @Name + '- MinorLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'MinorLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Add1Liable5','OS - ' + @Name + '- Add1Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add1Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Add2Liable5','OS - ' + @Name + '- Add2Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add2Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Add3Liable5','OS - ' + @Name + '- Add3Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add3Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Add4Liable5','OS - ' + @Name + '- Add4Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add4Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Add5Liable5','OS - ' + @Name + '- Add5Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Add5Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('AddCountryLiable5','OS - ' + @Name + '- AddCountryLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddCountryLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('AddPostCodeLiable5','OS - ' + @Name + '- AddPostCodeLiable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'AddPostCodeLiable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone1Liable5','OS - ' + @Name + '- Phone1Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone1Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone2Liable5','OS - ' + @Name + '- Phone2Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone2Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone3Liable5','OS - ' + @Name + '- Phone3Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone3Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone4Liable5','OS - ' + @Name + '- Phone4Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone4Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Phone5Liable5','OS - ' + @Name + '- Phone5Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Phone5Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Email1Liable5','OS - ' + @Name + '- Email1Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email1Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Email2Liable5','OS - ' + @Name + '- Email2Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email2Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('Email3Liable5','OS - ' + @Name + '- Email3Liable5',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'Email3Liable5',NULL,0,1,1,NULL,NULL,NULL),		
								('BatchId','OS - ' + @Name + '- BatchId',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('BatchDate','OS - ' + @Name + '- BatchDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('CaseNumber','OS - ' + @Name + '- CaseNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'OneStepCaseNumber',NULL,0,1,1,NULL,NULL,NULL),		
								('ClientId','OS - ' + @Name + '- ClientId',5,@UserId,GetDate(),@UserId,GetDate(),0,0,'ClientId',NULL,0,1,1,NULL,NULL,NULL),		
								('ConnId','OS - ' + @Name + '- ConnId',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('ClientName','OS - ' + @Name + '- ClientName',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('ExpiresOn','OS - ' + @Name + '- ExpiresOn',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('Status','OS - ' + @Name + '- Status',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('Phase','OS - ' + @Name + '- Phase',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('PhaseDate','OS - ' + @Name + '- PhaseDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('Stage','OS - ' + @Name + '- Stage',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('StageDate','OS - ' + @Name + '- StageDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,'dd/MM/yyyy HH:mm',NULL,NULL),		
								('IsAssignable','OS - ' + @Name + '- IsAssignable',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('OffenceDate','OS - ' + @Name + '- OffenceDate',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,'dd/MM/yyyy HH:mm','12/12/2019  00:00:00',NULL),		
								('CreatedOn','OS - ' + @Name + '- CreatedOn',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),		
								('CCaseId','OS - ' + @Name + '- CCaseId',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('BillNumber','OS - ' + @Name + '- BillNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('ClientPeriod','OS - ' + @Name + '- ClientPeriod',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerName','OS - ' + @Name + '- EmployerName',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerAddressLine1','OS - ' + @Name + '- EmployerAddressLine1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerAddressLine2','OS - ' + @Name + '- EmployerAddressLine2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerAddressLine3','OS - ' + @Name + '- EmployerAddressLine3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerAddressLine4','OS - ' + @Name + '- EmployerAddressLine4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerPostcode','OS - ' + @Name + '- EmployerPostcode',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerEmailAddress','OS - ' + @Name + '- EmployerEmailAddress',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerFaxNumber','OS - ' + @Name + '- EmployerFaxNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('EmployerTelephone','OS - ' + @Name + '- EmployerTelephone',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('Occupation','OS - ' + @Name + '- Occupation',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('RollNumber','OS - ' + @Name + '- RollNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
								('BenefitIndicator','OS - ' + @Name + '- BenefitIndicator',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL)


					INSERT INTO FileTemplateDataField 
					(
					FileTemplateId,
					DataFieldId
					)
					SELECT @FileTemplateId, Id 
					FROM DataField 
					WHERE Id > @MaxDFId

					INSERT INTO DataFieldDataCleansing
					SELECT DISTINCT df.Id, 3,'1','TRUE',NULL,NULL,NULL,NULL 
					FROM Datafield df 
					INNER JOIN FileTemplateDataField ftdf on df.Id = ftdf.DataFieldId
					INNER JOIN FileTemplate ft on ft.Id = ftdf.FileTemplateId
  					WHERE df.FieldType = 4 AND df.OutputFieldOnly = 0 AND FT.Id = @FileTemplateId

					INSERT INTO DataFieldDataCleansing
					SELECT DISTINCT df.Id, 3,'0','FALSE',NULL,NULL,NULL,NULL 
					FROM Datafield df 
					INNER JOIN FileTemplateDataField ftdf on df.Id = ftdf.DataFieldId
					INNER JOIN FileTemplate ft on ft.Id = ftdf.FileTemplateId
					WHERE df.FieldType = 4 AND df.OutputFieldOnly = 0 AND FT.Id = @FileTemplateId





					-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@Name ,
						@Description,
						@DBConnectionId ,
						@MimeTypeId ,
						@ClientId ,
						@BulkUpload ,
						@BUStagingTable ,
						@TransformData,
						@DTStoredProcedureId,
						@DTFileWriter ,
						@RemoveHeader ,
						@RemoveFooter ,
						@NoColumnName ,
						@DelimiterId ,
						@GenerateOutputFile ,
						@OutputFileDelimiterId ,
						@XMLRootNodeXPath ,
						@HeaderRowsToSkip ,
						@FooterRowsToSkip ,
						@OFExcludeColumnHeader ,
						@OutputFileEncoding ,
						@UploadToFileRepository ,
						@RepositoryFilePath ,
						@FNPAsSubfolder ,
						@FNTimestamp ,
						@OverrideDefaultDTActionSequence,
						@UserName 
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE dbo.[Stg_FileTemplate] 
				  SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0

				  COMMIT
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [dbo].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO


/****** Object:  Table [dbo].[Stg_DataField]    Script Date: 25/02/2020 14:41:32 ******/
DROP TABLE [dbo].[Stg_DataField]
GO

/****** Object:  Table [dbo].[Stg_DataField]    Script Date: 25/02/2020 14:41:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Stg_DataField](
	[FTName] [nvarchar](250) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[ExcludeFromOutput] [bit] NOT NULL DEFAULT ((0)),
	[OutputColumnName] [nvarchar](50) NULL,
	[XMLChildNodeXPath] [nvarchar](250) NULL,
	[HasMultipleNodes] [bit] NOT NULL DEFAULT ((0)),
	[OutputFieldOnly] [bit] NOT NULL DEFAULT ((0)),
	[OutputFormat] [nvarchar](50) NULL,
	[DefaultValue] [nvarchar](250) NULL,
	[FieldWidth] [int] NULL,
	[Imported] [bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
)

GO

/****** Object:  StoredProcedure [dbo].[usp_OS_DataFields_DT]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [dbo].[usp_OS_DataFields_DT]
GO

/****** Object:  StoredProcedure [dbo].[usp_OS_DataFields_DT]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [dbo].[usp_OS_DataFields_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
				
					UPDATE df
					SET 
						df.[Name] = sdf.[Name],
						df.[Description]= sdf.[Description] ,
						df.[ExcludeFromOutput]= sdf.[ExcludeFromOutput] ,
						df.[OutputColumnName]= sdf.[OutputColumnName],
						df.[XMLChildNodeXPath] = sdf.[XMLChildNodeXPath],
						df.[HasMultipleNodes]= sdf.[HasMultipleNodes] ,
						df.[OutputFieldOnly] = sdf.[OutputFieldOnly],
						df.[OutputFormat] = sdf.[OutputFormat],
						df.[DefaultValue]= sdf.[DefaultValue],
						df.[FieldWidth]= sdf.[FieldWidth] 		
					FROM dbo.DataField AS df
					INNER JOIN dbo.FileTemplateDataField AS ftdf ON df.Id = ftdf.DataFieldId
					INNER JOIN dbo.FileTemplate AS ft ON ft.Id = ftdf.FileTemplateId
					INNER JOIN dbo.Stg_DataField AS sdf ON sdf.OutputColumnName = df.OutputColumnName 
					WHERE ft.Name = sdf.FTName AND sdf.Imported = 0
					
					UPDATE dbo.[Stg_DataField]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0

					COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO dbo.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO



/****** Object:  StoredProcedure [dbo].[[usp_OS_AddNewDataFields]]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE [dbo].[usp_OS_AddNewDataFields]
GO

/****** Object:  StoredProcedure [dbo].[usp_OS_AddNewDataFields]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [dbo].[usp_OS_AddNewDataFields]
@FTName nvarchar (250),
@UserName nvarchar (100)
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

					DECLARE
							@FileTemplateId [int],
							@UserId [int],
							@MaxDFId [int]

					SELECT @UserId = Id FROM [dbo].[User] WHERE UserId = @UserName
					SELECT @FileTemplateId = Id FROM FileTemplate WHERE [Name] = @FTName
					SELECT @MaxDFId = Max(Id) From DataField 

					INSERT INTO DataField 
					(
						[Name],
						[Description],		
						[FieldType],	
						[CreatedBy],		
						[DateCreated],		
						[UpdatedBy],		
						[DateUpdated],		
						[Disabled],	
						[ExcludeFromOutput],	
						[OutputColumnName],		
						[XMLChildNodeXPath],	
						[HasMultipleNodes],	
						[OutputFieldOnly],	
						[FileSection],	
						[OutputFormat],		
						[DefaultValue],		
						[CoordinateForVariable]
					)


					VALUES  			
						('BillNumber','OS - ' + @FTName + '- BillNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('ClientPeriod','OS - ' + @FTName + '- ClientPeriod',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerName','OS - ' + @FTName + '- EmployerName',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerAddressLine1','OS - ' + @FTName + '- EmployerAddressLine1',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerAddressLine2','OS - ' + @FTName + '- EmployerAddressLine2',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerAddressLine3','OS - ' + @FTName + '- EmployerAddressLine3',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerAddressLine4','OS - ' + @FTName + '- EmployerAddressLine4',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerPostcode','OS - ' + @FTName + '- EmployerPostcode',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerEmailAddress','OS - ' + @FTName + '- EmployerEmailAddress',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerFaxNumber','OS - ' + @FTName + '- EmployerFaxNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('EmployerTelephone','OS - ' + @FTName + '- EmployerTelephone',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('Occupation','OS - ' + @FTName + '- Occupation',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('RollNumber','OS - ' + @FTName + '- RollNumber',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL),
						('BenefitIndicator','OS - ' + @FTName + '- BenefitIndicator',5,@UserId,GetDate(),@UserId,GetDate(),0,0,NULL,NULL,0,1,1,NULL,NULL,NULL)


					INSERT INTO FileTemplateDataField 
					(
					FileTemplateId,
					DataFieldId
					)
					SELECT @FileTemplateId, Id 
					FROM DataField 
					WHERE Id > @MaxDFId

					COMMIT TRANSACTION
                    SELECT
                           1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO dbo.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT
                           0 as Succeed
                END CATCH
            END
        END
GO


