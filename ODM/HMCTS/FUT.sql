﻿---- Add DataEntry table ----

ALTER TABLE [dbo].[DataEntry] DROP CONSTRAINT [FK_DataEntry_User]
GO

ALTER TABLE [dbo].[DataEntry] DROP CONSTRAINT [FK_DataEntry_FileTemplate]
GO

ALTER TABLE [dbo].[DataEntry] DROP CONSTRAINT [DF_DataEntry_Deleted]
GO

ALTER TABLE [dbo].[DataEntry] DROP CONSTRAINT [DF_DataEntry_Imported]
GO

/****** Object:  Table [dbo].[DataEntry]    Script Date: 01/09/2020 15:43:22 ******/
DROP TABLE IF EXISTS [dbo].[DataEntry]
GO

/****** Object:  Table [dbo].[DataEntry]    Script Date: 01/09/2020 15:43:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DataEntry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FileTemplateId] [int] NOT NULL,
	[DataEntryFormId] [int] NOT NULL,
	[JSONString] [nvarchar](max) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Uploaded] [bit] NOT NULL,
	[UploadedOn] [datetime] NULL,
	[Deleted] [bit] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_DataEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[DataEntry] ADD  CONSTRAINT [DF_DataEntry_Imported]  DEFAULT ((0)) FOR [Uploaded]
GO

ALTER TABLE [dbo].[DataEntry] ADD  CONSTRAINT [DF_DataEntry_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[DataEntry]  WITH CHECK ADD  CONSTRAINT [FK_DataEntry_FileTemplate] FOREIGN KEY([FileTemplateId])
REFERENCES [dbo].[FileTemplate] ([Id])
GO

ALTER TABLE [dbo].[DataEntry] CHECK CONSTRAINT [FK_DataEntry_FileTemplate]
GO

ALTER TABLE [dbo].[DataEntry]  WITH CHECK ADD  CONSTRAINT [FK_DataEntry_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO

ALTER TABLE [dbo].[DataEntry] CHECK CONSTRAINT [FK_DataEntry_User]
GO

---- Add DEAttachment table ----

ALTER TABLE [dbo].[DEAttachment] DROP CONSTRAINT [FK_DEAttachment_DataEntry]
GO

/****** Object:  Table [dbo].[DEAttachment]    Script Date: 01/09/2020 15:48:07 ******/
DROP TABLE IF EXISTS [dbo].[DEAttachment]
GO

/****** Object:  Table [dbo].[DEAttachment]    Script Date: 01/09/2020 15:48:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DEAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[ContentType] [nvarchar](250) NOT NULL,
	[FileType] [int] NOT NULL,
	[DataEntryId] [int] NOT NULL,
	[OriginalFileName] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_DEAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DEAttachment]  WITH CHECK ADD  CONSTRAINT [FK_DEAttachment_DataEntry] FOREIGN KEY([DataEntryId])
REFERENCES [dbo].[DataEntry] ([Id])
GO

ALTER TABLE [dbo].[DEAttachment] CHECK CONSTRAINT [FK_DEAttachment_DataEntry]
GO


INSERT INTO [dbo].[CmdText]
           ([DBConnectionId]
           ,[Name]
           ,[SQLScript]
           ,[Description]
           ,[FieldNames])
     VALUES
           (
				15,	'Columbus_Offence_Codes',	' SELECT [Code],[Description] FROM [dbo].[Offences] ORDER BY [Code]',	'Get offence code list', NULL	
			),
			(
				15,	'HMCTS_Breach_Clients',	'  SELECT c.Id AS ClientId, c.name AS ClientName, c.RegionId, r.Name as Region, ct.name as CaseType FROM Clients c INNER JOIN ClientCaseType cct ON c.Id = cct.clientId INNER JOIN CaseType ct on cct.caseTypeId = ct.Id INNER JOIN Regions r ON c.RegionId = r.Id WHERE ct.name IN (''Arrest Breach Bail'',''Arrest Breach No Bail'') or (c.name like ''%Financial Dated Bail%'' AND ct.name =''Arrest Bail'') ORDER BY c.name ASC',	NULL		
			),
			(
				15,	'HMCTS_Breach_Case_XRef',	' SELECT c.Id as CaseId, c.ClientCaseReference AS CaseNumber, cct.clientId AS ClientId FROM cases c inner join dbo.[Batches] b on c.batchid = b.id inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id	inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id inner join dbo.Clients cl on cct.clientId = cl.Id WHERE cct.BrandId = 1 AND ct.name IN (''Arrest Breach Bail'',''Arrest Breach No Bail'')  or (cl.name like ''%Financial Dated Bail%'' AND ct.name =''Arrest Bail'') UNION SELECT 999999 AS CaseId, ''XXXXXXXXXXXXXXXX'' AS CaseNumber, 888888',	'Get cross ref for HMCTS arrest Breach cases', 	NULL			
			),
			(
				15,	'HMCTS_Breach_Get_Case_AI',	' SELECT c.ClientCaseReference AS CaseNumber 	FROM cases c inner join dbo.[Batches] b on c.batchid = b.id inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id inner join dbo.Clients cl on cct.clientId = cl.Id WHERE cct.BrandId = 1 AND cl.Id = {ClientId} AND c.clientCaseReference = ''{ClientCaseReference}'' UNION SELECT c.ClientCaseReference AS CaseNumber FROM oso.stg_HMCTS_Cases c WHERE c.ClientId = {ClientId} AND c.ClientCaseReference = ''{ClientCaseReference}'' AND c.Imported=0 and c.ErrorId IS NULL ','Get HMCTS  AI Case', NULL			
			)

GO

-- Production script for Columbus
/*
Id	DBConnectionId	Name	SQLScript	Description	FieldNames
6	19	Columbus_Offence_Codes	SELECT [Code],[Description] FROM [dbo].[Offences] ORDER BY [Code]	Get offence code list	NULL
7	19	HMCTS_Breach_Clients	SELECT c.Id AS ClientId, c.name AS ClientName, c.RegionId, r.Name as Region, ct.name as CaseType FROM Clients c INNER JOIN ClientCaseType cct ON c.Id = cct.clientId INNER JOIN CaseType ct on cct.caseTypeId = ct.Id INNER JOIN Regions r ON c.RegionId = r.Id WHERE ct.name IN ('Arrest Breach Bail','Arrest Breach No Bail') or (c.name like '%Financial Dated Bail%' AND ct.name ='Arrest Bail') ORDER BY c.name ASC	Get list of HMCTS clients for arrest breach	NULL
8	19	HMCTS_Breach_Case_XRef	SELECT c.Id as CaseId, c.ClientCaseReference AS CaseNumber, cct.clientId AS ClientId FROM cases c inner join dbo.[Batches] b on c.batchid = b.id inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id	inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id inner join dbo.Clients cl on cct.clientId = cl.Id WHERE cct.BrandId = 1 AND ct.name IN ('Arrest Breach Bail','Arrest Breach No Bail')  or (cl.name like '%Financial Dated Bail%' AND ct.name ='Arrest Bail') UNION SELECT 999999 AS CaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber, 888888
	Get cross ref for HMCTS arrest Breach cases 	NULL
9	15	HMCTS_Breach_Get_Case_AI	SELECT c.ClientCaseReference AS CaseNumber 	FROM cases c inner join dbo.[Batches] b on c.batchid = b.id inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id inner join dbo.CaseType ct on cct.CaseTypeId = ct.Id inner join dbo.Clients cl on cct.clientId = cl.Id WHERE cct.BrandId = 1 AND cl.Id = {ClientId} AND c.clientCaseReference = '{ClientCaseReference}' UNION SELECT c.ClientCaseReference AS CaseNumber FROM oso.stg_HMCTS_Cases c WHERE c.ClientId = {ClientId} AND c.ClientCaseReference = '{ClientCaseReference}' AND c.Imported=0 and c.ErrorId IS NULL	Get HMCTS  AI Case	NULL
*/