

/****** Object:  Table [oso].[stg_HMCTS_Cases]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[stg_HMCTS_Cases]
GO


--DROP INDEX IF EXISTS [idx_stg_HMCTS_Cases_clientcasereference] ON  [oso].[stg_HMCTS_Cases]
--GO

--DROP INDEX IF EXISTS idx_stg_HMCTS_Cases_clientid ON  [oso].[stg_HMCTS_Cases]
--GO

--DROP INDEX IF EXISTS idx_stg_HMCTS_Cases_OneStepCaseNumber ON  [oso].[stg_HMCTS_Cases]
--GO

/****** Object:  Table [oso].[stg_HMCTS_Cases]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_HMCTS_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18,4) NOT NULL,
	[Currency] [int] NULL,
	[TitleLiable1] [varchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,	
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,	
	[AddCountryLiable1] [int] NULL,
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[OffenceValue] [decimal](18,4) NULL,
	[IssuingCourt] [nvarchar](200) NULL,
	[SittingCourt] [nvarchar](200) NULL,
	[WarningMarkers] [nvarchar](1000) NULL,
	[CaseNotes] [nvarchar](1000) NULL,
	[AttachmentInfo] [nvarchar](1000) NULL,
	[BailDate] [datetime] NULL,
	[FUTUserId] [int] NULL,
	[CaseId] [int] NULL,
	[BatchNoGenerated] [bit] DEFAULT 0,	
	[Imported] [bit] DEFAULT 0,
	[ImportedOn] [datetime] NULL,
	[ErrorId] Int NULL
) ON [PRIMARY]
GO
--CREATE INDEX idx_stg_HMCTS_Cases_clientcasereference
--ON [oso].[stg_HMCTS_Cases](ClientCaseReference);

--CREATE INDEX idx_stg_HMCTS_Cases_clientid
--ON [oso].[stg_HMCTS_Cases](ClientId);

--CREATE INDEX idx_stg_HMCTS_Cases_OneStepCaseNumber
--ON [oso].[stg_HMCTS_Cases](OneStepCaseNumber);


/****** Object:  StoredProcedure [oso].[usp_OS_AddBatches]    Script Date: 24/07/2020 10:21:26 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_HMCTS_AddBatches]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_AddBatches]    Script Date: 24/07/2020 10:21:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_AddBatches]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			DECLARE @CurrentDate DateTime = GetDate();

            INSERT into batches
                (
                [referenceNumber],
                [batchDate],
                [closedDate],
                [createdBy],
                [manual],
                [deleted],
                [crossReferenceSent],
                [crossReferenceSentDate],
                [clientCaseTypeId],
                [loadedOn],
                [loadedBy],
                [receivedOn],
                [summaryDate]
                )
            SELECT DISTINCT
                CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112))[referenceNumber]
                ,CAST(CONVERT(date,osc.batchdate) AS DateTime) [batchDate]
                ,@CurrentDate [closedDate]
                ,2 [createdBy]
                ,1 [manual]
                ,0 [deleted]
                ,1 [crossReferenceSent]
                ,@CurrentDate [crossReferenceSentDate]
                ,cct.id [clientCaseTypeId]
                ,@CurrentDate [loadedOn]
                ,2 [loadedBy]
                ,@CurrentDate [receivedOn]
                ,NULL [summaryDate]
            FROM
                oso.stg_HMCTS_Cases osc
                join clientcasetype cct on osc.clientid = cct.clientid
            WHERE
                osc.imported = 0
                AND osc.ErrorID IS NULL
                AND osc.[BatchNoGenerated] = 0
                AND CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) NOT IN (
               
                    SELECT DISTINCT
                        b.[referenceNumber]
                    FROM
                        batches b
                        join clientcasetype cct on b.clientcasetypeid = cct.id
                        join oso.stg_HMCTS_Cases osc on b.batchDate = CAST(CONVERT(date,osc.batchdate) AS DateTime) AND cct.clientid = osc.ClientId
                    WHERE
                        CONCAT(cct.batchrefprefix,CONVERT(varchar(8),osc.batchdate,112)) = b.referenceNumber
                                        AND osc.imported = 1
                        AND osc.[BatchNoGenerated] = 1
                )
            GROUP BY
                cct.id,
                cct.batchrefprefix,
                osc.batchdate

		END
    END
GO

/****** Object:  StoredProcedure [oso].[SplitString]    Script Date: 24/07/2020 10:21:26 ******/
DROP FUNCTION IF EXISTS [oso].[SplitString]
GO

/****** Object:  StoredProcedure [oso].[SplitString]    Script Date: 24/07/2020 10:21:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [oso].[SplitString]
    (
        @List NVARCHAR(MAX),
        @Delim VARCHAR(255)
    )
    RETURNS TABLE
    AS
        RETURN ( SELECT [Value], idx = RANK() OVER (ORDER BY n) FROM 
          ( 
            SELECT n = Number, 
              [Value] = LTRIM(RTRIM(SUBSTRING(@List, [Number],
              CHARINDEX(@Delim, @List + @Delim, [Number]) - [Number])))
            FROM (SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
              FROM sys.all_objects) AS x
              WHERE Number <= LEN(@List)
              AND SUBSTRING(@Delim + @List, [Number], LEN(@Delim)) = @Delim
          ) AS y
        );