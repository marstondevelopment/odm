﻿  SELECT c.Id AS ClientId, c.name AS ClientName, c.RegionId, r.Name as Region, ct.name as CaseType 
  FROM Clients c INNER JOIN ClientCaseType cct ON c.Id = cct.clientId INNER JOIN CaseType ct on cct.caseTypeId = ct.Id INNER JOIN Regions r ON c.RegionId = r.Id 
  WHERE 
  c.name like '%HMCTS West Yorkshire%'
  AND c.RegionId = 127
  --ORDER BY c.name ASC


  UPDATE c Set c.RegionId = 128
  FROM Clients c INNER JOIN ClientCaseType cct ON c.Id = cct.clientId INNER JOIN CaseType ct on cct.caseTypeId = ct.Id INNER JOIN Regions r ON c.RegionId = r.Id 
  WHERE 
  c.name like '%HMCTS West Yorkshire%'
  AND c.RegionId = 127