﻿USE [Columbus]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_UpdateMMEClient]    Script Date: 01/07/2021 17:00:34 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_UpdateMMEClient]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_UpdateMMEClient]    Script Date: 01/07/2021 17:00:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [oso].[usp_OS_UpdateMMEClient] 
@ClientName nvarchar (100),
@CGNPrefix nvarchar (50)
AS BEGIN
SET NOCOUNT ON;
BEGIN
	DECLARE @ErrorId int;
	DECLARE @ClientGroupName	varchar(100);		
	DECLARE @ClientId INT;
	DECLARE @CGId INT;
	DECLARE @MMEInSync bit = 0;
	DECLARE @ErrorMsg	varchar(1000);		


	DECLARE @ClientMessageSchemeName varchar(50)	=  NULL
	DECLARE	@PartnerToId		varchar(100)	= '1'
	DECLARE @PartnerToRole		varchar(100)	= 'CREDITOR'
	DECLARE @PartnerFromId		varchar(100)	= '1'
	DECLARE @PartnerFromRole	varchar(100)	= 'BAILIFF'

	DECLARE @InboundCaseFileType varchar(8)				= 'csv'	 
	DECLARE @InboundCaseFileFieldSeparator varchar(3)	=  NULL
	DECLARE @InboundCaseFileConverterFile varchar(100)	=  'Rossendales.conv'
	DECLARE @InboundCaseFileXSLT varchar(100)			= 'Rossendales.xsl'
	DECLARE @InboundCaseFileXSD varchar(100)			= NULL
	DECLARE @InboundCaseFileNameRegEx varchar(255)		= '.*\.csv'

	DECLARE @InboundMessageFileConverterFile varchar(100)	=  NULL
	DECLARE @InboundMessageFileXSLT varchar(100)			=  NULL
	DECLARE @InboundMessageFileXSD	varchar(100)			=  NULL
	DECLARE @InboundMessageFileNameRegEx varchar(255)		= 'notused.notused'
	
	DECLARE @OutboundMessageFileName varchar(100)		= 'notused.txt'

			DECLARE @REMOTE_ServerAddress varchar(100)			= '10.225.100.58'
			DECLARE @REMOTE_ServerPort int						=  221 
			DECLARE @REMOTE_ServerUserName varchar(100)			= 'mmetest'
			DECLARE @REMOTE_ServerPassword varchar(100)			= 'Monday12mme'

	declare @remote_outbound_filelocation varchar(100)			= concat('/users/rossendales/',@ClientName,'/outbound/')
	declare @remote_outbound_archivefilelocation varchar(100)	= concat('/users/rossendales/',@ClientName,'/outbound/archive/')

	declare @remote_inbound_filelocation varchar(100)			= concat('/users/rossendales/',@ClientName,'/inbound/')
	declare @remote_inbound_archivefilelocation varchar(100)	= concat('/users/rossendales/',@ClientName,'/inbound/archive/')

	DECLARE @MMEDIRECTORY varchar(255)		= '\\ITSAZ019305.itservices.local\mmeoutput$\' --'\\ITSAZ019305.itservices.local\mmeoutput$\'
	DECLARE @OffenceCode varchar(5)			= 'ODM'
	DECLARE @ServiceInterval	int			=  10
	DECLARE	@IsSettlementGroup	bit			=  0	/* 0=Message Group. 1=Settlement Group */
	DECLARE @ValidateClientCode bit			=  0
	DECLARE @ValidateFiles		bit			=  0
	DECLARE @AcknowledgeCaseFiles bit		=  0
	DECLARE @AcknowledgeMessageFiles bit	=  0

	DECLARE @WFPartnerFromRole varchar(50)	= 'not used'
	DECLARE @WFPartnerFromId varchar(50)	= ''
	DECLARE @WFPartnerToRole varchar(50)	= 'not used'
	DECLARE @WFPartnerToId varchar(50)		= 'not used'

	DECLARE @FirstLineNames		varchar(1000)				= NULL
	DECLARE @InboundMessageFileOriginalName varchar(250)	= NULL
	DECLARE @InboundMessageFileTargetName varchar(250)		= NULL
	SET @ClientGroupName = CONCAT(@CGNPrefix,': ', @ClientName);

	SELECT  @ClientId = c.[Id]
	  FROM [Columbus].[dbo].[Clients] c
	WHERE c.name = @ClientName

	IF (@ClientId IS NULL)
	BEGIN
		SET @ErrorMsg = 'Client not found.'
	END
	ELSE
	BEGIN
		SELECT @CGId = cg.Id
		FROM [MarstonMessageExchange].[dbo].ClientGroupMember cgm 
		  INNER JOIN [MarstonMessageExchange].[dbo].[ClientGroup] cg on cgm.ClientGroupId = cg.Id
		WHERE cgm.ClientID = @ClientId AND cg.GroupName = @ClientGroupName
		IF (@CGId IS NULL)
		BEGIN


			BEGIN TRY
				BEGIN TRANSACTION
					--DELETE MME SETTINGSS FOR THE CLIENT
					DELETE cgm 
					FROM [MarstonMessageExchange].[dbo].ClientGroupMember cgm
					INNER JOIN [MarstonMessageExchange].[dbo].[ClientGroup] cg ON cgm.clientgroupid = cg.id 
					WHERE cg.groupname = @ClientGroupName;

					DELETE  
					FROM [MarstonMessageExchange].[dbo].[ClientGroup] 
					WHERE groupname = @ClientGroupName

					--INSERT MME SETTINGS FOR THE CLIENT
					IF NOT EXISTS (SELECT 1 FROM [MarstonMessageExchange].[dbo].ClientGroup WHERE GroupName = @ClientGroupName)
					BEGIN 
						DECLARE @ClientCaseTypeId int	= (SELECT Id FROM Columbus.dbo.ClientCaseType WHERE ClientId = @ClientID)
						DECLARE @CaseTypeId int			= (SELECT caseTypeId FROM Columbus.dbo.ClientCaseType WHERE ClientID = @ClientID)

						INSERT INTO [MarstonMessageExchange].[dbo].ClientGroup (
						[GroupName]
						,[IsSettlementGroup]
						,[PartnerFromId]
						,[PartnerFromRole]
						,[PartnerToId]
						,[PartnerToRole]
						,InboundCaseFileLocation
						,[InboundCaseArchiveFileLocation]
						,[InboundCaseFileNameRegEx]
						,[InboundCaseFileXSD]
						,[InboundFileRepository]
						,[InboundErrorFileRepository]
						,[InboundMessageFileLocation]
						,[InboundMessageArchiveFileLocation]
						,[InboundMessageFileNameRegEx]
						,[InboundMessageFileXSD]
						,[OutboundMessageFileLocation]
						,[OutboundMessageFileName]
						,[OutboundMessageArchiveFileLocation]
						,[OutboundRemoteArchiveFileLocation]
						,[OutboundRemoteFileLocation]
						,[RemoteServerAddress]
						,[RemoteServerUsername]
						,[RemoteServerPassword]
						,[RemoteServerPort]
						,[ServiceInterval]
						,[OffenceCode]
						,[Separator]
						,[InputCaseFileType]
						,[ConverterFile]
						,[XSLTFile]
						,[FirstLineNames]
						,[InboundRemoteFileLocation]
						,[InboundRemoteArchiveFileLocation]
						,[ValidateClientCode]
						,[WFPartnerFromId]
						,[WFPartnerFromRole]
						,[WFPartnerToId]
						,[WFPartnerToRole]
						,[ValidateFiles]
						,[ConvertMessageFiles]
						,[MessageConverterFile]
						,[MessageXSLTFile]
						,[AcknowledgeCaseFiles]
						,[AcknowledgeMessageFiles]
						,[InboundMessageFileOriginalName]
						,[InboundMessageFileTargetName])
						SELECT
							@ClientGroupName AS [GroupName]
						, @IsSettlementGroup AS [IsSettlementGroup]
						, @PartnerFromId AS [PartnerFromId]
						, @PARTNERFROMROLE AS [PartnerFromRole]
						, @PartnerToId AS [PartnerToId]
						, @PARTNERTOROLE AS [PartnerToRole]
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\Valid\Case\') AS [InboundCaseFileLocation]
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\Valid\Case\Archive\') AS [InboundCaseArchiveFileLocation]
						, @InboundCaseFileNameRegEx AS [InboundCaseFileNameRegEx]
						, CONCAT(@MMEDIRECTORY, 'Client Maps\Rossendales\')+ @InboundCaseFileXSD AS InboundCaseFileXSD
						--, CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName, '\', @InboundCaseFileXSD) AS [InboundCaseFileXSD] --TODO: REINSTATE FOR STOCK CONFIG
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\') AS [InboundFileRepository]
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\Invalid\') as [InboundErrorFileRepository]
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\Valid\Msg\') as [InboundMessageFileLocation]
						, CONCAT(@MMEDIRECTORY, 'Data\Inbound\Rossendales\', @ClientName, '\Valid\Msg\Archive\') as [InboundMessageArchiveFileLocation]
						, @InboundMessageFileNameRegEx AS [InboundMessageFileNameRegEx]
						, CONCAT(@MMEDIRECTORY, 'Client Maps\Rossendales\') + @InboundMessageFileXSD AS [InboundMessageFileXSD]
						--CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName,'\', @InboundMessageFileXSD) AS [InboundMessageFileXSD] --TODO: REINSTATE FOR STOCK CONFIG
						, CONCAT(@MMEDIRECTORY, 'Data\Outbound\Rossendales\', @ClientName, '\') AS [OutboundMessageFileLocation]
						, @OutboundMessageFileName AS [OutboundMessageFileName]
						, CONCAT(@MMEDIRECTORY, 'Data\Outbound\Rossendales\', @ClientName, '\Archive\') AS [OutboundMessageArchiveFileLocation]
						, @REMOTE_OUTBOUND_ArchiveFileLocation AS [OutboundRemoteArchiveFileLocation]
						, @REMOTE_OUTBOUND_FileLocation AS [OutboundRemoteFileLocation]
						, @REMOTE_ServerAddress AS [RemoteServerAddress]
						, @REMOTE_ServerUserName AS [RemoteServerUsername]
						, @REMOTE_ServerPassword AS [RemoteServerPassword]
						, @REMOTE_ServerPort AS [RemoteServerPort]
						, @ServiceInterval AS [ServiceInterval]
						, @OffenceCode AS [OffenceCode]
						, @InboundCaseFileFieldSeparator AS [Separator]
						, @InboundCaseFileType AS [InputCaseFileType]
						, CONCAT(@MMEDIRECTORY, 'Client Maps\Rossendales\')+@InboundCaseFileConverterFile AS [ConverterFile]
						--, CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName, '\')+ @InboundCaseFileConverterFile AS [ConverterFile]  --TODO: REINSTATE FOR STOCK CONFIG
						, CONCAT(@MMEDIRECTORY, 'Client Maps\Rossendales\')+ @InboundCaseFileXSLT AS [XSLTFile]
						--, CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName, '\')+ @InboundCaseFileXSLT AS [XSLTFile]  --TODO: REINSTATE FOR STOCK CONFIG
						, NULL AS [FirstLineNames]
						, @REMOTE_INBOUND_FileLocation AS [InboundRemoteFileLocation]
						, @REMOTE_INBOUND_ArchiveFileLocation AS [InboundRemoteArchiveFileLocation]
						, @ValidateClientCode AS [ValidateClientCode]
						, @WFPartnerFromId AS [WFPartnerFromId]
						, @WFPartnerFromRole AS [WFPartnerFromRole]
						, @WFPartnerToId AS [WFPartnerToId]
						, @WFPartnerToRole AS [WFPartnerToRole]
						, @ValidateFiles AS [ValidateFiles]
						, IIF(@InboundMessageFileConverterFile IS NULL, 0, 1) [ConvertMessageFiles]
						, CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName, '\') + @InboundMessageFileConverterFile AS [MessageConverterFile] --TODO: REINSTATE FOR STOCK CONFIG
						, CONCAT(@MMEDIRECTORY, 'Client Maps\', @ClientName, '\') + @InboundMessageFileXSLT AS [MessageXSLTFile] --TODO: REINSTATE FOR STOCK CONFIG
						, @AcknowledgeCaseFiles AS [AcknowledgeCaseFiles]
						, @AcknowledgeMessageFiles AS [AcknowledgeMessageFiles]
						, @InboundMessageFileOriginalName AS [InboundMessageFileOriginalName]
						, @InboundMessageFileTargetName [InboundMessageFileTargetName]
						;

						SET @CGId = SCOPE_IDENTITY();


						INSERT INTO [MarstonMessageExchange].[dbo].ClientGroupMember (ClientGroupId, ClientCaseTypeId, ClientID, CaseTypeID, Messages, Warrants)
						SELECT @CGId AS ClientGroupId
						, @ClientCaseTypeId AS ClientCaseTypeId
						, @ClientID AS ClientID
						, @CaseTypeId AS CaseTypeID
						, IIF(@InboundMessageFileNameRegEx IS NOT NULL, 1, 0) AS [Messages]
						, IIF(@InboundCaseFileNameRegEx IS NOT NULL, 1, 0) AS Warrants
						EXCEPT 
						SELECT 
							ClientGroupId
						, ClientCaseTypeId
						, ClientID
						, CaseTypeID
						, [Messages]
						, Warrants
						FROM [MarstonMessageExchange].[dbo].ClientGroupMember WHERE ClientGroupId = @CGId
						;

						IF(@ClientMessageSchemeName IS NOT NULL)
							INSERT INTO [MarstonMessageExchange].[dbo].ClientMessageSchemeMapping (ClientCaseTypeId, MessageSchemeName)
							SELECT @ClientCaseTypeId
							, @ClientMessageSchemeName
							EXCEPT 
							SELECT ClientCaseTypeId
							, MessageSchemeName
							FROM [MarstonMessageExchange].[dbo].ClientMessageSchemeMapping
							;
					END;
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				INSERT INTO [Columbus].[oso].[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						);
				SET @ErrorId = SCOPE_IDENTITY();
				SET @ErrorMsg = Error_message();
			END CATCH
		END
		ELSE
		BEGIN
			SET @MMEInSync = 1; 
		END
	END

	SELECT @ClientName AS ClientName, @ClientId AS ClientId, @MMEInSync AS MMEInSync,@CGId AS ClientGroupId, @ErrorMsg AS ErrorMsg, @ErrorId AS ErrorId

END
END


/****** Object:  StoredProcedure [oso].[usp_OS_UpdateMMEClient]    Script Date: 18/01/2021 10:25:48 ******/
SET ANSI_NULLS ON
GO


