--USE [Columbus]
--GO
DROP TABLE IF EXISTS [oso].[stg_OneStep_DefaulterEmails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_DefaulterEmails](	
	[EmailAddress] [varchar](250) NOT NULL,	
	[DateLoaded] [datetime] NULL,
	[Source] [int] NULL,
	[cCaseId] [int] NOT NULL,
	[cDefaulterId] [int] NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterEmails_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterEmails_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_DefaulterEmails]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END

DROP PROCEDURE IF EXISTS [oso].[usp_OS_DefaulterEmails_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO	

GO
CREATE PROCEDURE [oso].[usp_OS_DefaulterEmails_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [dbo].[DefaulterEmails] 
			(
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			)
			SELECT 
				[cDefaulterId], 
				[EmailAddress],				
				[DateLoaded], 
				[Source]
			FROM 
				[oso].[stg_OneStep_DefaulterEmails]
			WHERE 
				Imported = 0
			EXCEPT
			SELECT 
				[DefaulterId], 
				[Email], 
				[DateLoaded], 
				[SourceId]
			FROM 
				[dbo].[DefaulterEmails]

			UPDATE [oso].[stg_OneStep_DefaulterEmails]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0	

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]	
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION 
				END

				INSERT INTO oso.[SQLErrors] 
				VALUES (Error_number(), 
						Error_severity(), 
						Error_state(), 
						Error_procedure(), 
						Error_line(), 
						Error_message(), 
						Getdate()) 

				SELECT 0 as Succeed
		END CATCH
	END
END
GO
GRANT EXECUTE ON OBJECT::[oso].[usp_OS_DefaulterEmails_DT] TO FUTUser;
GRANT ALTER ON OBJECT::[oso].[stg_OneStep_DefaulterEmails] TO FUTUser;