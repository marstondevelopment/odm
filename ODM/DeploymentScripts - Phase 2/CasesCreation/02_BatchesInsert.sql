﻿/****** Object:  StoredProcedure [oso].[usp_OS_AddBatches]    Script Date: 24/07/2020 10:21:26 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_AddBatches]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_AddBatches]    Script Date: 24/07/2020 10:21:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_AddBatches]
AS
    BEGIN
        SET NOCOUNT ON;
		BEGIN
			INSERT into batches
				(
				[referenceNumber],
				[batchDate],
				[closedDate],
				[createdBy],
				[manual],
				[deleted],
				[crossReferenceSent],
				[crossReferenceSentDate],
				[clientCaseTypeId],
				[loadedOn],
				[loadedBy],
				[receivedOn],
				[summaryDate]
				)

				select DISTINCT
					CONCAT(
					cct.batchrefprefix,
					datepart(year,CAST(CONVERT(date,osc.batchdate) AS DateTime)),
					FORMAT(CAST(CONVERT(date,osc.batchdate) AS DateTime),'MM'),
					FORMAT(CAST(CONVERT(date,osc.batchdate) AS DateTime),'dd')--,
					--FORMAT(osc.batchdate,'hhmmss')
					 )[referenceNumber]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [batchDate]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [closedDate]
					,2 [createdBy]
					,0 [manual]
					,0 [deleted]
					,1 [crossReferenceSent]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [crossReferenceSentDate]
					,cct.id [clientCaseTypeId]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [loadedOn]
					,2 [loadedBy]
					,CAST(CONVERT(date,osc.batchdate) AS DateTime) [receivedOn]
					,NULL [summaryDate]
				From oso.stg_Onestep_Cases osc
				join clientcasetype cct on osc.clientid = cct.clientid
				where osc.imported = 0 AND osc.ErrorID IS NULL
				GROUP BY cct.id, cct.batchrefprefix, CAST(CONVERT(date,osc.batchdate) AS DateTime)

				EXCEPT 

				SELECT
				b.[referenceNumber],
				b.[batchDate],
				b.[closedDate],
				b.[createdBy],
				b.[manual],
				b.[deleted],
				b.[crossReferenceSent],
				b.[crossReferenceSentDate],
				b.[clientCaseTypeId],
				b.[loadedOn],
				b.[loadedBy],
				b.[receivedOn],
				b.[summaryDate]

				from batches b
			    join clientcasetype cct on b.clientcasetypeid = cct.id AND cct.brandid IN (10,11,12,13)
                join oso.stg_Onestep_cases osc on b.batchDate = CAST(CONVERT(date,osc.batchdate) AS DateTime) AND cct.clientid = osc.ClientId




		END
    END
GO