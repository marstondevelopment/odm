begin tran

SET IDENTITY_INSERT dbo.Teams ON;  
GO  

-- SETUP LAA TEAMS
INSERT INTO [dbo].[Teams]([id],[name],[description],[feePercentage],[minimumFees],[averageFee],[minimumExecutions],[performanceTarget],[createdOn],[closedOn],[maxCases],[outCodes],[IsCoaForbidden],[ExcludeFromWorkflows],[email])
VALUES (200, 'LAA AOE Solicitors', 'LAA Attachment of Earnings Solicitors', 0, 0, 0, 0, 0, getdate(), NULL, 9999, NULL, 0, 0, NULL)
GO

INSERT INTO [dbo].[Teams]([id],[name],[description],[feePercentage],[minimumFees],[averageFee],[minimumExecutions],[performanceTarget],[createdOn],[closedOn],[maxCases],[outCodes],[IsCoaForbidden],[ExcludeFromWorkflows],[email])
VALUES (201, 'LAA CO Solicitors', 'LAA Charging Order Solicitors', 0, 0, 0, 0, 0, getdate(), NULL, 9999, NULL, 0, 0, NULL)
GO

INSERT INTO [dbo].[Teams]([id],[name],[description],[feePercentage],[minimumFees],[averageFee],[minimumExecutions],[performanceTarget],[createdOn],[closedOn],[maxCases],[outCodes],[IsCoaForbidden],[ExcludeFromWorkflows],[email])
VALUES (202, 'LAA HCE', 'LAA High Court Enforcement', 0, 0, 0, 0, 0, getdate(), NULL, 9999, NULL, 0, 0, NULL)
GO

SET IDENTITY_INSERT dbo.Teams OFF;  
GO  

--CONFIGURE EXISTING TEAMS FOR NEW BRANDS
INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (125,10) -- Helmshore Business Rates Team

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (125,12) -- Helmshore Business Rates Team

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (125,13) -- Helmshore Business Rates Team

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (128,10) -- Welfare Team

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (128,12) -- Welfare Team

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (128,13) -- Welfare Team

-- CONFIGURE NEW TEAMS FOR NEW BRANDS
INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (200,11)

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (201,11)

INSERT INTO [Brands].[TeamsBrands]([TeamId],[BrandId])
VALUES (202,11)


commit tran




