
begin tran

SET IDENTITY_INSERT dbo.Category ON
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (210, 'DCA Doorstep', GETDATE(), 2, 'DCA case will be subject to doorstep activity', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (211, 'DCA Third Party', GETDATE(), 2, 'DCA case will be subject to third party activity', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (212, 'DCA Sundry', GETDATE(), 2, 'DCA Sundry Debt case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (213, 'DCA HBOP', GETDATE(), 2, 'DCA Housing Benefit case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (214, 'DCA FTA', GETDATE(), 2, 'DCA FTA case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (215, 'DCA CTAX', GETDATE(), 2, 'DCA Council Tax case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (216, 'DCA CR', GETDATE(), 2, 'DCA Commercial Rent case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (217, 'DCA Misc', GETDATE(), 2, 'DCA Miscellaneous case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (218, 'DCA Recharge', GETDATE(), 2, 'DCA Recharge case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (219, 'DCA Committal Summons', GETDATE(), 2, 'DCA Committal Summons case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (220, 'DCA Empty Property', GETDATE(), 2, 'DCA Empty Proprty case', 1, 1, 0)
GO

INSERT INTO [dbo].[Category]([id],[name],[CreatedOn],[CreatedBy],[description],[enabled],[staffUseOnly],[IsExcluded])
VALUES (221, 'DCA Process Serve', GETDATE(), 2, 'DCA Process Serve case', 1, 1, 0)
GO

SET IDENTITY_INSERT dbo.Category OFF
GO

--rollback tran
commit tran