
--INSERT INTO [dbo].[OfficerTypes] VALUES
-- ('Van Self Employed Officer',	'Van SEO',	0,	0,	0,	0,	0,	0)
--,('First Call Self Employed Officer',	'FC SEO',	0,	0,	0,	0,	0,	0)
--,('Van PAYE Officer',	'Van PAYE',	0,	0,	0,	0,	0,	0)
--,('First Call PAYE Officer',	'FC PAYE',	0,	0,	0,	0,	0,	0)


/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Officers]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[stg_Onestep_PhsTwo_Officers]
GO

/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Officers]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_Officers](
					[officerno] [int] NOT NULL,
					[id] [int] NULL,
					[drakeOfficerId] [int] NULL,
					[firstName] [varchar](80) NULL,
					[middleName] [varchar](80) NULL,
					[lastName] [varchar](80) NULL,
					[email] [varchar](100) NULL,
					[phone] [varchar](25) NULL,
					[started] [datetime] NOT NULL,
					[leftDate] [datetime] NULL,
					[typeId] [int] NOT NULL,
					[maxCases] [int] NOT NULL,
					[minFee] [decimal](18, 4) NOT NULL,
					[averageFee] [decimal](18, 4) NOT NULL,
					[minExecs] [int] NOT NULL,
					[signature] [varchar](50) NULL,
					[enabled] [bit] NOT NULL,
					[performanceTarget]  [decimal](10, 2) NOT NULL,
					[outcodes] [varchar](1000) NOT NULL,
					[Manager] [bit] NULL,
					[parentId] [int] NULL,
					[isRtaCertified] [bit] NOT NULL,
					[rtaCertificationExpiryDate] [datetime] NULL,
					[marston] [bit] NOT NULL,
					[rossendales] [bit] NOT NULL,
					[swift] [bit] NOT NULL,
					[rossendalesdca] [bit] NOT NULL,
					[marstonlaa] [bit] NOT NULL,
					[ctax] [bit] NULL,
					[nndr] [bit] NULL,
					[tma] [bit] NULL,
					[cmg] [bit] NULL,
					[dca] [bit] NOT NULL,
					[laa] [bit] NOT NULL,
					[Existing] [bit] NOT NULL,
					[Imported]	[bit] NULL DEFAULT 0,
					[ImportedOn] [datetime] NULL

) ON [PRIMARY]
GO

--/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 11/04/2019 10:08:18 ******/
--DROP TABLE oso.[OSColOfficersXRef]
--GO

--/****** Object:  Table [oso].[OSColOfficersXRef]    Script Date: 11/04/2019 10:08:18 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE oso.[OSColOfficersXRef](
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[OSOfficerNumber] [int] NOT NULL,
--	[OSId] [int] NULL,
--	[COfficerNumber] [int] NOT NULL,
--	[ColId] [int] NOT NULL,
-- CONSTRAINT [PK_OSColOfficersXRef] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO



/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE [oso].[usp_CheckOSOfficer]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSOfficer]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS client is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSOfficer]
@drakeOfficerId int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;				
		Select @Id = o.Id	  
	   from  dbo.[Officers] o 
		where 
		o.[officerNumber] = @drakeOfficerId 
		OR
		o.[drakeOfficerId] = @drakeOfficerId 		 
		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO

 
/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Officers_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Officers from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 

					@officerno [int],
					@id [int] ,
					@drakeOfficerId [int] ,
					@firstName [varchar](80),
					@middleName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@phone [varchar](25),
					@started [datetime] ,
					@leftDate [datetime],
					@typeId [int] ,
					@maxCases [int],
					@minFee [decimal](18, 4),
					@averageFee [decimal](18, 4),
					@minExecs [int],
					@signature [varchar](50) ,
					@enabled [bit], 
					@performanceTarget  [decimal](10, 2),
					@outcodes [varchar](1000) ,
					@Manager [bit],
					@parentId [int] ,
					@isRtaCertified [bit] ,
					@rtaCertificationExpiryDate [datetime] ,
					@marston [bit] ,
					@rossendales [bit],
					@swift [bit] ,
					@rossendalesdca [bit] ,
					@marstonlaa [bit] ,
					@ctax [bit] ,
					@nndr [bit] ,
					@tma [bit] ,
					@cmg [bit] ,
					@dca [bit] ,
					@laa [bit] ,
					@Existing [bit] ,					
				
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BRossendalesDCAId int,
					@BMarstonLAAId int,
					@RTM_CTId int,
					@CTAX_CTId int,
					@NNDR_CTId int,
					@CMG_CTId int,
					@DCA_CTId int,
					@LAA_CTId int,
					@Comment [varchar] (250)
					
					SELECT @RTM_CTId = Id FROM [dbo].[CaseType] WHERE name = 'RTA/TMA (RoadTraffic/Traffic Management)'				
					SELECT @CTAX_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Council Tax Liability Order'				
					SELECT @NNDR_CTId = Id FROM [dbo].[CaseType] WHERE name = 'National Non Domestic Rates'				
					SELECT @CMG_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Child Maintenance Group'
					SELECT @DCA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Debt Collection'
					SELECT @LAA_CTId = Id FROM [dbo].[CaseType] WHERE name = 'Legal Aid'
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						[officerno]
						,[id]
						,[drakeOfficerId]
						,[firstName]
						,[middleName]
						,[lastName]
						,[email]
						,[phone]
						,[started]
						,[leftDate]
						,[typeId]
						,[maxCases]
						,[minFee]
						,[averageFee]
						,[minExecs]
						,[signature]
						,[enabled]
						,[performanceTarget]
						,[outcodes]
						,[Manager]
						,[parentId]
						,[isRtaCertified]
						,[rtaCertificationExpiryDate]
						,[marston]
						,[rossendales]
						,[swift]
						,[rossendalesdca]
						,[marstonlaa]
						,[ctax]
						,[nndr]
						,[tma]
						,[cmg]
						,[dca]
						,[laa]
						,[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Officers]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
					IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColOfficersXRef] WHERE OSId = @Id

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' + @firstName + ' ' + @middleName + ' ' + @lastName;
									THROW 51000, @Comment, 163; 
								
								END								

							END
					ELSE
					BEGIN
						IF NOT EXISTS (SELECT * FROM dbo.[Officers])
						INSERT INTO dbo.[Officers]
							(
							  [officerNumber]
							  ,[drakeOfficerId]
							  ,[firstName]
							  ,[middleName]
							  ,[lastName]
							  ,[email]
							  ,[phone]
							  ,[started]
							  ,[leftDate]
							  ,[typeId]
							  ,[maxCases]
							  ,[minFee]
							  ,[averageFee]
							  ,[minExecs]
							  ,[signature]
							  ,[enabled]
							  ,[performanceTarget]
							  ,[outcodes]
							  ,[isManager]
							  ,[parentId]
							  ,[isRtaCertified]
							  ,[rtaCertificationExpiryDate]	
							)

							VALUES
							(

							  @officerno
							  ,@officerno
							  ,@firstName
							  ,@middleName
							  ,@lastName
							  ,@email
							  ,@phone
							  ,@started
							  ,@leftDate
							  ,@typeId
							  ,@maxCases
							  ,@minFee
							  ,@averageFee
							  ,@minExecs
							  ,@signature
							  ,@enabled
							  ,@performanceTarget
							  ,@outcodes
							  ,@Manager
							  ,@parentId
							  ,@isRtaCertified
							  ,@rtaCertificationExpiryDate								  
							)

							SET @ColId = SCOPE_IDENTITY();

							-- Add OfficersBrands
							IF (@marston = 1)
								BEGIN
									SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands] 
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonId
									)
								END
			
							IF (@rossendales = 1)
								BEGIN
									SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendalesId
									)
								END			
						
							IF (@swift = 1)
								BEGIN
									SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
									IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
									INSERT INTO [Brands].[OfficersBrands]
									(
										[OfficerId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftId
									)
								END		
								
							-- Add to CaseTypeInclusion
							IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
							INSERT INTO dbo.[CaseTypeInclusion]
									(
										[caseTypeId]
										,[officerId]
									)

									VALUES
									(
										@RTM_CTId
										,@ColId
									),									
									(
										@CTAX_CTId
										,@ColId
									),
									(
										@NNDR_CTId
										,@ColId
									),
									(
										@CMG_CTId
										,@ColId
									)

							-- Add to OfficerBrandedSettings table
							IF NOT EXISTS (SELECT * FROM dbo.[OfficerBrandedSettings])
							INSERT INTO dbo.OfficerBrandedSettings
								(
									officerid
									, BrandTypeId
									, OfficerPerformanceBonusSchemeId
									, OfficerRateId
								) 
								
								VALUES 
								( 
									@ColId
									, 1
									, 1
									, 6
								)

							-- Add to XRef table
							IF NOT EXISTS (SELECT * FROM oso.[OSColOfficersXRef])
							INSERT INTO oso.[OSColOfficersXRef]
								(
									[OSOfficerNumber]
									,[OSId]
									,[COfficerNumber]
									,[ColId]
								)

								VALUES
								(
									@officerno
									,@id
									,@officerno
									,@ColId
								)										

							-- New rows creation completed	

					END

					IF (@rossendalesdca = 1)
						BEGIN
							SELECT @BRossendalesDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BRossendalesDCAId
							)
						END	

					IF (@marstonlaa = 1)
						BEGIN
							SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
							IF NOT EXISTS (SELECT * FROM [Brands].[OfficersBrands])
							INSERT INTO [Brands].[OfficersBrands]
							(
								[OfficerId]
								,[BrandId]
							)
							VALUES
							(
								@ColId
								,@BMarstonLAAId
							)
						END	

					-- Add to CaseTypeInclusion
					IF NOT EXISTS (SELECT * FROM dbo.[CaseTypeInclusion])
					INSERT INTO dbo.[CaseTypeInclusion]
							(
								[caseTypeId]
								,[officerId]
							)
							VALUES
							(
								@DCA_CTId
								,@ColId
							),									
							(
								@LAA_CTId
								,@ColId
							)

					FETCH NEXT
					FROM
						cursorT
					INTO
						@officerno,
						@id,
						@drakeOfficerId,
						@firstName,
						@middleName,
						@lastName,
						@email,
						@phone,
						@started,
						@leftDate,
						@typeId,
						@maxCases,
						@minFee,
						@averageFee,
						@minExecs,
						@signature,
						@enabled,
						@performanceTarget,
						@outcodes,
						@Manager,
						@parentId,
						@isRtaCertified,
						@rtaCertificationExpiryDate,
						@marston,
						@rossendales,
						@swift,
						@rossendalesdca,
						@marstonlaa,
						@ctax,
						@nndr,
						@tma,
						@cmg,
						@dca,
						@laa,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Officers]
				  SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0	

				  COMMIT
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Officers_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Officers_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Officers_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Officers staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From oso.stg_Onestep_Officers
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


DROP PROCEDURE [oso].[usp_OS_GetOfficersXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetOfficersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[OSOfficerNumber]
	,[OSId]
	,[ColId]
  FROM [oso].[OSColOfficersXRef]
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Officers_GetOfficerCount]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_Officers_GetOfficerCount]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Officers_GetOfficerCount]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Officers_GetOfficerCount]
@firstName nvarchar (250),
@lastName nvarchar (250),
@drakeOfficerId int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Officers where (firstName = @firstName and lastName = @lastName) OR (drakeOfficerId = @drakeOfficerId)
    END
END
GO

-- Insert existing officer ids from Columbus into cross-ref table
/*
INSERT INTO [oso].[OSColOfficersXRef] ([OSOfficerNumber]
      ,[OSId]
      ,[COfficerNumber]
      ,[ColId]) Values 
(	13151,4421,13151,3754	)
,(	24103,4761,24102,4703	)
,(	36963,4985,36963,5018	)
,(	36141,4589,36141,4252	)
,(	13023,4412,13023,3202	)
,(	36668,4687,36668,4709	)
,(	36718,4788,36718,4783	)
,(	36313,4629,36313,4522	)
,(	33030,3929,33030,2456	)
,(	23597,3901,23597,5718	)
,(	86668,4977,86668,5719	)
,(	23278,1372,23278,5720	)
,(	23471,3653,23471,5721	)
,(	24191,4964,24191,5722	)
,(	23281,2610,23281,5723	)
,(	23282,933,23282	,5724	)
,(	23283,2683,23283,5725	)
,(	23285,1563,23285,5726	)
,(	23286,2932,23286,5727	)
,(	24184,4913,24184,5728	)
,(	24194,4992,24194,5729	)
,(	23288,4372,23288,5730	)
,(	23780,4147,23780,5731	)
,(	23672,4078,23672,5732	)
,(	24190,4960,24190,5733	)
,(	24215,5010,24215,5734	)
,(	24074,4677,24074,5735	)
,(	23562,785,23562	,5736	)
,(	23289,785,23289	,5737	)
,(	24199,4993,24199,5738	)
,(	23293,3327,23293,5739	)
,(	23295,2645,23295,5740	)
,(	86667,4983,86667,5741	)
,(	23298,2829,23298,5742	)
,(	23299,2385,23299,5743	)
,(	23300,1530,23300,5744	)
,(	23991,4580,23991,5745	)
,(	23301,2767,23301,5746	)
,(	23303,2709,23303,5747	)
,(	24024,4590,24024,5748	)
,(	23989,4566,23989,5749	)
,(	23781,4143,23781,5750	)
,(	23072,2554,23072,5751	)
,(	23305,1431,23305,5752	)
,(	23306,3288,23306,5753	)
,(	23307,595,23307	,5754	)
,(	23309,3305,23309,5755	)
,(	24210,4995,24210,5756	)
,(	24185,4927,24185,5757	)
,(	23312,792,23312	,5758	)
,(	24108,4940,24108,5759	)
,(	24202,5006,24202,5760	)
,(	23314,2204,23314,5761	)
,(	23315,3128,23315,5762	)
,(	23956,4401,23956,5763	)
,(	23318,2495,23318,5764	)
,(	23320,706,23320	,5765	)
,(	24018,4572,24018,5766	)
,(	24040,4612,24040,5767	)
,(	24207,5024,24207,5768	)
,(	24195,4976,24195,5769	)
,(	23329,1505,23329,5770	)
,(	23777,4579,23777,5771	)
,(	23133,3065,23133,5772	)
,(	23331,2142,23331,5773	)
,(	24211,5026,24211,5774	)
,(	24204,4998,24204,5775	)
,(	23955,4401,23955,5776	)
,(	23393,3534,23393,5777	)
,(	23954,4507,23954,5778	)
,(	23334,926,23334	,5779	)
,(	23337,633,23337	,5780	)
,(	23338,866,23338	,5781	)
,(	23339,2118,23339,5782	)
,(	24206,5025,24206,5783	)
,(	23340,3057,23340,5784	)
,(	23342,1511,23342,5785	)
,(	23343,1096,23343,5786	)
,(	23560,1096,23560,5787	)
,(	23345,2549,23345,5788	)
,(	23620,3986,23620,5789	)
,(	23346,2650,23346,5790	)
,(	24178,4849,24178,5791	)
,(	23347,2749,23347,5792	)
,(	23349,865,23349	,5793	)
,(	24189,4948,24189,5794	)
,(	23354,2894,23354,5795	)
,(	23117,3002,23117,5796	)
,(	37147,4547,37147,5797	)
,(	24058,4667,24058,5798	)
,(	24118,4941,24118,5799	)
,(	23561,2251,23561,5800	)
,(	23360,2251,23360,5801	)
,(	23361,2655,23361,5802	)
,(	23362,2744,23362,5803	)
,(	24192,4966,24192,5804	)
,(	23366,4372,23366,5805	)
,(	23368,1424,23368,5806	)
,(	23370,4278,23370,5807	)
,(	23275,925,23275	,5808	)
,(	24203,4984,24203,5809	)
,(	24033,4625,24033,5810	)
,(	23373,1299,23373,5811	)
,(	23374,3121,23374,5812	)
,(	24038,3072,24038,5813	)
,(	23380,2813,23380,5814	)
,(	24200,5001,24200,5815	)
,(	23819,4174,23819,5816	)
,(	23820,4174,23820,5817	)
,(	23778,4763,23778,5818	)
,(	24111,4763,24111,5819	)
,(	24062,4668,24062,5820	)
,(	24036,4624,24036,5821	)
,(	24067,4672,24067,5822	)
,(	23383,833,23383	,5823	)


*/



GO
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetOfficerTypesCrossRef]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_GetOfficerTypesCrossRef]
AS
BEGIN
	SELECT Id typeId, abbreviation OfficerType FROM OfficerTypes 
	WHERE isDeleted = 0
END

