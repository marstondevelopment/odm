
DROP TABLE IF EXISTS [oso].[stg_OneStep_SchemePayment_CrossRef]
GO

/****** Object:  Table [oso].[stg_OneStep_SchemePayment_CrossRef]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_SchemePayment_CrossRef](	
	[CSchemePaymentName] varchar(250) NOT NULL,
	[OSSchemePaymentName] varchar(250) NOT NULL,
	[CSchemePaymentId] int NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [oso].[OneStep_PaymentScheme_CrossRef]   ******/
DROP TABLE IF EXISTS [oso].[OneStep_SchemePayment_CrossRef]
GO

/****** Object:  Table [oso].[OneStep_SchemePayment_CrossRef]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_SchemePayment_CrossRef](
	[CSchemePaymentName] varchar(250) NOT NULL,
	[OSSchemePaymentName] varchar(250) NOT NULL,
	[CSchemePaymentId] int NOT NULL
) ON [PRIMARY]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_CrossRef_DT]  ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_SchemePayment_CrossRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_CrossRef_DT]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_SchemePayment_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				INSERT [oso].[OneStep_SchemePayment_CrossRef] 
				(
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				)
				SELECT
					CSchemePaymentName
					,OSSchemePaymentName
					,CSchemePaymentId
				FROM
					[oso].[stg_OneStep_SchemePayment_CrossRef]	 

			  update spx 
			  set spx.cschemepaymentid = new.id 
			  --select spx.*,new.id [newid]
			  from 
			   oso.OneStep_SchemePayment_Crossref spx
			  join schemepayments new on new.name = spx.cschemepaymentname AND 
			  new.paymentschemeid = (select id from paymentscheme where name = 'Rossendales Generic Payment Scheme')


			COMMIT TRANSACTION
			SELECT
				1 as Succeed
			
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_DC_DT]   ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_SchemePayment_DC_DT]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_SchemePayment_DC_DT]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description: To clear [oso].[stg_OneStep_SchemePayment_CrossRef] staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_SchemePayment_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_SchemePayment_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO




/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentXMapping]   ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetSchemePaymentXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetSchemePaymentXMapping]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep payment type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [CSchemePaymentName]
      ,[OSSchemePaymentName]
	  ,[CSchemePaymentId]
  FROM [oso].[OneStep_SchemePayment_CrossRef]
    END
END
GO





DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetSchemePaymentId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetPaymentSchemeId]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Updated:		BP
-- Create date: 25-07-2018
-- Update date: 06-02-2020
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetSchemePaymentId]
@CSchemePaymentName varchar(250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @PaymentSchemeIds TABLE (PaymentSchemeId INT)

		INSERT INTO @PaymentSchemeIds (PaymentSchemeId)
		SELECT id FROM PaymentScheme
		WHERE [name] in ('Rossendales Generic Payment Scheme', 'Rossendales RTA Generic Payment Scheme')

		SELECT sp.ID AS CSchemePaymentId
		FROM SchemePayments sp		
		WHERE paymentSchemeId in (SELECT PaymentSchemeId FROM @PaymentSchemeIds) and sp.name = @CSchemePaymentName		
    END
END
GO

--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_SchemePayment_DC_DT] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_GetSchemePaymentXMapping] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_SchemePayment_CrossRef_DT] TO FUTUser;
--GRANT ALTER ON OBJECT::[oso].[stg_OneStep_SchemePayment_CrossRef] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_GetSchemePaymentId] TO FUTUser;
