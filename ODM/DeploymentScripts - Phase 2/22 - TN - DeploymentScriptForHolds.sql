

/****** Object:  Table [oso].[Staging_OS_Holds]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[Staging_OS_Holds]
GO

/****** Object:  Table [oso].[Staging_OS_Holds]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Holds](
	[cCaseId] int NOT NULL,
	[cUserId] int NOT NULL,
	[HoldReason] varchar(700) NULL,
	[HoldOn] datetime NOT NULL,
	[HoldUntil] datetime NOT NULL,
	[ClientHold] bit NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO





/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DT]    Script Date: 12/05/2020 15:01:02 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Holds_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DT]    Script Date: 12/05/2020 15:01:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Holds_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @CaseId                        INT
			, @UserId                          INT
			, @CaseNoteId                      INT
			, @HistoryTypeId_CaseStatusChanged INT
			, @HistoryTypeId_HoldCase          INT
			, @CaseStatusId_OnHold             INT
			, @CaseStatusId_Current            INT
			, @StatusDate_Current              DATETIME
			, @HoldReasonId                    INT
			, @CaseExpiryDate                  DATETIME
			, @CaseStatusName_Current          VARCHAR(250)
			, @CaseNumber                      VARCHAR(250)
			, @Comment                         VARCHAR(700)
			, @AddedOn                         DATETIME
			, @StageId                         INT
			, @StageName					   VARCHAR(250)
			, @ClientId                        INT
			, @PhaseTypeId                     INT
			, @StageCategory                       VARCHAR(250)
			, @HoldOn              DATETIME
			, @HoldUntil              DATETIME
			, @ClientHold			bit 
			, @HoldReasonName					   VARCHAR(128)
                
		SELECT
				@HistoryTypeId_CaseStatusChanged = [Id]
		FROM
				[dbo].[HistoryTypes]
		WHERE
				[name] = 'CaseStatusChanged'

		SELECT
				@HistoryTypeId_HoldCase = [Id]
		FROM
				[dbo].[HistoryTypes]
		WHERE
				[name] = 'HoldCase'

		SELECT
				@CaseStatusId_OnHold = [Id]
		FROM
				[dbo].[CaseStatus]
		WHERE
				[name] = 'On-Hold'



		DECLARE cursorT
		CURSOR
			--LOCAL STATIC
			--LOCAL FAST_FORWARD
			--LOCAL READ_ONLY FORWARD_ONLY
			FOR
			SELECT
				[cCaseId],
				[cUserId],
				[HoldReason],
				[HoldOn],
				[HoldUntil],
				[ClientHold]


			FROM
				oso.[Staging_OS_Holds]
			WHERE 
				Imported = 0
			OPEN cursorT
			FETCH NEXT
			FROM
					cursorT
			INTO
					@CaseId
				, @UserId
				, @Comment  
				, @HoldOn
				, @HoldUntil
				, @ClientHold

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT
							@CaseStatusId_Current   = c.statusId
							, @CaseStatusName_Current = cs.[name]
							, @StatusDate_Current     = c.statusDate
							, @CaseExpiryDate         = c.expiresOn
							, @CaseNumber             = c.caseNumber
							, @StageId             = c.stageId

				FROM
							dbo.[Cases] c
							INNER JOIN
										dbo.[CaseStatus] cs
										on
													c.statusId = cs.Id
				WHERE
							c.id = @CaseId

				SET @HoldReasonName = 'Client Request'

				IF (@ClientHold = 0)
					SET @HoldReasonName = 'Other'		

				SELECT
						@HoldReasonId = [Id]
				FROM
						[dbo].[HoldReasons]
				WHERE
						[name] = @HoldReasonName						


				-- Update stage and status of the case
				UPDATE
						dbo.[Cases]
				SET    [previousStatusId]   = @CaseStatusId_Current
						, [previousStatusDate] = @StatusDate_Current
						, [statusId]           = @CaseStatusId_OnHold
						, [statusDate]         = GetDate()
				WHERE
						[ID] = @CaseId

				INSERT INTO [dbo].[HoldCases]
				( 
					[caseId],
					[holdOn],
					[holdUntil],
					[recommencedOn],
					[holdReason],
					[recommenceReason],
					[lateStatDec],
					[holdReasonId],
					[AmendedByHoldCaseId]
				)
				VALUES
				( 
					@CaseId,
					@HoldOn,
					@HoldUntil,
					null,
					@Comment,
					null,
					0,
					@HoldReasonId,
					null
				)

			-- Add new Case Notes for case status changes
			--INSERT INTO dbo.[CaseNotes]
			--       ( [caseId]
			--            , [officerId]
			--            , [userId]
			--            , [text]
			--            , [occured]
			--            , [visible]
			--            , [groupId]
			--            , [TypeId]
			--       )
			--       VALUES
			--       ( @CaseId
			--            , NULL
			--            , @UserId
			--            , @Comment
			--            , GetDate()
			--            , 1
			--            , NULL
			--            , NULL
			--       )
			--       SET @CaseNoteId = @@IDENTITY;
			--       -- Case Status Chaged
			--       INSERT INTO dbo.[CaseHistory]
			--              ( [caseId]
			--                   , [userId]
			--                   , [officerId]
			--                   , [COMMENT]
			--                   , [occured]
			--                   , [typeId]
			--                   , [CaseNoteId]
			--              )
			--              VALUES
			--              ( @CaseId
			--                   , @UserId
			--                   , NULL
			--                   , 'Case status changed to: On-Hold from: ' + @CaseStatusName_Current + '.'
			--                   , GetDate()
			--                   , @HistoryTypeId_CaseStatusChanged
			--                   , null
			--              )
			--       -- On-Hold
			--       INSERT INTO dbo.[CaseHistory]
			--              ( [caseId]
			--                   , [userId]
			--                   , [officerId]
			--                   , [COMMENT]
			--                   , [occured]
			--                   , [typeId]
			--                   , [CaseNoteId]
			--              )
			--              VALUES
			--              ( @CaseId
			--                   , @UserId
			--                   , NULL
			--                   , 'HoldCase: CaseNumber = ' + @CaseNumber + 'has been put on hold for ' + CONVERT(varchar(10),DATEDIFF(day, GetDate(), @CaseExpiryDate)) + ' days. The last day of hold period is ' + convert(varchar, @CaseExpiryDate, 103) + '.'
			--                   , GetDate()
			--                   , @HistoryTypeId_HoldCase
			--                   , @CaseNoteId
			--              )
					-- New rows creation completed	
				FETCH NEXT
				FROM
					cursorT
				INTO
					@CaseId
				, @UserId
				, @Comment  
				, @HoldOn
				, @HoldUntil
				, @ClientHold
			END
			CLOSE cursorT
			DEALLOCATE cursorT

			UPDATE [oso].[Staging_OS_Holds]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0	

			COMMIT
			SELECT
					1 as Succeed
			END TRY
			BEGIN CATCH
			ROLLBACK TRANSACTION
				INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
				SELECT
						0 as Succeed
			END CATCH
		END
	END
GO




/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Holds_DC_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Holds_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Holds staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Holds_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_Holds]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO
