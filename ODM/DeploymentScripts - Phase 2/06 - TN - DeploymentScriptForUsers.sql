
/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Users]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[stg_Onestep_PhsTwo_Users]
GO

/****** Object:  Table [oso].[stg_Onestep_PhsTwo_Users]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_Users](
					[firstName] [varchar](80) NULL,
					[lastName] [varchar](80) NULL,
					[email] [varchar](100) NULL,
					[Department] [varchar](200) NULL,
					[JobTitle] [varchar](250) NULL,
					[marston] [bit] NOT NULL,
					[swift] [bit] NOT NULL,
					[rossendales] [bit] NOT NULL,
					[marstonLAA] [bit] NOT NULL,
					[rossendaleDCA] [bit] NOT NULL,
					[SwiftDCA] [bit] NOT NULL,
					[MarstonDCA] [bit] NOT NULL,
					[DCA] [bit] NOT NULL,
					[EXP] [bit] NOT NULL,
					[LAA] [bit] NOT NULL,
					[onestepid] [int] NOT NULL,
					[username] [nvarchar] (50)NOT NULL,
					[Existing] [bit] NOT NULL,
					[Imported]	[bit] NULL DEFAULT 0,
					[ImportedOn] [datetime] NULL

) ON [PRIMARY]
GO

--/****** Object:  Table [oso].[OSColUsersXRef]    Script Date: 11/04/2019 10:08:18 ******/
--DROP TABLE oso.[OSColUsersXRef]
--GO

--/****** Object:  Table [oso].[OSColUsersXRef]    Script Date: 11/04/2019 10:08:18 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE oso.[OSColUsersXRef](
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[username] [nvarchar] (50) NOT NULL,
--	[OSId] [int] NULL,
--	[ColId] [int] NOT NULL,
-- CONSTRAINT [PK_OSColUsersXRef] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO


/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_GetUserCount]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetUserCount]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Get Count for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUserCount]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN		
		Select count(*) as RecCount From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
    END
END
GO




/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE [oso].[usp_CheckOSUser]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSUser]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Check if the OS user is already exists in Columbus
-- =============================================
CREATE PROCEDURE [oso].[usp_CheckOSUser]
@firstName nvarchar (80),
@lastName nvarchar (80),
@username nvarchar (50)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @RecCount INT;				
		Select @RecCount = count(*) From Users where (firstName = @firstName and lastName = @lastName) OR (name = @username)
		Select 
			CASE
				WHEN @RecCount = 0 THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO

 /****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 05/12/2019 11:46:04 ******/
DROP PROCEDURE [oso].[usp_OS_Users_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Users_DT]    Script Date: 05/12/2019 11:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Users from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@firstName [varchar](80),
					@lastName [varchar](80),
					@email [varchar](100),
					@Department [varchar](200),
					@JobTitle [varchar](250),
					@marston [bit],
					@swift [bit],
					@rossendales [bit],
					@marstonLAA [bit],
					@rossendaleDCA [bit],
					@SwiftDCA [bit],
					@MarstonDCA [bit],
					@DCA [bit],
					@EXP [bit],
					@LAA [bit],
					@onestepid [int],
					@username [nvarchar] (50),
					@Existing [bit],
					
					@ColId int,
					@BMarstonId int,
					@BRossendalesId int,
					@BSwiftId int,
					@BMarstonLAAId int, 
					@BRossendaleDCAId int,
					@BSwiftDCAId int,
					@BMarstonDCAId int,
					@DEPTID int,
					@OFFICEID int,
					@RoleId int,
					@Comment [varchar] (250),
					@UBCount int,
					@ColUserName [varchar] (50),
					@ColUserCount int
					
					
					
				SELECT @BMarstonId = Id From [Brands].[Brands] WHERE Name = 'Marston CTAX'
				SELECT @BRossendalesId = Id From [Brands].[Brands] WHERE Name = 'Rossendales'
				SELECT @BSwiftId = Id From [Brands].[Brands] WHERE Name = 'Swift TMA & CTAX'
				SELECT @BMarstonLAAId = Id From [Brands].[Brands] WHERE Name = 'Marston Legal Aid'
				SELECT @BRossendaleDCAId = Id From [Brands].[Brands] WHERE Name = 'Rossendales DCA'
				SELECT @BSwiftDCAId = Id From [Brands].[Brands] WHERE Name = 'Swift DCA'
				SELECT @BMarstonDCAId = Id From [Brands].[Brands] WHERE Name = 'Marston DCA'
				
				---- DEPARTMENT
				--INSERT INTO Departments ([name])
				--VALUES ('Rossendales')
				--SET @DEPTID = SCOPE_IDENTITY();
				
				-- OFFICE
				SELECT @OFFICEID = id FROM [dbo].[Offices] WHERE [Name] = 'Helmshore'
				--IF (@OFFICEID IS NULL)
				--BEGIN
				--	INSERT INTO Offices ([Name], [Firstline], [Postcode], [Address])
				--	VALUES ('Helmshore',NULL,NULL,NULL)
				--	SET @OFFICEID = SCOPE_IDENTITY();
				--END

				IF CURSOR_STATUS('global','cursorT')>=-1
				BEGIN
					DEALLOCATE cursorT
				END

                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
					[firstName],
					[lastName],
					[email],
					[Department],
					[JobTitle],
					[marston],
					[swift],
					[rossendales],
					[marstonLAA],
					[rossendaleDCA],
					[SwiftDCA],
					[MarstonDCA],
					[DCA],
					[EXP],
					[LAA],
					[onestepid],
					[username],
					[Existing]

                    FROM
                        oso.[stg_Onestep_PhsTwo_Users]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
                    WHILE @@FETCH_STATUS = 0
                    BEGIN

							SELECT @DEPTID = Id FROM [dbo].[Departments] WHERE [name] = @Department
							SET @ColId = NULL
							IF (@Existing = 1)
							BEGIN
								SELECT @ColId = ColId FROM [oso].[OSColUsersXRef] WHERE OSId = @onestepId

								IF (@ColId IS NULL)
								BEGIN
									SET @Comment = 'ColId not found for ' +  @username;
									THROW 51000, @Comment, 163; 
								
								END								

							END
							ELSE
							BEGIN
									--Add new user here
									SET @ColUserName = @username
									SELECT @ColUserCount = COUNT(*) FROM dbo.Users WHERE name = @username
									IF (@ColUserCount > 0)
									BEGIN
										SET @ColUserName += '_os';  
									END

									INSERT INTO USERS (
									[name]
									, [password]
									, [firstName]
									, [lastName]
									, [email]
									, [departmentId]
									, [enabled]
									, [officeId]
									, [changePasswordOnLogin]
									, [passwordReset]
									, [defaultResultsExpected]
									, [sendEmailNotification]
									, [sendEmailNotificationEnabledOn]
									, [emailNotificationInterval])
									VALUES (
									@ColUserName
									,'1pF5kXsqfQdLufiK43b1Qw=='
									,@firstName
									,@lastName
									,@email
									,@DEPTID
									, 1
									, @OFFICEID
									, 1
									, 0
									, 1000
									, 0
									, NULL
									, 1)
									SET @ColId = SCOPE_IDENTITY();

							
						
									-- Add to XRef table
									INSERT INTO oso.[OSColUsersXRef]
										(
											[username],
											[OSId],
											[ColId]
										)

										VALUES
										(
											@username
											,@onestepid
											,@ColId
										)										
								
									-- Add OfficersBrands
								IF (@marston = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BMarstonId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BMarstonId
											)								
										END

									END
			
								IF (@rossendales = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BRossendalesId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BRossendalesId
											)							
										END

									END			
						
								IF (@swift = 1)
									BEGIN
										SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
											WHERE UserId = @ColId and BrandId = @BSwiftId
										IF (@UBCount = 0)
										BEGIN
											INSERT INTO [Brands].[UsersBrands] 
											(
												[UserId]
												,[BrandId]
											)
											VALUES
											(
												@ColId
												,@BSwiftId
											)					
										END

									END											
							END

							IF (@marstonLAA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonLAAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonLAAId
									)					
								END

							END	
								
							IF (@rossendaleDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BRossendaleDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BRossendaleDCAId
									)					
								END

							END				

							IF (@SwiftDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BSwiftDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BSwiftDCAId
									)					
								END

							END				

							IF (@MarstonDCA = 1)
							BEGIN
								SELECT @UBCount = ( count(*)) from [Brands].[UsersBrands] 
									WHERE UserId = @ColId and BrandId = @BMarstonDCAId
								IF (@UBCount = 0)
								BEGIN
									INSERT INTO [Brands].[UsersBrands] 
									(
										[UserId]
										,[BrandId]
									)
									VALUES
									(
										@ColId
										,@BMarstonDCAId
									)					
								END

							END				

							-- Add User ROLE
							IF (@JobTitle IS NOT NULL)
							BEGIN
								SELECT @RoleId = Id FROM Roles WHERE name = @JobTitle
								
								IF (@RoleId is null)
									BEGIN

										SET @Comment = 'RoleId not found for ' +  @JobTitle;
										THROW 51000, @Comment, 163;  
									END	
								DECLARE @UserRoleId int
								SELECT @UserRoleId = UserId From UserRoles WHERE UserId = @ColId AND RoleId = @RoleId

								IF (@UserRoleId IS NULL)
								BEGIN
									INSERT INTO dbo.[UserRoles]
										(
											[userId]
											,[roleId]
										)

										VALUES
										(
											@ColId
											,@RoleId									
										)		
								END
																	
							END	
					-- New rows creation completed	
					FETCH NEXT
					FROM
						cursorT
					INTO
						@firstName,
						@lastName,
						@email,
						@Department,
						@JobTitle,
						@marston,
						@swift,
						@rossendales,
						@marstonLAA,
						@rossendaleDCA,
						@SwiftDCA,
						@MarstonDCA,
						@DCA,
						@EXP,
						@LAA,
						@onestepid,
						@username,
						@Existing
				  END
				  CLOSE cursorT
				  DEALLOCATE cursorT

				  UPDATE [oso].[stg_Onestep_PhsTwo_Users]
			      SET Imported = 1, ImportedOn = GetDate()
				  WHERE Imported = 0		

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Users_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Users_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Users_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Users staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Users_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_PhsTwo_Users]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


DROP PROCEDURE [oso].[usp_OS_GetUsersXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	RETURN X Mapping table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetUsersXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[username],
			[OSId],
			[ColId]
		FROM [oso].[OSColUsersXRef]
    END
END
GO

---- Insert new roles for OneStep users in Columbus
--INSERT	
--	[dbo].[Roles] ([name], [type])
--VALUES	

--	('Allocations Officer', 0),
--	('AR Assistant', 0),
--	('Assistant Management Accountant', 0),
--	('Banking Assistant', 0),
--	('Client Liaison Officer', 0),
--	('Credit and Collections assistant', 0),
--	('Customer Care Administrator', 0),
--	('Customer Contact  Senior Team Leader', 0),
--	('Customer Contact Agent', 0),
--	('Customer Contact Senior Agent', 0),
--	('Data Processing administrator', 0),
--	('Enforcement Lead Officer', 0),
--	('National enforcement director', 0),
--	('Operational Services Administrator', 0),
--	('Operational Services Team leader', 0)

 
