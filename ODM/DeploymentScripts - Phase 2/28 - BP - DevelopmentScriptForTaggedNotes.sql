﻿-------------- Script for removing duplicated tag notes from staging table ----


-- Copy duplicated tag notes that have the same tag values to [stg_Onestep_PhsTwo_TaggedNotes_Duplicated]

WITH cte AS (
    SELECT 
*,
        ROW_NUMBER() OVER (
            PARTITION BY 
				cCaseId,CaseNote, TagName, TagValue
            ORDER BY DateAdded Desc
        ) row_num
     FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]

)

INSERT INTO [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated] 
SELECT  [cCaseId]
      ,[CUserId]
      ,[COfficerId]
      ,[CaseNote]
      ,[TagName]
      ,[TagValue]
      ,[DateAdded]
      ,[Imported]
      ,[ImportedOn] 
FROM cte WHERE row_num >1 

select count(*) from [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated] 

-- Remove duplicated tag notes that have the same tag values 

WITH cte AS (
    SELECT 
*,
        ROW_NUMBER() OVER (
            PARTITION BY 
				cCaseId,CaseNote, TagName, TagValue
            ORDER BY DateAdded Desc
        ) row_num
     FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]

)

DELETE FROM cte
WHERE row_num > 1;

-- Copy duplicated tag notes that have different tag values to [[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue]]
WITH cte AS (
    SELECT 
*,
        ROW_NUMBER() OVER (
            PARTITION BY 
				cCaseId,CaseNote, TagName
            ORDER BY DateAdded Desc
        ) row_num
     FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] 

)

--SELECT count(*) FROM cte WHERE row_num > 1;

INSERT INTO [oso].[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue] 
SELECT  [cCaseId]
      ,[CUserId]
      ,[COfficerId]
      ,[CaseNote]
      ,[TagName]
      ,[TagValue]
      ,[DateAdded]
      ,[Imported]
      ,[ImportedOn] 
FROM cte WHERE row_num >1 

-- Remove  duplicated tag notes that have different tag values from [stg_Onestep_PhsTwo_TaggedNotes]
WITH cte AS (
    SELECT 
*,
        ROW_NUMBER() OVER (
            PARTITION BY 
				cCaseId,CaseNote, TagName
            ORDER BY DateAdded Desc
        ) row_num
     FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] 

)
DELETE FROM cte
WHERE row_num > 1;


----------------- End of script for removing duplicated tag notes -------------------------------------------





DROP TABLE IF EXISTS [oso].[stg_Onestep_PhsTwo_TaggedNotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,	
	[COfficerId] [int] NULL,	
	[CaseNote] [nvarchar] (1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar] (250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


DROP TABLE IF EXISTS [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes_Duplicated](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,	
	[COfficerId] [int] NULL,	
	[CaseNote] [nvarchar] (1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar] (250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


DROP TABLE IF EXISTS [oso].[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_PhsTwo_TaggedNotes_ComKeyIssue](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,	
	[COfficerId] [int] NULL,	
	[CaseNote] [nvarchar] (1000) NULL,
	[TagName] [varchar](125) NULL,
	[TagValue] [nvarchar] (250) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 02/08/2020 12:14:02 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_TaggedNotes_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DT]    Script Date: 02/08/2020 12:14:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
					-- Insert case notes
					INSERT INTO dbo.[CaseNotes] 
					( [caseId]
						, [userId]
						, [officerId]
						, [text]
						, [occured]
						, [visible]
						, [groupId]
						, [TypeId]
					)
					SELECT
						[CCaseId],
						[CUserId],
						NULL,
						[CaseNote],
						[DateAdded],
						1,
						NULL,
						NULL
					FROM
						[oso].[stg_Onestep_PhsTwo_TaggedNotes] 
					WHERE 
						Imported = 0 AND CaseNote IS NOT NULL AND CaseNote <>''

					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					WHERE otn.Imported = 0 AND otn.CaseNote IS NOT NULL AND otn.CaseNote <>''

					-- Update existing tagged note
					UPDATE [dbo].AdditionalCaseData 
					SET 
						[Value] = otn.TagValue,
						[LoadedOn] = otn.DateAdded
					FROM [dbo].AdditionalCaseData ac
					INNER JOIN [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL
				
					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes] otn
					INNER JOIN [dbo].AdditionalCaseData ac ON ac.CaseId = otn.cCaseId AND ac.[Name] = otn.TagName
					WHERE otn.Imported = 0 AND otn.TagName IS NOT NULL AND otn.TagValue IS NOT NULL

					-- Insert new tagged notes
					INSERT INTO [dbo].AdditionalCaseData (CaseId,[Name],[Value],LoadedOn)
					SELECT cCaseId,TagName,TagValue,DateAdded
					FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL

					UPDATE [oso].[stg_Onestep_PhsTwo_TaggedNotes]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0 AND TagName IS NOT NULL AND TagValue IS NOT NULL

				  COMMIT TRANSACTION
				  SELECT
						 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_TaggedNotes_DC_DT]    Script Date: 09/07/2020 10:40:12 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_TaggedNotes_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_TaggedNotes_DC_DT]  Script Date: 09/07/2020 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 09-07-2020
-- Description: To clear Tagged Notes staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_TaggedNotes_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_PhsTwo_TaggedNotes]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


