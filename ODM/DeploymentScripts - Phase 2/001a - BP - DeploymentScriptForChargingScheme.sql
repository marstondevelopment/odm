INSERT INTO [dbo].[ChargingScheme]([schemeName],[description]) 
SELECT 'Generic DCA Charging Scheme','Generic DCA Charging Scheme'
EXCEPT 
SELECT schemename,description from [dbo].[ChargingScheme]
DECLARE @DCAChargingSchemeID int = SCOPE_IDENTITY();

DECLARE @DCASchemeChargesInserted table(id int, name varchar(50));

Insert Into SchemeCharges ([chargeTypeId],[note],[chargingSchemeId],[type],[percentageOf],[name],[applications],[isValid],[isDefaulterChargedVat],[ShortName],[Priority],[RemitType],[PayableBy])
OUTPUT Inserted.id AS [id], Inserted.[name] AS [name] INTO @DCASchemeChargesInserted
Values 
 (21,N'',@DCAChargingSchemeID,2,4,N'Locksmith Charges',0,1,0,N'',NULL,2,2)
,(21,N'',@DCAChargingSchemeID,2,4,N'Auctioneers Costs',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Attendance',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Waiting',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Affadavit',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Successful Process Serve',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Unsuccessful Process Serve',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Security',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Successful Trace',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Unsuccessful Trace',0,1,0,N'',NULL,2,2)
,(21,N'',@DCAChargingSchemeID,2,4,N'Surcharge',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Additional',0,1,0,N'',NULL,2,2)
,(37,N'',@DCAChargingSchemeID,2,4,N'Other',0,0,0,N'',NULL,2,2)
,(41,N'',@DCAChargingSchemeID,2,4,N'Overplus To Client',0,1,0,N'',NULL,1,2)

    
  
  

