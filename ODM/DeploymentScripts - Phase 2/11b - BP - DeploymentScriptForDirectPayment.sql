﻿
/****** Object:  Table [oso].[Staging_OS_DirectPayment]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[Staging_OS_DirectPayment]
GO

/****** Object:  Table [oso].[Staging_OS_DirectPayment]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_DirectPayment](
	[CCaseId] [int] NOT NULL,	
	[Amount] [decimal](18,4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[ClearedOn] [datetime] NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO



/****** Object:  StoredProcedure [oso].[usp_OS_DirectPayment_DT]    Script Date: 11/04/2019 10:15:00 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_DirectPayment_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_DirectPayment_DT]    Script Date: 11/04/2019 10:15:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  BP
-- Create date: 04-05-2020
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_DirectPayment_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE @CaseId				INT
                    , @Amount				DECIMAL(18,4)
                    , @UserId				INT
                    , @OfficerId			INT
                    , @AddedOn				DATETIME
                    , @PaymentId			INT
                    , @PaymentStatusId		INT
                    , @SchemePaymentId		INT
                    , @CaseNoteId			INT
                    , @HistoryTypeId		INT
					, @ClearedOn			DATETIME					

				SELECT
						@PaymentStatusId = [Id]
				FROM
						[dbo].[PaymentStatus]
				WHERE
						[name] = 'DIRECT PAYMENT'

				SELECT
						@SchemePaymentId = [Id]
				FROM
					   [dbo].[SchemePayments] 
				WHERE						
						[name] = 'Direct Payment'
				
				SELECT
						@HistoryTypeId = [Id]
				FROM
						[dbo].[HistoryTypes]
				WHERE
						[name] = 'UpdateClientBalance'
                DECLARE cursorT
                CURSOR
						--LOCAL STATIC
						--LOCAL FAST_FORWARD
						--LOCAL READ_ONLY FORWARD_ONLY
                    FOR
                    SELECT
						CCaseId,
						Amount,
						AddedOn,
						CUserId,
						COfficerId,						
						ClearedOn
                    FROM
                        oso.[Staging_OS_DirectPayment]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@CaseId,
						@Amount,
						@AddedOn,
						@UserId,
						@OfficerId,						
						@ClearedOn
                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        -- Add new rows
                        INSERT INTO dbo.[Payments]
                               ( [receivedOn]
                                    , [transactionDate]
                                    , [amount]
                                    , [clearedOn]
                                    , [bouncedOn]
                                    , [recordedBy]
                                    , [officerId]
                                    , [REFERENCE]
                                    , [authorisationNumber]
                                    , [journalStatusId]
                                    , [receiptTypeId]
                                    , [trusted]
                                    , [schemePaymentId]
                                    , [paymentStatusId]
                                    , [surchargeAmount]
                                    , [paymentID]
                                    , [reconciledOn]
                                    , [reconcileReference]
                                    , [reversedOn]
                                    , [parentId]
                                    , [previousPaymentStatusId]
                                    , [reconciledBy]
                                    , [keepGoodsRemovedStatus]
                                    , [cardStatementReference]
                                    , [bankReconciledOn]
                                    , [bounceReference]
                                    , [reconciledBouncedOn]
                                    , [reconciledBouncedBy]
                                    , [exceptionalAdjustmentId]
                                    , [bankReconciledBy]
                                    , [isReversedChargePayment]
                                    , [paymentOriginId]
                                    , [uploadedFileReference]
                                    , [ReverseOrBounceNotes]
                               )
                               VALUES
                               ( @AddedOn
                                    , @AddedOn
                                    , @Amount
                                    , @ClearedOn
                                    , NULL
                                    , @UserId
                                    , @OfficerId
                                    , NUll
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 1
                                    , @SchemePaymentId
                                    , @PaymentStatusId
                                    , 0.00
                                    , 'MigratedPayment'
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , NULL
                                    , 0
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @PaymentId = SCOPE_IDENTITY();
                        -- Add new Case Payment
                        INSERT INTO dbo.[CasePayments]
                               ( [paymentId]
                                    , [caseId]
                                    , [amount]
                                    , [receivedOn]
                                    , [clientPaymentRunId]
                                    , [PaidClient]
                                    , [PaidEnforcementAgency]
                                    , [PaidClientExceptionalBalance]
                                    , [PaidEAExceptionalBalance]
                               )
                               VALUES
                               ( @PaymentId
                                    , @CaseId
                                    , @Amount
                                    , @AddedOn
                                    , NULL
                                    , 0
                                    , 0
                                    , 0
                                    , 0
                               )
                        ;
                        
                        -- Add new Case Notes
                        INSERT INTO dbo.[CaseNotes]
                               ( [caseId]
                                    , [officerId]
                                    , [userId]
                                    , [text]
                                    , [occured]
                                    , [visible]
                                    , [groupId]
                                    , [TypeId]
                               )
                               VALUES
                               ( @CaseId
                                    , @OfficerId
                                    , @UserId
                                    , 'Add direct payment of ' + FORMAT(@Amount,'N2', 'en-US') 
                                    , @AddedOn
                                    , 1
                                    , NULL
                                    , NULL
                               )
                        ;
                        
                        SET @CaseNoteId = SCOPE_IDENTITY();
                        -- Add new Case History
                        INSERT INTO dbo.[CaseHistory]
                               ( [caseId]
                                    , [userId]
                                    , [officerId]
                                    , [COMMENT]
                                    , [occured]
                                    , [typeId]
                                    , [CaseNoteId]
                               )
                               VALUES
                               ( @CaseId
                                    , @UserId
                                    , @OfficerId
                                    , 'Direct Payment has been completed. Direct Payment amount = ' + FORMAT(@Amount,'N2', 'en-US')  + '.'
                                    , @AddedOn
                                    , @HistoryTypeId
                                    , @CaseNoteId
                               )
                        ;
                        
                        -- New rows creation completed	
                        FETCH NEXT
                        FROM
                              cursorT
                        INTO
							@CaseId,
							@Amount,
							@AddedOn,
							@UserId,
							@OfficerId,							
							@ClearedOn
                    END
                    CLOSE cursorT
                    DEALLOCATE cursorT 

					UPDATE [oso].[Staging_OS_DirectPayment]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0	

					COMMIT TRANSACTION
					SELECT 1 as Succeed
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRANSACTION
                    END
                    INSERT INTO oso.[SQLErrors] VALUES
                           (Error_number()
                                , Error_severity()
                                , Error_state()
                                , Error_procedure()
                                , Error_line()
                                , Error_message()
                                , Getdate()
                           )
                    SELECT 0 as Succeed
                END CATCH
            END
        END
GO




/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_DirectPayment_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_DirectPayment_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 04-05-2020
-- Description: To clear DirectPayment staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_DirectPayment_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From [oso].[Staging_OS_DirectPayment]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO