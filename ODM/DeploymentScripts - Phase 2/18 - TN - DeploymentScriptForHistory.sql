

/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[Staging_OS_History]
GO

/****** Object:  Table [oso].[Staging_OS_History]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_History](
	[cCaseId] [int] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[Comment] NVARCHAR(500) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[HistoryTypeName] [varchar](200) NULL,
	[CaseNoteId] [int] NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Payments_DT]    Script Date: 25/11/2019 16:08:55 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_History_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DT]    Script Date: 25/11/2019 16:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: Copy data from staging table to Payments and relevant tables.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION
				
				INSERT INTO dbo.[CaseHistory] 
				( 
					[caseId],
					[userId],
					[officerId],
					[Comment],
					[occured],
					[typeId],
					[CaseNoteId]
				)
				SELECT
					[cCaseId],
					[CUserId],
					[COfficerId],
					[Comment],
					[Occurred],	
					case [HistoryTypeName]
						when 'Broken' then 28
						when 'Clear arrange' then 29
						when 'Clear plan' then 29
						when 'Payment status' then 32
						when 'Status changed' then 42
						when 'Success' then 42
						when 'DVLA info' then 49
						when 'Removed agent' then 53
						when 'Allocated' then 54
						when 'Arrangement' then 56
						when 'Payment plan' then 56
						when 'Cancelled' then 57
						when 'Documents' then 59
						when 'Address' then 68
						when 'Linked' then 71
						when 'Unlinked' then 72
						when 'Fee added' then 74
						when 'Fee status' then 74
						when 'Debt Changed' then 75
						when 'Stage' then 97
						when 'DVLA Enquiry' then 102
						when 'Return Details' then 110
						when 'Off hold' then 111
						when 'Hold' then 112
						when 'Phone' then 123
						when 'COG Visit' then 126
						when 'No COG visit' then 126
						when 'Contact details' then 137
						when 'PayOnline' then 140
						when 'Moved' then 145
						when 'Figures changed' then 150
						when 'Phone (inbound)' then 181
						when 'Letter' then 302
						when 'SMS to debtor' then 608
						else 0
					end,					
					[CaseNoteId]
                FROM
                    oso.[Staging_OS_History]
				WHERE 
					Imported = 0
					
				UPDATE [oso].[Staging_OS_History]
				SET Imported = 1, ImportedOn = GetDate()
				WHERE Imported = 0	

				COMMIT TRANSACTION
                SELECT
                    1 as Succeed
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION
                END
                INSERT INTO oso.[SQLErrors] VALUES
                        (Error_number()
                            , Error_severity()
                            , Error_state()
                            , Error_procedure()
                            , Error_line()
                            , Error_message()
                            , Getdate()
                        )
                SELECT
                    0 as Succeed
            END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_ReturnCode_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_History_DC_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_History_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_History_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[Staging_OS_History]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

