
DROP TABLE IF EXISTS [oso].[stg_OneStep_Visits]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_Visits](
	[CCaseId] [int] NOT NULL,
	[DefaulterName] [nvarchar](150) NOT NULL,
	[VisitType] [varchar](50) NOT NULL,
	[VisitDate] [datetime] NULL,
	[AddressLine1] [varchar](80) NULL,
	[AddressLine2] [varchar](100) NULL,
	[AddressLine3] [varchar](100) NULL,
	[AddressLine4] [varchar](100) NULL,
	[AddressLine5] [varchar](100) NULL,
	[Postcode] [varchar](12) NULL,
	[COfficerId] [int] NOT NULL,
	[BuildingType] [varchar](50) NULL,
	[DoorColour] [varchar](50) NULL,
	[Notes] [varchar](1000) NULL,
	[ProofOfIdentity] [varchar](50) NULL,
	[PersonContacted] [varchar](50) NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO



DROP PROCEDURE IF EXISTS [oso].[usp_OS_Visits_CrossRef_DC_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DC_DT] AS
BEGIN
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRY
			DELETE FROM [oso].[stg_OneStep_Visits]
			SELECT 1 as Succeed
		END TRY
		BEGIN CATCH
			INSERT INTO oso.[SQLErrors] VALUES
						(Error_number()
							, Error_severity()
							, Error_state()
							, Error_procedure()
							, Error_line()
							, Error_message()
							, Getdate()
						)
			SELECT 0 as Succeed
		END CATCH
    END
END
	

GO
DROP PROCEDURE IF EXISTS [oso].[usp_OS_Visits_CrossRef_DT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [oso].[usp_OS_Visits_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			INSERT [dbo].[Visits] 
			(
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			)
			SELECT 
				[import].[CCaseId],
				[import].[COfficerId], 
				[import].[VisitDate],
				[import].[DoorColour],
				[import].[BuildingType],
				[import].[PersonContacted],	
				[import].[ProofOfIdentity],
				[import].[Notes],
				0,
				53.68456,
				-2.2769,
				0.00,
				getdate(),
				getdate()
			FROM 		
				[oso].[stg_OneStep_Visits] [import]	
			WHERE 
				Imported = 0
			EXCEPT
			SELECT 
				[caseId], 
				[officerId],
				[visitDate],		 
				[doorColour], 
				[buildingType],
				[personContacted],
				[proofOfIdentity],
				[description],	
				[ActionId], 
				[GPSLatitude], 
				[GPSLongitude], 
				[GPSHDOP], 
				[GPSSatDateTime], 
				[DateLoaded] 		
			FROM 
				[dbo].[Visits]

			UPDATE [oso].[stg_OneStep_Visits]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO
GRANT EXECUTE ON OBJECT::[oso].[usp_OS_Visits_CrossRef_DT] TO FUTUser;
GRANT EXECUTE ON OBJECT::[oso].[usp_OS_Visits_CrossRef_DC_DT] TO FUTUser;
GRANT ALTER ON OBJECT::[oso].[stg_OneStep_Visits] TO FUTUser;