begin tran

SET IDENTITY_INSERT dbo.ClientGroups ON
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (160, 'WORKFLOW - DCA Doorstep', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (161, 'WORKFLOW - DCA Third Party', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (150, 'WORKFLOW - DCA Sundry', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (151, 'WORKFLOW - DCA HBOP', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (152, 'WORKFLOW - DCA FTA', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (153, 'WORKFLOW - DCA CTAX', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (154, 'WORKFLOW - DCA CR', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (155, 'WORKFLOW - DCA MISC', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (156, 'WORKFLOW - DCA Recharge', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (157, 'WORKFLOW - DCA Committal Summons', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (158, 'WORKFLOW - DCA Empty Property', 'DO NOT CHANGE - Used by workflow', NULL)
GO

INSERT INTO [dbo].[ClientGroups]([id],[name],[description],[CurrencyId])
VALUES (159, 'WORKFLOW - DCA Process Serve', 'DO NOT CHANGE - Used by workflow', NULL)
GO


SET IDENTITY_INSERT dbo.ClientGroups OFF
GO

--rollback tran
commit tran




