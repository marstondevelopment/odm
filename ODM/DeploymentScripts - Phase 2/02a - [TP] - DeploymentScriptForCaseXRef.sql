--/****** Object:  Table [oso].[stg_Onestep_CaseStages]    Script Date: 11/04/2019 10:05:49 ******/

--DROP INDEX IF EXISTS [idx_stg_Onestep_CaseStages_OnestepCaseNumber] ON  [oso].[stg_Onestep_CaseStages]
--GO

--DROP TABLE IF EXISTS [oso].[stg_Onestep_CaseStages]
--GO

--/****** Object:  Table [oso].[stg_Onestep_CaseStages]    Script Date: 11/04/2019 10:05:49 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [oso].[stg_Onestep_CaseStages](
--	[OnestepCaseNumber] int NULL,
--	[Stage] [varchar](250) NULL,
--	[StageDate] [datetime] NULL
--) ON [PRIMARY]
--GO
--CREATE INDEX idx_stg_Onestep_CaseStages_OnestepCaseNumber
--ON [oso].[stg_Onestep_CaseStages](OnestepCaseNumber);




/****** Object:  Table [oso].[stg_Onestep_Cases]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE IF EXISTS [oso].[stg_Onestep_Cases]
GO


DROP INDEX IF EXISTS [idx_stg_Onestep_Cases_clientcasereference] ON  [oso].[stg_Onestep_Cases]
GO

DROP INDEX IF EXISTS idx_stg_Onestep_Cases_clientid ON  [oso].[stg_Onestep_Cases]
GO

DROP INDEX IF EXISTS idx_stg_Onestep_Cases_OneStepCaseNumber ON  [oso].[stg_Onestep_Cases]
GO

/****** Object:  Table [oso].[stg_Onestep_Cases]    Script Date: 11/04/2019 10:05:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_Onestep_Cases](
	[ClientCaseReference] [varchar](200) NOT NULL,
	[ClientDefaulterReference] [varchar](30) NULL,
	[IssueDate] [datetime] NOT NULL,
	[OffenceCode] [varchar](5) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceLocation] [nvarchar](300) NULL,
	[OffenceCourt] [nvarchar](50) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[VehicleVRM] [varchar](7) NULL,
	[VehicleMake] [varchar](50) NULL,
	[VehicleModel] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TECDate] [datetime] NULL,
	[FineAmount] [decimal](18,4) NOT NULL,
	[Currency] [int] NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[TitleLiable1] [varchar](50) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[NINOLiable1] [varchar](13) NULL,
	[MinorLiable1] [bit] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[MiddlenameLiable2] [nvarchar](50) NULL,
	[FullnameLiable2] [nvarchar](50) NULL,
	[TitleLiable2] [varchar](50) NULL,
	[CompanyNameLiable2] [nvarchar](50) NULL,
	[FirstnameLiable2] [nvarchar](50) NULL,
	[DOBLiable2] [datetime] NULL,
	[LastnameLiable2] [nvarchar](50) NULL,
	[NINOLiable2] [varchar](13) NULL,
	[MinorLiable2] [bit] NULL,
	[Add1Liable2] [nvarchar](80) NULL,
	[Add2Liable2] [nvarchar](320) NULL,
	[Add3Liable2] [nvarchar](100) NULL,
	[Add4Liable2] [nvarchar](100) NULL,
	[Add5Liable2] [nvarchar](100) NULL,
	[AddCountryLiable2] [int] NULL,
	[AddPostCodeLiable2] [varchar](12) NULL,
	[Phone1Liable2] [varchar](50) NULL,
	[Phone2Liable2] [varchar](50) NULL,
	[Phone3Liable2] [varchar](50) NULL,
	[Phone4Liable2] [varchar](50) NULL,
	[Phone5Liable2] [varchar](50) NULL,
	[Email1Liable2] [varchar](250) NULL,
	[Email2Liable2] [varchar](250) NULL,
	[Email3Liable2] [varchar](250) NULL,
	[MiddlenameLiable3] [nvarchar](50) NULL,
	[FullnameLiable3] [nvarchar](150) NULL,
	[TitleLiable3] [varchar](50) NULL,
	[CompanyNameLiable3] [nvarchar](50) NULL,
	[FirstnameLiable3] [nvarchar](50) NULL,
	[DOBLiable3] [datetime] NULL,
	[LastnameLiable3] [nvarchar](50) NULL,
	[NINOLiable3] [varchar](13) NULL,
	[MinorLiable3] [bit] NULL,
	[Add1Liable3] [nvarchar](80) NULL,
	[Add2Liable3] [nvarchar](320) NULL,
	[Add3Liable3] [nvarchar](100) NULL,
	[Add4Liable3] [nvarchar](100) NULL,
	[Add5Liable3] [nvarchar](100) NULL,
	[AddCountryLiable3] [int] NULL,
	[AddPostCodeLiable3] [varchar](12) NULL,
	[Phone1Liable3] [varchar](50) NULL,
	[Phone2Liable3] [varchar](50) NULL,
	[Phone3Liable3] [varchar](50) NULL,
	[Phone4Liable3] [varchar](50) NULL,
	[Phone5Liable3] [varchar](50) NULL,
	[Email1Liable3] [varchar](250) NULL,
	[Email2Liable3] [varchar](250) NULL,
	[Email3Liable3] [varchar](250) NULL,
	[MiddlenameLiable4] [nvarchar](50) NULL,
	[FullnameLiable4] [nvarchar](150) NULL,
	[TitleLiable4] [varchar](50) NULL,
	[CompanyNameLiable4] [nvarchar](50) NULL,
	[FirstnameLiable4] [nvarchar](50) NULL,
	[DOBLiable4] [datetime] NULL,
	[LastnameLiable4] [nvarchar](50) NULL,
	[NINOLiable4] [varchar](13) NULL,
	[MinorLiable4] [bit] NULL,
	[Add1Liable4] [nvarchar](80) NULL,
	[Add2Liable4] [nvarchar](320) NULL,
	[Add3Liable4] [nvarchar](100) NULL,
	[Add4Liable4] [nvarchar](100) NULL,
	[Add5Liable4] [nvarchar](100) NULL,
	[AddCountryLiable4] [int] NULL,
	[AddPostCodeLiable4] [varchar](12) NULL,
	[Phone1Liable4] [varchar](50) NULL,
	[Phone2Liable4] [varchar](50) NULL,
	[Phone3Liable4] [varchar](50) NULL,
	[Phone4Liable4] [varchar](50) NULL,
	[Phone5Liable4] [varchar](50) NULL,
	[Email1Liable4] [varchar](250) NULL,
	[Email2Liable4] [varchar](250) NULL,
	[Email3Liable4] [varchar](250) NULL,
	[MiddlenameLiable5] [nvarchar](50) NULL,
	[FullnameLiable5] [nvarchar](150) NULL,
	[TitleLiable5] [varchar](50) NULL,
	[CompanyNameLiable5] [nvarchar](50) NULL,
	[FirstnameLiable5] [nvarchar](50) NULL,
	[DOBLiable5] [datetime] NULL,
	[LastnameLiable5] [nvarchar](50) NULL,
	[NINOLiable5] [varchar](13) NULL,
	[MinorLiable5] [bit] NULL,
	[Add1Liable5] [nvarchar](80) NULL,
	[Add2Liable5] [nvarchar](320) NULL,
	[Add3Liable5] [nvarchar](100) NULL,
	[Add4Liable5] [nvarchar](100) NULL,
	[Add5Liable5] [nvarchar](100) NULL,
	[AddCountryLiable5] [int] NULL,
	[AddPostCodeLiable5] [varchar](12) NULL,
	[Phone1Liable5] [varchar](50) NULL,
	[Phone2Liable5] [varchar](50) NULL,
	[Phone3Liable5] [varchar](50) NULL,
	[Phone4Liable5] [varchar](50) NULL,
	[Phone5Liable5] [varchar](50) NULL,
	[Email1Liable5] [varchar](250) NULL,
	[Email2Liable5] [varchar](250) NULL,
	[Email3Liable5] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,	
	[OneStepCaseNumber] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[ConnId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[DefaultersNames] [nvarchar](1000) NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[OffenceDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[BillNumber] [varchar](20) NULL,
	[ClientPeriod] [varchar](20) NULL,	
	[EmployerName] [nvarchar](150) NULL,
	[EmployerAddressLine1] [nvarchar](80)  NULL,
	[EmployerAddressLine2] [nvarchar](320) NULL,
	[EmployerAddressLine3] [nvarchar](100) NULL,
	[EmployerAddressLine4] [nvarchar](100) NULL,
	[EmployerPostcode] [varchar](12) NULL,
	[EmployerEmailAddress] [varchar](50) NULL,
	[AddCountryLiable1] [int] NULL,
	[EmployerFaxNumber] [varchar](50) NULL,
	[EmployerTelephone] [varchar](50) NULL,	
	[RollNumber] [nvarchar](1) NULL,
	[Occupation] [nvarchar](1) NULL,
	[BenefitIndicator] [nvarchar](1) NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](1000) NULL,
	[Welfare] [bit] NULL,
	[DateOfBirth] [datetime] NULL,
	[NINO] [nvarchar](13) NULL,
	[OffenceValue] [decimal](18,4) NULL,
	[Imported] [bit] DEFAULT 0,
	[ImportedOn] [datetime] NULL,
	[ErrorId] Int NULL
) ON [PRIMARY]
GO
CREATE INDEX idx_stg_Onestep_Cases_clientcasereference
ON [oso].[stg_Onestep_Cases](ClientCaseReference);

CREATE INDEX idx_stg_Onestep_Cases_clientid
ON [oso].[stg_Onestep_Cases](ClientId);

CREATE INDEX idx_stg_Onestep_Cases_OneStepCaseNumber
ON [oso].[stg_Onestep_Cases](OneStepCaseNumber);


 /****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DT]    Script Date: 05/12/2019 11:46:04 ******/
DROP PROCEDURE [oso].[usp_OS_Cases_XRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DT]    Script Date: 05/12/2019 11:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import Cases from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_XRef_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION

				INSERT INTO dbo.[OldCaseNumbers] (CaseId, OldCaseNumber)
				SELECT c.Id as CaseId, osC.OneStepCaseNumber as OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
				Except
				SELECT c.Id as CaseId, ocn.OldCaseNumber 	  
				FROM cases c 
					inner join dbo.[Batches] b on c.batchid = b.id 
					inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
					inner join dbo.Clients cl on cct.clientId = cl.Id 
					inner join [oso].[stg_Onestep_Cases] osC on (c.ClientCaseReference = osC.ClientCaseReference AND cct.clientId = osC.ClientId)
					inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId AND osC.OneStepCaseNumber = ocn.OldCaseNumber)
				ORDER BY CaseId DESC

				COMMIT
				SELECT 1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH
        END
    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Cases_XRef_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Cases_XRef_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Cases_XRef_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Cases staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Cases_XRef_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_Onestep_Cases]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

-- =============================================
-- Author:  BP
-- Create date: 24-02-2020
-- Description: Update created dates and batches after MME Import
-- =============================================

DROP PROCEDURE IF EXISTS [oso].[usp_OS_CaseAgeUpdate_DT]
GO
CREATE PROCEDURE [oso].[usp_OS_CaseAgeUpdate_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesAgeDetails TABLE
				(
					[CaseId] [int],
					[CreatedOn] [datetime],
					[BatchDate] [datetime],
					[ExpireOn] [datetime],
					[BatchId] [int]
				)

				INSERT @CasesAgeDetails
				(
					[CaseId],
					[CreatedOn],
					[BatchDate],
					[ExpireOn],
					[BatchId]
				)
				SELECT 
					[ocn].[CaseId],
					[osc].[CreatedOn],
					DATEADD(DAY, [cli].[LifeExpectancy], [osc].[CreatedOn]) [ExpireOn],
					convert(date, [osc].[BatchDate]) BatchDate,
					[c].[batchId]	
				FROM
					[oso].[stg_Onestep_Cases] [osc]
					INNER JOIN [oso].[OSColClentsXRef] [clxref] ON [osc].[ClientId] = [clxref].[CClientId]
					INNER JOIN [oso].[stg_Onestep_Clients] [cli] ON [clxref].[Connid] = [cli].[connid]
					INNER JOIN [dbo].[OldCaseNumbers] [ocn] ON [osc].[OneStepCaseNumber] = [ocn].[OldCaseNumber]
					INNER JOIN [dbo].[Cases] [c] on [ocn].[CaseId] = [c].[Id]

				UPDATE [dbo].[Cases]
				SET 
					[Cases].[createdOn] = [cad].[CreatedOn],
					[Cases].[expiresOn] = [cad].[ExpireOn],
					[Cases].[batchLoadDate] = [cad].[BatchDate]
				FROM
					[Cases] [c]
					INNER JOIN @CasesAgeDetails [cad] ON [c].[Id] = [cad].[CaseId]

				UPDATE [dbo].[Batches]
				SET
					[Batches].[batchDate] = [cad].[BatchDate]
				FROM
					[dbo].[Batches] [ba]
					INNER JOIN (
						SELECT BatchId, MIN(BatchDate) BatchDate
						FROM @CasesAgeDetails
						GROUP BY BatchId
					) [cad] on [ba].[Id] = [cad].[BatchId]
				
					
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO


/****** Object:  StoredProcedure [oso].[usp_ODM_GetCasesXMapping]    Script Date: 23/04/2020 10:26:50 ******/
DROP PROCEDURE [oso].[usp_ODM_GetCasesXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_ODM_GetCasesXMapping]    Script Date: 23/04/2020 10:26:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_ODM_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO

