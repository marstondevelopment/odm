
--DROP TABLE IF EXISTS [oso].[OSColFrequencyXRef]
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [oso].[OSColFrequencyXRef]
--(
--	[Id] [INT] IDENTITY CONSTRAINT PK_OSColFrequencyXRef PRIMARY KEY,
--	[OSFrequency] [INT] NOT NULL,
--	[CFrequency] [VARCHAR](50) NOT NULL,
--	[CFrequencyId] [INT] NOT NULL	 
--)
--GO

--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(21,'3 Weekly',7)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(28,'4 Weekly',3)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(1,'Daily',5)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(14,'Fortnightly',2)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(30,'Monthly',4)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(31,'Monthly',4)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(90,'Quarterly',6)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(91,'Quarterly',6)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(92,'Quarterly',6)
--INSERT INTO [oso].[OSColFrequencyXRef]([OSFrequency],[CFrequency],[CFrequencyId])VALUES(7,'Weekly',1) 
--GO
--DROP TABLE IF EXISTS [oso].[stg_OneStep_Arrangements]
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [oso].[stg_OneStep_Arrangements](
--	[cCaseId] [int] NOT NULL,
--	[Reference] [varchar](500) NULL,
--	[Amount] [decimal](18, 4) NOT NULL,
--	[FirstInstalmentAmount] [decimal](18, 4) NOT NULL,
--	[CFrequencyId] [int] NOT NULL,
--	[CUserId] [int] NULL,
--	[StatusId] [int] NULL,
--	[StartDate] [datetime] NULL,
--	[LastInstalmentDate] [datetime] NOT NULL,
--	[FirstInstalmentDate] [datetime] NOT NULL,
--	[NextInstalmentDate] [datetime] NOT NULL,
--	[NoOfIntervals] [int] NOT NULL,
--	[Imported]	[bit] NULL DEFAULT 0,
--	[ImportedOn] [datetime] NULL
--) ON [PRIMARY]
--GO



--DROP PROCEDURE IF EXISTS [oso].[usp_OS_Arrangements_DC_DT]
--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE PROCEDURE [oso].[usp_OS_Arrangements_DC_DT] AS
--BEGIN
--    SET NOCOUNT ON;
--    BEGIN
--        BEGIN TRY
--			DELETE FROM [oso].[stg_OneStep_Arrangements]
--			SELECT 1 as Succeed
--		END TRY
--		BEGIN CATCH
--			INSERT INTO oso.[SQLErrors] VALUES
--						(Error_number()
--							, Error_severity()
--							, Error_state()
--							, Error_procedure()
--							, Error_line()
--							, Error_message()
--							, Getdate()
--						)
--			SELECT 0 as Succeed
--		END CATCH
--    END
--END
	

--GO

/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_DT]    Script Date: 02/08/2020 14:14:59 ******/
DROP PROCEDURE IF EXISTS[oso].[usp_OS_Arrangements_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Arrangements_DT]    Script Date: 02/08/2020 14:14:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Arrangements_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION	
			DECLARE	@Reference VARCHAR(50),
					@Amount DECIMAL(18,4), 
					@FirstInstalmentAmount DECIMAL(18,4),
					@CFrequencyId INT,
					@PaymentType INT,
					@CUserId INT,
					@StatusId INT,
					@StartDate DATETIME,
					@LastEndPeriodDate DATETIME,
					@LastInstalmentDate DATETIME,
					@FirstInstalmentDate DATETIME,
					@ArrangementId INT,
					@NextInstalmentDate	DATETIME,
					@NoOfIntervals INT							
			DECLARE cursorT
				CURSOR FOR
					select distinct
						reference,
						amount,
						FirstInstalmentAmount,
						CFrequencyId,
						2,
						isnull(cuserid, 2),
						StatusId,
						StartDate,
						case
							when CFrequencyId = 1 then startdate
							when CFrequencyId in (2,4,5,6,7,8) then
								case
									when dateadd(day,-fr.NumberOfDays,nextinstalmentdate) > startdate then dateadd(day,-fr.NumberOfDays,nextinstalmentdate) 
									else startdate
								end
							when CFrequencyId in (3,9) then
								case
									when dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) > startdate then dateadd(month,-fr.NumberOfMonths,nextinstalmentdate) 
									else startdate
								end
							else null
						end as LastEndPeriodDate,
						LastInstalmentDate,
						FirstInstalmentDate,
						NextInstalmentDate,
						case
							when CFrequencyId = 1 then 1
							else NoOfIntervals
						end
					from
						oso.[stg_OneStep_Arrangements] stg
						join InstalmentFrequency fr on fr.id = stg.CFrequencyId
					where
						imported = 0

				OPEN cursorT
				FETCH NEXT
					FROM
						cursorT
					INTO
						@Reference, 
						@Amount, 
						@FirstInstalmentAmount, 
						@CFrequencyId, 
						@PaymentType, 
						@CUserId,
						@StatusId, 
						@StartDate, 
						@LastEndPeriodDate,
						@LastInstalmentDate, 
						@FirstInstalmentDate,
						@NextInstalmentDate,
						@NoOfIntervals
						
				WHILE 
					@@FETCH_STATUS = 0
					BEGIN
						INSERT [dbo].[Arrangements] 
						(
							[reference],
							[amount],
							[firstInstalmentAmount],
							[frequencyId],
							[paymentMethodId], 
							[userId],
							[statusId],
							[startDate],
							[lastEndPeriodDate],
							[lastInstalmentDate],
							[FirstInstalmentDate],
							[DeferedPaymentDate],
							[payments]
						)
						VALUES
						(
							CAST(@Reference AS VARCHAR(50)), 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							ISNULL(@StartDate, @FirstInstalmentDate),
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						)
						SET @ArrangementId = SCOPE_IDENTITY();					
							

						INSERT dbo.CaseArrangements
						(
							[caseId],
							[arrangementId]
						)
						select
							cCaseId,
							@ArrangementId
						from
							oso.[stg_OneStep_Arrangements]
						where
							Reference = @Reference


						FETCH NEXT
						FROM
							cursorT
						INTO
							@Reference, 
							@Amount, 
							@FirstInstalmentAmount, 
							@CFrequencyId, 
							@PaymentType, 
							@CUserId,
							@StatusId, 
							@StartDate, 
							@LastEndPeriodDate,
							@LastInstalmentDate, 
							@FirstInstalmentDate,
							@NextInstalmentDate,
							@NoOfIntervals
						
					END			
				CLOSE cursorT
				DEALLOCATE cursorT
			
			UPDATE [oso].[stg_OneStep_Arrangements]
			SET Imported = 1, ImportedOn = GetDate()
			WHERE Imported = 0

			COMMIT TRANSACTION
			SELECT
				1 as [Succeed]
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END	
END
GO


--DROP TABLE IF EXISTS [oso].[OSColInstalmentFrequencyXRef]
--GO
--CREATE TABLE [oso].[OSColInstalmentFrequencyXRef]
--(	[Id] [int] identity(1,1) constraint PK_OSColInstalmentFrequencyXRef PRIMARY KEY,
--	[OSFrequencyDays] [int] NOT NULL,
--	[CFrequencyName] [varchar](50) NOT NULL,
--	[CFrequencyId] [int] NOT NULL
--)
--GO
--INSERT [oso].[OSColInstalmentFrequencyXRef] 
--	([OSFrequencyDays],[CFrequencyName],[CFrequencyId])
--VALUES
--	(1,'OneOff',1),
--	(7,'Weekly',2),
--	(30,'Monthly',3),
--	(31,'Monthly',3),
--	(14,'Fortnightly',4),
--	(1,'Daily',5),
--	(2,'2 Days',6),
--	(21,'3 Weeks',7),
--	(28,'4 Weeks',8),
--	(90,'3 Months',9),
--	(91,'3 Months',9),
--	(92,'3 Months',9)
--GO
---- =============================================
---- Author:		BP
---- Create date: 07-01-2020
---- Description:	Extract Frequency Id for each number of days that comes from OS import file
---- =============================================
--DROP PROCEDURE IF EXISTS [oso].[usp_OS_Arrangements_GetFrequencyId]
--GO
--CREATE PROCEDURE [oso].[usp_OS_Arrangements_GetFrequencyId]
--@Frequency int

--AS
--BEGIN
--	SET NOCOUNT ON;
--    BEGIN
--		SELECT TOP 1 [CFrequencyId]
--		FROM [oso].[OSColInstalmentFrequencyXRef]
--		WHERE [OSFrequencyDays] = @Frequency
--    END
--END
--GO