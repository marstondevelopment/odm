
INSERT INTO [dbo].[DrakeReturnCodes]([code],[name],[abbreviation],[description],[isEnabled],[IsForStaff],[IsForOfficer],[statisticalPositive],[nullaBona],[returnMatchigCaseOnLoad],[ForceReturn],[TranslationId],[IsClientReturn])
VALUES (150, 'Legal Aid - Request withdrawn', 'LAARW', NULL, 1, 1, 0, 1, 0, 0, 1, NULL, 1)
GO

INSERT INTO [dbo].[DrakeReturnCodes]([code],[name],[abbreviation],[description],[isEnabled],[IsForStaff],[IsForOfficer],[statisticalPositive],[nullaBona],[returnMatchigCaseOnLoad],[ForceReturn],[TranslationId],[IsClientReturn])
VALUES (151, 'Legal Aid - Application error', 'LAAAE', NULL, 1, 1, 0, 1, 0, 0, 1, NULL, 1)
GO

INSERT INTO [dbo].[DrakeReturnCodes]([code],[name],[abbreviation],[description],[isEnabled],[IsForStaff],[IsForOfficer],[statisticalPositive],[nullaBona],[returnMatchigCaseOnLoad],[ForceReturn],[TranslationId],[IsClientReturn])
VALUES (152, 'Legal Aid - Acquitted', 'LAAAQ', NULL, 1, 1, 0, 1, 0, 0, 1, NULL, 1)
GO

INSERT INTO [dbo].[DrakeReturnCodes]([code],[name],[abbreviation],[description],[isEnabled],[IsForStaff],[IsForOfficer],[statisticalPositive],[nullaBona],[returnMatchigCaseOnLoad],[ForceReturn],[TranslationId],[IsClientReturn])
VALUES (153, 'Legal Aid - FDC Not Payable', 'LAAFDCNP', NULL, 1, 1, 0, 1, 0, 0, 1, NULL, 1)
GO

INSERT INTO [dbo].[DrakeReturnCodes]([code],[name],[abbreviation],[description],[isEnabled],[IsForStaff],[IsForOfficer],[statisticalPositive],[nullaBona],[returnMatchigCaseOnLoad],[ForceReturn],[TranslationId],[IsClientReturn])
VALUES (154, 'Legal Aid - Write off', 'LAAWO', NULL, 1, 1, 0, 1, 0, 0, 1, NULL, 1)
GO