
DROP TABLE IF EXISTS [oso].[stg_OneStep_CaseStatus_CrossRef]
GO

/****** Object:  Table [oso].[stg_OneStep_CaseStatus_CrossRef]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_CaseStatus_CrossRef](	
	OSCaseStatusName varchar(50) not null,
	CCaseStatusName varchar(50) not null,
	CCaseStatusId int not null,	
) ON [PRIMARY]
GO

DROP TABLE IF EXISTS [oso].[OneStep_CaseStatus_CrossRef]
GO

/****** Object:  Table [oso].[OneStep_CaseStatus_CrossRef]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[OneStep_CaseStatus_CrossRef](	
	OSCaseStatusName varchar(50) not null,
	CCaseStatusName varchar(50) not null,
	CCaseStatusId int not null,	
) ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_CaseStatus_CrossRef_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_CrossRef_DT]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_CaseStatus_CrossRef_DT] AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			INSERT [oso].[OneStep_CaseStatus_CrossRef] 
			(
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			)
			SELECT
				OSCaseStatusName
				,CCaseStatusName
				,CCaseStatusId
			FROM
				[oso].[stg_OneStep_CaseStatus_CrossRef]	 
			COMMIT TRANSACTION
			SELECT
				1 as Succeed
		END TRY
		BEGIN CATCH        
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_CaseStatus_DC_DT]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_CaseStatus_DC_DT]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  BP
-- Create date: 22-10-2019
-- Description: To clear [oso].[stg_OneStep_CaseStatus_CrossRef] staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_CaseStatus_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM [oso].[stg_OneStep_CaseStatus_CrossRef]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCaseStatusXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusXMapping]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- Description:	Select columbus and onestep case status
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT [OSCaseStatusName]
      ,[CCaseStatusName]
	  ,[CCaseStatusId]
  FROM [oso].[OneStep_CaseStatus_CrossRef]
    END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCaseStatusId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_GetCaseStatusId]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		BP
-- Create date: 22-10-2019
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseStatusId]
@OSCaseStatusName varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT 
			Id [CCaseStatusId]
			,name [CCaseStatusName]
		FROM CaseStatus
		WHERE 
			name = CASE @OSCaseStatusName
						WHEN 'Arrangement' THEN 'Unpaid'
						WHEN 'Cancelled' THEN 'Returned'
						WHEN 'Live' THEN 'Unpaid'
						WHEN 'Fully Paid' THEN 'Paid'
						WHEN 'Successful' THEN 'Paid'
						WHEN 'Trace' THEN 'Tracing'
						WHEN 'Expired' THEN 'Returned'
					END
    END
END
GO

-- =============================================
-- Author:		BP
-- Create date: 30-01-2020
-- Description:	Script for updating the status columns for Columbus Cases table 
-- =============================================

DROP TABLE IF EXISTS [oso].[stg_OneStep_CaseStatuses]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_OneStep_CaseStatuses]
(
	[cCaseId] [int] NOT NULL,
	[CCaseStatusId] [int] NOT NULL
)ON [PRIMARY]
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_CStatus_DT]
GO
CREATE PROCEDURE [oso].[usp_OS_CStatus_DT]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				DECLARE @CasesVariableTable TABLE
				(	
					[CaseId] INT,
					[StatusId] INT,
					[StatusDate] DATETIME,
					[PreviousStatusId] INT,
					[PreviousStatusDate] DATETIME
				)

				INSERT @CasesVariableTable 
				(
					[CaseId],
					[StatusId],
					[StatusDate],
					[PreviousStatusId],
					[PreviousStatusDate]
				)
				SELECT 
					[c].[Id], 
					[oss].[CCaseStatusId], 
					GETDATE(),	 
					[c].[statusId], 
					[c].[statusDate]
				FROM [dbo].[Cases] [c]
				INNER JOIN [oso].[stg_OneStep_CaseStatuses] [oss] on [c].[Id] = [oss].[cCaseId]

				UPDATE [dbo].[Cases]
				SET
					[Cases].[statusId] = [cvt].[StatusId],
					[Cases].[statusDate] = [cvt].[StatusDate],
					[Cases].[previousStatusDate] = [cvt].[PreviousStatusDate],
					[Cases].[previousStatusId] = [cvt].[PreviousStatusId]  
				FROM 
					[dbo].[Cases] [c]
					INNER JOIN @CasesVariableTable [cvt] on [c].[Id] = [cvt].[CaseId]
			COMMIT TRANSACTION
			SELECT
				1 AS Succed
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRANSACTION 
			END

			INSERT INTO oso.[SQLErrors] 
			VALUES (Error_number(), 
					Error_severity(), 
					Error_state(), 
					Error_procedure(), 
					Error_line(), 
					Error_message(), 
					Getdate()) 

			SELECT 0 as Succeed
		END CATCH
	END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCStatusId]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_GetCStatusId] @OSCaseStatusName VARCHAR(50)
AS
BEGIN 
	SELECT TOP 1 CCaseStatusId 
	FROM [oso].[OneStep_CaseStatus_CrossRef] 
	WHERE OSCaseStatusName = @OSCaseStatusName
END
GO

--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_CaseStatus_CrossRef_DT] TO FUTUser;
--GRANT ALTER ON OBJECT::[oso].[stg_OneStep_CaseStatus_CrossRef] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_GetCaseStatusXMapping] TO FUTUser;
--GRANT EXECUTE ON OBJECT::[oso].[usp_OS_GetCaseStatusId] TO FUTUser;