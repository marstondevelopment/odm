DROP PROCEDURE IF EXISTS [oso].[usp_CheckOSCaseNumber]
GO
CREATE PROCEDURE [oso].[usp_CheckOSCaseNumber]
@CaseNumber nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		SELECT 
			@Id = [ocn].[CaseId]	  
		FROM 
			[dbo].[OldCaseNumbers] [ocn]		  
		WHERE 
			[ocn].[OldCaseNumber] = @CaseNumber		

		SELECT 
			CASE WHEN @Id is null THEN 1	ELSE 0 END AS Valid

    END
END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_NewCase_GetXMapping]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_NewCase_GetXMapping]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_NewCase_GetXMapping]
@ClientName nvarchar (250)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select c.Id as CCaseId, c.ClientCaseReference, cl.Id as ClientId 	  
	   from cases c 
		  inner join dbo.[Batches] b on c.batchid = b.id 
		  inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
		  inner join dbo.Clients cl on cct.clientId = cl.Id 
		  inner join dbo.CountryDetails cd on c.CountryId =  cd.id 
		where 
			cl.name IN (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 999999999 AS ClientId
    END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCasesXMapping]
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (7,8,9)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO


DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetCasesXMapping_DCA_LAA]
GO
CREATE PROCEDURE [oso].[usp_OS_GetCasesXMapping_DCA_LAA]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		SELECT c.Id as CCaseId, ocn.OldCaseNumber AS CaseNumber 	  
		FROM cases c 
			inner join dbo.[Batches] b on c.batchid = b.id 
			inner join dbo.ClientCaseType cct on b.clientCaseTypeId = cct.id
			inner join dbo.Clients cl on cct.clientId = cl.Id 
			inner join dbo.OldCaseNumbers ocn on (c.id = ocn.CaseId)
		WHERE
			cct.BrandId in (10,11,12,13)
		
		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS CaseNumber


    END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_Cases_GetClientsXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_OS_Cases_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			c.[ConnId]
			,c.[CClientId] as ClientId
			,c.[ClientName]
			,CASE
			WHEN ct.name ='Council Tax Liability Order' THEN 'CT'
			WHEN ct.name ='National Non Domestic Rates' THEN 'ND'
			WHEN ct.name ='RTA/TMA (RoadTraffic/Traffic Management)' THEN 'PC'
			WHEN ct.name ='Child Maintenance Group' THEN 'CS'
			WHEN ct.name ='Legal Aid' THEN 'LAA'
			WHEN ct.name ='Debt Collection' THEN 'DCA'
		END AS OffenceCode

		FROM [oso].[OSColClentsXRef] c 
		INNER JOIN dbo.ClientCaseType cct ON c.CClientId = cct.clientId
		INNER JOIN dbo.CaseType ct ON cct.caseTypeId = ct.Id
    END
END
GO