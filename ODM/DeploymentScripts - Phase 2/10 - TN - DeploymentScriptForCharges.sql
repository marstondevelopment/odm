
/****** Object:  Table [oso].[Staging_OS_Charges]    Script Date: 11/04/2019 10:05:49 ******/
DROP TABLE [oso].[Staging_OS_Charges]
GO

/****** Object:  Table [oso].[Staging_OS_Charges]    Script Date: 16/04/2019 10:36:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[Staging_OS_Charges](
	[CCaseId] [int] NOT NULL,
	[CSchemeChargeId] [int] NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
	[CUserId] [int] NULL,
	[COfficerId] [int] NULL,
	[PaidAmount] [decimal](18, 4) NOT NULL,
	[Imported]	[bit] NULL DEFAULT 0,
	[ImportedOn] [datetime] NULL
) ON [PRIMARY]
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Charges_GetCaseId]    Script Date: 25/04/2019 09:45:11 ******/
DROP PROCEDURE [oso].[usp_OS_Charges_GetCaseId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Charges_GetCaseId]    Script Date: 11/04/2019 10:13:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract case id for each imported case
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_GetCaseId]
@CaseNumber nvarchar (50)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
      Select top 1 c.Id as CCaseId 	  
	   from cases c 
			inner join [dbo].[OldCaseNumbers] ocn on ocn.CaseId = c.Id  
		where 
			ocn.OldCaseNumber = @CaseNumber
    END
END
GO



/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DT]    Script Date: 11/04/2019 10:15:26 ******/
DROP PROCEDURE [oso].[usp_OS_Charges_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DT]    Script Date: 11/04/2019 10:15:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 08-04-2019
-- Description:	Copy data from staging table to CaseCharges table.
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_DT]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						INSERT INTO [dbo].[CaseCharges]( 
								[caseId]
								,[userId]
								,[officerId]
								,[amount]
								,[chargeDate]
								,[schemeChargeId]
								,[reversedOn]
								,[vatAmount]
								,[paidAmount]
								,[paidVatAmount]
								,[casePaymentId]
								,[refundId]
								,[CaseNoteId]
								,[isCaseBalanceAdjust]
								,[ClientInvoiceRunId])
						SELECT [cCaseId]
								,[CUserId]
								,[COfficerId]
								,[Amount]
								,[AddedOn]
								,[CSchemeChargeId]
								,NULL
								,0.00
								,[PaidAmount]
								,0.00
								,NULL
								,NULL
								,NULL
								,0
								,NULL
						FROM [oso].[Staging_OS_Charges] 
						WHERE 
							Imported = 0

						UPDATE [oso].[Staging_OS_Charges]
						SET Imported = 1, ImportedOn = GetDate()
						WHERE Imported = 0
					COMMIT TRANSACTION
					SELECT 1 as Succeed
				END TRY
				BEGIN CATCH        
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION 
					END

					INSERT INTO oso.[SQLErrors] 
					VALUES (Error_number(), 
							Error_severity(), 
							Error_state(), 
							Error_procedure(), 
							Error_line(), 
							Error_message(), 
							Getdate()) 

					SELECT 0 as Succeed
				END CATCH
	
    END
END
GO


/****** Object:  StoredProcedure [oso].[[usp_OS_charges_GetSchemeChargeXMapping]]    Script Date: 04/11/2019 15:57:25 ******/
DROP PROCEDURE [oso].[usp_OS_charges_GetSchemeChargeXMapping]
GO

/****** Object:  StoredProcedure [oso].[[usp_OS_charges_GetSchemeChargeXMapping]]    Script Date: 04/11/2019 15:57:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Updated by:	BP
-- Update on:	09-10-2019
-- Description:	Select columbus and onestep charge type
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_charges_GetSchemeChargeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT [CSchemeChargeName]
		  ,[OSSchemeChargeName] AS ChargeType
		  ,[CSchemeChargeId]
		FROM [oso].[OneStep_SchemeCharge_CrossRef]	
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Charges_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Charges_DC_DT]
GO

/****** Object:  StoredProcedure oso.[usp_OS_Charges_DC_DT]  Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear charges staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Charges_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  Delete From [oso].[Staging_OS_Charges]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO

	

