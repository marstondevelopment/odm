
--IF NOT EXISTS ( SELECT  *
--                FROM    sys.schemas
--                WHERE   name = N'oso' )
--    EXEC('CREATE SCHEMA [oso]');
--GO


--/****** Object:  Table [oso].[SQLErrors]    Script Date: 11/04/2019 10:08:18 ******/
--DROP TABLE [oso].[SQLErrors]
--GO

--/****** Object:  Table [oso].[SQLErrors]    Script Date: 11/04/2019 10:08:18 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [oso].[SQLErrors](
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[ErrorNumber] [int] NULL,
--	[Severity] [int] NULL,
--	[State] [int] NULL,
--	[ErrorProcedure] [nvarchar](128) NULL,
--	[ErrorLine] [int] NULL,
--	[ErrorMessage] [nvarchar](4000) NULL,
--	[ExecutedOn] [datetime] NULL,
-- CONSTRAINT [PK_SQLErrors] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO


--/****** Object:  Table [oso].[stg_Onestep_Clients]    Script Date: 11/04/2019 10:05:49 ******/
--DROP TABLE IF EXISTS [oso].[stg_Onestep_Clients]
--GO

--/****** Object:  Table [oso].[stg_Onestep_Clients]    Script Date: 11/04/2019 10:05:49 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [oso].[stg_Onestep_Clients](
--connid													int	NOT NULL,
--Id														int NULL,
--Name													nvarchar(80) NOT NULL,
--Abbreviation											nvarchar(80) NOT NULL,
--ParentClientId											int NULL,
--firstLine												nvarchar(80) NULL,
--PostCode												varchar(12)	NOT NULL,
--Address													nvarchar(500) NULL,
--CountryId												nvarchar(100) NULL,
--RegionId												int	NULL,
--Brand													int	NOT NULL,
--IsActive												bit	NOT NULL,
--CaseTypeId												int	NOT NULL,
--AbbreviationName										nvarchar(80) NOT NULL,
--BatchRefPrefix											varchar(50)	NULL,
--ContactName												nvarchar(256) NULL,
--ContactAddress											nvarchar(500) NULL,
--OfficeId												int	NOT NULL,
--PaymentDistributionId									int	NOT NULL,
--PriorityCharges											varchar(50) NOT NULL,
--LifeExpectancy											int	NOT NULL,
--FeeCap													decimal(18,4) NOT NULL,
--minFees													decimal(18,4) NOT NULL,
--MaximumArrangementLength								int	NULL,
--ClientPaid												bit	NOT NULL,
--OfficerPaid												bit	NOT NULL,
--permitArrangement										bit	NOT NULL,
--NotifyAddressChange										bit	NOT NULL,
--Reinstate												bit	NOT NULL,
--AssignMatches											bit	NOT NULL,
--showNotes												bit	NOT NULL,
--showHistory												bit	NOT NULL,
--IsDirectHoldWithLifeExtPastExpiryAllowed				bit	NOT NULL,
--xrefEmail												varchar(512) NULL,
--PaymentRunFrequencyId									int	NULL,
--InvoiceRunFrequencyId									int	NULL,
--commissionInvoiceFrequencyId							int	NULL,
--isSuccessfulCompletionFee								bit	NOT NULL,
--successfulCompletionFee									decimal(18,4) NOT NULL,
--completionFeeType										bit	NOT NULL,
--feeInvoiceFrequencyId									int	NULL,
--standardReturn											bit	NOT NULL,
--paymentMethod											int	NULL,
--chargingScheme											varchar(50)	NOT NULL,
--paymentScheme											varchar(50)	NOT NULL,
--enforceAtNewAddress										bit	NOT NULL,
--defaultHoldTimePeriod									int	NOT NULL,
--addressChangeEmail										varchar(512) NULL,
--addressChangeStage										varchar(150) NULL,
--newAddressReturnCode									int	NULL,
--autoReturnExpiredCases									bit	NOT NULL,
--generateBrokenLetter									bit	NOT NULL,
--useMessageExchange										bit	NOT NULL,
--costCap													decimal(18,4) NOT NULL,
--includeUnattendedReturns								bit	NOT NULL,
--assignmentSchemeCharge									int	NULL,
--expiredCasesAssignable									bit	NOT NULL,
--useLocksmithCard										bit	NOT NULL,
--EnableAutomaticLinking									bit	NOT NULL,
--linkedCasesMoneyDistribution							int	NOT NULL,
--isSecondReferral										bit	NOT NULL,
--PermitGoodsRemoved										bit	NOT NULL,
--EnableManualLinking										bit	NOT NULL,
--PermitVRM												bit	NOT NULL,
--IsClientInvoiceRunPermitted								bit	NOT NULL,
--IsNegativeRemittancePermitted							bit	NOT NULL,
--ContactCentrePhoneNumber								varchar(50)	NULL,
--AutomatedLinePhoneNumber								varchar(50)	NULL,
--TraceCasesViaWorkflow									bit	NOT NULL,
--IsTecAuthorizationApplicable							bit	NOT NULL,
--TecDefaultHoldPeriod									int	NOT NULL,
--MinimumDaysRemainingForCoa								int	NOT NULL,
--TecMinimumDaysRemaining									int	NOT NULL,
--IsDecisioningViaWorkflowUsed							bit	NOT NULL,
--AllowReverseFeesOnPrimaryAddressChanged					bit	NOT NULL,
--IsClientInformationExchangeScheme						bit	NOT NULL,
--ClientLifeExpectancy									int	NULL,
--DaysToExtendCaseDueToTrace								int	NULL,
--DaysToExtendCaseDueToArrangement						int	NULL,
--IsWelfareReferralPermitted								bit	NOT NULL,
--IsFinancialDifficultyReferralPermitted					bit	NOT NULL,
--IsReturnCasePrematurelyPermitted						bit	NOT NULL,
--CaseAgeForTBTPEnquiry									int	NULL,
--PermitDvlaEnquiries										bit	NOT NULL,
--IsAutomaticDvlaRecheckEnabled							bit	NOT NULL,
--DaysToAutomaticallyRecheckDvla							int	NULL,
--IsComplianceFeeAutoReversed								bit	NOT NULL,
--LetterActionTemplateRequiredForCoaInDataCleanseId		int	NULL,
--LetterActionTemplateRequiredForCoaInComplianceId 		int	NULL,
--LetterActionTemplateRequiredForCoaInEnforcementId		int	NULL,
--LetterActionTemplateRequiredForCoaInCaseMonitoringId	int	NULL,
--CategoryRequiredForCoaInDataCleanseId					int	NULL,
--CategoryRequiredForCoaInComplianceId					int	NULL,
--CategoryRequiredForCoaInEnforcementId					int	NULL,
--CategoryRequiredForCoaInCaseMonitoringId				int	NULL,
--IsCoaWithActiveArrangementAllowed						bit	NOT NULL,
--ReturnCap												int	NULL,
--CurrentReturnCap										int	NULL,
--IsLifeExpectancyForCoaEnabled							bit	NOT NULL,
--LifeExpectancyForCoa									int	NOT NULL,
--ExpiredCaseReturnCode									int	NULL,
--WorkflowName											varchar(255) NULL,
--OneOffArrangementTolerance								int NOT NULL,
--DailyArrangementTolerance								int NOT NULL,
--TwoDaysArrangementTolerance							    int NOT NULL,
--WeeklyArrangementTolerance							    int NOT NULL,
--FortnightlyArrangementTolerance						    int NOT NULL,
--ThreeWeeksArrangementTolerance							int NOT NULL,
--FourWeeksArrangementTolerance							int NOT NULL,
--MonthlyArrangementTolerance							    int NOT NULL,
--ThreeMonthsArrangementTolerance							int NOT NULL,
--PDAPaymentMethodId									    varchar(50) NOT NULL,
--Payee												    varchar(150) NULL,
--IsDirectPaymentValid								    bit NOT NULL,
--IsDirectPaymentValidForAutomaticallyLinkedCases		    bit NOT NULL,
--IsChargeConsidered									    bit NOT NULL,
--Interest											    bit NOT NULL,
--InvoicingCurrencyId									    int NOT NULL,
--RemittanceCurrencyId								    int NOT NULL,
--IsPreviousAddressToBePrimaryAllowed					    bit NOT NULL,
--TriggerCOA											    bit NOT NULL,
--ShouldOfficerRemainAssigned							    bit NOT NULL,
--ShouldOfficerRemainAssignedForEB						bit NOT NULL,
--PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit NOT NULL,
--AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit NOT NULL,
--[Imported]												bit NULL DEFAULT 0,
--[ImportedOn]											datetime NULL
--) ON [PRIMARY]
--GO

--/****** Object:  Table [oso].[OSColClentsXRef]    Script Date: 11/04/2019 10:08:18 ******/
--DROP TABLE oso.[OSColClentsXRef]
--GO

--/****** Object:  Table [oso].[OSColClentsXRef]    Script Date: 11/04/2019 10:08:18 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE oso.[OSColClentsXRef](
--	[Id] [int] IDENTITY(1,1) NOT NULL,
--	[Connid] [int] NOT NULL,
--	[CClientId] [int] NOT NULL,
--	ClientName nvarchar (250) NULL
-- CONSTRAINT [PK_OSColClentsXRef] PRIMARY KEY CLUSTERED 
--(
--	[Id] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO


/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 14/06/2019 10:58:07 ******/
DROP PROCEDURE [oso].[usp_CheckOSClientName]
GO

/****** Object:  StoredProcedure [oso].[usp_CheckOSClientName]    Script Date: 14/06/2019 10:58:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [oso].[usp_CheckOSClientName]
@Name nvarchar (250),
--@CaseType nvarchar (250),
@Brand int

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN

		DECLARE @Id INT;		
		
		Select @Id = c.Id	  
	   from dbo.Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @Name
		AND cct.[BrandId] = @Brand

		Select 
			CASE
				WHEN @Id is null THEN 1
				ELSE 0
			END AS 	Valid

    END
END
GO 


/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 31/07/2019 14:30:55 ******/
DROP PROCEDURE [oso].[usp_OS_Clients_GetClientId]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_GetClientId]    Script Date: 31/07/2019 14:30:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_GetClientId]
@ClientName nvarchar (250),
@CaseType nvarchar (250)

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		DECLARE @OSClientName nvarchar(500)
		Set @OSClientName = @ClientName + ' (' + @CaseType + ')'
		
		Select top 1 c.Id	  
	   from Clients c 
		  inner join dbo.ClientCaseType cct on c.id = cct.ClientId
		where 
		c.[name] = @OSClientName
		AND cct.[abbreviationName] = @CaseType
    END
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DT]    Script Date: 28/04/2020 10:37:34 ******/
DROP PROCEDURE [oso].[usp_OS_Clients_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DT]    Script Date: 28/04/2020 10:37:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To import clients from OS Data File
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DT]
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN TRY
                BEGIN TRANSACTION
                DECLARE 
					@connid	int,
					@Id	int,
					@Name	nvarchar(80),
					@Abbreviation	nvarchar(80),
					@ParentClientId int,
					@firstLine	nvarchar(80),
					@PostCode	varchar(12),
					@Address	nvarchar(500),
					@CountryId nvarchar(100),
					@RegionId	int,
					@Brand	int,
					@IsActive	bit,
					@CaseTypeId	int,
					@AbbreviationName	nvarchar(80),
					@BatchRefPrefix	varchar(50),
					@ContactName	nvarchar(256),
					@ContactAddress	nvarchar(500),
					@OfficeId	int,
					@PaymentDistributionId	int,
					@PriorityCharges	varchar(50),
					@LifeExpectancy	int,
					@FeeCap	decimal(18,4),
					@minFees	decimal(18,4),
					@MaximumArrangementLength	int,
					@ClientPaid	bit,
					@OfficerPaid	bit,
					@permitArrangement	bit,
					@NotifyAddressChange	bit,
					@Reinstate	bit,
					@AssignMatches	bit,
					@showNotes	bit,
					@showHistory	bit,
					@IsDirectHoldWithLifeExtPastExpiryAllowed	bit,
					@xrefEmail	varchar(512),
					@PaymentRunFrequencyId	int,
					@InvoiceRunFrequencyId	int,
					@commissionInvoiceFrequencyId	int,
					@isSuccessfulCompletionFee	bit,
					@successfulCompletionFee	decimal(18,4),
					@completionFeeType	bit,
					@feeInvoiceFrequencyId	int,
					@standardReturn	bit,
					@paymentMethod	int,
					@chargingScheme	varchar(50),
					@paymentScheme	varchar(50),
					@enforceAtNewAddress	bit,
					@defaultHoldTimePeriod	int,
					@addressChangeEmail	varchar(512),
					@addressChangeStage	varchar(150),
					@newAddressReturnCode	int,
					@autoReturnExpiredCases	bit,
					@generateBrokenLetter	bit,
					@useMessageExchange	bit,
					@costCap	decimal(18,4),
					@includeUnattendedReturns	bit,
					@assignmentSchemeCharge	int,
					@expiredCasesAssignable	bit,
					@useLocksmithCard	bit,
					@EnableAutomaticLinking	bit,
					@linkedCasesMoneyDistribution	int,
					@isSecondReferral	bit,
					@PermitGoodsRemoved	bit,
					@EnableManualLinking	bit,
					@PermitVRM	bit,
					@IsClientInvoiceRunPermitted	bit,
					@IsNegativeRemittancePermitted	bit,
					@ContactCentrePhoneNumber	varchar(50),
					@AutomatedLinePhoneNumber	varchar(50),
					@TraceCasesViaWorkflow	bit,
					@IsTecAuthorizationApplicable	bit,
					@TecDefaultHoldPeriod	int,
					@MinimumDaysRemainingForCoa	int,
					@TecMinimumDaysRemaining	int,
					@IsDecisioningViaWorkflowUsed	bit,
					@AllowReverseFeesOnPrimaryAddressChanged	bit,
					@IsClientInformationExchangeScheme	bit,
					@ClientLifeExpectancy	int,
					@DaysToExtendCaseDueToTrace	int,
					@DaysToExtendCaseDueToArrangement	int,
					@IsWelfareReferralPermitted	bit,
					@IsFinancialDifficultyReferralPermitted	bit,
					@IsReturnCasePrematurelyPermitted	bit,
					@CaseAgeForTBTPEnquiry	int,
					@PermitDvlaEnquiries	bit,
					@IsAutomaticDvlaRecheckEnabled bit,
					@DaysToAutomaticallyRecheckDvla int,
					@IsComplianceFeeAutoReversed	bit,
					@LetterActionTemplateRequiredForCoaInDataCleanseId int,
					@LetterActionTemplateRequiredForCoaInComplianceId 	int,
					@LetterActionTemplateRequiredForCoaInEnforcementId		int,
					@LetterActionTemplateRequiredForCoaInCaseMonitoringId	int,
					@CategoryRequiredForCoaInDataCleanseId					int,
					@CategoryRequiredForCoaInComplianceId					int,
					@CategoryRequiredForCoaInEnforcementId					int,
					@CategoryRequiredForCoaInCaseMonitoringId				int,
					@IsCoaWithActiveArrangementAllowed	bit,
					@ReturnCap	int,
					@CurrentReturnCap	int,
					@IsLifeExpectancyForCoaEnabled	bit,
					@LifeExpectancyForCoa	int,
					@ExpiredCaseReturnCode	int,
					@ClientId int  ,
					@WorkflowName	varchar(255),
					@OneOffArrangementTolerance		  int,
					@DailyArrangementTolerance		  int,
					@2DaysArrangementTolerance		  int,
					@WeeklyArrangementTolerance		  int,
					@FortnightlyArrangementTolerance  int,
					@3WeeksArrangementTolerance		  int,
					@4WeeksArrangementTolerance		  int,
					@MonthlyArrangementTolerance	  int,
					@3MonthsArrangementTolerance	  int,
					@PDAPaymentMethod	varchar(50),
					@Payee varchar(150),
					@IsDirectPaymentValid							   bit,
					@IsDirectPaymentValidForAutomaticallyLinkedCases   bit,
					@IsChargeConsidered								   bit,
					@Interest										   bit,
					@InvoicingCurrencyId int,
					@RemittanceCurrencyId int,
					@IsPreviousAddressToBePrimaryAllowed bit,
					@TriggerCOA							 bit,
					@ShouldOfficerRemainAssigned		 bit,
					@ShouldOfficerRemainAssignedForEB	 bit,
					@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun						bit,
					@AllowWorkflowToBlockProcessingIfLinkedToGANTCase						bit,

					@WorkflowTemplateId int,				
					@ClientCaseTypeId int,
					@PhaseIdentity INT, 
					@StageIdentity INT, 
					@ActionIdentity INT,
                    @Comments VARCHAR(250),					
					@PhaseTemplateId int,
					@ChargingSchemeId int,
					@PaymentSchemeId int,
					@country int,
					@PDAPaymentMethodId int,
					@PriorityChargeId int,
					@AddressChangeStageId int,					
					@AddressChangePhaseName varchar(50),
					@AddressChangeStageName varchar(50),
					@ReturnCodeId int				
				
				 

			DECLARE @PId int, @PName varchar(50), @PTypeId int, @PDefaultPhaseLength varchar(4), @PNumberOfDaysToPay varchar(4)
			DECLARE @SId int, @SName varchar(50), @SOrderIndex int, @SOnLoad bit, @SDuration int, @SAutoAdvance bit , @SDeleted bit, @STypeId int, @SIsDeletedPermanently bit
			DECLARE @ATId int, @ATName varchar(50), @ATOrderIndex int, @ATDeleted bit, @ATPreconditionStatement varchar(2000), @ATStageTemplateId int
					, @ATMoveCaseToNextActionOnFail bit, @ATIsHoldAction bit, @ATExecuteFromPhaseDay varchar(7), @ATExecuteOnDeBindingKeyId varchar(7)
					, @ATUsePhaseLengthValue bit, @ATActionTypeName varchar(50)
			DECLARE @APParamValue varchar(500), @APTParameterTypeFullName varchar(200)


				
					   
                DECLARE cursorT
                CURSOR
                    FOR
                    SELECT
						connid
						,Id
						,Name
						,Abbreviation
						,ParentClientId
						,firstLine
						,PostCode
						,Address
						,CountryId
						,RegionId
						,Brand
						,IsActive
						,CaseTypeId
						,AbbreviationName
						,BatchRefPrefix
						,ContactName
						,ContactAddress
						,OfficeId
						,PaymentDistributionId
						,PriorityCharges
						,LifeExpectancy
						,FeeCap
						,minFees
						,MaximumArrangementLength
						,ClientPaid
						,OfficerPaid
						,permitArrangement
						,NotifyAddressChange
						,Reinstate
						,AssignMatches
						,showNotes
						,showHistory
						,IsDirectHoldWithLifeExtPastExpiryAllowed
						,xrefEmail
						,PaymentRunFrequencyId
						,InvoiceRunFrequencyId
						,commissionInvoiceFrequencyId
						,isSuccessfulCompletionFee
						,successfulCompletionFee
						,completionFeeType
						,feeInvoiceFrequencyId
						,standardReturn
						,paymentMethod
						,chargingScheme
						,paymentScheme
						,enforceAtNewAddress
						,defaultHoldTimePeriod
						,addressChangeEmail
						,addressChangeStage
						,newAddressReturnCode
						,autoReturnExpiredCases
						,generateBrokenLetter
						,useMessageExchange
						,costCap
						,includeUnattendedReturns
						,assignmentSchemeCharge
						,expiredCasesAssignable
						,useLocksmithCard
						,EnableAutomaticLinking
						,linkedCasesMoneyDistribution
						,isSecondReferral
						,PermitGoodsRemoved
						,EnableManualLinking
						,PermitVRM
						,IsClientInvoiceRunPermitted
						,IsNegativeRemittancePermitted
						,ContactCentrePhoneNumber
						,AutomatedLinePhoneNumber
						,TraceCasesViaWorkflow
						,IsTecAuthorizationApplicable
						,TecDefaultHoldPeriod
						,MinimumDaysRemainingForCoa
						,TecMinimumDaysRemaining
						,IsDecisioningViaWorkflowUsed
						,AllowReverseFeesOnPrimaryAddressChanged
						,IsClientInformationExchangeScheme
						,ClientLifeExpectancy
						,DaysToExtendCaseDueToTrace
						,DaysToExtendCaseDueToArrangement
						,IsWelfareReferralPermitted
						,IsFinancialDifficultyReferralPermitted
						,IsReturnCasePrematurelyPermitted
						,CaseAgeForTBTPEnquiry
						,PermitDvlaEnquiries
						,IsAutomaticDvlaRecheckEnabled
						,DaysToAutomaticallyRecheckDvla
						,IsComplianceFeeAutoReversed
						,LetterActionTemplateRequiredForCoaInDataCleanseId
						,LetterActionTemplateRequiredForCoaInComplianceId
						,LetterActionTemplateRequiredForCoaInEnforcementId
						,LetterActionTemplateRequiredForCoaInCaseMonitoringId
						,CategoryRequiredForCoaInDataCleanseId
						,CategoryRequiredForCoaInComplianceId
						,CategoryRequiredForCoaInEnforcementId
						,CategoryRequiredForCoaInCaseMonitoringId 
						,IsCoaWithActiveArrangementAllowed
						,ReturnCap
						,CurrentReturnCap
						,IsLifeExpectancyForCoaEnabled
						,LifeExpectancyForCoa
						,ExpiredCaseReturnCode						
						,WorkflowName
						,OneOffArrangementTolerance
						,DailyArrangementTolerance
						,TwoDaysArrangementTolerance
						,WeeklyArrangementTolerance
						,FortnightlyArrangementTolerance
						,ThreeWeeksArrangementTolerance
						,FourWeeksArrangementTolerance
						,MonthlyArrangementTolerance
						,ThreeMonthsArrangementTolerance
						,PDAPaymentMethodId
						,Payee
						,IsDirectPaymentValid
						,IsDirectPaymentValidForAutomaticallyLinkedCases
						,IsChargeConsidered
						,Interest
						,InvoicingCurrencyId
						,RemittanceCurrencyId
						,IsPreviousAddressToBePrimaryAllowed
						,TriggerCOA
						,ShouldOfficerRemainAssigned
						,ShouldOfficerRemainAssignedForEB
						,PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,AllowWorkflowToBlockProcessingIfLinkedToGANTCase

                    FROM
                        oso.[stg_Onestep_Clients]
					WHERE 
						Imported = 0
                    OPEN cursorT
                    FETCH NEXT
                    FROM
                          cursorT
                    INTO
						@connid
						,@Id
						,@Name
						,@Abbreviation
						,@ParentClientId
						,@firstLine
						,@PostCode
						,@Address
						,@CountryId						
						,@RegionId
						,@Brand
						,@IsActive
						,@CaseTypeId
						,@AbbreviationName
						,@BatchRefPrefix
						,@ContactName
						,@ContactAddress
						,@OfficeId
						,@PaymentDistributionId
						,@PriorityCharges
						,@LifeExpectancy
						,@FeeCap
						,@minFees
						,@MaximumArrangementLength
						,@ClientPaid
						,@OfficerPaid
						,@permitArrangement
						,@NotifyAddressChange
						,@Reinstate
						,@AssignMatches
						,@showNotes
						,@showHistory
						,@IsDirectHoldWithLifeExtPastExpiryAllowed
						,@xrefEmail
						,@PaymentRunFrequencyId
						,@InvoiceRunFrequencyId
						,@commissionInvoiceFrequencyId
						,@isSuccessfulCompletionFee
						,@successfulCompletionFee
						,@completionFeeType
						,@feeInvoiceFrequencyId
						,@standardReturn
						,@paymentMethod
						,@chargingScheme
						,@paymentScheme
						,@enforceAtNewAddress
						,@defaultHoldTimePeriod
						,@addressChangeEmail
						,@addressChangeStage
						,@newAddressReturnCode
						,@autoReturnExpiredCases
						,@generateBrokenLetter
						,@useMessageExchange
						,@costCap
						,@includeUnattendedReturns
						,@assignmentSchemeCharge
						,@expiredCasesAssignable
						,@useLocksmithCard
						,@EnableAutomaticLinking
						,@linkedCasesMoneyDistribution
						,@isSecondReferral
						,@PermitGoodsRemoved
						,@EnableManualLinking
						,@PermitVRM
						,@IsClientInvoiceRunPermitted
						,@IsNegativeRemittancePermitted
						,@ContactCentrePhoneNumber
						,@AutomatedLinePhoneNumber
						,@TraceCasesViaWorkflow
						,@IsTecAuthorizationApplicable
						,@TecDefaultHoldPeriod
						,@MinimumDaysRemainingForCoa
						,@TecMinimumDaysRemaining
						,@IsDecisioningViaWorkflowUsed
						,@AllowReverseFeesOnPrimaryAddressChanged
						,@IsClientInformationExchangeScheme
						,@ClientLifeExpectancy
						,@DaysToExtendCaseDueToTrace
						,@DaysToExtendCaseDueToArrangement
						,@IsWelfareReferralPermitted
						,@IsFinancialDifficultyReferralPermitted
						,@IsReturnCasePrematurelyPermitted
						,@CaseAgeForTBTPEnquiry
						,@PermitDvlaEnquiries
						,@IsAutomaticDvlaRecheckEnabled
						,@DaysToAutomaticallyRecheckDvla
						,@IsComplianceFeeAutoReversed
						,@LetterActionTemplateRequiredForCoaInDataCleanseId
						,@LetterActionTemplateRequiredForCoaInComplianceId
						,@LetterActionTemplateRequiredForCoaInEnforcementId		
						,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
						,@CategoryRequiredForCoaInDataCleanseId					
						,@CategoryRequiredForCoaInComplianceId					
						,@CategoryRequiredForCoaInEnforcementId					
						,@CategoryRequiredForCoaInCaseMonitoringId				 
						,@IsCoaWithActiveArrangementAllowed
						,@ReturnCap
						,@CurrentReturnCap
						,@IsLifeExpectancyForCoaEnabled
						,@LifeExpectancyForCoa
						,@ExpiredCaseReturnCode						
						,@WorkflowName
						,@OneOffArrangementTolerance		  
						,@DailyArrangementTolerance		  
						,@2DaysArrangementTolerance		  
						,@WeeklyArrangementTolerance		  
						,@FortnightlyArrangementTolerance  
						,@3WeeksArrangementTolerance		  
						,@4WeeksArrangementTolerance		  
						,@MonthlyArrangementTolerance	  
						,@3MonthsArrangementTolerance	  
						,@PDAPaymentMethod
						,@Payee
						,@IsDirectPaymentValid							 
						,@IsDirectPaymentValidForAutomaticallyLinkedCases 
						,@IsChargeConsidered								 
						,@Interest										 
						,@InvoicingCurrencyId 
						,@RemittanceCurrencyId 
						,@IsPreviousAddressToBePrimaryAllowed 
						,@TriggerCOA							 
						,@ShouldOfficerRemainAssigned		 
						,@ShouldOfficerRemainAssignedForEB	 
						,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
						,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						
					SELECT @ChargingSchemeId = Id FROM ChargingScheme WHERE schemeName = @chargingScheme
					SELECT @PaymentSchemeId	= Id FROM PaymentScheme WHERE name = @paymentScheme
					SELECT @country = Id FROM CountryDetails WHERE Name = @CountryId	



                    WHILE @@FETCH_STATUS = 0
						BEGIN

							INSERT INTO dbo.[Clients]
								(
								  [name]
								  ,[abbreviation]
								  ,[firstLine]
								  ,[postCode]
								  ,[address]
								  ,[RegionId]
								  ,[IsActive]
								  ,[CountryId]
								  ,[ParentClientId]						
								)

								VALUES
								(
									@Name
									,@Abbreviation
									,@firstLine
									,@PostCode
									,@Address
									,@RegionId
									,@IsActive		
									,@country
									,@ParentClientId
								)

							SET @ClientId = SCOPE_IDENTITY();

						SELECT @PDAPaymentMethodId = sp.Id 
						FROM
							PaymentTypes pt
							INNER JOIN SchemePayments sp ON pt.Id = sp.paymentTypeId
							INNER JOIN PaymentScheme ps ON sp.paymentSchemeId = ps.Id
						WHERE 
							pt.name = @PDAPaymentMethod 
							AND ps.name = @paymentScheme  
						
						SET @ReturnCodeId = NULL
						IF (@newAddressReturnCode IS NOT NULL)
						BEGIN
							SELECT @ReturnCodeId = Id From dbo.DrakeReturnCodes WHERE code = @newAddressReturnCode
						END 

						DECLARE @SplitAddressChangeStage table (Id int identity (1,1), Words varchar(50))  
						INSERT @SplitAddressChangeStage (Words) 
						SELECT value from dbo.fnSplit(@addressChangeStage, '-')  
  
						SELECT @AddressChangePhaseName = rtrim(ltrim(Words))
						from @SplitAddressChangeStage WHERE Id = 1

						SELECT @AddressChangeStageName = rtrim(ltrim(Words))
						FROM @SplitAddressChangeStage WHERE Id = 2  
						
						SELECT @AddressChangeStageId = s.Id 
						FROM
							StageTemplates st
							INNER JOIN Stages s on st.Id = s.StageTemplateId  
							INNER JOIN PhaseTemplates pht on st.PhaseTemplateId = pht.Id
						WHERE 
							st.name = @AddressChangeStageName 
							and pht.Name = @AddressChangePhaseName
							and s.clientId = @ClientId 
							and pht.WorkflowTemplateId = (select Id from WorkflowTemplates where name = @WorkflowName)


						-- Add new ClientCaseType
						   INSERT INTO dbo.[ClientCaseType]
								  ( 
									ClientId
									,BrandId
									,caseTypeId
									,abbreviationName
									,batchRefPrefix
									,contactName
									,contactAddress
									,officeId
									,paymentDistributionId
									,lifeExpectancy
									,feeCap
									,minFees
									,maximumArrangementLength
									,clientPaid
									,officerPaid
									,permitArrangement
									,notifyAddressChange
									,reinstate
									,assignMatches
									,showNotes
									,showHistory
									,IsDirectHoldWithLifeExtPastExpiryAllowed
									,xrefEmail
									,paymentRunFrequencyId
									--,clientInvoiceRunFrequencyId
									,commissionInvoiceFrequencyId
									,isSuccessfulCompletionFee
									,successfulCompletionFee
									,completionFeeType
									,feeInvoiceFrequencyId
									,standardReturn
									,paymentMethod
									,chargingSchemeId
									,paymentSchemeId
									,enforceAtNewAddress
									,defaultHoldTimePeriod
									,addressChangeEmail
									,addressChangeStage
									,newAddressReturnCodeId
									,autoReturnExpiredCases
									,generateBrokenLetter
									,useMessageExchange
									,costCap
									,includeUnattendedReturns
									,assignmentSchemeChargeId
									,expiredCasesAssignable
									,useLocksmithCard
									,enableAutomaticLinking
									,linkedCasesMoneyDistribution
									,isSecondReferral
									,PermitGoodsRemoved
									,EnableManualLinking
									,PermitVrm
									,IsClientInvoiceRunPermitted
									,IsNegativeRemittancePermitted
									,ContactCentrePhoneNumber
									,AutomatedLinePhoneNumber
									,TraceCasesViaWorkflow
									,IsTecAuthorizationApplicable
									,TecDefaultHoldPeriod
									,MinimumDaysRemainingForCoa
									,TecMinimumDaysRemaining
									,IsDecisioningViaWorkflowUsed
									,AllowReverseFeesOnPrimaryAddressChanged
									,IsClientInformationExchangeScheme
									,ClientLifeExpectancy
									,DaysToExtendCaseDueToTrace
									,DaysToExtendCaseDueToArrangement
									,IsWelfareReferralPermitted
									,IsFinancialDifficultyReferralPermitted
									,IsReturnCasePrematurelyPermitted
									,CaseAgeForTBTPEnquiry
									,PermitDvlaEnquiries
									,IsAutomaticDvlaRecheckEnabled
									,DaysToAutomaticallyRecheckDvla
									,IsComplianceFeeAutoReversed
									,LetterActionTemplateRequiredForCoaId
									,LetterActionTemplateRequiredForCoaInDataCleanseId
									,LetterActionTemplateRequiredForCoaInEnforcementId
									,LetterActionTemplateRequiredForCoaInCaseMonitoringId
									,CategoryRequiredForCoaInDataCleanseId
									,CategoryRequiredForCoaInComplianceId
									,CategoryRequiredForCoaInEnforcementId
									,CategoryRequiredForCoaInCaseMonitoringId
									,IsCoaWithActiveArrangementAllowed
									,ReturnCap
									,CurrentReturnCap
									,IsLifeExpectancyForCoaEnabled
									,LifeExpectancyForCoa
									,ExpiredCaseReturnCodeId									
									,PdaPaymentMethodId
									,payee
									,IsDirectPaymentValid
									,IsDirectPaymentValidForAutomaticallyLinkedCases
									,IsChargeConsidered
									,Interest
									,InvoicingCurrencyId
									,RemittanceCurrencyId
									,IsPreviousAddressToBePrimaryAllowed
									,TriggerCOA
									,ShouldOfficerRemainAssigned
									,ShouldOfficerRemainAssignedForEB	
									,BlockLinkedGantCasesProcessingViaWorkflow
									,IsHoldCaseIncludedInCPRandCIR
								  
								  )
								  VALUES
								  ( 
										@ClientId
										,@Brand
										,@CaseTypeId
										,@AbbreviationName
										,@BatchRefPrefix
										,@ContactName
										,@ContactAddress
										,@OfficeId
										,@PaymentDistributionId
										,@LifeExpectancy
										,@FeeCap
										,@minFees
										,@MaximumArrangementLength
										,@ClientPaid
										,@OfficerPaid
										,@permitArrangement
										,@NotifyAddressChange
										,@Reinstate
										,@AssignMatches
										,@showNotes
										,@showHistory
										,@IsDirectHoldWithLifeExtPastExpiryAllowed
										,@xrefEmail
										,@PaymentRunFrequencyId
										--,@InvoiceRunFrequencyId
										,@commissionInvoiceFrequencyId
										,@isSuccessfulCompletionFee
										,@successfulCompletionFee
										,@completionFeeType
										,@feeInvoiceFrequencyId
										,@standardReturn
										,@paymentMethod
										,@ChargingSchemeId
										,@PaymentSchemeId
										,@enforceAtNewAddress
										,@defaultHoldTimePeriod
										,@addressChangeEmail
										,@AddressChangeStageId
										,@ReturnCodeId
										,@autoReturnExpiredCases
										,@generateBrokenLetter
										,@useMessageExchange
										,@costCap
										,@includeUnattendedReturns
										,@assignmentSchemeCharge
										,@expiredCasesAssignable
										,@useLocksmithCard
										,@EnableAutomaticLinking
										,@linkedCasesMoneyDistribution
										,@isSecondReferral
										,@PermitGoodsRemoved
										,@EnableManualLinking
										,@PermitVRM
										,@IsClientInvoiceRunPermitted
										,@IsNegativeRemittancePermitted
										,@ContactCentrePhoneNumber
										,@AutomatedLinePhoneNumber
										,@TraceCasesViaWorkflow
										,@IsTecAuthorizationApplicable
										,@TecDefaultHoldPeriod
										,@MinimumDaysRemainingForCoa
										,@TecMinimumDaysRemaining
										,@IsDecisioningViaWorkflowUsed
										,@AllowReverseFeesOnPrimaryAddressChanged
										,@IsClientInformationExchangeScheme
										,@ClientLifeExpectancy
										,@DaysToExtendCaseDueToTrace
										,@DaysToExtendCaseDueToArrangement
										,@IsWelfareReferralPermitted
										,@IsFinancialDifficultyReferralPermitted
										,@IsReturnCasePrematurelyPermitted
										,@CaseAgeForTBTPEnquiry
										,@PermitDvlaEnquiries
										,@IsAutomaticDvlaRecheckEnabled
										,@DaysToAutomaticallyRecheckDvla
										,@IsComplianceFeeAutoReversed
										,@LetterActionTemplateRequiredForCoaInComplianceId
										,@LetterActionTemplateRequiredForCoaInDataCleanseId
										,@LetterActionTemplateRequiredForCoaInEnforcementId
										,@LetterActionTemplateRequiredForCoaInCaseMonitoringId
										,@CategoryRequiredForCoaInDataCleanseId
										,@CategoryRequiredForCoaInComplianceId
										,@CategoryRequiredForCoaInEnforcementId
										,@CategoryRequiredForCoaInCaseMonitoringId 
										,@IsCoaWithActiveArrangementAllowed
										,@ReturnCap
										,@CurrentReturnCap
										,@IsLifeExpectancyForCoaEnabled
										,@LifeExpectancyForCoa
										,@ExpiredCaseReturnCode
										,@PDAPaymentMethodId
										,@Payee
										,@IsDirectPaymentValid
										,@IsDirectPaymentValidForAutomaticallyLinkedCases
										,@IsChargeConsidered
										,@Interest
										,@InvoicingCurrencyId
										,@RemittanceCurrencyId
										,@IsPreviousAddressToBePrimaryAllowed
										,@TriggerCOA
										,@ShouldOfficerRemainAssigned
										,@ShouldOfficerRemainAssignedForEB		
										,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
										,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
									)
							
							SET @ClientCaseTypeId = SCOPE_IDENTITY();
							


							INSERT INTO clientfrequencytolerances (clientcasetypeid, frequencyid, toleranceindays)							
							SELECT 
								@ClientCaseTypeID, 
								Id, 
								case name 
									when 'OneOff' then @OneOffArrangementTolerance
									when 'Daily' then @DailyArrangementTolerance
									when '2 Days' then @2DaysArrangementTolerance
									when 'Weekly' then @WeeklyArrangementTolerance
									when 'Fortnightly' then @FortnightlyArrangementTolerance
									when '3 Weeks' then @3WeeksArrangementTolerance
									when '4 Weeks' then @4WeeksArrangementTolerance
									when 'Monthly' then @MonthlyArrangementTolerance
									when '3 Months' then @3MonthsArrangementTolerance
								end
								FROM InstalmentFrequency						
							
							--SELECT @PriorityChargeId = sc.id
							--FROM 
							--	clientcasetype cct
							--	INNER JOIN ChargingScheme cs ON cs.Id = cct.chargingSchemeId
							--	INNER JOIN SchemeCharges sc ON sc.chargingSchemeId = cs.Id
							--	INNER JOIN ChargeTypes ct ON ct.Id = sc.chargeTypeId
							--WHERE 
							--	cct.id = @ClientCaseTypeId AND ct.Id = (SELECT Id FROM ChargeTypes WHERE name = @PriorityCharges)

							--INSERT INTO PrioritisedClientSchemeCharges (clientcasetypeid, schemechargeid)
							--VALUES (@ClientCaseTypeID, @PriorityChargeId)
							
							-- Attach workflow to client			
							
							SELECT @WorkflowTemplateId = Id  FROM [dbo].[WorkflowTemplates] WHERE Name = @WorkflowName
							IF (@WorkflowTemplateId IS NULL)
							BEGIN
								SET @Comments = 'WorkflowTemplateId not found for ' +  @WorkflowName;
								THROW 51000, @Comments, 163;  														
							END
							

							DECLARE CURSOR_Phases CURSOR LOCAL FAST_FORWARD
							FOR   
							SELECT Id
							, [name]
							, PhaseTypeId
							, DefaultPhaseLength
							, NumberOfDaysToPay
							FROM dbo.PhaseTemplates  
							WHERE WorkflowTemplateID = @WorkflowTemplateID 
							ORDER BY Id
  
							OPEN CURSOR_Phases  
  
							FETCH NEXT FROM CURSOR_Phases   
							INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
  
							WHILE @@FETCH_STATUS = 0  
							BEGIN  
							INSERT INTO dbo.Phases ([name], PhaseTemplateID, PhaseTypeId, IsDisabled, CountOnlyActiveCaseDays, DefaultNumberOfVisits, DefaultPhaseLength, NumberOfDaysToPay)
									VALUES (@PName, @PId, @PTypeId, 0, 0, NULL, @PDefaultPhaseLength, @PNumberOfDaysToPay)					
		
									SET @PhaseIdentity = SCOPE_IDENTITY();

								DECLARE CURSOR_Stages CURSOR LOCAL FAST_FORWARD
								FOR   
								SELECT ST.iD, ST.[Name], ST.OrderIndex, ST.OnLoad, ST.Duration, ST.AutoAdvance, ST.Deleted, ST.TypeId, ST.IsDeletedPermanently
								FROM dbo.PhaseTemplates PT
								JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
								WHERE WorkflowTemplateID = @WorkflowTemplateID
								AND PhaseTemplateId = @PId
								ORDER BY ST.OrderIndex
  
								OPEN CURSOR_Stages  
  
								FETCH NEXT FROM CURSOR_Stages   
								INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  

  
								WHILE @@FETCH_STATUS = 0  
								BEGIN  
									INSERT INTO dbo.Stages ([Name], OrderIndex, ClientID, OnLoad, Duration, AutoAdvance, Deleted, TypeId, PhaseId, StageTemplateId, IsDeletedPermanently) 
											VALUES (@SName, @SOrderIndex, @ClientID, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @PhaseIdentity, @SId, @SIsDeletedPermanently)

									SET @StageIdentity = SCOPE_IDENTITY()
		
									DECLARE CURSOR_Actions CURSOR LOCAL FAST_FORWARD
									FOR   
									SELECT at.Id, at.[Name], at.OrderIndex, at.Deleted, at.PreconditionStatement
									, at.stageTemplateId, at.MoveCaseToNextActionOnFail, at.IsHoldAction, at.ExecuteFromPhaseDay
									, at.ExecuteOnDeBindingKeyId, at.UsePhaseLengthValue, aty.name
									FROM dbo.PhaseTemplates PT
									JOIN dbo.StageTemplates ST ON ST.PhaseTemplateId = PT.Id
									join dbo.ActionTemplates AT ON AT.StageTemplateId = ST.Id
									JOIN dbo.ActionTypes ATY ON AT.ActionTypeId = ATY.Id
									WHERE WorkflowTemplateID = @WorkflowTemplateID
									AND PhaseTemplateId = @PId
									AND StageTemplateId = @SId
									ORDER BY at.OrderIndex
  
									OPEN CURSOR_Actions  
  
									FETCH NEXT FROM CURSOR_Actions   
									INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
									, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
									, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName
  
									WHILE @@FETCH_STATUS = 0  
									BEGIN  
										INSERT INTO dbo.Actions ([Name], OrderIndex, Deleted, PreconditionStatement, stageId, ActionTemplateId, MoveCaseToNextActionOnFail, IsHoldAction, ExecuteFromPhaseDay, ExecuteOnDeBindingKeyId, UsePhaseLengthValue, ActionTypeId)
											VALUES (@ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement, @StageIdentity, @ATId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, (SELECT Id FROM dbo.ActionTypes WHERE name = @ATActionTypeName))

										SET @ActionIdentity = SCOPE_IDENTITY()
			

										INSERT INTO dbo.ActionParameters (ParamValue, ActionId, ActionParameterTemplateId, ActionParameterTypeId)
										SELECT AP.ParamValue, @ActionIdentity, AP.Id, APT.Id
										FROM dbo.ActionParameterTemplates AS AP 
										JOIN dbo.ActionParameterTypes APT ON AP.ActionParameterTypeId = APT.Id
										WHERE AP.ActionTemplateId = @ATId
	

										FETCH NEXT FROM CURSOR_Actions   
										INTO @ATId, @ATName, @ATOrderIndex, @ATDeleted, @ATPreconditionStatement
											, @ATStageTemplateId, @ATMoveCaseToNextActionOnFail, @ATIsHoldAction, @ATExecuteFromPhaseDay
											, @ATExecuteOnDeBindingKeyId, @ATUsePhaseLengthValue, @ATActionTypeName  
									END   
									CLOSE CURSOR_Actions;  
									DEALLOCATE CURSOR_Actions; 


									FETCH NEXT FROM CURSOR_Stages   
									INTO @SId, @SName, @SOrderIndex, @SOnLoad, @SDuration, @SAutoAdvance, @SDeleted, @STypeId, @SIsDeletedPermanently  
								END   
								CLOSE CURSOR_Stages;  
								DEALLOCATE CURSOR_Stages; 
	

								FETCH NEXT FROM CURSOR_Phases   
								INTO @PId, @PName, @PTypeId, @PDefaultPhaseLength, @PNumberOfDaysToPay  
							END   
							CLOSE CURSOR_Phases;  
							DEALLOCATE CURSOR_Phases;  

								
							-- Add to XRef table
							INSERT INTO oso.[OSColClentsXRef]
								(
									[ConnId]
									,[CClientId]
									,[ClientName]
								)

								VALUES
								(
									@connid
									,@ClientId
									,@Name
								)										

							-- New rows creation completed	
							FETCH NEXT
								FROM
									cursorT
								INTO
									@connid
									,@Id
									,@Name
									,@Abbreviation
									,@ParentClientId
									,@firstLine
									,@PostCode
									,@Address
									,@CountryId						
									,@RegionId
									,@Brand
									,@IsActive
									,@CaseTypeId
									,@AbbreviationName
									,@BatchRefPrefix
									,@ContactName
									,@ContactAddress
									,@OfficeId
									,@PaymentDistributionId
									,@PriorityCharges
									,@LifeExpectancy
									,@FeeCap
									,@minFees
									,@MaximumArrangementLength
									,@ClientPaid
									,@OfficerPaid
									,@permitArrangement
									,@NotifyAddressChange
									,@Reinstate
									,@AssignMatches
									,@showNotes
									,@showHistory
									,@IsDirectHoldWithLifeExtPastExpiryAllowed
									,@xrefEmail
									,@PaymentRunFrequencyId
									,@InvoiceRunFrequencyId
									,@commissionInvoiceFrequencyId
									,@isSuccessfulCompletionFee
									,@successfulCompletionFee
									,@completionFeeType
									,@feeInvoiceFrequencyId
									,@standardReturn
									,@paymentMethod
									,@chargingScheme
									,@paymentScheme
									,@enforceAtNewAddress
									,@defaultHoldTimePeriod
									,@addressChangeEmail
									,@addressChangeStage
									,@newAddressReturnCode
									,@autoReturnExpiredCases
									,@generateBrokenLetter
									,@useMessageExchange
									,@costCap
									,@includeUnattendedReturns
									,@assignmentSchemeCharge
									,@expiredCasesAssignable
									,@useLocksmithCard
									,@EnableAutomaticLinking
									,@linkedCasesMoneyDistribution
									,@isSecondReferral
									,@PermitGoodsRemoved
									,@EnableManualLinking
									,@PermitVRM
									,@IsClientInvoiceRunPermitted
									,@IsNegativeRemittancePermitted
									,@ContactCentrePhoneNumber
									,@AutomatedLinePhoneNumber
									,@TraceCasesViaWorkflow
									,@IsTecAuthorizationApplicable
									,@TecDefaultHoldPeriod
									,@MinimumDaysRemainingForCoa
									,@TecMinimumDaysRemaining
									,@IsDecisioningViaWorkflowUsed
									,@AllowReverseFeesOnPrimaryAddressChanged
									,@IsClientInformationExchangeScheme
									,@ClientLifeExpectancy
									,@DaysToExtendCaseDueToTrace
									,@DaysToExtendCaseDueToArrangement
									,@IsWelfareReferralPermitted
									,@IsFinancialDifficultyReferralPermitted
									,@IsReturnCasePrematurelyPermitted
									,@CaseAgeForTBTPEnquiry
									,@PermitDvlaEnquiries
									,@IsAutomaticDvlaRecheckEnabled
									,@DaysToAutomaticallyRecheckDvla
									,@IsComplianceFeeAutoReversed
									,@LetterActionTemplateRequiredForCoaInDataCleanseId
									,@LetterActionTemplateRequiredForCoaInComplianceId
									,@LetterActionTemplateRequiredForCoaInEnforcementId		
									,@LetterActionTemplateRequiredForCoaInCaseMonitoringId	
									,@CategoryRequiredForCoaInDataCleanseId					
									,@CategoryRequiredForCoaInComplianceId					
									,@CategoryRequiredForCoaInEnforcementId					
									,@CategoryRequiredForCoaInCaseMonitoringId				 
									,@IsCoaWithActiveArrangementAllowed
									,@ReturnCap
									,@CurrentReturnCap
									,@IsLifeExpectancyForCoaEnabled
									,@LifeExpectancyForCoa
									,@ExpiredCaseReturnCode						
									,@WorkflowName
									,@OneOffArrangementTolerance		  
									,@DailyArrangementTolerance		  
									,@2DaysArrangementTolerance		  
									,@WeeklyArrangementTolerance		  
									,@FortnightlyArrangementTolerance  
									,@3WeeksArrangementTolerance		  
									,@4WeeksArrangementTolerance		  
									,@MonthlyArrangementTolerance	  
									,@3MonthsArrangementTolerance	  
									,@PDAPaymentMethod
									,@Payee
									,@IsDirectPaymentValid							 
									,@IsDirectPaymentValidForAutomaticallyLinkedCases 
									,@IsChargeConsidered								 
									,@Interest										 
									,@InvoicingCurrencyId 
									,@RemittanceCurrencyId 
									,@IsPreviousAddressToBePrimaryAllowed 
									,@TriggerCOA							 
									,@ShouldOfficerRemainAssigned		 
									,@ShouldOfficerRemainAssignedForEB	
									,@PermitHoldCaseToBeIncludedInClientPaymentRunAndClientInvoiceRun
									,@AllowWorkflowToBlockProcessingIfLinkedToGANTCase
						END
				
					CLOSE cursorT
					DEALLOCATE cursorT
					
					UPDATE [oso].[stg_Onestep_Clients]
					SET Imported = 1, ImportedOn = GetDate()
					WHERE Imported = 0				
					
					COMMIT TRANSACTION
					SELECT
						1 as Succeed
			  END TRY
			  BEGIN CATCH
				ROLLBACK TRANSACTION
				CLOSE cursorT
				DEALLOCATE cursorT
				  INSERT INTO [oso].[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				  SELECT
						 0 as Succeed
			  END CATCH

    END
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
DROP PROCEDURE [oso].[usp_OS_Clients_DC_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_OS_Clients_DC_DT]    Script Date: 26/07/2019 10:40:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  TN
-- Create date: 08-04-2019
-- Description: To clear Clients staging table
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_Clients_DC_DT]
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN
            BEGIN TRY
			  DELETE FROM oso.[stg_Onestep_Clients]
			  SELECT 1 as Succeed
			END TRY
			BEGIN CATCH
				INSERT INTO oso.[SQLErrors] VALUES
						 (Error_number()
							  , Error_severity()
							  , Error_state()
							  , Error_procedure()
							  , Error_line()
							  , Error_message()
							  , Getdate()
						 )
				SELECT 0 as Succeed
			END CATCH
        END
    END
GO


DROP PROCEDURE [oso].[usp_OS_GetClientsXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientsXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	[ConnId]
	,[CClientId]
	,[ClientName]
  FROM [oso].[OSColClentsXRef]
    END
END
GO



-- Add cross-mapping for CaseType

/****** Object:  Table [oso].[OSColCaseTypeXRef]    Script Date: 11/04/2019 10:08:18 ******/
DROP TABLE oso.[OSColCaseTypeXRef]
GO

/****** Object:  Table [oso].[OSColCaseTypeXRef]    Script Date: 11/04/2019 10:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE oso.[OSColCaseTypeXRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CaseType] nvarchar(100) NOT NULL,
	[CaseTypeId] [int] NOT NULL,
 CONSTRAINT [PK_OSColCaseTypeXRef] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--DELETE FROM  oso.[OSColCaseTypeXRef]

--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('BID Levy - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('BID Levy - TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('BID Levy Summons',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Bids',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Bus Lane PCN - TCE',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Bus Lane PCN -TCE',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - 14',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - TCE 16',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - TCE EXT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - TCE EXT 90',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax - TCE FT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax 14 Project',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax 2 - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax CityCentre TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Fast Track',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax OOA - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Rec - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Rec - TCE FT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Rec- TCE FT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Recycled',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax SB - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax T & C - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax T & C 7 - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax T&C - FT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax Wales',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax WRP - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax WRP - TCE EXT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax WRP 2 - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('C Tax WRP-TCE EXT',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMEC Debt - TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMEC Legal - TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMS 2012 Special',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMS 2012 TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMS 2nd Ref TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMS Arrears Only TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CMS Pilot TCE',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Ctax',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CTAX BAU - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CTAX TAX MAX - TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CTax TCE 21',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CTax TCE Rec 21',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('CTaxSBTCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR - TCE 16',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR - TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR 2 - TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR BAU - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR BID - TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR Fast Track',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR Rec - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR Rec - TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR Rec- TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR Summons',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR T & C - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR T & C FT - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR TAX MAX - TCE',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR TCE 21',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('NNDR- TCE FT',	21	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Notts CTAX T&C TCE',	20	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Persistent Evader',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Prime EA Service',	22	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Road Traffic FT TCE',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Road Traffic TCE',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Road Traffic TCE Rec',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('TMA - TCE 16',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('TMA - TCE FT',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('TMA Rec- TCE FT',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('TMA T&C- TCE FT',	9	)
--INSERT INTO oso.[OSCOLCaseTypeXRef] ( CaseType, CaseTypeId) Values ('Commercial Rent',	23	)



DROP PROCEDURE [oso].[usp_OS_GetCaseTypeXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract client id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetCaseTypeXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT	
	CaseType
	,CaseTypeId
  FROM [oso].[OSColCaseTypeXRef]
    END
END
GO

DROP PROCEDURE IF EXISTS [oso].[usp_OS_GetClientParentXMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BP
-- Create date: 27-01-2020
-- Description:	Extract ParentClient id for each imported client
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_GetClientParentXMapping]

AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
		SELECT	
			[Id] ParentClientId,
			[name] ParentClientName
		FROM 
			[dbo].[ParentClient]
    END
END
GO

-- =============================================
-- Author:		BP
-- Create date: 04-02-2020
-- Description:	Insert records into the ParentClient table
-- =============================================

--INSERT INTO dbo.ParentClient(name, isvalid, createdby, createddate)
--VALUES
--('Adur District Council', 1, 2, GETDATE()),
--('Allerdale Borough Council', 1, 2, GETDATE()),
--('Arun District Council', 1, 2, GETDATE()),
--('Ashfield District Council', 1, 2, GETDATE()),
--('Ashford Borough Council', 1, 2, GETDATE()),
--('Barnsley MBC', 1, 2, GETDATE()),
--('Basildon Borough Council', 1, 2, GETDATE()),
--('Basingstoke & Deane B C ', 1, 2, GETDATE()),
--('Bassetlaw District Council', 1, 2, GETDATE()),
--('Blaby District Council', 1, 2, GETDATE()),
--('Black Sluice Internal Drainage Board', 1, 2, GETDATE()),
--('Blackburn with Darwen BC', 1, 2, GETDATE()),
--('Bolton MBC', 1, 2, GETDATE()),
--('Boston Borough Council', 1, 2, GETDATE()),
--('Bournemouth Borough Council', 1, 2, GETDATE()),
--('Bracknell-Forest Borough Council', 1, 2, GETDATE()),
--('Braintree District Council', 1, 2, GETDATE()),
--('Breckland District Council', 1, 2, GETDATE()),
--('Brentwood Borough Council', 1, 2, GETDATE()),
--('Bridgend County Borough Council', 1, 2, GETDATE()),
--('Bristol City Council', 1, 2, GETDATE()),
--('Broadland District Council', 1, 2, GETDATE()),
--('Broads IDB ', 1, 2, GETDATE()),
--('Broxtowe Borough Council', 1, 2, GETDATE()),
--('Burnley Borough Council', 1, 2, GETDATE()),
--('Bury MBC', 1, 2, GETDATE()),
--('Caerphilly County Borough Council', 1, 2, GETDATE()),
--('Cannock Chase District Council ', 1, 2, GETDATE()),
--('Canterbury City', 1, 2, GETDATE()),
--('Cardiff County Council', 1, 2, GETDATE()),
--('Carlisle City Council', 1, 2, GETDATE()),
--('Castle Point Borough Council', 1, 2, GETDATE()),
--('Charnwood Borough Council', 1, 2, GETDATE()),
--('Chelmsford City Council', 1, 2, GETDATE()),
--('Cheltenham BC ', 1, 2, GETDATE()),
--('Chesterfield BC', 1, 2, GETDATE()),
--('Chichester District Council', 1, 2, GETDATE()),
--('Child Maintenance Group', 1, 2, GETDATE()),
--('Chorley Council', 1, 2, GETDATE()),
--('City & County of Swansea', 1, 2, GETDATE()),
--('City of Bradford MDC', 1, 2, GETDATE()),
--('City of London', 1, 2, GETDATE()),
--('City of York Council', 1, 2, GETDATE()),
--('Cobb Farrell & Cudworth', 1, 2, GETDATE()),
--('Colchester Borough Council', 1, 2, GETDATE()),
--('Copeland B C ', 1, 2, GETDATE()),
--('Cornwall Council', 1, 2, GETDATE()),
--('Cotswold DC', 1, 2, GETDATE()),
--('Crawley Borough Council', 1, 2, GETDATE()),
--('Dacorum Borough Council', 1, 2, GETDATE()),
--('Daventry District Council', 1, 2, GETDATE()),
--('Denbighshire CBC', 1, 2, GETDATE()),
--('Devon CC', 1, 2, GETDATE()),
--('Dover District Council', 1, 2, GETDATE()),
--('Durham County Council', 1, 2, GETDATE()),
--('East Cambs District Council', 1, 2, GETDATE()),
--('East Devon DC', 1, 2, GETDATE()),
--('East Hertfordshire District Council', 1, 2, GETDATE()),
--('East Lindsey District Council', 1, 2, GETDATE()),
--('East Staffs BC (Stoke City)', 1, 2, GETDATE()),
--('East Suffolk IDB', 1, 2, GETDATE()),
--('Eastbourne Borough Council ', 1, 2, GETDATE()),
--('Eastleigh Borough Council', 1, 2, GETDATE()),
--('Eden District Council', 1, 2, GETDATE()),
--('Epping Forest District Council', 1, 2, GETDATE()),
--('Erewash Borough Council', 1, 2, GETDATE()),
--('Fareham Borough Council', 1, 2, GETDATE()),
--('Flintshire County Council', 1, 2, GETDATE()),
--('Forest Heath District Council', 1, 2, GETDATE()),
--('Forest of Dean District Council', 1, 2, GETDATE()),
--('Fylde Borough Council Parking', 1, 2, GETDATE()),
--('Gateshead Council ', 1, 2, GETDATE()),
--('Gedling Borough Council', 1, 2, GETDATE()),
--('Gloucester CC ', 1, 2, GETDATE()),
--('Gloucester City Council', 1, 2, GETDATE()),
--('Gosport Borough Council', 1, 2, GETDATE()),
--('Great Yarmouth Borough Council', 1, 2, GETDATE()),
--('Halton Borough Council ', 1, 2, GETDATE()),
--('Harborough DC ', 1, 2, GETDATE()),
--('Hartlepool Borough Council', 1, 2, GETDATE()),
--('Hastings Borough Council', 1, 2, GETDATE()),
--('Hinckley & Bosworth BC', 1, 2, GETDATE()),
--('Horsham District Council', 1, 2, GETDATE()),
--('Hull City Council', 1, 2, GETDATE()),
--('Hyndburn Borough Council', 1, 2, GETDATE()),
--('Isle of Anglesey County Council', 1, 2, GETDATE()),
--('Isle of Wight Council', 1, 2, GETDATE()),
--('Kings Lynn & West Norfolk B.C', 1, 2, GETDATE()),
--('Kings Lynn International Drainage Board ', 1, 2, GETDATE()),
--('Kirklees Council', 1, 2, GETDATE()),
--('Knowsley MBC', 1, 2, GETDATE()),
--('Lancaster City Council', 1, 2, GETDATE()),
--('LB of Barking and Dagenham', 1, 2, GETDATE()),
--('LB of Richmond-upon-Thames', 1, 2, GETDATE()),
--('Leeds City Council', 1, 2, GETDATE()),
--('Leicester City Council', 1, 2, GETDATE()),
--('Lichfield DC (Stoke City)', 1, 2, GETDATE()),
--('Lincoln City Council', 1, 2, GETDATE()),
--('Lindsey Marsh Drainage Board', 1, 2, GETDATE()),
--('Liverpool City Council', 1, 2, GETDATE()),
--('London Borough of Camden', 1, 2, GETDATE()),
--('London Borough Of Hackney', 1, 2, GETDATE()),
--('London Borough Of Merton ', 1, 2, GETDATE()),
--('London Borough of Waltham Forest', 1, 2, GETDATE()),
--('Maldon DC', 1, 2, GETDATE()),
--('Malvern Hills District Council', 1, 2, GETDATE()),
--('Manchester City Council', 1, 2, GETDATE()),
--('Manchester City Council ', 1, 2, GETDATE()),
--('Mansfield District Council', 1, 2, GETDATE()),
--('Melton Borough Council', 1, 2, GETDATE()),
--('Merthyr Tydfil CBC', 1, 2, GETDATE()),
--('Mid Sussex District Council', 1, 2, GETDATE()),
--('Middlesbrough Borough Council', 1, 2, GETDATE()),
--('Milton Keynes Council', 1, 2, GETDATE()),
--('Milton Keynes Council ', 1, 2, GETDATE()),
--('Monmouthshire City Council', 1, 2, GETDATE()),
--('Monmouthshire County Council', 1, 2, GETDATE()),
--('Neath Port Talbot CBC', 1, 2, GETDATE()),
--('New Forest District Council', 1, 2, GETDATE()),
--('Newark & Sherwood DC', 1, 2, GETDATE()),
--('Newcastle City Council', 1, 2, GETDATE()),
--('Newcastle Under Lyme BC ', 1, 2, GETDATE()),
--('Newcastle Upon Tyne City Council', 1, 2, GETDATE()),
--('Newport City Council ', 1, 2, GETDATE()),
--('Norfolk Rivers International Drainage Board ', 1, 2, GETDATE()),
--('North Devon District Council', 1, 2, GETDATE()),
--('North East Lincolnshire Council', 1, 2, GETDATE()),
--('North Hertfordshire DC', 1, 2, GETDATE()),
--('North Kesteven District Council', 1, 2, GETDATE()),
--('North Lincolnshire Council', 1, 2, GETDATE()),
--('North Norfolk District Council', 1, 2, GETDATE()),
--('North Somerset District Council', 1, 2, GETDATE()),
--('North Tyneside Council', 1, 2, GETDATE()),
--('North West Leicestershire DC ', 1, 2, GETDATE()),
--('Northampton BC', 1, 2, GETDATE()),
--('Northumberland County Council', 1, 2, GETDATE()),
--('Norwich CC ', 1, 2, GETDATE()),
--('Nottingham City Council', 1, 2, GETDATE()),
--('Oadby & Wigston Borough Council ', 1, 2, GETDATE()),
--('Oldham MBC ', 1, 2, GETDATE()),
--('Oxford City Council ', 1, 2, GETDATE()),
--('Pembroke County Council', 1, 2, GETDATE()),
--('Pendle Borough Council', 1, 2, GETDATE()),
--('Peterborough City Council', 1, 2, GETDATE()),
--('Portsmouth City Council', 1, 2, GETDATE()),
--('Preston City Council', 1, 2, GETDATE()),
--('RB of Windsor & Maidenhead', 1, 2, GETDATE()),
--('Reigate & Banstead', 1, 2, GETDATE()),
--('Rhondda Cynon Taf CBC', 1, 2, GETDATE()),
--('Ribble Valley B C', 1, 2, GETDATE()),
--('Rochdale MBC', 1, 2, GETDATE()),
--('Rochford District Council', 1, 2, GETDATE()),
--('Rossendale Borough Council', 1, 2, GETDATE()),
--('Rotherham MBC', 1, 2, GETDATE()),
--('Rutland County Council', 1, 2, GETDATE()),
--('Ryedale District Council', 1, 2, GETDATE()),
--('Salford City Council', 1, 2, GETDATE()),
--('Sandwell Council', 1, 2, GETDATE()),
--('Scarborough Borough Council', 1, 2, GETDATE()),
--('Sefton MBC ', 1, 2, GETDATE()),
--('Selby District Council', 1, 2, GETDATE()),
--('Sheffield City Council', 1, 2, GETDATE()),
--('South Essex Parking Partnership', 1, 2, GETDATE()),
--('South Holland District Council', 1, 2, GETDATE()),
--('South Kesteven District Council', 1, 2, GETDATE()),
--('South Staffordshire DC (Stoke City)', 1, 2, GETDATE()),
--('South Tyneside Council', 1, 2, GETDATE()),
--('South Wales Parking', 1, 2, GETDATE()),
--('Southend-on-Sea BC', 1, 2, GETDATE()),
--('St Albans City & District Council', 1, 2, GETDATE()),
--('St Edmundsbury BC', 1, 2, GETDATE()),
--('St Helens MBC ', 1, 2, GETDATE()),
--('Stafford Borough Council', 1, 2, GETDATE()),
--('Staffordshire County Council', 1, 2, GETDATE()),
--('Staffordshire Moorlands DC (Stoke City)', 1, 2, GETDATE()),
--('Stevenage Borough Council', 1, 2, GETDATE()),
--('Stoke on Trent City Council', 1, 2, GETDATE()),
--('Stroud District Council', 1, 2, GETDATE()),
--('Suffolk Coastal District Council', 1, 2, GETDATE()),
--('Sunderland CC ', 1, 2, GETDATE()),
--('Swale Borough Council', 1, 2, GETDATE()),
--('Tameside Council', 1, 2, GETDATE()),
--('Tamworth Borough Council', 1, 2, GETDATE()),
--('Tandridge Borough Council ', 1, 2, GETDATE()),
--('Teignbridge D C', 1, 2, GETDATE()),
--('Telford & Wrekin Borough Council', 1, 2, GETDATE()),
--('Test Valley Borough Council', 1, 2, GETDATE()),
--('Tewkesbury Borough Council', 1, 2, GETDATE()),
--('Thanet District Council', 1, 2, GETDATE()),
--('Three Rivers District Council', 1, 2, GETDATE()),
--('Thurrock Council', 1, 2, GETDATE()),
--('Torbay Council', 1, 2, GETDATE()),
--('Torfaen County Borough Council', 1, 2, GETDATE()),
--('Torridge District Council', 1, 2, GETDATE()),
--('Trafford MBC', 1, 2, GETDATE()),
--('Trent Valley Internal Drainage Board', 1, 2, GETDATE()),
--('Upper Witham Internal Drainage Board', 1, 2, GETDATE()),
--('Uttlesford District Council', 1, 2, GETDATE()),
--('Vale of Glamorgan', 1, 2, GETDATE()),
--('Wakefield Metropolitan District Council', 1, 2, GETDATE()),
--('Warrington Borough Council', 1, 2, GETDATE()),
--('Warwick DC', 1, 2, GETDATE()),
--('Watford Borough Council', 1, 2, GETDATE()),
--('Waverley Borough Council', 1, 2, GETDATE()),
--('Welland & Deepings International Drainage Board', 1, 2, GETDATE()),
--('Wellingborough BC', 1, 2, GETDATE()),
--('Welwyn Hatfield Borough Council', 1, 2, GETDATE()),
--('West Berkshire Council', 1, 2, GETDATE()),
--('West Lancashire B C ', 1, 2, GETDATE()),
--('West Lindsey District Council', 1, 2, GETDATE()),
--('West Oxford District Council', 1, 2, GETDATE()),
--('Westminster City Council', 1, 2, GETDATE()),
--('Wigan MBC', 1, 2, GETDATE()),
--('Wiltshire Council', 1, 2, GETDATE()),
--('Wiltshire Council ', 1, 2, GETDATE()),
--('Winchester CC ', 1, 2, GETDATE()),
--('Witham International Drainage Board', 1, 2, GETDATE()),
--('Wokingham BC', 1, 2, GETDATE()),
--('Worcester City ', 1, 2, GETDATE()),
--('Worthing Borough Council', 1, 2, GETDATE()),
--('Wrexham County Borough Council', 1, 2, GETDATE()),
--('Wychavon District Council', 1, 2, GETDATE()),
--('Wyre Borough Council', 1, 2, GETDATE())


--GRANT SELECT ON SCHEMA::de TO FUTUser
--GRANT SELECT,INSERT,DELETE,UPDATE,EXECUTE ON SCHEMA::oso TO FUTUser
--GRANT SELECT,INSERT ON SCHEMA::Brands TO FUTUser