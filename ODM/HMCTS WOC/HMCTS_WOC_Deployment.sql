/****** Object:  UserDefinedFunction [oso].[SplitString]    Script Date: 12/05/2021 13:28:51 ******/
DROP FUNCTION IF EXISTS [oso].[SplitString]
GO

/****** Object:  UserDefinedFunction [oso].[SplitString]    Script Date: 12/05/2021 13:28:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [oso].[SplitString]
    (
        @List NVARCHAR(MAX),
        @Delim VARCHAR(255)
    )
    RETURNS TABLE
    AS
        RETURN ( SELECT [Value], idx = RANK() OVER (ORDER BY n) FROM 
          ( 
            SELECT n = Number, 
              [Value] = LTRIM(RTRIM(SUBSTRING(@List, [Number],
              CHARINDEX(@Delim, @List + @Delim, [Number]) - [Number])))
            FROM (SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
              FROM sys.all_objects) AS x
              WHERE Number <= LEN(@List)
              AND SUBSTRING(@Delim + @List, [Number], LEN(@Delim)) = @Delim
          ) AS y
        );
GO


ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] DROP CONSTRAINT [DF_stg_HMCTS_WOC_Cases_UploadedOn]
GO

ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] DROP CONSTRAINT [DF__stg_HMCTS_WOC__Impor__2B4EECD3]
GO

ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] DROP CONSTRAINT [DF__stg_HMCTS_WOC__Batch__2A5AC89A]
GO

/****** Object:  Table [oso].[stg_HMCTS_WOC_Cases]    Script Date: 12/05/2021 11:18:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[oso].[stg_HMCTS_WOC_Cases]') AND type in (N'U'))
DROP TABLE [oso].[stg_HMCTS_WOC_Cases]
GO

/****** Object:  Table [oso].[stg_HMCTS_WOC_Cases]    Script Date: 12/05/2021 11:18:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [oso].[stg_HMCTS_WOC_Cases](
	[ClientDefaulterReference] [varchar](30) NULL,
	[ClientCaseReference] [varchar](200) NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[FineAmount] [decimal](18, 4) NOT NULL,
	[StartDate] [datetime] NULL,
	[LastnameLiable1] [nvarchar](50) NULL,
	[FirstnameLiable1] [nvarchar](50) NULL,
	[TitleLiable1] [varchar](50) NULL,
	[DOBLiable1] [datetime] NULL,
	[Add1Liable1] [nvarchar](80) NULL,
	[Add2Liable1] [nvarchar](320) NULL,
	[Add3Liable1] [nvarchar](100) NULL,
	[AddPostCodeLiable1] [varchar](12) NOT NULL,
	[NINOLiable1] [varchar](13) NULL,
	[Phone2Liable1] [varchar](50) NULL,
	[Phone3Liable1] [varchar](50) NULL,
	[Phone1Liable1] [varchar](50) NULL,
	[Email1Liable1] [varchar](250) NULL,
	[Email2Liable1] [varchar](250) NULL,
	[OffenceDescription] [nvarchar](1000) NULL,
	[OffenceDate] [datetime] NULL,
	[OffenceCode] [varchar](5) NULL,
	[DebtAddress1] [nvarchar](80) NOT NULL,
	[DebtAddress2] [nvarchar](320) NULL,
	[DebtAddress3] [nvarchar](100) NULL,
	[DebtAddress4] [nvarchar](100) NULL,
	[DebtAddress5] [nvarchar](100) NULL,
	[DebtAddressCountry] [int] NULL,
	[DebtAddressPostcode] [varchar](12) NOT NULL,
	[TECDate] [datetime] NULL,
	[Currency] [int] NULL,
	[MiddlenameLiable1] [nvarchar](50) NULL,
	[FullnameLiable1] [nvarchar](150) NULL,
	[CompanyNameLiable1] [nvarchar](50) NULL,
	[MinorLiable1] [bit] NULL,
	[Add4Liable1] [nvarchar](100) NULL,
	[Add5Liable1] [nvarchar](100) NULL,
	[IsBusinessAddress] [bit] NOT NULL,
	[Phone4Liable1] [varchar](50) NULL,
	[Phone5Liable1] [varchar](50) NULL,
	[Email3Liable1] [varchar](250) NULL,
	[BatchDate] [datetime] NULL,
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](80) NOT NULL,
	[PhaseDate] [datetime] NULL,
	[StageDate] [datetime] NULL,
	[IsAssignable] [bit] NULL,
	[EndDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[AddCountryLiable1] [int] NULL,
	[CStatus] [varchar](20) NULL,
	[CReturnCode] [int] NULL,
	[OffenceNotes] [nvarchar](MAX) NULL,
	[CaseNotes] [nvarchar](1000) NULL,
	[FUTUserId] [int] NULL,
	[CaseId] [int] NULL,
	[BatchReferenceNo] [varchar](500) NULL,
	[BatchNoGenerated] [bit] NULL,
	[Imported] [bit] NULL,
	[ImportedOn] [datetime] NULL,
	[ErrorId] [int] NULL,
	[UploadedOn] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF__stg_HMCTS_WOC__Batch__2A5AC89A]  DEFAULT ((0)) FOR [BatchNoGenerated]
GO

ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF__stg_HMCTS_WOC__Impor__2B4EECD3]  DEFAULT ((0)) FOR [Imported]
GO

ALTER TABLE [oso].[stg_HMCTS_WOC_Cases] ADD  CONSTRAINT [DF_stg_HMCTS_WOC_Cases_UploadedOn]  DEFAULT (getdate()) FOR [UploadedOn]
GO

/****** Object:  StoredProcedure [oso].[usp_HMCTS_WOC_Cases_DT]    Script Date: 04/05/2021 14:49:25 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_HMCTS_WOC_Cases_DT]
GO

/****** Object:  StoredProcedure [oso].[usp_HMCTS_WOC_Cases_DT]    Script Date: 04/05/2021 14:49:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  TN
-- Create date: 31-03-2020
-- Description: Transpose cases data from staging to production
-- =============================================
CREATE PROCEDURE [oso].[usp_HMCTS_WOC_Cases_DT] AS BEGIN
SET NOCOUNT ON;
BEGIN
    BEGIN TRY
        DECLARE 
			@ClientDefaulterReference varchar(30),
			@ClientCaseReference varchar(200)  ,
			@IssueDate datetime  ,
			@FineAmount decimal(18, 4)  ,
			@StartDate datetime  ,
			@LastnameLiable1 nvarchar(50)  ,
			@FirstnameLiable1 nvarchar(50)  ,
			@TitleLiable1 varchar(50)  ,
			@DOBLiable1 datetime  ,
			@Add1Liable1 nvarchar(80)  ,
			@Add2Liable1 nvarchar(320)  ,
			@Add3Liable1 nvarchar(100)  ,
			@AddPostCodeLiable1 varchar(12)  ,
			@NINOLiable1 varchar(13)  ,
			@Phone2Liable1 varchar(50)  ,
			@Phone3Liable1 varchar(50)  ,
			@Phone1Liable1 varchar(50)  ,
			@Email1Liable1 varchar(250)  ,
			@Email2Liable1 varchar(250)  ,
			@OffenceDescription nvarchar(1000)  ,
			@EndDate datetime  ,
			@OffenceCode varchar(5)  ,
			@DebtAddress1 nvarchar(80)  ,
			@DebtAddress2 nvarchar(320)  ,
			@DebtAddress3 nvarchar(100)  ,
			@DebtAddress4 nvarchar(100)  ,
			@DebtAddress5 nvarchar(100)  ,
			@DebtAddressCountry int  ,
			@DebtAddressPostcode varchar(12)  ,
			@TECDate datetime  ,
			@Currency int  ,
			@MiddlenameLiable1 nvarchar(50)  ,
			@FullnameLiable1 nvarchar(150)  ,
			@CompanyNameLiable1 nvarchar(50)  ,
			@MinorLiable1 bit  ,
			@Add4Liable1 nvarchar(100)  ,
			@Add5Liable1 nvarchar(100)  ,
			@IsBusinessAddress bit  ,
			@Phone4Liable1 varchar(50)  ,
			@Phone5Liable1 varchar(50)  ,
			@Email3Liable1 varchar(250)  ,
			@BatchDate datetime  ,
			@ClientId int  ,
			@ClientName nvarchar(80)  ,
			@PhaseDate datetime  ,
			@StageDate datetime  ,
			@IsAssignable bit  ,
			@OffenceDate datetime  ,
			@CreatedOn datetime  ,
			@AddCountryLiable1 int  ,
			@CStatus varchar(20)  ,
			@CReturnCode int  ,
			@OffenceNotes nvarchar(max)  ,
			@CaseNotes nvarchar(1000)  ,
     
            @CurrentBatchId        INT
            ,@CaseId                INT
            ,@UnPaidStatusId        INT
            ,@StageId        INT
			,@ExpiredOn	DateTime
			,@LifeExpectancy INT
			,@NewCaseNumber varchar(7)
			,@CaseTypeId INT
			,@VTECDate	DateTime
			,@ANPR bit
			,@cnid INT 
			,@Succeed bit
			,@ErrorId INT
			,@OffenceCodeId int
			,@LeadDefaulterId int
			,@CStatusId int
			,@ReturnRunId int
			,@UserId int = 2
			,@BatchRef varchar(50)
			,@OffNote nvarchar(500)
			,@OffDesc nvarchar(500)
			,@OffLocation nvarchar(500) 
			,@OffDate nvarchar(50)
			,@IsPrimary bit
			,@DPhoneId int
			,@DEmailId int;


		--DECLARE 			
		--	@COTbl TABLE(VALUE nvarchar(1000));
		BEGIN TRANSACTION
			UPDATE [oso].[stg_HMCTS_WOC_Cases] SET OffenceNotes = REPLACE(OffenceNotes,'XXXXXXXXXX','') WHERE Imported = 0
			UPDATE [oso].[stg_HMCTS_WOC_Cases] SET OffenceNotes = REPLACE(OffenceNotes,'01/01/1753 00:00:00','') WHERE Imported = 0
		COMMIT TRANSACTION
		SET @succeed = 1

        SELECT
               @UnPaidStatusId = cs.[Id]
        FROM
               [dbo].[CaseStatus] cs
        WHERE
               cs.[name] = 'Unpaid'

		IF CURSOR_STATUS('global','cursorT')>=-1
		BEGIN
			DEALLOCATE cursorT
		END

        DECLARE cursorT CURSOR LOCAL READ_ONLY FORWARD_ONLY FOR
        SELECT --TOP 100000
			ClientDefaulterReference
			,ClientCaseReference
			,IssueDate
			,FineAmount
			,StartDate
			,LastnameLiable1
			,FirstnameLiable1
			,TitleLiable1
			,DOBLiable1
			,Add1Liable1
			,Add2Liable1
			,Add3Liable1
			,AddPostCodeLiable1
			,NINOLiable1
			,Phone2Liable1
			,Phone3Liable1
			,Phone1Liable1
			,Email1Liable1
			,Email2Liable1
			,OffenceDescription
			,EndDate
			,OffenceCode
			,DebtAddress1
			,DebtAddress2
			,DebtAddress3
			,DebtAddress4
			,DebtAddress5
			,DebtAddressCountry
			,DebtAddressPostcode
			,TECDate
			,Currency
			,MiddlenameLiable1
			,FullnameLiable1
			,CompanyNameLiable1
			,MinorLiable1
			,Add4Liable1
			,Add5Liable1
			,IsBusinessAddress
			,Phone4Liable1
			,Phone5Liable1
			,Email3Liable1
			,BatchDate
			,ClientId
			,ClientName
			,PhaseDate
			,StageDate
			,IsAssignable
			,OffenceDate
			,CreatedOn
			,AddCountryLiable1
			,CStatus
			,CReturnCode
			,OffenceNotes
			,CaseNotes


		FROM
                 [oso].[stg_HMCTS_WOC_Cases]
        WHERE
                 Imported = 0 AND ErrorId IS NULL

        OPEN cursorT
        FETCH NEXT
        FROM
              cursorT
        INTO
			@ClientDefaulterReference
			,@ClientCaseReference
			,@IssueDate
			,@FineAmount
			,@StartDate
			,@LastnameLiable1
			,@FirstnameLiable1
			,@TitleLiable1
			,@DOBLiable1
			,@Add1Liable1
			,@Add2Liable1
			,@Add3Liable1
			,@AddPostCodeLiable1
			,@NINOLiable1
			,@Phone2Liable1
			,@Phone3Liable1
			,@Phone1Liable1
			,@Email1Liable1
			,@Email2Liable1
			,@OffenceDescription
			,@EndDate
			,@OffenceCode
			,@DebtAddress1
			,@DebtAddress2
			,@DebtAddress3
			,@DebtAddress4
			,@DebtAddress5
			,@DebtAddressCountry
			,@DebtAddressPostcode
			,@TECDate
			,@Currency
			,@MiddlenameLiable1
			,@FullnameLiable1
			,@CompanyNameLiable1
			,@MinorLiable1
			,@Add4Liable1
			,@Add5Liable1
			,@IsBusinessAddress
			,@Phone4Liable1
			,@Phone5Liable1
			,@Email3Liable1
			,@BatchDate
			,@ClientId
			,@ClientName
			,@PhaseDate
			,@StageDate
			,@IsAssignable
			,@OffenceDate
			,@CreatedOn
			,@AddCountryLiable1
			,@CStatus
			,@CReturnCode
			,@OffenceNotes
			,@CaseNotes

        WHILE @@FETCH_STATUS = 0
        BEGIN
			    BEGIN TRY
					BEGIN TRANSACTION
						-- Check and update BatchId
						SET @CurrentBatchId = NULL
                        Select @CurrentBatchId = (SELECT TOP 1 Id from Batches where batchDate = CAST(CONVERT(date,@BatchDate) AS DateTime) and clientCaseTypeId in  (select Id from ClientCaseType where clientId = @ClientId ) order by Id desc)
						IF (@CurrentBatchId IS NULL)
						BEGIN
							DECLARE @CurrentDate DateTime = GetDate();
							INSERT into batches
								(
								[referenceNumber],
								[batchDate],
								[closedDate],
								[createdBy],
								[manual],
								[deleted],
								[crossReferenceSent],
								[crossReferenceSentDate],
								[clientCaseTypeId],
								[loadedOn],
								[loadedBy],
								[receivedOn],
								[summaryDate]
								)

							SELECT 
								CONCAT(cct.batchrefprefix,CONVERT(varchar(8),@BatchDate,112))[referenceNumber]
								,CAST(CONVERT(date,@BatchDate) AS DateTime) [batchDate]
								,@CurrentDate [closedDate]
								,2 [createdBy]
								,1 [manual]
								,0 [deleted]
								,1 [crossReferenceSent]
								,@CurrentDate [crossReferenceSentDate]
								,cct.id [clientCaseTypeId]
								,@CurrentDate [loadedOn]
								,2 [loadedBy]
								,@CurrentDate [receivedOn]
								,NULL [summaryDate]
							FROM
								clientcasetype cct
							WHERE
								cct.clientid = @ClientId
						
							SET @CurrentBatchId = SCOPE_IDENTITY();

							-- Set the BatchIDGenerated flag to true in staging
							UPDATE
							[oso].[stg_HMCTS_WOC_Cases]
							SET    BatchNoGenerated = 1
							WHERE
							Imported = 0 
							AND ErrorId is NULL
							AND ClientId = @ClientId
							AND ClientCaseReference = @ClientCaseReference;

						END

						SELECT @BatchRef = [referenceNumber] FROM [dbo].[Batches] WHERE Id = @CurrentBatchId
						
						SELECT @LifeExpectancy = cct.LifeExpectancy, @CaseTypeId = cct.caseTypeId  from clients cli
						INNER JOIN clientcasetype cct on cli.id = cct.clientid
						where cli.id = @ClientId
						
						SET @StageId = NULL

						SELECT
							@StageId = (s.id)
						FROM
							clients cl
							join stages s on s.clientId = cl.id
						WHERE
							s.onLoad = 1
							and cl.id = @Clientid
						
						SET @CStatusId = @UnPaidStatusId;
						SET @ReturnRunId = NULL

						Set @ExpiredOn = DateAdd(day, @LifeExpectancy, @CreatedOn )
			
						SET @ANPR = 0
						SET @VTECDate = NULL
						


						SET @cnid = (  
                                    SELECT MIN(cn.Id)  
                                    FROM CaseNumbers AS cn WITH (  
                                        XLOCK  
                                        ,HOLDLOCK  
                                        )  
                                    WHERE cn.Reserved = 0)

						UPDATE CaseNumbers  
						SET Reserved = 1  
						WHERE Id = @cnid;  
  
						SELECT @NewCaseNumber = alphaNumericCaseNumber from CaseNumbers where Id = @cnid and used = 0  

						-- 2) Insert Case	
						INSERT INTO dbo.[Cases]
							   ( 
									[batchId]
									, [originalBalance]
									, [statusId]
									, [statusDate]
									, [previousStatusId]
									, [previousStatusDate]
									, [stageId]
									, [stageDate]
									, [PreviousStageId]
									, [drakeReturnCodeId]
									, [assignable]
									, [expiresOn]
									, [createdOn]
									, [createdBy]
									, [clientCaseReference]
									, [caseNumber]
									, [note]
									, [tecDate]
									, [issueDate]
									, [startDate]
									, [endDate]
									, [firstLine]
									, [postCode]
									, [address]
									, [addressLine1]
									, [addressLine2]
									, [addressLine3]
									, [addressLine4]
									, [addressLine5]
									, [countryId]
									, [currencyId]
									, [businessAddress]
									, [manualInput]
									, [traced]
									, [extendedDays]
									, [payableOnline]
									, [payableOnlineEndDate]
									, [returnRunId]
									, [defaulterEmail]
									, [debtName]
									, [officerPaymentRunId]
									, [isDefaulterWeb]
									, [grade]
									, [defaulterAddressChanged]
									, [isOverseas]
									, [batchLoadDate]
									, [anpr]
									, [h_s_risk]
									, [billNumber]
									, [propertyBand]
									, [LinkId]
									, [IsAutoLink]
									, [IsManualLink]
									, [PropertyId]
									, [IsLocked]
									, [ClientDefaulterReference]
									, [ClientLifeExpectancy]
									, [CommissionRate]
									, [CommissionValueType]
							   )
							   VALUES
							   (
									@CurrentBatchId --[batchId]
									,@FineAmount --[originalBalance]
									,@CStatusId	--[statusId]
									,GetDate()	--[statusDate]
									,NULL	--[previousStatusId]
									,NULL	--[previousStatusDate]
									,@StageId --[stageId]
									,GetDate() --     This will be replaced by @StageDate from the staging table [stageDate]
									,NULL --[PreviousStageId]
									,NULL --[drakeReturnCodeId] The @CReturncode is the Cid from oso.
									, @IsAssignable --[assignable]
									, @ExpiredOn --[expiresOn]
									, @CreatedOn --[createdOn]
									, 2 -- [createdBy]
									, @ClientCaseReference --[clientCaseReference]
									, @NewCaseNumber --[caseNumber]
									, NULL --[note]
									, @VTECDate --[tecDate]
									, ISNULL(@TECDate,@IssueDate) --[issueDate]
									, @StartDate --[startDate]
									, @EndDate --[endDate]
									, @DebtAddress1--[firstLine]
									, @DebtAddressPostcode--[postCode]
									, RTRIM(LTRIM(IsNull(@DebtAddress2,'') + ' ' + IsNull(@DebtAddress3,'') + ' ' + IsNull(@DebtAddress4,'') + ' ' + IsNull(@DebtAddress5,''))) --[address]
									, @DebtAddress1 --[addressLine1]
									, @DebtAddress2--[addressLine2]
									, @DebtAddress3--[addressLine3]
									, @DebtAddress4--[addressLine4]
									, @DebtAddress5--[addressLine5]
									, @DebtAddressCountry --[countryId]
									, @Currency --[currencyId]
									, @IsBusinessAddress --[businessAddress]
									, 0--[manualInput]
									, 0--[traced]
									, 0--[extendedDays]
									, 1 --[payableOnline]
									, GETDATE() --[payableOnlineEndDate]
									, NULL --[returnRunId]
									, @Email1Liable1 --[defaulterEmail]
			                        , RTRIM(LTRIM(ISNULL(@FullnameLiable1,''))) --[debtName]
									, NULL--[officerPaymentRunId]
									, 0--[isDefaulterWeb]
									, -1 --[grade]
									, 0--[defaulterAddressChanged]
									, 0--[isOverseas]
									, @BatchDate--[batchLoadDate]
									, @ANPR--[anpr]
									, 0 --[h_s_risk]
									, NULL--[billNumber]
									, NULL --[propertyBand]
									, NULL --[LinkId]
									, 1 --[IsAutoLink]
									, 0 --[IsManualLink]
									, NULL --[PropertyId]
									, 0 --[IsLocked]
									, @ClientDefaulterReference --[ClientDefaulterReference]
									, @LifeExpectancy --[ClientLifeExpectancy]
									, NULL --[CommissionRate]
									, NULL --[CommissionValueType]	
						
							   )
						;
            
						SET @CaseId = SCOPE_IDENTITY();
						
						
					--  Insert Case Note
						INSERT INTO dbo.[CaseNotes] 
						( [caseId]
							, [userId]
							, [officerId]
							, [text]
							, [occured]
							, [visible]
							, [groupId]
							, [TypeId]
						)
						VALUES
						(
							@CaseId,
							@UserId,
							NULL,
							@CaseNotes,
							@CreatedOn,
							1,
							NULL,
							NULL
						)

					-- Insert Case Offence.
					IF (@OffenceNotes IS NOT NULL)
					BEGIN
						SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;

						DECLARE @COTbl TABLE(VALUE nvarchar(1000));

						INSERT INTO @COTbl
						SELECT Value 
						FROM oso.splitstring(@OffenceNotes,'|');


						INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						SELECT
							@CaseId,
							IIF((SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 3) IS NULL, CONVERT(datetime, (SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 2), 103),CONVERT(datetime, (SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 3), 103)),
							IIF((SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 3) IS NULL, '', (SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 2)),
							(SELECT Value FROM oso.SplitString(t.Value,';') WHERE idx = 1),
							@OffenceCodeId,
							NULL
						FROM @COTbl AS t;

						DELETE @COTbl;

						--SELECT @OffenceCodeId = Id From Offences WHERE Code = @OffenceCode;
						--INSERT INTO @COTbl
						--SELECT Value 
						--FROM oso.splitstring(@OffenceNotes,'|');

						--WHILE EXISTS(SELECT TOP 1 Value FROM @COTbl)
						--BEGIN
						--	SELECT TOP 1 @OffNote = Value FROM @COTbl
						--	SELECT @OffDesc = Value FROM oso.SplitString(@OffNote,';') WHERE idx = 1;
						--	SELECT @OffLocation = Value FROM oso.SplitString(@OffNote,';') WHERE idx = 2;
						--	IF (@OffLocation = 'XXXXXXXXXX')
						--	BEGIN
						--		SET @OffLocation = NULL
						--	END
						--	SELECT @OffDate = Value FROM oso.SplitString(@OffNote,';') WHERE idx = 3;
						--	IF (@OffDate = '01/01/1753 00:00:00')
						--	BEGIN
						--		SET @OffDate = NULL
						--	END							
						--	INSERT into CaseOffences (caseid, offencedate, offencelocation, notes, offenceid,fineamount)
						--	VALUES
						--	(
						--		@CaseId,
						--		CONVERT(datetime, @OffDate, 103),
						--		@OffLocation,
						--		@OffDesc,
						--		@OffenceCodeId,
						--		NULL
						--	);

						--	DELETE FROM @COTbl WHERE VALUE = @OffNote;
						--END
					END

					-- 4) Insert Defaulters	
					--Lead Defaulter
					IF ((@FullnameLiable1 IS NOT NULL AND LEN(@FullnameLiable1) >0) OR (@CompanyNameLiable1 IS NOT NULL AND LEN(@CompanyNameLiable1) >0))
					BEGIN
						SET @IsPrimary = 1
						SET @IsBusinessAddress = 0
						IF (@CompanyNameLiable1 IS NOT NULL AND LEN(@CompanyNameLiable1) >0)
						BEGIN
							SET @IsBusinessAddress = 1
							SET @FullnameLiable1 = @CompanyNameLiable1
						END
						INSERT INTO dbo.Defaulters (
							[name],
							[title],
							[firstName],
							[middleName],
							[lastName],
							[firstLine],
							[postCode],
							[address],
							[addressLine1],
							[addressLine2],
							[addressLine3],
							[addressLine4],
							[addressLine5],
							[countryId],
							[business],
							[forcesAddress],
							[isOverseas],
							[addressCreatedOn],
							[IsLeadCustomer]
							)

							VALUES
							(
							@FullnameLiable1 --[name],
							,@TitleLiable1  --[title],
							,@FirstnameLiable1 --[firstName],
							,@MiddlenameLiable1 --[middleName],
							,@LastnameLiable1 --[lastName],
							,@Add1Liable1 --[firstLine],
							,@AddPostcodeLiable1 --[postCode],
							, RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))) --[address]
							,@Add1Liable1 --[addressLine1],
							,@Add2Liable1 --[addressLine2],
							,@Add3Liable1 --[addressLine3],
							,@Add4Liable1 --[addressLine4],
							,@Add5Liable1 --[addressLine5],
							,231 --[countryId],
							,@IsBusinessAddress --[business],
							,0 --[forcesAddress],
							,0 --[isOverseas],
							,@createdon --[addressCreatedOn],
							,1
							);
						Set @LeadDefaulterId = SCOPE_IDENTITY();

						-- Insert Defaulter Details
						INSERT INTO dbo.DefaulterDetails (
						[DefaulterId],
						[BirthDate],
						[IsBirthDateConfirmed],
						[IsTreatedAsMinor], 
						[PotentialBirthDate],
						[IsPotentialBirthDateConfirmed],
						[NINO],
						[IsNINOConfirmed],
						[PotentialNINO],
						[IsPotentialNINOConfirmed],
						[BirthDateConfirmedBy],
						[NINOConfirmedBy])
						VALUES
						(
						@LeadDefaulterId,
						@DOBLiable1,
						0,
						@MinorLiable1,
						NULL,
						0,
						@NINOLiable1,
						0,
						NULL,
						0,
						NULL,
						NULL
						)

						-- Insert Contact Address
						Insert Into ContactAddresses (
						[firstLine],
						[postCode],
						[address],
						[addressLine1],
						[addressLine2],
						[addressLine3],
						[addressLine4],
						[addressLine5],
						[countryId],
						[business],
						[defaulterId],
						[createdOn],
						[forcesAddress],
						[isOverseas],
						[StatusId],
						[IsPrimary],
						[IsConfirmed],
						[SourceId],
						[TidReviewedId])
						VALUES
						(
							@Add1Liable1,
							@AddPostcodeLiable1,
							RTRIM(LTRIM(IsNull(@Add2Liable1,'') + ' ' + IsNull(@Add3Liable1,'') + ' ' + IsNull(@Add4Liable1,'') + ' ' + IsNull(@Add5Liable1,''))), --[address]
							@Add1Liable1, --[addressLine1],
							@Add2Liable1, --[addressLine2],
							@Add3Liable1, --[addressLine3],
							@Add4Liable1, --[addressLine4],
							NULL,
							231,
							@IsBusinessAddress, --[business],
							@LeadDefaulterId,
							@CreatedOn,
							0,
							0,
							3,
							@IsPrimary,
							0,
							1,
							3
						)

						-- Insert Defaulter Case
						INSERT INTO dbo.DefaulterCases (caseId, defaulterId)
						VALUES (@CaseId, @LeadDefaulterId)

						-- Insert Defaulter Phone
						IF (@Phone1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterPhones] 
							(
								[defaulterId], 
								[phone], 
								[TypeId],		 
								[LoadedOn], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Phone1Liable1,
								1,
								@CreatedOn,
								1
							)
						END

						IF (@Phone2Liable1 IS NOT NULL)
						BEGIN
							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @LeadDefaulterId AND phone = @Phone2Liable1
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@LeadDefaulterId,
									@Phone2Liable1,
									2,
									@CreatedOn,
									1
								)
							END
						END

						IF (@Phone3Liable1 IS NOT NULL)
						BEGIN

							SET @DPhoneId = NULL
							SELECT @DPhoneId = Id FROM DefaulterPhones WHERE defaulterId = @LeadDefaulterId AND phone = @Phone3Liable1
							IF (@DPhoneId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterPhones] 
								(
									[defaulterId], 
									[phone], 
									[TypeId],		 
									[LoadedOn], 
									[SourceId]
								)
								VALUES
								(
									@LeadDefaulterId,
									@Phone3Liable1,
									2,
									@CreatedOn,
									1
								)
							END
						END

						-- Insert Defaulter Email
						IF (@Email1Liable1 IS NOT NULL)
						BEGIN
							INSERT [dbo].[DefaulterEmails] 
							(
								[DefaulterId], 
								[Email], 
								[DateLoaded], 
								[SourceId]
							)
							VALUES
							(
								@LeadDefaulterId,
								@Email1Liable1,
								@CreatedOn,
								1
							)
						END
						IF (@Email2Liable1 IS NOT NULL)
						BEGIN
							SET @DEmailId = NULL
							SELECT @DEmailId = Id FROM DefaulterEmails WHERE defaulterId = @LeadDefaulterId AND Email = @Email2Liable1
							IF (@DEmailId IS NULL)
							BEGIN
								INSERT [dbo].[DefaulterEmails] 
								(
									[DefaulterId], 
									[Email], 
									[DateLoaded], 
									[SourceId]
								)
								VALUES
								(
									@LeadDefaulterId,
									@Email2Liable1,
									@CreatedOn,
									1
								)
							END			
						END
					END

					-- Insert PhaseLog

					INSERT INTO [dbo].[PhaseLog]
							   ([CaseId]
							   ,[PhaseTypeId]
							   ,[PhaseStart]
							   ,[PhaseEnd]
							   ,[LastProcessedOn]
							   ,[OriginalDuration]
							   ,[CountOnlyActiveDays]
							   ,[ActiveDaysInPhase]
							   ,[WaitingDays]
							   ,[WaitingDaysIncreasedOn]
							   ,[DaysFromEnforcement])
					select
						c.Id,
						p.PhaseTypeId,
						getdate(),
						null,
						getdate(),
						null,
						p.CountOnlyActiveCaseDays,
						0,
						0,
						null,
						0
					from
						cases c
						join batches b on b.id = c.batchid
						join ClientCaseType cct on cct.id = b.clientCaseTypeId
						join clients cl on cl.id = cct.clientId
						join stages s on s.clientId = cl.Id and s.onLoad = 1
						join phases p on p.id = s.PhaseId
					where
						c.Id = @CaseId
						and cl.Id = @ClientId


					-- Set the Imported flag to true in staging
					UPDATE
					[oso].[stg_HMCTS_WOC_Cases]
					SET
					[BatchReferenceNo] = @BatchRef
					,Imported   = 1
					, ImportedOn = GetDate()
					, CaseId = @CaseId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;

					-- New rows creation completed
					COMMIT TRANSACTION

			    END TRY
				BEGIN CATCH
					SET @Succeed = 0
					IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION
					END
					INSERT INTO oso.[SQLErrors] VALUES
						   (Error_number()
								, Error_severity()
								, Error_state()
								, Error_procedure()
								, Error_line()
								, Error_message()
								, Getdate()
						   )

					SET @ErrorId = SCOPE_IDENTITY();

					UPDATE
					[oso].[stg_HMCTS_WOC_Cases]
					SET    ErrorId = @ErrorId
					WHERE
					(Imported = 0 OR Imported IS NULL)
					AND ErrorId is NULL
					AND ClientId = @ClientId
					AND ClientCaseReference = @ClientCaseReference;
				END CATCH

            -- New rows creation completed
            FETCH NEXT
            FROM
                  cursorT
            INTO
				@ClientDefaulterReference
				,@ClientCaseReference
				,@IssueDate
				,@FineAmount
				,@StartDate
				,@LastnameLiable1
				,@FirstnameLiable1
				,@TitleLiable1
				,@DOBLiable1
				,@Add1Liable1
				,@Add2Liable1
				,@Add3Liable1
				,@AddPostCodeLiable1
				,@NINOLiable1
				,@Phone2Liable1
				,@Phone3Liable1
				,@Phone1Liable1
				,@Email1Liable1
				,@Email2Liable1
				,@OffenceDescription
				,@EndDate
				,@OffenceCode
				,@DebtAddress1
				,@DebtAddress2
				,@DebtAddress3
				,@DebtAddress4
				,@DebtAddress5
				,@DebtAddressCountry
				,@DebtAddressPostcode
				,@TECDate
				,@Currency
				,@MiddlenameLiable1
				,@FullnameLiable1
				,@CompanyNameLiable1
				,@MinorLiable1
				,@Add4Liable1
				,@Add5Liable1
				,@IsBusinessAddress
				,@Phone4Liable1
				,@Phone5Liable1
				,@Email3Liable1
				,@BatchDate
				,@ClientId
				,@ClientName
				,@PhaseDate
				,@StageDate
				,@IsAssignable
				,@OffenceDate
				,@CreatedOn
				,@AddCountryLiable1
				,@CStatus
				,@CReturnCode
				,@OffenceNotes
				,@CaseNotes
        END
        CLOSE cursorT
        DEALLOCATE cursorT

    END TRY
    BEGIN CATCH
		SET @Succeed = 0
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		INSERT INTO oso.[SQLErrors] VALUES
				(Error_number()
					, Error_severity()
					, Error_state()
					, Error_procedure()
					, Error_line()
					, Error_message()
					, Getdate()
				)

    END CATCH

	SELECT @Succeed

END
END
GO

