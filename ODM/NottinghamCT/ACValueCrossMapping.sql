﻿
/****** Object:  UserDefinedFunction [oso].[udf_GetOutstandingBalanceIncludingMatchedCases]    Script Date: 07/06/2021 15:53:51 ******/
DROP FUNCTION IF EXISTS [oso].[udf_GetOutstandingBalanceIncludingLinkedCases]
GO

/****** Object:  UserDefinedFunction [oso].[udf_GetOutstandingBalanceIncludingLinkedCases]    Script Date: 07/06/2021 15:53:51 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [oso].[udf_GetOutstandingBalanceIncludingLinkedCases] (@caseId INT)
RETURNS DECIMAL(18,4)
AS
BEGIN

DECLARE @ListOfCaseIDs TABLE (caseId INT); 
DECLARE @CasesInfo TABLE (Id INT IDENTITY, caseId INT, amount DECIMAL(18,4), currency NVARCHAR(20)); 
DECLARE @noLinkedCases INT

DECLARE @counter INT = 1
DECLARE @totalAmount DECIMAL(18,4) = 0.00

INSERT @ListOfCaseIDs(caseId) VALUES (@caseId)

INSERT INTO @ListOfCaseIDs (caseId) 
SELECT MatchingCaseId FROM vw_MatchedCases mc
WHERE CaseId = @caseId

SELECT @noLinkedCases = Count(*) FROM @ListOfCaseIDs

INSERT INTO @casesInfo (caseId, amount, currency) 
SELECT l.caseId, [dbo].[udf_GetOutstandingBalance](l.caseId), cd.CurrencyCode
FROM @ListOfCaseIDs l
JOIN Cases c on l.caseId = c.Id
JOIN Batches b on c.batchId = b.Id
JOIN ClientCaseType ct on b.clientCaseTypeId = ct.Id
JOIN Brands.Brands br on ct.BrandId = br.Id
LEFT JOIN CurrencyDetails cd on c.currencyId = cd.Id
WHERE 
c.statusId IN (
		1 --Unpaid
		,2 --Part-Paid
		,6 --On-Hold
		)
AND NOT EXISTS (
		-- Arrangements conditions.
		--SELECT 1
		--FROM dbo.vw_CasesInActiveArrangements AS ciaa
		--WHERE ciaa.caseId = c.Id
		Select 1
		FROM [dbo].[Arrangements] AS [a]
		JOIN [dbo].[CaseArrangements] AS [ca] ON [a].[id] = [ca].[arrangementId]
		JOIN [dbo].[Cases] AS [c] ON [ca].[caseId] = [c].[Id]
		WHERE [a].[statusId] = 1 and ca.caseId = @caseId
		)

--If currency is null consider them GBP(UK) cases
UPDATE @CasesInfo SET currency = 'GBP' WHERE currency IS NULL

WHILE (@counter <= @noLinkedCases)
	BEGIN
		DECLARE @caseCurrency nvarchar(20)
		DECLARE @targetCurrency nvarchar(3) = 'GBP'
		DECLARE @amount Decimal(18,4)
		DECLARE @convertedCaseAmount Decimal(18,4)

		SELECT @caseCurrency = currency FROM @casesInfo WHERE Id = @counter
		SELECT @amount = amount FROM @casesInfo WHERE Id = @counter
		Select @convertedCaseAmount = dbo.[udf_GetConvertedValueFromExchangeRate](@caseCurrency, @targetCurrency, @amount)

		SET @totalAmount = @totalAmount + @convertedCaseAmount

		SET @counter = @counter +1
	END

	RETURN IIF(@totalAmount IS NULL, 0, @totalAmount)
END
GO


/****** Object:  StoredProcedure [oso].[usp_OS_ACValue_GetXMapping]    Script Date: 07/06/2021 16:18:39 ******/
DROP PROCEDURE IF EXISTS [oso].[usp_OS_ACValue_GetXMapping]
GO

/****** Object:  StoredProcedure [oso].[[usp_OS_ACValue_GetXMapping]]    Script Date: 07/06/2021 16:18:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TN
-- Create date: 25-07-2018
-- Description:	Extract ACValue for open cases
-- =============================================
CREATE PROCEDURE [oso].[usp_OS_ACValue_GetXMapping]
@ClientName nvarchar (500)
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN
SELECT C.Id AS CCaseId, C.clientCaseReference, oso.udf_GetOutstandingBalanceIncludingLinkedCases(C.Id) as ACValue  
FROM dbo.Cases AS C
JOIN dbo.CaseStatus AS CS ON C.statusId = CS.Id  
Join Batches b on C.batchId = b.Id
Join ClientCaseType cct on b.clientCaseTypeId = cct.Id
Join Clients cl on cct.clientId = cl.id
LEFT JOIN dbo.CurrencyDetails AS CD ON C.currencyId = CD.Id  
WHERE 
cl.name in (Select * from dbo.udf_ConvertStringListToTable(@ClientName))
AND c.statusId IN (
		1 --Unpaid
		,2 --Part-Paid
		,6 --On-Hold
		)

		UNION
		SELECT 99999999 AS CCaseId, 'XXXXXXXXXXXXXXXX' AS ClientCaseReference, 0 AS ACValue
    END
END

GO


